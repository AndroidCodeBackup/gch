package inspectiondepot.gch;

import inspectiondepot.gch.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.SocketTimeoutException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;


import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ReportToIT extends Activity implements Runnable  {
	CommonFunctions cf;

	EditText RI_ed_Uname,RI_ed_cmt,RI_ed_cnt_no,RI_ed_cnt_mailid;
	ImageView RI_Image,RI_Image2,RI_Image3;
	ProgressDialog pd;
	private String[] selectedImagePath={"","",""};
	int count=0;
    TextView RI_ed_name;
	String picname,vn;
	RelativeLayout image_li1,image_li2,image_li3;
	protected int show_handler;
	private static final int SELECT_PICTURE = 0;
	 public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf = new CommonFunctions(this);
	        setContentView(R.layout.report_it);
	        cf.getInspectorId();
	        cf.releasecode = (TextView)findViewById(R.id.releasecode);
	        cf.releasecode.setText(cf.apkrc);
	        RI_ed_name=(TextView) findViewById(R.id.RI_ed_name);
	        RI_ed_Uname=(EditText) findViewById(R.id.RI_ed_Uname);
	        RI_ed_cmt=(EditText) findViewById(R.id.RI_ed_cmt);
	        RI_ed_cnt_no=(EditText) findViewById(R.id.RI_ed_cnt_no);
	        RI_ed_cnt_mailid=(EditText) findViewById(R.id.RI_ed_cnt_mailid);
	        RI_Image=(ImageView) findViewById(R.id.RI_image);
	        RI_Image2=(ImageView) findViewById(R.id.RI_image_2);
	        RI_Image3=(ImageView) findViewById(R.id.RI_image_3);
	        RI_ed_name.setText(cf.Insp_firstname+" "+cf.Insp_lastname);
	        cf.Device_Information();
	        image_li1 =(RelativeLayout) findViewById(R.id.img_1);
	        image_li2 =(RelativeLayout) findViewById(R.id.img_2);
	        image_li3 =(RelativeLayout) findViewById(R.id.img_3);
	        TextView tv_comment = (TextView) this.findViewById(R.id.RI_tv_cmt);
			tv_comment.setText(Html.fromHtml(cf.redcolor+" Comments"));
	        RI_ed_cnt_mailid.setText(cf.Insp_email);
	 }
	 public void clicker(View v)
	 {
		 switch(v.getId())
		  {
		  case R.id.RI_bt_send:
		try
		{
			if(cf.Email_Validation(RI_ed_cnt_mailid.getText().toString()).equals("Yes") || RI_ed_cnt_mailid.getText().toString().equals("") )
			{
						
			ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo info = conMgr
					.getActiveNetworkInfo();
			if (info != null && info.isConnected()) {
				
				if(RI_ed_cmt.getText().toString().trim().equals("")){
					cf.ShowToast("The text for comments is not found.",0);
					RI_ed_cmt.requestFocus();
				}
				else{pd = ProgressDialog.show(ReportToIT.this, "", "Reporting to IT", true);
				Thread thread = new Thread(ReportToIT.this);
				thread.start();}
					
			}
			else
			{
				cf.ShowToast("Internet connection is not available.",1);
			}
			}else
			{
				cf.ShowToast("Please enter the valid Email.", 1);
			}
		
			
		}
		catch (Exception ex) 
		{
			String	strerrorlog="Exception happens while Uploading the error information from the user  Error ="+ex;
			cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			
		}
	
	
		  break;
		  case R.id.RI_bt_cancel:
			 RI_ed_Uname.setText("");
			  RI_ed_cmt.setText("");
			  RI_ed_cnt_no.setText("");
			  selectedImagePath[0]="";
			  selectedImagePath[1]="";
			  selectedImagePath[2]="";
			  image_li1.setVisibility(View.GONE);
			  image_li2.setVisibility(View.GONE);
			  image_li3.setVisibility(View.GONE);
			  
		  break;
		  case R.id.RI_bt_brw1:
			  
			  if((image_li1.getVisibility()==View.GONE) || (image_li2.getVisibility()==View.GONE) || (image_li3.getVisibility()==View.GONE))
				{
					
					if((image_li1.getVisibility()==View.GONE))
					{
						count=0;
					}
					else if((image_li2.getVisibility()==View.GONE))
					{
						count=1;
					}
					else if((image_li3.getVisibility()==View.GONE))
					{
						count=2;
					}
					
					Intent intent = new Intent();
					intent.setType("image/*");
					intent.setAction(Intent.ACTION_GET_CONTENT);
					startActivityForResult(Intent.createChooser(intent, "Select Picture"),SELECT_PICTURE);
				}
/*
			  	count=0;
				Intent intent = new Intent();
				intent.setType("image/*");
				intent.setAction(Intent.ACTION_GET_CONTENT);
				startActivityForResult(Intent.createChooser(intent, "Select Picture"),SELECT_PICTURE);*/
		  break;
		  case R.id.RI_bt_brw2:
			  	count=1;
				Intent intent2 = new Intent();
				intent2.setType("image/*");
				intent2.setAction(Intent.ACTION_GET_CONTENT);
				startActivityForResult(Intent.createChooser(intent2, "Select Picture"),SELECT_PICTURE);
		  break;
		  case R.id.RI_bt_brw3:
			  	count=2;
				Intent intent3 = new Intent();
				intent3.setType("image/*");
				intent3.setAction(Intent.ACTION_GET_CONTENT);
				startActivityForResult(Intent.createChooser(intent3, "Select Picture"),SELECT_PICTURE);
		  break;
		  case R.id.RI_bt_home:
		  cf.gohome();
		  break;
		  case R.id.RI_image_c:
			  selectedImagePath[0]="";
			  image_li1.setVisibility(v.GONE); 
		  break;
		  case R.id.RI_image2_c:
			  selectedImagePath[1]="";
			  image_li2.setVisibility(v.GONE);
		  break;
		  case R.id.RI_image3_c:
			 selectedImagePath[2]="";
			  image_li3.setVisibility(v.GONE);
	      break;

			  
		  }
	 }
	 public boolean senduser_error()throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException  {
		// TODO Auto-generated method stub
		 try {
				
			 vn = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		   SoapObject request = new SoapObject(cf.NAMESPACE,"SendReportToIT");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("InspectorID",(cf.Insp_id.equals(""))?"":cf.Insp_id);
			request.addProperty("ApplicationVersion",vn);
			request.addProperty("DeviceName",cf.model);
			request.addProperty("APILevel",cf.apiLevel);
			request.addProperty("InspectorName",RI_ed_name.getText().toString());
			request.addProperty("Comments",RI_ed_cmt.getText().toString());
			request.addProperty("ContactNumber",RI_ed_cnt_no.getText().toString());
			request.addProperty("ContactEmail",RI_ed_cnt_mailid.getText().toString());
			request.addProperty("ReportDate",cf.datewithtime);
		
		if(!selectedImagePath[0].equals("") && image_li1.getVisibility()==View.VISIBLE)
		  {
		  	String mypath = selectedImagePath[0]; // get the full path of the image 
			String temppath[] = mypath.split("/");
			int ss = temppath[temppath.length - 1].lastIndexOf(".");
			String tests = temppath[temppath.length - 1].substring(ss);
			String elevationttype;
			File f = new File(mypath);
			if(f.exists())
			{
				if(tests.equals("") || tests.equals("null")	|| tests == null)
				{
					tests=".jpg";
				}
				elevationttype ="Error image"+tests;
				
				Bitmap bitmap = cf.ShrinkBitmap(selectedImagePath[0], 400, 400);

				MarshalBase64 marshal = new MarshalBase64();
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				bitmap.compress(CompressFormat.PNG, 100, out);
				byte[] raw = out.toByteArray();
				request.addProperty("imgByte1",raw);
				request.addProperty("ImageNameWithExtension1",elevationttype);
				marshal.register(envelope);
			}
		}
		  else
		  {
				
			  	request.addProperty("imgByte1","");
				request.addProperty("ImageNameWithExtension1","");
				
		  }
		 if(!selectedImagePath[1].equals("") && image_li2.getVisibility()==View.VISIBLE)
		  {
		  	String mypath = selectedImagePath[1]; // get the full path of the image 
			String temppath[] = mypath.split("/");
			int ss = temppath[temppath.length - 1].lastIndexOf(".");
			String tests = temppath[temppath.length - 1].substring(ss);
			String elevationttype;
			File f = new File(mypath);
			if(f.exists())
			{
				if(tests.equals("") || tests.equals("null")	|| tests == null)
				{
					tests=".jpg";
				}
				elevationttype ="Error image"+tests;
				
				Bitmap bitmap = cf.ShrinkBitmap(selectedImagePath[1], 400, 400);

				MarshalBase64 marshal = new MarshalBase64();
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				bitmap.compress(CompressFormat.PNG, 100, out);
				byte[] raw = out.toByteArray();
				request.addProperty("imgByte2",raw);
				request.addProperty("ImageNameWithExtension2",elevationttype);
				marshal.register(envelope);
			}
		}
		  else
		  {
				
			  	request.addProperty("imgByte2","");
				request.addProperty("ImageNameWithExtension2","");
				
			
		  }
		 if(!selectedImagePath[2].equals("") && image_li3.getVisibility()==View.VISIBLE)
		  {
		  	String mypath = selectedImagePath[2]; // get the full path of the image 
			String temppath[] = mypath.split("/");
			int ss = temppath[temppath.length - 1].lastIndexOf(".");
			String tests = temppath[temppath.length - 1].substring(ss);
			String elevationttype;
			File f = new File(mypath);
			if(f.exists())
			{
				if(tests.equals("") || tests.equals("null")	|| tests == null)
				{
					tests=".jpg";
				}
				elevationttype ="Error image"+tests;
				
				Bitmap bitmap = cf.ShrinkBitmap(selectedImagePath[2], 400, 400);

				MarshalBase64 marshal = new MarshalBase64();
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				bitmap.compress(CompressFormat.PNG, 100, out);
				byte[] raw = out.toByteArray();
				request.addProperty("imgByte3",raw);
				request.addProperty("ImageNameWithExtension3",elevationttype);
				marshal.register(envelope);
			}
		}
		  else
		  {
				
			  	request.addProperty("imgByte3","");
				request.addProperty("ImageNameWithExtension3","");
			
		  }
		
		  envelope.setOutputSoapObject(request);
		 	HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
			androidHttpTransport.call(cf.NAMESPACE+"SendReportToIT",envelope);
			String result =  envelope.getResponse().toString();
			
			return cf.check_Status(result);
		
	}
	 @Override
	 protected void onActivityResult(int requestCode, int resultCode, Intent data) {
			if (resultCode == RESULT_OK) {
				if (requestCode == SELECT_PICTURE) {
					Uri selectedImageUri = data.getData();
					if(selectedImagePath[0].equals(getPath(selectedImageUri)) || selectedImagePath[1].equals(getPath(selectedImageUri)) || selectedImagePath[2].equals(getPath(selectedImageUri)))
					{
						cf.ShowToast("Your image has been selected already.",1);
					}
					else
					{
						selectedImagePath[count] = getPath(selectedImageUri);
						boolean bu = cf.common(selectedImagePath[count]);
						if (bu) {
								try
								{
									Bitmap b=cf.ShrinkBitmap(selectedImagePath[count], 400, 400);
									if(b!=null)
									{
										if(count==0)
										{
											RI_Image.setImageBitmap(b);
											image_li1.setVisibility(View.VISIBLE);
										}
										else if(count==1)
										{
											RI_Image2.setImageBitmap(b);
											image_li2.setVisibility(View.VISIBLE);
										}
										else if(count==2)
										{
											RI_Image3.setImageBitmap(b);
											image_li3.setVisibility(View.VISIBLE);
										}
									}
									else
									{
										selectedImagePath[count]="";
										cf.ShowToast("This image is not a supported format.You cannot upload.",1);
									}
								}
								catch(Exception e)
								{
									cf.ShowToast("Your selected image have some problem. Please try another one.",1);
									if(count==0)
									{
										
										image_li1.setVisibility(View.GONE);
									}
									else if(count==1)
									{
										
										image_li2.setVisibility(View.GONE);
									}
									else if(count==2)
									{
										
										image_li3.setVisibility(View.GONE);
									}
								}
								
							
						} else {
							cf.ShowToast("Please choose image less then 2MB.",1);
						}
					}
				
				}
		}

	}


	 public String getPath(Uri uri) {
			String[] projection = { MediaStore.Images.Media.DATA };
			Cursor cursor = managedQuery(uri, projection, null, null, null);
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		boolean b;
		try {
			b = senduser_error();
			if(b)
			{
				show_handler=0;
				handler.sendEmptyMessage(0);
			}
			else
			{
				show_handler=1;
				
				
				handler.sendEmptyMessage(0);
			}
		} catch (SocketTimeoutException e) {
			show_handler=2;
			handler.sendEmptyMessage(0);
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NetworkErrorException e) {
			show_handler=2;
			handler.sendEmptyMessage(0);
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			show_handler=2;
			
			// TODO Auto-generated catch block
			handler.sendEmptyMessage(0);
			
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			show_handler=2;
			handler.sendEmptyMessage(0);
			
		}
	}
		
		Handler handler = new Handler() {
			public void handleMessage(Message msg) {
				pd.dismiss();
				if(show_handler==0)
				{
					cf.ShowToast("Thank you for submitting your IT issue. We will review this and get back to you as soon as possible.", 1);
					 RI_ed_Uname.setText("");
					  RI_ed_cmt.setText("");
					  RI_ed_cnt_no.setText("");
					  selectedImagePath[0]="";
					  selectedImagePath[1]="";
					  selectedImagePath[2]="";
					  image_li1.setVisibility(View.GONE);
					  image_li2.setVisibility(View.GONE);
					  image_li3.setVisibility(View.GONE);
							  
				}
				else if(show_handler==1)
				{
					cf.ShowToast("Report moving to admin failed.", 1);
				}
				else if(show_handler==2)
				{
					String	strerrorlog="Exception happens while Uploading the error information from the user  Error =";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			}
		};
	}


