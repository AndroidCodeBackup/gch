package inspectiondepot.gch;
 
import inspectiondepot.gch.R;

import java.util.ArrayList;
 
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;
 
import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;

 
public class AddItemizedOverlay extends ItemizedOverlay<OverlayItem> {
 
       private ArrayList<OverlayItem> mapOverlays = new ArrayList<OverlayItem>();
 
       private Context context;
       private GeoPoint geoPoint;
       private Drawable drawable;
 
       public AddItemizedOverlay(Drawable defaultMarker) {
            super(boundCenterBottom(defaultMarker));
          

       }
 
       public AddItemizedOverlay(Drawable defaultMarker, Context contexts) {
            this(defaultMarker);
            this.context = contexts;
  
       }
 
       @Override
       protected OverlayItem createItem(int i) {
          return mapOverlays.get(i);
       }
 
       @Override
       public int size() {
          return mapOverlays.size();       }
 
       @Override
       protected boolean onTap(int index) {
         OverlayItem item = mapOverlays.get(index);

          Dialog dialog = new Dialog(context,android.R.style.Theme_Translucent_NoTitleBar);
          dialog.getWindow().setContentView(R.layout.custom);
          dialog.setTitle(item.getTitle());
          dialog.show();
      

          return true;
          

       }
      
 
       public void addOverlay(OverlayItem overlay) {
          mapOverlays.add(overlay);
           this.populate();
       }
 
    }