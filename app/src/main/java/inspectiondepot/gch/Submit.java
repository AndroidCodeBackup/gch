package inspectiondepot.gch;

import inspectiondepot.gch.R;

import java.io.File;
import java.net.InetAddress;


import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.Random;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class Submit extends Activity {
	private static final int visibility = 0;
	private static final String TAG = null;
	RadioButton accept, decline;
	EditText etsetword, etgetword;
	Resources res;
	private String[] myString;
	String word,strhomeid,data="";
	TextView tv1, tv2;
	String  erromsg="",fe="",be="",le="",re="",ae="",roof="",off="",sup="";
	
	CommonFunctions cf;
	private static final Random rgenerator = new Random();

	public void onCreate(Bundle SavedInstanceState) {
		super.onCreate(SavedInstanceState);
		cf = new CommonFunctions(this);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			cf.selectedhomeid = strhomeid= extras.getString("homeid");
			cf.onlinspectionid = extras.getString("InspectionType");
		    cf.onlstatus = extras.getString("status");
	 	}
		cf.getInspectorId();
		setContentView(R.layout.submit);
		/**Used for hide he key board when it clik out side**/

		cf.setupUI((ScrollView) findViewById(R.id.scr));

	/**Used for hide he key board when it clik out side**/
		/** menu **/
		cf.getDeviceDimensions();
		LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
		mainmenu_layout.setMinimumWidth(cf.wd);
	    mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(),9, 0,cf));
			
		res = getResources();
		myString = res.getStringArray(R.array.myArray);
		accept = (RadioButton) this.findViewById(R.id.accept);
		decline = (RadioButton) this.findViewById(R.id.decline);
		tv1 = (TextView) findViewById(R.id.textView2);
		tv2 = (TextView) findViewById(R.id.textView1);

		etsetword = (EditText) findViewById(R.id.wrdedt);
		etgetword = (EditText) findViewById(R.id.wrdedt1);
		word = myString[rgenerator.nextInt(myString.length)];
		
		etsetword.setText(word);
		if (word.contains(" ")) {
			word = word.replace(" ", "");
		}
		try {

		    Cursor cm = cf.SelectTablefunction(cf.inspectorlogin,
					" where Fld_InspectorId='" + cf.encode(cf.Insp_id) + "'");
			int rws1 = cm.getCount();
			cm.moveToFirst();
			data = cf.decode(
					cm.getString(cm.getColumnIndex("Fld_InspectorFirstName")))
					.toLowerCase();
			data += " ";
			data += cf.decode(
					cm.getString(cm.getColumnIndex("Fld_InspectorLastName")))
					.toLowerCase();

		} catch (Exception e) {
			
		}
		tv1.setText(Html
				.fromHtml("<font color=black>I, "
						+ "</font>"
						+ "<b><font color=#bddb00>  "
						+ data 
						+ "</font></b><font color=black> (Field Inspector) have conducted this inspection in accordance with the required standards and processes and confirm that I am properly licensed to conduct and sign off on the same inspection. I have made every attempt to collect the data required, have spoken to the Policyholder and/or Agent of record and have conducted the necessary research requirements to ensure permitting, replacement cost valuations or other information are properly completed as part of this inspection process."
						+ "</font>"));
		tv2.setText(Html
				.fromHtml("<font color=black>I, "
						+ "</font>"
						+ "<b><font color=#bddb00> "+data
						+ "</font></b><font color=black> (Field Inspector) also understand that failure to properly conduct this inspection and submit the verifiable inspection data is grounds for termination and considered possible insurance fraud."
						+ "</font>"));
		accept.setOnClickListener(onClickAnswer1);
		decline.setOnClickListener(onClickAnswer2);

		
	}

	public OnClickListener onClickAnswer1 = new OnClickListener() {

		public void onClick(View arg0) {
			accept.setSelected(true);
			decline.setChecked(false);
		}

	};
	public OnClickListener onClickAnswer2 = new OnClickListener() {

		public void onClick(View arg0) {
			
			accept.setSelected(false);
			decline.setChecked(true);
		}

	};

	public void clicker(View v) {
		switch (v.getId()) {

		case R.id.submit:
			cf.Create_Table(11);
			cf.Create_Table(15);
			cf.Create_Table(21);
			cf.Create_Table(22);
			if (accept.isChecked() == true) {
				if (etgetword.getText().toString().equals("")) {
                    cf.ShowToast("Please enter Word Verification.",0);
					etgetword.requestFocus();
				} 
				else 
				{
					if (etgetword.getText().toString().equals(word.trim().toString()))
					{ erromsg=fe=be=le=re=ae=roof=off=sup="";
						Cursor c2 = cf.SelectTablefunction(cf.General_Roof_information, " where RC_SRID='"+cf.selectedhomeid+"'");
						if(c2.getCount()>0)
						{
							System.out.println("the SRID "+cf.selectedhomeid);
							Cursor c1=cf.SelectTablefunction(cf.Roof_cover_type, " where RC_RCT_SRID='"+cf.selectedhomeid+"'");
							
							if(c1.getCount()>0)
							{ c1.moveToFirst();
								boolean b=false;
								for(int i=0;i<c1.getCount();i++)
								{ System.out.println("the pre dom"+c1.getString(c1.getColumnIndex("RC_RCT_Domi")));
									if(c1.getString(c1.getColumnIndex("RC_RCT_Domi")).trim().equals("1"))
									{
										b=true;
									}
									c1.moveToNext();
								}
								if(b)
								{
									if(checkforImageAvailability())
									{
									update();
									}
									else
									{
										ErrorAlert();
										
									}
								}
								else
								{
									cf.ShowToast("Please select Roof Cover Predominant for atleast one Roof Cover Type in Roof Section.", 0);
								}
							}else
							{
								cf.ShowToast("Please enter atleast one Roof Cover Type in Roof Section.", 0);
							}
						}
						else
						{
							if(checkforImageAvailability())
							{
								update();
							}
							else
							{
								ErrorAlert();
								
							}
						}
					}
					else
					{
						cf.ShowToast("Please enter valid Word Verification(case sensitive).", 1);
					    etgetword.setText("");
						etgetword.requestFocus();
					}
				}
			}      
			else
			{
				cf.ShowToast("Please select Accept Radio Button.", 1);
			}
			break;
		case R.id.home:
			  cf.gohome();
			  break;
		case R.id.S_refresh:
			
			word = myString[rgenerator.nextInt(myString.length)];
			
			etsetword.setText(word);
			if (word.contains(" ")) {
				word = word.replace(" ", "");
			}
			
			break;
		}
	}
	private void ErrorAlert()
	{
		final Dialog dialog1 = new Dialog(Submit.this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alert);

		/**
		 * get the help and update relative layou and set the visbilitys for the
		 * respective relative layout
		 */
		LinearLayout Re = (LinearLayout) dialog1.findViewById(R.id.maintable);
		Re.setVisibility(View.GONE);
		LinearLayout Reup = (LinearLayout) dialog1.findViewById(R.id.delete_img);
		Reup.setVisibility(View.VISIBLE);
		TextView tv =(TextView) dialog1.findViewById(R.id.DI_report);
		String tst="The images/PDFs have been corrupted while saving. Please re upload the image/PDF in the following section.<br><br>";
		if(!fe.trim().equals(""))
		{
			tst +="   Front Elevation Images : "+fe.substring(0,fe.length()-1)+"<br><br>";
		}
		if(!re.trim().equals(""))
		{
			tst +="   Right Elevation Images : "+re.substring(0,re.length()-1)+"<br><br>";
		}
		if(!be.trim().equals(""))
		{
			tst +="   Back Elevation Images : "+be.substring(0,be.length()-1)+"<br><br>";
		}
		if(!le.trim().equals(""))
		{
			tst +="   Left Elevation Images : "+le.substring(0,le.length()-1)+"<br><br>";
		}
		if(!roof.trim().equals(""))
		{
			tst +="   Roof Images : "+roof.substring(0,roof.length()-1)+"<br><br>";
		}
		if(!ae.trim().equals(""))
		{
			tst +="   Additional Photographs Images : "+ae.substring(0,ae.length()-1)+"<br><br>";
		}
		if(off.trim().equals("true"))
		{
			tst +="   Feedback Document, Office Use <br><br>";
		}if(sup.trim().equals("true"))
		{
			tst +="   Feedback Document, Supplemental <br><br>";
		}
		
		tv.setText(Html.fromHtml(tst));
		Button 	b1 =(Button) dialog1.findViewById(R.id.DI_yes);
		b1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				update();
				dialog1.setCancelable(true);
				dialog1.cancel();
				
				
			}
		});
		Button 	b2 =(Button) dialog1.findViewById(R.id.DI_close);
		b2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//update();
				dialog1.setCancelable(true);
				dialog1.cancel();
				
				
			}
		});
		Button 	b3 =(Button) dialog1.findViewById(R.id.DI_no);
		b3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//update();
				dialog1.setCancelable(true);
				dialog1.cancel();
				
				
			}
		});
		dialog1.setCancelable(false);
		dialog1.show();
	}
	private boolean checkforImageAvailability() {
		// TODO Auto-generated method stub
		erromsg="";
		Cursor c=cf.SelectTablefunction(cf.ImageTable, " Where GCH_IM_SRID='"+cf.selectedhomeid+"'");
		
		if(c.getCount()>0)
		{
			c.moveToFirst();
		for (int i=0;i<c.getCount();i++)
		{
			String mypath = cf.decode(c.getString(c.getColumnIndex("GCH_IM_ImageName"))); // get the full path of the image 
			String temppath[] = mypath.split("/");
			int ss = temppath[temppath.length - 1].lastIndexOf(".");
			String tests = temppath[temppath.length - 1].substring(ss);
			String elevationttype;
			File f = new File(mypath);
		
			/** check the file is exist in the device **/
			if(f!=null)
			{
			if (!f.exists()) {
				if (c.getString(3).toString().equals("1")) {
					fe += c.getString(9).toString()+",";
					erromsg += "Front Elevation Image Orders : "
							+ c.getString(9).toString() +"<br>";
				} else if (c.getString(3).toString().equals("2")) {
					re += c.getString(9).toString()+",";
					erromsg += "Right Elevation Image Orders : "
							+ c.getString(9).toString()+"<br>";
				} else if (c.getString(3).toString().equals("3")) {
					be += c.getString(9).toString()+",";
					erromsg += "Back Elevation Image Orders : "
							+ c.getString(9).toString()+"<br>";
				} else if (c.getString(3).toString().equals("4")) {
					le += c.getString(9).toString()+",";
					erromsg += "Left Elevation Image Orders : "
							+ c.getString(9).toString()+"<br>";
				} else if (c.getString(3).toString().equals("5")) {
					ae += c.getString(9).toString()+",";
					erromsg += "Additional Photograps Image Orders : "
							+ c.getString(9).toString()+"<br>";
				} else if (c.getString(3).toString().equals("6")) {
					roof += c.getString(9).toString()+",";
					erromsg += "Roof Images Image Orders : "
							+ c.getString(9).toString()+"<br>";
				} else  {
					erromsg += "Image not availabel in the Orders : "
							+ c.getString(9).toString()+"<br>";
				}
				
			}
			
			
				
		}
			else
			{
				if (c.getString(3).toString().equals("1")) {
					fe += c.getString(9).toString()+",";
					erromsg += "Front Elevation Image Order : "
							+ c.getString(9).toString() +"<br>";
				} else if (c.getString(3).toString().equals("2")) {
					re += c.getString(9).toString()+",";
					erromsg += "Right Elevation Image Order : "
							+ c.getString(9).toString()+"<br>";
				} else if (c.getString(3).toString().equals("3")) {
					be += c.getString(9).toString()+",";
					erromsg += "Back Elevation Image Order : "
							+ c.getString(9).toString()+"<br>";
				} else if (c.getString(3).toString().equals("4")) {
					le += c.getString(9).toString()+",";
					erromsg += "Left Elevation Image Order : "
							+ c.getString(9).toString()+"<br>";
				} else if (c.getString(3).toString().equals("5")) {
					ae += c.getString(9).toString()+",";
					erromsg += "Additional Photograps Image Order : "
							+ c.getString(9).toString()+"<br>";
				} else if (c.getString(3).toString().equals("6")) {
					roof+= c.getString(9).toString()+",";
					erromsg += "Roof Images Image Order : "
							+ c.getString(9).toString()+"<br>";
				} else  {
					erromsg += "Image not availabel in the Order : "
							+ c.getString(9).toString()+"<br>";
				}
			}
		
			c.moveToNext();
		}
		
		}
		
		Cursor c1=cf.SelectTablefunction(cf.FeedBackDocumentTable, " Where GCH_FI_D_SRID='"+cf.selectedhomeid+"'");
		
		if(c1.getCount()>0)
		{
			c1.moveToFirst();
		for (int i=0;i<c1.getCount();i++)
		{
			String mypath = cf.decode(c1.getString(c1.getColumnIndex("GCH_FI_D_FileName"))); // get the full path of the image 
			String temppath[] = mypath.split("/");
			int ss = temppath[temppath.length - 1].lastIndexOf(".");
			String tests = temppath[temppath.length - 1].substring(ss);
			String elevationttype;
			File f = new File(mypath);
			
			/** check the file is exist in the device **/
			if(f!=null)
			{
			if (!f.exists()) {
				if (c1.getString(c1.getColumnIndex("GCH_FI_D_IsOfficeUse")).toString().equals("1")) {
					off="true";
					erromsg += "Feed back Document Office Use Title : "
							+ cf.decode(c1.getString(c1.getColumnIndex("GCH_FI_D_DocumentTitle")).toString()) +"<br>";
				} else {
					sup="true";
					erromsg += "Feed back Document Supplemental  Title : "
							+ cf.decode(c1.getString(c1.getColumnIndex("GCH_FI_D_DocumentTitle")).toString()) +"<br>";
				} 
				
			}
		}
		
			c1.moveToNext();
		}
		}
			if(erromsg.trim().equals(""))
			{
				return true;
			}
			else
			{
				return false;
			}
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if(cf.strschdate.equals("")){
				cf.ShowToast("Layout navigation without scheduling not allowed. ", 1);
			}else{ cf.netalert("MAPD");}
			return true;
		}
		return true;
	}
	
	public void update()
	{
		cf.gch_db.execSQL("UPDATE " + cf.policyholder+ " SET GCH_PH_IsInspected=1,GCH_PH_Status=1 WHERE GCH_PH_SRID ='"	+ cf.encode(cf.selectedhomeid) + "' and GCH_PH_InspectorId = '"	+ cf.encode(cf.Insp_id) + "'");
		cf.ShowToast("Saved sucessfully.", 1);
		Intent inte = new Intent(getApplicationContext(),ExportInspection.class);
		inte.putExtra("type", "export");
		startActivity(inte);
	}
	
}
