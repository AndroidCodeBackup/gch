package inspectiondepot.gch;

import inspectiondepot.gch.R;

import org.ksoap2.serialization.SoapObject;


import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Dashboard extends Activity implements Runnable{
	CommonFunctions cf;
	LinearLayout mainlinear;
	int v;
	TextView txtexplain,txtcit,txtcio,txtuts,txtcan,onltxtcio,onltxtuts,onltxtcan;
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf = new CommonFunctions(this);
	        setContentView(R.layout.dash);
	        //mainlinear = (LinearLayout)findViewById(R.id.mainlin);
	        cf.releasecode = (TextView)findViewById(R.id.releasecode);
	        cf.releasecode.setText(cf.apkrc);
	        cf.getInspectorId();
	        cf.welcome = (TextView) this.findViewById(R.id.welcomename);
	        cf.welcome.setText(cf.Insp_firstname.toUpperCase()+" "+cf.Insp_lastname.toUpperCase());
	        txtcit = (TextView)findViewById(R.id.txtcit);
	        txtcio = (TextView)findViewById(R.id.txtcio);
	        txtuts = (TextView)findViewById(R.id.txtuts);
	        txtcan = (TextView)findViewById(R.id.txtcan);
	        onltxtcio = (TextView)findViewById(R.id.onltxtcio);
	        onltxtuts = (TextView)findViewById(R.id.onltxtuts);
	        onltxtcan = (TextView)findViewById(R.id.onltxtcan);
	        txtcit.setText(Html.fromHtml("CIT "+cf.redcolor));
	        txtcio.setText(Html.fromHtml("CIO "+cf.redcolor));
	        txtuts.setText(Html.fromHtml("UTS "+cf.redcolor));
	        txtcan.setText(Html.fromHtml("CAN "+cf.redcolor));
	        onltxtcio.setText(Html.fromHtml("CIO "+cf.redcolor));
	        onltxtuts.setText(Html.fromHtml("UTS "+cf.redcolor));
	        onltxtcan.setText(Html.fromHtml("CAN "+cf.redcolor));
	        txtexplain = (TextView)findViewById(R.id.txtbriefexplanation);
            txtexplain.setText(Html.fromHtml("CIT "+cf.redcolor+"  - Completed Inspections in Tablet " + ";"+" "
							+ "CIO "+cf.redcolor+"  - Completed Inspections in Online" + "<br/>"
							+ "UTS "+cf.redcolor+"  - Unable to Schedule " + ";"+" "
							+ "CAN "+cf.redcolor+" - Cancelled Inspection" + "<br/>"));
            
            /*DECLARATION OF BUTTON - CARRIER AND RETAIL */
            cf.btn_carrassign = (Button)findViewById(R.id.carrassign);
            cf.btn_carrschedule= (Button)findViewById(R.id.carrschedule);
            cf.btn_carrcit = (Button)findViewById(R.id.carrcit);
            cf.btn_carrcio = (Button)findViewById(R.id.carrcio);
            cf.btn_carruts = (Button)findViewById(R.id.carruts);
            cf.btn_carrcan = (Button)findViewById(R.id.carrcan);
            cf.btn_carrtotal = (Button)findViewById(R.id.carrtotal);
            
            cf.btn_retassign = (Button)findViewById(R.id.retassign);
            cf.btn_retschedule= (Button)findViewById(R.id.retschedule);
            cf.btn_retcit = (Button)findViewById(R.id.retcit);
            cf.btn_retcio = (Button)findViewById(R.id.retcio);
            cf.btn_retuts = (Button)findViewById(R.id.retuts);
            cf.btn_retcan = (Button)findViewById(R.id.retcan);
            cf.btn_rettotal = (Button)findViewById(R.id.rettotal);
            
            /*DECLARATION OF BUTTON - CARRIER AND RETAIL - ONLINE*/
            cf.btn_onlcarrassign = (Button)findViewById(R.id.onlcarrassign);
            cf.btn_onlcarrschedule= (Button)findViewById(R.id.onlcarrschedule);
            cf.btn_onlcarrcio = (Button)findViewById(R.id.onlcarrcio);
            cf.btn_onlcarruts = (Button)findViewById(R.id.onlcarruts);
            cf.btn_onlcarrcan = (Button)findViewById(R.id.onlcarrcan);
            cf.btn_onlcarrtotal = (Button)findViewById(R.id.onlcarrtotal);
            
            cf.btn_onlretassign = (Button)findViewById(R.id.onlretassign);
            cf.btn_onlretschedule= (Button)findViewById(R.id.onlretschedule);
            cf.btn_onlretcio = (Button)findViewById(R.id.onlretcio);
            cf.btn_onlretuts = (Button)findViewById(R.id.onlretuts);
            cf.btn_onlretcan = (Button)findViewById(R.id.onlretcan);
            cf.btn_onlrettotal = (Button)findViewById(R.id.onlrettotal);
            db_sttaus();

	 }
	 private void db_sttaus() {
		// TODO Auto-generated method stub
		
				try {
					Cursor cur = cf.SelectTablefunction(cf.policyholder," where GCH_PH_InspectorId='" + cf.encode(cf.Insp_id) + "'");
					
					cur.moveToFirst();
					if (cur != null) {
						do{
							/*CHECKING SINKHOLE CARRIER*/
							if (cf.encode(cf.Insp_id).equals(cf.encode(cur.getString(cur.getColumnIndex("GCH_PH_InspectorId"))))
									&& "12".equals(cf.encode(cur.getString(cur.getColumnIndex("GCH_PH_InspectionTypeId"))))) {
									if(cf.encode(cur.getString(cur.getColumnIndex("GCH_PH_Status"))).equals("40")
											&& !cf.encode(cur.getString(cur.getColumnIndex("GCH_PH_SubStatus"))).equals("41")) {
										cf.carrschedule++;
									}
									if(cf.encode(cur.getString(cur.getColumnIndex("GCH_PH_Status"))).equals("30")) {
										cf.carrassign++;
									}
									if(cf.encode(cur.getString(cur.getColumnIndex("GCH_PH_SubStatus"))).equals("41")) {
										cf.carrcio++;
									}
									if(cf.encode(cur.getString(cur.getColumnIndex("GCH_PH_Status"))).equals("110")) {
										cf.carruts++;
									}
									if(cf.encode(cur.getString(cur.getColumnIndex("GCH_PH_Status"))).equals("111")) {
										cf.carrcan++;
									}
									if(cf.encode(cur.getString(cur.getColumnIndex("GCH_PH_IsInspected"))).equals("1")) {
										cf.carrcit++;
									}
                           	}
							/*CHECKING SINKHOLE */
							if (cf.encode(cf.Insp_id).equals(cf.encode(cur.getString(cur.getColumnIndex("GCH_PH_InspectorId"))))
									&& "11".equals(cf.encode(cur.getString(cur.getColumnIndex("GCH_PH_InspectionTypeId"))))) {
								if(cf.encode(cur.getString(cur.getColumnIndex("GCH_PH_Status"))).equals("40")
										&& !cf.encode(cur.getString(cur.getColumnIndex("GCH_PH_SubStatus"))).equals("41")) {
									cf.retschedule++;
								}
								if (cf.encode(cur.getString(cur.getColumnIndex("GCH_PH_Status"))).equals("30")) {
									cf.retassign++;
								}
								if (cf.encode(cur.getString(cur.getColumnIndex("GCH_PH_SubStatus"))).equals("41")) {
									cf.retcio++;
								}
								if (cf.encode(cur.getString(cur.getColumnIndex("GCH_PH_Status"))).equals("110")) {
									cf.retuts++;
								}
								if (cf.encode(cur.getString(cur.getColumnIndex("GCH_PH_Status"))).equals("111")) {
									cf.retcan++;
								}
								if (cf.encode(cur.getString(cur.getColumnIndex("GCH_PH_IsInspected"))).equals("1")) {
									cf.retcit++;
								}
							}

						} while (cur.moveToNext());
					}
					cur.close();
				} catch (Exception e) {
					cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ Dashboard.this +" "+" in the processing stage of separating carrier and retail data from PH table  at "+" "+ cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
					
				}
				cf.carrtotal = cf.carrschedule + cf.carrassign + cf.carrcit + cf.carrcio + cf.carruts + cf.carrcan;
                cf.rettotal = cf.retschedule + cf.retassign + cf.retcit	+ cf.retcio + cf.retuts + cf.retcan;
               
                /*SETITNG VALUES TO THE BUTTONS OF CARRIER AND RETAIL*/
                cf.btn_carrassign.setText(String.valueOf(cf.carrassign));
                cf.btn_carrschedule.setText(String.valueOf(cf.carrschedule));
              //  cf.btn_carrschedule.setText("2");
                cf.btn_carrcit.setText(String.valueOf(cf.carrcit));
                cf.btn_carrcio.setText(String.valueOf(cf.carrcio));
                cf.btn_carruts.setText(String.valueOf(cf.carruts));
                cf.btn_carrcan.setText(String.valueOf(cf.carrcan));
                cf.btn_carrtotal.setText(String.valueOf(cf.carrtotal));
              
                cf.btn_retassign.setText(String.valueOf(cf.retassign));
                cf.btn_retschedule.setText(String.valueOf(cf.retschedule));
                cf.btn_retcit.setText(String.valueOf(cf.retcit));
                cf.btn_retcio.setText(String.valueOf(cf.retcio));
                cf.btn_retuts.setText(String.valueOf(cf.retuts));
                cf.btn_retcan.setText(String.valueOf(cf.retcan));
                cf.btn_rettotal.setText(String.valueOf(cf.rettotal));
               
	}
	public void clicker(View v)
	 {
		  switch(v.getId())
		  {
		  case R.id.home:
			  cf.gohome();
			  break;
		  case R.id.carrassign: /*CLICK EVENT FOR CARRIER ASSIGN*/
			  cf.MoveTo_HomeOwner("12",cf.carrassign,"Assign",1);
			  break;
		  case R.id.carrschedule: /*CLICK EVENT FOR CARRIER SCHEDULE*/
			 // cf.carrschedule=2;
			  cf.MoveTo_HomeOwner("12",cf.carrschedule,"Schedule",1);
			  break;
		  case R.id.carrcit: /*CLICK EVENT FOR CARRIER CIT*/
			  cf.MoveTo_HomeOwner("12",cf.carrcit,"CIT",1);
			  break;
		  case R.id.carrcio: /*CLICK EVENT FOR CARRIER CIO*/
			  cf.MoveTo_HomeOwner("12",cf.carrcio,"CIO",2);
			  break;
		  case R.id.carruts: /*CLICK EVENT FOR CARRIER UTS*/
			  cf.MoveTo_HomeOwner("12",cf.carruts,"UTS",2);
			  break;
		  case R.id.carrcan: /*CLICK EVENT FOR CARRIER CAN*/
			  cf.MoveTo_HomeOwner("12",cf.carrcan,"CAN",2);
			  break;
		  case R.id.carrtotal: /*CLICK EVENT FOR CARRIER TOTAL*/
			  if(String.valueOf(cf.carrtotal).equals("0"))
			  {
				  cf.ShowToast("Sorry, No records found for General Conditions and Hazards Carrier.",0);
			  }
			  else
			  {
				  cf.ShowToast("Total No. of Records : " + cf.carrtotal, 1);
			  }
			  break;
		  case R.id.retassign: /*CLICK EVENT FOR RETAIL ASSIGN*/
			  cf.MoveTo_HomeOwner("11",cf.retassign,"Assign",1);
			  break;
		  case R.id.retschedule: /*CLICK EVENT FOR RETAIL SCHEDULE*/
			  cf.MoveTo_HomeOwner("11",cf.retschedule,"Schedule",1);
			  break;
		  case R.id.retcit: /*CLICK EVENT FOR RETAIL CIT*/
			  cf.MoveTo_HomeOwner("11",cf.retcit,"CIT",1);
			  break;
		  case R.id.retcio: /*CLICK EVENT FOR RETAIL CIO*/
			  cf.MoveTo_HomeOwner("11",cf.retcio,"CIO",2);
			  break;
		  case R.id.retuts: /*CLICK EVENT FOR RETAIL UTS*/
			  cf.MoveTo_HomeOwner("11",cf.retuts,"UTS",2);
			  break;
		  case R.id.retcan: /*CLICK EVENT FOR RETAIL CAN*/
			  cf.MoveTo_HomeOwner("11",cf.retcan,"CAN",2);
			  break;
		  case R.id.rettotal: /*CLICK EVENT FOR RETAIL TOTAL*/
			  if(String.valueOf(cf.rettotal).equals("0"))
			  {
				  cf.ShowToast("Sorry, No records found for General Conditions and Hazards.",0);
			  }
			  else
			  {
				  cf.ShowToast("Total No. of Records : " + cf.rettotal, 1);
			  }
			  break;
		  case R.id.online:
			//  mainlinear.setActivated(false);
			  cf.alerttitle="Synchronization";
			  cf.alertcontent="Are you sure want to Synchronize  with Online?";
			    final Dialog dialog1 = new Dialog(Dashboard.this,android.R.style.Theme_Translucent_NoTitleBar);
				dialog1.getWindow().setContentView(R.layout.alertsync);
				TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
				txttitle.setText( cf.alerttitle);
				TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
				txt.setText(Html.fromHtml( cf.alertcontent));
				Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
				Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
				btn_yes.setOnClickListener(new OnClickListener()
				{
                	@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						dialog1.dismiss();
						if(cf.isInternetOn()==true)
						{
                           String source = "<font color=#FFFFFF>Loading data. Please wait..."
									+ "</font>";
							cf.pd = ProgressDialog.show(Dashboard.this,"", Html.fromHtml(source), true);
							Thread thread = new Thread(Dashboard.this);
							thread.start();
						}
						else
						{
							cf.ShowToast("Internet connection is not available.",1);
						}
						
					}
					
				});
				btn_cancel.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						dialog1.dismiss();
						
					}
					
				});
				dialog1.setCancelable(false);
				dialog1.show();
			    
			  break;
		  case R.id.onlcarrassign: /*CLICK EVENT FOR ONLINE CARRIER -ASSIGN*/
			  cf.MoveTo_OnlineList("12",cf.onlcarrassign,"30","0","Assign");
			  break;
		  case R.id.onlcarrschedule: /*CLICK EVENT FOR ONLINE CARRIER -SCHEDULE*/
			  cf.MoveTo_OnlineList("12",cf.onlcarrschedule,"40","0","Schedule");
			  break;
		  case R.id.onlcarrcio: /*CLICK EVENT FOR ONLINE CARRIER -CIO*/
			  cf.MoveTo_OnlineList("12",cf.onlcarrcio,"40","41","CIO");
			  break;
		  case R.id.onlcarruts: /*CLICK EVENT FOR ONLINE CARRIER -UTS*/
			  cf.MoveTo_OnlineList("12",cf.onlcarruts,"110","111","UTS");
			  break;
		  case R.id.onlcarrcan: /*CLICK EVENT FOR ONLINE CARRIER -CAN*/
			  cf.MoveTo_OnlineList("12",cf.onlcarrcan,"111","0","CAN");
			  break;
		  case R.id.onlcarrtotal: /*CLICK EVENT FOR ONLINE CARRIER TOTAL*/
			  if(String.valueOf(cf.onlcarrtotal).equals("0"))
			  {
				  cf.ShowToast("Sorry, No records found for General Conditions and Hazards Carr. Order.",0);
			  }
			  else
			  {
				  cf.ShowToast("Total No. of Records : " + cf.onlcarrtotal, 1);
			  }
			  break;
		  case R.id.onlretassign: /*CLICK EVENT FOR ONLINE RETAIL -ASSIGN*/
			  cf.MoveTo_OnlineList("11",cf.onlretassign,"30","0","Assign");
			  break;
		  case R.id.onlretschedule: /*CLICK EVENT FOR ONLINE RETAIL -SCHEDULE*/
			  cf.MoveTo_OnlineList("11",cf.onlretschedule,"40","0","Schedule");
			  break;
		  case R.id.onlretcio: /*CLICK EVENT FOR ONLINE RETAIL -CIO*/
			  cf.MoveTo_OnlineList("11",cf.onlretcio,"40","41","CIO");
			  break;
		  case R.id.onlretuts: /*CLICK EVENT FOR ONLINE RETAIL -UTS*/
			  cf.MoveTo_OnlineList("11",cf.onlretuts,"110","111","UTS");
			  break;
		  case R.id.onlretcan: /*CLICK EVENT FOR ONLINE RETAIL -CAN*/
			  cf.MoveTo_OnlineList("11",cf.onlretcan,"111","0","CAN");
			  break;
		  case R.id.onlrettotal: /*CLICK EVENT FOR ONLINE RETAIL TOTAL*/
			  if(String.valueOf(cf.onlrettotal).equals("0"))
			  {
				  cf.ShowToast("Sorry, No records found for General Conditions and Hazards.",0);
			  }
			  else
			  {
				  cf.ShowToast("Total No. of Records : " + cf.onlrettotal, 1);
			  }
			  break;
		  }
	 }
	public void run() {
		// TODO Auto-generated method stub
			try {
				
				 cf.onlresult=cf.Calling_WS1(cf.Insp_id, "UpdateMobileDBCount_GenHazards");
				 retrieveonlinedata(cf.onlresult);
                 v=0;
                
			} catch (Exception e) {
               v=1;
			}
			 handler.sendEmptyMessage(0);
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			cf.pd.dismiss();
         if(v==0){
			if (cf.onlcarrassign != 0) {
				cf.btn_onlcarrassign.setText(String.valueOf(cf.onlcarrassign));

			} else {
				cf.btn_onlcarrassign.setText("0");
			}
			if (cf.onlcarrschedule != 0) {
				cf.btn_onlcarrschedule.setText(String.valueOf(cf.onlcarrschedule));

			} else {
				cf.btn_onlcarrschedule.setText("0");
			}
			if (cf.onlcarrcio != 0) {
				cf.btn_onlcarrcio.setText(String.valueOf(cf.onlcarrcio));

			} else {
				cf.btn_onlcarrcio.setText("0");
			}
			if (cf.onlcarruts != 0) {
				cf.btn_onlcarruts.setText(String.valueOf(cf.onlcarruts));

			} else {
				cf.btn_onlcarruts.setText("0");
			}
			if (cf.onlcarrcan != 0) {
				cf.btn_onlcarrcan.setText(String.valueOf(cf.onlcarrcan));

			} else {
				cf.btn_onlcarrcan.setText("0");
			}
			if (cf.onlcarrtotal != 0) {
				cf.btn_onlcarrtotal.setText(String.valueOf(cf.onlcarrtotal));

			} else {
				cf.btn_onlcarrtotal.setText("0");
			}
			if (cf.onlretassign != 0) {
				cf.btn_onlretassign.setText(String.valueOf(cf.onlretassign));

			} else {
				cf.btn_onlretassign.setText("0");
			}
			if (cf.onlretschedule != 0) {
				cf.btn_onlretschedule.setText(String.valueOf(cf.onlretschedule));

			} else {
				cf.btn_onlretschedule.setText("0");
			}
			if (cf.onlretcio != 0) {
				cf.btn_onlretcio.setText(String.valueOf(cf.onlretcio));

			} else {
				cf.btn_onlretcio.setText("0");
			}
			if (cf.onlretuts != 0) {
				cf.btn_onlretuts.setText(String.valueOf(cf.onlretuts));

			} else {
				cf.btn_onlretuts.setText("0");
			}
			if (cf.onlretcan != 0) {
				cf.btn_onlretcan.setText(String.valueOf(cf.onlretcan));

			} else {
				cf.btn_onlretcan.setText("0");
			}
			if (cf.onlrettotal != 0) {
				cf.btn_onlrettotal.setText(String.valueOf(cf.onlrettotal));

			} else {
				cf.btn_onlrettotal.setText("0");
			}
		 }
		 else if(v==1)
		 {
			 cf.ShowToast("There is a problem in the server. Please contact paperless admin.", 1);
		 }
		}
	};
	public void retrieveonlinedata(SoapObject onlresult)
	{
		int Cnt = onlresult.getPropertyCount();
		if (Cnt >= 1) {
			SoapObject obj = (SoapObject) onlresult;
			for (int i = 0; i < obj.getPropertyCount(); i++) {
				SoapObject obj1 = (SoapObject) obj.getProperty(i);
				if (!obj1.getProperty("inspectiontypeid").toString().equals("")) {
					if (12 == Integer.parseInt(obj1.getProperty(
							"inspectiontypeid").toString())) {
						cf.onlcarrassign = Integer.parseInt(obj1.getProperty("Assign")
								.toString());
						cf.onlcarrschedule = Integer.parseInt(obj1.getProperty("Sch")
								.toString());
						cf.onlcarrcio = Integer.parseInt(obj1.getProperty(
								"Comp_Ins_in_Online").toString());
						cf.onlcarruts = Integer.parseInt(obj1.getProperty("UTS")
								.toString());
						cf.onlcarrcan = Integer.parseInt(obj1.getProperty("Can")
								.toString());
						cf.onlcarrtotal = Integer.parseInt(obj1.getProperty("Total")
								.toString());
					}
					if (11 == Integer.parseInt(obj1.getProperty(
							"inspectiontypeid").toString())) {
						cf.onlretassign = Integer.parseInt(obj1.getProperty("Assign")
								.toString());
						cf.onlretschedule = Integer.parseInt(obj1.getProperty("Sch")
								.toString());
						cf.onlretcio = Integer.parseInt(obj1.getProperty(
								"Comp_Ins_in_Online").toString());
						cf.onlretuts = Integer.parseInt(obj1.getProperty("UTS")
								.toString());
						cf.onlretcan = Integer.parseInt(obj1.getProperty("Can")
								.toString());
						cf.onlrettotal = Integer.parseInt(obj1.getProperty("Total")
								.toString());
					}
				}
			}
		}
	}
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
			// replaces the default 'Back' button action
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				Intent loginpage = new Intent(Dashboard.this,HomeScreen.class);
				loginpage.putExtra("back", "exit");
				startActivity(loginpage);
				return true;
			}
			if (keyCode == KeyEvent.KEYCODE_MENU) {

			}
			return super.onKeyDown(keyCode, event);
		}

}
