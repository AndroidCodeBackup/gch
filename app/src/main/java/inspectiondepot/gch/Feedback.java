package inspectiondepot.gch;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Feedback extends Activity{
	private static final int SELECT_PICTURE = 0;
	private static final int SELECT_PDF = 1;
	private static final int CAPTURE_PICTURE_INTENT = 0;
	private ListView lv1;View v2;public int focus=0;
	private String lv_arr[]={"Android","iPhone","BlackBerry","AndroidPeople","J2ME", "Listview","ArrayAdapter","ListItem","Us","UK","India"};
	private static final String TAG = null;
	protected static final int visibility = 0;Bitmap rotated_b;
	ArrayList<String> listItems=new ArrayList<String>();
    private File[] imagelist;
    String[] pdflist=null;
	int o=0,documentothertextsupp=0,documentothertextoff=0,commentlength;
	Cursor curfeed1,curfeed2;
	TextWatcher watcher;
	ListAdapter adapter,adapter11;ArrayAdapter<String> adapter1;
	View v1;File images;
	ListView doclst,offlst;
	String[] items = {"gallery","pdf"};
	String[] items1 = {"pdf"};
	static String[] docnamearr;static String[] docidarr; static String[] offdocidarr;
	static String[] docpatharr;
	static String[] offnamearr;
	TableLayout tbl;
	static String[] offpatharr;
	String[] array_who,array_doc;String selected_id,offselected_id;
	Uri mCapturedImageURI;int ImgOrder =0;
	String doc_id,offdoc_id,strdoctitle,strdoctitle1,strdoctitleoff,strdoctitlesupp,homeid,strpresatins="",strfbcoment,strdocname,selectedImagePath="empty",selectedImagePath1,capturedImageFilePath,
	       docname,docpath,offname,offpath,InspectionType,status,inspectiontype,picname,strother="";
	RadioButton cusyrd,cusnrd,insyrd,insnrd,insntrd,infyrd,infnrd,infntrd;
	int isCSC=0,isInsavail=0,isManf=0,rws,t,c,docrws,offrws,rws1,rws2,flagstr,inspid,value,Count,feedrws1,feedrws2;
	EditText edfbcoment,offedittitle,docpathedit,offpathedit,othrtxt,othrtxtsupp,othrtxtoff; 
	android.text.format.DateFormat df;CharSequence cd,md;Cursor c11;
	TableLayout offtbl,tblsupp,tbloffice;
	LinearLayout doctbllst,offtbllst,linpar,lintyp;
	Button docbrws,offbrws,docupd,offupd;
	private static final String mbtblInspectionList = "Retail_inpgeneralinfo";
	Spinner s,s1,s2; TextView policyholderinfo;
	ScrollView pscr,cscr;
	int currnet_rotated=0;
	TableLayout exceedslimit1;
	TextView lintxt,CSEform,Whowaspresent,didhomeowner,didhomeownermanu,txtfeedbackcomments;
	
	CommonFunctions cf;
	
	public void onCreate(Bundle SavedInstanceState) {
		super.onCreate(SavedInstanceState);
		cf = new CommonFunctions(this);
		 /** Creating feedback table **/
	  
		cf.Create_Table(10);
		cf.Create_Table(11);
			        
		Bundle bunQ1extras1 = getIntent().getExtras();        
		  if (bunQ1extras1 != null) {
			cf.selectedhomeid= bunQ1extras1.getString("homeid");
			cf.onlinspectionid = bunQ1extras1.getString("InspectionType");
		    cf.onlstatus = bunQ1extras1.getString("status");
			
		  }

		  
		   setContentView(R.layout.feedbackdocument); 
		   /**Used for hide he key board when it clik out side**/

			cf.setupUI((ScrollView) findViewById(R.id.scr));

		/**Used for hide he key board when it clik out side**/
		   focus=1;
		   cf.getInspectorId();cf.getinspectiondate();
		   cf.getDeviceDimensions();
		   /** menu **/
		   System.out.println("no more issues 1");
		   LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
			  mainmenu_layout.setMinimumWidth(cf.wd);
		      mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 6, 0,cf));
		      TableRow tblrw = (TableRow)findViewById(R.id.row2);
		      tblrw.setMinimumHeight(cf.ht);
		      System.out.println("no more issues 2");   

	        
	       df = new android.text.format.DateFormat();
		   cd = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		   md = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		  
		   System.out.println("no more issues 3");
			cusyrd = (RadioButton)findViewById(R.id.customeryes);
			cusyrd.setOnClickListener(OnClickListener);
			cusnrd = (RadioButton)findViewById(R.id.customerno);
			cusnrd.setOnClickListener(OnClickListener);
			insyrd = (RadioButton)findViewById(R.id.insyes);
			insyrd.setOnClickListener(OnClickListener);
			insnrd = (RadioButton)findViewById(R.id.insno);
			insnrd.setOnClickListener(OnClickListener);
			insntrd = (RadioButton)findViewById(R.id.insnot);
			insntrd.setOnClickListener(OnClickListener);
			infyrd = (RadioButton)findViewById(R.id.infyes);
			infyrd.setOnClickListener(OnClickListener);
			infnrd = (RadioButton)findViewById(R.id.infno);
			infnrd.setOnClickListener(OnClickListener);
			infntrd = (RadioButton)findViewById(R.id.infnot);
			infntrd.setOnClickListener(OnClickListener);
			 s = (Spinner)findViewById(R.id.Spinner01);
			
			 ((TextView)findViewById(R.id.CSEform)).setText(Html.fromHtml(cf.redcolor+" Customer Service Evaluation Form Completed and Signed by Homeowner"));
			 ((TextView)findViewById(R.id.Whowaspresent)).setText(Html.fromHtml(cf.redcolor+" Who Was Present at Inspection"));
			 ((TextView)findViewById(R.id.didhomeowner)).setText(Html.fromHtml(cf.redcolor+" Owner / Representative Satisfied with Inspection "));
			 ((TextView)findViewById(R.id.didhomeownermanu)).setText(Html.fromHtml(cf.redcolor+" Customer Service Evaluation Form Completed and Signed by Owner / Representative "));
			 
			 
			 txtfeedbackcomments=(TextView)findViewById(R.id.txtfeedbackcomments);System.out.println("otssser");
			 txtfeedbackcomments.setText(Html.fromHtml(cf.redcolor + " FeedBack Comments"));
			 
			 othrtxt=(EditText)findViewById(R.id.other);System.out.println("oter");
			 othrtxt.setOnTouchListener(new Touch_Listener(1));
			 othrtxtsupp=(EditText)findViewById(R.id.othersupp);System.out.println("otessrsupp");
			 othrtxtsupp.setOnTouchListener(new Touch_Listener(2));
			 othrtxtoff=(EditText)findViewById(R.id.otheroff);System.out.println("oterosssssssfff");
			 othrtxtoff.setOnTouchListener(new Touch_Listener(3));

			
			 s1 = (Spinner)findViewById(R.id.Spinner02);
			 s2 = (Spinner)findViewById(R.id.Spinner03);System.out.println("Spinner03");
			
				  linpar = (LinearLayout) findViewById(R.id.SQ_ED_type1_parrant);
				  lintyp = (LinearLayout) findViewById(R.id.SQ_ED_type1);
				  lintxt = (TextView) findViewById(R.id.SQ_TV_type1);
			edfbcoment = (EditText)findViewById(R.id.comment);
			edfbcoment.setOnTouchListener(new Touch_Listener(4));
			edfbcoment.addTextChangedListener(new FB_textwatcher(1));
			
		
			docbrws = (Button)findViewById(R.id.browsetxt);
			offbrws = (Button)findViewById(R.id.offbrowsetxt);
			docpathedit = (EditText)findViewById(R.id.pathtxt);
			offpathedit = (EditText)findViewById(R.id.offpathtxt);
			docupd = (Button)findViewById(R.id.updtxt);
			offupd = (Button)findViewById(R.id.offupdtxt);
			doclst = (ListView)findViewById(R.id.ListView01);
			offlst = (ListView)findViewById(R.id.ListView02);System.out.println("listview2");
			doctbllst= (LinearLayout)findViewById(R.id.uploadeddoc1);
			offtbllst= (LinearLayout)findViewById(R.id.uploadeddoc2);

		    array_who=new String[4];
		    array_who[0]="Owner";
		    array_who[1]="Representative";
		    array_who[2]="Agent";
		    array_who[3]="Other";
		    
		    array_doc=new String[12];
		    array_doc[0]="--Select--";
		    array_doc[1]="Acknowledgement Form";
		    array_doc[2]="CSE Form";
		    array_doc[3]="OIR 1802 Form";
		    array_doc[4]="Paper Signup Sheet";
		    array_doc[5]="Other Information";
		    array_doc[6]="Roof Permit ";
		    array_doc[7]="Sketch";
		    array_doc[8]="Building Permit";
		    array_doc[9]="Property Appraisal Information";
		    array_doc[10]="Field Inspection Report";
		    array_doc[11]="Field Notes";
		    
			ArrayAdapter adapter2 = new ArrayAdapter(this,android.R.layout.simple_spinner_item, array_who);
			adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

	        s.setAdapter(adapter2);
	        
	        s.setOnItemSelectedListener(new MyOnItemSelectedListenerdata());
	        ArrayAdapter adapter1 = new ArrayAdapter(this,android.R.layout.simple_spinner_item, array_doc);
	        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	        s1.setAdapter(adapter1);
	        s1.setOnItemSelectedListener(new MyOnItemSelectedListenerdata1());
	        s2.setAdapter(adapter1);
	        s2.setOnItemSelectedListener(new MyOnItemSelectedListenerdata2());
			
			 try {
				   Cursor	c1 = cf.gch_db.rawQuery("SELECT * FROM "+ cf.FeedBackInfoTable	+ " WHERE GCH_FI_Srid='"+cf.selectedhomeid+"'",null);
	               rws = c1.getCount();
	               
	               if(rws==0)
	  			 {
	  				 
	  			 }
	  			 else
	  			 {
	  				     int Column1=c1.getColumnIndex("GCH_FI_IsCusServiceCompltd"); 
	    		     	 int Column2=c1.getColumnIndex("GCH_FI_PresentatInspection"); 
	    		     	 int Column3=c1.getColumnIndex("GCH_FI_IsInspectionPaperAvbl");
	    		     	 int Column4=c1.getColumnIndex("GCH_FI_IsManufacturerInfo");
	    		     	 int Column5=c1.getColumnIndex("GCH_FI_FeedbackComments");
	    		     	 int Column6=c1.getColumnIndex("GCH_FI_othertxt");
	    		     	 c1.moveToFirst();
	    		     	 if(c1!=null)
	    		     	 {
	    		     		do{
	    		     			 String csc=c1.getString(Column1); 
	    		     			 String pati=c1.getString(Column2);
	    		     			 String ipa=c1.getString(Column3);
	    		     			 String imi=c1.getString(Column4);
	    		     			 String fc=c1.getString(Column5);
	    		     			 if(csc.equals("1"))
	    						 {
	    							 cusyrd.setChecked(true);cusnrd.setChecked(false);isCSC=1;
	    						 }
	    						 else
	    						 {
	    							 cusyrd.setChecked(false);cusnrd.setChecked(true);isCSC=2;
	    						 }
	    		     			 int spinnerPosition = adapter2.getPosition(pati);
	    				         s.setSelection(spinnerPosition);
	    		     			 if(ipa.equals("1"))
	    						 {
	    							 insyrd.setChecked(true);insnrd.setChecked(false);insntrd.setChecked(false);
	    							 isInsavail=1;
	    						 }
	    						 else if(ipa.equals("2"))
	    						 {
	    							 insyrd.setChecked(false);insnrd.setChecked(true);insntrd.setChecked(false);
	    							 isInsavail=2;
	    						 }
	    						 else if(ipa.equals("3"))
	    						 {
	    							 insyrd.setChecked(false);insnrd.setChecked(false);insntrd.setChecked(true);
	    							 isInsavail=3;
	    						 }
	    		     			 if(imi.equals("1"))
	   						     {
	    		     				infyrd.setChecked(true);infnrd.setChecked(false);infntrd.setChecked(false);
	    		     				isManf=1;
	   						     }
	    		     			 else if(imi.equals("2"))
	   						     {
	     		     				infyrd.setChecked(false);infnrd.setChecked(true);infntrd.setChecked(false);
	     		     				isManf=2;
	    						 }
	    		     			 else if(imi.equals("3"))
	  						     {
	    		     				infyrd.setChecked(false);infnrd.setChecked(false);infntrd.setChecked(true);
	    		     				isManf=3;
	   						     }
	    		     			cf.showing_limit(cf.decode(fc),linpar,lintyp,lintxt,"1498");	
	    		     			edfbcoment.setMaxWidth(cf.wd-150);
	    		     			edfbcoment.setText(cf.decode(fc));
	    		     			
	    		     			
	    		     			if(c1.getString(c1.getColumnIndex("GCH_FI_othertxt")).equals(""))
	    		     			{
	    		     				
	    		     			}
	    		     			else
	    		     			{
	    		     				othrtxt.setText(cf.decode(c1.getString(c1.getColumnIndex("GCH_FI_othertxt"))));
	    		     			}
	    		     		}while(c1.moveToNext());
	    		     	 }c1.close(); 
	  			 }
			 }
			 catch(Exception e)
			 {
				 Log.i(TAG,"error= "+e.getMessage());
			 }
			 show_list();
			 showoffice_list();
			 
			 offlst.setOnItemClickListener(new OnItemClickListener() {
				  public void onItemClick(AdapterView<?> a, View v, int position, long id) {
					
				        final String offselarr = offnamearr[position];
				        offselected_id=offdocidarr[position];
				     
				        final Dialog dialog = new Dialog(Feedback.this);
			            dialog.getWindow().setContentView(R.layout.maindialog);
			            dialog.setTitle(offpatharr[position]);  
			            dialog.setCancelable(true);
			            final ImageView img = (ImageView) dialog.findViewById(R.id.ImageView01);
			            EditText ed1 = (EditText) dialog.findViewById(R.id.TextView01);
			            ed1.setVisibility(v1.GONE);
			            img.setVisibility(v1.GONE);
			            Button button_close = (Button) dialog.findViewById(R.id.Button01);
			    		button_close.setText("Close");//button_close.setVisibility(v2.GONE);
			            Button button_d = (Button) dialog.findViewById(R.id.Button03);
			    		button_d.setText("Delete");
			    		button_d.setVisibility(v2.VISIBLE);
			    		final Button button_view = (Button) dialog.findViewById(R.id.Button02);
			    		button_view.setText("View");
			    		button_view.setVisibility(v2.VISIBLE);
			    		
			    		final Button button_saveimage= (Button) dialog.findViewById(R.id.Button05);
			    		button_saveimage.setVisibility(v2.GONE);
			    		final LinearLayout linrotimage = (LinearLayout) dialog.findViewById(R.id.linrotation);
			    		linrotimage.setVisibility(v2.GONE);
			    		final Button rotateright= (Button) dialog.findViewById(R.id.rotateright);
			    		rotateright.setVisibility(v2.GONE);
			    		final Button rotateleft= (Button) dialog.findViewById(R.id.rotateleft);
			    		rotateleft.setVisibility(v2.GONE);
			    		
			    		button_d.setOnClickListener(new OnClickListener() {
				            public void onClick(View v) {
				            
				            AlertDialog.Builder builder = new AlertDialog.Builder(Feedback.this);
				   			builder.setMessage("Are you sure, Do you want to delete?")
			   			       .setCancelable(false)
			   			       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			   			           public void onClick(DialogInterface dialog, int id) {
			   			        	   cf.gch_db.execSQL("Delete From " +cf. FeedBackDocumentTable + "  WHERE GCH_FI_D_SRID ='" + cf.selectedhomeid + "' and GCH_FI_DocumentId='"+offselected_id+"' and GCH_FI_D_IsOfficeUse=1");
			   			        	cf.ShowToast("Document has been deleted sucessfully.",1);
			   			        	
			   					   
			   					    showoffice_list();
			   			           }
			   			       })
			   			       .setNegativeButton("No", new DialogInterface.OnClickListener() {
			   			           public void onClick(DialogInterface dialog, int id) {
			   			                dialog.cancel();
			   			           }
			   			       });
			   			 builder.show();
			            
			   			 dialog.cancel();
		            }
	    		           	
				  	 });System.out.println("del= ");
			    		button_view.setOnClickListener(new OnClickListener() {
				            public void onClick(View v) {
				            	currnet_rotated=0;
				            	
				            	if(offselarr.equals(""))
				            	{
				            		img.setVisibility(v2.VISIBLE);
				            	}
				            	else if(offselarr.endsWith(".pdf"))
							     {
				            		img.setVisibility(v2.GONE);
				            		String tempstr ;
				            		if(offselarr.contains("file://"))
				    		    	{
				    		    		 tempstr = offselarr.replace("file://","");		    		
				    		    	}
				            		else
				            		{
				            			tempstr = offselarr;
				            		}
				            		File file = new File(tempstr);
								 
					                 if (file.exists()) {
					                	Uri path = Uri.fromFile(file);
					                    Intent intent = new Intent(Intent.ACTION_VIEW);
					                    intent.setDataAndType(path, "application/pdf");
					                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					 
					                    try {
					                        startActivity(intent);
					                    } 
					                    catch (ActivityNotFoundException e) {
					                    	cf.ShowToast("No Application available to view PDF.",1);
					                       
					                    }
					               
					                }
							     }
				            	else
				            	{
				            		Bitmap bitmap2=ShrinkBitmap(offselarr,400,400);
				            		BitmapDrawable bmd2 = new BitmapDrawable(bitmap2);
				            		img.setImageDrawable(bmd2);
			    					img.setVisibility(v2.VISIBLE);
			    					rotateright.setVisibility(v2.VISIBLE);
			    					rotateleft.setVisibility(v2.VISIBLE);
			    					button_view.setVisibility(v2.GONE);
						            button_saveimage.setVisibility(v2.VISIBLE);
						            linrotimage.setVisibility(View.VISIBLE);
				            	}
				            	
				            }
			    		});
			    		rotateleft.setOnClickListener(new OnClickListener() {  			
			    			public void onClick(View v) {

			    				// TODO Auto-generated method stub
			    			
			    				System.gc();
			    				currnet_rotated-=90;
			    				if(currnet_rotated<0)
			    				{
			    					currnet_rotated=270;
			    				}

			    				
			    				Bitmap myImg;
			    				try {
			    					myImg = BitmapFactory.decodeStream(new FileInputStream(offselarr));
			    					Matrix matrix =new Matrix();
			    					matrix.reset();
			    					//matrix.setRotate(currnet_rotated);
			    					System.out.println("Ther is no more issues top ");
			    					matrix.postRotate(currnet_rotated);
			    					
			    					 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
			    					        matrix, true);
			    					 
			    					 img.setImageBitmap(rotated_b);

			    				} catch (FileNotFoundException e) {
			    					// TODO Auto-generated catch block
			    					e.printStackTrace();
			    				}
			    				catch (Exception e) {
			    					
			    				}
			    				catch (OutOfMemoryError e) {
			    					System.out.println("comes in to out ot mem exception");
			    					System.gc();
			    					try {
			    						myImg=null;
			    						System.gc();
			    						Matrix matrix =new Matrix();
			    						matrix.reset();
			    						//matrix.setRotate(currnet_rotated);
			    						matrix.postRotate(currnet_rotated);
			    						myImg= cf.ShrinkBitmap(offselarr, 800, 800);
			    						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
			    						System.gc();
			    						img.setImageBitmap(rotated_b); 

			    					} catch (Exception e1) {
			    						// TODO Auto-generated catch block
			    						e1.printStackTrace();
			    					}
			    					 catch (OutOfMemoryError e1) {
			    							// TODO Auto-generated catch block
			    						 cf.ShowToast("You can not rotate this image. Image size is too large.",1);
			    					}
			    				}

			    			
			    			}
			    		});
			    		rotateright.setOnClickListener(new OnClickListener() {
			    		    public void onClick(View v) {
			    		    	System.out.println("problme in nothig");
			    		    	currnet_rotated+=90;
			    				if(currnet_rotated>=360)
			    				{
			    					currnet_rotated=0;
			    				}
			    				System.out.println("selarr "+offselarr);
			    				Bitmap myImg;
			    				try {
			    					myImg = BitmapFactory.decodeStream(new FileInputStream(offselarr));
			    					Matrix matrix =new Matrix();
			    					matrix.reset();System.out.println("Ther is no more issues top ");
			    					//matrix.setRotate(currnet_rotated);
			    					System.out.println("Ther is no more issues top ");
			    					matrix.postRotate(currnet_rotated);
			    					System.out.println("postRotate "+currnet_rotated);
			    					 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
			    					        matrix, true);
			    					 System.out.println("rotate"+rotated_b); 
			    					 img.setImageBitmap(rotated_b); System.out.println("rotatepic"); 

			    				} catch (FileNotFoundException e) {
			    					System.out.println("FileNotFoundException "+e.getMessage()); 
			    					// TODO Auto-generated catch block
			    					e.printStackTrace();
			    				}
			    				catch(Exception e){}
			    				catch (OutOfMemoryError e) {
			    					System.out.println("comes in to out ot mem exception");
			    					System.gc();
			    					try {
			    						myImg=null;
			    						System.gc();
			    						Matrix matrix =new Matrix();
			    						matrix.reset();
			    						//matrix.setRotate(currnet_rotated);
			    						matrix.postRotate(currnet_rotated);
			    						myImg= cf.ShrinkBitmap(offselarr, 800, 800);
			    						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
			    						System.gc();
			    						img.setImageBitmap(rotated_b); 

			    					} catch (Exception e1) {
			    						// TODO Auto-generated catch block
			    						e1.printStackTrace();
			    					}
			    					 catch (OutOfMemoryError e1) {
			    							// TODO Auto-generated catch block
			    						 cf.ShowToast("You can not rotate this image. Image size is too large.",1);
			    					}
			    				}

			    		    }
			    		});
			    		
			    		button_saveimage.setOnClickListener(new OnClickListener() {
							
							public void onClick(View v) {
								// TODO Auto-generated method stub
								System.out.println("selarr "+offselarr);
								
								System.out.println("currnet_rotated "+currnet_rotated);
								
								if(currnet_rotated>0)
								{ 

									try
									{
										/**Create the new image with the rotation **/
										System.out.println("rotated_b "+rotated_b);
										String current=MediaStore.Images.Media.insertImage(getContentResolver(), rotated_b, "My bitmap", "My rotated bitmap");
										 ContentValues values = new ContentValues();
										  values.put(MediaStore.Images.Media.ORIENTATION, 0);
										  Feedback.this.getContentResolver().update(Uri.parse(current), values, MediaStore.Images.Media.DATA+ "=?", new String[] { current } );
										
										System.out.println("current "+current);
										if(current!=null)
										{
										String path=getPath(Uri.parse(current));
										File fout = new File(offselarr);
										fout.delete();
										/** delete the selected image **/
										File fin = new File(path);
										/** move the newly created image in the slected image pathe ***/
										fin.renameTo(new File(offselarr));
										cf.ShowToast("Saved successfully.",1);dialog.cancel();
										show_list();
										
										
									}
									} catch(Exception e)
									{
										System.out.println("Error occure while rotate the image "+e.getMessage());
									}
									
								}
								else
								{
									dialog.cancel();
									show_list();
								}
								
							}
						});
			    		button_close.setOnClickListener(new OnClickListener() {
				            public void onClick(View v) {
				            	dialog.cancel();
				            }
			    		});
			    		dialog.show();
				     
				  }
		   });
			 adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_row, items) {
			        
			        ViewHolder holder;
			        Drawable icon;
			 
			        class ViewHolder {
			                ImageView icon;
			                TextView title;
			        }
			 
			        public View getView(int position, View convertView,ViewGroup parent) {
			                final LayoutInflater inflater = (LayoutInflater) getApplicationContext()
			                                .getSystemService(
			                                                Context.LAYOUT_INFLATER_SERVICE);
			 
			                if (convertView == null) {
			                        convertView = inflater.inflate(
			                                        R.layout.list_row, null);
			 
			                        holder = new ViewHolder();
			                        holder.icon = (ImageView) convertView
			                                        .findViewById(R.id.icon);
			                        holder.title = (TextView) convertView
			                                        .findViewById(R.id.title);
			                        convertView.setTag(holder);
			                } else {
			                        // view already defined, retrieve view holder
			                        holder = (ViewHolder) convertView.getTag();
			                }              
			  
			                holder.title.setText(items[position]);
			                if(items[position].equals("gallery"))
			                {
			                	 Drawable tile = getResources().getDrawable(R.drawable.gallery);
			                	 holder.icon.setImageDrawable(tile);
			                }
			                else  if(items[position].equals("camera"))
			                {
			                	 Drawable tile1 = getResources().getDrawable(R.drawable.iconphoto);
			                	 holder.icon.setImageDrawable(tile1);
			                }
			       
	                else  if(items[position].equals("pdf"))
	                {
	                	 Drawable tile2 = getResources().getDrawable(R.drawable.pdficon);
	                	 holder.icon.setImageDrawable(tile2);
	                }
			                
			 
			                return convertView;
			        }
			};
			 adapter11 = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_row, items) {
			        
			        ViewHolder holder;
			        Drawable icon;
			 
			        class ViewHolder {
			                ImageView icon;
			                TextView title;
			        }
			 
			        public View getView(int position, View convertView,ViewGroup parent) {
			                final LayoutInflater inflater = (LayoutInflater) getApplicationContext()
			                                .getSystemService(
			                                                Context.LAYOUT_INFLATER_SERVICE);
			 
			                if (convertView == null) {
			                        convertView = inflater.inflate(
			                                        R.layout.list_row, null);
			 
			                        holder = new ViewHolder();
			                        holder.icon = (ImageView) convertView
			                                        .findViewById(R.id.icon);
			                        holder.title = (TextView) convertView
			                                        .findViewById(R.id.title);
			                        convertView.setTag(holder);
			                } else {
			                        // view already defined, retrieve view holder
			                        holder = (ViewHolder) convertView.getTag();
			                }              
			  
			                holder.title.setText(items[position]);
			                if(items[position].equals("pdf"))
	                {
	                	 Drawable tile2 = getResources().getDrawable(R.drawable.pdficon);
	                	 holder.icon.setImageDrawable(tile2);
	                }
			                
			 
			                return convertView;
			        }
			};
	}
	class FB_textwatcher implements TextWatcher
	{
         public int type;
         FB_textwatcher(int type)
		{
			this.type=type;
		}
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			if(this.type==1)
			{
				cf.showing_limit(s.toString(),linpar,lintyp,lintxt,"1500");
			}
				
		}

		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}

		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			
		}
		
	}		 
	class Touch_Listener implements OnTouchListener
	{
		   public int type;
		   Touch_Listener(int type)
			{
				this.type=type;
				
			}
		  public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
		    	if(this.type==1)
				{
		    		cf.setFocus(othrtxt);
				}
		    	else if(this.type==2)
		    	{
		    		cf.setFocus(othrtxtsupp);
		    	}
		    	else if(this.type==3)
		    	{
		    		cf.setFocus(othrtxtoff);
		    	}  	
		    	else if(this.type==4)
		    	{
		    		cf.setFocus(edfbcoment);
		    	}  	
				return false;
			}
		 
	}
	RadioButton.OnClickListener OnClickListener =  new RadioButton.OnClickListener()
	{

	  public void onClick(View v) {
		    switch(v.getId())
			 {
		    case R.id.customeryes:
		    	isCSC=1;cusyrd.setChecked(true);cusnrd.setChecked(false);
		    	break;
		    case R.id.customerno:
		    	isCSC=2;cusyrd.setChecked(false);cusnrd.setChecked(true);
		    	break;
		    case R.id.insyes:
		    	isInsavail=1;insyrd.setChecked(true);insnrd.setChecked(false);insntrd.setChecked(false);
		    	break;
		    case R.id.insno:
		    	isInsavail=2;insyrd.setChecked(false);insnrd.setChecked(true);insntrd.setChecked(false);
		    	break;
		    case R.id.insnot:
		    	isInsavail=3;insyrd.setChecked(false);insnrd.setChecked(false);insntrd.setChecked(true);
		    	break;
		    case R.id.infyes:
		    	isManf=1;infyrd.setChecked(true);infnrd.setChecked(false);infntrd.setChecked(false);
		    	break;
		    case R.id.infno:
		    	isManf=2;infyrd.setChecked(false);infnrd.setChecked(true);infntrd.setChecked(false);
		    	break;
		    case R.id.infnot:
		    	isManf=3;infyrd.setChecked(false);infnrd.setChecked(false);infntrd.setChecked(true);
		    	break;
		  
			 }
	  }
	  };
	private int ii;
	private String[] pdfsample;
	 

	public void clicker(View v) {
		switch (v.getId()) {
		
		 case R.id.save:
			 System.out.println("Save inside");
			 try
			 {
				 curfeed1 = cf.gch_db.rawQuery("SELECT * FROM "+ cf.FeedBackDocumentTable	+ " WHERE GCH_FI_D_SRID='"+cf.selectedhomeid+"' and GCH_FI_D_IsOfficeUse=0",null);
				 feedrws1 = curfeed1.getCount();
				 
				 curfeed2 = cf.gch_db.rawQuery("SELECT * FROM "+ cf.FeedBackDocumentTable	+ " WHERE GCH_FI_D_SRID='"+cf.selectedhomeid+"' and GCH_FI_D_IsOfficeUse=1",null);
				 feedrws2 = curfeed2.getCount();
			 }
			 catch(Exception e)
			 {
				 System.out.println(" Error  save = "+e.getMessage());	 
			 }

		    	strfbcoment=edfbcoment.getText().toString();
		    	if(o==1)
		    	{
		    		strother = othrtxt.getText().toString();
		    	}
		    	else
		    	{
		    		strother="";
		    	}
		    	/*if(isCSC==0)
		    	{
		    		cf.ShowToast("Please select Customer Service Evaluation form Options in Feedback Section.",1);
                    
		    		
		    	}
		    	else*/ if(isInsavail==0)
		    	{
		    		cf.ShowToast("Please select Owner / Representative Satisfied with Inspection .",1);
		    		
		    	}
		    	else if(isManf==0)
		    	{
		    		cf.ShowToast("Please select Customer Service Evaluation Form Completed and Signed by Owner / Representative .",1);
		    		
		    	}
		    	else if(strfbcoment.trim().equals(""))
		    	{
		    		cf.ShowToast("Please enter Feedback Comments.",1);		    		
		    	}
		    	else if(feedrws1==0)
		    	{
		    		cf.ShowToast("Please select Supplemental Documents.",1);
		    	}
		    	else if(strother.trim().equals("") && (o==1))
		    	{
		    		cf.ShowToast("Please enter the text for Other.",1);
		    	}
		    	else
		    	{
		    				if(rws==0)
				    		{
				    			try
				    			{
				    			cf.gch_db.execSQL("INSERT INTO "
										+  cf.FeedBackInfoTable
										+ " (GCH_FI_Srid,GCH_FI_IsCusServiceCompltd,GCH_FI_PresentatInspection,GCH_FI_IsInspectionPaperAvbl,GCH_FI_IsManufacturerInfo,GCH_FI_FeedbackComments,GCH_FI_CreatedOn,GCH_FI_othertxt)"
										+ " VALUES ('" + cf.selectedhomeid + "','" + isCSC+"','"+strpresatins+"','"+isInsavail + "','"
										+ isManf + "','" + cf.encode(strfbcoment)+"','"+cd+"','"+cf.encode(strother)+"')");
				    			 
				    					
				    		    		cf.ShowToast("Feed Back Saved successfully.",1);
				    		    		 //cf.netalert("MAP");
				    		    		Intent myintent=new Intent(Feedback.this,Maps.class);
				    		    		  cf.putExtras(myintent);
				    				 		 startActivity(myintent);
				    				}
				    				catch(Exception e)
				    				{
				    					cf.ShowToast("There is a problem in saving your data due to invalid character.",1);
				    				}
				    		}
				    		else
				    		{
				    			try
				    			{
				    			 cf.gch_db.execSQL("UPDATE " +  cf.FeedBackInfoTable + " SET GCH_FI_IsCusServiceCompltd='"+ isCSC + "',GCH_FI_PresentatInspection='"+ strpresatins+"',GCH_FI_othertxt='"+cf.encode(strother)+"',GCH_FI_IsInspectionPaperAvbl='"+isInsavail+"',GCH_FI_IsManufacturerInfo='"+isManf+"',GCH_FI_FeedbackComments='"+cf.encode(strfbcoment)+"',GCH_FI_CreatedOn='"+cd+"' WHERE GCH_FI_Srid ='" + cf.selectedhomeid + "'");
				    			 flagstr=0;
				    			 cf.ShowToast("Feed back Updated successfully.",1);
				    			 //cf.netalert("MAP");
				    			 Intent myintent=new Intent(Feedback.this,Maps.class);
		    		    		  cf.putExtras(myintent);
		    				 		 startActivity(myintent);
						 			
				    		}
		    				catch(Exception e)
		    				{
		    					cf.ShowToast("There is a problem in saving your data due to invalid character.",1);
		    				}
				    			
				        	}
		    		
		    		
		    	}
		    	break;
		    case R.id.hme:
		    	Intent homepage = new Intent(Feedback.this,Accessbymainapplication.class);
				
				startActivity(homepage);
		    	break;
		    case R.id.browsetxt:
		    	c=1;
		    	System.out.println("the value of the S2"+s1.getSelectedItem().toString());
		    	
		    	if(!"--Select--".equals(s1.getSelectedItem().toString()))
    			{
    		        show();
    			}
		    	else
		    	{
		    		cf.ShowToast("Please select the Document Title.",1);
		    		
		    	}
		    	break;
		    case R.id.offbrowsetxt:
		    	c=2;selectedImagePath="";
		    	if(!"--Select--".equals(s2.getSelectedItem().toString()))
    			{
    		        AlertDialog.Builder builder = new AlertDialog.Builder(Feedback.this);
		            builder.setTitle("Pick an option");
		            builder.setAdapter(adapter,
		                            new DialogInterface.OnClickListener() {
		                                    private String[] pdfname;

											public void onClick(DialogInterface dialog,
		                                                    int item) {
		                                    	 if(items[item].equals("gallery"))
		                                         {
		                                         	t=0;pickfromgallery();
		                                         }
		                                    	 else if(items[item].equals("pdf"))
		                                            {
		                                           
		                                            	final Dialog dialog1 = new Dialog(Feedback.this);
		                                                dialog1.setContentView(R.layout.dialog);
		                                                dialog1.setTitle(Html.fromHtml("<h2><font  color=#bddb00>Select PDF to upload  </font></h2>"));
		                                                lv1=(ListView)dialog1.findViewById(R.id.ListView01);
		                                                images = Environment.getExternalStorageDirectory(); 
		                                                ii=0;
		                                                //pdfsample=new String[100];
		                                                walkdir(images);
		                                                pdflist= new String[ii];
		                                                 pdfname=new String[ii];
		                                              if(ii>=1)
		                                              {
		                                                for(int i=0;i<ii;i++)
		                                                {
		                                              
		                                                	
		                                                	pdflist[i]=pdfsample[i];	
		                                                	
		                                                	 String[] bits = pdfsample[i].split("/");
		                                                	 pdfname[i] = bits[bits.length - 1];
		                                                }
		                                              	                                             
			                                                lv1.setAdapter(new ArrayAdapter<String>(Feedback.this,android.R.layout.simple_list_item_1 ,pdfname));
			                                                lv1.setOnItemClickListener(new OnItemClickListener() {
			                                                	public void onItemClick(AdapterView<?> a, View v, int position, long id) {
			                                                		PackageManager packageManager = getPackageManager(); 
				                                                    Intent testIntent = new Intent(Intent.ACTION_VIEW); 
				                                                    testIntent.setType("application/pdf"); 
				                                                    List list = packageManager.queryIntentActivities(testIntent, PackageManager.MATCH_DEFAULT_ONLY);
				                                                    if (list.size() > 0 ) {  
				                                                        Intent intent = new Intent();
				                                                        intent.setAction(Intent.ACTION_VIEW);
				                                                        Uri uri = Uri.fromFile(new File(pdfsample[(int) id]).getAbsoluteFile());
				                                                        selectedImagePath=cf.decode(uri.toString());
				                                                         String[] bits = selectedImagePath.split("/");
				                           							     picname = bits[bits.length - 1];
			                                                		
			                                                		AlertDialog.Builder builder = new AlertDialog.Builder(Feedback.this);
			                                            			builder.setMessage("What would you like to do?")
			                                            			       .setCancelable(false)
			                                            			       .setPositiveButton("Preview", new DialogInterface.OnClickListener() {
			                                            			           public void onClick(DialogInterface dialog, int id) {
			                                            			        	   String suboffstring = selectedImagePath.substring(11);
			                                            			        	  System.out.println("suboff"+suboffstring);
			          		                     		    					   File file = null;
																				file = new File(suboffstring);
																				    if (file.exists()) {
			          		                     				                	Uri path = Uri.fromFile(file);
			          		                     				                    Intent intent1 = new Intent(Intent.ACTION_VIEW);
			          		                     				                    intent1.setDataAndType(path, "application/pdf");
			          		                     				                    intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			          		                     				                   
			          		                     				                    try {
			          		                     				                        startActivity(intent1);
			          		                     				                    } 
			          		                     				                    catch (ActivityNotFoundException e) {
			          		                     				                    	cf.ShowToast("No Application available to view PDF.",1);
			          		                     				                      
			          		                     				                    }
			          		                     				                 if(c==1)
				       		                                        			    {
				       		                                        			    	docpathedit.setText(selectedImagePath);
				       		                                        				    docbrws.setVisibility(v1.GONE);
				       		                                        				    docupd.setVisibility(visibility);
				       		                                        			    }
				       		                                        			    else if(c==2)
				       		                                        			    {
				       		                                        			    	offpathedit.setText(selectedImagePath);
				       		                                        				    offbrws.setVisibility(v1.GONE);
				       		                                        				    offupd.setVisibility(visibility);
				       		                                        			    }
			          		                     				                }
			                                            			           }
			                                            			       })
			                                            			       .setNeutralButton("Select", new DialogInterface.OnClickListener() {
                                    			           public void onClick(DialogInterface dialog, int id) {
                                    			        	   if(c==1 && !selectedImagePath.equals(""))
                                                              	 
                                               			    {
                                               			    	docpathedit.setText(selectedImagePath);
                                               				    docbrws.setVisibility(v1.GONE);
                                               				    docupd.setVisibility(visibility);
                                               			    }
                                               			    else if(c==2 && !selectedImagePath.equals(""))
                                               			    {
                                               			    	offpathedit.setText(selectedImagePath);
                                               				    offbrws.setVisibility(v1.GONE);
                                               				    offupd.setVisibility(visibility);
                                               			    }
                                           				    System.out.println("path"+selectedImagePath);
                                    			           }
                                    			       })
			                                            			       .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			                                            			           public void onClick(DialogInterface dialog, int id) {
			                                            			        	   selectedImagePath="";
			                                            			           }
		                                            			       });
		                                            			 builder.show();
		                                                	   dialog1.cancel();
		                                                     
		                                                    }
		                                                   
		                                                	}
		                                                
		                                                	});                                           
		                                                  
		                                                dialog1.show();
		                                            }// end the valdation of the pdf count 
			                                         else
			                                             {
			                                        	 cf.ShowToast("Please check whether you have a PDF in your Device.",1);
			                                              }
		                                            }
		                                            dialog.dismiss();
		                                    }
		                            });
		            AlertDialog alert = builder.create();
		            alert.show();
    			}
		    	else
		    	{
		    		cf.ShowToast("Please select the Document Title.",1);
		    		
		    	}
		    	break;
		    case R.id.updtxt:
		    	selectedImagePath=docpathedit.getText().toString().trim();
		    	if(selectedImagePath.contains("file://"))
		    	{
		    		selectedImagePath = selectedImagePath.replace("file://","");		    		
		    	}
		    	boolean bu=cf.common(selectedImagePath);
		    	if(bu)
				{
		    		if((documentothertextsupp==1) && (othrtxtsupp.length()!=0))
			      	{
		    			strdoctitle1 = othrtxtsupp.getText().toString();
		    		}
				    else if((documentothertextoff==1)  && (othrtxtoff.length()!=0))
				    {
				    	strdoctitle1 = othrtxtoff.getText().toString();				    	
				    }
			        else
				    {
			        	strdoctitle1=strdoctitle;
				    }
		    		flagstr=0;
			      	ImgOrder = getImageOrder(cf.selectedhomeid);
			      	System.out.println("sup= "+ImgOrder);
			      	if(othrtxtsupp.getText().toString().trim().equals("") && (documentothertextsupp==1))
		    		{
			      		cf.ShowToast("Please enter the other text information.",1);
		    			othrtxtsupp.setVisibility(v1.VISIBLE);	    			 
		    		}
			      	
			      	else
			      	{
				      	 try
			    			{
				      		 
				      		 
				      		 
				      		
				      		 if(!s1.getSelectedItem().toString().equals("--Select--"))
				      		 {
				      			
				      			 
						      	      cf.gch_db.execSQL("INSERT INTO "
				        					+  cf.FeedBackDocumentTable
				        					+ " (GCH_FI_D_SRID,GCH_FI_D_DocumentTitle,GCH_FI_D_FileName,GCH_FI_D_Nameext,GCH_FI_D_ImageOrder,GCH_FI_D_IsOfficeUse,GCH_FI_D_CreatedOn,GCH_FI_D_ModifiedDate)"
				        					+ " VALUES ('" + cf.selectedhomeid + "','"+cf.encode((s1.getSelectedItem().toString().equals("Other Information"))? othrtxtsupp.getText().toString():s1.getSelectedItem().toString().trim())+"','"+cf.encode(selectedImagePath)+"','"+cf.encode(picname)+"','"+ImgOrder+"','"+flagstr+ "','"+ cd+"','"+md+"')");
				        	   
				    				
				    	    		show_list();
					        	    docpathedit.setText("");
					        	    othrtxtsupp.setText("");othrtxtoff.setText("");
					        	    othrtxtsupp.setVisibility(v1.GONE);othrtxtoff.setVisibility(v1.GONE);

					        		docbrws.setVisibility(visibility);docupd.setVisibility(v1.GONE);
					        		s1.setSelection(0);
				      		 }
				      		 else
				      		 {
				      			System.out.println("doc title not select");
				      			cf.ShowToast("Please select the Document Title.",1);
				      		 }
		    			}
		        	    catch(Exception e)
		    			{
		        	    	cf.ShowToast("There is a problem in saving your data due to invalid character.",1);
		    				
		    			}
	        	    
			      	}
				}
		    	else
		    	{
		    		cf.ShowToast("Your file size exceeds 2MB.",1);
		    		docpathedit.setText("");
	        	    othrtxtsupp.setText("");othrtxtoff.setText("");
	        	    othrtxtsupp.setVisibility(v1.GONE);othrtxtoff.setVisibility(v1.GONE);

	        		docbrws.setVisibility(visibility);docupd.setVisibility(v1.GONE);
	        		s1.setSelection(0);
		    	}
	        	    
        	   
		    	break;
		    case R.id.offupdtxt:
		    	selectedImagePath=offpathedit.getText().toString().trim();
		    	if(selectedImagePath.contains("file://"))
		    	{
		    		selectedImagePath = selectedImagePath.replace("file://","");
		    	}
		    	boolean bu1=cf.common(selectedImagePath);
		    	if(bu1)
				{
		    		if((documentothertextsupp==1) && (othrtxtsupp.length()!=0))
			      	{
		    			strdoctitle1 = othrtxtsupp.getText().toString();
		    		}
				    else if((documentothertextoff==1)  && (othrtxtoff.length()!=0))
				    {
				    	strdoctitle1 = othrtxtoff.getText().toString();				    	
				    }
			        else
				    {
			        	strdoctitle1=strdoctitle;
				    }
		    		
			    	flagstr=1;
			    	ImgOrder = getImageOrder(cf.selectedhomeid);
			    	System.out.println("off= "+ImgOrder);
			    	
			    	
			    	if(othrtxtoff.getText().toString().trim().equals("") && (documentothertextoff==1))
		    		{
			    		cf.ShowToast("Please enter the other text information.",1);
		    			othrtxtoff.setVisibility(v1.VISIBLE);		    			 
		    		}
			    	else
			    	{
			    		
			    		
						    	try
						    	{
						    		 if(!s2.getSelectedItem().toString().equals("--Select--"))
						      		 {
						    			 cf.gch_db.execSQL("INSERT INTO "
				        					+  cf.FeedBackDocumentTable
				        					+ " (GCH_FI_D_SRID,GCH_FI_D_DocumentTitle,GCH_FI_D_FileName,GCH_FI_D_Nameext,GCH_FI_D_ImageOrder,GCH_FI_D_IsOfficeUse,GCH_FI_D_CreatedOn,GCH_FI_D_ModifiedDate)"
				        					+ " VALUES ('" + cf.selectedhomeid + "','"+ cf.encode((s2.getSelectedItem().toString().equals("Other Information"))? othrtxtoff.getText().toString():s2.getSelectedItem().toString().trim())+"','"+cf.encode(selectedImagePath)+"','"+cf.encode(picname)+"','"+ImgOrder+"','"+flagstr + "','"
				        					+ cd+"','"+md+"')");
				        	    		
				        	 				
				        	 	    		showoffice_list();
					        	    		offpathedit.setText("");
					        	    		othrtxtsupp.setText("");othrtxtoff.setText("");
					      	    		  	othrtxtsupp.setVisibility(v1.GONE);othrtxtoff.setVisibility(v1.GONE);
							      			offbrws.setVisibility(visibility);offupd.setVisibility(v1.GONE);
							      			s2.setSelection(0);
				        	 			}
							    		 else
							      		 {
							      			System.out.println("doc title not select");
							      			cf.ShowToast("Please select the Document Title.",1);
							      		 }
						    	}
					      		
							catch(Exception e)
			    			{
								cf.ShowToast("There is a problem in saving your data due to invalid character.",1);
			    				
			    			}
				        	    		
			    	}
				}
		    	else
				{
		    		cf.ShowToast("Your file size exceeds 2MB.",1);
		    			offpathedit.setText("");
	      	    		  othrtxtsupp.setText("");othrtxtoff.setText("");
	      	    		  othrtxtsupp.setVisibility(v1.GONE);othrtxtoff.setVisibility(v1.GONE);

	      			 offbrws.setVisibility(visibility);offupd.setVisibility(v1.GONE);
		    			s2.setSelection(0);
				}
		    	
        	   
		    	break;
		
		}
	}
	public int getImageOrder(String srid)
	{
		 Cursor	c11 =  cf.gch_db.rawQuery("SELECT * FROM "+  cf.FeedBackDocumentTable	+ " WHERE GCH_FI_D_SRID='"+srid+"' order by GCH_FI_D_ImageOrder desc",null);
	     int imgrws = c11.getCount();
	     
	     if(imgrws==0){
	    	 ImgOrder=1;
	     }
	     else
	     {
	    	 c11.moveToFirst();
	    	 int imgordr=c11.getInt(c11.getColumnIndex("GCH_FI_D_ImageOrder"));
	    	 ImgOrder=imgordr+1;
	    	 
	     }
	    return ImgOrder;
	}
	public class MyOnItemSelectedListenerdata implements OnItemSelectedListener {

	    public void onItemSelected(AdapterView<?> parent,
	        View view, int pos, long id) {	  
	    	strpresatins = parent.getItemAtPosition(pos).toString();
	    	if(strpresatins.equals("Other"))
	    	{
	    		othrtxt.setVisibility(visibility);
	    		o=1;
	    	}
	    	else
	    	{
	    		othrtxt.setVisibility(view.GONE);
	    		o=0;
	    	}
	     }

	    public void onNothingSelected(AdapterView parent) {
	      // Do nothing.
	    }
	}
	public class MyOnItemSelectedListenerdata1 implements OnItemSelectedListener {

	    public void onItemSelected(AdapterView<?> parent,
	        View view, int pos, long id) {	  
	    	strdoctitle = parent.getItemAtPosition(pos).toString();
	    	if(strdoctitle.equals("Other Information"))
	    	{
	    		othrtxtsupp.setVisibility(visibility);	    
	    		
	    		documentothertextsupp=1;
	    	}
	    	else
	    	{
	    		othrtxtsupp.setVisibility(view.GONE);
	    		documentothertextsupp=0;
	    	}

	     }

	    public void onNothingSelected(AdapterView parent) {
	      // Do nothing.
	    }
	}

	public class MyOnItemSelectedListenerdata2 implements OnItemSelectedListener {

    public void onItemSelected(AdapterView<?> parent,
        View view, int pos, long id) {	  
    	strdoctitle = parent.getItemAtPosition(pos).toString();
    	if(strdoctitle.equals("Other Information"))
    	{
    		othrtxtoff.setVisibility(visibility);	    	 
    		documentothertextoff=1;
    	}
    	else
    	{
    		othrtxtoff.setVisibility(view.GONE);
    		documentothertextoff=0;
    	}

     }

    public void onNothingSelected(AdapterView parent) {
      // Do nothing.
    }
}
	
	public static class EfficientAdapter extends BaseAdapter {
		private static final int IMAGE_MAX_SIZE = 0;
		private LayoutInflater mInflater;
		ViewHolder holder;
		View v2;String j;

		public EfficientAdapter(Context context) {
			mInflater = LayoutInflater.from(context);
		}

		public int getCount() {
			int arrlen = 0;
			arrlen = docpatharr.length;
			

			return arrlen;
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {

				holder = new ViewHolder();

				convertView = mInflater.inflate(R.layout.listview, null);
				holder.text2 = (ImageView) convertView
						.findViewById(R.id.TextView02);
				holder.text3 = (TextView) convertView
						.findViewById(R.id.TextView03);
				holder.text4 = (TextView) convertView
				.findViewById(R.id.TextView04);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			try {

				if (docpatharr[position].contains("null")
						|| docnamearr[position].contains("null")||docidarr[position].contains("null")) {
					docpatharr[position] = docpatharr[position].replace("null", "");
					docnamearr[position] = docnamearr[position].replace("null", "");
				
					docidarr[position] = docidarr[position].replace("null", "");
				
				}
				holder.text2.setBackgroundResource(R.drawable.allfilesicon);

				  holder.text3.setText(docpatharr[position]);
				  holder.text4.setText(docnamearr[position]);
				

			} catch (Exception e) {
				Log.i(TAG, "error:  efficiant" + e.getMessage());
			}

			return convertView;
		}

		static class ViewHolder {
			TextView text3,text4;
			ImageView text2;

		}

	}
	public static class EfficientAdapter1 extends BaseAdapter {
		private static final int IMAGE_MAX_SIZE = 0;
		private LayoutInflater mInflater;
		ViewHolder holder;
		View v2;String j;

		public EfficientAdapter1(Context context) {
			mInflater = LayoutInflater.from(context);
		}

		public int getCount() {
			int arrlen = 0;
			
				arrlen = offpatharr.length;
			
			return arrlen;
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {

				holder = new ViewHolder();

				convertView = mInflater.inflate(R.layout.listview, null);
				holder.text2 = (ImageView) convertView
						.findViewById(R.id.TextView02);
				holder.text3 = (TextView) convertView
						.findViewById(R.id.TextView03);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			try {

				if (offpatharr[position].contains("null")
						|| offnamearr[position].contains("null")||offdocidarr[position].contains("null")) {
					offpatharr[position] = offpatharr[position].replace("null", "");
					offnamearr[position] = offnamearr[position].replace("null", "");
					offdocidarr[position] = offdocidarr[position].replace("null", "");
				
				}
				holder.text2.setBackgroundResource(R.drawable.allfilesicon);
            	holder.text3.setText(offpatharr[position]);
				

			} catch (Exception e) {
				Log.i(TAG, "error: efficiaNT2 " + e.getMessage());
			}

			return convertView;
		}

		static class ViewHolder {
			TextView text3;
			ImageView text2;

		}

	}
	public void show()
	{
		selectedImagePath="";
		AlertDialog.Builder builder = new AlertDialog.Builder(Feedback.this);
        builder.setTitle("Pick an option");
        builder.setAdapter(adapter,
                        new DialogInterface.OnClickListener() {
        	
                            

								private String[] pdfname;

								public void onClick(DialogInterface dialog,
                                                int item) {
                                        if(items[item].equals("gallery"))
                                        {
                                        	
                                        	t=0;pickfromgallery();
                                        }
                                        else if(items[item].equals("camera"))
                                        {
                                        	t=1;startCameraActivity();
                                        }
                                        else
                                        {
                                        	final Dialog dialog1 = new Dialog(Feedback.this);
                                            dialog1.setContentView(R.layout.dialog);
                                            dialog1.setTitle(Html.fromHtml("<h2><font  color=#bddb00>Select PDF to upload  </font></h2>"));
                                            lv1=(ListView)dialog1.findViewById(R.id.ListView01);
                                            images = Environment.getExternalStorageDirectory(); 
                                            ii=0;
                                           // pdfsample=new String[100];
                                            walkdir(images);
                                            pdflist= new String[ii];
                                             pdfname=new String[ii];
                                          if(ii>=1)
                                          {
                                            for(int i=0;i<ii;i++)
                                            {
                                          
                                            	
                                            	pdflist[i]=pdfsample[i];	
                                            	
                                            	 String[] bits = pdfsample[i].split("/");
                                            	 pdfname[i] = bits[bits.length - 1];
                                            }
                                            
                                                                                   
                                            lv1.setAdapter(new ArrayAdapter<String>(Feedback.this,android.R.layout.simple_list_item_1 ,pdfname));
                                            lv1.setOnItemClickListener(new OnItemClickListener() {
                                            	public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                                            		 	PackageManager packageManager = getPackageManager(); 
                                                Intent testIntent = new Intent(Intent.ACTION_VIEW); 
                                                testIntent.setType("application/pdf"); 
                                                List list = packageManager.queryIntentActivities(testIntent, PackageManager.MATCH_DEFAULT_ONLY);
                                                if (list.size() > 0 ) {  
                                                    Intent intent = new Intent();
                                                    intent.setAction(Intent.ACTION_VIEW);
                                                    Uri uri = Uri.fromFile(new File(pdflist[(int) id]).getAbsoluteFile());
                                                   
                                                    System.out.println("find you have done "+pdflist[(int) id]);
                                                     selectedImagePath=cf.decode(uri.toString());
                                                    
                                                     System.out.println("find you have done "+selectedImagePath);
       					     					     String[] bits = selectedImagePath.split("/");
                       							     picname = bits[bits.length - 1];
                       							  AlertDialog.Builder builder = new AlertDialog.Builder(Feedback.this);
                                      			builder.setMessage("What would you like to do?")
                                      			       .setCancelable(false)
                                      			       .setPositiveButton("Preview", new DialogInterface.OnClickListener() {
                                      			           public void onClick(DialogInterface dialog, int id) {
                                      			        	   String suboffstring = selectedImagePath.substring(11);
                                      			        
                                      			        	 File file = null;
																//file = new File(URLDecoder.decode(suboffstring, "UTF-8"));
																file = new File(suboffstring);
    		                     							 
    		                     				                   if (file.exists()) {
    		                     				                	Uri path = Uri.fromFile(file);
    		                     				                    Intent intent1 = new Intent(Intent.ACTION_VIEW);
    		                     				                    intent1.setDataAndType(path, "application/pdf");
    		                     				                    intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    		                     				 
    		                     				                    try {
    		                     				                        startActivity(intent1);
    		                     				                    } 
    		                     				                    catch (ActivityNotFoundException e) {
    		                     				                    	cf.ShowToast("No Application available to view PDF.",1);
    		                     				                       
    		                     				                    }

                                             if(c==1 && !selectedImagePath.equals(""))
                                            	 
                                    			    {
                                    			    	docpathedit.setText(selectedImagePath);
                                    				    docbrws.setVisibility(v1.GONE);
                                    				    docupd.setVisibility(visibility);
                                    			    }
                                    			    else if(c==2 && !selectedImagePath.equals(""))
                                    			    {
                                    			    	offpathedit.setText(selectedImagePath);
                                    				    offbrws.setVisibility(v1.GONE);
                                    				    offupd.setVisibility(visibility);
                                    			    }
    		                     				                  }
                                    			           }
                                    			       })
                                    			       
                                      			.setNeutralButton("Select", new DialogInterface.OnClickListener() {
                                    			           public void onClick(DialogInterface dialog, int id) {
                                    			        	   if(c==1 && !selectedImagePath.equals(""))
                                                              	 
                                               			    {
                                               			    	docpathedit.setText(selectedImagePath);
                                               				    docbrws.setVisibility(v1.GONE);
                                               				    docupd.setVisibility(visibility);
                                               			    }
                                               			    else if(c==2 && !selectedImagePath.equals(""))
                                               			    {
                                               			    	offpathedit.setText(selectedImagePath);
                                               				    offbrws.setVisibility(v1.GONE);
                                               				    offupd.setVisibility(visibility);
                                               			    }
                                           				    System.out.println("path"+selectedImagePath);
                                    			           }
                                    			       })
                                    			       .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    			           public void onClick(DialogInterface dialog, int id) {
                                    			        	   selectedImagePath="";
                                    			           }
                                    			       });
                                    			 builder.show();

                                    			  dialog1.cancel();
                                                  
                                                }

                                            	}
                                            
                                            	});                                           
                                              
                                            dialog1.show();
                                       } // the validation if of pdf count 
                                            else
                                            {
                                            	cf.ShowToast("Please check whether you have PDF in your Device.",1);
                                             }
                                        }
                                        dialog.dismiss();
                                }
                        });
        AlertDialog alert = builder.create();
        alert.show();
	}
	protected void pickpdf() {
		// TODO Auto-generated method stub
		PackageManager packageManager = getPackageManager();
		Intent testIntent = new Intent(Intent.ACTION_VIEW);
		testIntent.setType("application/pdf");
		startActivityForResult(Intent.createChooser(testIntent, "Select pdf"), SELECT_PDF);

}
	protected void startCameraActivity() {
		String fileName = "temp.jpg";
		ContentValues values = new ContentValues();
		values.put(MediaStore.Images.Media.TITLE, fileName);
		mCapturedImageURI = getContentResolver().insert(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
		startActivityForResult(intent, CAPTURE_PICTURE_INTENT);

	}
	protected void pickfromgallery() {	
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		startActivityForResult(Intent.createChooser(intent, "Select Picture"),
				SELECT_PICTURE);
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
			if(t==0)
			{
				if (resultCode == RESULT_OK) {
					if (requestCode == SELECT_PICTURE) {
						Uri selectedImageUri = data.getData();
						selectedImagePath = getPath(selectedImageUri);
						 String[] bits = selectedImagePath.split("/");
						 picname = bits[bits.length - 1];
					}
				}
			}
			else if (t == 1) {
					switch (resultCode) {
					case 0:
						break;
					case -1:
						String[] projection = { MediaStore.Images.Media.DATA };
						Cursor cursor = managedQuery(mCapturedImageURI, projection,
								null, null, null);
						int column_index_data = cursor
								.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
						cursor.moveToFirst();
						capturedImageFilePath = cursor.getString(column_index_data);
						selectedImagePath = capturedImageFilePath;
						
						 String[] bits = selectedImagePath.split("/");
						 picname = bits[bits.length - 1];
						break;

					}
				}
			if(!selectedImagePath.trim().equals(""))
			{
			Bitmap b= cf.ShrinkBitmap(selectedImagePath, 400, 400);
		
			if(b==null)
			{
				cf.ShowToast("This image is not a supported format.You can not upload.",1);
			}
			else if(c==1 && !selectedImagePath.equals("") )
			    {
			    	docpathedit.setText(selectedImagePath);
				    docbrws.setVisibility(v1.GONE);
				    docupd.setVisibility(visibility);
			    }
			    else if(c==2 && !selectedImagePath.equals("") )
			    {
			    	offpathedit.setText(selectedImagePath);
				    offbrws.setVisibility(v1.GONE);
				    offupd.setVisibility(visibility);
			    }
			}
			else
			{
				cf.ShowToast("you have not selected any images.",1);
			}
		
	}
	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}
	protected void offshowcommon() {
		
		offtbl.setVisibility(visibility);offedittitle.setVisibility(visibility);
	}

	public boolean onKeyDown(int keyCode, KeyEvent event)
	 {
	 //replaces the default 'Back' button action
	 if(keyCode==KeyEvent.KEYCODE_BACK)
	 {
		 System.out.println("selectedImagePath= "+selectedImagePath);
		 Intent myintent=new Intent(Feedback.this,photos.class);
		  cf.putExtras(myintent);
		  myintent.putExtra("type", 55);
	 		 startActivity(myintent);
	      return true;
	 }
	 return super.onKeyDown(keyCode, event);
	 }
	private void show_list()
	{
		docname="";docpath="";doc_id="";Log.i(TAG,"homeid= "+cf.selectedhomeid);
		 try {
			   Cursor	c2 =  cf.gch_db.rawQuery("SELECT * FROM "+  cf.FeedBackDocumentTable	+ " WHERE GCH_FI_D_SRID='"+cf.selectedhomeid+"' and GCH_FI_D_IsOfficeUse=0",null);
			   docrws = c2.getCount(); 
			   if(docrws!=0)        
			   {
				   int Column1=c2.getColumnIndex("GCH_FI_D_FileName");
	     	        int Column2=c2.getColumnIndex("GCH_FI_D_DocumentTitle");
	     	        c2.moveToFirst();
	     	        int i=0;docnamearr=new String[docrws];docpatharr=new String[docrws];docidarr=new String[docrws];
			     	if(c2!=null)
			     	{
			     		do{
			     			docnamearr[i]=cf.decode(c2.getString(Column1));
			     			docpatharr[i]=cf.decode(c2.getString(Column2));
			     			docidarr[i]=c2.getString(c2.getColumnIndex("GCH_FI_DocumentId"));
			     			// docnamearr = docname.split("~");
			     			// docpatharr=docpath.split("~"); 
			     			// docidarr=doc_id.split("~");
		     				doctbllst.setVisibility(visibility);
		     				ColorDrawable sage = new ColorDrawable(this.getResources().getColor(R.color.sage));
		     				doclst.setDivider(sage);
		     				doclst.setVisibility(visibility);doclst.setDividerHeight(2);
		     			   // doclst.setAdapter(new EfficientAdapter(this));
		     			   
		     		i++;
                     }while(c2.moveToNext());
			     		TableRow.LayoutParams mParam = new TableRow.LayoutParams((int)(cf.wd-150),(int)(docrws * 50)+20);
			            doclst.setLayoutParams(mParam);
			            doclst.setAdapter(new EfficientAdapter(this));
	              }
			   }
			   else
			   {
				   doclst.setVisibility(v2.GONE);doclst.setVisibility(v2.GONE);
			   }
       }
		 catch(Exception e)
		 {
			 Log.i(TAG,"error= show list "+e.getMessage());
		 } 
		 doclst.setOnItemClickListener(new OnItemClickListener() {
			  public void onItemClick(AdapterView<?> a, View v, int position, long id) {
			      final String selarr = docnamearr[position];
			      selected_id=docidarr[position];
			      System.out.println(selected_id +"selected_id");
			      final Dialog dialog = new Dialog(Feedback.this);
		            dialog.getWindow().setContentView(R.layout.maindialog);
		            dialog.setTitle(docpatharr[position]);//System.out.pri
		            dialog.setCancelable(true);
		            final ImageView img = (ImageView) dialog.findViewById(R.id.ImageView01);
		            EditText ed1 = (EditText) dialog.findViewById(R.id.TextView01);
		            ed1.setVisibility(v1.GONE);img.setVisibility(v1.GONE);
		            
		            Button button_close = (Button) dialog.findViewById(R.id.Button01);
		    		button_close.setText("Close");//button_close.setVisibility(v2.GONE);
		    		
		    		
		            Button button_d = (Button) dialog.findViewById(R.id.Button03);
		    		button_d.setText("Delete");
		    		button_d.setVisibility(v2.VISIBLE);
		    		
		    		final Button button_view = (Button) dialog.findViewById(R.id.Button02);
		    		button_view.setText("View");
		    		button_view.setVisibility(v2.VISIBLE);
		    		
		    		
		    		final Button button_saveimage= (Button) dialog.findViewById(R.id.Button05);
		    		button_saveimage.setVisibility(v2.GONE);
		    		final LinearLayout linrotimage = (LinearLayout) dialog.findViewById(R.id.linrotation);
		    		linrotimage.setVisibility(v2.GONE);
		    		
		    		final Button rotateleft= (Button) dialog.findViewById(R.id.rotateleft);
		    		rotateleft.setVisibility(v2.GONE);
		    		final Button rotateright= (Button) dialog.findViewById(R.id.rotateright);
		    		rotateright.setVisibility(v2.GONE);
		    		
		    		
		    		
		    		
		    		System.out.println("problme");
		    		button_d.setOnClickListener(new OnClickListener() {
			            public void onClick(View v) {
			            
			            AlertDialog.Builder builder = new AlertDialog.Builder(Feedback.this);
			   			builder.setMessage("Are you sure you want to delete?")
		   			       .setCancelable(false)
		   			       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		   			           public void onClick(DialogInterface dialog, int id) {
		   			        	 cf.gch_db.execSQL("Delete From " +  cf.FeedBackDocumentTable + "  WHERE GCH_FI_D_SRID ='" + cf.selectedhomeid + "' and GCH_FI_DocumentId='"+selected_id+"' and GCH_FI_D_IsOfficeUse=0");
		   			        	cf.ShowToast("Document has been deleted sucessfully.",1);
  	   					   show_list();
		   			           }
		   			       })
		   			       .setNegativeButton("No", new DialogInterface.OnClickListener() {
		   			           public void onClick(DialogInterface dialog, int id) {
		   			                dialog.cancel();
		   			           }
		   			       });
		   			 builder.show();
		            
		   			 dialog.cancel(); 
	            }
   		           	
			  	 });
		    		
		    		
		    		
		    		button_view.setOnClickListener(new OnClickListener() {
			            public void onClick(View v) {
			            currnet_rotated=0;
			            	if(selarr.endsWith(".pdf"))
						     {
			            		img.setVisibility(v2.GONE);
			            		String tempstr ;
			            		if(selarr.contains("file://"))
			    		    	{
			    		    		 tempstr = selarr.replace("file://","");		    		
			    		    	}
			            		else
			            		{
			            			tempstr = selarr;
			            		}
			            		 File file = new File(tempstr);
							 
				                 if (file.exists()) {
				                	Uri path = Uri.fromFile(file);
				                    Intent intent = new Intent(Intent.ACTION_VIEW);
				                    intent.setDataAndType(path, "application/pdf");
				                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				  
				                    try {
				                        startActivity(intent);
				                    } 
				                    catch (ActivityNotFoundException e) {
				                    	cf.ShowToast("No application available to view PDF.",1);
				                       
				                    }
				               
				                }
						     }
			            	else  
			            	{
			            		Bitmap bitmap2=ShrinkBitmap(selarr,250,250);
			            		BitmapDrawable bmd2 = new BitmapDrawable(bitmap2); 
		    					img.setImageDrawable(bmd2);
		    					img.setVisibility(v2.VISIBLE);
		    					linrotimage.setVisibility(v2.VISIBLE);
		    					rotateleft.setVisibility(v2.VISIBLE);
		    					rotateright.setVisibility(v2.VISIBLE);
					            button_view.setVisibility(v2.GONE);
					            button_saveimage.setVisibility(v2.VISIBLE);
					           
			            		
			            	}
			            }
		    		});
		    		rotateleft.setOnClickListener(new OnClickListener() {  			
		    			public void onClick(View v) {

		    				// TODO Auto-generated method stub
		    			
		    				System.gc();
		    				currnet_rotated-=90;
		    				if(currnet_rotated<0)
		    				{
		    					currnet_rotated=270;
		    				}

		    				
		    				Bitmap myImg;
		    				try {
		    					myImg = BitmapFactory.decodeStream(new FileInputStream(selarr));
		    					Matrix matrix =new Matrix();
		    					matrix.reset();
		    					//matrix.setRotate(currnet_rotated);
		    					System.out.println("Ther is no more issues top ");
		    					matrix.postRotate(currnet_rotated);
		    					
		    					 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
		    					        matrix, true);
		    					 
		    					 img.setImageBitmap(rotated_b);

		    				} catch (FileNotFoundException e) {
		    					// TODO Auto-generated catch block
		    					e.printStackTrace();
		    				}
		    				catch (Exception e) {
		    					
		    				}
		    				catch (OutOfMemoryError e) {
		    					
		    					System.gc();
		    					try {
		    						myImg=null;
		    						System.gc();
		    						Matrix matrix =new Matrix();
		    						matrix.reset();
		    						//matrix.setRotate(currnet_rotated);
		    						matrix.postRotate(currnet_rotated);
		    						myImg= cf.ShrinkBitmap(selarr, 800, 800);
		    						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
		    						System.gc();
		    						img.setImageBitmap(rotated_b); 

		    					} catch (Exception e1) {
		    						// TODO Auto-generated catch block
		    						e1.printStackTrace();
		    					}
		    					 catch (OutOfMemoryError e1) {
		    							// TODO Auto-generated catch block
		    						 cf.ShowToast("You can not rotate this image. Image size is too large.",1);
		    					}
		    				}

		    			
		    			}
		    		});
		    		rotateright.setOnClickListener(new OnClickListener() {
		    		    public void onClick(View v) {
		    		    	System.out.println("problme in nothig");
		    		    	currnet_rotated+=90;
		    				if(currnet_rotated>=360)
		    				{
		    					currnet_rotated=0;
		    				}
		    				System.out.println("selarr "+selarr);
		    				Bitmap myImg;
		    				try {
		    					myImg = BitmapFactory.decodeStream(new FileInputStream(selarr));
		    					Matrix matrix =new Matrix();
		    					matrix.reset();System.out.println("Ther is no more issues top ");
		    					//matrix.setRotate(currnet_rotated);
		    					System.out.println("Ther is no more issues top ");
		    					matrix.postRotate(currnet_rotated);
		    					System.out.println("postRotate "+currnet_rotated);
		    					 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
		    					        matrix, true);
		    					 System.out.println("rotate"+rotated_b); 
		    					 img.setImageBitmap(rotated_b); System.out.println("rotatepic"); 

		    				} catch (FileNotFoundException e) {
		    					System.out.println("FileNotFoundException "+e.getMessage()); 
		    					// TODO Auto-generated catch block
		    					e.printStackTrace();
		    				}
		    				catch(Exception e){}
		    				catch (OutOfMemoryError e) {
		    					System.out.println("comes in to out ot mem exception");
		    					System.gc();
		    					try {
		    						myImg=null;
		    						System.gc();
		    						Matrix matrix =new Matrix();
		    						matrix.reset();
		    						//matrix.setRotate(currnet_rotated);
		    						matrix.postRotate(currnet_rotated);
		    						myImg= cf.ShrinkBitmap(selarr, 800, 800);
		    						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
		    						System.gc();
		    						img.setImageBitmap(rotated_b); 

		    					} catch (Exception e1) {
		    						// TODO Auto-generated catch block
		    						e1.printStackTrace();
		    					}
		    					 catch (OutOfMemoryError e1) {
		    							// TODO Auto-generated catch block
		    						 cf.ShowToast("You can not rotate this image. Image size is too large.",1);
		    					}
		    				}

		    		    }
		    		});
		    		button_saveimage.setOnClickListener(new OnClickListener() {
						
						public void onClick(View v) {
							// TODO Auto-generated method stub
							System.out.println("selarr "+selarr);
							
							System.out.println("currnet_rotated "+currnet_rotated);
							
							if(currnet_rotated>0)
							{ 

								try
								{
									/**Create the new image with the rotation **/
									System.out.println("rotated_b "+rotated_b);
									String current=MediaStore.Images.Media.insertImage(getContentResolver(), rotated_b, "My bitmap", "My rotated bitmap");
									 ContentValues values = new ContentValues();
									  values.put(MediaStore.Images.Media.ORIENTATION, 0);
									  Feedback.this.getContentResolver().update(Uri.parse(current), values, MediaStore.Images.Media.DATA+ "=?", new String[] { current } );
									
									System.out.println("current "+current);
									if(current!=null)
									{
									String path=getPath(Uri.parse(current));
									File fout = new File(selarr);
									fout.delete();
									/** delete the selected image **/
									File fin = new File(path);
									/** move the newly created image in the slected image pathe ***/
									fin.renameTo(new File(selarr));
									cf.ShowToast("Saved successfully.",1);dialog.cancel();
									show_list();
									
									
								}
								} catch(Exception e)
								{
									System.out.println("Error occure while rotate the image "+e.getMessage());
								}
								
							}
							else
							{
								cf.ShowToast("Saved successfully.",1);
								dialog.cancel();
								show_list();
							}
							
						}
					});
		    		button_close.setOnClickListener(new OnClickListener() {
			            public void onClick(View v) {
			            	dialog.cancel();
			            }
		    		});
		    		dialog.show();

		  }
			  });
	}
	void showoffice_list()
	{
		try {
			   Cursor	c3 =  cf.gch_db.rawQuery("SELECT * FROM "+  cf.FeedBackDocumentTable	+ " WHERE GCH_FI_D_SRID='"+cf.selectedhomeid+"' and GCH_FI_D_IsOfficeUse=1",null);
			   offrws = c3.getCount();
			   if(offrws!=0)
			   {
				  int Column1=c3.getColumnIndex("GCH_FI_D_FileName");
	     	       int Column2=c3.getColumnIndex("GCH_FI_D_DocumentTitle");
	     	      c3.moveToFirst();
	     	    offnamearr= new String[c3.getCount()];
	     	   offpatharr= new String[c3.getCount()];
	     	  offdocidarr= new String[c3.getCount()];
			     	if(c3!=null)
			     	{int i=0;
			     		do{
			     			offnamearr[i] = cf.decode(c3.getString(Column1));
			     			 offpatharr[i]=cf.decode(c3.getString(Column2)); 
			     			offdocidarr[i]=c3.getString(c3.getColumnIndex("GCH_FI_DocumentId"));
			     			
			     			offtbllst.setVisibility(visibility);
		     			    ColorDrawable sage = new ColorDrawable(this.getResources().getColor(R.color.sage));
			     			offlst.setDivider(sage);
			     		    offlst.setVisibility(visibility);offlst.setDividerHeight(2);
		     			   // offlst.setAdapter(new EfficientAdapter1(this));
		     			           
		     			    i++;
                    }while(c3.moveToNext());
			     		
			     		TableRow.LayoutParams mParam = new TableRow.LayoutParams((int)(cf.wd-150),(int)((i*50)+20));
			     		offlst.setLayoutParams(mParam);
			            offlst.setAdapter(new EfficientAdapter1(this));
	       }
			   }
			   else
			   {
				    offtbllst.setVisibility(v2.GONE);
				    offlst.setVisibility(v2.GONE);
			   }
      }
		 catch(Exception e)
		 {
			 Log.i(TAG,"error lastr= "+e.getMessage());
		 }
		
	}
	
	 public void walkdir(File dir) {
		    String pdfPattern = ".pdf";
		    File listFile[] = dir.listFiles();
           if (listFile != null) {
		    	  for (int i = 0; i < listFile.length; i++) {

		            if (listFile[i].isDirectory()) {
		                walkdir(listFile[i]);
		            } else  {
		            if (listFile[i].getName().endsWith(pdfPattern)){
		                   
		            	 pdfsample=dynamicarraysetting(dir+"/"+listFile[i].getName(),pdfsample);
		            	  ii++;
		              }
		            }
		        }
		    }    
		}
	 private String[] dynamicarraysetting(String ErrorMsg,String[] pieces3) {
			// TODO Auto-generated method stub
			 System.out.println("ErrorMsg "+ErrorMsg);
			
			 try
			 {
			if(pieces3==null)
			{
				pieces3=new String[1];
				
				pieces3[0]=ErrorMsg;
			}
			else
			{
				
				String tmp[]=new String[pieces3.length+1];
				int i;
				for(i =0;i<pieces3.length;i++)
				{
					
					tmp[i]=pieces3[i];
				}
			
				tmp[tmp.length-1]=ErrorMsg;
				pieces3=null;
				pieces3=tmp.clone();
			}
			 }
			 catch(Exception e)
			 {
				 System.out.println("Exception "+e.getMessage());
				 //return pieces3;
			 }
			return pieces3;
		}
	 Bitmap ShrinkBitmap(String file, int width, int height) {
		 Bitmap bitmap =null;
			try {
				BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
				bmpFactoryOptions.inJustDecodeBounds = true;
			    bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

				int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
						/ (float) height);
				int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
						/ (float) width);
                
				if (heightRatio > 1 || widthRatio > 1) {
					if (heightRatio > widthRatio) {
						bmpFactoryOptions.inSampleSize = heightRatio;
					} else {
						bmpFactoryOptions.inSampleSize = widthRatio;
					}
				}

				bmpFactoryOptions.inJustDecodeBounds = false;
				bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
				
				 System.out.println("ratio= "+bitmap.getWidth()+"\n"+bitmap.getHeight());
				return bitmap;
			} catch (Exception e) {
			
				return bitmap;
			}

		}
	 public void onWindowFocusChanged(boolean hasFocus) {

		    super.onWindowFocusChanged(hasFocus);
		    if(focus==1)
		    {
		    	focus=0;
		    	edfbcoment.setText(edfbcoment.getText().toString());    
		    }
		 }      
	
}
