package inspectiondepot.gch;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import inspectiondepot.gch.R;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class InspectorInfo extends Activity{
	CommonFunctions cf;
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.selectedhomeid = extras.getString("homeid");
				cf.onlinspectionid = extras.getString("InspectionType");
			    cf.onlstatus = extras.getString("status");
         	}
			setContentView(R.layout.inspectorinfo);
	        cf.getDeviceDimensions();
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 1, 0,cf));
			LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.generalsubmenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 13, 1,cf));
			ScrollView scr = (ScrollView)findViewById(R.id.scr);
		    scr.setMinimumHeight(cf.ht);
			TextView geninfotxt = (TextView) findViewById(R.id.topgentext);
			geninfotxt.setText(Html.fromHtml(cf.toptextvalue));
			
			TextView txt_inspectorname = (TextView) findViewById(R.id.txtinspectorname);
			TextView txt_inspectioncompanyname = (TextView) findViewById(R.id.txtinspectioncompanyname);
			TextView txt_inspectionLT = (TextView) findViewById(R.id.txtinspectionLT);
			TextView txt_inspectionLN = (TextView) findViewById(R.id.txtinspectionLN);
			ImageView txt_inspectionLP = (ImageView) findViewById(R.id.txtinspectionLP);
			ImageView txt_inspectionLS = (ImageView) findViewById(R.id.txtinspectionLS);
			String sign = "",inspext = "";
			cf.getInspectorId();
			try {
				Cursor c2 = cf.gch_db.rawQuery("SELECT * FROM "
						+ cf.inspectorlogin + " WHERE Fld_InspectorId='" + cf.Insp_id + "'", null);
				int rws = c2.getCount();
				c2.moveToFirst();
				if (c2 != null) {
					do {
						txt_inspectorname.setText(cf.decode(c2.getString(c2.getColumnIndex("Fld_InspectorFirstName"))+" "+c2.getString(c2.getColumnIndex("Fld_InspectorLastName"))));
						txt_inspectioncompanyname.setText(cf.decode(c2.getString(c2.getColumnIndex("Fld_InspectorCompanyName"))));
						txt_inspectionLT.setText(cf.decode(c2.getString(c2.getColumnIndex("Insp_PrimaryLicenseType"))));
						txt_inspectionLN.setText(cf.decode(c2.getString(c2.getColumnIndex("Insp_PrimaryLicenseNumber"))));
						sign=cf.decode(c2.getString(c2.getColumnIndex("Insp_sign_img")));
						inspext = cf.decode(c2.getString(c2.getColumnIndex("Fld_InspectorPhotoExtn")));
			    } while (c2.moveToNext());
			   }
	         } catch (Exception e) {
				cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ InspectorInfo.this +" "+" in the processing stage of retrieving data from Inspector table  at "+" "+ cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
	         }	
			try
			{
			File outputFile = new File(this.getFilesDir()+"/"+cf.Insp_id.toString()+inspext);
			if(outputFile.exists())
			{
				BitmapFactory.Options o = new BitmapFactory.Options();
				o.inJustDecodeBounds = true;
				BitmapFactory.decodeStream(new FileInputStream(outputFile),
						null, o);
				final int REQUIRED_SIZE = 200;
				int width_tmp = o.outWidth, height_tmp = o.outHeight;
				int scale = 1;
				while (true) {
					if (width_tmp / 2 < REQUIRED_SIZE
							|| height_tmp / 2 < REQUIRED_SIZE)
						break;
					width_tmp /= 2;
					height_tmp /= 2;
					scale *= 2;
				}
				BitmapFactory.Options o2 = new BitmapFactory.Options();
				o2.inSampleSize = scale;
				Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(
						outputFile), null, o2);
				BitmapDrawable bmd = new BitmapDrawable(bitmap);
				
				txt_inspectionLP.setImageDrawable(bmd);
				
			}
			/* Strat code for the sign **/
			File outputFile1 = new File(this.getFilesDir()+"/"+sign);
			if(outputFile1.exists())
			{
			BitmapFactory.Options o1 = new BitmapFactory.Options();
			o1.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(outputFile1),
					null, o1);
			final int REQUIRED_SIZE1 = 200;
			int width_tmp1 = o1.outWidth, height_tmp1 = o1.outHeight;
			int scale1 = 1;
			while (true) {
				if (width_tmp1 / 2 < REQUIRED_SIZE1
						|| height_tmp1 / 2 < REQUIRED_SIZE1)
					break;
				width_tmp1 /= 2;
				height_tmp1 /= 2;
				scale1 *= 2;
			}
			BitmapFactory.Options o4 = new BitmapFactory.Options();
			o4.inSampleSize = scale1;
			Bitmap bitmap1 = BitmapFactory.decodeStream(new FileInputStream(
					outputFile1), null, o4);
			BitmapDrawable bmd1 = new BitmapDrawable(bitmap1);
			
			txt_inspectionLS.setImageDrawable(bmd1);
		}
			/* Strat code for the sign ends **/	
		}
		catch (IOException e)
		{
			cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ InspectorInfo.this+" "+" in the stage of retrieving inspector image at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}     
	 }
	 public void clicker(View v)
	 {
		  switch(v.getId())
		  {
		  case R.id.home:
			  cf.gohome();
			  break;
		  }
	 }
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
			// replaces the default 'Back' button action
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				Intent  myintent = new Intent(InspectorInfo.this,Schedule.class);
				myintent.putExtra("InspectionType", cf.onlinspectionid);
				myintent.putExtra("status", cf.onlstatus);
				myintent.putExtra("homeid", cf.selectedhomeid);
				startActivity(myintent);
				return true;
			}
			if (keyCode == KeyEvent.KEYCODE_MENU) {

			}
			return super.onKeyDown(keyCode, event);
		}
	 protected void onActivityResult(int requestCode, int resultCode, Intent data) {
				switch (resultCode) {
	 			case 0:
	 				break;
	 			case -1:
	 				try {

	 					String[] projection = { MediaStore.Images.Media.DATA };
	 					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
	 							null, null, null);
	 					int column_index_data = cursor
	 							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
	 					cursor.moveToFirst();
	 					String capturedImageFilePath = cursor.getString(column_index_data);
	 					cf.showselectedimage(capturedImageFilePath);
	 				} catch (Exception e) {
	 				
	 				}
	 				
	 				break;

	 		}

	 	}

}
