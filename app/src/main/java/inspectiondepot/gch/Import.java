package inspectiondepot.gch;

import inspectiondepot.gch.R;

import java.io.FileNotFoundException;
import java.io.IOException;

import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeoutException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;



import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

public class Import extends Activity {
	CommonFunctions cf;
	public ProgressThread progThread;
	ProgressDialog progDialog;
	String contactperson,buidingsize,Website;
	int vcode;
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cf = new CommonFunctions(this);
        setContentView(R.layout.dataimport);
        progDialog = new ProgressDialog(this);
        cf.releasecode = (TextView)findViewById(R.id.releasecode);
        cf.releasecode.setText(cf.apkrc);
        cf.getInspectorId();
        vcode = getcurrentversioncode();
	}
	 public void clicker(View v)
	 {
		  switch(v.getId())
		  {
		  case R.id.importnow:
			 
			  insert_defaultcaption();
			  
			  if(cf.isInternetOn()==true)/*CHECK FOR INTERNET CONNECTION*/
			  {
				   
				    cf.total = 0;
					showDialog(1);
					start_import();
			  }
			  else
			  {
				    cf.usercheck = 1;
					handler.sendEmptyMessage(0);
					
			  }
			  break;
		  case R.id.back:
			  startActivity(new Intent(Import.this, HomeScreen.class));
			  break;
		  }
	 }
	 private void insert_defaultcaption() {
			// TODO Auto-generated method stub
			 try
			 {
				 cf.Create_Table(16);
				 Cursor c1=cf.SelectTablefunction(cf.Photo_caption, "");
				 
				 /*if(c1.getCount()<1)
				 {
			 				cf.gch_db.execSQL("INSERT INTO "
			 						+ cf.Photo_caption
			 						+ " SELECT  '1' as GCH_IMP_ID,'"+cf.Insp_id+"' as GCH_IMP_INSP_ID ,'" + cf.encode("Front Elevation") + "'as  GCH_IMP_Caption,'"+ 1 + "'as  GCH_IMP_Elevation,'" + 1 + "'as GCH_IMP_ImageOrders" +
			 						  " UNION SELECT '2','"+cf.Insp_id+"','" + cf.encode("Right Elevation") + "','" + 1 + "','" + 2 + "' " +
			 						  " UNION SELECT '3','"+cf.Insp_id+"','" + cf.encode("Rear Elevation") + "','" + 1 + "','" + 3 + "' " +
			 						  " UNION SELECT '4','"+cf.Insp_id+"','" + cf.encode("Left Elevation") + "','" + 1 + "','" + 4 + "' " +
			 						  " UNION SELECT '5','"+cf.Insp_id+"','" + cf.encode("Settlement Noted") + "','" + 1 + "','" + 5 + "' " +
			 						  " UNION SELECT '6','"+cf.Insp_id+"','" + cf.encode("Driveway Cracking") + "','" + 1 + "','" + 5 + "' " +
			 						  " UNION SELECT '7','"+cf.Insp_id+"','" + cf.encode("Concrete Uplift to Driveway") + "','" + 1 + "','" + 5 + "' " +
			 					  	  " UNION SELECT '8','"+cf.Insp_id+"','" + cf.encode("Tree Root Damage") + "','" + 1 + "','" + 5 + "' " +
			 						  " UNION SELECT '9','"+cf.Insp_id+"','" + cf.encode("Hairline Cracking") + "','" + 1 + "','" + 5 + "' " +
			 						  " UNION SELECT '10','"+cf.Insp_id+"','" + cf.encode("View of Level Sills") + "','" + 1 + "','" + 5 + "' " +
			 				  	      " UNION SELECT '11','"+cf.Insp_id+"','" + cf.encode("View of Plumb Walls") + "','" + 1 + "','" + 5 + "' " +
			 				  	      " UNION SELECT '12','"+cf.Insp_id+"','" + cf.encode("Settlement Noted") + "','" + 1 + "','" + 6 + "' " +
			 						  " UNION SELECT '13','"+cf.Insp_id+"','" + cf.encode("Driveway Cracking") + "','" + 1 + "','" + 6 + "' " +
			 						  " UNION SELECT '14','"+cf.Insp_id+"','" + cf.encode("Concrete Uplift to Driveway") + "','" + 1 + "','" + 6 + "' " +
			 					  	  " UNION SELECT '15','"+cf.Insp_id+"','" + cf.encode("Tree Root Damage") + "','" + 1 + "','" + 6 + "' " +
			 						  " UNION SELECT '16','"+cf.Insp_id+"','" + cf.encode("Hairline Cracking") + "','" + 1 + "','" + 6 + "' " +
			 						  " UNION SELECT '17','"+cf.Insp_id+"','" + cf.encode("View of Level Sills") + "','" + 1 + "','" + 6 + "' " +
			 				  	      " UNION SELECT '18','"+cf.Insp_id+"','" + cf.encode("View of Plumb Walls") + "','" + 1 + "','" + 6 + "' " +
			 				  	      " UNION SELECT '19','"+cf.Insp_id+"','" + cf.encode("Settlement Noted") + "','" + 1 + "','" + 7 + "' " +
			 						  " UNION SELECT '20','"+cf.Insp_id+"','" + cf.encode("Driveway Cracking") + "','" + 1 + "','" + 7 + "' " +
			 						  " UNION SELECT '21','"+cf.Insp_id+"','" + cf.encode("Concrete Uplift to Driveway") + "','" + 1 + "','" + 7 + "' " +
			 					  	  " UNION SELECT '22','"+cf.Insp_id+"','" + cf.encode("Tree Root Damage") + "','" + 1 + "','" + 7 + "' " +
			 						  " UNION SELECT '23','"+cf.Insp_id+"','" + cf.encode("Hairline Cracking") + "','" + 1 + "','" + 7 + "' " +
			 						  " UNION SELECT '24','"+cf.Insp_id+"','" + cf.encode("View of Level Sills") + "','" + 1 + "','" + 7 + "' " +
			 				  	      " UNION SELECT '25','"+cf.Insp_id+"','" + cf.encode("View of Plumb Walls") + "','" + 1 + "','" + 7 + "' " +
			 				  	      " UNION SELECT '26','"+cf.Insp_id+"','" + cf.encode("Settlement Noted") + "','" + 1 + "','" + 8 + "' " +
			 						  " UNION SELECT '27','"+cf.Insp_id+"','" + cf.encode("Driveway Cracking") + "','" + 1 + "','" + 8 + "' " +
			 						  " UNION SELECT '28','"+cf.Insp_id+"','" + cf.encode("Concrete Uplift to Driveway") + "','" + 1 + "','" + 8 + "' " +
			 					  	  " UNION SELECT '29','"+cf.Insp_id+"','" + cf.encode("Tree Root Damage") + "','" + 1 + "','" + 8 + "' " +
			 						  " UNION SELECT '30','"+cf.Insp_id+"','" + cf.encode("Hairline Cracking") + "','" + 1 + "','" + 8 + "' " +
			 						  " UNION SELECT '31','"+cf.Insp_id+"','" + cf.encode("View of Level Sills") + "','" + 1 + "','" + 8 + "' " +
			 				  	      " UNION SELECT '32','"+cf.Insp_id+"','" + cf.encode("View of Plumb Walls") + "','" + 1 + "','" + 8 + "' " +
			 		  	       	      " UNION SELECT '33','"+cf.Insp_id+"','" + cf.encode("Cracked glass") + "','" + 1 + "','" + 9 + "' " +
			 				  	      " UNION SELECT '34','"+cf.Insp_id+"','" + cf.encode("Uneven Door Opening") + "','" + 1 + "','" + 9 + "' " +
			 				  	      " UNION SELECT '35','"+cf.Insp_id+"','" + cf.encode("Cracks Around Openings") + "','" + 1 + "','" + 9 + "' " +
			 				  	      " UNION SELECT '36','"+cf.Insp_id+"','" + cf.encode("Settled Steps") + "','" + 1 + "','" + 9 + "' " + 
			 				  	      
			               
			 			  	      // elevation  Roof and Attic photographs starts here 
			 				  	      
			 				  	      " UNION SELECT '37','"+cf.Insp_id+"','" + cf.encode("General Roof Line Photograph") + "','" + 2 + "','" + 1 + "' " +
			 				  	      " UNION SELECT '38','"+cf.Insp_id+"','" + cf.encode("Right Elevation Roof Line") + "','" + 2 + "','" + 2 + "' " +
			 				  	      " UNION SELECT '39','"+cf.Insp_id+"','" + cf.encode("Rear Elevation Roof Line") + "','" + 2 + "','" + 3 + "' " +
			 				  	      " UNION SELECT '40','"+cf.Insp_id+"','" + cf.encode("Left Elevation Roof Line") + "','" + 2 + "','" + 4 + "' " +
			 				  	      " UNION SELECT '41','"+cf.Insp_id+"','" + cf.encode("Rood Sag") + "','" + 2 + "','" + 5 + "' " +
			 				  	      " UNION SELECT '42','"+cf.Insp_id+"','" + cf.encode("Broken Roof Truss") + "','" + 2 + "','" + 5 + "' " +
			 				  	      " UNION SELECT '43','"+cf.Insp_id+"','" + cf.encode("Altered Roof Truss / Structure") + "','" + 2 + "','" + 5 + "' " +
			 				  	      " UNION SELECT '44','"+cf.Insp_id+"','" + cf.encode("Excessively Notched Roof Member") + "','" + 2 + "','" + 5 + "' " +
			 				  	      " UNION SELECT '45','"+cf.Insp_id+"','" + cf.encode("Roof Leakage/Decay") + "','" + 2 + "','" + 5 + "' " +
			 				  	      " UNION SELECT '46','"+cf.Insp_id+"','" + cf.encode("Rood Sag") + "','" + 2 + "','" + 6 + "' " +
			 				  	      " UNION SELECT '47','"+cf.Insp_id+"','" + cf.encode("Broken Roof Truss") + "','" + 2 + "','" + 6 + "' " +
			 				  	      " UNION SELECT '48','"+cf.Insp_id+"','" + cf.encode("Altered Roof Truss / Structure") + "','" + 2 + "','" + 6 + "' " +
			 				  	      " UNION SELECT '49','"+cf.Insp_id+"','" + cf.encode("Excessively Notched Roof Member") + "','" + 2 + "','" + 6 + "' " +
			 				  	      " UNION SELECT '50','"+cf.Insp_id+"','" + cf.encode("Roof Leakage/Decay") + "','" + 2 + "','" + 6 + "' " +
			 				  	      " UNION SELECT '51','"+cf.Insp_id+"','" + cf.encode("Rood Sag") + "','" + 2 + "','" + 7 + "' " +
			 				  	      " UNION SELECT '52','"+cf.Insp_id+"','" + cf.encode("Broken Roof Truss") + "','" + 2 + "','" + 7 + "' " +
			 				  	      " UNION SELECT '53','"+cf.Insp_id+"','" + cf.encode("Altered Roof Truss / Structure") + "','" + 2 + "','" + 7 + "' " +
			 				  	      " UNION SELECT '54','"+cf.Insp_id+"','" + cf.encode("Excessively Notched Roof Member") + "','" + 2 + "','" + 7 + "' " +
			 				  	      " UNION SELECT '55','"+cf.Insp_id+"','" + cf.encode("Roof Leakage/Decay") + "','" + 2 + "','" + 7 + "' " +
			 				  	      " UNION SELECT '56','"+cf.Insp_id+"','" + cf.encode("Rood Sag") + "','" + 2 + "','" + 8+ "' " +
			 				  	      " UNION SELECT '57','"+cf.Insp_id+"','" + cf.encode("Broken Roof Truss") + "','" + 2 + "','" + 8 + "' " +
			 				  	      " UNION SELECT '58','"+cf.Insp_id+"','" + cf.encode("Altered Roof Truss / Structure") + "','" + 2 + "','" + 8 + "' " +
			 				  	      " UNION SELECT '59','"+cf.Insp_id+"','" + cf.encode("Excessively Notched Roof Member") + "','" + 2 + "','" + 8 + "' " +
			 				  	      " UNION SELECT '60','"+cf.Insp_id+"','" + cf.encode("Roof Leakage/Decay") + "','" + 2 + "','" + 8 + "' " +
			 				  	    // elevation  Roof and Attic photographs ENDS here
			 				  	    // elevation  Ground and Adjoining Photographs STARTS here
			 				  	    

			 				  	      " UNION SELECT '61','"+cf.Insp_id+"','" + cf.encode("View of Front Elevation Yard Facing Right") + "','" +3 + "','" + 1 + "'" +
			 				  	      " UNION SELECT '62','"+cf.Insp_id+"','" + cf.encode("View of Front Elevation Yard Facing Left") + "','" +3 + "','" + 2 + "'" +
			 				  	      " UNION SELECT '63','"+cf.Insp_id+"','" + cf.encode("View of Right Elevation Yard Facing Front") + "','" +3 + "','" + 3 + "'" +
			 				  	      " UNION SELECT '64','"+cf.Insp_id+"','" + cf.encode("View of Right Elevation Yard Facing Rear") + "','" +3 + "','" + 4 + "'" +
			 				  	      " UNION SELECT '65','"+cf.Insp_id+"','" + cf.encode("View of Left Elevation Yard Facing Rear") + "','" +3 + "','" + 5 + "'" +
			 				  	      " UNION SELECT '66','"+cf.Insp_id+"','" + cf.encode("View of Left Elevation Yard Facing Front ") + "','" +3 + "','" + 6 + "'" +
			 				  	      " UNION SELECT '67','"+cf.Insp_id+"','" + cf.encode("View of Rear Elevation Yard Facing Rear") + "','" +3 + "','" + 7 + "'" +
			 				  	      " UNION SELECT '68','"+cf.Insp_id+"','" + cf.encode("View of Rear Elevation Yard Facing Front") + "','" +3 + "','" + 8 + "'" +
			 				  	      " UNION SELECT '69','"+cf.Insp_id+"','" + cf.encode("View of Left Adjoining Premises") + "','" +3 + "','" + 9 + "'" +
			 				  	      " UNION SELECT '70','"+cf.Insp_id+"','" + cf.encode("View of Right Adjoining Premises") + "','" +3 + "','" + 10 + "'" +
			 				  	      " UNION SELECT '71','"+cf.Insp_id+"','" + cf.encode("Irregular Land Surface") + "','" +3 + "','" + 10 + "'" +
			 				  	      " UNION SELECT '72','"+cf.Insp_id+"','" + cf.encode("Soil Collapse Feature noted") + "','" +3 + "','" + 10 + "'" +
			 				  	      " UNION SELECT '73','"+cf.Insp_id+"','" + cf.encode(" Severe Cracking Noted") + "','" +3 + "','" + 10 + "'" +
			 				  	    // elevation  Ground and Adjoining Photographs ENDS here
			 				  	      
			  						// elevation  Internal  Photographs STARTS here
			 			  	  
			            				  	      
			 				  	      " UNION SELECT '74','"+cf.Insp_id+"','" + cf.encode("General View") + "','" + 4 + "','" + 1 + "'" +
			 				  	      " UNION SELECT '75','"+cf.Insp_id+"','" + cf.encode("Floor Separation") + "','" + 4 + "','" + 2 + "'" +
			 				  	      " UNION SELECT '76','"+cf.Insp_id+"','" + cf.encode("Cracked Floor Tile") + "','" + 4 + "','" + 4 + "'" +
			 				  	      " UNION SELECT '77','"+cf.Insp_id+"','" + cf.encode("Uneven Flooring") + "','" + 4 + "','" + 4 + "'" +
			 				  	      " UNION SELECT '78','"+cf.Insp_id+"','" + cf.encode("Lifting Floor Tile") + "','" + 4 + "','" + 4 + "'" +
			 				  	      " UNION SELECT '79','"+cf.Insp_id+"','" + cf.encode("General View of Flooring") + "','" + 4 + "','" + 4 + "'" +
			 				  	      " UNION SELECT '80','"+cf.Insp_id+"','" + cf.encode("Wall Cracking") + "','" + 4 + "','" + 5 + "'" +
			 				  	      " UNION SELECT '81','"+cf.Insp_id+"','" + cf.encode("General View of Wall Surfaces") + "','" + 4 + "','" + 5 + "'" +
			 				  	      " UNION SELECT '82','"+cf.Insp_id+"','" + cf.encode("Wall Damage noted") + "','" + 4 + "','" + 5 + "'" +
			 				  	      " UNION SELECT '83','"+cf.Insp_id+"','" + cf.encode("Wall Separate Noted") + "','" + 4 + "','" + 5 + "'" +
			 				  	      " UNION SELECT '84','"+cf.Insp_id+"','" + cf.encode("Off Plumb / Sticking Door") + "','" + 4 + "','" + 6 + "'" +
			 				  	      " UNION SELECT '85','"+cf.Insp_id+"','" + cf.encode("General View of door openings Inside") + "','" + 4 + "','" + 6 + "'" +
			 				  	      " UNION SELECT '86','"+cf.Insp_id+"','" + cf.encode("Wall Separation / Cracking") + "','" + 4 + "','" + 7 + "'" +
			 				  	      " UNION SELECT '87','"+cf.Insp_id+"','" + cf.encode("Binding Frames") + "','" + 4 + "','" + 9 + "'" +
			 				  	      " UNION SELECT '88','"+cf.Insp_id+"','" + cf.encode("General view of windows" )+ "','" + 4 + "','" + 10 + "'");
				 }*/	 	
			 }
			 catch(Exception e)
			 {
			 	
			 }
		}

	 private void start_import() {
		// TODO Auto-generated method stub
		 new Thread() {
				public void run() {
					try{
						
						if (getversioncodefromweb() == true) {
							System.out.println("insdget version");
							
							cf.total = 15;
							try
							{
								SoapObject chklogin = cf.Calling_WS1(cf.Insp_id,"UpdateMobileDB_GenHazards");
								System.out.println("chl "+chklogin);
								if (chklogin.equals("anyType{}")) {
									cf.ShowToast("Server is busy. Please try again later.",2);
									startActivity(new Intent(Import.this,HomeScreen.class));
								     
								} else {
									System.out.println("chl else");	
									Policyholder_Insert(chklogin); /*INSERTING DATA INTO POLICYHOLDER*/
									cf.total=50;
									GetAgentinformation();
									cf.total=75;
									GetCloudInfomation();
									cf.total=95;
									cf.total = 100;
									cf.usercheck = 4;
									
								}
								
							}
							catch (SocketTimeoutException s) {
								System.out.println("s "+s.getMessage());
								    cf.usercheck = 2;
									cf.total = 100;
									
							 } catch (NetworkErrorException n) {
								 System.out.println("n "+n.getMessage());
								    cf.usercheck = 2;
								    cf.total = 100;
								   
							 } catch (IOException io) {
								 System.out.println("io "+io.getMessage());
								    cf.usercheck = 2;
								    cf.total = 100;
								    
							 } catch (XmlPullParserException x) {
								 System.out.println("x "+x.getMessage());
								   cf.usercheck = 2;
								    cf.total = 100;
								    
							 }
		               	     catch (Exception e) {
		               	    	 System.out.println("e "+e.getMessage());
		               	    	    cf.usercheck = 3;
									cf.total = 100;
								
							}
							
					         
							
						}
					    else {
						  cf.total = 100;
					   }
					 }
					 catch (Exception e) {
						cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ Import.this +" "+" in the stage of(catch) importing  at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
					}
				}
		 }
		 .start();
	}
	 public void GetCloudInfomation() throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException  {
		// TODO Auto-generated method stub
		  
		 	String SRID,assignmentId,assignmentMasterId,inspectionPolicy_Id,priorAssignmentId,inspectionType,assignmentType,policyId,policyVersion,policyEndorsement,
			policyEffectiveDate,policyForm,policySystem,lob,structureCount,structureNumber,structureDescription,propertyAddress,propertyAddress2,propertyCity,
			propertyState,propertyCounty,propertyZip,insuredFirstName,insuredLastName,insuredHomePhone,insuredWorkPhone,insuredAlternatePhone,insuredEmail,
			insuredMailingAddress,insuredMailingAddress2,insuredMailingCity,insuredMailingState,insuredMailingZip,agencyID,agencyName,agencyPhone,agencyFax,agencyEmail,
			agencyPrincipalFirstName,agencyPrincipalLastName,agencyPrincipalEmail,agencyFEIN,agencyMailingAddress,agencyMailingAddress2,agencyMailingCity,
			agencyMailingState,agencyMailingZip,agentID,agentFirstName,agentLastName,agentEmail,previousInspectionDate,previousInspectionCompany,previousInspectorFirstName,
			previousInspectorLastName,previousInspectorLicense,yearBuilt,squareFootage,numberOfStories,construction,roofCovering,roofDeckAttachment,
			roofWallAttachment,roofShape,secondaryWaterResistance,openingCoverage,buildingType;
		 	SoapObject request = new SoapObject(cf.NAMESPACE,"LoadCloudInformation_New");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("InspectorID",cf.Insp_id);
			envelope.setOutputSoapObject(request);;
			HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
			androidHttpTransport.call(cf.NAMESPACE+"LoadCloudInformation_New",envelope);
			SoapObject cloudinfo = (SoapObject) envelope.getResponse();
			 
			 int Cnt = cloudinfo.getPropertyCount();
			 double Totaltmp = 0.0;
			 int countchk = Cnt;
			 Totaltmp = (20.00 / (double) countchk);
			 double temptot = Totaltmp;
			 int temptotal = cf.total;
			 
			cf.Create_Table(18);
			for(int i=0;i<cloudinfo.getPropertyCount();i++)
			{
				SoapObject temp_result=(SoapObject) cloudinfo.getProperty(i);
				SRID = (String.valueOf(temp_result.getProperty("SRID")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("SRID")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("SRID")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("SRID"));
				assignmentId = (String.valueOf(temp_result.getProperty("assignmentId")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("assignmentId")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("assignmentId")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("assignmentId"));
				assignmentMasterId = (String.valueOf(temp_result.getProperty("assignmentMasterId")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("assignmentMasterId")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("assignmentMasterId")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("assignmentMasterId"));
				inspectionPolicy_Id = (String.valueOf(temp_result.getProperty("inspectionPolicy_Id")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("inspectionPolicy_Id")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("inspectionPolicy_Id")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("inspectionPolicy_Id"));
				priorAssignmentId = (String.valueOf(temp_result.getProperty("priorAssignmentId")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("priorAssignmentId")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("priorAssignmentId")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("priorAssignmentId"));
				inspectionType = (String.valueOf(temp_result.getProperty("inspectionType")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("inspectionType")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("inspectionType")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("inspectionType"));
				assignmentType = (String.valueOf(temp_result.getProperty("assignmentType")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("assignmentType")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("assignmentType")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("assignmentType"));
				policyId = (String.valueOf(temp_result.getProperty("policyId")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("policyId")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("policyId")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("policyId"));
				policyVersion = (String.valueOf(temp_result.getProperty("policyVersion")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("policyVersion")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("policyVersion")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("policyVersion"));
				policyEndorsement = (String.valueOf(temp_result.getProperty("policyEndorsement")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("policyEndorsement")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("policyEndorsement")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("policyEndorsement"));
				policyEffectiveDate = (String.valueOf(temp_result.getProperty("policyEffectiveDate")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("policyEffectiveDate")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("policyEffectiveDate")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("policyEffectiveDate"));
				policyForm = (String.valueOf(temp_result.getProperty("policyForm")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("policyForm")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("policyForm")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("policyForm"));
				policySystem = (String.valueOf(temp_result.getProperty("policySystem")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("policySystem")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("policySystem")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("policySystem"));
				lob = (String.valueOf(temp_result.getProperty("lob")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("lob")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("lob")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("lob"));
				structureCount = (String.valueOf(temp_result.getProperty("structureCount")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("structureCount")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("structureCount")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("structureCount"));
				structureNumber = (String.valueOf(temp_result.getProperty("structureNumber")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("structureNumber")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("structureNumber")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("structureNumber"));
				structureDescription = (String.valueOf(temp_result.getProperty("structureDescription")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("structureDescription")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("structureDescription")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("structureDescription"));
				propertyAddress = (String.valueOf(temp_result.getProperty("propertyAddress")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("propertyAddress")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("propertyAddress")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("propertyAddress"));
				propertyAddress2 = (String.valueOf(temp_result.getProperty("propertyAddress2")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("propertyAddress2")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("propertyAddress2")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("propertyAddress2"));
				propertyCity = (String.valueOf(temp_result.getProperty("propertyCity")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("propertyCity")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("propertyCity")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("propertyCity"));
				propertyState = (String.valueOf(temp_result.getProperty("propertyState")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("propertyState")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("propertyState")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("propertyState"));
				propertyCounty = (String.valueOf(temp_result.getProperty("propertyCounty")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("propertyCounty")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("propertyCounty")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("propertyCounty"));
				propertyZip = (String.valueOf(temp_result.getProperty("propertyZip")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("propertyZip")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("propertyZip")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("propertyZip"));
				insuredFirstName = (String.valueOf(temp_result.getProperty("insuredFirstName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("insuredFirstName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("insuredFirstName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("insuredFirstName"));
				insuredLastName = (String.valueOf(temp_result.getProperty("insuredLastName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("insuredLastName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("insuredLastName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("insuredLastName"));
				insuredHomePhone = (String.valueOf(temp_result.getProperty("insuredHomePhone")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("insuredHomePhone")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("insuredHomePhone")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("insuredHomePhone"));
				insuredWorkPhone = (String.valueOf(temp_result.getProperty("insuredWorkPhone")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("insuredWorkPhone")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("insuredWorkPhone")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("insuredWorkPhone"));
				insuredAlternatePhone = (String.valueOf(temp_result.getProperty("insuredAlternatePhone")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("insuredAlternatePhone")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("insuredAlternatePhone")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("insuredAlternatePhone"));
				insuredEmail = (String.valueOf(temp_result.getProperty("insuredEmail")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("insuredEmail")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("insuredEmail")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("insuredEmail"));
				insuredMailingAddress = (String.valueOf(temp_result.getProperty("insuredMailingAddress")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("insuredMailingAddress")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("insuredMailingAddress")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("insuredMailingAddress"));
				insuredMailingAddress2 = (String.valueOf(temp_result.getProperty("insuredMailingAddress2")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("insuredMailingAddress2")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("insuredMailingAddress2")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("insuredMailingAddress2"));
				insuredMailingCity = (String.valueOf(temp_result.getProperty("insuredMailingCity")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("insuredMailingCity")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("insuredMailingCity")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("insuredMailingCity"));
				insuredMailingState = (String.valueOf(temp_result.getProperty("insuredMailingState")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("insuredMailingState")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("insuredMailingState")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("insuredMailingState"));
				insuredMailingZip = (String.valueOf(temp_result.getProperty("insuredMailingZip")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("insuredMailingZip")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("insuredMailingZip")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("insuredMailingZip"));
				agencyID = (String.valueOf(temp_result.getProperty("agencyID")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agencyID")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agencyID")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agencyID"));
				agencyName = (String.valueOf(temp_result.getProperty("agencyName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agencyName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agencyName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agencyName"));
				agencyPhone = (String.valueOf(temp_result.getProperty("agencyPhone")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agencyPhone")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agencyPhone")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agencyPhone"));
				agencyFax = (String.valueOf(temp_result.getProperty("agencyFax")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agencyFax")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agencyFax")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agencyFax"));
				agencyEmail = (String.valueOf(temp_result.getProperty("agencyEmail")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agencyEmail")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agencyEmail")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agencyEmail"));
				agencyPrincipalFirstName = (String.valueOf(temp_result.getProperty("agencyPrincipalFirstName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agencyPrincipalFirstName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agencyPrincipalFirstName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agencyPrincipalFirstName"));
				agencyPrincipalLastName = (String.valueOf(temp_result.getProperty("agencyPrincipalLastName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agencyPrincipalLastName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agencyPrincipalLastName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agencyPrincipalLastName"));
				agencyPrincipalEmail = (String.valueOf(temp_result.getProperty("agencyPrincipalEmail")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agencyPrincipalEmail")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agencyPrincipalEmail")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agencyPrincipalEmail"));
				agencyFEIN = (String.valueOf(temp_result.getProperty("agencyFEIN")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agencyFEIN")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agencyFEIN")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agencyFEIN"));
				agencyMailingAddress = (String.valueOf(temp_result.getProperty("agencyMailingAddress")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agencyMailingAddress")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agencyMailingAddress")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agencyMailingAddress"));
				agencyMailingAddress2 = (String.valueOf(temp_result.getProperty("agencyMailingAddress2")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agencyMailingAddress2")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agencyMailingAddress2")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agencyMailingAddress2"));
				agencyMailingCity = (String.valueOf(temp_result.getProperty("agencyMailingCity")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agencyMailingCity")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agencyMailingCity")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agencyMailingCity"));
				agencyMailingState = (String.valueOf(temp_result.getProperty("agencyMailingState")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agencyMailingState")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agencyMailingState")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agencyMailingState"));
				agencyMailingZip = (String.valueOf(temp_result.getProperty("agencyMailingZip")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agencyMailingZip")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agencyMailingZip")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agencyMailingZip"));
				agentID = (String.valueOf(temp_result.getProperty("agentID")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agentID")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agentID")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agentID"));
				agentFirstName = (String.valueOf(temp_result.getProperty("agentFirstName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agentFirstName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agentFirstName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agentFirstName"));
				agentLastName = (String.valueOf(temp_result.getProperty("agentLastName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agentLastName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agentLastName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agentLastName"));
				agentEmail = (String.valueOf(temp_result.getProperty("agentEmail")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agentEmail")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agentEmail")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agentEmail"));
				previousInspectionDate = (String.valueOf(temp_result.getProperty("previousInspectionDate")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("previousInspectionDate")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("previousInspectionDate")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("previousInspectionDate"));
				previousInspectionCompany = (String.valueOf(temp_result.getProperty("previousInspectionCompany")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("previousInspectionCompany")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("previousInspectionCompany")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("previousInspectionCompany"));
				previousInspectorFirstName = (String.valueOf(temp_result.getProperty("previousInspectorFirstName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("previousInspectorFirstName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("previousInspectorFirstName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("previousInspectorFirstName"));
				previousInspectorLastName = (String.valueOf(temp_result.getProperty("previousInspectorLastName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("previousInspectorLastName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("previousInspectorLastName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("previousInspectorLastName"));
				previousInspectorLicense = (String.valueOf(temp_result.getProperty("previousInspectorLicense")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("previousInspectorLicense")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("previousInspectorLicense")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("previousInspectorLicense"));
				yearBuilt = (String.valueOf(temp_result.getProperty("yearBuilt")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("yearBuilt")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("yearBuilt")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("yearBuilt"));
				squareFootage = (String.valueOf(temp_result.getProperty("squareFootage")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("squareFootage")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("squareFootage")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("squareFootage"));
				numberOfStories = (String.valueOf(temp_result.getProperty("numberOfStories")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("numberOfStories")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("numberOfStories")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("numberOfStories"));
				construction = (String.valueOf(temp_result.getProperty("construction")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("construction")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("construction")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("construction"));
				roofCovering = (String.valueOf(temp_result.getProperty("roofCovering")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("roofCovering")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("roofCovering")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("roofCovering"));
				roofDeckAttachment = (String.valueOf(temp_result.getProperty("roofDeckAttachment")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("roofDeckAttachment")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("roofDeckAttachment")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("roofDeckAttachment"));
				roofWallAttachment = (String.valueOf(temp_result.getProperty("roofWallAttachment")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("roofWallAttachment")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("roofWallAttachment")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("roofWallAttachment"));
				roofShape = (String.valueOf(temp_result.getProperty("roofShape")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("roofShape")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("roofShape")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("roofShape"));
				secondaryWaterResistance = (String.valueOf(temp_result.getProperty("secondaryWaterResistance")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("secondaryWaterResistance")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("secondaryWaterResistance")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("secondaryWaterResistance"));
				openingCoverage = (String.valueOf(temp_result.getProperty("openingCoverage")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("openingCoverage")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("openingCoverage")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("openingCoverage"));
				buildingType = (String.valueOf(temp_result.getProperty("buildingType")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("buildingType")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("buildingType")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("buildingType"));
				try
				{
					Cursor c2=cf.SelectTablefunction(cf.Cloud_table," where SRID ='"+SRID+"'");
					if(c2.getCount()>0)
					{
						cf.gch_db.execSQL("Delete from "+cf.Cloud_table+" where SRID='"+SRID+"' ");
					}
					
					
					cf.gch_db.execSQL("INSERT INTO "+cf.Cloud_table +"(SRID ,  assignmentId , assignmentMasterId , inspectionPolicy_Id , priorAssignmentId , inspectionType , assignmentType , policyId , policyVersion , policyEndorsement , policyEffectiveDate , policyForm , policySystem , lob , structureCount , structureNumber " +
					", structureDescription , propertyAddress , propertyAddress2 , propertyCity , propertyState , propertyCounty , propertyZip , insuredFirstName , " +
					"insuredLastName , insuredHomePhone , insuredWorkPhone , insuredAlternatePhone , insuredEmail , insuredMailingAddress , insuredMailingAddress2 , " +
					"insuredMailingCity , insuredMailingState , insuredMailingZip , agencyID , agencyName , agencyPhone , agencyFax , agencyEmail , agencyPrincipalFirstName " +
					", agencyPrincipalLastName , agencyPrincipalEmail , agencyFEIN , agencyMailingAddress , agencyMailingAddress2 , agencyMailingCity , agencyMailingState ," +
					" agencyMailingZip , agentID , agentFirstName , agentLastName , agentEmail , previousInspectionDate , previousInspectionCompany , " +
					"previousInspectorFirstName , previousInspectorLastName , previousInspectorLicense , yearBuilt , squareFootage , numberOfStories , construction , " +
					"roofCovering , roofDeckAttachment , roofWallAttachment , roofShape , secondaryWaterResistance , openingCoverage , buildingType) VALUES " +
					"('"+SRID +"','"+  assignmentId +"','"+ assignmentMasterId +"','"+ inspectionPolicy_Id +"','"+ priorAssignmentId +"','"+ inspectionType +"','"+
					assignmentType +"','"+ policyId +"','"+ policyVersion +"','"+ policyEndorsement +"','"+ policyEffectiveDate +"','"+ policyForm +"','"+ policySystem 
					+"','"+ lob +"','"+ structureCount +"','"+ structureNumber +"','"+ structureDescription +"','"+ propertyAddress +"','"+ propertyAddress2 +"','"+ 
					propertyCity +"','"+ propertyState +"','"+ propertyCounty +"','"+ propertyZip +"','"+ insuredFirstName +"','"+ insuredLastName +"','"+ insuredHomePhone 
					+"','"+ insuredWorkPhone +"','"+ insuredAlternatePhone +"','"+ insuredEmail +"','"+ insuredMailingAddress +"','"+ insuredMailingAddress2 +"','"+ 
					insuredMailingCity +"','"+ insuredMailingState +"','"+ insuredMailingZip +"','"+ agencyID +"','"+ agencyName +"','"+ agencyPhone +"','"+ agencyFax +"','"
					+ agencyEmail +"','"+ agencyPrincipalFirstName +"','"+ agencyPrincipalLastName +"','"+ agencyPrincipalEmail +"','"+ agencyFEIN +"','"+ agencyMailingAddress +
					"','"+ agencyMailingAddress2 +"','"+ agencyMailingCity +"','"+ agencyMailingState +"','"+ agencyMailingZip +"','"+ agentID +"','"+ agentFirstName +"','"+
					agentLastName +"','"+ agentEmail +"','"+ previousInspectionDate +"','"+ previousInspectionCompany +"','"+ previousInspectorFirstName +"','"+ 
					previousInspectorLastName +"','"+ previousInspectorLicense +"','"+ yearBuilt +"','"+ squareFootage +"','"+ numberOfStories +"','"+ construction +"','"+
					roofCovering +"','"+ roofDeckAttachment +"','"+ roofWallAttachment +"','"+ roofShape +"','"+ secondaryWaterResistance +"','"+ openingCoverage +"','"+ 
					buildingType+"')");
				
				}
				catch(Exception e)
				{
					cf.Error_LogFile_Creation("Error  while create or select or delete, or insert the data in the cloud information in import ");
				}
				if (cf.total <= 95) {

					cf.total = temptotal + (int) (Totaltmp);
				} else {
					cf.total = 95;
				}
				Totaltmp += temptot;
				
			}
			
	}
	public void GetAgentinformation() throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException {
		// TODO Auto-generated method stub
		    
			String SRID,AgencyName,AgentName,AgentAddress,AgentAddress2,AgentCity,AgentCounty,AgentRole,AgentState,AgentZip,AgentOffPhone,AgentContactPhone,AgentFax,AgentEmail,AgentWebSite;
		 	SoapObject request = new SoapObject(cf.NAMESPACE,"LoadAgentInformation_New");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("InspectionID",cf.Insp_id);
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
			androidHttpTransport.call(cf.NAMESPACE+"LoadAgentInformation_New",envelope);
			SoapObject agentinfo = (SoapObject) envelope.getResponse();
			
			 int Cnt = agentinfo.getPropertyCount();
			 double Totaltmp = 0.0;
			 int countchk = Cnt;
			 Totaltmp = (25.00 / (double) countchk);
			 double temptot = Totaltmp;
			 int temptotal = cf.total;
			cf.Create_Table(17);
			for(int i=0;i<agentinfo.getPropertyCount();i++)
			{
				SoapObject temp_result=(SoapObject) agentinfo.getProperty(i);
				SRID = (String.valueOf(temp_result.getProperty("SRID")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("SRID")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("SRID")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("SRID"));
				AgencyName=(String.valueOf(temp_result.getProperty("AgencyName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgencyName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgencyName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgencyName"));
				AgentName=(String.valueOf(temp_result.getProperty("AgentName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentName"));
				AgentAddress=(String.valueOf(temp_result.getProperty("AgentAddress")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentAddress")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentAddress")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentAddress"));
				AgentAddress2=(String.valueOf(temp_result.getProperty("AgentAddress2")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentAddress2")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentAddress2")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentAddress2"));
				AgentCity=(String.valueOf(temp_result.getProperty("AgentCity")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentCity")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentCity")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentCity"));
				AgentCounty=(String.valueOf(temp_result.getProperty("AgentCounty")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentCounty")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentCounty")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentCounty"));
				AgentRole=(String.valueOf(temp_result.getProperty("AgentRole")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentRole")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentRole")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentRole"));
				AgentState=(String.valueOf(temp_result.getProperty("AgentState")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentState")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentState")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentState"));
				AgentZip=(String.valueOf(temp_result.getProperty("AgentZip")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentZip")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentZip")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentZip"));
				AgentOffPhone=(String.valueOf(temp_result.getProperty("AgentOffPhone")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentOffPhone")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentOffPhone")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentOffPhone"));
				AgentContactPhone=(String.valueOf(temp_result.getProperty("AgentContactPhone")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentContactPhone")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentContactPhone")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentContactPhone"));
				AgentFax=(String.valueOf(temp_result.getProperty("AgentFax")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentFax")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentFax")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentFax"));
				AgentEmail=(String.valueOf(temp_result.getProperty("AgentEmail")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentEmail")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentEmail")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentEmail"));
				AgentWebSite=(String.valueOf(temp_result.getProperty("AgentWebSite")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentWebSite")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentWebSite")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentWebSite"));
				try
				{
					Cursor c2=cf.SelectTablefunction(cf.Agent_tabble," where GCH_AI_SRID ='"+SRID+"'");
					if(c2.getCount()==0)
					{
						cf.gch_db.execSQL("Insert into "+cf.Agent_tabble+" (GCH_AI_SRID,GCH_AI_AgencyName,GCH_AI_AgentName,GCH_AI_AgentAddress,GCH_AI_AgentAddress2,GCH_AI_AgentCity,GCH_AI_AgentCounty,GCH_AI_AgentRole,GCH_AI_AgentState,GCH_AI_AgentZip,GCH_AI_AgentOffPhone,GCH_AI_AgentContactPhone,GCH_AI_AgentFax,GCH_AI_AgentEmail,GCH_AI_AgentWebSite) values" +
								"('"+SRID+"','"+AgencyName+"','"+AgentName+"','"+AgentAddress+"','"+AgentAddress2+"','"+AgentCity+"','"+AgentCounty+"','"+AgentRole+"','"+AgentState+"','"+AgentZip+"','"+AgentOffPhone+"','"+AgentContactPhone+"','"+AgentFax+"','"+AgentEmail+"','"+AgentWebSite+"') ");
					}
					else if(c2.getCount()>=1)
					{
						cf.gch_db.execSQL("UPDATE "+cf.Agent_tabble+" SET GCH_AI_SRID='"+SRID+"',GCH_AI_AgencyName='"+AgencyName+"',GCH_AI_AgentName='"+AgentName+"',GCH_AI_AgentAddress='"+AgentAddress+"',GCH_AI_AgentAddress2='"+AgentAddress2+"'," +
						"GCH_AI_AgentCity='"+AgentCity+"',GCH_AI_AgentCounty='"+AgentCounty+"',GCH_AI_AgentRole='"+AgentRole+"',GCH_AI_AgentState='"+AgentState+"',GCH_AI_AgentZip='"+AgentZip+"',GCH_AI_AgentOffPhone='"+AgentOffPhone+"'," +
						"GCH_AI_AgentContactPhone='"+AgentContactPhone+"',GCH_AI_AgentFax='"+AgentFax+"',GCH_AI_AgentEmail='"+AgentEmail+"',GCH_AI_AgentWebSite='"+AgentWebSite+"' WHERE GCH_AI_SRID='"+SRID+"'");
					}
				}
				catch(Exception e)
				{
					cf.Error_LogFile_Creation("Error  while create or select or delete, or insert the data in the cloud information in import ");
				}
				if (cf.total <= 75) {

					cf.total = temptotal + (int) (Totaltmp);
				} else {
					cf.total = 75;
				}
				Totaltmp += temptot;
				
			}
	}
	
	public void Policyholder_Insert(SoapObject objInsert) throws NetworkErrorException,
		IOException, XmlPullParserException, SocketTimeoutException {
				 cf.Create_Table(3);
		 int Cnt = objInsert.getPropertyCount();
		 double Totaltmp = 0.0;
		 int countchk = Cnt;
		 Totaltmp = (35.00 / (double) countchk);
		 double temptot = Totaltmp;
		 int temptotal = cf.total;
		 for (int i = 0; i < Cnt; i++) {
			 
			 SoapObject obj = (SoapObject) objInsert.getProperty(i);
			 try {
				 cf.HomeId = (String.valueOf(obj.getProperty("SRID")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("SRID")).equals("NA"))?"":(String.valueOf(obj.getProperty("SRID")).equals("N/A"))?"":String.valueOf(obj.getProperty("SRID"));
				 cf.InspectorId = (String.valueOf(obj.getProperty("InspectorId")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectorId")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectorId")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectorId"));
				 cf.ScheduledDate = (String.valueOf(obj.getProperty("ScheduledDate")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("ScheduledDate")).equals("NA"))?"":(String.valueOf(obj.getProperty("ScheduledDate")).equals("N/A"))?"":String.valueOf(obj.getProperty("ScheduledDate"));
				 cf.ScheduledCreatedDate = (String.valueOf(obj.getProperty("ScheduleCreatedDate")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("ScheduleCreatedDate")).equals("NA"))?"":(String.valueOf(obj.getProperty("ScheduleCreatedDate")).equals("N/A"))?"":String.valueOf(obj.getProperty("ScheduleCreatedDate"));
				 cf.AssignedDate = (String.valueOf(obj.getProperty("AssignedDate")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("AssignedDate")).equals("NA"))?"":(String.valueOf(obj.getProperty("AssignedDate")).equals("N/A"))?"":String.valueOf(obj.getProperty("AssignedDate"));
				 cf.Schedulecomments = (String.valueOf(obj.getProperty("InspectionComment")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionComment")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionComment")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionComment"));
				 cf.InspectionStartTime = (String.valueOf(obj.getProperty("InspectionStartTime")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionStartTime")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionStartTime")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionStartTime"));
				 cf.InspectionEndTime = (String.valueOf(obj.getProperty("InspectionEndTime")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionEndTime")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionEndTime")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionEndTime"));
				 cf.PH_Fname = (String.valueOf(obj.getProperty("FirstName")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("FirstName")).equals("NA"))?"":(String.valueOf(obj.getProperty("FirstName")).equals("N/A"))?"":String.valueOf(obj.getProperty("FirstName"));
				 cf.PH_Lname = (String.valueOf(obj.getProperty("LastName")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("LastName")).equals("NA"))?"":(String.valueOf(obj.getProperty("LastName")).equals("N/A"))?"":String.valueOf(obj.getProperty("LastName"));
				 cf.PH_Address1 = (String.valueOf(obj.getProperty("Address1")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Address1")).equals("NA"))?"":(String.valueOf(obj.getProperty("Address1")).equals("N/A"))?"":String.valueOf(obj.getProperty("Address1"));
				 cf.PH_Address2 = (String.valueOf(obj.getProperty("Address2")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Address2")).equals("NA"))?"":(String.valueOf(obj.getProperty("Address2")).equals("N/A"))?"":String.valueOf(obj.getProperty("Address2"));
				 cf.PH_City = (String.valueOf(obj.getProperty("City")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("City")).equals("NA"))?"":(String.valueOf(obj.getProperty("City")).equals("N/A"))?"":String.valueOf(obj.getProperty("City"));
				 cf.PH_State = (String.valueOf(obj.getProperty("State")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("State")).equals("NA"))?"":(String.valueOf(obj.getProperty("State")).equals("N/A"))?"":String.valueOf(obj.getProperty("State"));
				 cf.PH_Zip = (String.valueOf(obj.getProperty("Zip")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Zip")).equals("NA"))?"":(String.valueOf(obj.getProperty("Zip")).equals("N/A"))?"":String.valueOf(obj.getProperty("Zip"));
				 cf.PH_County = (String.valueOf(obj.getProperty("Country")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Country")).equals("NA"))?"":(String.valueOf(obj.getProperty("Country")).equals("N/A"))?"":String.valueOf(obj.getProperty("Country"));
				 cf.PH_Policyno = (String.valueOf(obj.getProperty("OwnerPolicyNo")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("OwnerPolicyNo")).equals("NA"))?"":(String.valueOf(obj.getProperty("OwnerPolicyNo")).equals("N/A"))?"":String.valueOf(obj.getProperty("OwnerPolicyNo"));
				 cf.PH_HPhone = (String.valueOf(obj.getProperty("HomePhone")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("HomePhone")).equals("NA"))?"":(String.valueOf(obj.getProperty("HomePhone")).equals("N/A"))?"":String.valueOf(obj.getProperty("HomePhone"));
				 cf.PH_CPhone = (String.valueOf(obj.getProperty("CellPhone")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("CellPhone")).equals("NA"))?"":(String.valueOf(obj.getProperty("CellPhone")).equals("N/A"))?"":String.valueOf(obj.getProperty("CellPhone"));
				 cf.PH_WPhone = (String.valueOf(obj.getProperty("WorkPhone")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("WorkPhone")).equals("NA"))?"":(String.valueOf(obj.getProperty("WorkPhone")).equals("N/A"))?"":String.valueOf(obj.getProperty("WorkPhone"));
				 cf.PH_Email = (String.valueOf(obj.getProperty("Email")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Email")).equals("NA"))?"":(String.valueOf(obj.getProperty("Email")).equals("N/A"))?"":String.valueOf(obj.getProperty("Email"));
				 cf.PH_InsuranceCompany = (String.valueOf(obj.getProperty("InsuranceCompany")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InsuranceCompany")).equals("NA"))?"":(String.valueOf(obj.getProperty("InsuranceCompany")).equals("N/A"))?"":String.valueOf(obj.getProperty("InsuranceCompany"));
				 cf.PH_InspectionTypeId = (String.valueOf(obj.getProperty("InspectionTypeId")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionTypeId")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionTypeId")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionTypeId"));
				 cf.PH_Status = (String.valueOf(obj.getProperty("Status")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Status")).equals("NA"))?"":(String.valueOf(obj.getProperty("Status")).equals("N/A"))?"":String.valueOf(obj.getProperty("Status"));
				 cf.PH_SubStatus = (String.valueOf(obj.getProperty("SubStatusID")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("SubStatusID")).equals("NA"))?"":(String.valueOf(obj.getProperty("SubStatusID")).equals("N/A"))?"":String.valueOf(obj.getProperty("SubStatusID"));
				 cf.PH_Inspectionfees = (String.valueOf(obj.getProperty("s_InspFees")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("s_InspFees")).equals("NA"))?"":(String.valueOf(obj.getProperty("s_InspFees")).equals("N/A"))?"":String.valueOf(obj.getProperty("s_InspFees"));
				 cf.yearbuilt = (String.valueOf(obj.getProperty("YearBuilt")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("YearBuilt")).equals("NA"))?"":(String.valueOf(obj.getProperty("YearBuilt")).equals("N/A"))?"":String.valueOf(obj.getProperty("YearBuilt"));
				 contactperson = (String.valueOf(obj.getProperty("ContactPerson")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("ContactPerson")).equals("NA"))?"":(String.valueOf(obj.getProperty("ContactPerson")).equals("N/A"))?"":String.valueOf(obj.getProperty("ContactPerson"));
				 buidingsize = (String.valueOf(obj.getProperty("BuildingSize")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("BuildingSize")).equals("NA"))?"":(String.valueOf(obj.getProperty("BuildingSize")).equals("N/A"))?"":String.valueOf(obj.getProperty("BuildingSize"));
				 cf.PH_Noofstories = (String.valueOf(obj.getProperty("Nstories")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Nstories")).equals("NA"))?"":(String.valueOf(obj.getProperty("Nstories")).equals("N/A"))?"":String.valueOf(obj.getProperty("Nstories"));
				 Website = (String.valueOf(obj.getProperty("Website")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Website")).equals("NA"))?"":(String.valueOf(obj.getProperty("Website")).equals("N/A"))?"":String.valueOf(obj.getProperty("Website"));
				 /*CHECKING POLICYHOLDER TABLE USING QUERY*/
				 Cursor cur = cf.SelectTablefunction(cf.policyholder,
							" where GCH_PH_InspectorId='" + cf.encode(cf.InspectorId) + "' and GCH_PH_SRID='"+cf.encode(cf.HomeId)+"'");
				 int rws = cur.getCount();
				 if(rws >= 1)
				 {
					 cur.moveToFirst();
							 /*UPDATING THE TABLE - POLICYHOLDER*/
					 if(cur.getString(cur.getColumnIndex("GCH_PH_IsInspected")).equals("0")){
							 cf.gch_db.execSQL("UPDATE "
										+ cf.policyholder
										+ " SET  GCH_PH_FirstName='"+ cf.encode(cf.PH_Fname)+"'," +
										       " GCH_PH_LastName ='"+ cf.encode(cf.PH_Lname)+"'," +
										       " GCH_PH_Address1='"+ cf.encode(cf.PH_Address1)+"'," +
										       " GCH_PH_Address2='"+ cf.encode(cf.PH_Address2)+"'," +
										       " GCH_PH_City='"+ cf.encode(cf.PH_City)+"'," +
										       " GCH_PH_Zip='"+ cf.encode(cf.PH_Zip)+"'," +
										       " GCH_PH_State='"+ cf.encode(cf.PH_State)+"', "+
										       " GCH_PH_County='"+ cf.encode(cf.PH_County)+"',"+
										       " GCH_PH_Policyno='"+ cf.encode(cf.PH_Policyno)+"',"+
										       " GCH_PH_Inspectionfees='"+ cf.encode(cf.PH_Inspectionfees)+"',"+
										       " GCH_PH_InsuranceCompany='"+ cf.encode(cf.PH_InsuranceCompany)+"',"+
										       " GCH_PH_HomePhone='"+ cf.encode(cf.PH_HPhone)+"',"+
										       " GCH_PH_WorkPhone='"+ cf.encode(cf.PH_WPhone)+"',"+
										       " GCH_PH_CellPhone='"+ cf.encode(cf.PH_CPhone)+"', "+
										       " GCH_PH_Email='"+ cf.encode(cf.PH_Email)+"', "+
										       " GCH_PH_EmailChkbx='"+ cf.encode(cf.PH_EmailChk)+"', "+
										       " GCH_PH_InspectionTypeId='"+ cf.encode(cf.PH_InspectionTypeId)+"', "+
										       " GCH_PH_IsInspected='"+ cf.encode(cf.PH_IsInspected)+"',"+
										       " GCH_PH_IsUploaded='"+ cf.encode(cf.PH_IsUploaded)+"',"+
										       " GCH_PH_Status='"+ cf.encode(cf.PH_Status)+"',"+
										       " GCH_PH_SubStatus='"+ cf.encode(cf.PH_SubStatus)+"'," +
										       " GCH_Schedule_ScheduledDate='"+ cf.encode(cf.ScheduledDate)+"'," +
										       " GCH_Schedule_InspectionStartTime='"+cf.encode(cf.InspectionStartTime)+"'," +
										       " GCH_Schedule_InspectionEndTime='"+cf.encode(cf.InspectionEndTime)+"'," +
										       " GCH_Schedule_Comments='"+cf.encode(cf.Schedulecomments)+"',"+
										       " GCH_Schedule_ScheduleCreatedDate='"+cf.encode(cf.ScheduledCreatedDate)+"',"+
										       " GCH_Schedule_AssignedDate='"+cf.encode(cf.AssignedDate)+"',"+
										       " GCH_ScheduleFlag=0 ," +
										       " GCH_YearBuilt='"+cf.yearbuilt+"',"+
										       " GCH_ContactPerson='"+cf.encode(contactperson)+"',BuidingSize='"+cf.decode(buidingsize)+"',GCH_PH_NOOFSTORIES='"+cf.decode(cf.PH_Noofstories)+"',GCH_PH_WEBSITE='"+cf.decode(Website)+"' "+
										       " where GCH_PH_SRID='" + cf.encode(cf.HomeId)+ "' and GCH_PH_InspectorId='"+ cf.encode(cf.InspectorId)+"'");
					 } }
				 else
				 {
					 /*INSERTING THE TABLE - POLICYHOLDER*/
					
					 cf.gch_db.execSQL("INSERT INTO "
								+ cf.policyholder
								+ " (GCH_PH_InspectorId,GCH_PH_SRID,GCH_PH_FirstName,GCH_PH_LastName,GCH_PH_Address1,GCH_PH_Address2," +
								" GCH_PH_City,GCH_PH_Zip,GCH_PH_State,GCH_PH_County,GCH_PH_Policyno,GCH_PH_Inspectionfees,GCH_PH_InsuranceCompany," +
								" GCH_PH_HomePhone,GCH_PH_WorkPhone,GCH_PH_CellPhone,GCH_PH_Email,GCH_PH_EmailChkbx,GCH_PH_InspectionTypeId," +
								" GCH_PH_IsInspected,GCH_PH_IsUploaded,GCH_PH_Status,GCH_PH_SubStatus,GCH_Schedule_ScheduledDate,GCH_Schedule_InspectionStartTime,"+
							  " GCH_Schedule_InspectionEndTime,GCH_Schedule_Comments,GCH_Schedule_ScheduleCreatedDate,GCH_Schedule_AssignedDate,GCH_ScheduleFlag,GCH_YearBuilt,GCH_ContactPerson,BuidingSize,GCH_PH_NOOFSTORIES,GCH_PH_WEBSITE)"
								+ " VALUES ('"+ cf.encode(cf.InspectorId)+"','"
								+ cf.encode(cf.HomeId)+"','"
								+ cf.encode(cf.PH_Fname)+"','"
								+ cf.encode(cf.PH_Lname)+"','"
								+ cf.encode(cf.PH_Address1)+"','"
								+ cf.encode(cf.PH_Address2)+"','"
								+ cf.encode(cf.PH_City)+"','"
								+ cf.encode(cf.PH_Zip)+"','"
								+ cf.encode(cf.PH_State)+"','"
								+ cf.encode(cf.PH_County)+"','"
								+ cf.encode(cf.PH_Policyno)+"','"
								+ cf.encode(cf.PH_Inspectionfees)+"','"
								+ cf.encode(cf.PH_InsuranceCompany)+"','"
								+ cf.encode(cf.PH_HPhone)+"','"
								+ cf.encode(cf.PH_WPhone)+"','"
								+ cf.encode(cf.PH_CPhone)+"','"
								+ cf.encode(cf.PH_Email)+"','"
								+ cf.encode(cf.PH_EmailChk)+"','"
								+ cf.encode(cf.PH_InspectionTypeId)+"','"
								+ cf.encode(cf.PH_IsInspected)+"','"
								+ cf.encode(cf.PH_IsUploaded)+"','"
								+ cf.encode(cf.PH_Status)+"','"
								+ cf.encode(cf.PH_SubStatus)+"','"
								+ cf.encode(cf.ScheduledDate)+"','"+cf.encode(cf.InspectionStartTime)+"','"+cf.encode(cf.InspectionEndTime)+"','"
								+ cf.encode(cf.Schedulecomments)+"','"+cf.encode(cf.ScheduledCreatedDate)+"','"+cf.encode(cf.AssignedDate)+"','0','"+cf.encode(cf.yearbuilt)+"','"+cf.encode(contactperson)+"','"+cf.encode(buidingsize)+"','"+cf.encode(cf.PH_Noofstories)+"','"+cf.encode(Website)+"')");
					
				 }
			 }
			 catch(Exception e)
			 {
				 cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ Import.this +" "+" in the stage of(catch) retrieving policyholder details from webservice at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			 }
			 GetAdditionalInfo(cf.HomeId,cf.InspectorId);
			 if (cf.total <= 51) {

					cf.total = temptotal + (int) (Totaltmp);
				} else {
					cf.total = 50;
				}
				Totaltmp += temptot;
				
		 }
	 }
	 private void GetAdditionalInfo(String homeId, String inspectorId) {
		// TODO Auto-generated method stub
		try{  
		           cf.Create_Table(19);	
		     
		    	    SoapObject request = new SoapObject(cf.NAMESPACE,"GetAdditionalInfo");
					SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
					envelope.dotNet = true;
					request.addProperty("SRID",homeId);
					request.addProperty("InspectorID",inspectorId);
					envelope.setOutputSoapObject(request);;
					HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
					androidHttpTransport.call(cf.NAMESPACE+"GetAdditionalInfo",envelope);
					SoapObject additonalinfo = (SoapObject) envelope.getResponse();
					String rechk = String.valueOf(additonalinfo.getProperty("IsRecord"));
					if (rechk.equals("true")) {
					
						for(int i=0;i<additonalinfo.getPropertyCount();i++)
					    {
						     String usrtyp = String.valueOf(additonalinfo.getProperty("UserTypeName"));
							 String email = String.valueOf(additonalinfo.getProperty("ContactEmail"));
							 String phnnum = String.valueOf(additonalinfo.getProperty("PhoneNumber"));
							 String mblnum = String.valueOf(additonalinfo.getProperty("MobileNumber"));
							 String timetocall = String.valueOf(additonalinfo.getProperty("BestTimetoCallYou"));
							 String fchoice = String.valueOf(additonalinfo.getProperty("FirstChoice"));
							 String schoice = String.valueOf(additonalinfo.getProperty("SecondChoice"));
							 String tchoice = String.valueOf(additonalinfo.getProperty("ThirdChoice"));
							 String fdcomments = String.valueOf(additonalinfo.getProperty("FeedbackComments"));
							 try
							 {
									Cursor c2=cf.SelectTablefunction(cf.Additional_table," where GCH_AD_SRID='"+homeId+"' and GCH_AD_INSPID='"+inspectorId+"'");
									 if(c2.getCount()>0)
									{
										cf.gch_db.execSQL("Delete from "+cf.Additional_table+" where GCH_AD_SRID='"+homeId+"' and GCH_AD_INSPID='"+inspectorId+"'");
									}
									cf.gch_db.execSQL("Insert into "+cf.Additional_table+" (GCH_AD_SRID,GCH_AD_INSPID,GCH_AD_IsRecord,GCH_AD_UserTypeName,GCH_AD_ContactEmail,GCH_AD_PhoneNumber,GCH_AD_MobileNumber,GCH_AD_BestTimetoCallYou,GCH_AD_FirstChoice,GCH_AD_SecondChoice,GCH_AD_ThirdChoice,SF_AD_FeedbackComments) values" +
											"('"+homeId+"','"+inspectorId+"','"+rechk+"','"+usrtyp+"','"+email+"','"+phnnum+"','"+mblnum+"','"+timetocall+"','"+fchoice+"','"+schoice+"','"+tchoice+"','"+fdcomments+"') ");
						          
							 }
							 catch(Exception e)
							 {
								cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ Import.this +" "+" in the stage of(catch) inserting additional info  table at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
							 }
					
					}
					}
			
		}
		catch(Exception e)
		{
			 cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ Import.this +" "+" in the stage of(catch) checking additional info  table at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				
		}
	}
	public boolean getversioncodefromweb() throws NetworkErrorException,
		SocketTimeoutException, IOException, XmlPullParserException,Exception
	 {
		System.out.println("insdgetn");
		
		 SoapObject request = new SoapObject(cf.NAMESPACE, "GetVersionInformation_GenHazards");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.dotNet = true;
			System.out.println("PROPERTY: "+request );
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
        	androidHttpTransport.call(cf.NAMESPACE+"GetVersionInformation_GenHazards", envelope);
			SoapObject result = (SoapObject) envelope.getResponse();
			System.out.println("inresult:"+result);
			
			SoapObject obj = (SoapObject) result.getProperty(0);
			cf.newcode = String.valueOf(obj.getProperty("VersionCode"));
			cf.newversion = String.valueOf(obj.getProperty("VersionName"));
			System.out.println(" v code"+vcode+" new vode" +cf.newcode);
			if (!"null".equals(cf.newcode) && null != cf.newcode && !cf.newcode.equals("")) {
				if (Integer.toString(vcode).equals(cf.newcode)) {
					
				} else {
					try {
						cf.Create_Table(2);
						Cursor c12 = cf.SelectTablefunction(cf.Version," ");

						if (c12.getCount() < 1) {
							try {
								cf.gch_db.execSQL("INSERT INTO "
										+ cf.Version
										+ "(VId,GCH_VersionCode,GCH_VersionName)"
										+ " VALUES ('1','"+cf.newcode+"','" + cf.newversion
										+ "')");

							} catch (Exception e) {
								
							}

						} else {
							try {
								cf.gch_db.execSQL("UPDATE " + cf.Version
										+ " set GCH_VersionCode='"+cf.newcode+"',GCH_VersionName='"
										+ cf.newversion
										+ "'");
							} catch (Exception e) {
							
							}

						}
						
					} catch (Exception e) {
						

					}

				}
			} else {
				
			}
			
			
			
			 SoapObject request1 = new SoapObject(cf.NAMESPACE, "GetVersionInformation_IDMS");
				SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(
						SoapEnvelope.VER11);
				envelope1.dotNet = true;
				envelope1.setOutputSoapObject(request1);
				HttpTransportSE androidHttpTransport1 = new HttpTransportSE(cf.URL_IDMA);
		    	androidHttpTransport1.call(cf.NAMESPACE+"GetVersionInformation_IDMS", envelope1);
		    	
		    	SoapObject result1 = (SoapObject) envelope1.getResponse();
				
				SoapObject obj1 = (SoapObject) result1.getProperty(0);
				cf.newcode = String.valueOf(obj1.getProperty("VersionCode"));
				cf.newversion = String.valueOf(obj1.getProperty("VersionName"));
				System.out.println(" version name and version code  "+cf.newcode+"  "+cf.newversion);
				if (!"null".equals(cf.newcode) && null != cf.newcode && !cf.newcode.equals("")) {
						try {     
							cf.Create_Table(20);
							Cursor c12 = cf.SelectTablefunction(cf.IDMAVersion," Where ID_VersionType='IDMA_New' ");

							if (c12.getCount() < 1) {
								
									cf.gch_db.execSQL("INSERT INTO "
											+ cf.IDMAVersion
											+ "(VId,ID_VersionCode,ID_VersionName,ID_VersionType)"
											+ " VALUES ('2','"+cf.newcode+"','" + cf.newversion
											+ "','IDMA_New')");

								

							} else {
									cf.gch_db.execSQL("UPDATE " + cf.IDMAVersion
											+ " set ID_VersionCode='"+cf.newcode+"',ID_VersionName='"
											+ cf.newversion
											+ "' Where  ID_VersionType='IDMA_New'");
								
							}
					
						} catch (Exception e) {
					
							System.out.println("new data  error  "+e.getMessage());
						}

					}
				
			 
			return true;
	 }
	 private int getcurrentversioncode() {
			// TODO Auto-generated method stub

			try {
				vcode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;

			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return vcode;
		}

	@Override
		protected Dialog onCreateDialog(int id) {
			switch (id) {
			case 1:
				progDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
				progDialog.setMax(cf.maxBarValue);	
				progDialog.setMessage("Importing please wait :");	
				progDialog.setCancelable(false);	
				progThread = new ProgressThread(handler1);	
				progThread.start();	
				return progDialog;
			default:
				return null;
			}
		}
	  final Handler handler1 = new Handler() {
			public void handleMessage(Message msg) {
				// Get the current value of the variable total from the message data
				// and update the progress bar.
				int total = msg.getData().getInt("total");
				progDialog.setProgress(total);
				if (total == 100) {
					progDialog.setCancelable(true);
					dismissDialog(cf.typeBar);
					handler.sendEmptyMessage(0);
					progThread.setState(ProgressThread.DONE);
			 	}

			}
		};
		final Handler handler = new Handler() {

			public void handleMessage(Message msg) {
				 if(cf.usercheck==1)
				 {
					 cf.usercheck = 0;
					 cf.ShowToast("Internet connection is not available.",1);
					 startActivity(new Intent(Import.this, Import.class));
				 }
				 else if(cf.usercheck==2)
				 {
					 cf.usercheck = 0;
					 cf.ShowToast("There is a problem on your network,so please try again later with better network.",2);
					 startActivity(new Intent(Import.this, Import.class));
				 }
				 else if(cf.usercheck==3)
				 {
					 cf.usercheck = 0;
					 cf.ShowToast("There is a problem on your application. Please contact Paperless administrator.",2);
					 startActivity(new Intent(Import.this, Import.class));
				 }
				 else if(cf.usercheck==4)
				 {
					 cf.usercheck=0;
					 cf.alerttitle="Import";
					 cf.alertcontent="Import successfully completed";
					 cf.showalert(cf.alerttitle,cf.alertcontent);
				 }
				
			}

		};
		private class ProgressThread extends Thread {

			// Class constants defining state of the thread
			final static int DONE = 0;

			Handler mHandler;
			ProgressThread(Handler h) {
				mHandler = h;
			}

			@Override
			public void run() {
				cf.mState = cf.RUNNING;
				while (cf.mState == cf.RUNNING) {
					try {
						Thread.sleep(cf.delay);
					} catch (InterruptedException e) {
						Log.e("ERROR", "Thread was Interrupted");
					}

					Message msg = mHandler.obtainMessage();
					Bundle b = new Bundle();
					b.putInt("total", cf.total);
					msg.setData(b);
					mHandler.sendMessage(msg);

				}
			}

			public void setState(int state) {
				cf.mState = state;
			}

		}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(Import.this, HomeScreen.class));
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}


}
