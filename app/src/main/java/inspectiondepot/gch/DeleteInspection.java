package inspectiondepot.gch;

import inspectiondepot.gch.R;

import java.util.ArrayList;



import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class DeleteInspection extends Activity {
    TextView title;
	CommonFunctions cf;
	String strtit;
	Button deleteall; 
View v;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.deleteinspection);
		cf = new CommonFunctions(this);
		cf.Create_Table(3);
		cf.Create_Table(7);
		cf.welcome = (TextView) this.findViewById(R.id.welcomename);
		cf.getInspectorId();
		cf.releasecode = (TextView)findViewById(R.id.releasecode);
        cf.releasecode.setText(cf.apkrc);
        
        deleteall = (Button)this.findViewById(R.id.deleteall);
        cf.welcome.setText(cf.Insp_firstname.toUpperCase()+" "+cf.Insp_lastname.toUpperCase());
		title = (TextView) this.findViewById(R.id.deleteiinformation);
		try {
			Cursor c = cf.SelectTablefunction(cf.policyholder,
					"where GCH_PH_InspectorId='" + cf.encode(cf.Insp_id) + "'");
			if(c.getCount()==0)
			{
				deleteall.setVisibility(v.INVISIBLE);cf.gohome();
			}
			else
			{
				deleteall.setVisibility(v.VISIBLE);
				c.moveToFirst();
				if (c.getString(c.getColumnIndex("GCH_PH_InspectionTypeId")).equals("18")) {
					strtit = cf.strret;
				} else {
					strtit =cf.strcarr;
				}
			}
			
			
		} catch (Exception e) {
			cf.strerrorlog="retrieving from PolicyHolder table";
			cf.Error_LogFile_Creation(cf.strerrorlog+" "+" at "+ cf.con.getClass().getName().toString()+" "+"  at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			
		}
		
		
		cf.search = (Button) this.findViewById(R.id.search);
		cf.search_text = (EditText)findViewById(R.id.search_text);
		cf.search_clear_txt = (Button)findViewById(R.id.search_clear_txt);
		cf.onlinspectionlist = (LinearLayout) findViewById(R.id.linlayoutdyn);
			
		cf.search_clear_txt.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				cf.search_text.setText("");
				cf.res = "";
				dbquery();
			}

		});
		cf.search.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				String temp = cf.encode(cf.search_text.getText()
						.toString());
				if (temp.equals("")) {
					cf.ShowToast("Please enter the Name or Policy Number to search.", 1);
					cf.search_text.requestFocus();
				} else {
					cf.res = temp;
					dbquery();
				}

			}

		});
		dbquery();
	}
	 private void dbquery() {
			cf.data = null;
			cf.inspdata = "";
			cf.countarr = null;
			cf.rws = 0;
			cf.sql = "select * from " + cf.policyholder
					+ " where  GCH_PH_InspectorId = '"
					+ cf.encode(cf.Insp_id) + "'";
			if (!cf.res.trim().equals("")) {
				cf.sql += " and (GCH_PH_FirstName like '%" + cf.encode(cf.res)
						+ "%' or GCH_PH_LastName like '%" + cf.encode(cf.res)
						+ "%' or GCH_PH_Policyno like '%" + cf.encode(cf.res) + "%' ) ";
				
			} 
			Cursor cur = cf.gch_db.rawQuery(cf.sql, null);
			cf.rws = cur.getCount();
			title.setText("Total Record : " + cf.rws);
			cf.data = new String[cf.rws];
			cf.countarr = new String[cf.rws];
			int j = 0;
			cur.moveToFirst();
			if (cur.getCount() >= 1) {

				do {
					
					cf.data[j]= " "+ cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_FirstName")))
									+ " ";
					cf.data[j] += cf.decode(cur.getString(cur
									.getColumnIndex("GCH_PH_LastName"))) + " | ";
					cf.data[j] += cf.decode(cur.getString(cur
									.getColumnIndex("GCH_PH_Policyno")))
									+ " \n ";
					cf.data[j] += cf.decode(cur.getString(cur
									.getColumnIndex("GCH_PH_Address1"))) + " | ";
					cf.data[j] += cf.decode(cur.getString(cur
									.getColumnIndex("GCH_PH_City"))) + " | ";
					cf.data[j] += cf.decode(cur.getString(cur
									.getColumnIndex("GCH_PH_State"))) + " | ";
					cf.data[j] += cf.decode(cur.getString(cur
									.getColumnIndex("GCH_PH_County"))) + " | ";
					cf.data[j] += cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_Zip")))
									+ " \n ";
					cf.data[j] += cf.decode(cur.getString(cur
									.getColumnIndex("GCH_Schedule_ScheduledDate"))) + " | ";
					cf.data[j] += cf.decode(cur.getString(cur
									.getColumnIndex("GCH_Schedule_InspectionStartTime"))) + " - ";
					cf.data[j] += cf.decode(cur.getString(cur
									.getColumnIndex("GCH_Schedule_InspectionEndTime"))) + " | ";
					cf.data[j] += cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_Status")));
							if (cf.data[j].contains("| 111")) {
								cf.data[j] =cf.data[j].replace("| 111", "| Cancelled Inspection");
							}
							if (cf.data[j].contains("| 110")) {
								cf.data[j] = cf.data[j].replace("| 110", "| Unable to Scheduled");
							}
							if (cf.data[j].contains("| 40")) {
								String ss = cf.decode(cur.getString(cur
										.getColumnIndex("GCH_PH_SubStatus")));
								if (!ss.equals("0")) {
									cf.data[j] += ss;
									if (cf.data[j].contains("| 4041")) {
										cf.data[j] = cf.data[j].replace("| 4041",
												"| Completed Inspections in Online");
									}
								} else {
									cf.data[j] = cf.data[j].replace("| 40", "| Schedule");
								}
							}
							if (cf.data[j].contains("| 30")) {
								cf.data[j] = cf.data[j].replace("| 30", "| Assign");
							}
							cf.countarr[j] = cur.getString(cur
									.getColumnIndex("GCH_PH_SRID"));
							

							if (cf.data[j].contains("null")) {
								cf.data[j]=cf.data[j].replace("null", "");
							}
							if (cf.data[j].contains("N/A | ")) {
								cf.data[j] = cf.data[j].replace("N/A |", "");
							}
							if (cf.data[j].contains("N/A - N/A")) {
								cf.data[j] = cf.data[j].replace("N/A - N/A", "");
							}
												
					j++;
				} while (cur.moveToNext());
				cf.search_text.setText("");
				display();
			} else {
				
				cf.onlinspectionlist.removeAllViews();
				if(cf.res.equals(""))
				{
					cf.gohome();
				}
				else
				{
					cf.ShowToast("Sorry, No results found.", 1);cf.hidekeyboard();
				}
				
				
			}

		}

		private void display() {

			cf.onlinspectionlist.removeAllViews();
			cf.sv = new ScrollView(this);
			cf.onlinspectionlist.addView(cf.sv);

			final LinearLayout l1 = new LinearLayout(this);
			l1.setOrientation(LinearLayout.VERTICAL);
			cf.sv.addView(l1);

			if (cf.data!=null && cf.data.length>0) {
			//	this.cf.data = cf.inspdata.split("~");
				for (int i = 0; i < cf.data.length; i++) {
					cf.tvstatus = new TextView[cf.rws];
					cf.deletebtn = new Button[cf.rws];
					LinearLayout l2 = new LinearLayout(this);
					LinearLayout.LayoutParams mainparamschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
					l2.setLayoutParams(mainparamschk);
					l2.setOrientation(LinearLayout.HORIZONTAL);
					l1.addView(l2);

					LinearLayout lchkbox = new LinearLayout(this);
					LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
					paramschk.topMargin = 8;
					paramschk.leftMargin = 20;
					paramschk.bottomMargin = 10;
					l2.addView(lchkbox);
					cf.tvstatus[i] = new TextView(this);
					cf.tvstatus[i].setTag("textbtn" + i);
					cf.tvstatus[i].setText(cf.data[i]);
					cf.tvstatus[i].setTextColor(Color.WHITE);
					cf.tvstatus[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,14);

				    lchkbox.addView(cf.tvstatus[i], paramschk);

					LinearLayout ldelbtn = new LinearLayout(this);
					LinearLayout.LayoutParams paramsdelbtn = new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			        paramsdelbtn.setMargins(0, 10, 10, 0); //left, top, right, bottom
					ldelbtn.setLayoutParams(mainparamschk);
					ldelbtn.setGravity(Gravity.RIGHT);
				    l2.addView(ldelbtn);
					cf.deletebtn[i] = new Button(this);
					cf.deletebtn[i].setBackgroundResource(R.drawable.deletebtn1);
					cf.deletebtn[i].setTag("deletebtn" + i);
					cf.deletebtn[i].setPadding(30, 0, 0, 0);
					ldelbtn.addView(cf.deletebtn[i], paramsdelbtn);

					cf.deletebtn[i].setOnClickListener(new View.OnClickListener() {
						public void onClick(final View v) {
							String getidofselbtn = v.getTag().toString();
							final String repidofselbtn = getidofselbtn.replace(
									"deletebtn", "");
							final int cvrtstr = Integer.parseInt(repidofselbtn);
							final String dt = cf.countarr[cvrtstr];
							 cf.alerttitle="Delete";
							  cf.alertcontent="Are you sure want to delete?";
							    final Dialog dialog1 = new Dialog(DeleteInspection.this,android.R.style.Theme_Translucent_NoTitleBar);
								dialog1.getWindow().setContentView(R.layout.alertsync);
								TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
								txttitle.setText( cf.alerttitle);
								TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
								txt.setText(Html.fromHtml( cf.alertcontent));
								Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
								Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
								btn_yes.setOnClickListener(new OnClickListener()
								{
				                	@Override
									public void onClick(View arg0) {
										// TODO Auto-generated method stub
										dialog1.dismiss();
										cf.fn_delete(dt);
										startActivity(new Intent(DeleteInspection.this,DeleteInspection.class));
										
									}
									
								});
								btn_cancel.setOnClickListener(new OnClickListener()
								{

									@Override
									public void onClick(View arg0) {
										// TODO Auto-generated method stub
										dialog1.dismiss();
										
									}
									
								});
								dialog1.setCancelable(false);
								dialog1.show();
						

						}
					});
				}
			}
			else
			{
			
			}

		}
	 public void clicker(View v)
	 {
		  switch(v.getId())
		  {
		  case R.id.deletehome:
			  cf.gohome();
			  break;
		  case R.id.deleteall:
		 		 cf.delete_all("");
			 break;
		  }
	 }
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(DeleteInspection.this,HomeScreen.class));
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}