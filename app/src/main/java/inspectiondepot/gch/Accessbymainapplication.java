package inspectiondepot.gch;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import org.kobjects.base64.Base64;
import org.ksoap2.transport.HttpTransportSE;

import com.google.android.maps.Overlay.Snappable;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

public class Accessbymainapplication extends Activity{
	
	private static final String TAG = null;
	CommonFunctions cf;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle extras = getIntent().getExtras();
		
		cf = new CommonFunctions(this);
		if (extras != null) {
			    String Ins_Id=extras.getString("Ins_Id");
			    String Ins_FirstName=extras.getString("Ins_FirstName");
			    String Ins_LastName=extras.getString("Ins_LastName");
			    String Ins_Address=extras.getString("Ins_Address");
			    String Ins_CompanyName=extras.getString("Ins_CompanyName");
			    String Ins_CompanyId= extras.getString("Ins_CompanyId");
			    String Ins_UserName=extras.getString("Ins_UserName");
			    String Ins_Password= extras.getString("Ins_Password");
			    String Ins_PhotoExtn= extras.getString("Ins_PhotoExtn");
			    String Ins_Email= extras.getString("Ins_Email");
			    String Insp_PrimaryLicenseNumber= extras.getString("Insp_PrimaryLicenseNumber");
			    String Insp_sign= extras.getString("Insp_sign");
			    String Insp_PrimaryLicenseType= extras.getString("Insp_PrimaryLicenseType");
			    System.out.println("there is no more prb"+Insp_sign+Insp_PrimaryLicenseNumber+Insp_PrimaryLicenseType);
			    int V_code=0;
			    
			    String V_name="0";
			    try
			    {
			     V_code= extras.getInt("IDMA_Version_code");
			    
			     V_name= extras.getString("IDMA_Version_name");
			     
			    }
			    catch(Exception e)
			    {
			    	V_code=0;
			    	V_name="0";
			    	
			    }
			     if(V_name==null)
			     {
			    	 V_name="";
			     }
			    String inspext =  Ins_PhotoExtn;
			    String  FILENAME=Ins_Id+inspext;
			   
				
				
			    try
			    {
			    	/**update the version of the IDMA application for getting updated version **/
			    	cf.Create_Table(20);
			    	Cursor cs=cf.SelectTablefunction(cf.IDMAVersion," Where  ID_VersionType='IDMA_Old' ");
			    	
			    	if(cs.getCount()>=1)
			    	{
			    		cf.gch_db.execSQL("UPDATE "+cf.IDMAVersion+" set ID_VersionCode='"+V_code+"' ,ID_VersionName='"+V_name+"' Where  ID_VersionType='IDMA_Old' "); 
			    	}
			    	else
			    	{
			    		
			    		cf.gch_db.execSQL("INSERT INTO "+ cf.IDMAVersion+" (ID_VersionCode,ID_VersionName,ID_VersionType) VALUES ('" + V_code + "','"+V_name + "','IDMA_Old')");
			    		
			    	}
			    	
			    	 cf.Create_Table(1);
					    Cursor log= cf.SelectTablefunction(cf.inspectorlogin, " where Fld_InspectorId='"+Ins_Id+"'");
						log.moveToFirst();
						if(log.getCount()>=1)
						{
							if(log.getString(log.getColumnIndex("Fld_InspectorFlag")).equals("1"))
							{
								
								cf.gch_db.execSQL("UPDATE "+cf.inspectorlogin+" set Fld_InspectorFlag=0"); // set the flag zero  for the inspector to clear all hte login 
								cf.gch_db.execSQL("UPDATE "+cf.inspectorlogin+" set Fld_InspectorFlag=1 where Fld_InspectorId='"+Ins_Id+"'"); // set the flag one for the inspector who logined new 
								
								Intent Star = new Intent(Accessbymainapplication.this,
										HomeScreen.class);
								Star.putExtra("keyName", 1);
								startActivity(Star);
								
								
							}
							else 
							{
								
								cf.gch_db.execSQL("UPDATE "+cf.inspectorlogin+" set Fld_InspectorFlag=1 where Fld_InspectorId='"+Ins_Id+"'"); // set the flag one for the inspector
								Intent Star = new Intent(Accessbymainapplication.this,
										HomeScreen.class);
								Star.putExtra("keyName", 1);
								startActivity(Star);
								
								
							}
							
							
						}
						else
						{
						    try {
						    	System.out.println("comes in the insert  "+Insp_sign+Insp_PrimaryLicenseNumber+Insp_PrimaryLicenseType);
						    	 cf.Create_Table(1);
								cf.gch_db.execSQL("INSERT INTO "
										+ cf.inspectorlogin
										+ " (Fld_InspectorId,Fld_InspectorFirstName,Fld_InspectorMiddleName,Fld_InspectorLastName,Fld_InspectorAddress,Fld_InspectorCompanyName,Fld_InspectorCompanyId,Fld_InspectorUserName,Fld_InspectorPassword,Fld_InspectorFlag,Android_status,Fld_Remember_pass,Fld_InspectorPhotoExtn,Fld_InspectorEmail,Insp_sign_img,Insp_PrimaryLicenseType,Insp_PrimaryLicenseNumber)"
										+ " VALUES ('" + Ins_Id + "','"
										+Ins_FirstName + "','"
										+ "''" + "','"
										+ Ins_LastName + "','"
										+ Ins_Address + "','"
										+ Ins_CompanyName + "','"
										+Ins_CompanyId + "','"
										+ Ins_UserName.toLowerCase()
										+ "','" + Ins_Password + "','"
										+ "1" + "','" +"true"+ "','','"+Ins_PhotoExtn+"','"+Ins_Email+"','"+Insp_sign+"','"+Insp_PrimaryLicenseType+"','"+Insp_PrimaryLicenseNumber+"')");
								// insert the inspector and set the flag as login 
								
								/**update the version of the IDMA application for getting updated version  Ends**/
							    FileOutputStream fos = openFileOutput(FILENAME, this.MODE_WORLD_READABLE);
								File outputFile = new File("/data/data/com.idinspection/files"+"/"+Ins_Id+inspext);
								//byte b[]=outputFile.to
										if (outputFile.exists()) 
										{
								    		
													
													InputStream fis = new FileInputStream(outputFile);
													long length = outputFile.length();
													byte[] bytes = new byte[(int) length];
													int offset = 0;
													int numRead = 0;
													while (offset < bytes.length && (numRead = fis.read(bytes, offset, bytes.length - offset)) >= 0)
													{
														offset += numRead;
													}
													
												fos.write(bytes);	
												
													
										
									}
										 FileOutputStream fos1 = openFileOutput(Insp_sign, this.MODE_WORLD_READABLE);
											File outputFile1 = new File("/data/data/com.idinspection/files"+"/"+Insp_sign);
											//byte b[]=outputFile.to
													if (outputFile1.exists()) 
													{
											    		
														
																InputStream fis = new FileInputStream(outputFile1);
																long length = outputFile1.length();
																byte[] bytes = new byte[(int) length];
																
																int offset = 0;
																int numRead = 0;
																while (offset < bytes.length && (numRead = fis.read(bytes, offset, bytes.length - offset)) >= 0)
																{
																	offset += numRead;
																	
																}
																
															fos1.write(bytes);	
															
																
													
												}
													else
													{
														System.out.println("isseus in the not exitis");
													}
								
								Intent Star = new Intent(Accessbymainapplication.this,
										HomeScreen.class);
								Star.putExtra("keyName", 1);
								startActivity(Star);
								
								
							} catch (Exception e) {
								Log.i(TAG, "categorytableerror=" + e.getMessage());
								if(cf.application_sta)
								{
									Intent intent = new Intent(Intent.ACTION_MAIN);
									intent.setComponent(new ComponentName("com.idinspection","com.idinspection.ApplicationMenu"));
									startActivity(intent);
								}else
								{
									Intent Star = new Intent(Accessbymainapplication.this,
											GeneralConditionHazardActivity.class);
									Star.putExtra("keyName", 1);
									startActivity(Star);
								}
							}
				   
						}
			    
						
			    }
			    catch(Exception e)
			    {
			    	System.out.println("error was indma"+e.getMessage());
			    }
			   
		    
			}
		else
		{ 
			   cf.Create_Table(1);
			   Cursor log= cf.SelectTablefunction(cf.inspectorlogin, " where Fld_InspectorFlag='1'");
			   if(log.getCount()==1)
			   {
				   Intent Star = new Intent(Accessbymainapplication.this,
							HomeScreen.class);
					Star.putExtra("keyName", 1);
					startActivity(Star); 
			   }
			   else 
			   {
				   try
					{
						File f = new File(Environment.getExternalStorageDirectory()+"/Gch_logininfo.txt"); // storing login information in to the txt fil for access the login information
						
						if (f.exists()) {
									   
						    FileInputStream fis = new FileInputStream(f);
						    DataInputStream in = new DataInputStream(fis);
						    String Ins_Id="",Ins_FirstName="",Ins_Email="",Ins_LastName="",Ins_Address="",Ins_CompanyName="",Ins_CompanyId="",Ins_UserName="",Ins_Password="",Ins_PhotoExtn="";
						    String s[]= new String[10];
						 
						    try {
						    	
						    	 
						    	int i=0;
						    	
						        for (;;) {
						        	
						        	s[i]=in.readUTF();
						        	i++;
						        }
						      } catch (EOFException e) {
						       
						    }
						   
						    if(s[0].startsWith("Ins_Id="))
					        {
					        Ins_Id=s[0].substring(s[0].indexOf("=")+1);
					        }
						    
					        if(s[1].startsWith("Ins_FirstName="))
						    Ins_FirstName=s[1].substring(s[1].indexOf("=")+1);
					        
					        if(s[2].startsWith("Ins_LastName="))
						    Ins_LastName=s[2].substring(s[2].indexOf("=")+1);
					        if(s[3].startsWith("Ins_Address="))
						    Ins_Address=s[3].substring(s[3].indexOf("=")+1);


						    if(s[4].startsWith("Ins_CompanyName="))
						    Ins_CompanyName=s[4].substring(s[4].indexOf("=")+1);
						   
						    if(s[5].startsWith("Ins_CompanyId="))
						    Ins_CompanyId= s[5].substring(s[5].indexOf("=")+1);
						    if(s[6].startsWith("Ins_UserName="))
						    Ins_UserName=s[6].substring(s[6].indexOf("=")+1);
						    if(s[7].startsWith("Ins_Password="))
						    Ins_Password= s[7].substring(s[7].indexOf("=")+1);
						    if(s[8].startsWith("Ins_PhotoExtn="))
						    	Ins_PhotoExtn= s[8].substring(s[8].indexOf("=")+1);
						    if(s[9].startsWith("Ins_Email="))
						    	Ins_Email= s[9].substring(s[9].indexOf("=")+1);
						    in.close();
						    f.delete(); // delete the file once we update in to the tabel
						
						    Intent Star = new Intent(Accessbymainapplication.this,Accessbymainapplication.class);
						    Star.putExtra("Ins_Id",Ins_Id);
						    Star.putExtra("Ins_FirstName",Ins_FirstName);
						    Star.putExtra("Ins_LastName",Ins_LastName);
						    Star.putExtra("Ins_Address",Ins_Address);
						    Star.putExtra("Ins_CompanyName",Ins_CompanyName);
						    Star.putExtra("Ins_CompanyId",Ins_CompanyId);
						    Star.putExtra("Ins_UserName",Ins_UserName);
						    Star.putExtra("Ins_Password",Ins_Password);
						    Star.putExtra("Ins_PhotoExtn",Ins_PhotoExtn);
						    Star.putExtra("Ins_Email",Ins_Email);
							startActivity(Star);
							
						    
						}
						else
						{
							if(cf.application_sta)
							{
								Intent intent = new Intent(Intent.ACTION_MAIN);
								intent.setComponent(new ComponentName("com.idinspection","com.idinspection.ApplicationMenu"));
								startActivity(intent);
							}  
							else
							{
								cf.gch_db.execSQL("UPDATE "+cf.inspectorlogin+" set Fld_InspectorFlag=0 "); // set the flag one for the inspector
								 Intent Star = new Intent(Accessbymainapplication.this,GeneralConditionHazardActivity.class);
								 startActivity(Star);
							}
						}
						
					}catch(Exception e)
					{System.out.println("error"+e.getMessage());
					}
		 }
		}
		
	}
	
	

}
