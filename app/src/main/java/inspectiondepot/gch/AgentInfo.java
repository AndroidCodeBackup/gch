package inspectiondepot.gch;

import inspectiondepot.gch.R;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AgentInfo extends Activity {
	CommonFunctions cf;
	TextView[] agent = new TextView[16];
	 public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.selectedhomeid = extras.getString("homeid");
				cf.onlinspectionid = extras.getString("InspectionType");
			    cf.onlstatus = extras.getString("status");
        	}
			setContentView(R.layout.agent);
			cf.getDeviceDimensions();
       	    LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
            mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 1, 0,cf));
		    LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.generalsubmenu);
		    submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 15, 1,cf));
		    cf.getInspectorId();
		    agent[3] = (TextView) findViewById(R.id.efirstname);
		    agent[4] = (TextView) findViewById(R.id.txtaddr1);
		    agent[5] = (TextView) findViewById(R.id.txtaddr2);
		    agent[6] = (TextView) findViewById(R.id.txtcity);
		    agent[7] = (TextView) findViewById(R.id.txtcountry);
		    agent[8] = (TextView) findViewById(R.id.econtactperson);
		    agent[9] = (TextView) findViewById(R.id.txtstate);
		    agent[10] = (TextView) findViewById(R.id.txtzip);
		    agent[11] = (TextView) findViewById(R.id.txtoffphn);
		    agent[12] = (TextView) findViewById(R.id.txtconphn);
		    agent[13] = (TextView) findViewById(R.id.txtfax);
		    agent[14] = (TextView) findViewById(R.id.txtmail);
		    agent[15] = (TextView) findViewById(R.id.txtweb);
		    agent[2] = (TextView) findViewById(R.id.txtagncyname);
		    
		    try {
				Cursor cur = cf.SelectTablefunction(cf.Agent_tabble, "where GCH_AI_SRID='"
						+ cf.selectedhomeid + "'");
				int rws = cur.getCount();
				cur.moveToFirst();
                if (cur != null) {
					for (int i = 2; i <= 15; i++) {
						agent[i].setText(cur.getString(i));
						if (cur.getString(i).trim().equals("anyType{}")
								|| cur.getString(i).trim().equals("")) {
							agent[i].setText("Not Available");
						}
					}

				}while (cur.moveToNext());
		    }
		    catch(Exception e)
		    {
		    	cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ AgentInfo.this +" "+" in the stage of(catch) retreving  at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				
		    }
	 }
	 public void clicker(View v) {
			switch (v.getId()) {

			case R.id.home:
						cf.gohome();
			break;

			}
		}
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				Intent intimg = new Intent(getApplicationContext(), PolicyHolder.class);
				intimg.putExtra("homeid", cf.selectedhomeid);
				intimg.putExtra("InspectionType", cf.onlinspectionid);
				intimg.putExtra("status", cf.onlstatus);
				startActivity(intimg);
				return true;
			}
			return super.onKeyDown(keyCode, event);
		}

}

