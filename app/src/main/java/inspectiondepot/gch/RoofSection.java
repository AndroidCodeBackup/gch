package inspectiondepot.gch;

import java.util.Calendar;

import android.R.integer;
import android.R.string;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class RoofSection extends Activity {
	CommonFunctions cf;
	/***RCT Declaration starts **/
	int DATE_DIALOG_ID=24;
	public EditText GCH_RC_PA_ed,GCH_RC_Rsh_Other,GCH_RC_Rsh_per,GCH_RC_RSt_Other,GCH_RC_RCT_Other,GCH_RC_YIR_Other;
	public Button GCH_RC_PA_DP,GCH_RC_RCT_SA,GCH_RC_RCT_can;
	public Spinner GCH_RC_YIR_sp,GCH_RC_RS_sp,GCH_RC_Rsh_sp,GCH_RC_RSt_sp,GCH_RC_PD_sp,GCH_RC_RCT_sp;
	private int mYear,mMonth,mDay;
	TableLayout RCT_ShowValue,RCN_ShowValue;
	ArrayAdapter adapter1,adapter2,adapter3,adapter4,adapter5,adapter6;	
	int Current_select_id=0;
	String roof_slope[]={"--Select--","1/12","2/12","3/12","4/12","5/12","6/12","7/12","8/12","9/12","10/12","11/12","12/12","13/12","14/12","Flat","Unknown"};
	String roof_shape[]={"--Select--","Hip Roof","Non-Hip Roof","Other"};
	String roof_structure[]={"--Select--","Truss","Conventional","Wood","Steel","Other"};
	String roof_cover_type[]={"--Select--","Asphalt/Fiberglass","Concrete/Clay Tile","Metal","Built Up","Membrane","Other"};
//	String roof_predominent[]={"--Select--","FBC Compliant","Non FBC Compliant","Unknown or undetermined"};
	String roof_predominent[]={"--Select--",">2001 FBC","1994 SFBC","Pre 2001 FBC","Pre 1994 SFBC","Not Applicable","Beyond Scope of Inspection"};
	
	String RCT_in_DB[];
	int RCT_in_DB_id[];
	CheckBox RC_NIP_chk;
	RadioButton RC_Dom_rd;
	int Per_in_DB=0;
	int focus=0;
	/***RCT Declaration ends **/
	
	/***General Roof infromation  Declaration Starts **/
	
	RadioGroup GCH_RC_RG_ERAE,GCH_RC_RG_3YR;
	LinearLayout RC_cmt_h,RC_cmt_par,RC_cmt_chaild;
    EditText RC_cmt;
	TextView RC_cmt_tv;
	Boolean b1=false;
	Button RC_AMC;
	TableRow RCN_row[];
	private boolean pre_dom;
	/***General Roof infromation  Declaration Starts **/
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		 Bundle extras = getIntent().getExtras();
		 cf=new CommonFunctions(this);	
		 if (extras != null) {
				cf.selectedhomeid = extras.getString("homeid");
				cf.onlinspectionid = extras.getString("InspectionType");
			    cf.onlstatus = extras.getString("status");
	    	}
			
			   
			setContentView(R.layout.roof_section);
			/**Used for hide he key board when it clik out side**/

			cf.setupUI((ScrollView) findViewById(R.id.scr));

		/**Used for hide he key board when it clik out side**/
			cf.getInspectorId();
		    cf.getDeviceDimensions();
		    LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
		    mainmenu_layout.setMinimumWidth(cf.wd-20);
		    TableRow tbrw=(TableRow)findViewById(R.id.row2);
		    tbrw.setMinimumHeight(cf.ht);
			mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 4, 0,cf));
			
			final Calendar cal = Calendar.getInstance();
			mYear = cal.get(Calendar.YEAR);
			mMonth = cal.get(Calendar.MONTH);
			mDay = cal.get(Calendar.DAY_OF_MONTH);
			((TextView) findViewById(R.id.RC_txt1)).setText(Html.fromHtml(cf.redcolor +" Roof Covering Type "));
			((TextView) findViewById(R.id.RC_txt2)).setText(Html.fromHtml(cf.redcolor +" Permit Application Date  "));
			((TextView) findViewById(R.id.RC_txt3)).setText(Html.fromHtml(cf.redcolor +" Year of Ins"));
			((TextView) findViewById(R.id.RC_txt4)).setText(Html.fromHtml(cf.redcolor +" Roof Slope  "));
			((TextView) findViewById(R.id.RC_txt5)).setText(Html.fromHtml(cf.redcolor +" Roof Shape (%) "));
			((TextView) findViewById(R.id.RC_txt6)).setText(Html.fromHtml(cf.redcolor +" Roof Structure  "));
			((TextView) findViewById(R.id.RC_txt7)).setText(Html.fromHtml(cf.redcolor +" Building Code "));
			/*((TextView) findViewById(R.id.RC_txt8)).setText(Html.fromHtml(cf.redcolor +" Roof Damage Noted"));
			((TextView) findViewById(R.id.RC_txt9)).setText(Html.fromHtml(cf.redcolor +" Roof Repairs Noted"));
			((TextView) findViewById(R.id.RC_txt10)).setText(Html.fromHtml(cf.redcolor +" Roof Leakage Noted"));
			*/((TextView) findViewById(R.id.RC_txt11)).setText(Html.fromHtml(cf.redcolor +" Excessive Roof Aging Evident"));
			/*((TextView) findViewById(R.id.RC_txt12)).setText(Html.fromHtml(cf.redcolor +" Other Condtion Present"));
			((TextView) findViewById(R.id.RC_txt13)).setText(Html.fromHtml(cf.redcolor +" Other Condtion Present"));*/
			((TextView) findViewById(R.id.RC_txt14)).setText(Html.fromHtml(cf.redcolor +" 3 Year Replacement Probability"));
			((TextView) findViewById(R.id.RC_txt15)).setText(Html.fromHtml(cf.redcolor +" Overall Comments - Roof "));
			
			
			
			
			Set_RC_value();
			focus=1;
			//cf.hidekeyboard();
	}
	class RC_Selected_listener implements OnItemSelectedListener
	{
		int id;
		
		RC_Selected_listener(int i)
		{
			id=i;
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			if(id==1)
			{
				 if(GCH_RC_RCT_sp.getSelectedItem().toString().trim().equals("Other"))
				 {
					 //GCH_RC_RCT_Other.setText("");
					 GCH_RC_RCT_Other.setVisibility(View.VISIBLE);
				 }
				 else
				 {
					 GCH_RC_RCT_Other.setText("");
					 GCH_RC_RCT_Other.setVisibility(View.GONE); 
				 }
				
			}else
			if(id==2)
			{
				 if(GCH_RC_YIR_sp.getSelectedItem().toString().trim().equals("Other"))
				 {
					 //GCH_RC_YIR_Other.setText("");
					 GCH_RC_YIR_Other.setVisibility(View.VISIBLE);
				 }
				 else
				 {
					 GCH_RC_YIR_Other.setText("");
					 GCH_RC_YIR_Other.setVisibility(View.GONE); 
				 }
				
			}else
			if(id==3)
			{
				 if(GCH_RC_Rsh_sp.getSelectedItem().toString().trim().equals("Other"))
				 {
					 //GCH_RC_Rsh_Other.setText("");
					 GCH_RC_Rsh_Other.setVisibility(View.VISIBLE);
				 }
				 else
				 {
					 GCH_RC_Rsh_Other.setText("");
					 GCH_RC_Rsh_Other.setVisibility(View.GONE); 
				 }
				
			}else
			if(id==4)
			{
				 if(GCH_RC_RSt_sp.getSelectedItem().toString().trim().equals("Other"))
				 {
					 //GCH_RC_RSt_Other.setText("");
					 GCH_RC_RSt_Other.setVisibility(View.VISIBLE);
				 }
				 else
				 {
					 GCH_RC_RSt_Other.setText("");
					 GCH_RC_RSt_Other.setVisibility(View.GONE); 
				 }
				
			}
			
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	private void Set_RC_value() {
		// TODO Auto-generated method stub
		
		GCH_RC_RCT_sp=(Spinner) findViewById(R.id.GCH_RC_RCT_sp);
		adapter5 = new ArrayAdapter(this,android.R.layout.simple_spinner_item, roof_cover_type);
	 	adapter5.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	 	GCH_RC_RCT_sp.setAdapter(adapter5);
	 	GCH_RC_RCT_Other=(EditText) findViewById(R.id.GCH_RC_RCT_Other);
	 	GCH_RC_RCT_Other.setOnTouchListener(new RC_editclick());
	 	GCH_RC_RCT_sp.setOnItemSelectedListener(new  RC_Selected_listener(1));
	 	
		
		GCH_RC_PA_ed=(EditText) findViewById(R.id.GCH_RC_PA_ed);
		
		GCH_RC_PA_DP=(Button) findViewById(R.id.GCH_RC_PA_DP);
		GCH_RC_PA_DP.setOnClickListener(new RC_clicker());
		
		
		GCH_RC_YIR_sp=(Spinner) findViewById(R.id.GCH_RC_YIR_sp);
		adapter1 = new ArrayAdapter(this,android.R.layout.simple_spinner_item, cf.year);
	 	adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	 	GCH_RC_YIR_sp.setAdapter(adapter1);
	 	GCH_RC_YIR_sp.setOnItemSelectedListener(new  RC_Selected_listener(2));
	 	GCH_RC_YIR_Other=(EditText) findViewById(R.id.GCH_RC_YIR_Other);
	 	GCH_RC_YIR_Other.setOnTouchListener(new RC_editclick());
	 	
	 	GCH_RC_RS_sp=(Spinner) findViewById(R.id.GCH_RC_RS_sp);
		adapter2 = new ArrayAdapter(this,android.R.layout.simple_spinner_item, roof_slope);
	 	adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	 	GCH_RC_RS_sp.setAdapter(adapter2);
	 	
	 	
	 	GCH_RC_Rsh_sp=(Spinner) findViewById(R.id.GCH_RC_Rsh_sp);
		adapter3 = new ArrayAdapter(this,android.R.layout.simple_spinner_item, roof_shape);
	 	adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	 	GCH_RC_Rsh_sp.setAdapter(adapter3);
		GCH_RC_Rsh_Other=(EditText) findViewById(R.id.GCH_RC_Rsh_Other);
		GCH_RC_Rsh_per=(EditText) findViewById(R.id.GCH_RC_Rsh_per);
		GCH_RC_Rsh_per.setOnTouchListener(new RC_editclick());
		GCH_RC_Rsh_Other.setOnTouchListener(new RC_editclick());
		GCH_RC_Rsh_sp.setOnItemSelectedListener(new  RC_Selected_listener(3));
		
		GCH_RC_RSt_sp=(Spinner) findViewById(R.id.GCH_RC_RSt_sp);
		adapter4 = new ArrayAdapter(this,android.R.layout.simple_spinner_item, roof_structure);
	 	adapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	 	GCH_RC_RSt_sp.setAdapter(adapter4);
	 	GCH_RC_RSt_Other=(EditText) findViewById(R.id.GCH_RC_RSt_Other);
	 	GCH_RC_RSt_Other.setOnTouchListener(new RC_editclick());
	 	GCH_RC_RSt_sp.setOnItemSelectedListener(new  RC_Selected_listener(4));
	 	
	 	
	 	GCH_RC_PD_sp=(Spinner) findViewById(R.id.GCH_RC_PD_sp);
		adapter6 = new ArrayAdapter(this,android.R.layout.simple_spinner_item, roof_predominent);
	 	adapter6.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	 	GCH_RC_PD_sp.setAdapter(adapter6);
	 	
	 	
	 	GCH_RC_RCT_SA =(Button) findViewById(R.id.GCH_RC_RCT_SA);
	 	GCH_RC_RCT_SA.setOnClickListener(new RC_clicker());
	 	
	 	
	 	GCH_RC_RCT_can =(Button) findViewById(R.id.GCH_RC_RCT_can);
	 	GCH_RC_RCT_can.setOnClickListener(new RC_clicker());
		
	 	RC_NIP_chk=(CheckBox) findViewById(R.id.GCH_RC_NIP_chk);
	 	RC_NIP_chk.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(RC_NIP_chk.isChecked())
				{
					GCH_RC_PA_ed.setText("Not Applicable");
					GCH_RC_PA_ed.setEnabled(false);
					GCH_RC_PA_DP.setEnabled(false);
					
				}
				else
				{
					//if(!GCH_RC_PA_ed.getText().toString().contains("/"))
					GCH_RC_PA_ed.setText("");
					GCH_RC_PA_ed.setEnabled(false);
					GCH_RC_PA_DP.setEnabled(true);
					
				}
			}
		});
	 	RC_Dom_rd=(RadioButton) findViewById(R.id.GCH_RC_DOM_rd);
	//RCT_Content=(TableRow) findViewById(R.id.RC_RCT_main_row);
	 	RCT_ShowValue= (TableLayout) findViewById(R.id.RC_RCT_ShowValue);
	 	
	 	/***declaring the General Roof information ***/
	 
		 	RC_cmt_tv = (TextView) findViewById(R.id.GCH_RC_ED_txt_Comments);
	 	
		 	RC_cmt_h =(LinearLayout) findViewById(R.id.GCH_RC_ED_Li_Comments_h);
		 	
		 	RC_cmt_par=(LinearLayout) findViewById(R.id.GCH_RC_ED_Li_par_Comments);
		 	
		 	RC_cmt_chaild=(LinearLayout) findViewById(R.id.GCH_RC_ED_Li_chaild_Comments);
	 
	 	
		 	RC_cmt=(EditText) findViewById(R.id.GCH_RC_ED_Li_Comments);
		 	RC_cmt.setOnTouchListener(new RC_editclick());
		 	RC_cmt.addTextChangedListener(new GCH_RC_watcher(RC_cmt_par,RC_cmt_chaild,RC_cmt_tv,"750"));

		 	GCH_RC_RG_ERAE=(RadioGroup) findViewById(R.id.GCH_RC_RG_ERAE);
		 	GCH_RC_RG_3YR = (RadioGroup) findViewById(R.id.GCH_RC_RG_3YRP);
		 	RCN_ShowValue= (TableLayout) findViewById(R.id.RCN_Condition_N_tbl);
		 	RC_AMC=(Button) findViewById(R.id.RC_AMC);
		 	RC_AMC.setOnClickListener(new RC_clicker());
		
		/***declaring the General Roof information ends ***/
		 System.out.println("No moer issues in the top");	
		
		show_GRI_Value();
	 	
	 	show_RCT_Value();
	 	
	
	}

    private void show_GRI_Value() {
		// TODO Auto-generated method stub
    	
    	cf.Create_Table(22);
    	/**condtion noted value setting starts***/
    Cursor RC_GRI=cf.SelectTablefunction(cf.General_Roof_information, " WHERE RC_SRID='"+cf.selectedhomeid+"' and RC_Condition_type_order<>'"+0012+"'");
  
    	if(RC_GRI.getCount()>0)
    	{
    		
    		RC_GRI.moveToFirst();
    		RCN_row=new TableRow[RC_GRI.getCount()];
    		for(int i=0;i<RC_GRI.getCount();i++)
    		{
    		String RC_Cond="",RC_Cond_desc="",RC_Cond_val="",RC_Cond_id="";
    		
    		
    		RC_Cond=cf.decode(RC_GRI.getString(2));
    		RC_Cond_desc=cf.decode(RC_GRI.getString(3));
    		RC_Cond_val=cf.decode(RC_GRI.getString(4));
    		RC_Cond_id=RC_GRI.getString(0);
    		
    		/**Seting value to the respective fields **/
    		
    		RCN_row[i] =(TableRow) getLayoutInflater().inflate(R.layout.roof_conditon_noted_inflat,null);
    		
    		String s="RCTablRow_"+i;
    		TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT); 
		 	lp.setMargins(2, 0, 2, 2); 
		 	RCN_row[i].setPadding(10, 0, 0, 0);  
		 	RCN_ShowValue.addView(RCN_row[i],lp);
		 	RCN_row[i].setTag(s);
		 	
    		RadioGroup GCH_RC_RG_CN=(RadioGroup) RCN_row[i].findViewWithTag("RCN_RG");
    		LinearLayout ED_Li_CN_h=(LinearLayout) RCN_row[i].findViewWithTag("RCN_Li_h");
    		LinearLayout ED_Li_par_CN=(LinearLayout) RCN_row[i].findViewWithTag("RCN_Li_par");
    		LinearLayout ED_Li_chaild_CN=(LinearLayout) RCN_row[i].findViewWithTag("RCN_Li_chi");
    		EditText ED_Li_CN=(EditText) RCN_row[i].findViewWithTag("RCN_Li_ed");
    		TextView ED_txt_CN=(TextView) RCN_row[i].findViewWithTag("RCN_Li_txt");
    		TextView ED_txt_CN_h=(TextView) RCN_row[i].findViewWithTag("text_head");
    		ED_Li_CN.setOnTouchListener(new RC_editclick()); 
    		ED_Li_CN.addTextChangedListener(new GCH_RC_watcher(ED_Li_par_CN, ED_Li_chaild_CN, ED_txt_CN,"500"));
    		GCH_RC_RG_CN.setOnCheckedChangeListener(new GCH_check_listener(ED_Li_CN,ED_Li_CN_h));
    		TextView RCN_txt_CN_id=(TextView) RCN_row[i].findViewWithTag("RCN_ID");
    		RCN_txt_CN_id.setText(RC_Cond_id);
    		EditText RCN_ED_CN_h=(EditText) RCN_row[i].findViewWithTag("Edit_head");
    		ImageView RCN_btn_edit=(ImageView) RCN_row[i].findViewWithTag("RCN_Edit");
    		RCN_btn_edit.setOnClickListener(new RCN_clicker(1));
    		ImageView RCN_btn_delete=(ImageView) RCN_row[i].findViewWithTag("RCN_Delete");
    		RCN_btn_delete.setOnClickListener(new RCN_clicker(2));
    		GCH_RC_RG_CN.setTag("RCN_RG_"+i);
    		ED_Li_CN_h.setTag("RCN_Li_h_"+i);
    		ED_Li_par_CN.setTag("RCN_Li_par_"+i);
    		ED_Li_chaild_CN.setTag("RCN_Li_chi_"+i);
    		ED_Li_CN.setTag("RCN_Li_ed_"+i);
    		ED_txt_CN.setTag("RCN_Li_txt_"+i);
    		ED_txt_CN_h.setTag("text_head_"+i);
    		RCN_txt_CN_id.setTag("RCN_ID_"+i);
    		RCN_ED_CN_h.setTag("Edit_head_"+i);
    		RCN_btn_edit.setTag("RCN_Edit_"+i);
    		RCN_btn_delete.setTag("RCN_Delete_"+i);
    		 
    		if(i>=4)
    		{
    			RCN_btn_edit.setVisibility(View.VISIBLE);
    			RCN_btn_delete.setVisibility(View.VISIBLE);
    			if(!RC_Cond.equals(""))
        		{
        			((TextView) RCN_row[i].findViewWithTag("text_head_"+i)).setText(RC_Cond);
        			((EditText) RCN_row[i].findViewWithTag("Edit_head_"+i)).setText(RC_Cond);
        		}
    			
    			
    		}
    		else
    		{
    			if(!RC_Cond.equals(""))
        		{
    				((TextView) RCN_row[i].findViewWithTag("text_head_"+i)).setText(Html.fromHtml(cf.redcolor+" "+RC_Cond));
        			((EditText) RCN_row[i].findViewWithTag("Edit_head_"+i)).setText(RC_Cond);
        			((RadioButton) RCN_row[i].findViewWithTag("Not Determined")).setVisibility(View.GONE);
        			((RadioButton) RCN_row[i].findViewWithTag("Beyond Scope of Inspection")).setVisibility(View.GONE);
        		}
    		}
    		if(!RC_Cond_val.equals(""))
    		{
    			
    			((RadioButton) ((RadioGroup) RCN_row[i].findViewWithTag("RCN_RG_"+i)).findViewWithTag(RC_Cond_val)).setChecked(true);
    			
	    		if(RC_Cond_val.equals("Yes"))
	    		{
	    				
		    		if(!RC_Cond_desc.equals(""))
		    		{
		    			((LinearLayout) RCN_row[i].findViewWithTag("RCN_Li_h_"+i)).setVisibility(View.VISIBLE);
		    			((EditText) RCN_row[i].findViewWithTag("RCN_Li_ed_"+i)).setText(RC_Cond_desc);
		    			
		    		}
	    		}
    		}
    		
    		RC_GRI.moveToNext();
    		
    	}
    	}
    	else
    	{
    		
    		String s[]={"Roof Damage Noted","Roof Repairs Noted","Roof Leakage Noted","Other Condition Present"};
    		RCN_row=new TableRow[4];
    		for(int i=0;i<4;i++)
    		{
    		
	    		String RC_Cond="",RC_Cond_desc="",RC_Cond_val="";
	    		
	    		RC_Cond=s[i];
	    		RC_Cond_desc="";
	    		RC_Cond_val="No";
	    		
	    		/**Seting value to the respective fields **/
	    		
	    		RCN_row[i] =(TableRow) getLayoutInflater().inflate(R.layout.roof_conditon_noted_inflat,null); 
	    		TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT); 
			 	lp.setMargins(2, 0, 2, 2); 
			 	RCN_row[i].setPadding(10, 0, 0, 0); 
			 	RCN_ShowValue.addView(RCN_row[i],lp);
			 	String s1="RCTablRow_"+i;
			 	RCN_row[i].setTag(s1);
	    		RadioGroup GCH_RC_RG_CN=(RadioGroup) RCN_row[i].findViewWithTag("RCN_RG");
	    		LinearLayout ED_Li_CN_h=(LinearLayout) RCN_row[i].findViewWithTag("RCN_Li_h");
	    		LinearLayout ED_Li_par_CN=(LinearLayout) RCN_row[i].findViewWithTag("RCN_Li_par");
	    		LinearLayout ED_Li_chaild_CN=(LinearLayout) RCN_row[i].findViewWithTag("RCN_Li_chi");
	    		EditText ED_Li_CN=(EditText) RCN_row[i].findViewWithTag("RCN_Li_ed");
	    		TextView ED_txt_CN=(TextView) RCN_row[i].findViewWithTag("RCN_Li_txt");
	    		TextView ED_txt_CN_h=(TextView) RCN_row[i].findViewWithTag("text_head");
	    		ED_Li_CN.setOnTouchListener(new RC_editclick()); 
	    		ED_Li_CN.addTextChangedListener(new GCH_RC_watcher(ED_Li_par_CN, ED_Li_chaild_CN, ED_txt_CN,"500"));
	    		GCH_RC_RG_CN.setOnCheckedChangeListener(new GCH_check_listener(ED_Li_CN,ED_Li_CN_h));
	    		TextView RCN_txt_CN_id=(TextView) RCN_row[i].findViewWithTag("RCN_ID");
	    		RCN_txt_CN_id.setText("");
	    		EditText RCN_ED_CN_h=(EditText) RCN_row[i].findViewWithTag("Edit_head");
	    		ImageView RCN_btn_edit=(ImageView) RCN_row[i].findViewWithTag("RCN_Edit");
	    		ImageView RCN_btn_delete=(ImageView) RCN_row[i].findViewWithTag("RCN_Delete");
	    		RCN_btn_edit.setOnClickListener(new RCN_clicker(1));
	    		RCN_btn_delete.setOnClickListener(new RCN_clicker(2));
	    		
	    		GCH_RC_RG_CN.setTag("RCN_RG_"+i);
	    		ED_Li_CN_h.setTag("RCN_Li_h_"+i);
	    		ED_Li_par_CN.setTag("RCN_Li_par_"+i);
	    		ED_Li_chaild_CN.setTag("RCN_Li_chi_"+i);
	    		ED_Li_CN.setTag("RCN_Li_ed_"+i);
	    		ED_txt_CN.setTag("RCN_Li_txt_"+i);
	    		ED_txt_CN_h.setTag("text_head_"+i);
	    		RCN_txt_CN_id.setTag("RCN_ID_"+i);
	    		RCN_ED_CN_h.setTag("Edit_head_"+i);
	    		RCN_btn_edit.setTag("RCN_Edit_"+i);
	    		RCN_btn_delete.setTag("RCN_Delete_"+i);
	    		
	    		((RadioButton) RCN_row[i].findViewWithTag("Not Determined")).setVisibility(View.GONE);
    			((RadioButton) RCN_row[i].findViewWithTag("Beyond Scope of Inspection")).setVisibility(View.GONE);
    			
	    		if(!RC_Cond.equals(""))  
	    		{
	    			//ED_txt_CN_h.setText(RC_Cond);
	    			ED_txt_CN_h.setText(Html.fromHtml(cf.redcolor+" "+RC_Cond));
	    			RCN_ED_CN_h.setText(RC_Cond);
	    		}
	    		   
	    		if(!RC_Cond_val.equals("")) 
	    		{
	    			
	    			((RadioButton) GCH_RC_RG_CN.findViewWithTag(RC_Cond_val)).setChecked(true);
	    			
	    		}
	    		
	    		 
	    		
    		}
    	
    	}
    	/**condtion noted value setting ends***/
    	/**year And ERAE and cmt value setting Starts***/
    
    	
    	 Cursor RC_GRI_1=cf.SelectTablefunction(cf.General_Roof_information, " WHERE RC_SRID='"+cf.selectedhomeid+"' and RC_Condition_type_order='10'");
    	 System.out.println("comes i the 1");
     	if(RC_GRI_1.getCount()>0)
     	{ RC_GRI_1.moveToFirst();
     	System.out.println("comes i the 2");
     		for(int i=0;i<RC_GRI_1.getCount();i++)
     		{
     			System.out.println("comes i the 3"+cf.decode(RC_GRI_1.getString(4)));
	     			if(RC_GRI_1.getString(2).equals("Excessive Roof Aging evident"))
	     			{
	     			 ((RadioButton) GCH_RC_RG_ERAE.findViewWithTag(cf.decode(RC_GRI_1.getString(4)).trim())).setChecked(true);
	     			}
	     			else if(RC_GRI_1.getString(2).equals("3 Year Replacement Probability"))
	     			{
	        			 ((RadioButton) GCH_RC_RG_3YR.findViewWithTag(RC_GRI_1.getString(4))).setChecked(true);
	        		}
	     			else if(RC_GRI_1.getString(2).equals("Comments"))
	     			{
	     				RC_cmt.setText(cf.decode(RC_GRI_1.getString(3)));
	     			}
	     			System.out.println("comes i the 4");
	     			RC_GRI_1.moveToNext();
	     			
     			}
     		
     	}
     	
     		/**year And ERAE and cmt value setting ENds***/
    System.out.println("her not isseus in the GRI");	
	}
    class RCN_clicker implements OnClickListener
    {
    	
    	int type;
    	RCN_clicker(int type)
    	{
    		
    		this.type=type;
    	}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			String s=((ImageView)  v).getTag().toString().trim();
			int pos=s.lastIndexOf("_");
			String tmp=s.substring(pos+1,s.length());
			 int i = 0;
			try
			{
				i=Integer.parseInt(tmp);
			}catch (Exception e) {
				// TODO: handle exception
				
				
			}
			
			if(type==2)
			{
				final int k=i;
				
				AlertDialog.Builder builder = new AlertDialog.Builder(RoofSection.this);
				builder.setMessage("Are you sure, Do you want to delete this Roof condition noted?")
						.setCancelable(false)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int id) {
										
										RCN_row=Delete_from_array(RCN_row, k);
									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

										dialog.cancel();
									}
								});
				builder.show();
				
				
				
				//show_GRI_Value();
				
				
			}
			else if(type==1)
			{
				((TextView) RCN_row[i].findViewWithTag("text_head_"+i)).setVisibility(View.GONE);
				((EditText) RCN_row[i].findViewWithTag("Edit_head_"+i)).setVisibility(View.VISIBLE);
				((ImageView) v).setVisibility(View.GONE);
				
			}
		}
    	
    }
	class GCH_check_listener implements OnCheckedChangeListener
	{
    	EditText ed;
    	LinearLayout li;
    	GCH_check_listener(EditText ed,LinearLayout li)
    	{
    		this.ed=ed;
    		this.li=li;
    	}
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			if(((RadioButton) findViewById(group.getCheckedRadioButtonId())).getText().toString().trim().equals("Yes"))
			{
				
				
				//ed.setFocusableInTouchMode(true);
				//ed.requestFocus();
				//ed.setCursorVisible(true);
				//ed.setText("");
				li.setVisibility(View.VISIBLE);
			}
			else
			{
				
				ed.setText("");
				
				li.setVisibility(View.GONE);
			}
		}
	}

	class GCH_RC_watcher implements TextWatcher
    {
    	String size;
    	LinearLayout par,chi;
    	TextView tv;
    	
    	GCH_RC_watcher(LinearLayout par,LinearLayout chi,TextView tv,String s)
    	{
    		this.size=s;
    		this.par=par;
    		this.chi=chi;
    		this.tv=tv;
    	}

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			cf.showing_limit(s.toString(),par,chi,tv,size);
			
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			
		}
    	
    }
    
	private void show_RCT_Value() {
		// TODO Auto-generated method stub
		cf.Create_Table(21);
		Cursor RCT_c= cf.SelectTablefunction(cf.Roof_cover_type, " Where RC_RCT_SRID='"+cf.selectedhomeid+"' ");
		
		if(RCT_c.getCount()>0)
		{
			b1=true;
			RCT_c.moveToFirst();
			
			try
			{
			RCT_ShowValue.removeAllViews();
			}
			catch (Exception e) {
				// TODO: handle exception
				System.out.println("Not issue"+e.getLocalizedMessage());
			}
			TableRow th= (TableRow) getLayoutInflater().inflate(R.layout.roof_listview_header, null); 
			TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
		 	lp.setMargins(2, 0, 2, 2); 
		 	th.setPadding(10, 0, 0, 0);
		 	
		 	RCT_ShowValue.addView(th,lp);
			RCT_in_DB=new String[RCT_c.getCount()];
			RCT_in_DB_id=new int[RCT_c.getCount()];
			RCT_ShowValue.setVisibility(View.VISIBLE);
			for(int i=0;i<RCT_c.getCount();i++)
			{
				
				/*try
				{
					RCT_ShowValue.removeViewAt(i+1);
				}
				catch(Exception e)
				{
					System.out.println("error in removing the value"+e.getMessage());
				}
				*/TextView no,RCT,PAD,YIR,RS,RSh,RSt,PD;
				 ImageView edit,delete;
				String RCT_s="",PAD_s="",YIR_s="",RS_s="",RSh_s="",RSt_s="",PD_s="",per="";
				
				RCT_s=cf.decode(RCT_c.getString(2));
				RCT_in_DB[i]=RCT_s;
				RCT_in_DB_id[i]=RCT_c.getInt(0);
				
				PAD_s=cf.decode(RCT_c.getString(4));
				
				YIR_s=cf.decode(RCT_c.getString(5));
				
				
				RS_s=cf.decode(RCT_c.getString(6));
				
				RSh_s=(cf.decode(RCT_c.getString(7)).trim().equals("Other"))? cf.decode(RCT_c.getString(8)):cf.decode(RCT_c.getString(7));
				
				if(!cf.decode(RCT_c.getString(12)).equals(""))
				{
				RSh_s +=" \n("+cf.decode(RCT_c.getString(12))+"%)";
					
				}
				
				per=cf.decode(RCT_c.getString(12));
				
				
				try
				{
					Per_in_DB += Integer.parseInt(per);
				}
				catch(Exception e)
				{
					
					System.out.println("You have problem in converting string to number in set_RCT value func");
				}
				RSt_s=(cf.decode(RCT_c.getString(9)).trim().equals("Other"))? cf.decode(RCT_c.getString(10)):cf.decode(RCT_c.getString(9));
				
				PD_s=cf.decode(RCT_c.getString(11));
				
				
				
				TableRow t= (TableRow) getLayoutInflater().inflate(R.layout.roof_listview, null);
			 	t.setId(44444+i);/// Set some id for further use
			 	
			 	
			 	no= (TextView) t.findViewWithTag("RC_RCT_No_1");
			 	no.setText(String.valueOf(i+1));
			 	RCT= (TextView) t.findViewWithTag("RC_RCT_RCT_1");
			 	RCT.setText(RCT_s);
			 				 	
			 	PAD= (TextView) t.findViewWithTag("RC_RCT_PAD_1");
			 	PAD.setText(PAD_s);
			 	
			 	YIR= (TextView) t.findViewWithTag("RC_RCT_YIR_1");
			 	if(YIR_s.trim().equals(""))
			 		YIR.setText("-");
			 	else
			 		YIR.setText(YIR_s);
			 	
			 	RS= (TextView) t.findViewWithTag("RC_RCT_RS_1");
			 	if(RS_s.trim().equals(""))
			 		RS.setText("-");
			 	else
			 		RS.setText(RS_s);
			 	
			 	RSh= (TextView) t.findViewWithTag("RC_RCT_RSh_1");
			 	if(RSh_s.trim().equals(""))
			 		RSh.setText("-");
			 	else
			 		RSh.setText(Html.fromHtml(RSh_s));
			 	
			 	
			 	
			 	RSt= (TextView) t.findViewWithTag("RC_RCT_RSt_1");
			 	if(RSt_s.trim().equals(""))
			 		RSt.setText("-");
			 	else
			 		RSt.setText(RSt_s);
			 	
			 //	RSt.setText(RSt_s);
			 	
			 	PD= (TextView) t.findViewWithTag("RC_RCT_PD_1");
			 	if(PD_s.trim().equals(""))
			 		PD.setText("-");
			 	else
			 		PD.setText(PD_s);
			 	//PD.setText(PD_s);
			 	CheckBox  NIP= (CheckBox) t.findViewWithTag("RC_RCT_NIP_1");
			 	if(RCT_c.getString(13).equals("1"))
				 	NIP.setChecked(true);
			 	RadioButton  Dom= (RadioButton) t.findViewWithTag("RC_RCT_Dom_1");
			 	if(RCT_c.getString(14).equals("1"))
			 	{
			 		Dom.setChecked(true);
			 		pre_dom=true;
			 	}
			 		
			 	
			 	edit= (ImageView) t.findViewWithTag("RC_RCT_edit_1");
			 	edit.setId(789456+i);
			 	edit.setTag(RCT_c.getString(0));
			 	edit.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
					try
					{
					int i=Integer.parseInt(v.getTag().toString());
					update_RCT(i);
					}
					catch (Exception e) {
						// TODO: handle exception
					}
					}

					private void update_RCT(int i) {
						// TODO Auto-generated method stub
						Current_select_id=i;
						Cursor RCT_c=cf.SelectTablefunction(cf.Roof_cover_type, " Where RCT_Id='"+i+"'");
						if(RCT_c.getCount()>0)
						{
							RCT_c.moveToFirst();
							String RCT_s="",PAD_s="",YIR_s="",RS_s="",RSh_s="",RSt_s="",PD_s="",per="";
							RCT_s=(cf.decode(RCT_c.getString(2)).trim().equals("Other"))? cf.decode(RCT_c.getString(3)):cf.decode(RCT_c.getString(2));
							PAD_s=cf.decode(RCT_c.getString(4));
							
							YIR_s=cf.decode(RCT_c.getString(5));
							RS_s=cf.decode(RCT_c.getString(6));
							RSh_s=(cf.decode(RCT_c.getString(7)).trim().equals("Other"))? cf.decode(RCT_c.getString(8)):cf.decode(RCT_c.getString(7));
							//RSh_s +="  "+cf.decode(RCT_c.getString(12))+" %";
							per=cf.decode(RCT_c.getString(12));
							RSt_s=(cf.decode(RCT_c.getString(9)).trim().equals("Other"))? cf.decode(RCT_c.getString(10)):cf.decode(RCT_c.getString(9));
							PD_s=cf.decode(RCT_c.getString(11));
							
							
							/** Set the value to the respective fields***/
							
							
							int RCT_pos=0,YIR_pos=0,RS_pos=0,RSh_pos=0,RSt_pos=0,PD_pos=0;
							GCH_RC_PA_ed.setText(PAD_s);
							
								RCT_pos=adapter5.getPosition(RCT_s);
								if(!RCT_s.equals(""))
								{
									if(RCT_pos!=-1)
									{
									
									GCH_RC_RCT_sp.setSelection(RCT_pos);
									
									}else
									{
									RCT_pos=0;
									GCH_RC_RCT_sp.setSelection(adapter5.getPosition("Other"));
									GCH_RC_RCT_Other.setVisibility(View.VISIBLE);
									GCH_RC_RCT_Other.setText(RCT_s);
									
									}
								}
								YIR_pos=adapter1.getPosition(YIR_s);
								if(!YIR_s.equals(""))
								{	
									if(YIR_pos!=-1)
									{
										
										GCH_RC_YIR_sp.setSelection(YIR_pos);
									}else
									{
										YIR_pos=0;
										GCH_RC_YIR_sp.setSelection(adapter1.getPosition("Other"));
										GCH_RC_YIR_Other.setText(YIR_s);
										GCH_RC_YIR_Other.setVisibility(View.VISIBLE);
									}
								}
							RS_pos=adapter2.getPosition(RS_s);
							if(!RS_s.equals(""))
							{
								if(RS_pos!=-1)
								{
									
									GCH_RC_RS_sp.setSelection(RS_pos);
								}else
								{
									RS_pos=0;
								}
							}
							RSh_pos=adapter3.getPosition(RSh_s);
							if(!RSh_s.equals(""))
							{
								if(RSh_pos!=-1)
								{
									
									GCH_RC_Rsh_sp.setSelection(RSh_pos);
									GCH_RC_Rsh_per.setText(cf.decode(RCT_c.getString(12)));
								}else
								{
								
									RSh_pos=0;
									GCH_RC_Rsh_sp.setSelection(adapter3.getPosition("Other"));
									GCH_RC_Rsh_Other.setVisibility(View.VISIBLE);
									GCH_RC_Rsh_Other.setText(RSh_s);
									
									GCH_RC_Rsh_per.setText(cf.decode(RCT_c.getString(12)));
									
								}
							}
							RSt_pos=adapter4.getPosition(RSt_s);
							if(!RSt_s.equals(""))
							{
								if(RSt_pos!=-1)
								{
									
									GCH_RC_RSt_sp.setSelection(RSt_pos);
								}else
								{
									RSt_pos=0;
									GCH_RC_RSt_sp.setSelection(adapter4.getPosition("Other"));
									GCH_RC_RSt_Other.setVisibility(View.VISIBLE);
									GCH_RC_RSt_Other.setText(RSt_s);
									
								}
							}
							PD_pos=adapter6.getPosition(PD_s);
							if(!PD_s.equals(""))
							{
								if(PD_pos!=-1)
								{
									
									GCH_RC_PD_sp.setSelection(PD_pos);
								}else
								{
									PD_pos=0;
									
								}
							}
							RC_Dom_rd.setChecked(false);
							RC_NIP_chk.setChecked(false);
							if(RCT_c.getString(13).equals("1"))
							{
								RC_NIP_chk.setChecked(true);
								GCH_RC_PA_ed.setText("Not Applicable");
								GCH_RC_PA_ed.setEnabled(false);
								GCH_RC_PA_DP.setEnabled(false);
									
							}
							else
							{
								//if(!GCH_RC_PA_ed.getText().toString().contains("/"))
								//GCH_RC_PA_ed.setText("");
								GCH_RC_PA_ed.setEnabled(false);
								GCH_RC_PA_DP.setEnabled(true);
								
							}
							
							if(RCT_c.getString(14).equals("1"))
							{
								RC_Dom_rd.setChecked(true);
							}
							GCH_RC_RCT_SA.setText("Update and Add more");
							
						 	
						 	
						 	
							
							/** Set the value to the respective fields Ends***/
							
						}
						
					}
				});
			 	
			 	delete=(ImageView) t.findViewWithTag("RC_RCT_edit_2");
			 	delete.setTag(RCT_c.getString(0));
			 	delete.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(final View v) {
						// TODO Auto-generated method stub
						
						AlertDialog.Builder builder = new AlertDialog.Builder(RoofSection.this);
						builder.setMessage("Are you sure, Do you want to delete the Roof Cover Type?")
								.setCancelable(false)
								.setPositiveButton("Yes",
										new DialogInterface.OnClickListener() {

											public void onClick(DialogInterface dialog,
													int id) {
												
												try
												{
												int i=Integer.parseInt(v.getTag().toString());
												if(i==Current_select_id)
												{
													GCH_RC_RCT_SA.setText("Save and Add more");
													Current_select_id=0;
												}
												cf.gch_db.execSQL("Delete from "+cf.Roof_cover_type+" Where RCT_Id='"+i+"'");
												cf.ShowToast("Your information deleted successfully", 0);
												show_RCT_Value();
												}
												catch (Exception e) {
													// TODO: handle exception
												}
											}
										})
								.setNegativeButton("No",
										new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog,
													int id) {

												dialog.cancel();
											}
										});
						builder.show();
					
						
					}
				});
			 	
			 	t.setPadding(10, 0, 0, 0);
			 	RCT_ShowValue.addView(t,lp);
			 	RCT_c.moveToNext();
			 	
			}
			
		}
		else
		{
			
			b1=false;
			RCT_ShowValue.setVisibility(View.GONE);
			RCT_in_DB=null;
			Per_in_DB=0;
			
			
		}
		
	 	
	}
	
	class RC_editclick implements OnTouchListener
	{
		
	@Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			((EditText) v).setFocusableInTouchMode(true);
			((EditText) v).requestFocus();
			 ((EditText) v).setCursorVisible(true);
			return false;
		}
		
	}
	class RC_clicker implements OnClickListener
	{
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
		
			switch (v.getId()) {
			case R.id.GCH_RC_PA_DP:
				GCH_RC_PA_ed.setText("");
				DATE_DIALOG_ID=0;
				showDialog(0);
			break;
			case R.id.GCH_RC_RCT_SA:
				Save_RCT();
			break;
			case R.id.GCH_RC_RCT_can:
				clear_RCT();
			break;
			case R.id.RC_AMC:
				Add_more_condtion();
			break;
			
			default:
				break;
			}
			
		
		}

		
	
	}
	private void Save_RCT() {
		// TODO Auto-generated method stub
		boolean b[]=new boolean[3];
		int per=0;
		String save_RCT,save_RCT_Other,save_PAD,save_YIR,save_RS,save_RSh,save_RSh_per,save_RSh_Other,save_RSt,save_RSt_Other,save_PD;
		save_RCT=GCH_RC_RCT_sp.getSelectedItem().toString().trim();
		save_RCT_Other=(save_RCT.equals("Other"))? GCH_RC_RCT_Other.getText().toString().trim():"";
		
		save_PAD = GCH_RC_PA_ed.getText().toString().trim();
		
		save_YIR = GCH_RC_YIR_sp.getSelectedItem().toString().trim();
		save_YIR =(save_YIR.equals("Other"))?GCH_RC_YIR_Other.getText().toString().trim():save_YIR;
		
		save_RS = GCH_RC_RS_sp.getSelectedItem().toString().trim();
		
		save_RSh=GCH_RC_Rsh_sp.getSelectedItem().toString().trim();
		save_RSh_Other=(save_RSh.equals("Other"))? GCH_RC_Rsh_Other.getText().toString().trim():"";
		save_RSh_per = GCH_RC_Rsh_per.getText().toString().trim();
		
		try
		{
		  per=	Integer.parseInt(save_RSh_per);
				  
		}
		catch(Exception e)
		{
		 System.out.println("issues comes here "+e.getMessage());	
		}
		
		
		save_RSt=GCH_RC_RSt_sp.getSelectedItem().toString().trim();
		save_RSt_Other=(save_RSt.equals("Other"))? GCH_RC_RSt_Other.getText().toString().trim():"";
		
		save_PD = GCH_RC_PD_sp.getSelectedItem().toString().trim();
		
		b[0]=true;
		if(GCH_RC_RCT_SA.getText().toString().trim().equals("Save and Add more"))
		{
		if(RCT_in_DB!=null)
		{
			for(int i=0;i<RCT_in_DB.length;i++)
			{
				if((RCT_in_DB[i].equals(save_RCT)))
				{
					b[0]=false;
				}
			}
		}
		}else {
			if(RCT_in_DB!=null)
			{
				for(int i=0;i<RCT_in_DB.length;i++)
				{
					if((RCT_in_DB[i].equals(save_RCT)) && Current_select_id != RCT_in_DB_id[i])
					{
						b[0]=false;
					}
				}
			}
		}
			
		
		b[1]=false;
		if(per>0 && per<=100)
		{
			
			b[1]=true;
		}
		
		
		b[2]=true;
	/*	if(per+Per_in_DB<=100)
		{
			
			b[2]=true;
		}
	*/	
		
		if(!save_RCT.equals("--Select--"))
		{
			if((save_RCT.equals("Other") && !save_RCT_Other.equals("") ) || !save_RCT.equals("Other") )
			{
				
				if(b[0])
				{
					if(!RC_NIP_chk.isChecked())
					{
				if(!save_PAD.equals(""))
				{
					if(checkfordateistoday(save_PAD).equals("true"))
					{
							if(!save_YIR.equals("--Select--") && !save_YIR.equals(""))
							{
								if(!save_RS.equals("--Select--"))
								{
									if(!save_RSh.equals("--Select--"))
									{
										if((save_RSh.equals("Other") && !save_RSh_Other.equals("") ) || !save_RSh.equals("Other") )
										{
											if(!save_RSh_per.equals(""))
											{
												if(b[1] && b[2])
												{
											if(!save_RSt.equals("--Select--"))
											{
												
												if((save_RSt.equals("Other") && !save_RSt_Other.equals("") ) || !save_RSt.equals("Other") )
												{
													if(!save_PD.equals("--Select--"))
													{ try
													{
														String dom="0";
														cf.Create_Table(21);
														/*cf.SelectTablefunction(cf.Roof_cover_type, " Where RC_RCT_SRID='"+cf.selectedhomeid+"' and RC_RCT_RCT='"+cf.encode()+""' ")*/
														if(GCH_RC_RCT_SA.getText().toString().trim().equals("Save and Add more"))
														{
															if(RC_Dom_rd.isChecked())
															{
																cf.gch_db.execSQL("UPDATE "+cf.Roof_cover_type+" set RC_RCT_Domi='0' WHERE RC_RCT_SRID='"+cf.selectedhomeid+"' ");
																dom="1";
															}
														cf.gch_db.execSQL("Insert into "+cf.Roof_cover_type+" (RC_RCT_SRID,RC_RCT_RCT,RC_RCT_RCT_Other,RC_RCT_PAD,RC_RCT_YIR,RC_RCT_RS,RC_RCT_RSh,RC_RCT_RSh_Other,RC_RCT_RSt,RC_RCT_RSt_Other,RC_RCT_PD,RC_RCT_RSh_per,RC_RCT_NIP,RC_RCT_Domi) " +
													 		"VALUES ('"+cf.selectedhomeid+"','"+cf.encode(save_RCT)+"','"+cf.encode(save_RCT_Other)+"','"+cf.encode(save_PAD)+"','"+cf.encode(save_YIR)+"','"+cf.encode(save_RS)+"','"+cf.encode(save_RSh)+"','"+cf.encode(save_RSh_Other)+"','"+cf.encode(save_RSt)+"','"+cf.encode(save_RSt_Other)+"','"+cf.encode(save_PD)+"','"+cf.encode(save_RSh_per)+"','0','"+dom+"')");
														cf.ShowToast("Your Roof Type has been saved succesfully. You can add more Roof Cover Type ", 1);
														}
														else
														{
															if(RC_Dom_rd.isChecked())
															{
																cf.gch_db.execSQL("UPDATE "+cf.Roof_cover_type+" set RC_RCT_Domi='0' WHERE RC_RCT_SRID='"+cf.selectedhomeid+"' ");
																dom="1";
															}
															cf.gch_db.execSQL("UPDATE  "+cf.Roof_cover_type+" SET RC_RCT_RCT='"+cf.encode(save_RCT)+"',RC_RCT_RCT_Other='"+cf.encode(save_RCT_Other)+"',RC_RCT_PAD='"+cf.encode(save_PAD)+"',RC_RCT_YIR='"+cf.encode(save_YIR)+"',RC_RCT_RS='"+cf.encode(save_RS)+"',RC_RCT_RSh='"+cf.encode(save_RSh)+"',RC_RCT_RSh_Other='"+cf.encode(save_RSh_Other)+"',RC_RCT_RSt='"+cf.encode(save_RSt)+"',RC_RCT_RSt_Other='"+cf.encode(save_RSt_Other)+"',RC_RCT_PD='"+cf.encode(save_PD)+"',RC_RCT_RSh_per='"+cf.encode(save_RSh_per)+"',RC_RCT_NIP='0',RC_RCT_Domi='"+dom+"' Where RCT_Id='"+Current_select_id+"' ");
															
															cf.ShowToast("Your Roof Type has been updated succesfully.", 1);
														}
														show_RCT_Value();
														clear_RCT();
														
													}
													catch(Exception e)
													{
														System.out.println("Issues available"+e.getMessage());
													}
														
													}
													else
													{
														cf.ShowToast("Please Select for Building Code", 0);
													}
												}
												else
												{
													cf.ShowToast("Please Enter the Other text for Roof Structure ", 0);
												}
												
											}
											else
											{
												cf.ShowToast("Please select the Roof Structure  ", 0);
											}
											
											}
											
											else
											{
												if(!b[1])
												{
													cf.ShowToast("Please Enter the Percentage for Roof Shape between 0 to 100 ", 0);
												}
												else if(!b[2])
												{
													
													cf.ShowToast("Please Enter the Percentage for Roof Shape less than"+(100-(per+Per_in_DB))+" You have already entered "+per+Per_in_DB +" For Some other Roof Cover Types " , 0);
													
												}
													
												
											}
										}
										else
										{
											cf.ShowToast("Please Enter the Percentage for Roof Shape ", 0);
										}
									}
										else
										{
											cf.ShowToast("Please Enter the Other text for Roof Shape ", 0);
										}
										
									}
									else
									{
										cf.ShowToast("Please select the Roof Shape  ", 0);
									}
									
								}
								else
								{
									cf.ShowToast("Please select the Roof Slope  ", 0);
								}
								
							}
							else
							{
								cf.ShowToast("Please select the Year of Original Installation or Replacement   ", 0);
							}
					}else
					{
						cf.ShowToast("Please Enter the Permit Application Date less then Todays date  ", 0);
					}
							
				}
				else
				{
					cf.ShowToast("Please select the  Permit Application Date  ", 0);
				}
					}
					else
					{
						try
						{
							if(save_RCT.equals("--Select--"))
							{
								save_RCT="";
							}
							if(save_YIR.equals("--Select--"))
							{
								save_YIR="";
							}
							if(save_RS.equals("--Select--"))
							{
								save_RS="";
							}
							if(save_RSh.equals("--Select--"))
							{
								save_RSh="";
							}
							if(save_RSt.equals("--Select--"))
							{
								save_RSt="";
							}
							if(save_PD.equals("--Select--"))
							{
								save_PD="";
							}
							
							cf.Create_Table(21);
							/*cf.SelectTablefunction(cf.Roof_cover_type, " Where RC_RCT_SRID='"+cf.selectedhomeid+"' and RC_RCT_RCT='"+cf.encode()+""' ")*/
							String dom="0";
							System.out.println("no issues "+GCH_RC_RCT_SA.getText().toString());
							if(GCH_RC_RCT_SA.getText().toString().trim().equals("Save and Add more"))
							{
								
								if(RC_Dom_rd.isChecked())
								{
									cf.gch_db.execSQL("UPDATE "+cf.Roof_cover_type+" set RC_RCT_Domi='0' WHERE RC_RCT_SRID='"+cf.selectedhomeid+"' ");
									dom="1";
								}
							cf.gch_db.execSQL("Insert into "+cf.Roof_cover_type+" (RC_RCT_SRID,RC_RCT_RCT,RC_RCT_RCT_Other,RC_RCT_PAD,RC_RCT_YIR,RC_RCT_RS,RC_RCT_RSh,RC_RCT_RSh_Other,RC_RCT_RSt,RC_RCT_RSt_Other,RC_RCT_PD,RC_RCT_RSh_per,RC_RCT_NIP,RC_RCT_Domi) " +
						 		"VALUES ('"+cf.selectedhomeid+"','"+cf.encode(save_RCT)+"','"+cf.encode(save_RCT_Other)+"','"+cf.encode(save_PAD)+"','"+cf.encode(save_YIR)+"','"+cf.encode(save_RS)+"','"+cf.encode(save_RSh)+"','"+cf.encode(save_RSh_Other)+"','"+cf.encode(save_RSt)+"','"+cf.encode(save_RSt_Other)+"','"+cf.encode(save_PD)+"','"+cf.encode(save_RSh_per)+"','1','"+dom+"')");
							cf.ShowToast("Your Roof Type has been saved succesfully. You add more Roof Cover Type ", 1);
							}
							else
							{
								
								if(RC_Dom_rd.isChecked())
								{
									cf.gch_db.execSQL("UPDATE "+cf.Roof_cover_type+" set RC_RCT_Domi='0' WHERE RC_RCT_SRID='"+cf.selectedhomeid+"' ");
									dom="1";
								}
								cf.gch_db.execSQL("UPDATE  "+cf.Roof_cover_type+" SET RC_RCT_RCT='"+cf.encode(save_RCT)+"',RC_RCT_RCT_Other='"+cf.encode(save_RCT_Other)+"',RC_RCT_PAD='"+cf.encode(save_PAD)+"',RC_RCT_YIR='"+cf.encode(save_YIR)+"',RC_RCT_RS='"+cf.encode(save_RS)+"',RC_RCT_RSh='"+cf.encode(save_RSh)+"',RC_RCT_RSh_Other='"+cf.encode(save_RSh_Other)+"',RC_RCT_RSt='"+cf.encode(save_RSt)+"',RC_RCT_RSt_Other='"+cf.encode(save_RSt_Other)+"',RC_RCT_PD='"+cf.encode(save_PD)+"',RC_RCT_RSh_per='"+cf.encode(save_RSh_per)+"',RC_RCT_NIP='1',RC_RCT_Domi='"+dom+"' Where RCT_Id='"+Current_select_id+"' ");
								cf.ShowToast("Your Roof Type has been updated succesfully.", 1);
							}
							show_RCT_Value();
							clear_RCT();
							
						}
						catch(Exception e)
						{
							System.out.println("Issues available"+e.getMessage());
						}
					}
				}
				else
				{
					//cf.ShowToast("Please Select Someother Roof Cover Type . You have already added this Roof ="+save_RCT, 0);
					cf.ShowToast("This Roof cover type already Exists", 0);
					
				}
				
				
			}
			else
			{
				cf.ShowToast("Please Enter the Other text for Roof Cover Type ", 0);
			}
		}
		else
		{
			cf.ShowToast("Please select the Roof Cover Type ", 0);
		}
		
	}

	public void Add_more_condtion() {
		// TODO Auto-generated method stub
		RCN_row=dynamicarraysetting(RCN_row);
		
		int i=RCN_row.length-1;
		RCN_row[i] =(TableRow) getLayoutInflater().inflate(R.layout.roof_conditon_noted_inflat,null); 
		TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT); 
	 	lp.setMargins(2, 0, 2, 2); 
	 	RCN_row[i].setPadding(10, 0, 0, 0); 
	 	RCN_ShowValue.addView(RCN_row[i],lp);
	 	String s="RCTablRow_"+i;
	 	RCN_row[i].setTag(s);
		RadioGroup GCH_RC_RG_CN=(RadioGroup) RCN_row[i].findViewWithTag("RCN_RG");
		LinearLayout ED_Li_CN_h=(LinearLayout) RCN_row[i].findViewWithTag("RCN_Li_h");
		LinearLayout ED_Li_par_CN=(LinearLayout) RCN_row[i].findViewWithTag("RCN_Li_par");
		LinearLayout ED_Li_chaild_CN=(LinearLayout) RCN_row[i].findViewWithTag("RCN_Li_chi");
		EditText ED_Li_CN=(EditText) RCN_row[i].findViewWithTag("RCN_Li_ed");
		TextView ED_txt_CN=(TextView) RCN_row[i].findViewWithTag("RCN_Li_txt");
		TextView ED_txt_CN_h=(TextView) RCN_row[i].findViewWithTag("text_head");
		ED_Li_CN.setOnTouchListener(new RC_editclick()); 
		ED_Li_CN.addTextChangedListener(new GCH_RC_watcher(ED_Li_par_CN, ED_Li_chaild_CN, ED_txt_CN,"500"));
		GCH_RC_RG_CN.setOnCheckedChangeListener(new GCH_check_listener(ED_Li_CN,ED_Li_CN_h));
		TextView RCN_txt_CN_id=(TextView) RCN_row[i].findViewWithTag("RCN_ID");
		RCN_txt_CN_id.setText("");
		
		EditText RCN_ED_CN_h=(EditText) RCN_row[i].findViewWithTag("Edit_head");
		ImageView RCN_btn_edit=(ImageView) RCN_row[i].findViewWithTag("RCN_Edit");
		ImageView RCN_btn_delete=(ImageView) RCN_row[i].findViewWithTag("RCN_Delete");
		GCH_RC_RG_CN.setTag("RCN_RG_"+i);
		ED_Li_CN_h.setTag("RCN_Li_h_"+i);
		ED_Li_par_CN.setTag("RCN_Li_par_"+i);
		ED_Li_chaild_CN.setTag("RCN_Li_chi_"+i);
		ED_Li_CN.setTag("RCN_Li_ed_"+i);
		ED_txt_CN.setTag("RCN_Li_txt_"+i);
		ED_txt_CN_h.setTag("text_head_"+i);
		RCN_txt_CN_id.setTag("RCN_ID_"+i);
		RCN_ED_CN_h.setTag("Edit_head_"+i);
		RCN_ED_CN_h.setTag("Edit_head_"+i);
		RCN_btn_edit.setTag("RCN_Edit_"+i);
		RCN_btn_delete.setTag("RCN_Delete_"+i);
		
		ED_txt_CN_h.setText("");
		((RadioButton) GCH_RC_RG_CN.findViewWithTag("No")).setChecked(true);
		RCN_btn_delete.setVisibility(View.VISIBLE);
		RCN_ED_CN_h.setVisibility(View.VISIBLE);
		ED_txt_CN_h.setVisibility(View.GONE);
		RCN_btn_edit.setOnClickListener(new RCN_clicker(1));
		RCN_btn_delete.setOnClickListener(new RCN_clicker(2));
		
	}

	private TableRow[] dynamicarraysetting(TableRow[] pieces3) {
		// TODO Auto-generated method stub
		 try
		 {
		if(pieces3==null)
		{
			pieces3=new TableRow[1];
			
			//pieces3[0]=ErrorMsg;
		}
		else
		{
			
			TableRow tmp[]=new TableRow[pieces3.length+1];
			int i;
			for(i =0;i<pieces3.length;i++)
			{
				
				tmp[i]=pieces3[i];
				
			}
		
			//tmp[tmp.length-1]=ErrorMsg;
			pieces3=null;
			pieces3=tmp.clone();
		}
		 }
		 catch(Exception e)
		 {
			 System.out.println("Exception "+e.getMessage());
			
		 }
		return pieces3;
	}
	protected TableRow[] Delete_from_array(TableRow[] selectedtestcaption2,int txt) {
		// TODO Auto-generated method stub
		TableRow tmp[]=null;
		if(selectedtestcaption2!=null)
		{
			
			tmp=new TableRow[selectedtestcaption2.length-1];
		int j=0;
			for(int i=0;i<selectedtestcaption2.length;i++)
			{
				if(selectedtestcaption2[i]!=null)
				{
					
					if(i!=txt)
					{
						tmp[j]=selectedtestcaption2[i];
						RadioGroup GCH_RC_RG_CN=(RadioGroup) RCN_row[i].findViewWithTag("RCN_RG_"+i);
						LinearLayout ED_Li_CN_h=(LinearLayout) RCN_row[i].findViewWithTag("RCN_Li_h_"+i);
						LinearLayout ED_Li_par_CN=(LinearLayout) RCN_row[i].findViewWithTag("RCN_Li_par_"+i);
						LinearLayout ED_Li_chaild_CN=(LinearLayout) RCN_row[i].findViewWithTag("RCN_Li_chi_"+i);
						EditText ED_Li_CN=(EditText) RCN_row[i].findViewWithTag("RCN_Li_ed_"+i);
						TextView ED_txt_CN=(TextView) RCN_row[i].findViewWithTag("RCN_Li_txt_"+i);
						TextView ED_txt_CN_h=(TextView) RCN_row[i].findViewWithTag("text_head_"+i);
						TextView RCN_txt_CN_id=(TextView) RCN_row[i].findViewWithTag("RCN_ID_"+i);
						EditText RCN_ED_CN_h=(EditText) RCN_row[i].findViewWithTag("Edit_head_"+i);
						ImageView RCN_btn_edit=(ImageView) RCN_row[i].findViewWithTag("RCN_Edit_"+i);
						ImageView RCN_btn_delete=(ImageView) RCN_row[i].findViewWithTag("RCN_Delete_"+i);
						
						GCH_RC_RG_CN.setTag("RCN_RG_"+j);
						ED_Li_CN_h.setTag("RCN_Li_h_"+j);
						ED_Li_par_CN.setTag("RCN_Li_par_"+j);
						ED_Li_chaild_CN.setTag("RCN_Li_chi_"+j);
						ED_Li_CN.setTag("RCN_Li_ed_"+j);
						ED_txt_CN.setTag("RCN_Li_txt_"+j);
						ED_txt_CN_h.setTag("text_head_"+j);
						RCN_txt_CN_id.setTag("RCN_ID_"+j);
						RCN_ED_CN_h.setTag("Edit_head_"+j);
						RCN_ED_CN_h.setTag("Edit_head_"+j);
						RCN_btn_edit.setTag("RCN_Edit_"+j);
						RCN_btn_delete.setTag("RCN_Delete_"+j);
						
						j++;
					}
					else
					{
						try
						{
							String s=((TextView) RCN_row[txt].findViewWithTag("RCN_ID_"+i)).getText().toString().trim();
							
							if(!s.equals(""))
							{
								cf.gch_db.execSQL("Delete from  "+cf.General_Roof_information +" WHERE RC_SRID='"+cf.selectedhomeid+"' and RC_Id='"+s+"'");
							}
							RCN_ShowValue.removeView(RCN_row[i]);
						}
						catch (Exception e) {
							// TODO: handle exception
							System.out.println("the deleting error  "+e.getMessage());
						}     
					}
				}
			}
			
		}
		
		
		
		return tmp;
	}
	private void clear_RCT() {
		// TODO Auto-generated method stub
		
		GCH_RC_RCT_sp.setSelection(0);
		GCH_RC_RCT_Other.setText("");
		GCH_RC_RCT_Other.setVisibility(View.GONE);
		GCH_RC_PA_ed.setText("");
		GCH_RC_YIR_sp.setSelection(0);
		GCH_RC_YIR_Other.setVisibility(View.GONE);
		GCH_RC_YIR_Other.setText("");
		GCH_RC_RS_sp.setSelection(0);
		GCH_RC_Rsh_sp.setSelection(0);
		GCH_RC_Rsh_Other.setVisibility(View.GONE);
		GCH_RC_Rsh_Other.setText("");
		GCH_RC_Rsh_per.setText("");
		GCH_RC_RSt_sp.setSelection(0);
		GCH_RC_RSt_Other.setVisibility(View.GONE);
		GCH_RC_RSt_Other.setText("");
		GCH_RC_PD_sp.setSelection(0);
		GCH_RC_RCT_SA.setText("Save and Add more");
		RC_NIP_chk.setChecked(false);
		RC_Dom_rd.setChecked(false);
		GCH_RC_PA_ed.setEnabled(false);
		GCH_RC_PA_DP.setEnabled(true);
	}


	
	@Override
	protected Dialog onCreateDialog(int id) {
		if(DATE_DIALOG_ID==id)
		{
				return new DatePickerDialog(this, mDateSetListener, mYear, mMonth,mDay);
		}	
		return null;
	}
	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;

			GCH_RC_PA_ed.setText(new StringBuilder()
					// Month is 0 based so add 1
					.append(mMonth + 1).append("/").append(mDay).append("/")
					.append(mYear).append(" "));
			
			

		}
	};

	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.GCH_RC_savebtn:
			if(b1)
			{	
				if(pre_dom)
				{
					RC_Save_validation();
				}
				else
				{
					cf.ShowToast("Select Roof Predominant for One Roof Type", 0);
				}
			}
			else
			{
				cf.ShowToast("Please save atleast One Roof Cover Type  ",0);
				
			}
			break;
		case R.id.GCH_RC_homebtn:
			 cf.gohome();
			break;
			
		default:
			break;
		}
	}

	private void RC_Save_validation() {
		// TODO Auto-generated method stub
		String RCN="",ERAE="",RCN_o="",RCN_val="",YR3="",cmt="";
		/**Validate the same question already exist **/
		String RCN_db[] = null;
		int db_count=0;
		/*try
		{
		  Cursor RCN_cur= cf.SelectTablefunction(cf.General_Roof_information, " Where RC_SRID='"+cf.selectedhomeid+"'");
		  if(RCN_cur.getCount()>0)
		  {
		  RCN_db=new String[(RCN_cur.getCount()+RCN_row.length)];
		  db_count=RCN_cur.getCount();
		  RCN_cur.moveToFirst();
		  System.out.println("the value in the db db_count "+db_count);
		  for(int i=0;i<RCN_cur.getCount();i++)
		  {
			  RCN_db[i]=cf.decode(RCN_cur.getString(2));
			  RCN_cur.moveToNext();
			  System.out.println("the value in the db "+RCN_db[i]);
		  }
		  }
		  else
		  {
			  RCN_db=new String[RCN_row.length];
			  
		  }
		}
		catch (Exception e) {
			// TODO: handle exception
			
		}*/
		/**Validate the same question already exist **/
		int count=RCN_row.length,compl=0;
		RCN_db=new String[count];
		for (int i=0;i<count;i++)
		{
			
			
		try
		{
		RCN_val=((RadioButton)findViewById(((RadioGroup) RCN_row[i].findViewWithTag("RCN_RG_"+i)).getCheckedRadioButtonId())).getText().toString().trim();
		RCN_o=((EditText)RCN_row[i].findViewWithTag("RCN_Li_ed_"+i)).getText().toString().trim();
		RCN=((TextView) RCN_row[i].findViewWithTag("Edit_head_"+i)).getText().toString().trim();
		ERAE=cf.encode(((RadioButton)findViewById((GCH_RC_RG_ERAE.getCheckedRadioButtonId()))).getText().toString().trim());
		YR3=cf.encode(((RadioButton)findViewById((GCH_RC_RG_3YR.getCheckedRadioButtonId()))).getText().toString().trim());
		cmt=cf.encode(RC_cmt.getText().toString().trim());
		RCN_db[i]=RCN;
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println("error"+e.getMessage());
		}
						if(!RCN.equals(""))
						{
						if(!RCN_val.equals(""))
						{
							if(!RCN_val.equals("Yes") || (RCN_val.equals("Yes") && !RCN_o.equals("")))
							{
								
							}
							else
							{
								cf.ShowToast("Please Enter the Comments for "+RCN,0);
								compl=1;
								return;
							}
							
						}
						else
						{
							cf.ShowToast("Please Select option for "+RCN,0);
							compl=1;
							return;
						}
						}
						else
						{
							cf.ShowToast("Please Enter the  Condtion Noted Title  ",0);
							compl=1;
							return;
						}	

		}
		/**validation for the Cmt and 3yr ***/
		if(compl==0)
		{
			if(!ERAE.equals(""))
			{				
				if(!YR3.equals(""))
				{
					if(!cmt.equals(""))
					{
						
					}
					else
					{
						cf.ShowToast("Please Enter the over all Comments ",0);
						compl=1;
					}
				}
				else
				{
					cf.ShowToast("Please Select option for 3 Year Replacement Probability",0);
					compl=1;
				}
			}
			else
			{
				cf.ShowToast("Please Select option for Excessive Roof Aging evident ",0);
				compl=1;
			}
		}	/**validation for the Cmt and 3yr ends ***/
		
		
		/**Validation fro the repeated data starts**/
		if(compl==0)
		{
			if(RCN_db!=null)
			{
			for(int i=0;i<(count);i++)
			{
				for(int j=0;j<(count);j++)
				{
					if(RCN_db[i].toLowerCase().trim().equals(RCN_db[j].toLowerCase().trim()) && i!=j)
					{
						cf.ShowToast("You have already  Enterd this Roof Condtion "+RCN_db[i],0);
						compl=1;
						return;
						
					}
				}
			}
			}
			else
			{
				System.out.println("null returend");
			}
		}
		/**Validation fro the repeated data starts**/
		if(compl==0)
		{
			
			
			for (int i=0;i<count;i++)
			{
			RCN=RCN_o=RCN_val=ERAE=YR3=cmt="";	
				try
				{
					
					RCN_val=cf.encode(((RadioButton)findViewById(((RadioGroup) RCN_row[i].findViewWithTag("RCN_RG_"+i)).getCheckedRadioButtonId())).getText().toString().trim());
					RCN_o=cf.encode(((EditText) RCN_row[i].findViewWithTag("RCN_Li_ed_"+i)).getText().toString().trim());
					RCN=cf.encode(((TextView) RCN_row[i].findViewWithTag("Edit_head_"+i)).getText().toString().trim());
				
				}catch (Exception e) {
					// TODO: handle exception
					System.out.println("error "+e.getMessage());
				}
			
				try
				{
					
					String temp="";
					try
					{
					temp=((TextView) RCN_row[i].findViewWithTag("RCN_ID_"+i)).getText().toString().trim();
					
					}
					catch (Exception e) {
						// TODO: handle exception
						
					}
					if(temp!=null)
					{
						if(!temp.equals(""))
						{
						Cursor c=cf.SelectTablefunction(cf.General_Roof_information, " Where RC_SRID='"+cf.selectedhomeid+"' and RC_Id='"+temp+"'");
							if(c.getCount()>0)
							{
								cf.gch_db.execSQL("UPDATE  "+cf.General_Roof_information +" SET RC_Condition_type='"+RCN+"', RC_Condition_type_DESC='"+RCN_o+"',RC_Condition_type_val='"+RCN_val+"' WHERE RC_SRID='"+cf.selectedhomeid+"' and RC_Id='"+temp+"'");
							}
							else
							{
								cf.gch_db.execSQL("INSERT INTO "+cf.General_Roof_information +" (RC_SRID,RC_Condition_type,RC_Condition_type_DESC,RC_Condition_type_val) VALUES ('"+cf.selectedhomeid+"','"+RCN+"','"+RCN_o+"','"+RCN_val+"')");
							}
						}
						else
						{
							cf.gch_db.execSQL("INSERT INTO "+cf.General_Roof_information +" (RC_SRID,RC_Condition_type,RC_Condition_type_DESC,RC_Condition_type_val) VALUES ('"+cf.selectedhomeid+"','"+RCN+"','"+RCN_o+"','"+RCN_val+"')");
						}
						
					}
				}catch(Exception e)
				{
					System.out.println("issues in the saving the value of the Roof section in save"+e.getMessage());
				}
			}
			/**year And ERAE and cmt value insert  Starts***/
			Cursor c;
			ERAE=cf.encode(((RadioButton)findViewById((GCH_RC_RG_ERAE.getCheckedRadioButtonId()))).getText().toString().trim());
			YR3=cf.encode(((RadioButton)findViewById((GCH_RC_RG_3YR.getCheckedRadioButtonId()))).getText().toString().trim());
			cmt=cf.encode(RC_cmt.getText().toString().trim());
			try
			{
				c=cf.SelectTablefunction(cf.General_Roof_information, " Where RC_SRID='"+cf.selectedhomeid+"' and RC_Condition_type_order='10' and RC_Condition_type='Comments' ");
				if(c.getCount()>0)
				{
					cf.gch_db.execSQL("UPDATE  "+cf.General_Roof_information +" SET RC_Condition_type_DESC='',RC_Condition_type_val='"+YR3+"' WHERE RC_SRID='"+cf.selectedhomeid+"' and RC_Condition_type_order='10' and  RC_Condition_type='3 Year Replacement Probability' ");
					cf.gch_db.execSQL("UPDATE  "+cf.General_Roof_information +" SET RC_Condition_type_DESC='',RC_Condition_type_val='"+ERAE+"' WHERE RC_SRID='"+cf.selectedhomeid+"' and RC_Condition_type_order='10' and  RC_Condition_type='Excessive Roof Aging evident' ");
					cf.gch_db.execSQL("UPDATE  "+cf.General_Roof_information +" SET RC_Condition_type_DESC='"+cmt+"',RC_Condition_type_val='' WHERE RC_SRID='"+cf.selectedhomeid+"' and RC_Condition_type_order='10' and  RC_Condition_type='Comments' ");
				}
				else
				{
					cf.gch_db.execSQL("INSERT INTO "+cf.General_Roof_information +" (RC_SRID,RC_Condition_type,RC_Condition_type_val,RC_Condition_type_order) VALUES ('"+cf.selectedhomeid+"','Excessive Roof Aging evident','"+ERAE+"','10')");
					cf.gch_db.execSQL("INSERT INTO "+cf.General_Roof_information +" (RC_SRID,RC_Condition_type,RC_Condition_type_val,RC_Condition_type_order) VALUES ('"+cf.selectedhomeid+"','3 Year Replacement Probability','"+YR3+"','10')");
					cf.gch_db.execSQL("INSERT INTO "+cf.General_Roof_information +" (RC_SRID,RC_Condition_type,RC_Condition_type_DESC,RC_Condition_type_order) VALUES ('"+cf.selectedhomeid+"','Comments','"+cmt+"','10')");
				}
			}
			catch (Exception e) {
				// TODO: handle exception
			}
			/**year And ERAE and cmt value insert Ends***/
			nextlayout();
		cf.ShowToast("Roof Information Saved Successfully ", 1);
		}
		
		
	}
	
	private void nextlayout() {
		// TODO Auto-generated method stub
		Intent intimg = new Intent(RoofSection.this,
				photos.class);
		cf.putExtras(intimg);;
		intimg.putExtra("type",51);
	    startActivity(intimg);
		
		
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
			case 0:
				break;
			case -1:
				try {

					String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();
					String capturedImageFilePath = cursor.getString(column_index_data);
					cf.showselectedimage(capturedImageFilePath);
				} catch (Exception e) {
					
				}
				
				break;

		}

	}

	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if(cf.strschdate.equals("")){
				cf.ShowToast("Layout navigation without scheduling not allowed. ", 1);
			}else{
				Intent  myintent = new Intent(getApplicationContext(),GeneralHazards.class);
				cf.putExtras(myintent);
				startActivity(myintent);
			}
			return true;
		}
		if (keyCode == KeyEvent.KEYCODE_MENU) {

		}
		return super.onKeyDown(keyCode, event);
	}	
	
	private String checkfordateistoday(String getinspecdate2) {
		// TODO Auto-generated method stub
		String chkdate = null;
		int i1 = getinspecdate2.indexOf("/");
		String result = getinspecdate2.substring(0, i1);
		int i2 = getinspecdate2.lastIndexOf("/");
		String result1 = getinspecdate2.substring(i1 + 1, i2);
		String result2 = getinspecdate2.substring(i2 + 1);
		result2 = result2.trim();
		int j1 = Integer.parseInt(result);
		int j2 = Integer.parseInt(result1);
		int j = Integer.parseInt(result2);
		final Calendar c = Calendar.getInstance();
		int thsyr = c.get(Calendar.YEAR);
		int curmnth = c.get(Calendar.MONTH);
		int curdate = c.get(Calendar.DAY_OF_MONTH);
		int day = c.get(Calendar.DAY_OF_WEEK);
		curmnth = curmnth + 1;

		if (j < thsyr || (j1 < curmnth && j <= thsyr)

		|| (j2 <= curdate && j1 <= curmnth && j <= thsyr)) {
			chkdate = "true";
		} else {
			chkdate = "false";
		}

		return chkdate;

	}

	public void onWindowFocusChanged(boolean hasFocus) {

        super.onWindowFocusChanged(hasFocus);
        if(focus==1)
        {
        	focus=0;
        try
        {
        	
        	cf.showing_limit(RC_cmt.getText().toString(),RC_cmt_par,RC_cmt_chaild,RC_cmt_tv,"750");
        	for(int i=0;i<RCN_row.length;i++)
        	{
        		EditText ED_Li_CN=(EditText) RCN_row[i].findViewWithTag("RCN_Li_ed_"+i);
        		
        		if(ED_Li_CN.getVisibility()==View.VISIBLE)
            	{
        			LinearLayout ED_Li_par_CN=(LinearLayout) RCN_row[i].findViewWithTag("RCN_Li_par_"+i); 
            		LinearLayout ED_Li_chaild_CN=(LinearLayout) RCN_row[i].findViewWithTag("RCN_Li_chi_"+i); 
            		TextView ED_txt_CN=(TextView) RCN_row[i].findViewWithTag("RCN_Li_txt_"+i);
            		cf.showing_limit(ED_Li_CN.getText().toString(),ED_Li_par_CN,ED_Li_chaild_CN,ED_txt_CN,"500");
            	}
        		
            	
        		
        	}
        }
        catch (Exception e) {
			// TODO: handle exception
        	System.out.println("isesfsfs"+e.getMessage());
		}
        	
        
        }         
}
	
}
