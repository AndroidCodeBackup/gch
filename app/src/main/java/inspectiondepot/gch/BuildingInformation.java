package inspectiondepot.gch;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class BuildingInformation extends Activity {
	CommonFunctions cf;
	int ISwallstructurepresent=0,ISwallcladdingpresent=0,ISisopresent=0;
	CheckBox BI_WSC_NA,BI_WC_CHK_NA,BI_ISO_CHK_NA;
	private static final int visibility = 0;
	View v1;EditText overallcomment;
	ArrayAdapter adapter,adapter1;
	EditText etlist1other,etlist2other,etlist3other;;
	TableRow genrw,locrw,obsrw,wsrw,wcrw,isorw,tblrwrail;
	TableLayout gentbl;
	public int focus=0;
	TextView txtbpresent,PD_TV_type1,AS_TV_type1,LOC_TV_type1,OBS3_TV_type1,OBS4_TV_type1,OBS2_TV_type1,OBS5_TV_type1;
	boolean b[]=new boolean[32];
	boolean wc[] = new boolean[3];
	LinearLayout loclin,loctbl,obstbl,wstbl,wctbl,isotbl,obs_rw3lin,obs_rw4lin,obs_rw2lin,obs_rw5lin,permiconflin,addistrulin,PC_ED_type1_parrant,PC_ED_type1,AS_ED_type1_parrant,AS_ED_type1,LOC_ED_type1_parrant,LOC_ED_type1,OBS3_ED_type1_parrant,OBS3_ED_type1,OBS2_ED_type1_parrant,OBS2_ED_type1,OBS5_ED_type1_parrant,OBS5_ED_type1,OBS4_ED_type1_parrant,OBS4_ED_type1,permiconf_lin,addistru_lin;
	CheckBox cb_ws,cb_wc,cb_iso;
	String BL_CONC_UNREIN,BI_CONC_UNREINPER,BI_CONC_REIN,BI_CONC_REINPER,BI_SOLID_CONC,BI_SOILD_CONCPER,BI_WFRAME,WFRAMERPER,BI_REIN_MASONRY,BI_REIN_MASONRYPER,BI_UNREIN_MASONRY,BI_UNREIN_MASONRYPER,BI_OTHER_WSTITLE="",BI_OTHER_WS="",BI_OTHER_WALLPER="",BI_OTHER_WCLADTITLE1="",BI_OTHER_WCLADTITLE2="",BI_OTHER_WCLADTITLE3="";
	String BI_ISOCLASSPRES="",InspectedDate="",StartTime="",EndTime="",FinishTime="",yearofcons="",Ins_Carrier="",BI_PERMITCONFIRMED="",otheryearbuilttext="",BI_ADDITONALSTRU="",BI_BALCONY_RAILPRESENT,BI_BALCONYPRESENT,BI_LOC1="",BI_LOC2="",BI_LOC3="",BI_LOC4="",BI_LOC_OTHER="",BI_LOC1_DESC="",BI_LOC2_DESC="",BI_LOC3_DESC="",BI_LOC4_DESC=""
			,BI_OBSERV1="",BI_OBSERV2="",BI_OBSERV3="",BI_OBSERV4="",BI_OBSERV5="",BI_OBSERV3_DESC="",BI_OBSERV4_DESC="",BI_OBSERV2_DESC="",BI_OBSERV5_DESC="";
	int iso=0,BI_OCCUPIED=0,BI_VACANT=0,BI_NOTDETERMINED=0;String buildingsize = "";
	int frameval=0,joistedmasonryval=0,noncombustibleval=0,masonryoncomval=0,modifiedfireresistiveval=0,fireresistveval=0,
			ws1=0,ws2=0,ws3=0,ws4=0,ws5=0,ws6=0,ws7=0,iso1=0,iso2=0,iso3=0,iso4=0,iso5=0,iso6=0;
	String frametxt="",joistedmasonrytxt="",noncombustibletxt="",masonryoncomtxt="",modifiedfireresistivetxt="",fireresistvetxt="";
	String yearbuilt[] = { "Select","Unknown","Other", "2012","2011","2010","2009","2008","2007","2006","2005","2004","2003","2002","2001","2000","1999","1998","1997","1996",
			"1995","1994","1993","1992","1991","1990","1989","1988","1987","1986","1985","1984","1983","1982","1981","1980","1979","1978","1977","1976",
			"1975","1974","1973","1972","1971","1970","1969","1968","1967","1966","1965","1964","1963","1962","1961","1960","1959","1958","1957","1956",
			"1955","1954","1953","1952","1951","1950","1949","1948","1947","1946","1945","1944","1943","1942","1941","1940","1939","1938","1937","1936",
			"1935","1934","1933","1932","1931","1930","1929","1928","1927","1926","1925","1924","1923","1922","1921","1920","1919","1918","1917","1916",
			"1915","1914","1913","1912","1911","1910","1909","1908","1907","1906","1905","1904","1903","1902","1901","1900"};
	 String GCH_BI_INSUREDIS,GCH_BI_INSOTHER,GCH_BI_OCCUPIED,GCH_BI_OCCUPIEDPER,GCH_BI_VACANT,GCH_BI_VACANTPER,GCH_BI_NOT_DETERMINED,
	 GCH_BI_OCC_TYPE,GCH_BI_OCC_OTHER,GCH_BI_OBSERV_TYPE,GCH_BI_OBSERV_OTHER,GCH_BI_NEIGH_IS,GCH_BI_NEIGH_OTHER,GCH_BI_BUIL_SIZE,
	 GCH_BI_NSTORIES,GCH_BI_BUILT_YR,GCH_BI_YR_BUILT_OPTION,GCH_BI_YR_BUILT_OTHER,GCH_BI_PERM_CONFIRMED,GCH_BI_OBSERV3_DESC,GCH_BI_OBSERV2_DESC,GCH_BI_OBSERV5_DESC,
	 GCH_BI_PERM_CONF_DESC,GCH_BI_ADDI_STRU,GCH_BI_ADDI_STRU_DESC,GCH_BI_BALCONY_PRES,GCH_BI_NBALCONY_PRES,GCH_BI_BALCONY_RAIL_PRES,GCH_BI_OBSERV3,
	 GCH_BI_LOC1,GCH_BI_LOC2,GCH_BI_LOC3,GCH_BI_LOC4,GCH_BI_LOC1_DESC="",GCH_BI_LOC2_DESC="",GCH_BI_LOC3_DESC="",GCH_BI_LOC4_DESC="",GCH_BI_OTHER_LOC,GCH_BI_OBSERV1,GCH_BI_OBSERV2,GCH_BI_OBSERV4,GCH_BI_OBSERV4_DESC,GCH_BI_OBSERV5,
	GCH_BI_CONC_UNREIN,GCH_BI_CONC_UNREINPER,GCH_BI_CONC_REIN,GCH_BI_CONC_REINPER,GCH_BI_WSC_PRESENT,GCH_BI_WFRAME,GCH_BI_WFRAMERPER,GCH_BI_UNREIN_MASONRYPER,
	GCH_BI_SOLID_CONC,GCH_BI_SOILD_CONCPER,GCH_BI_REIN_MASONRY,GCH_BI_REIN_MASONRYPER,GCH_BI_UNREIN_MASONRY,GCH_BI_OTHER_WSTITLE,GCH_BI_CEMENT_FIBER,
	GCH_BI_STUCCO,GCH_BI_BRICKVENEER,GCH_BI_PAINTEDBLOCK,GCH_BI_OTHER_WCLADTITLE1,GCH_BI_OTHER_WCLAD1,GCH_BI_OTHER_WCLADTITLE2,GCH_BI_OTHER_WCLAD2,
	GCH_BI_OTHER_WCLADTITLE3,GCH_BI_OTHER_WCLAD3,GCH_BI_ISO_CLASSPRES,GCH_BI_FRAME,GCH_BI_OTHER_WS,GCH_BI_OTHER_WALLPER,GCH_BI_WALLCLADD_PRES,
	GCH_BI_ALUM_SLIDING,GCH_BI_VINYL_SLIDING,GCH_BI_WOOD_SLIDING,
	GCH_BI_JOISTED,GCH_BI_NONCOMBUST,GCH_BI_MASON_NONCOMBUST,GCH_BI_MODIFY_FIRERESIST,GCH_BI_FIRERESIST,GCH_BI_BUILD_COMMENTS;
	String BI_OCCU_PERCENT,BI_VACANT_PERCENT;
	String ntories,nstoriespolicy="",ybuilt,s="",wscvalue="",yocpolicy="";
	String noofstories[] = {"Select","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35"};
	String BI_INSUREDIS,BI_OTHERINSUREDIS,BI_OCCU_TYPE,BI_OBSERV_TYPE,BI_BUIL_OCCU,BI_NEIGHBOURHOOD,BI_OTHEROCCUTYPE,BI_OTHEROBSERVTYPE,BI_NEIGH_IS,BI_OTHERNEIGH,BI_YEARBUILT,BI_YEARBUILT_OPTION,
	BI_WC_ALUMISLIDING,BI_WC_VLUMISLIDING,BI_WC_WLUMISLIDING,BI_WC_list1_otheropt,BI_WC_CEMFIBSLIDING,BI_WC_Stucco,BL_WC_BVeneer,BI_WC_Paintedblock,BI_WC_list2_otheropt,BI_WC_list3_otheropt;
	/*LinearLayout loc_rw1_lin,loc_rw1_lin_child,loc_rw1_parrent,loc_rw2_lin_child,loc_rw2_lin,loc_rw2_parrent,loc_rw3_lin,loc_rw3_lin_child,loc_rw3_parrent,loc_rw4_lin,loc_rw4_parrent,loc_rw4_lin_child;
	TextView  loc_rw1_txt,loc_rw2_txt,loc_rw3_txt,loc_rw4_txt;
	*/
	
	
	public RadioButton BI_INS_txt1_opt[]=new RadioButton[6];
	public RadioButton BI_OCCU_TYPE_opt[]=new RadioButton[8];
	public RadioButton BI_Observ_Type_opt[]=new RadioButton[5];
	public RadioButton BI_NEIGH_txt_opt[]=new RadioButton[4];
	public RadioButton BI_YEARBUILT_opt[]=new RadioButton[3];
	public RadioButton PP_opt_yesno[]=new RadioButton[14];
	public CheckBox BI_WSC_QUES1_chk[] = new CheckBox[6];
	public CheckBox BI_WSC_QUES2_chk[] = new CheckBox[6];
	public CheckBox BI_WSC_QUES3_chk[] = new CheckBox[6];
	public CheckBox BI_WSC_QUES4_chk[] = new CheckBox[6];
	public CheckBox BI_WSC_QUES5_chk[] = new CheckBox[6];
	public CheckBox BI_WSC_QUES6_chk[] = new CheckBox[6];		
	public CheckBox BI_WSC_QUES7_chk[] = new CheckBox[6];
	public CheckBox BI_WC_ALSlid_opt[] = new CheckBox[6];
	public CheckBox BI_WC_VLSlid_opt[] = new CheckBox[6];
	public CheckBox BI_WC_WLSlid_opt[] = new CheckBox[6];
	public CheckBox BI_WC_CEMFIBER_opt[] = new CheckBox[6];
	public CheckBox BI_WC_list1_otheropt_chk[] = new CheckBox[6];
	public CheckBox BI_WC_Stucc_opt[] = new CheckBox[6];
	public CheckBox BI_WC_BrickVeneer_opt[] = new CheckBox[6];
	public CheckBox BI_WC_PaintedBlock_opt[] = new CheckBox[6];
	public CheckBox BI_WC_list2_otheropt_chk[] = new CheckBox[6];
	public CheckBox BI_WC_list3_otheropt_chk[] = new CheckBox[6];
	public RadioGroup PP_ground;
	public TableRow otheryearbuilttr;
	
	public RadioButton BI_BALPRES_YES,BI_BALPRES_NO,BI_Permit_Confirmed_yes,BI_Permit_Confirmed_No,BI_Permit_Confirmed_BSI,BI_additionalstru_yes,BI_additionalstru_no,BI_Loc_txt1_yes,BI_Loc_txt1_no,BI_Loc_txt1_nd,BI_Loc_txt2_yes,BI_Loc_txt2_no,BI_Loc_txt2_nd,BI_Loc_txt3_yes,BI_Loc_txt3_no,BI_Loc_txt3_nd,BI_Loc_txt4_yes,BI_Loc_txt4_no,BI_Loc_txt4_nd,
	BI_OBSERV_txt1_yes,BI_OBSERV_txt1_no,BI_OBSERV_txt2_yes,BI_OBSERV_txt2_no,BI_OBSERV_txt3_yes,BI_OBSERV_txt3_no,BI_OBSERV_txt3_BSI,BI_OBSERV_txt4_yes,BI_OBSERV_txt4_no,BI_OBSERV_txt5_yes,BI_OBSERV_txt5_no,BI_OBSERV_txt5_BSI,BI_OBSERV_txt2_notdeter,BI_Balc_Rail_pres_yes,BI_Balc_Rail_pres_no;
	public RadioButton PP_txt1_opt_yes,PP_txt1_opt_no,PP_txt2_opt_yes,PP_txt2_opt_no,PP_txt3_opt_yes,PP_txt3_opt_no,PP_txt4_opt_yes,PP_txt4_opt_no,PP_txt5_opt_yes,PP_txt5_opt_no,PP_txt6_opt_yes,PP_txt6_opt_no;
	public CheckBox BI_BUIL_OCCU_chk[] = new CheckBox[3];
	
	public EditText etlocationcomments,etobserv3desc,etobserv2desc,etobserv5desc,edt_BI_maincomments,etinsuredother,etobservtypeother,etbuild_occup1,etbuild_occup2,etbuildoccuptype,etneighbourhoodother,etpermitconfirmed,etadditionalstructures,etbuildingsize,etobservcomment,etbalconiespresent,etotheryearbuilt
	,etwallstrucoptionopt1,etwallstrucoptionopt2,etwallstrucoptionopt3,etwallstrucoptionopt4,etwallstrucoptionopt5,etwallstrucoptionopt6,etwallstrucoptionopt7, etwallstrucothertxt,etframe,etjoistmasonry,etnoncombustib,etmasonrynoncomb,etmodifyfiresistance,etfiresistance;
	public LinearLayout BI_GEN_HEADER,BI_LOC_HEADER,BI_OBSERV_HEADER,BI_WSC_HEADER,BI_WC_HEADER,BI_ISO_HEADER,appnoofbalconieslinear;
	public RelativeLayout BI_WC_REL,BI_GENERAL_REL1,BI_GENERAL_REL2,BI_GENERAL_REL3,BI_GENERAL_REL4,BI_GENERAL_REL5,BI_GENERAL_REL6;
	public TableLayout BI_LOC_TBL,BI_OBSERV_TBL,BI_WSC_TBL,BI_ISO_TBL;
	public Spinner spin_noofstories,spin_yearbuilt;
	
/* DECLARATION OF VARIABLES FOR  BUILDING INFORAMTION STARTS HERE **/

	  
	  public Button BI_savenext;
private boolean setvalue=false; 
	
	
	
	
	public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    cf = new CommonFunctions(this);
    Bundle extras = getIntent().getExtras();
	if (extras != null) {
		cf.selectedhomeid =  extras.getString("homeid");
		cf.onlinspectionid = extras.getString("InspectionType");
	    cf.onlstatus = extras.getString("status");
 	}
    setContentView(R.layout.buildinginformation);
    /**Used for hide he key board when it clik out side**/
	cf.setupUI((ScrollView) findViewById(R.id.scr));
/**Used for hide he key board when it clik out side**/
    cf.getInspectorId();
    cf.Create_Table(5);
    cf.getDeviceDimensions();

    LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
    mainmenu_layout.setMinimumWidth(cf.wd);
    TableRow tblrw = (TableRow)findViewById(R.id.row2);
    tblrw.setMinimumHeight(cf.ht);
    mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 2, 0,cf));
    
    setObjectsToView();
    setObjectsDeclarations();
    focus=1;
    BI_setValue();
    
    spin_noofstories.setOnItemSelectedListener(new spinerlisner());
    spin_yearbuilt.setOnItemSelectedListener(new spinerlisner());
}
	private void BI_setValue() {

		Cursor BI_retrive;
		   try
			{
				 BI_retrive=cf.SelectTablefunction(cf.BIQ_table, " where GCH_BI_Homeid='"+cf.encode(cf.selectedhomeid)+"'");
				 System.out.println("BI_retrieve "+BI_retrive.getCount());
				 if(BI_retrive.getCount()>0)
				 {
					 
					 BI_retrive.moveToFirst();
					 GCH_BI_INSUREDIS=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_INSURED_IS")));
					 GCH_BI_INSOTHER=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_INS_OTHER")));
					 GCH_BI_OCCUPIED=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_OCCUPIED")));
					 GCH_BI_OCCUPIEDPER=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_OCCUPIEDPER")));
					 GCH_BI_VACANT=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_VACANT")));
					 GCH_BI_VACANTPER=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_VACANTPER")));
					 GCH_BI_NOT_DETERMINED=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_NOT_DETERMINED")));
					 GCH_BI_OCC_TYPE=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_OCC_TYPE")));
					 GCH_BI_OCC_OTHER=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_OCC_OTHER")));
					 GCH_BI_OBSERV_TYPE=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_OBSERV_TYPE")));
					 GCH_BI_OBSERV_OTHER=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_OBSERV_OTHER")));
					 GCH_BI_NEIGH_IS=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_NEIGH_IS")));
					 GCH_BI_NEIGH_OTHER=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_NEIGH_OTHER")));
					 GCH_BI_BUIL_SIZE=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_BUIL_SIZE")));
					 GCH_BI_NSTORIES=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_NSTORIES")));
					 GCH_BI_BUILT_YR=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_BUILT_YR")));
					 GCH_BI_YR_BUILT_OPTION=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_YR_BUILT_OPTION")));
					 GCH_BI_YR_BUILT_OTHER=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_YR_BUILT_OTHER")));
					 GCH_BI_PERM_CONFIRMED=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_PERM_CONFIRMED")));
					 GCH_BI_PERM_CONF_DESC =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_PERM_CONF_DESC")));
					GCH_BI_ADDI_STRU=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_ADDI_STRU")));
					GCH_BI_ADDI_STRU_DESC=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_ADDI_STRU_DESC")));
					GCH_BI_BALCONY_PRES=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_BALCONY_PRES")));
					GCH_BI_NBALCONY_PRES=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_NBALCONY_PRES")));
					GCH_BI_BALCONY_RAIL_PRES=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_BALCONY_RAIL_PRES")));
					GCH_BI_LOC1 =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_LOC1")));
					GCH_BI_LOC2=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_LOC2")));
					GCH_BI_LOC3=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_LOC3")));
					GCH_BI_LOC4=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_LOC4")));
					/*GCH_BI_LOC1_DESC =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_LOC1_DESC")));
					GCH_BI_LOC2_DESC=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_LOC2_DESC")));
					GCH_BI_LOC3_DESC=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_LOC3_DESC")));
					GCH_BI_LOC4_DESC=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_LOC4_DESC")));
					*/
					GCH_BI_OTHER_LOC =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_OTHER_LOC")));
					GCH_BI_OBSERV1 =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_OBSERV1")));
					GCH_BI_OBSERV2 =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_OBSERV2")));
					GCH_BI_OBSERV2_DESC=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_OBSERV2_DESC")));
					GCH_BI_OBSERV3 =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_OBSERV3")));
					GCH_BI_OBSERV3_DESC=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_OBSERV3_DESC")));
					
					GCH_BI_OBSERV4 =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_OBSERV4")));
					GCH_BI_OBSERV4_DESC =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_OBSERV4_DESC")));
					GCH_BI_OBSERV5 =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_OBSERV5")));
					GCH_BI_OBSERV5_DESC=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_OBSERV5_DESC")));
					GCH_BI_WSC_PRESENT =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_WSC_PRESENT")));
					GCH_BI_CONC_UNREIN =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_CONC_UNREIN")));
					GCH_BI_CONC_UNREINPER=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_CONC_UNREINPER")));
					GCH_BI_CONC_REIN =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_CONC_REIN")));
					GCH_BI_CONC_REINPER=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_CONC_REINPER")));
					GCH_BI_SOLID_CONC =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_SOLID_CONC")));
					GCH_BI_SOILD_CONCPER=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_SOILD_CONCPER")));
					GCH_BI_WFRAME =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_WFRAME")));
					GCH_BI_WFRAMERPER=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_WFRAMERPER")));
					GCH_BI_REIN_MASONRY =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_REIN_MASONRY")));
					GCH_BI_REIN_MASONRYPER=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_REIN_MASONRYPER")));
					GCH_BI_UNREIN_MASONRY =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_UNREIN_MASONRY")));
					GCH_BI_UNREIN_MASONRYPER=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_UNREIN_MASONRYPER")));
					GCH_BI_OTHER_WSTITLE =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_OTHER_WSTITLE")));
					GCH_BI_OTHER_WS =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_OTHER_WS")));
					GCH_BI_OTHER_WALLPER=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_OTHER_WALLPER")));
					GCH_BI_WALLCLADD_PRES=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_WALLCLADD_PRES")));
					GCH_BI_ALUM_SLIDING=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_ALUM_SLIDING")));
					GCH_BI_VINYL_SLIDING =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_VINYL_SLIDING")));
					GCH_BI_WOOD_SLIDING =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_WOOD_SLIDING")));				
					GCH_BI_CEMENT_FIBER =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_CEMENT_FIBER")));
					GCH_BI_STUCCO =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_STUCCO")));
					GCH_BI_BRICKVENEER =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_BRICKVENEER")));
					GCH_BI_PAINTEDBLOCK =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_PAINTEDBLOCK")));
					GCH_BI_OTHER_WCLADTITLE1 =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_OTHER_WCLADTITLE1")));
					GCH_BI_OTHER_WCLAD1 =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_OTHER_WCLAD1")));
					GCH_BI_OTHER_WCLADTITLE2 =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_OTHER_WCLADTITLE2")));
					GCH_BI_OTHER_WCLAD2 =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_OTHER_WCLAD2")));
					GCH_BI_OTHER_WCLADTITLE3 =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_OTHER_WCLADTITLE3")));
					GCH_BI_OTHER_WCLAD3 =cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_OTHER_WCLAD3")));
					GCH_BI_ISO_CLASSPRES=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_ISO_CLASSPRES")));
					GCH_BI_FRAME=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_FRAME")));
					GCH_BI_JOISTED=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_JOISTED")));
					GCH_BI_NONCOMBUST=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_NONCOMBUST")));
					GCH_BI_MASON_NONCOMBUST=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_MASON_NONCOMBUST")));
					GCH_BI_MODIFY_FIRERESIST=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_MODIFY_FIRERESIST")));
					GCH_BI_FIRERESIST=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_FIRERESIST")));
					GCH_BI_BUILD_COMMENTS=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_BI_BUILD_COMMENTS")));
					
					((RadioButton) findViewById(R.id.BI_INS_txt1_opt1)).setChecked(false);
					((CheckBox) findViewById(R.id.BI_BUIL_OCCU_chk1)).setChecked(false);
					((RadioButton) findViewById(R.id.BI_OCCU_TYPE_opt7)).setChecked(false);
					((RadioButton) findViewById(R.id.BI_Observ_Type_opt2)).setChecked(false);
					((RadioButton) findViewById(R.id.BI_NEIGH_txt_opt3)).setChecked(false);
					((RadioButton) findViewById(R.id.BI_YEARBUILT_opt1)).setChecked(false);
					((RadioButton) findViewById(R.id.BI_BALPRES_NO)).setChecked(false);
					((RadioButton) findViewById(R.id.BI_additionalstru_no)).setChecked(false);
					((RadioButton) findViewById(R.id.BI_Permit_Confirmed_BSI)).setChecked(false);
					((RadioButton) findViewById(R.id.BI_Balc_Rail_pres_no)).setChecked(false);
					((EditText) findViewById(R.id.etbuild_occup1)).setText("");
					((EditText) findViewById(R.id.etbuild_occup1)).setEnabled(false);
					
					System.out.println("The title  value for the "+GCH_BI_OTHER_WCLADTITLE1+" ti 2"+GCH_BI_OTHER_WCLADTITLE2+" tit 3"+GCH_BI_OTHER_WCLADTITLE3);
					cf.setvalueradio(GCH_BI_INSUREDIS,BI_INS_txt1_opt);
					etbuildoccuptype.setText("");
					//etbuildoccuptype.setEnabled(false);
					if(GCH_BI_INSUREDIS.equals("Other")){etinsuredother.setVisibility(v1.VISIBLE);etinsuredother.setText(GCH_BI_INSOTHER);}
					else{etinsuredother.setVisibility(v1.GONE);etinsuredother.setText("");}
						
					cf.setvalueradio(GCH_BI_OCC_TYPE,BI_OCCU_TYPE_opt); 
					if(GCH_BI_OCC_TYPE.equals("Other")){
						
						etbuildoccuptype.setFocusableInTouchMode(true);
						etbuildoccuptype.requestFocus();
						etbuildoccuptype.setCursorVisible(true);
						
						etbuildoccuptype.setVisibility(v1.VISIBLE);etbuildoccuptype.setText(GCH_BI_OCC_OTHER);}
					else{etbuildoccuptype.setVisibility(v1.GONE);etbuildoccuptype.setText("");}
					
					cf.setvalueradio(GCH_BI_OBSERV_TYPE,BI_Observ_Type_opt); 
					if(GCH_BI_OBSERV_TYPE.equals("Other")){
						etobservtypeother.setFocusableInTouchMode(true);
						etobservtypeother.requestFocus();
						etobservtypeother.setCursorVisible(true);
						etobservtypeother.setVisibility(v1.VISIBLE);etobservtypeother.setText(GCH_BI_OBSERV_OTHER);}
					else{etobservtypeother.setVisibility(v1.GONE);etobservtypeother.setText("");}
					
					cf.setvalueradio(GCH_BI_NEIGH_IS,BI_NEIGH_txt_opt); 
					if(GCH_BI_NEIGH_IS.equals("Other")){
						etneighbourhoodother.setFocusableInTouchMode(true);
						etneighbourhoodother.requestFocus();
						etneighbourhoodother.setCursorVisible(true);
						etneighbourhoodother.setVisibility(v1.VISIBLE);etneighbourhoodother.setText(GCH_BI_NEIGH_OTHER);}
					else{etneighbourhoodother.setVisibility(v1.GONE);etneighbourhoodother.setText("");}
					setvalue=true;
					
					etbuildingsize.setText(GCH_BI_BUIL_SIZE);
					spin_noofstories.setSelection(adapter.getPosition(GCH_BI_NSTORIES));
					if(adapter1.getPosition(GCH_BI_BUILT_YR)==-1)
					{
						spin_yearbuilt.setSelection(adapter1.getPosition("Other"));
						etotheryearbuilt.setText(GCH_BI_BUILT_YR);
						etotheryearbuilt.setVisibility(View.VISIBLE);
						//cf.setFocus(etotheryearbuilt);
					}else
					{
						spin_yearbuilt.setSelection(adapter1.getPosition(GCH_BI_BUILT_YR));
					}
					if(!"".equals(GCH_BI_YR_BUILT_OTHER)){etotheryearbuilt.setVisibility(v1.VISIBLE);etotheryearbuilt.setText(GCH_BI_YR_BUILT_OTHER);}
					System.out.println("no more isseues "+GCH_BI_YR_BUILT_OPTION);
					BI_YEARBUILT_opt[0].setChecked(false);
					BI_YEARBUILT_opt[1].setChecked(false);
					BI_YEARBUILT_opt[2].setChecked(false);
					cf.setvalueradio(GCH_BI_YR_BUILT_OPTION,BI_YEARBUILT_opt);
					
					/*if(GCH_BI_LOC1.trim().equals("No"))
					{
						loc_rw1_lin.setVisibility(View.VISIBLE);
						etloc1desc.setFocusableInTouchMode(true);
						etloc1desc.requestFocus();
						etloc1desc.setCursorVisible(true);
						//GCH_BI_LOC1_DESC=(GCH_BI_LOC1_DESC.trim().equals(""))? "Beyond scope of the inspection":GCH_BI_LOC1_DESC;
						//etloc1desc.setText(GCH_BI_LOC1_DESC);
					}
					else
					{
						loc_rw1_lin.setVisibility(View.GONE);
						etloc1desc.setText("");
					}
					
					if(GCH_BI_LOC2.trim().equals("No"))
					{
						loc_rw2_lin.setVisibility(View.VISIBLE);
						etloc2desc.setFocusableInTouchMode(true);
						etloc2desc.requestFocus();
						etloc2desc.setCursorVisible(true);
						GCH_BI_LOC1_DESC=(GCH_BI_LOC2_DESC.trim().equals(""))? "Beyond scope of the inspection":GCH_BI_LOC2_DESC;
						etloc2desc.setText(GCH_BI_LOC2_DESC);
					}
					else
					{
						loc_rw2_lin.setVisibility(View.GONE);
						etloc2desc.setText("");
					}
					
					if(GCH_BI_LOC3.trim().equals("No"))
					{
						loc_rw3_lin.setVisibility(View.VISIBLE);
						etloc3desc.setFocusableInTouchMode(true);
						etloc3desc.requestFocus();
						etloc3desc.setCursorVisible(true);
						GCH_BI_LOC3_DESC=(GCH_BI_LOC3_DESC.trim().equals(""))? "Beyond scope of the inspection":GCH_BI_LOC3_DESC;
						etloc3desc.setText(GCH_BI_LOC3_DESC);
					}
					else
					{
						loc_rw3_lin.setVisibility(View.GONE);
						etloc3desc.setText("");
					}
					
					if(GCH_BI_LOC4.trim().equals("No"))
					{
						loc_rw4_lin.setVisibility(View.VISIBLE);
						etloc4desc.setFocusableInTouchMode(true);
						etloc4desc.requestFocus();
						etloc4desc.setCursorVisible(true);
						GCH_BI_LOC4_DESC=(GCH_BI_LOC4_DESC.trim().equals(""))? "Beyond scope of the inspection":GCH_BI_LOC4_DESC;
						etloc4desc.setText(GCH_BI_LOC4_DESC);
					}
					else
					{
						loc_rw4_lin.setVisibility(View.GONE);
						etloc4desc.setText("");
					}*/
					
					if(GCH_BI_OBSERV2.equals("Yes")){
						BI_OBSERV_txt2_yes.setChecked(true);
						etobserv2desc.setFocusableInTouchMode(true);
						etobserv2desc.requestFocus();
						etobserv2desc.setCursorVisible(true);
						etobserv2desc.setVisibility(v1.VISIBLE);etobserv2desc.setText(GCH_BI_OBSERV2_DESC);obs_rw2lin.setVisibility(v1.VISIBLE);
						}
					else{BI_OBSERV_txt2_no.setChecked(true);etobserv2desc.setVisibility(v1.GONE);obs_rw2lin.setVisibility(v1.GONE);}
					
					
					
				
			
				   
				
			
					
					if(GCH_BI_OBSERV3.equals("Yes")){
						BI_OBSERV_txt3_yes.setChecked(true);
						obs_rw3lin.setVisibility(View.VISIBLE);
						etobserv3desc.setFocusableInTouchMode(true);
						etobserv3desc.requestFocus();
						etobserv3desc.setCursorVisible(true);
						etobserv3desc.setText(GCH_BI_OBSERV3_DESC);
						etobserv3desc.setVisibility(View.VISIBLE);
						}
					else if(GCH_BI_OBSERV3.equals("No"))
						{
							BI_OBSERV_txt3_no.setChecked(true);
							
							obs_rw3lin.setVisibility(View.GONE);
							etobserv3desc.setFocusable(false);
							etobserv3desc.setVisibility(View.GONE);
							etobserv3desc.setText("");
						}
					else
					{
						BI_OBSERV_txt3_BSI.setChecked(true);
						obs_rw3lin.setVisibility(View.GONE);
						etobserv3desc.setFocusable(false);
						etobserv3desc.setVisibility(View.GONE);
						etobserv3desc.setText("");
					}
					
					
					if(GCH_BI_OBSERV4.equals("Yes")){
						BI_OBSERV_txt4_yes.setChecked(true);
						/*etobserv4desc.setFocusableInTouchMode(true);
						etobserv4desc.requestFocus();
						etobserv4desc.setCursorVisible(true);
						etobserv4desc.setVisibility(v1.VISIBLE);etobserv4desc.setText(GCH_BI_OBSERV4_DESC);obs_rw4lin.setVisibility(v1.VISIBLE);*/
						}
					else{BI_OBSERV_txt4_no.setChecked(true);/*etobserv4desc.setVisibility(v1.GONE);obs_rw4lin.setVisibility(v1.GONE);*/}
					//GCH_BI_OBSERV5_DESC=(GCH_BI_OBSERV5_DESC.trim().equals(""))? "Beyond scope of the inspection":GCH_BI_OBSERV5_DESC;
					etobserv5desc.setText(GCH_BI_OBSERV5_DESC);
					System.out.println("the text in the GCH_BI_OBSERV5"+GCH_BI_OBSERV5);
					if(GCH_BI_OBSERV5.equals("Yes")){
						BI_OBSERV_txt5_yes.setChecked(true);
						etobserv5desc.setFocusableInTouchMode(true);
						etobserv5desc.requestFocus();
						etobserv5desc.setCursorVisible(true);
						etobserv5desc.setVisibility(v1.VISIBLE);etobserv5desc.setText(GCH_BI_OBSERV5_DESC);obs_rw5lin.setVisibility(v1.VISIBLE);
						}
					else{ 
						if(GCH_BI_OBSERV5.equals("No"))
						{
							BI_OBSERV_txt5_no.setChecked(true);
						}
						else
						{
							BI_OBSERV_txt5_BSI.setChecked(true);
						}
						obs_rw5lin.setVisibility(v1.GONE);
						}
					
					
					if(GCH_BI_NOT_DETERMINED.equals("1")){BI_BUIL_OCCU_chk[2].setChecked(true);}else{}
					if(GCH_BI_OCCUPIED.equals("1")){BI_BUIL_OCCU_chk[0].setChecked(true);etbuild_occup1.setText(GCH_BI_OCCUPIEDPER);etbuild_occup1.setEnabled(true);}else{}
					if(GCH_BI_VACANT.equals("1")){BI_BUIL_OCCU_chk[1].setChecked(true);etbuild_occup2.setText(GCH_BI_VACANTPER);etbuild_occup2.setEnabled(true);}else{}
					if(GCH_BI_LOC1.equals("Yes")){
						BI_Loc_txt1_yes.setChecked(true);
						}else if (GCH_BI_LOC1.equals("No")){
							BI_Loc_txt1_no.setChecked(true);
						}
						else if(GCH_BI_LOC1.equals("Not Determined"))
						{
							BI_Loc_txt1_nd.setChecked(true);
						}
						else
						{
							((RadioButton) findViewById(R.id.BI_Loc_txt1_BSI)).setChecked(true);
						}
					if(GCH_BI_LOC2.equals("Yes")){
						BI_Loc_txt2_yes.setChecked(true);
						}else if (GCH_BI_LOC2.equals("No")){
						BI_Loc_txt2_no.setChecked(true);
					}
					else if(GCH_BI_LOC2.equals("Not Determined"))
					{
						BI_Loc_txt2_nd.setChecked(true);
					}
					else
					{
						((RadioButton) findViewById(R.id.BI_Loc_txt2_BSI)).setChecked(true);
					}
					if(GCH_BI_LOC3.equals("Yes")){
						BI_Loc_txt3_yes.setChecked(true);
					}
					else if (GCH_BI_LOC3.equals("No")){
							BI_Loc_txt3_no.setChecked(true);
						}
						else if(GCH_BI_LOC3.equals("Not Determined"))
						{
							BI_Loc_txt3_nd.setChecked(true);
						}
						else
						{
							((RadioButton) findViewById(R.id.BI_Loc_txt3_BSI)).setChecked(true);
						}
					
					if(GCH_BI_LOC4.equals("Yes")){
						BI_Loc_txt4_yes.setChecked(true);}
					else if (GCH_BI_LOC4.equals("No")){
						BI_Loc_txt4_no.setChecked(true);
					}
					else if(GCH_BI_LOC4.equals("Not Determined"))
					{
						BI_Loc_txt4_nd.setChecked(true);
					}
					else
					{
						((RadioButton) findViewById(R.id.BI_Loc_txt4_BSI)).setChecked(true);
					}
					
					
					if(GCH_BI_OBSERV1.equals("Yes")){BI_OBSERV_txt1_yes.setChecked(true);}else{BI_OBSERV_txt1_no.setChecked(true);}
					if(GCH_BI_OBSERV2.equals("Yes"))
					{
						BI_OBSERV_txt2_yes.setChecked(true);
					}
					else if(GCH_BI_OBSERV2.equals("No"))
					{
						BI_OBSERV_txt2_no.setChecked(true);
					}
					else 
					{
						BI_OBSERV_txt2_notdeter.setChecked(true);
					}
					cf.showing_limit(GCH_BI_OTHER_LOC,LOC_ED_type1_parrant,LOC_ED_type1,LOC_TV_type1,"750");					
					
					
					
					if(GCH_BI_BALCONY_PRES.equals("Yes")){
						BI_BALPRES_YES.setChecked(true);
						txtbpresent.setVisibility(v1.VISIBLE);	
						
						etbalconiespresent.setFocusableInTouchMode(true);
						etbalconiespresent.requestFocus();
						etbalconiespresent.setCursorVisible(true);
						
						etbalconiespresent.setVisibility(v1.VISIBLE);
						etbalconiespresent.setText(GCH_BI_NBALCONY_PRES);
						tblrwrail.setVisibility(visibility);
					
					if(GCH_BI_BALCONY_RAIL_PRES.equals("Yes")){BI_Balc_Rail_pres_yes.setChecked(true);}else{BI_Balc_Rail_pres_no.setChecked(true);}
					}
					else {
						BI_BALPRES_NO.setChecked(true);etbalconiespresent.setVisibility(v1.GONE);tblrwrail.setVisibility(v1.GONE);
						
					}
					
					if(GCH_BI_PERM_CONFIRMED.equals("Yes")){BI_Permit_Confirmed_yes.setChecked(true);etpermitconfirmed.setText(GCH_BI_PERM_CONF_DESC); permiconf_lin.setVisibility(View.VISIBLE);}
					else if(GCH_BI_PERM_CONFIRMED.equals("No")){
						permiconf_lin.setVisibility(View.GONE);
						BI_Permit_Confirmed_No.setChecked(true);
						etpermitconfirmed.setText("");}else
						{
							permiconf_lin.setVisibility(View.GONE);
							BI_Permit_Confirmed_BSI.setChecked(true);
							etpermitconfirmed.setText("");
						}
					
					if(GCH_BI_ADDI_STRU.equals("Yes")){BI_additionalstru_yes.setChecked(true);addistru_lin.setVisibility(v1.VISIBLE);etadditionalstructures.setText(GCH_BI_ADDI_STRU_DESC);}
					else{BI_additionalstru_no.setChecked(true);}
					if(!"".equals(GCH_BI_OTHER_LOC)){etlocationcomments.setText(GCH_BI_OTHER_LOC);}
					
					cf.showing_limit(GCH_BI_PERM_CONF_DESC,PC_ED_type1_parrant,PC_ED_type1,PD_TV_type1,"150"); 
					cf.showing_limit(GCH_BI_ADDI_STRU_DESC,AS_ED_type1_parrant,AS_ED_type1,AS_TV_type1,"150"); 
					
					cf.showing_limit(GCH_BI_OBSERV3_DESC,OBS3_ED_type1_parrant,OBS3_ED_type1,OBS3_TV_type1,"750");
					cf.showing_limit(GCH_BI_OBSERV2_DESC,OBS2_ED_type1_parrant,OBS2_ED_type1,OBS2_TV_type1,"750");
					cf.showing_limit(GCH_BI_OBSERV5_DESC,OBS5_ED_type1_parrant,OBS5_ED_type1,OBS5_TV_type1,"750");
					cf.showing_limit(GCH_BI_OBSERV4_DESC,OBS4_ED_type1_parrant,OBS4_ED_type1,OBS4_TV_type1,"750");
					System.out.println("GCH_BI_CONC_UNREINPER id "+GCH_BI_WSC_PRESENT);
					System.out.println("GCH_BI_WSC_PRESENT "+GCH_BI_WSC_PRESENT);
					if(GCH_BI_WSC_PRESENT.equals("0"))
					{
						cb_ws.setChecked(false);
						
					if("0".equals(GCH_BI_CONC_UNREINPER)){etwallstrucoptionopt1.setText("");}else{etwallstrucoptionopt1.setText(GCH_BI_CONC_UNREINPER);}
					if("0".equals(GCH_BI_CONC_REINPER)){etwallstrucoptionopt2.setText("");}else{etwallstrucoptionopt2.setText(GCH_BI_CONC_REINPER);}
					if("0".equals(GCH_BI_SOILD_CONCPER)){etwallstrucoptionopt3.setText("");}else{etwallstrucoptionopt3.setText(GCH_BI_SOILD_CONCPER);}
					if("0".equals(GCH_BI_WFRAMERPER)){etwallstrucoptionopt4.setText("");}else{etwallstrucoptionopt4.setText(GCH_BI_WFRAMERPER);}
					if("0".equals(GCH_BI_REIN_MASONRYPER)){etwallstrucoptionopt5.setText("");}else{etwallstrucoptionopt5.setText(GCH_BI_REIN_MASONRYPER);}
					if("0".equals(GCH_BI_UNREIN_MASONRYPER)){etwallstrucoptionopt6.setText("");}else{etwallstrucoptionopt6.setText(GCH_BI_UNREIN_MASONRYPER);}
					if("0".equals(GCH_BI_OTHER_WALLPER)){etwallstrucoptionopt7.setText("");}else{etwallstrucoptionopt7.setText(GCH_BI_OTHER_WALLPER);}
					if("".equals(GCH_BI_OTHER_WSTITLE)){etwallstrucothertxt.setText("");}else{etwallstrucothertxt.setText(GCH_BI_OTHER_WSTITLE);}
					
					cf.setvaluechk(GCH_BI_CONC_UNREIN, BI_WSC_QUES1_chk);
					cf.setvaluechk(GCH_BI_CONC_REIN, BI_WSC_QUES2_chk);
					cf.setvaluechk(GCH_BI_SOLID_CONC, BI_WSC_QUES3_chk);
					cf.setvaluechk(GCH_BI_WFRAME, BI_WSC_QUES4_chk);
					cf.setvaluechk(GCH_BI_REIN_MASONRY, BI_WSC_QUES5_chk);
					cf.setvaluechk(GCH_BI_UNREIN_MASONRY, BI_WSC_QUES6_chk);
					cf.setvaluechk(GCH_BI_OTHER_WS, BI_WSC_QUES7_chk);
					}
					else
					{
						cb_ws.setChecked(true);
						wstbl.setVisibility(v1.GONE);
						
					}
					
					if(GCH_BI_WALLCLADD_PRES.equals("0"))
					{
						
					cb_wc.setChecked(false);
					wctbl.setVisibility(v1.GONE);
					if("".equals(GCH_BI_OTHER_WCLADTITLE1)){etlist1other.setText("");}else{etlist1other.setText(GCH_BI_OTHER_WCLADTITLE1);}
					if("".equals(GCH_BI_OTHER_WCLADTITLE2)){etlist2other.setText("");}else{etlist2other.setText(GCH_BI_OTHER_WCLADTITLE2);}
					if("".equals(GCH_BI_OTHER_WCLADTITLE3)){etlist3other.setText("");}else{etlist3other.setText(GCH_BI_OTHER_WCLADTITLE3);}
					cf.setvaluechk(GCH_BI_ALUM_SLIDING,BI_WC_ALSlid_opt);
					cf.setvaluechk(GCH_BI_VINYL_SLIDING, BI_WC_VLSlid_opt);
					cf.setvaluechk(GCH_BI_WOOD_SLIDING, BI_WC_WLSlid_opt);
					cf.setvaluechk(GCH_BI_CEMENT_FIBER, BI_WC_CEMFIBER_opt);
					cf.setvaluechk(GCH_BI_STUCCO, BI_WC_Stucc_opt);
					cf.setvaluechk(GCH_BI_BRICKVENEER, BI_WC_BrickVeneer_opt);
					cf.setvaluechk(GCH_BI_PAINTEDBLOCK, BI_WC_PaintedBlock_opt);
					cf.setvaluechk(GCH_BI_OTHER_WCLAD1, BI_WC_list1_otheropt_chk);
					cf.setvaluechk(GCH_BI_OTHER_WCLAD2, BI_WC_list2_otheropt_chk);
					cf.setvaluechk(GCH_BI_OTHER_WCLAD3, BI_WC_list3_otheropt_chk);
					}
					else
					{
						cb_wc.setChecked(true);
						wctbl.setVisibility(v1.GONE);
					}
					
					
					if(GCH_BI_ISO_CLASSPRES.equals("0"))
					{
						cb_iso.setChecked(false);
						
						if(GCH_BI_FRAME.equals("0"))
							{
								etframe.setText("");
								etframe.setEnabled(false);
							}
						else
							{
								etframe.setText(GCH_BI_FRAME);
								etframe.setEnabled(true);
								((CheckBox) findViewById(R.id.GCH_B_ISO_Chk1)).setChecked(true);
							}
						if(GCH_BI_JOISTED.equals("0"))
							{
								etjoistmasonry.setText("");
								etjoistmasonry.setEnabled(false);
							}
						else
							{
								etjoistmasonry.setText(GCH_BI_JOISTED);
								etjoistmasonry.setEnabled(true);
								((CheckBox) findViewById(R.id.GCH_B_ISO_Chk2)).setChecked(true);
							}
						if(GCH_BI_NONCOMBUST.equals("0"))
							{
								etnoncombustib.setText("");
								etnoncombustib.setEnabled(false);
							}
						else
							{
								etnoncombustib.setText(GCH_BI_NONCOMBUST);
								etnoncombustib.setEnabled(true);
								((CheckBox) findViewById(R.id.GCH_B_ISO_Chk3)).setChecked(true);
							}
						if(GCH_BI_MASON_NONCOMBUST.equals("0"))
							{
								etmasonrynoncomb.setText("");
								etmasonrynoncomb.setEnabled(false);
							}
						else
							{
								etmasonrynoncomb.setText(GCH_BI_MASON_NONCOMBUST);
								etmasonrynoncomb.setEnabled(true);
								((CheckBox) findViewById(R.id.GCH_B_ISO_Chk4)).setChecked(true);
							}
						if(GCH_BI_MODIFY_FIRERESIST.equals("0"))
							{
								etmodifyfiresistance.setText("");
								etmodifyfiresistance.setEnabled(false);
							}
						else
							{
								etmodifyfiresistance.setText(GCH_BI_MODIFY_FIRERESIST);
								etmodifyfiresistance.setEnabled(true);
								((CheckBox) findViewById(R.id.GCH_B_ISO_Chk5)).setChecked(true);
							}
						if(GCH_BI_FIRERESIST.equals("0"))
							{
								etfiresistance.setText("");
								etfiresistance.setEnabled(false);
							}
						else
							{
								etfiresistance.setText(GCH_BI_FIRERESIST);
								etfiresistance.setEnabled(true);
								((CheckBox) findViewById(R.id.GCH_B_ISO_Chk6)).setChecked(true);
							}
					}
					else
					{
						cb_iso.setChecked(true);
						isotbl.setVisibility(v1.GONE);						
					}
					
					overallcomment.setText(GCH_BI_BUILD_COMMENTS);
					
					
					
					cf.showing_limit(GCH_BI_BUILD_COMMENTS,cf.SQ_ED_type1_parrant,cf.SQ_ED_type1,cf.SQ_TV_type1,"750"); 
					
				 }
				 else
				 {
					((RadioButton) findViewById(R.id.BI_Loc_txt1_BSI)).setChecked(true);
					((RadioButton) findViewById(R.id.BI_Loc_txt2_BSI)).setChecked(true);
					((RadioButton) findViewById(R.id.BI_Loc_txt3_BSI)).setChecked(true);
					((RadioButton) findViewById(R.id.BI_Loc_txt4_BSI)).setChecked(true);
					 
					 BI_OBSERV_txt1_no.setChecked(true);
					 BI_OBSERV_txt2_no.setChecked(true);
					 //BI_OBSERV_txt2_notdeter.setChecked(true);
					 BI_OBSERV_txt3_BSI.setChecked(true);obs_rw3lin.setVisibility(View.GONE);
					 BI_OBSERV_txt4_no.setChecked(true);
					 BI_OBSERV_txt5_BSI.setChecked(true);obs_rw5lin.setVisibility(View.GONE);
					 /**Set the beyond the scope of inspection Starts***/	
					 /*	loc_rw1_lin.setVisibility(View.VISIBLE);
						etloc1desc.setFocusableInTouchMode(true);
						etloc1desc.setText("Beyond scope of the inspection");
						
					 	loc_rw2_lin.setVisibility(View.VISIBLE);
						etloc2desc.setFocusableInTouchMode(true);
						etloc2desc.setText("Beyond scope of the inspection");
					 	
					 	loc_rw3_lin.setVisibility(View.VISIBLE);
						etloc3desc.setFocusableInTouchMode(true);
						etloc3desc.setText("Beyond scope of the inspection");
					    
						loc_rw4_lin.setVisibility(View.VISIBLE);
						etloc4desc.setFocusableInTouchMode(true);
						etloc4desc.setText("Beyond scope of the inspection");
					*/	
						/*etobserv3desc.setVisibility(v1.VISIBLE);
						etobserv3desc.setText("Beyond scope of the inspection");
						obs_rw3lin.setVisibility(v1.VISIBLE);
						
						etobserv5desc.setVisibility(v1.VISIBLE);
						etobserv5desc.setText("Beyond scope of the inspection");
						obs_rw5lin.setVisibility(v1.VISIBLE);
						
						*/obs_rw2lin.setVisibility(View.GONE);
						
						
						//etpermitconfirmed.setText("Beyond scope of the inspection");
					//	BI_Permit_Confirmed_BSI.setChecked(true);
						permiconf_lin.setVisibility(View.GONE);
						/**Set the beyond the scope of inspection ends***/
					 /** Auto population for the year of home **/
					 Cursor c2=cf.SelectTablefunction(cf.policyholder, " where GCH_PH_SRID='"+cf.selectedhomeid+"'");
					 /*if(c2.getCount()>0)
					 { */
						 c2.moveToFirst();
						 String Y_O_C=c2.getString(c2.getColumnIndex("GCH_YearBuilt"));
						 if(!Y_O_C.trim().equals(""))
							{
							 
							 	BI_YEARBUILT_opt[0].setChecked(true);
								if(getFromArray(Y_O_C,cf.BI_GBI_year))
								 {
									spin_yearbuilt.setSelection(adapter1.getPosition("Other"));
									etotheryearbuilt.setText(Y_O_C); 
								 }
								 else
								 {
									
									 spin_yearbuilt.setSelection(adapter1.getPosition(Y_O_C));
								 }
							}	
						 
						 String buildingsize =c2.getString(c2.getColumnIndex("BuidingSize"));
						 if(!buildingsize.equals(""))
						 {
							 etbuildingsize.setText(buildingsize);	 
						 }
						 
						 String noofstories =cf.decode(c2.getString(c2.getColumnIndex("GCH_PH_NOOFSTORIES")));
						 
						 if(!noofstories.equals(""))
						 {
							 spin_noofstories.setSelection(adapter.getPosition(noofstories));
						 }
						 
					 }
					
				// }
			}
		   catch(Exception e)
		   {
			   System.out.println("E ee"+e.getMessage());
			   
		   }
	}
	public boolean getFromArray(String sH_BI_GBI_YOC2, String[] bI_GBI_year)
	{
		for(int i=0;i < bI_GBI_year.length;i++)
		 {
			 if(sH_BI_GBI_YOC2.equals(bI_GBI_year[i]))
			 {
				 return false;
			 }
		 }
		return true;
	}

	class spinerlisner implements OnItemSelectedListener
	{

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			String spintxt;
			switch (arg0.getId())
			{
			
			 case R.id.spin_noofstories:
				 spintxt=spin_noofstories.getSelectedItem().toString();
				 ntories = noofstories[arg2];
				 if(arg2>0 && arg2<6)
				 {
					 try
					 {
						 DisableWS(0,true);// Set enable for all the  check box  
						 int cnt=Integer.parseInt(ntories);
						 DisableWS(cnt,false);
					 }
					 catch (Exception e)
					 {
						 DisableWS(0,true);// Set enable for all the  check box 
					 }
				 }
				 else
				 {
					 DisableWS(0,true);// Set enable for all the  check box 
				 }
				
				 

					 
			 break;
			 case R.id.spin_yearbuilt:
				 spintxt=spin_yearbuilt.getSelectedItem().toString();
				 ybuilt = yearbuilt[arg2];
	 			 if(spintxt.equals("Other"))
	 			 {
	 				 
		 				etotheryearbuilt.setFocusableInTouchMode(true);
						etotheryearbuilt.requestFocus();
						etotheryearbuilt.setCursorVisible(true);
		 				etotheryearbuilt.setVisibility(v1.VISIBLE);
	 				 
		 				if(!setvalue)
		 				 { 
			 				BI_YEARBUILT_opt[1].setChecked(false);
			 				BI_YEARBUILT_opt[0].setChecked(true);
			 				BI_YEARBUILT_opt[2].setChecked(false);
		 				 }
		 				 else
		 				 {
		 					setvalue=false;     
		 				 }
	 				
	 			 }
	 			 else if(spintxt.equals("Unknown"))
	 			 {
	 				etotheryearbuilt.setText("");
	 				if(!setvalue)
	 				 { 
		 				BI_YEARBUILT_opt[1].setChecked(true);
		 				BI_YEARBUILT_opt[0].setChecked(false);
		 				BI_YEARBUILT_opt[2].setChecked(false);
	 				 }
	 				 else
	 				 {
	 					setvalue=false;     
	 				 }
	 				 etotheryearbuilt.setVisibility(v1.GONE);
	 			 }
	 			 else
	 			 {
	 				etotheryearbuilt.setText("");
	 				if(!setvalue)
	 				 { 
		 				BI_YEARBUILT_opt[1].setChecked(false);
		 				BI_YEARBUILT_opt[0].setChecked(true);
		 				BI_YEARBUILT_opt[2].setChecked(false);
	 				 }
	 				 else
	 				 {
	 					setvalue=false;     
	 				 }
	 				
	 				 etotheryearbuilt.setVisibility(v1.GONE);
	 			 }
				
			 break;
			}
			 
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
private void setObjectsToView() {

	
	/** DECLARING TABLE ROW HEADER AND ITS CONTENT TABLE **/
    genrw = (TableRow)findViewById(R.id.BI_GEN_HEADER);
    locrw = (TableRow)findViewById(R.id.BI_Location_Header);
    obsrw = (TableRow)findViewById(R.id.BI_Observation_Header);
    wsrw = (TableRow)findViewById(R.id.BI_WS_Header);
    wcrw = (TableRow)findViewById(R.id.BI_WC_Header);
    isorw = (TableRow)findViewById(R.id.BI_ISO_Header);
	    
	    gentbl = (TableLayout)findViewById(R.id.gen_table);
	    loctbl = (LinearLayout)findViewById(R.id.location_table);
	    obstbl = (LinearLayout)findViewById(R.id.observation_table);
	    wstbl = (LinearLayout)findViewById(R.id.ws_table);
	    wctbl = (LinearLayout)findViewById(R.id.wc_table);
	    isotbl = (LinearLayout)findViewById(R.id.iso_table);
	    
	    /** DECLARING CHECKBOX OF WS, WC,ISO NOT APPLICABLE **/
	    cb_ws = (CheckBox)findViewById(R.id.ws_chkbx);
	    cb_wc = (CheckBox)findViewById(R.id.wc_chkbx);
	    cb_iso = (CheckBox)findViewById(R.id.iso_chkbx);
	    
	  /*  *//** DECLARING AND SETTING THE VALUE FOR LOCATION TEXT CONTENT **//*
	   ((TextView)findViewById(R.id.loc_txt1)).setText(Html.fromHtml(cf.redcolor+" Within 1,500 FT of Salt Water:"));
	   ((TextView)findViewById(R.id.loc_txt2)).setText(Html.fromHtml(cf.redcolor+" Within 5 Miles of Salt Water:"));
	   ((TextView)findViewById(R.id.loc_txt3)).setText(Html.fromHtml(cf.redcolor+" Within 40 FT of Commercial Structure:"));
	   ((TextView)findViewById(R.id.loc_txt4)).setText(Html.fromHtml(cf.redcolor+" Subject to Brush / Forest Fires:"));
	   
	   *//** DECLARING AND SETTING THE VALUE FOR OBSERVATION TEXT CONTENT **//*
	   ((TextView)findViewById(R.id.obs_txt1)).setText(Html.fromHtml(cf.redcolor+" Construction originally intended for current use:"));
	   ((TextView)findViewById(R.id.obs_txt2)).setText(Html.fromHtml(cf.redcolor+" Alterations / Remodeling noted since original construction:"));
	   ((TextView)findViewById(R.id.obs_txt3)).setText(Html.fromHtml(cf.redcolor+" Professional construction practices appears present:"));
	   ((TextView)findViewById(R.id.obs_txt4)).setText(Html.fromHtml(cf.redcolor+" Visible non-standard / home made / do it yourself alterations:"));
	   ((TextView)findViewById(R.id.obs_txt5)).setText(Html.fromHtml(cf.redcolor+" Permit information for alterations confirmed:"));
	    
	   *//** DECLARING LINEARLAYOUT TO MAKE VISIBLE IF YES/NO RADIOBUTTON CHECKED FOR ROW 3 AND 4 **/
	   obs_rw3lin = (LinearLayout)findViewById(R.id.obs_rw3_lin);
	   obs_rw4lin = (LinearLayout)findViewById(R.id.obs_rw4_lin);
	   obs_rw2lin = (LinearLayout)findViewById(R.id.obs_rw2_lin);
	   obs_rw5lin = (LinearLayout)findViewById(R.id.obs_rw5_lin);
	   loclin = (LinearLayout)findViewById(R.id.loclin);
	   addistru_lin= (LinearLayout)findViewById(R.id.addistru_lin);
	   permiconf_lin= (LinearLayout)findViewById(R.id.permiconf_lin);
	   

	   
	   ((TextView)findViewById(R.id.tvbuildingsiz)).setText(Html.fromHtml(cf.redcolor+" Building Size"));
	   ((TextView)findViewById(R.id.tvnoofstories)).setText(Html.fromHtml(cf.redcolor+" Number of Stories"));
	   ((TextView)findViewById(R.id.tvyearbuilt)).setText(Html.fromHtml(cf.redcolor+" Year Built"));
	   ((TextView)findViewById(R.id.tvbuildingsiz)).setText(Html.fromHtml(cf.redcolor+" Buidling Size"));
	   ((TextView)findViewById(R.id.tvneeighbourhood)).setText(Html.fromHtml(cf.redcolor+" Neighbourhood Is"));
	   ((TextView)findViewById(R.id.appronoofbalconies)).setText(Html.fromHtml(cf.redcolor+" Approximate Number of Balconies"));
	   
	   
	
	/***  RADIOBUTTON FOR GENERAL DATA STARTS HERE***/
	
	BI_INS_txt1_opt[0]=(RadioButton) findViewById(R.id.BI_INS_txt1_opt1);
    BI_INS_txt1_opt[1]=(RadioButton) findViewById(R.id.BI_INS_txt1_opt2);
    BI_INS_txt1_opt[2]=(RadioButton) findViewById(R.id.BI_INS_txt1_opt3);
    BI_INS_txt1_opt[3]=(RadioButton) findViewById(R.id.BI_INS_txt1_opt4);
    BI_INS_txt1_opt[4]=(RadioButton) findViewById(R.id.BI_INS_txt1_opt5);
    BI_INS_txt1_opt[5]=(RadioButton) findViewById(R.id.BI_INS_txt1_opt6);BI_INS_txt1_opt[5].setOnClickListener(new BI_clicker());
    etinsuredother = (EditText) findViewById(R.id.etinsuredother);
    ArrayListenerInsured(BI_INS_txt1_opt);   
    
    BI_BUIL_OCCU_chk[0]=(CheckBox) findViewById(R.id.BI_BUIL_OCCU_chk1);  
    BI_BUIL_OCCU_chk[0].setOnClickListener(new BI_clicker());
    BI_BUIL_OCCU_chk[1]=(CheckBox) findViewById(R.id.BI_BUIL_OCCU_chk2);
    BI_BUIL_OCCU_chk[1].setOnClickListener(new BI_clicker());
    BI_BUIL_OCCU_chk[2]=(CheckBox) findViewById(R.id.BI_BUIL_OCCU_chk3);
    BI_BUIL_OCCU_chk[2].setOnClickListener(new BI_clicker());
    
    etbuild_occup1 = (EditText)findViewById(R.id.etbuild_occup1);
    etbuild_occup2 = (EditText)findViewById(R.id.etbuild_occup2);
    etbuild_occup1.setOnTouchListener(new Touch_Listener(1));
    etbuild_occup2.setOnTouchListener(new Touch_Listener(2));
    
    BI_OCCU_TYPE_opt[0]=(RadioButton) findViewById(R.id.BI_OCCU_TYPE_opt1);
    BI_OCCU_TYPE_opt[1]=(RadioButton) findViewById(R.id.BI_OCCU_TYPE_opt2);
    BI_OCCU_TYPE_opt[2]=(RadioButton) findViewById(R.id.BI_OCCU_TYPE_opt3);
    BI_OCCU_TYPE_opt[3]=(RadioButton) findViewById(R.id.BI_OCCU_TYPE_opt4);
    BI_OCCU_TYPE_opt[4]=(RadioButton) findViewById(R.id.BI_OCCU_TYPE_opt5);
    BI_OCCU_TYPE_opt[5]=(RadioButton) findViewById(R.id.BI_OCCU_TYPE_opt6);
    BI_OCCU_TYPE_opt[6]=(RadioButton) findViewById(R.id.BI_OCCU_TYPE_opt7);
    BI_OCCU_TYPE_opt[7]=(RadioButton) findViewById(R.id.BI_OCCU_TYPE_opt8);   
    etbuildoccuptype = (EditText) findViewById(R.id.etbuildoccuptype);
    etbuildoccuptype.setOnTouchListener(new Touch_Listener(3));
    ArrayListenerOcutype(BI_OCCU_TYPE_opt);
    
    BI_Observ_Type_opt[0]=(RadioButton) findViewById(R.id.BI_Observ_Type_opt1);
    BI_Observ_Type_opt[1]=(RadioButton) findViewById(R.id.BI_Observ_Type_opt2);
    BI_Observ_Type_opt[2]=(RadioButton) findViewById(R.id.BI_Observ_Type_opt3);
    BI_Observ_Type_opt[3]=(RadioButton) findViewById(R.id.BI_Observ_Type_opt4);
    BI_Observ_Type_opt[4]=(RadioButton) findViewById(R.id.BI_Observ_Type_opt5); 
    etobservtypeother = (EditText) findViewById(R.id.etobservtypeother);
    etobservtypeother.setOnTouchListener(new Touch_Listener(4));
    ArrayListenerObservype(BI_Observ_Type_opt);
    
    BI_NEIGH_txt_opt[0]=(RadioButton) findViewById(R.id.BI_NEIGH_txt_opt1);
    BI_NEIGH_txt_opt[1]=(RadioButton) findViewById(R.id.BI_NEIGH_txt_opt2);
    BI_NEIGH_txt_opt[2]=(RadioButton) findViewById(R.id.BI_NEIGH_txt_opt3);
    BI_NEIGH_txt_opt[3]=(RadioButton) findViewById(R.id.BI_NEIGH_txt_opt4);
    etneighbourhoodother = (EditText) findViewById(R.id.etneighbourhood);
    etneighbourhoodother.setOnTouchListener(new Touch_Listener(5));
    ArrayListenerNeigh(BI_NEIGH_txt_opt);
     
     
     BI_YEARBUILT_opt[0]=(RadioButton) findViewById(R.id.BI_YEARBUILT_opt1);
     BI_YEARBUILT_opt[1]=(RadioButton) findViewById(R.id.BI_YEARBUILT_opt2);
     BI_YEARBUILT_opt[2]=(RadioButton) findViewById(R.id.BI_YEARBUILT_opt3);
     ArrayListenerYear(BI_YEARBUILT_opt);
     
     
     etbuildingsize = (EditText) findViewById(R.id.etbuildingsize);
     etbuildingsize.setOnTouchListener(new Touch_Listener(15));
     BI_BALPRES_YES=(RadioButton) findViewById(R.id.BI_BALPRES_YES);BI_BALPRES_YES.setOnClickListener(new BI_clicker());
     BI_BALPRES_NO=(RadioButton) findViewById(R.id.BI_BALPRES_NO);BI_BALPRES_NO.setOnClickListener(new BI_clicker());
     txtbpresent = (TextView)findViewById(R.id.appronoofbalconies);
     etbalconiespresent = (EditText) findViewById(R.id.etnoofbalconiespresent);
     etbalconiespresent.setOnTouchListener(new Touch_Listener(15));
     etotheryearbuilt = (EditText)this.findViewById(R.id.etotheryearbuilt);
     etotheryearbuilt.setOnTouchListener(new Touch_Listener(15));    
     
     BI_Permit_Confirmed_yes=(RadioButton) findViewById(R.id.BI_Permit_Confirmed_yes);BI_Permit_Confirmed_yes.setOnClickListener(new BI_clicker());
     BI_Permit_Confirmed_No=(RadioButton) findViewById(R.id.BI_Permit_Confirmed_No);BI_Permit_Confirmed_No.setOnClickListener(new BI_clicker());
     BI_Permit_Confirmed_BSI=(RadioButton) findViewById(R.id.BI_Permit_Confirmed_BSI);BI_Permit_Confirmed_BSI.setOnClickListener(new BI_clicker());
     etpermitconfirmed = (EditText) findViewById(R.id.etpermitconfirmed);
     etpermitconfirmed.setOnTouchListener(new Touch_Listener(15));    
     
     tblrwrail = (TableRow)findViewById(R.id.balrailrw);
     BI_Balc_Rail_pres_yes=(RadioButton) findViewById(R.id.BI_Balc_Rail_pres_yes);BI_Balc_Rail_pres_yes.setOnClickListener(new BI_clicker());
     BI_Balc_Rail_pres_no=(RadioButton) findViewById(R.id.BI_Balc_Rail_pres_no);BI_Balc_Rail_pres_no.setOnClickListener(new BI_clicker());
    
     
     BI_additionalstru_yes=(RadioButton) findViewById(R.id.BI_additionalstru_yes);BI_additionalstru_yes.setOnClickListener(new BI_clicker());
     BI_additionalstru_no=(RadioButton) findViewById(R.id.BI_additionalstru_no);BI_additionalstru_no.setOnClickListener(new BI_clicker());
     etadditionalstructures = (EditText) findViewById(R.id.etadditionalstructures);
     etadditionalstructures.setOnTouchListener(new Touch_Listener(15));
     
     BI_Loc_txt1_yes=(RadioButton) findViewById(R.id.BI_Loc_txt1_yes);//BI_Loc_txt1_yes.setOnClickListener(new BI_clicker());
     BI_Loc_txt1_no=(RadioButton) findViewById(R.id.BI_Loc_txt1_no);//BI_Loc_txt1_no.setOnClickListener(new BI_clicker());
     BI_Loc_txt1_nd=(RadioButton) findViewById(R.id.BI_Loc_txt1_notdetemined);//BI_Loc_txt1_nd.setOnClickListener(new BI_clicker());
     /*loc_rw1_lin =(LinearLayout) findViewById(R.id.loc_rw1_lin);
     loc_rw1_lin_child =(LinearLayout) findViewById(R.id.loc_rw1_type);
     loc_rw1_parrent =(LinearLayout) findViewById(R.id.loc_rw1_parrent);
     loc_rw1_txt =(TextView) findViewById(R.id.loc_rw1_txt); 
     etloc1desc =(EditText)  findViewById(R.id.etloc1desc);
     etloc1desc.addTextChangedListener(new BI_textwatcher(15));
     etloc1desc.setOnTouchListener(new Touch_Listener(15));*/
     
     BI_Loc_txt2_yes=(RadioButton) findViewById(R.id.BI_Loc_txt2_yes);//BI_Loc_txt2_yes.setOnClickListener(new BI_clicker());
     BI_Loc_txt2_no=(RadioButton) findViewById(R.id.BI_Loc_txt2_no);//BI_Loc_txt2_no.setOnClickListener(new BI_clicker());
     BI_Loc_txt2_nd=(RadioButton) findViewById(R.id.BI_Loc_txt2_notdetemined);//BI_Loc_txt2_nd.setOnClickListener(new BI_clicker());
  /*   loc_rw2_lin =(LinearLayout) findViewById(R.id.loc_rw2_lin);
     loc_rw2_lin_child =(LinearLayout) findViewById(R.id.loc_rw2_type);
     loc_rw2_parrent =(LinearLayout) findViewById(R.id.loc_rw2_parrent);
     loc_rw2_txt =(TextView) findViewById(R.id.loc_rw2_txt);
     etloc2desc =(EditText)  findViewById(R.id.etloc2desc);
     etloc2desc.addTextChangedListener(new BI_textwatcher(16));
     etloc2desc.setOnTouchListener(new Touch_Listener(16));*/
 
     BI_Loc_txt3_yes=(RadioButton) findViewById(R.id.BI_Loc_txt3_yes);//BI_Loc_txt3_yes.setOnClickListener(new BI_clicker());
     BI_Loc_txt3_no=(RadioButton) findViewById(R.id.BI_Loc_txt3_no);//BI_Loc_txt3_no.setOnClickListener(new BI_clicker());
     BI_Loc_txt3_nd=(RadioButton) findViewById(R.id.BI_Loc_txt3_notdetemined);//BI_Loc_txt3_nd.setOnClickListener(new BI_clicker());
    /* loc_rw3_lin =(LinearLayout) findViewById(R.id.loc_rw3_lin);
     loc_rw3_lin_child =(LinearLayout) findViewById(R.id.loc_rw3_type);
     loc_rw3_parrent =(LinearLayout) findViewById(R.id.loc_rw3_parrent);
     loc_rw3_txt =(TextView) findViewById(R.id.loc_rw3_txt); 
     etloc3desc =(EditText)  findViewById(R.id.etloc3desc);
     etloc3desc.addTextChangedListener(new BI_textwatcher(17));
     etloc3desc.setOnTouchListener(new Touch_Listener(17));*/
     
     BI_Loc_txt4_yes=(RadioButton) findViewById(R.id.BI_Loc_txt4_yes);BI_Loc_txt4_yes.setOnClickListener(new BI_clicker());
     BI_Loc_txt4_no=(RadioButton) findViewById(R.id.BI_Loc_txt4_no);BI_Loc_txt4_no.setOnClickListener(new BI_clicker());
     BI_Loc_txt4_nd=(RadioButton) findViewById(R.id.BI_Loc_txt4_notdetemined);BI_Loc_txt4_nd.setOnClickListener(new BI_clicker());
   /*  loc_rw4_lin =(LinearLayout) findViewById(R.id.loc_rw4_lin);
     loc_rw4_parrent =(LinearLayout) findViewById(R.id.loc_rw4_parrent);
     loc_rw4_lin_child =(LinearLayout) findViewById(R.id.loc_rw4_type);
     loc_rw4_txt =(TextView) findViewById(R.id.loc_rw4_txt); 
     etloc4desc =(EditText)  findViewById(R.id.etloc4desc);
     etloc4desc.addTextChangedListener(new BI_textwatcher(18));
     etloc4desc.setOnTouchListener(new Touch_Listener(18));*/
     
     etlocationcomments= (EditText)findViewById(R.id.commentlocationother);
     
     
     BI_OBSERV_txt1_yes=(RadioButton) findViewById(R.id.BI_OBSERV_txt1_yes);BI_OBSERV_txt1_yes.setOnClickListener(new BI_clicker());
     BI_OBSERV_txt1_no=(RadioButton) findViewById(R.id.BI_OBSERV_txt1_no);BI_OBSERV_txt1_no.setOnClickListener(new BI_clicker());
     
     BI_OBSERV_txt2_yes=(RadioButton) findViewById(R.id.BI_OBSERV_txt2_yes);BI_OBSERV_txt2_yes.setOnClickListener(new BI_clicker());
     BI_OBSERV_txt2_no=(RadioButton) findViewById(R.id.BI_OBSERV_txt2_no);BI_OBSERV_txt2_no.setOnClickListener(new BI_clicker());
     BI_OBSERV_txt2_notdeter=(RadioButton) findViewById(R.id.BI_OBSERV_txt2_notdeter);BI_OBSERV_txt2_notdeter.setOnClickListener(new BI_clicker());
     BI_OBSERV_txt3_yes=(RadioButton) findViewById(R.id.BI_OBSERV_txt3_yes);BI_OBSERV_txt3_yes.setOnClickListener(new BI_clicker());
     BI_OBSERV_txt3_no=(RadioButton) findViewById(R.id.BI_OBSERV_txt3_no);BI_OBSERV_txt3_no.setOnClickListener(new BI_clicker());
     BI_OBSERV_txt3_BSI=(RadioButton) findViewById(R.id.BI_OBSERV_txt3_BSI);BI_OBSERV_txt3_BSI.setOnClickListener(new BI_clicker());
     
     BI_OBSERV_txt4_yes=(RadioButton) findViewById(R.id.BI_OBSERV_txt4_yes);BI_OBSERV_txt4_yes.setOnClickListener(new BI_clicker());
     BI_OBSERV_txt4_no=(RadioButton) findViewById(R.id.BI_OBSERV_txt4_no);BI_OBSERV_txt4_no.setOnClickListener(new BI_clicker());
     
     BI_OBSERV_txt5_yes=(RadioButton) findViewById(R.id.BI_OBSERV_txt5_yes);BI_OBSERV_txt5_yes.setOnClickListener(new BI_clicker());
     BI_OBSERV_txt5_no=(RadioButton) findViewById(R.id.BI_OBSERV_txt5_no);BI_OBSERV_txt5_no.setOnClickListener(new BI_clicker());
     BI_OBSERV_txt5_BSI=(RadioButton) findViewById(R.id.BI_OBSERV_txt5_BSI);BI_OBSERV_txt5_BSI.setOnClickListener(new BI_clicker());
     etobserv3desc = (EditText)this.findViewById(R.id.etobserv3desc);    
    // etobserv4desc = (EditText)this.findViewById(R.id.etobserv4desc);
     etobserv2desc = (EditText)this.findViewById(R.id.etobserv2desc);    
     etobserv5desc = (EditText)this.findViewById(R.id.etobserv5desc);
     
     BI_WSC_QUES1_chk[0]=(CheckBox) findViewById(R.id.BI_WSC_QUES1_chk1);
     BI_WSC_QUES1_chk[1]=(CheckBox) findViewById(R.id.BI_WSC_QUES1_chk2);
     BI_WSC_QUES1_chk[2]=(CheckBox) findViewById(R.id.BI_WSC_QUES1_chk3);
     BI_WSC_QUES1_chk[3]=(CheckBox) findViewById(R.id.BI_WSC_QUES1_chk4);
     BI_WSC_QUES1_chk[4]=(CheckBox) findViewById(R.id.BI_WSC_QUES1_chk5);
     BI_WSC_QUES1_chk[5]=(CheckBox) findViewById(R.id.BI_WSC_QUES1_chk6);
    
     
     etwallstrucoptionopt1 = (EditText)this.findViewById(R.id.etwallstrucoptionopt1);
     ArrayListenerWSC(BI_WSC_QUES1_chk,etwallstrucoptionopt1);
     
     BI_WSC_QUES2_chk[0]=(CheckBox) findViewById(R.id.BI_WSC_QUES2_chk1);
     BI_WSC_QUES2_chk[1]=(CheckBox) findViewById(R.id.BI_WSC_QUES2_chk2);
     BI_WSC_QUES2_chk[2]=(CheckBox) findViewById(R.id.BI_WSC_QUES2_chk3);
     BI_WSC_QUES2_chk[3]=(CheckBox) findViewById(R.id.BI_WSC_QUES2_chk4);
     BI_WSC_QUES2_chk[4]=(CheckBox) findViewById(R.id.BI_WSC_QUES2_chk5);
     BI_WSC_QUES2_chk[5]=(CheckBox) findViewById(R.id.BI_WSC_QUES2_chk6);
     etwallstrucoptionopt2 = (EditText)this.findViewById(R.id.edt_WS_Unrein);
     ArrayListenerWSC(BI_WSC_QUES2_chk,etwallstrucoptionopt2);
     
     BI_WSC_QUES3_chk[0]=(CheckBox) findViewById(R.id.BI_WSC_QUES3_chk1);
     BI_WSC_QUES3_chk[1]=(CheckBox) findViewById(R.id.BI_WSC_QUES3_chk2);
     BI_WSC_QUES3_chk[2]=(CheckBox) findViewById(R.id.BI_WSC_QUES3_chk3);
     BI_WSC_QUES3_chk[3]=(CheckBox) findViewById(R.id.BI_WSC_QUES3_chk4);
     BI_WSC_QUES3_chk[4]=(CheckBox) findViewById(R.id.BI_WSC_QUES3_chk5);
     BI_WSC_QUES3_chk[5]=(CheckBox) findViewById(R.id.BI_WSC_QUES3_chk6);
     etwallstrucoptionopt3 = (EditText)this.findViewById(R.id.edt_WS_rein);
     ArrayListenerWSC(BI_WSC_QUES3_chk,etwallstrucoptionopt3);
     
     BI_WSC_QUES4_chk[0]=(CheckBox) findViewById(R.id.BI_WSC_QUES4_chk1);
     BI_WSC_QUES4_chk[1]=(CheckBox) findViewById(R.id.BI_WSC_QUES4_chk2);
     BI_WSC_QUES4_chk[2]=(CheckBox) findViewById(R.id.BI_WSC_QUES4_chk3);
     BI_WSC_QUES4_chk[3]=(CheckBox) findViewById(R.id.BI_WSC_QUES4_chk4);
     BI_WSC_QUES4_chk[4]=(CheckBox) findViewById(R.id.BI_WSC_QUES4_chk5);
     BI_WSC_QUES4_chk[5]=(CheckBox) findViewById(R.id.BI_WSC_QUES4_chk6);
     etwallstrucoptionopt4 = (EditText)this.findViewById(R.id.edt_WS_woodmetalframe);
     ArrayListenerWSC(BI_WSC_QUES4_chk,etwallstrucoptionopt4);
     
     BI_WSC_QUES5_chk[0]=(CheckBox) findViewById(R.id.BI_WSC_QUES5_chk1);
     BI_WSC_QUES5_chk[1]=(CheckBox) findViewById(R.id.BI_WSC_QUES5_chk2);
     BI_WSC_QUES5_chk[2]=(CheckBox) findViewById(R.id.BI_WSC_QUES5_chk3);
     BI_WSC_QUES5_chk[3]=(CheckBox) findViewById(R.id.BI_WSC_QUES5_chk4);
     BI_WSC_QUES5_chk[4]=(CheckBox) findViewById(R.id.BI_WSC_QUES5_chk5);
     BI_WSC_QUES5_chk[5]=(CheckBox) findViewById(R.id.BI_WSC_QUES5_chk6);
     etwallstrucoptionopt5 = (EditText)this.findViewById(R.id.edt_WS_reinformasonry);
     ArrayListenerWSC(BI_WSC_QUES5_chk,etwallstrucoptionopt5);
     
     BI_WSC_QUES6_chk[0]=(CheckBox) findViewById(R.id.BI_WSC_QUES6_chk1);
     BI_WSC_QUES6_chk[1]=(CheckBox) findViewById(R.id.BI_WSC_QUES6_chk2);
     BI_WSC_QUES6_chk[2]=(CheckBox) findViewById(R.id.BI_WSC_QUES6_chk3);
     BI_WSC_QUES6_chk[3]=(CheckBox) findViewById(R.id.BI_WSC_QUES6_chk4);
     BI_WSC_QUES6_chk[4]=(CheckBox) findViewById(R.id.BI_WSC_QUES6_chk5);
     BI_WSC_QUES6_chk[5]=(CheckBox) findViewById(R.id.BI_WSC_QUES6_chk6);
     etwallstrucoptionopt6 = (EditText)this.findViewById(R.id.edt_WS_unreinformasonry);
     ArrayListenerWSC(BI_WSC_QUES6_chk,etwallstrucoptionopt6);
     
     BI_WSC_QUES7_chk[0]=(CheckBox) findViewById(R.id.BI_WSC_QUES7_chk1);
     BI_WSC_QUES7_chk[1]=(CheckBox) findViewById(R.id.BI_WSC_QUES7_chk2);
     BI_WSC_QUES7_chk[2]=(CheckBox) findViewById(R.id.BI_WSC_QUES7_chk3);
     BI_WSC_QUES7_chk[3]=(CheckBox) findViewById(R.id.BI_WSC_QUES7_chk4);
     BI_WSC_QUES7_chk[4]=(CheckBox) findViewById(R.id.BI_WSC_QUES7_chk5);
     BI_WSC_QUES7_chk[5]=(CheckBox) findViewById(R.id.BI_WSC_QUES7_chk6);
     etwallstrucoptionopt7 = (EditText)this.findViewById(R.id.edt_WS_other);
     etwallstrucothertxt = (EditText)this.findViewById(R.id.etwallstrucothertxt);
     ArrayListenerWSC(BI_WSC_QUES7_chk,etwallstrucoptionopt7);
     
     BI_WC_ALSlid_opt[0]=(CheckBox) findViewById(R.id.BI_WC_ALSlid_opt1);
     BI_WC_ALSlid_opt[1]=(CheckBox) findViewById(R.id.BI_WC_ALSlid_opt2);
     BI_WC_ALSlid_opt[2]=(CheckBox) findViewById(R.id.BI_WC_ALSlid_opt3);
     BI_WC_ALSlid_opt[3]=(CheckBox) findViewById(R.id.BI_WC_ALSlid_opt4);
     BI_WC_ALSlid_opt[4]=(CheckBox) findViewById(R.id.BI_WC_ALSlid_opt5);
     BI_WC_ALSlid_opt[5]=(CheckBox) findViewById(R.id.BI_WC_ALSlid_opt6);
     ArrayListenerClickCheck(BI_WC_ALSlid_opt);
     
     BI_WC_VLSlid_opt[0]=(CheckBox) findViewById(R.id.BI_WC_VLSlid_opt1);
     BI_WC_VLSlid_opt[1]=(CheckBox) findViewById(R.id.BI_WC_VLSlid_opt2);
     BI_WC_VLSlid_opt[2]=(CheckBox) findViewById(R.id.BI_WC_VLSlid_opt3);
     BI_WC_VLSlid_opt[3]=(CheckBox) findViewById(R.id.BI_WC_VLSlid_opt4);
     BI_WC_VLSlid_opt[4]=(CheckBox) findViewById(R.id.BI_WC_VLSlid_opt5);
     BI_WC_VLSlid_opt[5]=(CheckBox) findViewById(R.id.BI_WC_VLSlid_opt6);
     ArrayListenerClickCheck(BI_WC_VLSlid_opt);
     
     BI_WC_WLSlid_opt[0]=(CheckBox) findViewById(R.id.BI_WC_WLSlid_opt1);
     BI_WC_WLSlid_opt[1]=(CheckBox) findViewById(R.id.BI_WC_WLSlid_opt2);
     BI_WC_WLSlid_opt[2]=(CheckBox) findViewById(R.id.BI_WC_WLSlid_opt3);
     BI_WC_WLSlid_opt[3]=(CheckBox) findViewById(R.id.BI_WC_WLSlid_opt4);
     BI_WC_WLSlid_opt[4]=(CheckBox) findViewById(R.id.BI_WC_WLSlid_opt5);
     BI_WC_WLSlid_opt[5]=(CheckBox) findViewById(R.id.BI_WC_WLSlid_opt6);
     ArrayListenerClickCheck(BI_WC_WLSlid_opt);
     
     BI_WC_CEMFIBER_opt[0]=(CheckBox) findViewById(R.id.BI_WC_CEMFIBER_opt1);
     BI_WC_CEMFIBER_opt[1]=(CheckBox) findViewById(R.id.BI_WC_CEMFIBER_opt2);
     BI_WC_CEMFIBER_opt[2]=(CheckBox) findViewById(R.id.BI_WC_CEMFIBER_opt3);
     BI_WC_CEMFIBER_opt[3]=(CheckBox) findViewById(R.id.BI_WC_CEMFIBER_opt4);
     BI_WC_CEMFIBER_opt[4]=(CheckBox) findViewById(R.id.BI_WC_CEMFIBER_opt5);
     BI_WC_CEMFIBER_opt[5]=(CheckBox) findViewById(R.id.BI_WC_CEMFIBER_opt6);
     ArrayListenerClickCheck(BI_WC_CEMFIBER_opt);
     
     BI_WC_list1_otheropt_chk[0]=(CheckBox) findViewById(R.id.BI_WC_list1_otheropt1);
     BI_WC_list1_otheropt_chk[1]=(CheckBox) findViewById(R.id.BI_WC_list1_otheropt2);
     BI_WC_list1_otheropt_chk[2]=(CheckBox) findViewById(R.id.BI_WC_list1_otheropt3);
     BI_WC_list1_otheropt_chk[3]=(CheckBox) findViewById(R.id.BI_WC_list1_otheropt4);
     BI_WC_list1_otheropt_chk[4]=(CheckBox) findViewById(R.id.BI_WC_list1_otheropt5);
     BI_WC_list1_otheropt_chk[5]=(CheckBox) findViewById(R.id.BI_WC_list1_otheropt6);
     ArrayListenerClickCheck(BI_WC_list1_otheropt_chk);
     
     BI_WC_Stucc_opt[0]=(CheckBox) findViewById(R.id.BI_WC_Stucc_opt1);
     BI_WC_Stucc_opt[1]=(CheckBox) findViewById(R.id.BI_WC_Stucc_opt2);
     BI_WC_Stucc_opt[2]=(CheckBox) findViewById(R.id.BI_WC_Stucc_opt3);
     BI_WC_Stucc_opt[3]=(CheckBox) findViewById(R.id.BI_WC_Stucc_opt4);
     BI_WC_Stucc_opt[4]=(CheckBox) findViewById(R.id.BI_WC_Stucc_opt5);
     BI_WC_Stucc_opt[5]=(CheckBox) findViewById(R.id.BI_WC_Stucc_opt6);     
     ArrayListenerClickCheck(BI_WC_Stucc_opt);
     
     BI_WC_BrickVeneer_opt[0]=(CheckBox) findViewById(R.id.BI_WC_BrickVeneer_opt1);
     BI_WC_BrickVeneer_opt[1]=(CheckBox) findViewById(R.id.BI_WC_BrickVeneer_opt2);
     BI_WC_BrickVeneer_opt[2]=(CheckBox) findViewById(R.id.BI_WC_BrickVeneer_opt3);
     BI_WC_BrickVeneer_opt[3]=(CheckBox) findViewById(R.id.BI_WC_BrickVeneer_opt4);
     BI_WC_BrickVeneer_opt[4]=(CheckBox) findViewById(R.id.BI_WC_BrickVeneer_opt5);
     BI_WC_BrickVeneer_opt[5]=(CheckBox) findViewById(R.id.BI_WC_BrickVeneer_opt6);
     ArrayListenerClickCheck(BI_WC_BrickVeneer_opt);
     
     BI_WC_PaintedBlock_opt[0]=(CheckBox) findViewById(R.id.BI_WC_PaintedBlock_opt1);
     BI_WC_PaintedBlock_opt[1]=(CheckBox) findViewById(R.id.BI_WC_PaintedBlock_opt2);
     BI_WC_PaintedBlock_opt[2]=(CheckBox) findViewById(R.id.BI_WC_PaintedBlock_opt3);
     BI_WC_PaintedBlock_opt[3]=(CheckBox) findViewById(R.id.BI_WC_PaintedBlock_opt4);
     BI_WC_PaintedBlock_opt[4]=(CheckBox) findViewById(R.id.BI_WC_PaintedBlock_opt5);
     BI_WC_PaintedBlock_opt[5]=(CheckBox) findViewById(R.id.BI_WC_PaintedBlock_opt6);
     ArrayListenerClickCheck(BI_WC_PaintedBlock_opt);
     
     
     
     BI_WC_list2_otheropt_chk[0]=(CheckBox) findViewById(R.id.BI_WC_list2_otheropt1);
     BI_WC_list2_otheropt_chk[1]=(CheckBox) findViewById(R.id.BI_WC_list2_otheropt2);
     BI_WC_list2_otheropt_chk[2]=(CheckBox) findViewById(R.id.BI_WC_list2_otheropt3);
     BI_WC_list2_otheropt_chk[3]=(CheckBox) findViewById(R.id.BI_WC_list2_otheropt4);
     BI_WC_list2_otheropt_chk[4]=(CheckBox) findViewById(R.id.BI_WC_list2_otheropt5);
     BI_WC_list2_otheropt_chk[5]=(CheckBox) findViewById(R.id.BI_WC_list2_otheropt6);
     ArrayListenerClickCheck(BI_WC_list2_otheropt_chk);
     
     
     BI_WC_list3_otheropt_chk[0]=(CheckBox) findViewById(R.id.BI_WC_list3_otheropt1);
     BI_WC_list3_otheropt_chk[1]=(CheckBox) findViewById(R.id.BI_WC_list3_otheropt2);
     BI_WC_list3_otheropt_chk[2]=(CheckBox) findViewById(R.id.BI_WC_list3_otheropt3);
     BI_WC_list3_otheropt_chk[3]=(CheckBox) findViewById(R.id.BI_WC_list3_otheropt4);
     BI_WC_list3_otheropt_chk[4]=(CheckBox) findViewById(R.id.BI_WC_list3_otheropt5);
     BI_WC_list3_otheropt_chk[5]=(CheckBox) findViewById(R.id.BI_WC_list3_otheropt6);
     ArrayListenerClickCheck(BI_WC_list3_otheropt_chk);
     
     etlist1other = (EditText)this.findViewById(R.id.etlist1other);
     etlist2other = (EditText)this.findViewById(R.id.etlist2other);
     etlist3other = (EditText)this.findViewById(R.id.etlist3other);

     
     
     etframe = (EditText)this.findViewById(R.id.etframe);
     etjoistmasonry = (EditText)this.findViewById(R.id.etjoistmasonry);
     etnoncombustib = (EditText)this.findViewById(R.id.etnoncombustib);
     etmasonrynoncomb = (EditText)this.findViewById(R.id.etmasonrynoncomb);
     etmodifyfiresistance = (EditText)this.findViewById(R.id.etmodifyfiresistance);
     etfiresistance = (EditText)this.findViewById(R.id.etfiresistance);
     
     spin_noofstories = (Spinner) this.findViewById(R.id.spin_noofstories);
    // spin_noofstories.setOnItemSelectedListener(new )
 	spin_yearbuilt = (Spinner) this.findViewById(R.id.spin_yearbuilt);
 	
 	
 	adapter = new ArrayAdapter(BuildingInformation.this,
 			android.R.layout.simple_spinner_item, noofstories);System.out.println("sd");
 	adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
 	spin_noofstories.setAdapter(adapter);System.out.println("cx");
 	
 	 adapter1 = new ArrayAdapter(BuildingInformation.this,
 			android.R.layout.simple_spinner_item, yearbuilt);System.out.println("dd");
 	adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
 	spin_yearbuilt.setAdapter(adapter1);System.out.println("adda");

     
    overallcomment = (EditText)this.findViewById(R.id.comment);
   
     
	/***  RADIOBUTTON FOR GENERAL DATA ENDS HERE ***/
	
	PC_ED_type1_parrant = (LinearLayout)this.findViewById(R.id.permiconf_parrent); 
    PC_ED_type1=(LinearLayout) findViewById(R.id.permconf_type);
	PD_TV_type1 = (TextView) findViewById(R.id.permconf_txt);
	
	AS_ED_type1_parrant = (LinearLayout)this.findViewById(R.id.addistru_parrent);
    AS_ED_type1=(LinearLayout) findViewById(R.id.addistru_type);
	AS_TV_type1 = (TextView) findViewById(R.id.addistru_txt);
	
	LOC_ED_type1_parrant = (LinearLayout)this.findViewById(R.id.locother_parrent);
    LOC_ED_type1=(LinearLayout) findViewById(R.id.locother_type);
	LOC_TV_type1 = (TextView) findViewById(R.id.locother_txt);
	
	OBS3_ED_type1_parrant = (LinearLayout)this.findViewById(R.id.obs_rw3_parrent);
	OBS3_ED_type1=(LinearLayout) findViewById(R.id.obs_rw3_type);
	OBS3_TV_type1 = (TextView) findViewById(R.id.obs_rw3_txt);
	

	OBS2_ED_type1_parrant = (LinearLayout)this.findViewById(R.id.obs_rw2_parrent);
	OBS2_ED_type1=(LinearLayout) findViewById(R.id.obs_rw2_type);
	OBS2_TV_type1 = (TextView) findViewById(R.id.obs_rw2_txt);
	

	OBS5_ED_type1_parrant = (LinearLayout)this.findViewById(R.id.obs_rw5_parrent);
	OBS5_ED_type1=(LinearLayout) findViewById(R.id.obs_rw5_type);
	OBS5_TV_type1 = (TextView) findViewById(R.id.obs_rw5_txt);
	
	OBS4_ED_type1_parrant = (LinearLayout)this.findViewById(R.id.obs_rw4_parrent);
	OBS4_ED_type1=(LinearLayout) findViewById(R.id.obs_rw4_type);
	OBS4_TV_type1 = (TextView) findViewById(R.id.obs_rw4_txt);
	
	 cf.SQ_ED_type1_parrant = (LinearLayout)this.findViewById(R.id.SQ_ED_type1_parrant);
	 cf.SQ_ED_type1=(LinearLayout) findViewById(R.id.SQ_ED_type1);
	 cf.SQ_TV_type1 = (TextView) findViewById(R.id.SQ_TV_type1);
	
	etpermitconfirmed.setOnTouchListener(new Touch_Listener(9));
	etadditionalstructures.setOnTouchListener(new Touch_Listener(10));	
	etlocationcomments.setOnTouchListener(new Touch_Listener(6));
    etobserv3desc.setOnTouchListener(new Touch_Listener(7));
    
  //  etobserv4desc.setOnTouchListener(new Touch_Listener(8));	
    overallcomment.setOnTouchListener(new Touch_Listener(11));
    etobserv2desc.setOnTouchListener(new Touch_Listener(13));
    etobserv5desc.setOnTouchListener(new Touch_Listener(14));
	
    etpermitconfirmed.addTextChangedListener(new BI_textwatcher(9));
	etadditionalstructures.addTextChangedListener(new BI_textwatcher(10));	
	etlocationcomments.addTextChangedListener(new BI_textwatcher(6));
    etobserv3desc.addTextChangedListener(new BI_textwatcher(7));
   // etobserv4desc.addTextChangedListener(new BI_textwatcher(8));	
    overallcomment.addTextChangedListener(new BI_textwatcher(11));
	
	etinsuredother.setOnTouchListener(new Touch_Listener(12));
	etobserv2desc.addTextChangedListener(new BI_textwatcher(13));
	etobserv5desc.addTextChangedListener(new BI_textwatcher(14));
} 
class Touch_Listener implements OnTouchListener
{
	   public int type;
	   Touch_Listener(int type)
		{
			this.type=type;
			
		}
	    @Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
	    
	    	cf.setFocus((EditText)v);
			return false;
	    }
}

class BI_textwatcher implements TextWatcher

{
     public int type;
     BI_textwatcher(int type)
	{
		this.type=type;
	}
	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub
		if(this.type==6)
		{
			cf.showing_limit(s.toString(),LOC_ED_type1_parrant,LOC_ED_type1,LOC_TV_type1,"750"); 
		}
		else if(this.type==7)
		{
			cf.showing_limit(s.toString(),OBS3_ED_type1_parrant,OBS3_ED_type1,OBS3_TV_type1,"750");
		}
		else if(this.type==8)
		{
			cf.showing_limit(s.toString(),OBS4_ED_type1_parrant,OBS4_ED_type1,OBS4_TV_type1,"750");
		}
		else if(this.type==9)
		{
			cf.showing_limit(s.toString(),PC_ED_type1_parrant,PC_ED_type1,PD_TV_type1,"150");
		}
		else if(this.type==10)
		{
			cf.showing_limit(s.toString(),AS_ED_type1_parrant,AS_ED_type1,AS_TV_type1,"150");
		}
		else if(this.type==11)
		{
			cf.showing_limit(s.toString(),cf.SQ_ED_type1_parrant,cf.SQ_ED_type1,cf.SQ_TV_type1,"750");
		}
		else if(this.type==13)
		{
			cf.showing_limit(s.toString(),OBS2_ED_type1_parrant,OBS2_ED_type1,OBS2_TV_type1,"750");
		}
		else if(this.type==14)
		{
			cf.showing_limit(s.toString(),OBS5_ED_type1_parrant,OBS5_ED_type1,OBS5_TV_type1,"750");
		}
		/*else if(this.type==15)
		{
			cf.showing_limit(s.toString(),loc_rw1_parrent,loc_rw1_lin_child,loc_rw1_txt,"199");
		}
		else if(this.type==16)
		{
			cf.showing_limit(s.toString(),loc_rw2_parrent,loc_rw2_lin_child,loc_rw2_txt,"199");
		}
		else if(this.type==17)
		{
			cf.showing_limit(s.toString(),loc_rw3_parrent,loc_rw3_lin_child,loc_rw3_txt,"199");
		}
		else if(this.type==18)
		{
			cf.showing_limit(s.toString(),loc_rw4_parrent,loc_rw4_lin_child,loc_rw4_txt,"199");
		}
*/	
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before,
			int count) {
		// TODO Auto-generated method stub
		
	}
	
}

class B_focus implements OnClickListener
{

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		((EditText) v).setFocusableInTouchMode(true);
		((EditText) v).requestFocus();
		((EditText) v).setCursorVisible(true);
		
	}
	
}


private void ArrayListenerWSC(CheckBox[] BI_WSC_QUES1_chk,EditText etwallstrucoptionopt1) {
	// TODO Auto-generated method stub
	for(int i=0;i<BI_WSC_QUES1_chk.length;i++)
	{
		BI_WSC_QUES1_chk[i].setOnClickListener(new CheckArrayclickerWSC(BI_WSC_QUES1_chk,etwallstrucoptionopt1));
	}
}
private void ArrayListenerClickCheck(CheckBox[] bI_click) {
	// TODO Auto-generated method stub
	for(int i=0;i<bI_click.length;i++)
	{
		bI_click[i].setOnClickListener(new BI_clicker());
	}
}
private void ArrayListenerYear(RadioButton[] BI_YEARBUILT_opt) {
	// TODO Auto-generated method stub
	for(int i=0;i<BI_YEARBUILT_opt.length;i++)
	{
		BI_YEARBUILT_opt[i].setOnClickListener(new RadioArrayclickerYear());
	}
}
private void ArrayListenerNeigh(RadioButton[] BI_click) {
	// TODO Auto-generated method stub
	for(int i=0;i<BI_click.length;i++)
	{
		BI_click[i].setOnClickListener(new RadioArrayclickerNeigh());
	}
}
private void ArrayCommonListener(CheckBox[] BI_click) {
	// TODO Auto-generated method stub
	for(int i=0;i<BI_click.length;i++)
	{
		BI_click[i].setOnClickListener(new BI_clicker());
	}
}

private void ArrayListenerInsured(RadioButton[] BI_INS_txt1_opt) {
	// TODO Auto-generated method stub
	for(int i=0;i<BI_INS_txt1_opt.length;i++)
	{
		
		BI_INS_txt1_opt[i].setOnClickListener(new RadioArrayclickerInsured());
	}
}
private void ArrayListenerOcutype(RadioButton[] BI_OCCU_TYPE_opt) {
	// TODO Auto-generated method stub
	for(int i=0;i<BI_OCCU_TYPE_opt.length;i++)
	{
		BI_OCCU_TYPE_opt[i].setOnClickListener(new RadioArrayclickerOccutype());
	}
}
private void ArrayListenerObservype(RadioButton[] BI_Observ_Type_opt) {
	// TODO Auto-generated method stub
	for(int i=0;i<BI_Observ_Type_opt.length;i++)
	{
		BI_Observ_Type_opt[i].setOnClickListener(new RadioArrayclickerObservType());
	}
}
class  CheckArrayclickerWSC implements OnClickListener
{
	public EditText temped;
	public CheckBox[] tempchkarray;
	

	public CheckArrayclickerWSC(CheckBox[] bI_WSC_QUES1_chk,
			EditText etwallstrucoptionopt1) {
		// TODO Auto-generated constructor stub
		temped=etwallstrucoptionopt1;
		tempchkarray=bI_WSC_QUES1_chk;
	}

	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		s="";
		for(int i=0;i<tempchkarray.length;i++)
		{
			if(tempchkarray[i].isChecked()==true)
			{
				s+=tempchkarray[i].getText().toString()+":";
				
			}
		}
		
			String[] numberSplit = s.split(":") ;
			int ws=0;
			if(numberSplit.length==1)
			{
				if(s.length()==0){wscvalue="";}
				else{
					wscvalue="100";
				}				
				
			}
			else if(numberSplit.length==6)
			{
				wscvalue="1";
			}
			else
			{
				 ws= 100/numberSplit.length;
				 wscvalue=ws+"";
				 System.out.println( "the s value "+numberSplit.length);
				 
			}
			
			/*else if(numberSplit.length==2)
			{
				wscvalue="80";
			}
			else if(numberSplit.length==3)
			{
				wscvalue="60";
			}
			else if(numberSplit.length==4)
			{
				wscvalue="40";
			}
			else if(numberSplit.length==5)
			{
				wscvalue="20";
			}
			else 
			{
				wscvalue="1";
			}*/
		
		 temped.setText(wscvalue);
		
	}
	
}
class  RadioArrayclickerYear implements OnClickListener
{

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		for(int i=0;i<BI_YEARBUILT_opt.length;i++)
		{
			if(v.getId()==BI_YEARBUILT_opt[i].getId())
			{
				BI_YEARBUILT_opt[i].setChecked(true);
				/*if(v.getId()==R.id.BI_YEARBUILT_opt2)
				{
					spin_yearbuilt.setSelection(1);
				}*/
			}
			else
			{
				BI_YEARBUILT_opt[i].setChecked(false);
			}
		}
	}
	
}
class  RadioArrayclickerNeigh implements OnClickListener
{

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		for(int i=0;i<BI_NEIGH_txt_opt.length;i++)
		{
			if(v.getId()==BI_NEIGH_txt_opt[i].getId())
			{
				BI_NEIGH_txt_opt[i].setChecked(true);
				if(BI_NEIGH_txt_opt[i]==BI_NEIGH_txt_opt[3])
				{
					
					etneighbourhoodother.setVisibility(v1.VISIBLE);
					etneighbourhoodother.setFocusableInTouchMode(true);
					etneighbourhoodother.requestFocus();
					etneighbourhoodother.setCursorVisible(true);
					
				}
				else
				{
					etneighbourhoodother.setFocusable(false);
					etneighbourhoodother.setText("");
					etneighbourhoodother.setVisibility(v1.GONE);
				}
				
			}
			else
			{
				BI_NEIGH_txt_opt[i].setChecked(false);
			}
		}
	}
	
}
class  RadioArrayclickerObservType implements OnClickListener
{

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		for(int i=0;i<BI_Observ_Type_opt.length;i++)
		{
			if(v.getId()==BI_Observ_Type_opt[i].getId())
			{
				BI_Observ_Type_opt[i].setChecked(true);
				if(BI_Observ_Type_opt[i]==BI_Observ_Type_opt[4])
				{
					
					etobservtypeother.setVisibility(v1.VISIBLE);
					etobservtypeother.setFocusableInTouchMode(true);
					etobservtypeother.requestFocus();
					etobservtypeother.setCursorVisible(true);
					
				}
				else
				{
					etobservtypeother.setFocusable(false);
					etobservtypeother.setText("");
					etobservtypeother.setVisibility(v1.GONE);
				}
				
				
			}
			else
			{
				BI_Observ_Type_opt[i].setChecked(false);
			}
		}
	}
	
}
class  RadioArrayclickerOccutype implements OnClickListener
{

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		for(int i=0;i<BI_OCCU_TYPE_opt.length;i++)
		{
			if(v.getId()==BI_OCCU_TYPE_opt[i].getId())
			{
				BI_OCCU_TYPE_opt[i].setChecked(true);
				
				if(BI_OCCU_TYPE_opt[i]==BI_OCCU_TYPE_opt[7])
				{
					etbuildoccuptype.setVisibility(v1.VISIBLE);
					etbuildoccuptype.setFocusableInTouchMode(true);
					etbuildoccuptype.requestFocus();
					etbuildoccuptype.setCursorVisible(true);
				}
				else
				{
					etbuildoccuptype.setFocusable(false);
					etbuildoccuptype.setText("");
					etbuildoccuptype.setVisibility(v1.GONE);
				}
				
			}
			else
			{
				BI_OCCU_TYPE_opt[i].setChecked(false);
			}
		}
	}
	
}
class  RadioArrayclickerInsured implements OnClickListener
{

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
	
		
		for(int i=0;i<BI_INS_txt1_opt.length;i++)
		{
	
			
			if(v.getId()==BI_INS_txt1_opt[i].getId())
			{	
				if(BI_INS_txt1_opt[i]==BI_INS_txt1_opt[5])
				{
					
					etinsuredother.setVisibility(v1.VISIBLE);
					etinsuredother.setFocusableInTouchMode(true);
					etinsuredother.requestFocus();
					etinsuredother.setCursorVisible(true);
				}
				else
				{
					etinsuredother.setFocusable(false);
					etinsuredother.setVisibility(v1.GONE);
				}
				BI_INS_txt1_opt[i].setChecked(true);
				
				
			}
			else
			{
				BI_INS_txt1_opt[i].setChecked(false);
			}
		}
	}
	
}

public void savevalidation()
{
	BI_INSUREDIS=cf.getslected_radio(BI_INS_txt1_opt);	
	BI_OTHERINSUREDIS = (BI_INS_txt1_opt[5].isChecked() ? etinsuredother.getText().toString() : ""); 
	BI_BUIL_OCCU = cf.getselected_chk(BI_BUIL_OCCU_chk);
	BI_OCCUPIED= (BI_BUIL_OCCU_chk[0].isChecked())? 1:0;
	BI_VACANT= (BI_BUIL_OCCU_chk[1].isChecked())? 1:0;
	BI_NOTDETERMINED= (BI_BUIL_OCCU_chk[2].isChecked())? 1:0;

	BI_PERMITCONFIRMED = (BI_Permit_Confirmed_yes.isChecked())? "Yes":(BI_Permit_Confirmed_No.isChecked())? "No":(BI_Permit_Confirmed_BSI.isChecked())? "Beyond scope of  Inspection":"";
	BI_ADDITONALSTRU = (BI_additionalstru_yes.isChecked())? "Yes":(BI_additionalstru_no.isChecked())? "No":"";
	BI_BALCONY_RAILPRESENT = (BI_Balc_Rail_pres_yes.isChecked())? "Yes":(BI_Balc_Rail_pres_no.isChecked())? "No":"";
	BI_BALCONYPRESENT = (BI_BALPRES_YES.isChecked())? "Yes":(BI_BALPRES_NO.isChecked())? "No":"";
	
	
	
	BI_LOC1 = (BI_Loc_txt1_yes.isChecked()==true)? "Yes":(BI_Loc_txt1_no.isChecked()==true)? "No":(BI_Loc_txt1_nd.isChecked()==true)? "Not Determined":"Beyond Scope of Inspection";
	BI_LOC2 = (BI_Loc_txt2_yes.isChecked()==true)? "Yes":(BI_Loc_txt2_no.isChecked()==true)? "No":(BI_Loc_txt2_nd.isChecked()==true)? "Not Determined":"Beyond Scope of Inspection";
	BI_LOC3 = (BI_Loc_txt3_yes.isChecked()==true)? "Yes":(BI_Loc_txt3_no.isChecked()==true)? "No":(BI_Loc_txt3_nd.isChecked()==true)? "Not Determined":"Beyond Scope of Inspection";
	BI_LOC4 = (BI_Loc_txt4_yes.isChecked()==true)? "Yes":(BI_Loc_txt4_no.isChecked()==true)? "No":(BI_Loc_txt4_nd.isChecked()==true)? "Not Determined":"Beyond Scope of Inspection";
	
	/*BI_LOC1_DESC=etloc1desc.getText().toString();
	BI_LOC2_DESC=etloc2desc.getText().toString();
	BI_LOC3_DESC=etloc3desc.getText().toString();
	BI_LOC4_DESC=etloc4desc.getText().toString();*/
	
	/*b[28]= (BI_LOC1.trim().equals("No"))? (BI_LOC1_DESC.equals(""))? false:true :true;
	b[29]= (BI_LOC2.trim().equals("No"))? (BI_LOC2_DESC.equals(""))? false:true :true;
	b[30]= (BI_LOC3.trim().equals("No"))? (BI_LOC3_DESC.equals(""))? false:true :true;
	b[31]= (BI_LOC4.trim().equals("No"))? (BI_LOC4_DESC.equals(""))? false:true :true;
	*/
	
	
	BI_OBSERV1 = (BI_OBSERV_txt1_yes.isChecked()==true)? "Yes":(BI_OBSERV_txt1_no.isChecked()==true)? "No":"";
	
	BI_OBSERV2 = (BI_OBSERV_txt2_yes.isChecked()==true)? "Yes":(BI_OBSERV_txt2_no.isChecked()==true)? "No":(BI_OBSERV_txt2_notdeter.isChecked()==true)? "Not Determined":"";
	BI_OBSERV3 = (BI_OBSERV_txt3_yes.isChecked()==true)? "Yes":(BI_OBSERV_txt3_no.isChecked()==true)? "No":(BI_OBSERV_txt3_BSI.isChecked()==true)? "Beyond scope of  Inspection":"";
	BI_OBSERV4 = (BI_OBSERV_txt4_yes.isChecked()==true)? "Yes":(BI_OBSERV_txt4_no.isChecked()==true)? "No":"";
	BI_OBSERV5 = (BI_OBSERV_txt5_yes.isChecked()==true)? "Yes":(BI_OBSERV_txt5_no.isChecked()==true)? "No":(BI_OBSERV_txt5_BSI.isChecked()==true)? "Beyond scope of  Inspection":"";
	
	BI_OBSERV3_DESC = etobserv3desc.getText().toString();
	BI_OBSERV4_DESC =""; //etobserv4desc.getText().toString();
	BI_OBSERV2_DESC =(BI_OBSERV2.trim().equals("Yes"))? etobserv2desc.getText().toString():"";
	BI_OBSERV5_DESC =etobserv5desc.getText().toString();
			
	BI_OCCU_TYPE=cf.getslected_radio(BI_OCCU_TYPE_opt);
	BI_OTHEROCCUTYPE = (BI_OCCU_TYPE_opt[7].isChecked() ? etbuildoccuptype.getText().toString() : ""); 
	BI_OBSERV_TYPE=cf.getslected_radio(BI_Observ_Type_opt);	
	BI_OTHEROBSERVTYPE = (BI_Observ_Type_opt[4].isChecked() ? etobservtypeother.getText().toString() : "");System.out.println("Savtostrin");
	
	
	BI_NEIGH_IS=cf.getslected_radio(BI_NEIGH_txt_opt);	
	BI_OTHERNEIGH = (BI_NEIGH_txt_opt[3].isChecked() ? etneighbourhoodother.getText().toString() : "");System.out.println("Saetnedd");
	
	BI_YEARBUILT=spin_yearbuilt.getSelectedItem().toString();
	BI_YEARBUILT=(BI_YEARBUILT.trim().equals("Other"))? etotheryearbuilt.getText().toString().trim():BI_YEARBUILT;
	BI_YEARBUILT_OPTION=cf.getslected_radio(BI_YEARBUILT_opt);
	
	try
	{
	
	b[0]=(BI_INS_txt1_opt[BI_INS_txt1_opt.length-1].isChecked())? ((etinsuredother.getText().toString().trim().equals(""))? false:true):true;
	b[1]=(BI_OCCU_TYPE_opt[BI_OCCU_TYPE_opt.length-1].isChecked())? ((etbuildoccuptype.getText().toString().trim().equals(""))? false:true):true;
	b[2]=(BI_Observ_Type_opt[BI_Observ_Type_opt.length-1].isChecked())? ((etobservtypeother.getText().toString().trim().equals(""))? false:true):true;
	b[3]=(BI_NEIGH_txt_opt[BI_NEIGH_txt_opt.length-1].isChecked())? ((etneighbourhoodother.getText().toString().trim().equals(""))? false:true):true;
	b[4]=(BI_BALPRES_YES.isChecked())? ((etbalconiespresent.getText().toString().trim().equals(""))? false:true):true;
	if(!etbalconiespresent.getText().toString().trim().equals(""))
	{
		try
		{
		int s=Integer.parseInt(etbalconiespresent.getText().toString().trim());
		if(s>500 || s<0 )
		{
			b[4]=false;
		}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	b[5]=(BI_Permit_Confirmed_yes.isChecked())? (etpermitconfirmed.getText().toString().trim().equals(""))? false:true:true;
	b[6]=(BI_additionalstru_yes.isChecked())? ((etadditionalstructures.getText().toString().trim().equals(""))? false:true):true;
	
	frametxt = etframe.getText().toString();
	joistedmasonrytxt =etjoistmasonry.getText().toString();
	noncombustibletxt =etnoncombustib.getText().toString();
	masonryoncomtxt =etmasonrynoncomb.getText().toString();
	modifiedfireresistivetxt =etmodifyfiresistance.getText().toString();
	fireresistvetxt =etfiresistance.getText().toString();
	
	frameval = (frametxt.equals(""))? 0:Integer.parseInt(frametxt);
	joistedmasonryval = (joistedmasonrytxt.equals(""))? 0:Integer.parseInt(joistedmasonrytxt);
	noncombustibleval = (noncombustibletxt.equals(""))? 0:Integer.parseInt(noncombustibletxt);
	masonryoncomval = (masonryoncomtxt.equals(""))? 0:Integer.parseInt(masonryoncomtxt);
	modifiedfireresistiveval = (modifiedfireresistivetxt.equals(""))? 0:Integer.parseInt(modifiedfireresistivetxt);
	fireresistveval = (fireresistvetxt.equals(""))? 0:Integer.parseInt(fireresistvetxt);
	 
	b[7]=(frameval>100 || (((CheckBox) findViewById(R.id.GCH_B_ISO_Chk1)).isChecked())? (frameval>=1)? false:true : false ) ? false:true;
	b[8]=(joistedmasonryval>100 || (((CheckBox) findViewById(R.id.GCH_B_ISO_Chk2)).isChecked())? (joistedmasonryval>=1)? false:true : false ) ? false:true;
	b[9]=(noncombustibleval>100 || (((CheckBox) findViewById(R.id.GCH_B_ISO_Chk3)).isChecked())? (noncombustibleval>=1)? false:true : false) ? false:true;
	b[10]=(masonryoncomval>100 || (((CheckBox) findViewById(R.id.GCH_B_ISO_Chk4)).isChecked())? (masonryoncomval>=1)? false:true : false ) ? false:true;
	b[11]=(modifiedfireresistiveval>100 || (((CheckBox) findViewById(R.id.GCH_B_ISO_Chk5)).isChecked())? (modifiedfireresistiveval>=1)? false:true : false ) ? false:true;
	b[12]=(fireresistveval>100 || (((CheckBox) findViewById(R.id.GCH_B_ISO_Chk6)).isChecked())? (fireresistveval>=1)? false:true : false ) ? false:true;
	
	b[13]=(BI_BALPRES_NO.isChecked())? true:false;
	b[14]=(BI_OBSERV_txt3_yes.isChecked())? ((etobserv3desc.getText().toString().trim().equals(""))? false:true):true;
	
	b[16]=true;//(BI_OBSERV_txt4_yes.isChecked())? ((etobserv4desc.getText().toString().trim().equals(""))? false:true):true;
	b[17]=(BI_OBSERV_txt4_no.isChecked())? true:false;
	b[26]=(BI_OBSERV_txt2_yes.isChecked())? ((etobserv2desc.getText().toString().trim().equals(""))? false:true):true;
	b[27]=(BI_OBSERV_txt5_yes.isChecked())?(etobserv5desc.getText().toString().trim().equals(""))? false:true:true;
	ws1 = (etwallstrucoptionopt1.getText().toString().equals(""))? 0:Integer.parseInt(etwallstrucoptionopt1.getText().toString());
	ws2 = (etwallstrucoptionopt2.getText().toString().equals(""))? 0:Integer.parseInt(etwallstrucoptionopt2.getText().toString());
	ws3 = (etwallstrucoptionopt3.getText().toString().equals(""))? 0:Integer.parseInt(etwallstrucoptionopt3.getText().toString());
	ws4 = (etwallstrucoptionopt4.getText().toString().equals(""))? 0:Integer.parseInt(etwallstrucoptionopt4.getText().toString());
	ws5 = (etwallstrucoptionopt5.getText().toString().equals(""))? 0:Integer.parseInt(etwallstrucoptionopt5.getText().toString());
	ws6 = (etwallstrucoptionopt6.getText().toString().equals(""))? 0:Integer.parseInt(etwallstrucoptionopt6.getText().toString());
	ws7 = (etwallstrucoptionopt7.getText().toString().equals(""))? 0:Integer.parseInt(etwallstrucoptionopt7.getText().toString());
	
	
	iso1 = (etframe.getText().toString().equals(""))? 0:Integer.parseInt(etframe.getText().toString());
	iso2 = (etjoistmasonry.getText().toString().equals(""))? 0:Integer.parseInt(etjoistmasonry.getText().toString());
	iso3 = (etnoncombustib.getText().toString().equals(""))? 0:Integer.parseInt(etnoncombustib.getText().toString());
	iso4 = (etmasonrynoncomb.getText().toString().equals(""))? 0:Integer.parseInt(etmasonrynoncomb.getText().toString());
	iso5 = (etmodifyfiresistance.getText().toString().equals(""))? 0:Integer.parseInt(etmodifyfiresistance.getText().toString());
	iso6 = (etfiresistance.getText().toString().equals(""))? 0:Integer.parseInt(etfiresistance.getText().toString());

	
	
	}catch(Exception e)
	{
		
		System.out.println("eee "+e.getMessage());
	}
	
	BI_WC_ALUMISLIDING=cf.getselected_chk(BI_WC_ALSlid_opt);	
	BI_WC_VLUMISLIDING=cf.getselected_chk(BI_WC_VLSlid_opt);
	BI_WC_WLUMISLIDING=cf.getselected_chk(BI_WC_WLSlid_opt);
	BI_WC_CEMFIBSLIDING=cf.getselected_chk(BI_WC_CEMFIBER_opt);
	BI_WC_list1_otheropt=cf.getselected_chk(BI_WC_list1_otheropt_chk);
	
	BI_WC_Stucco=cf.getselected_chk(BI_WC_Stucc_opt);
	BL_WC_BVeneer=cf.getselected_chk(BI_WC_BrickVeneer_opt);
	BI_WC_Paintedblock=cf.getselected_chk(BI_WC_PaintedBlock_opt);
	BI_WC_list2_otheropt=cf.getselected_chk(BI_WC_list2_otheropt_chk);
	BI_WC_list3_otheropt=cf.getselected_chk(BI_WC_list3_otheropt_chk);
	
	BL_CONC_UNREIN = cf.getselected_chk(BI_WSC_QUES1_chk);
	
	BI_CONC_REIN = cf.getselected_chk(BI_WSC_QUES2_chk);
	BI_SOLID_CONC = cf.getselected_chk(BI_WSC_QUES3_chk);
	BI_WFRAME = cf.getselected_chk(BI_WSC_QUES4_chk);
	BI_REIN_MASONRY = cf.getselected_chk(BI_WSC_QUES5_chk);
	BI_UNREIN_MASONRY = cf.getselected_chk(BI_WSC_QUES6_chk);
	BI_OTHER_WS = cf.getselected_chk(BI_WSC_QUES7_chk);
	
	
	b[18]=((ws1>100 || ws1<1 ) && !BL_CONC_UNREIN.trim().equals("")) ? false:true;
	b[19]=((ws2>100 || ws2<1) &&  !BI_CONC_REIN.trim().equals("")) ? false:true;
	b[20]=((ws3>100 || ws3<1) && !BI_SOLID_CONC.trim().equals("")) ? false:true;
	b[21]=((ws4>100 || ws4<1) &&!BI_WFRAME.trim().equals("")) ? false:true;
	b[22]=((ws5>100 || ws5<1) &&!BI_REIN_MASONRY.trim().equals("")) ? false:true;
	b[23]=((ws6>100 || ws6<1) &&!BI_UNREIN_MASONRY.trim().equals(""))? false:true;
	b[24]=((ws7>100 || ws7<1) &&!BI_OTHER_WS.trim().equals(""))? false:true;
	BI_OTHER_WSTITLE =((etwallstrucothertxt.getText().toString().trim().equals(""))? "":etwallstrucothertxt.getText().toString());
	BI_OTHER_WCLADTITLE1 = ((etlist1other.getText().toString().trim().equals(""))? "": etlist1other.getText().toString());
	BI_OTHER_WCLADTITLE2=  ((etlist2other.getText().toString().trim().equals(""))? "": etlist2other.getText().toString());
	BI_OTHER_WCLADTITLE3=  ((etlist3other.getText().toString().trim().equals(""))? "": etlist3other.getText().toString());
		
	
	BI_OCCU_PERCENT = etbuild_occup1.getText().toString(); 
	BI_VACANT_PERCENT = etbuild_occup2.getText().toString(); 
	wc[0]=(!BI_WC_list1_otheropt.equals(""))? ((etlist1other.getText().toString().trim().equals(""))? false:true):true;
	wc[1]=(!BI_WC_list2_otheropt.equals(""))? ((etlist2other.getText().toString().trim().equals(""))? false:true):true;
	wc[2]=(!BI_WC_list3_otheropt.equals(""))? ((etlist3other.getText().toString().trim().equals(""))? false:true):true;
	boolean builoccu[]=new boolean[7];
	
	try
	{
		builoccu[0]=(BI_BUIL_OCCU_chk[0].isChecked())? ((etbuild_occup1.getText().toString().trim().equals(""))? false:true):true;
		builoccu[1]=(BI_BUIL_OCCU_chk[0].isChecked())? ((Integer.parseInt(BI_OCCU_PERCENT)==0)? false:true):true;
		builoccu[2]=(BI_BUIL_OCCU_chk[0].isChecked())? ((Integer.parseInt(BI_OCCU_PERCENT)>100)? false:true):true;	
		builoccu[3]=(BI_BUIL_OCCU_chk[1].isChecked())? ((etbuild_occup2.getText().toString().trim().equals(""))? false:true):true;
		builoccu[4]=(BI_BUIL_OCCU_chk[1].isChecked())? ((Integer.parseInt(BI_VACANT_PERCENT)==0)? false:true):true;
		builoccu[5]=(BI_BUIL_OCCU_chk[1].isChecked())? ((Integer.parseInt(BI_VACANT_PERCENT)>100)? false:true):true;
		if(!BI_VACANT_PERCENT.equals("") && !BI_OCCU_PERCENT.equals("") )
		{
		builoccu[6]=((Integer.parseInt(BI_VACANT_PERCENT)+(Integer.parseInt(BI_OCCU_PERCENT)))==100)?true:false;
		}
		else
		{
			builoccu[6]=true;
		}
	}
	catch(Exception e)
	{
		
	}
	try
	{
	
	if(!BI_INSUREDIS.equals("") && b[0]==true)
	{
		if("".equals(BI_BUIL_OCCU))
		{
			cf.ShowToast("Please select Building Occupancy", 0);
		}
		else
		{
			if(builoccu[0] && builoccu[1] && builoccu[2] && builoccu[3] && builoccu[4] && builoccu[5] && builoccu[6] )
			{
				
				if(!BI_OCCU_TYPE.equals("") && b[1]==true)
				{
					
					if(!BI_OBSERV_TYPE.equals("") && b[2]==true)
					{
						
						if(!BI_NEIGH_IS.equals("") && b[3]==true)
						{
							
						try
							{
								buildingsize = etbuildingsize.getText().toString();
								b[25]=(Integer.parseInt(buildingsize)>399 && Integer.parseInt(buildingsize)<1000000)? true:false;
								
								}catch(Exception e)
							{
							}
							if(!buildingsize.trim().equals("")) 
							{
								if(b[25])
								{
								
								if(!ntories.equals("Select"))
								{
									
									if(!BI_YEARBUILT.equals("Select")&& !BI_YEARBUILT.equals("") )
									{
										if(!"".equals(BI_YEARBUILT_OPTION))
										{
											if(BI_BALPRES_YES.isChecked() || BI_BALPRES_NO.isChecked())
											{
												if(b[4])
												{
													if(BI_Permit_Confirmed_yes.isChecked() || BI_Permit_Confirmed_No.isChecked()|| BI_Permit_Confirmed_BSI.isChecked())
													{
														if(b[5])
														{
															if((BI_Balc_Rail_pres_yes.isChecked() || BI_Balc_Rail_pres_no.isChecked()) || b[13]==true)
															{
																
																if(BI_additionalstru_yes.isChecked() || BI_additionalstru_no.isChecked())
																{
																	
																	if(b[6])
																	{
																		if(b[14])
																		{
																			
																				if(b[16]==true){
																					if(b[26]==true && b[27]==true )
																					{
																								if(cb_ws.isChecked()==false && ISwallstructurepresent==0 && "".equals(etwallstrucoptionopt1.getText().toString().trim()) && "".equals(etwallstrucoptionopt2.getText().toString().trim())  && "".equals(etwallstrucoptionopt3.getText().toString().trim())
																										 && "".equals(etwallstrucoptionopt4.getText().toString().trim()) && "".equals(etwallstrucoptionopt5.getText().toString().trim()) && "".equals(etwallstrucoptionopt6.getText().toString().trim()) && "".equals(etwallstrucoptionopt7.getText().toString().trim()))
																								{
																										cf.ShowToast("Please enter atleast one value under Wall Structure - Construction.", 0);
																								}
																								else
																								{
																									
																									if(!("".equals(etwallstrucoptionopt7.getText().toString().trim())) && ISwallstructurepresent==0)
																									{
																										if(etwallstrucothertxt.getText().toString().trim().equals("") && ISwallstructurepresent==0)
																										{
																											cf.ShowToast("Please enter title for other under Wall Structure.", 0);
																											etwallstrucothertxt.requestFocus();
																											etwallstrucothertxt.setCursorVisible(true);
																										}
																										else
																										{
																											wscheck();
																											
																										}
																									}
																									else
																									{
																										wscheck();
																										
																									}
																								}
																					}
																					else
																					{
																						if(b[26]==false)
																						{
																							cf.ShowToast("Please enter comments for Alterations / Remodeling noted since original construction in Observation section ", 0);
																						}else if(b[27]==false)
																						{
																							cf.ShowToast("Please enter comments for Permit information for alterations confirmed in Observation section. ", 0);
																						}
																					}
																		
																		}
																			else
																			{
																				cf.ShowToast("Please enter comments for Visible non-standard / home made / do it yourself alterations. in Observation section", 0);
																			}
																				
																		}
																	
																	else
																	{
																		cf.ShowToast("Please enter comments for Professional construction practices appears present. in Observation section", 0);
																	}	
																	}
																	else
																	{
																		cf.ShowToast("Please select  comments under Additional Structures", 0);
																	}
																	
																}
																else
																{
																	cf.ShowToast("Please select Additional Structures", 0);
																}
																
															}
															else
															{
																cf.ShowToast("Please select Balcony's Railing Present option", 0);	
															}
														}
														else
														{
															cf.ShowToast("Please Enter the Permit Confirmed Comments ", 0);	
														}
													}
													else
													{
														cf.ShowToast("Please select Permit Confirmed", 0);
													}
												}
												else
												{
													cf.ShowToast("Number of Balcony�s Present should not be greater than 500", 0);
												}
											}
											else
											{
												cf.ShowToast("Please select Balconies Present", 0);
											}
										} 
										else
										{
											cf.ShowToast("Please select Year Built Option", 0);
										}
									}
									else
									{
										cf.ShowToast("Please select Year Built Year", 0);
									}
									
									
								}
								else
								{
									cf.ShowToast("Please select Number of Stories", 0);
								}
							}
							else
							{
								cf.ShowToast("Please enter Building Size greater than 399 and less than 1,000,000", 0);
								
							}
							}
							else
							{
								cf.ShowToast("Please enter Building Size", 0);
								
							}
						}
						else
						{
							if(BI_NEIGH_IS.equals("")){	cf.ShowToast("Please select Neighbourhood", 0);	}
							else {	cf.ShowToast("Please enter other text under Neighbourhood", 0); 
							etneighbourhoodother.requestFocus();
							etneighbourhoodother.setCursorVisible(true);etneighbourhoodother.setEnabled(true);}
						}
					}
					else
					{
						if(BI_OBSERV_TYPE.equals("")){	cf.ShowToast("Please select Observation Type", 0);	}
						else {	cf.ShowToast("Please enter other text under Observation Type", 0); 
						etobservtypeother.requestFocus();
						etobservtypeother.setCursorVisible(true);etobservtypeother.setEnabled(true);}
					}
				}
				else
				{
					if(BI_OCCU_TYPE.equals(""))	{cf.ShowToast("Please select  Occupancy Type", 0);}
					else{cf.ShowToast("Please enter Other Text under  Occupancy Type", 0);
					etbuildoccuptype.requestFocus();
					etbuildoccuptype.setCursorVisible(true);etbuildoccuptype.setEnabled(true);}
				}
			}
			else
			{
				if(builoccu[0]==false) { cf.ShowToast("Please enter Occupied Percentage under Building Occupancy", 0);
				etbuild_occup1.requestFocus();etbuild_occup1.setEnabled(true);etbuild_occup1.setCursorVisible(true);}
				else if(builoccu[1]==false) { cf.ShowToast("Percentage for Building Occupancy Occupied should be greater than 0", 0);
				etbuild_occup1.setText("");etbuild_occup1.requestFocus();etbuild_occup1.setCursorVisible(true);}
				else if(builoccu[2]==false) { cf.ShowToast("Percentage for Building Occupancy Occupied cannot be greater than 100", 0);
				etbuild_occup1.setText("");etbuild_occup1.requestFocus();etbuild_occup1.setCursorVisible(true);}
				else if(builoccu[3]==false) { cf.ShowToast("Please enter Vacant Percentage under Building Occupancy", 0);
				etbuild_occup2.requestFocus();etbuild_occup2.setEnabled(true);etbuild_occup2.setCursorVisible(true);}
				else if(builoccu[4]==false) { cf.ShowToast("Percentage for Building Occupancy Vacant should be greater than 0", 0);
				etbuild_occup2.setText("");etbuild_occup2.requestFocus();etbuild_occup2.setCursorVisible(true);}
				else if(builoccu[5]==false) { cf.ShowToast("Percentage for Building Occupancy Vacant cannot be greater than 100", 0);
				etbuild_occup2.setText("");etbuild_occup2.requestFocus();etbuild_occup2.setCursorVisible(true);}
				else if(builoccu[6]==false) { cf.ShowToast("Total Percentage of Building Occupied and Building Occupancy Vacant should be  100", 0);
			     etbuild_occup2.requestFocus();etbuild_occup2.setCursorVisible(true);}
			}
		}
		
	}
	else
	{
		if(BI_INSUREDIS.equals("")){	cf.ShowToast("Please select Insured Is", 0); }
		else{	cf.ShowToast("Please enter other text under Insured Is", 0);
		etinsuredother.requestFocus();}			
	}
	}
	catch(Exception e){
	
	}
	
}

private void wscheck() {
	// TODO Auto-generated method stub
	if(b[18] && b[19] && b[20] && b[21] && b[22] && b[23] && b[24])
	{
		if(ws1+ws2+ws3+ws4+ws5+ws6+ws7==100 || cb_ws.isChecked())
		{
			wallcladding();
		}
		else
		{
			cf.ShowToast("Percentage of under Wall Structure - Construction total should be 100.", 0);
		}
	}
	else
	{
		if(b[18]==false)
		{
			cf.ShowToast("Percentage for Concrete Block Unreinforce cannot be greater than 100 and Should be grater than 0 under Wall Structure - Construction   . ", 0);
		}
		else if(b[19]==false)
		{
			cf.ShowToast("Percentage for Concrete Block Reinforce cannot be greater than 100 and Should be grater than 0 under Wall Structure - Construction.", 0);
		}
		else if(b[20]==false)
		{
			cf.ShowToast("Percentage for Solid Poured Concrete cannot be greater than 100 and Should be grater than 0 under Wall Structure - Construction.", 0);
		}
		else if(b[21]==false)
		{
			cf.ShowToast("Percentage for Wood/light Metal Frame cannot be greater than 100 and Should be grater than 0 under Wall Structure - Construction.", 0);
		}
		else if(b[22]==false)
		{
			cf.ShowToast("Percentage for Un-reinforced Masonry cannot be greater than 100 and Should be grater than 0 under Wall Structure - Construction.", 0);	
		}
		else if(b[23]==false)
		{
			cf.ShowToast("Percentage for Reinforced Masonry cannot be greater than 100 and Should be grater than 0 under Wall Structure - Construction.", 0);	
		}
		else if(b[24]==false)
		{
			cf.ShowToast("Percentage for Reinforced Masonry cannot be greater than 100 under Wall Structure - Construction.", 0);	
		}
	}
}
private void wallcladding()
{
	
		if(cb_wc.isChecked()==false && ISwallcladdingpresent==0 && BI_WC_ALUMISLIDING.equals("") && BI_WC_VLUMISLIDING.equals("") && BI_WC_WLUMISLIDING.equals("") 
				&& BI_WC_CEMFIBSLIDING.equals("") && BI_WC_Stucco.equals("") && BL_WC_BVeneer.equals("") && BI_WC_Paintedblock.equals("")
				&& BI_WC_list1_otheropt.equals("") && BI_WC_list2_otheropt.equals("") && BI_WC_list3_otheropt.equals(""))
		{
			cf.ShowToast("Please select atleast one value under Wall Cladding", 0);
		}
		else
		{
			if(wc[0] && wc[1] && wc[2])
			{
				isocheck();
			}
			else
			{
				if(wc[0]==false)
				{
					cf.ShowToast("Please enter title for Other Wall Cladding1.", 0);
				}
				else if(wc[1]==false)
				{
					cf.ShowToast("Please enter title for Other Wall Cladding2.", 0);
				}
				else if(wc[2]==false)
				{
					cf.ShowToast("Please enter title for Other Wall Cladding3.", 0);
				}
			}
		
		}
}

private void isocheck() {
	// TODO Auto-generated method stub
	if(cb_iso.isChecked()==false && ISisopresent==0 && "".equals(etframe.getText().toString().trim()) && "".equals(etjoistmasonry.getText().toString().trim())  && "".equals(etnoncombustib.getText().toString().trim())
			 && "".equals(etmasonrynoncomb.getText().toString().trim()) && "".equals(etmodifyfiresistance.getText().toString().trim()) && "".equals(etfiresistance.getText().toString().trim()))
		{
			cf.ShowToast("Please enter atleast one value under ISO Classification \nPercentage of under ISO Classification total should be 100", 0);
		}
		else
		{
			if(ISisopresent==0 && cb_iso.isChecked()==false)
			{
				if(b[7] && b[8] && b[9] && b[10] && b[11] && b[12])		/**i have set the true or based on the selection in the validation page ***/																		
				{
					iso = iso1+iso2+iso3+iso4+iso5+iso6;
					if(iso==100)
					{
						InsertValues();
					}
					else
					{
							cf.ShowToast("Percentage of under ISO Classsification total should be 100.", 0);	
					}
					
				}
				else
				{
					if(b[7]==false)
					{
						cf.ShowToast("Percentage for Frame  under ISO cannot be  empty or greater than 100 or less than 1", 0);
					}
					else if(b[8]==false)
					{
						cf.ShowToast("Percentage for Joisted masonry under ISO cannot be  empty or greater than 100 less than 1", 0);
					}
					else if(b[9]==false)
					{
						cf.ShowToast("Percentage for Non-combustible under ISO cannot be  empty or greater than 100 less than 1", 0);
					}
					else if(b[10]==false)
					{
						cf.ShowToast("Percentage for Non-combustible Masonry under ISO cannot be  empty or  greater than 100 less than 1", 0);
					}
					else if(b[11]==false)
					{
						cf.ShowToast("Percentage for Modify Fire Resistive under ISO cannot be  empty or greater than 100 less than 1", 0);	
					}
					else if(b[12]==false)
					{
						cf.ShowToast("Percentage for Fire Resistive under ISO cannot be  empty or greater than 100 less than 1", 0);	
					}
					
				}
			}
			else
			{
				InsertValues();
			}
			
	}

}

private void setObjectsDeclarations() {

	// TODO Auto-generated method stub
	TextView insuredis = (TextView)findViewById(R.id.tvinsuredislabel);
	insuredis.setText(Html.fromHtml(cf.redcolor+" Insured Is "));
	
	TextView buildingoccupancy = (TextView)findViewById(R.id.tvbuildoccuplabel);
	buildingoccupancy.setText(Html.fromHtml(cf.redcolor+" Building Occupancy  "));
	
	TextView occupancytype = (TextView)findViewById(R.id.occupancytypelabel);
	occupancytype.setText(Html.fromHtml(cf.redcolor+" Occupancy Type  "));
	
	TextView observationtype = (TextView)findViewById(R.id.observationtypelabel);
	observationtype.setText(Html.fromHtml(cf.redcolor+" Observation Type  "));

	
	TextView balconiespresent = (TextView)findViewById(R.id.tvbalconiespresentlabel);
	balconiespresent.setText(Html.fromHtml(cf.redcolor+" Balconies Present  "));
	 
	TextView permitconfirmed = (TextView)findViewById(R.id.tvpermitconfirmedlabel);
	permitconfirmed.setText(Html.fromHtml(cf.redcolor+" Permit Confirmed   "));
	
	TextView balconiesrailing = (TextView)findViewById(R.id.tvbalconiesrailinglabel);
	balconiesrailing.setText(Html.fromHtml(cf.redcolor+" Balconies Railing Present  "));
	
	TextView additionalstructures = (TextView)findViewById(R.id.tvadditionalstructures);
	
	additionalstructures.setText(Html.fromHtml(cf.redcolor+" Additional Structures  "));
	
	
	
	///<string name="demoStr"><Data><![CDATA[ <b>ABC</b> ]]> </Data></string> this may be use
	
}


public void tablelayout_hide() {
	// TODO Auto-generated method stub
	 gentbl.setVisibility(cf.show.GONE);
	    loctbl.setVisibility(cf.show.GONE);
	    obstbl.setVisibility(cf.show.GONE);
	    wstbl.setVisibility(cf.show.GONE);
	    wctbl.setVisibility(cf.show.GONE);
	    isotbl.setVisibility(cf.show.GONE);
		genrw.setBackgroundResource(R.drawable.backrepeatnor);
		locrw.setBackgroundResource(R.drawable.backrepeatnorobs);
		obsrw.setBackgroundResource(R.drawable.backrepeatnor);
		wsrw.setBackgroundResource(R.drawable.backrepeatnorobs);
		wcrw.setBackgroundResource(R.drawable.backrepeatnorobs);
		isorw.setBackgroundResource(R.drawable.backrepeatnorobs);
	
}



private void InsertValues()
{
	
	
	if(!overallcomment.getText().toString().trim().equals(""))
	{
		if(cb_wc.isChecked()==true){ISwallcladdingpresent=1;}else if(cb_wc.isChecked()==false){ISwallcladdingpresent=0;}
		if(cb_ws.isChecked()==true){ISwallstructurepresent=1;}else if(cb_ws.isChecked()==false){ ISwallstructurepresent =0;}
		if(cb_iso.isChecked()==true){ISisopresent=1;}else if(cb_iso.isChecked()==false){ISisopresent=0;}
		
		
	cf.Create_Table(5);
	Cursor BI_save=null,c2=null;
	
	try
	{
		c2 = cf.gch_db.rawQuery("SELECT * FROM "
				+ cf.policyholder + " WHERE GCH_PH_SRID='" + cf.encode(cf.selectedhomeid)
				+ "' and GCH_PH_InspectorId='" + cf.encode(cf.Insp_id) + "'", null);
		 if(c2.getCount()>0)
		 {
			 c2.moveToFirst();
			 InspectedDate=cf.decode(c2.getString(c2.getColumnIndex("GCH_Schedule_ScheduledDate")));
			 StartTime=cf.decode(c2.getString(c2.getColumnIndex("GCH_Schedule_InspectionStartTime")));
			 FinishTime=cf.decode(c2.getString(c2.getColumnIndex("GCH_Schedule_InspectionEndTime")));
			 nstoriespolicy=c2.getString(c2.getColumnIndex("GCH_PH_NOOFSTORIES"));
			 //yocpolicy=c2.getString(c2.getColumnIndex("GCH_YearBuilt"));
			 yocpolicy=(BI_YEARBUILT.trim().equals("Other"))? etotheryearbuilt.getText().toString().trim():BI_YEARBUILT;
		 }
		
		 BI_save=cf.SelectTablefunction(cf.BIQ_table, " where GCH_BI_Homeid='"+cf.encode(cf.selectedhomeid)+"'");
		 if(BI_save.getCount()>0)
			{
				try
				{
						cf.gch_db.execSQL("UPDATE "+cf.BIQ_table+ " set GCH_BI_Inspectorid='"+cf.encode(cf.Insp_id)+"',GCH_BI_Homeid='"+cf.encode(cf.selectedhomeid)+"',GCH_BI_INSPECTED_DATE='"+cf.encode(InspectedDate)+"'," +
								"GCH_BI_START_TIME='"+StartTime+"',GCH_BI_END_TIME='"+EndTime+"',GCH_FINIGCH_TIME='"+FinishTime+"'," +
								"GCH_BI_NO_STORIES='"+ntories+"',GCH_BI_YOC='"+yocpolicy+"',GCH_BI_INS_CARRIER='"+Ins_Carrier+"'," +
								"GCH_BI_INSURED_IS='"+cf.encode(BI_INSUREDIS)+"',GCH_BI_INS_OTHER='"+cf.encode(BI_OTHERINSUREDIS)+"',GCH_BI_OCCUPIED='"+BI_OCCUPIED+"'," +
								"GCH_BI_OCCUPIEDPER='"+cf.encode(BI_OCCU_PERCENT)+"',GCH_BI_VACANT='"+BI_VACANT+"',GCH_BI_VACANTPER='"+cf.encode(BI_VACANT_PERCENT)+"'," +
								"GCH_BI_NOT_DETERMINED='"+BI_NOTDETERMINED+"',GCH_BI_OCC_TYPE='"+cf.encode(BI_OCCU_TYPE)+"',GCH_BI_OCC_OTHER='"+cf.encode(BI_OTHEROCCUTYPE)+"'," +
								"GCH_BI_OBSERV_TYPE='"+cf.encode(BI_OBSERV_TYPE)+"',GCH_BI_OBSERV_OTHER='"+cf.encode(BI_OTHEROBSERVTYPE)+"',GCH_BI_NEIGH_IS='"+cf.encode(BI_NEIGH_IS)+"',GCH_BI_NEIGH_OTHER='"+cf.encode(BI_OTHERNEIGH)+"'," +
								"GCH_BI_BUIL_SIZE='"+cf.encode(buildingsize)+"',GCH_BI_NSTORIES='"+cf.encode(ntories)+"',GCH_BI_BUILT_YR='"+cf.encode(spin_yearbuilt.getSelectedItem().toString())+"'," +
								"GCH_BI_YR_BUILT_OPTION='"+cf.encode(BI_YEARBUILT_OPTION)+"',GCH_BI_YR_BUILT_OTHER='"+cf.encode(etotheryearbuilt.getText().toString())+"'," +
								"GCH_BI_PERM_CONFIRMED='"+cf.encode(BI_PERMITCONFIRMED)+"',GCH_BI_PERM_CONF_DESC='"+cf.encode(etpermitconfirmed.getText().toString())+"'," +
								"GCH_BI_ADDI_STRU='"+cf.encode(BI_ADDITONALSTRU)+"',GCH_BI_ADDI_STRU_DESC='"+cf.encode(etadditionalstructures.getText().toString())+"',GCH_BI_BALCONY_PRES='"+cf.encode(BI_BALCONYPRESENT)+"'," +
								"GCH_BI_NBALCONY_PRES='"+cf.encode(etbalconiespresent.getText().toString())+"'," +
								"GCH_BI_BALCONY_RAIL_PRES='"+cf.encode(BI_BALCONY_RAILPRESENT)+"',GCH_BI_LOC1='"+cf.encode(BI_LOC1)+"',GCH_BI_LOC1_DESC='"+cf.encode(BI_LOC1_DESC)+"',GCH_BI_LOC2='"+cf.encode(BI_LOC2)+"',GCH_BI_LOC2_DESC='"+cf.encode(BI_LOC2_DESC)+"',GCH_BI_LOC3='"+cf.encode(BI_LOC3)+"',GCH_BI_LOC3_DESC='"+cf.encode(BI_LOC3_DESC)+"'," +
								"GCH_BI_LOC4='"+cf.encode(BI_LOC4)+"',GCH_BI_LOC4_DESC='"+cf.encode(BI_LOC4_DESC)+"',GCH_BI_OTHER_LOC='"+cf.encode(etlocationcomments.getText().toString())+"',GCH_BI_OBSERV1='"+cf.encode(BI_OBSERV1)+"',GCH_BI_OBSERV2='"+cf.encode(BI_OBSERV2)+"',GCH_BI_OBSERV2_DESC='"+cf.encode(BI_OBSERV2_DESC)+"'," +
								"GCH_BI_OBSERV3='"+cf.encode(BI_OBSERV3)+"',GCH_BI_OBSERV3_DESC='"+cf.encode(BI_OBSERV3_DESC)+"',GCH_BI_OBSERV4='"+cf.encode(BI_OBSERV4)+"'," +
								"GCH_BI_OBSERV4_DESC='"+cf.encode(BI_OBSERV4_DESC)+"',GCH_BI_OBSERV5='"+cf.encode(BI_OBSERV5)+"',GCH_BI_OBSERV5_DESC='"+cf.encode(BI_OBSERV5_DESC)+"',GCH_BI_WSC_PRESENT='"+ISwallstructurepresent+"'," +
								"GCH_BI_CONC_UNREIN='"+cf.encode(BL_CONC_UNREIN)+"',GCH_BI_CONC_UNREINPER='"+ws1+"',GCH_BI_CONC_REIN='"+cf.encode(BI_CONC_REIN)+"'," +
								"GCH_BI_CONC_REINPER='"+ws2+"',GCH_BI_SOLID_CONC='"+cf.encode(BI_SOLID_CONC)+"',GCH_BI_SOILD_CONCPER='"+ws3+"',GCH_BI_WFRAME='"+cf.encode(BI_WFRAME)+"',GCH_BI_WFRAMERPER='"+ws4+"'," +
								"GCH_BI_REIN_MASONRY='"+cf.encode(BI_REIN_MASONRY)+"',GCH_BI_REIN_MASONRYPER='"+ws5+"',GCH_BI_UNREIN_MASONRY='"+cf.encode(BI_UNREIN_MASONRY)+"',GCH_BI_UNREIN_MASONRYPER='"+ws6+"'," +
								"GCH_BI_OTHER_WSTITLE='"+cf.encode(BI_OTHER_WSTITLE)+"',GCH_BI_OTHER_WS='"+cf.encode(BI_OTHER_WS)+"',GCH_BI_OTHER_WALLPER='"+ws7+"',GCH_BI_WALLCLADD_PRES='"+ISwallcladdingpresent+"'," +
								"GCH_BI_ALUM_SLIDING='"+cf.encode(BI_WC_ALUMISLIDING)+"',GCH_BI_VINYL_SLIDING='"+cf.encode(BI_WC_VLUMISLIDING)+"',GCH_BI_WOOD_SLIDING='"+cf.encode(BI_WC_WLUMISLIDING)+"',GCH_BI_CEMENT_FIBER='"+cf.encode(BI_WC_CEMFIBSLIDING)+"'," +
								"GCH_BI_STUCCO='"+cf.encode(BI_WC_Stucco)+"',GCH_BI_BRICKVENEER='"+cf.encode(BL_WC_BVeneer)+"',GCH_BI_PAINTEDBLOCK='"+cf.encode(BI_WC_Paintedblock)+"',GCH_BI_OTHER_WCLADTITLE1='"+cf.encode(BI_OTHER_WCLADTITLE1)+"'," +
								"GCH_BI_OTHER_WCLAD1='"+cf.encode(BI_WC_list1_otheropt)+"',GCH_BI_OTHER_WCLADTITLE2='"+cf.encode(BI_OTHER_WCLADTITLE2)+"',GCH_BI_OTHER_WCLAD2='"+cf.encode(BI_WC_list2_otheropt)+"',GCH_BI_OTHER_WCLADTITLE3='"+cf.encode(BI_OTHER_WCLADTITLE3)+"'," +
								"GCH_BI_OTHER_WCLAD3='"+cf.encode(BI_WC_list3_otheropt)+"',GCH_BI_ISO_CLASSPRES='"+ISisopresent+"',GCH_BI_FRAME='"+frameval+"',GCH_BI_JOISTED='"+joistedmasonryval+"'," +
								"GCH_BI_NONCOMBUST='"+noncombustibleval+"',GCH_BI_MASON_NONCOMBUST='"+masonryoncomval+"',GCH_BI_MODIFY_FIRERESIST='"+modifiedfireresistiveval+"'," +
								"GCH_BI_FIRERESIST='"+fireresistveval+"',GCH_BI_BUILD_COMMENTS='"+cf.encode(overallcomment.getText().toString())+"',GCH_BI_CREATEDON='"+cf.datewithtime+"' "+
								"where GCH_BI_Homeid='"+cf.encode(cf.selectedhomeid)+"' ");

						
							cf.gch_db.execSQL("UPDATE " + cf.policyholder + " SET GCH_PH_NOOFSTORIES='"+ntories+"',GCH_YearBuilt='"+BI_YEARBUILT+"' WHERE GCH_PH_SRID ='" + cf.encode(cf.selectedhomeid)+"'");

						cf.ShowToast("Building Information has been updated sucessfully.", 1);
						// move the page to next page // 
						 nextlayout();
						//	startActivity(new Intent(getApplicationContext(),Observation1.class));
						// move the page to next page ends
				}
				catch (Exception E)
				{
					System.out.println("UPDATE EEE "+E.getMessage());
					String strerrorlog="update the Builing question  table not working ";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table update at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			}
			else
			{
					
					try
					{
						System.out.println("issues not herer");
						
						 cf.gch_db.execSQL("INSERT INTO "
									+ cf.BIQ_table
									+ " (GCH_BI_Inspectorid,GCH_BI_Homeid,GCH_BI_INSPECTED_DATE,GCH_BI_START_TIME,GCH_BI_END_TIME,GCH_FINIGCH_TIME,GCH_BI_NO_STORIES,GCH_BI_YOC,GCH_BI_INS_CARRIER,GCH_BI_INSURED_IS,GCH_BI_INS_OTHER,GCH_BI_OCCUPIED,GCH_BI_OCCUPIEDPER,GCH_BI_VACANT,GCH_BI_VACANTPER,GCH_BI_NOT_DETERMINED,GCH_BI_OCC_TYPE,GCH_BI_OCC_OTHER,GCH_BI_OBSERV_TYPE,GCH_BI_OBSERV_OTHER,GCH_BI_NEIGH_IS,GCH_BI_NEIGH_OTHER,GCH_BI_BUIL_SIZE,GCH_BI_NSTORIES,GCH_BI_BUILT_YR,GCH_BI_YR_BUILT_OPTION,GCH_BI_YR_BUILT_OTHER,GCH_BI_PERM_CONFIRMED,GCH_BI_PERM_CONF_DESC,GCH_BI_ADDI_STRU,GCH_BI_ADDI_STRU_DESC,GCH_BI_BALCONY_PRES,GCH_BI_NBALCONY_PRES,GCH_BI_BALCONY_RAIL_PRES,GCH_BI_LOC1,GCH_BI_LOC1_DESC,GCH_BI_LOC2,GCH_BI_LOC2_DESC,GCH_BI_LOC3,GCH_BI_LOC3_DESC,GCH_BI_LOC4,GCH_BI_LOC4_DESC,GCH_BI_OTHER_LOC,GCH_BI_OBSERV1,GCH_BI_OBSERV2,GCH_BI_OBSERV2_DESC,GCH_BI_OBSERV3,GCH_BI_OBSERV3_DESC,GCH_BI_OBSERV4,GCH_BI_OBSERV4_DESC,GCH_BI_OBSERV5,GCH_BI_OBSERV5_DESC,GCH_BI_WSC_PRESENT,GCH_BI_CONC_UNREIN,GCH_BI_CONC_UNREINPER,GCH_BI_CONC_REIN,GCH_BI_CONC_REINPER,GCH_BI_SOLID_CONC,GCH_BI_SOILD_CONCPER,GCH_BI_WFRAME,GCH_BI_WFRAMERPER,GCH_BI_REIN_MASONRY,GCH_BI_REIN_MASONRYPER,GCH_BI_UNREIN_MASONRY,GCH_BI_UNREIN_MASONRYPER,GCH_BI_OTHER_WSTITLE,GCH_BI_OTHER_WS,GCH_BI_OTHER_WALLPER,GCH_BI_WALLCLADD_PRES,GCH_BI_ALUM_SLIDING,GCH_BI_VINYL_SLIDING,GCH_BI_WOOD_SLIDING,GCH_BI_CEMENT_FIBER,GCH_BI_STUCCO,GCH_BI_BRICKVENEER,GCH_BI_PAINTEDBLOCK,GCH_BI_OTHER_WCLADTITLE1,GCH_BI_OTHER_WCLAD1,GCH_BI_OTHER_WCLADTITLE2,GCH_BI_OTHER_WCLAD2,GCH_BI_OTHER_WCLADTITLE3,GCH_BI_OTHER_WCLAD3,GCH_BI_ISO_CLASSPRES,GCH_BI_FRAME,GCH_BI_JOISTED,GCH_BI_NONCOMBUST,GCH_BI_MASON_NONCOMBUST,GCH_BI_MODIFY_FIRERESIST,GCH_BI_FIRERESIST,GCH_BI_BUILD_COMMENTS,GCH_BI_CREATEDON)"
									+ "VALUES ('"+cf.encode(cf.Insp_id)+"','"+cf.encode(cf.selectedhomeid)+"','"+InspectedDate+"','"+StartTime+"','"+EndTime+"','"+FinishTime+"','"+nstoriespolicy+"','"+yocpolicy+"','"+cf.encode(Ins_Carrier)+"','"+cf.encode(BI_INSUREDIS)+"','"+cf.encode(BI_OTHERINSUREDIS)+"','"+BI_OCCUPIED+"','"+cf.encode(BI_OCCU_PERCENT)+"','"+BI_VACANT+"','"+cf.encode(BI_VACANT_PERCENT)+"','"+BI_NOTDETERMINED+"','"+cf.encode(BI_OCCU_TYPE)+"','"+cf.encode(BI_OTHEROCCUTYPE)+"','"+cf.encode(BI_OBSERV_TYPE)+"','"+cf.encode(BI_OTHEROBSERVTYPE)+"','"+cf.encode(BI_NEIGH_IS)+"','"+cf.encode(BI_OTHERNEIGH)+"','"+cf.encode(buildingsize)+"','"+cf.encode(ntories)+"','"+cf.encode(spin_yearbuilt.getSelectedItem().toString())+"','"+cf.encode(BI_YEARBUILT_OPTION)+"','"+cf.encode(etotheryearbuilt.getText().toString())+"','"+cf.encode(BI_PERMITCONFIRMED)+"','"+cf.encode(etpermitconfirmed.getText().toString())+"','"+cf.encode(BI_ADDITONALSTRU )+"','"+cf.encode(etadditionalstructures.getText().toString())+"','"+cf.encode(BI_BALCONYPRESENT)+"','"+cf.encode(etbalconiespresent.getText().toString())+"','"+cf.encode(BI_BALCONY_RAILPRESENT)+"','"+cf.encode(BI_LOC1)+"','"+cf.encode(BI_LOC1_DESC)+"','"+cf.encode(BI_LOC2)+"','"+cf.encode(BI_LOC2_DESC)+"','"+cf.encode(BI_LOC3)+"','"+cf.encode(BI_LOC3_DESC)+"','"+cf.encode(BI_LOC4)+"','"+cf.encode(BI_LOC4_DESC)+"','"+cf.encode(etlocationcomments.getText().toString())+"','"+cf.encode(BI_OBSERV1)+"','"+cf.encode(BI_OBSERV2)+"','"+cf.encode(BI_OBSERV2_DESC)+"','"+cf.encode(BI_OBSERV3)+"','"+cf.encode(BI_OBSERV3_DESC)+"','"+cf.encode(BI_OBSERV4)+"','"+cf.encode(BI_OBSERV4_DESC)+"','"+cf.encode(BI_OBSERV5)+"','"+cf.encode(BI_OBSERV5_DESC)+"','"+ISwallstructurepresent+"','"+cf.encode(BL_CONC_UNREIN)+"','"+ws1+"','"+cf.encode(BI_CONC_REIN)+"','"+ws2+"','"+cf.encode(BI_SOLID_CONC)+"','"+ws3+"','"+cf.encode(BI_WFRAME)+"','"+ws4+"','"+cf.encode(BI_REIN_MASONRY)+"','"+ws5+"','"+cf.encode(BI_UNREIN_MASONRY)+"','"+ws6+"','"+cf.encode(BI_OTHER_WSTITLE)+"','"+cf.encode(BI_OTHER_WS)+"','"+ws7+"','"+ISwallcladdingpresent+"','"+cf.encode(BI_WC_ALUMISLIDING)+"','"+cf.encode(BI_WC_VLUMISLIDING)+"','"+cf.encode(BI_WC_WLUMISLIDING)+"','"+cf.encode(BI_WC_CEMFIBSLIDING)+"','"+cf.encode(BI_WC_Stucco)+"','"+cf.encode(BL_WC_BVeneer)+"','"+cf.encode(BI_WC_Paintedblock)+"','"+cf.encode(BI_OTHER_WCLADTITLE1)+"','"+cf.encode(BI_WC_list1_otheropt)+"','"+cf.encode(BI_OTHER_WCLADTITLE2)+"','"+cf.encode(BI_WC_list2_otheropt)+"','"+cf.encode(BI_OTHER_WCLADTITLE3)+"','"+cf.encode(BI_WC_list3_otheropt)+"','"+ISisopresent+"','"+frameval+"','"+joistedmasonryval+"','"+noncombustibleval+"','"+masonryoncomval+"','"+modifiedfireresistiveval+"','"+fireresistveval+"','"+cf.encode(overallcomment.getText().toString())+"','"+cf.datewithtime+"')");
						
							cf.gch_db.execSQL("UPDATE " + cf.policyholder + " SET GCH_PH_NOOFSTORIES='"+ntories+"',GCH_YearBuilt='"+yocpolicy+"' WHERE GCH_PH_SRID ='" + cf.encode(cf.selectedhomeid)+"'");
							 cf.ShowToast("Building Information has been saved sucessfully.", 1);
							// move the page to next page // 
							nextlayout();
								//startActivity(new Intent(getApplicationContext(),Observation1.class));
							// move the page to next page ends
							
					}
					catch (Exception E)
					{
						System.out.println("EEE "+E.getMessage());
						String strerrorlog="Insert the Building  question  table not working ";
						cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
					}
					
				
			}
	}
	catch (Exception E)
	{
		String strerrorlog="Selection of the Building information   table not working ";
		cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Select tabele at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
	}
	}
	else
	{
			cf.ShowToast("Please enter the Overall Comments", 0);	
	}
} 
private void nextlayout() {
	// TODO Auto-generated method stub
	Intent intimg = new Intent(BuildingInformation.this,
			GeneralHazards.class);
	cf.putExtras(intimg);;
	startActivity(intimg);
	
	
}
public void clicker(View v)
{
	
	switch(v.getId())
	{
	
	case R.id.save:
		 savevalidation();
		  break;

	  case R.id.hme:
		  cf.gohome();
		  break;

	case R.id.BI_GEN_HEADER: /**  CLCIKING GEERAL DATA HEADER **/
		  tablelayout_hide();gentbl.setVisibility(visibility);
		  genrw.setBackgroundResource(R.drawable.subbackrepeatnor);
		  ((TableRow) v).requestFocus();
		break;
	case R.id.BI_Location_Header: /**  CLCIKING LOCATION HEADER **/
		
		//etlocationcomments.setFocusableInTouchMode(true);
		//etlocationcomments.requestFocus();
		//etlocationcomments.setCursorVisible(true);
		 tablelayout_hide();loctbl.setVisibility(visibility);
		 locrw.setBackgroundResource(R.drawable.subbackrepeatnor);
		
		 ((TableRow) v).requestFocus();
		break;
	case R.id.BI_Observation_Header: /**  CLCIKING OBSERVATION HEADER **/
		
		
		//etobserv3desc.setFocusableInTouchMode(true);
		//etobserv3desc.requestFocus();
		//etobserv3desc.setCursorVisible(true);
		
		 tablelayout_hide();obstbl.setVisibility(View.VISIBLE);
		 obsrw.setBackgroundResource(R.drawable.subbackrepeatnor);
		 ((TableRow) v).requestFocus();
		break;

	case R.id.BI_WS_Header: /**  CLCIKING WS HEADER **/

		if(!cb_ws.isChecked()){
			tablelayout_hide();wstbl.setVisibility(View.VISIBLE);
			 wsrw.setBackgroundResource(R.drawable.subbackrepeatnor);
			 System.out.println("its clicked perfectly");
			 ((ScrollView) findViewById(R.id.scr)).scrollTo(0, 100);
		}
		else{
			/*etwallstrucoptionopt1.setFocusableInTouchMode(true);
			etwallstrucoptionopt1.requestFocus();
			etwallstrucoptionopt1.setCursorVisible(true);
			
				wstbl.setVisibility(show.GONE);*/
		/* */}
		
		break;
	case R.id.BI_WC_Header: /**  CLCIKING WC HEADER **/
		if(!cb_wc.isChecked()){
			tablelayout_hide();wctbl.setVisibility(View.VISIBLE);
			 wcrw.setBackgroundResource(R.drawable.subbackrepeatnor);
			 ((ScrollView) findViewById(R.id.scr)).scrollTo(0, 200);
		}
		else{
			/*etlist1other.setFocusableInTouchMode(true);
			etlist1other.requestFocus();
			etlist1other.setCursorVisible(true);
			
			
		
				wctbl.setVisibility(show.GONE);*/
		 }
		
		break;
	case R.id.BI_ISO_Header: /**  CLCIKING ISO HEADER **/
		
		
		if(!cb_iso.isChecked()){
				
			 tablelayout_hide();isotbl.setVisibility(View.VISIBLE);
			 isorw.setBackgroundResource(R.drawable.subbackrepeatnor);
			 ((ScrollView) findViewById(R.id.scr)).scrollTo(0, 300);
		}
		else{
			/*etframe.setFocusableInTouchMode(true);
			etframe.requestFocus();
			etframe.setCursorVisible(true);
		
				isotbl.setVisibility(show.GONE);*/
		}
		
		break;
	case R.id.ws_chkbx: /** CHECKING WALL STRUCTURE CHECKBOX **/
		
		if(cb_ws.isChecked())
		{
			ISwallstructurepresent=1;
			 tablelayout_hide();gentbl.setVisibility(visibility);
			 genrw.setBackgroundResource(R.drawable.subbackrepeatnor);
			 wallstructureclear();
			 
			 
			 
		}
		else
		{
			ISwallstructurepresent=0;
			 tablelayout_hide();wstbl.setVisibility(visibility);
			 wsrw.setBackgroundResource(R.drawable.subbackrepeatnor);
			 
		}
		//((RelativeLayout) findViewById(R.id.LinearLayout01)).requestFocus();
		break;
	case R.id.wc_chkbx: /** CHECKING WALL CLADDING CHECKBOX **/
		if(cb_wc.isChecked())
		{
			ISwallcladdingpresent=1;
			 tablelayout_hide();gentbl.setVisibility(visibility);
			 genrw.setBackgroundResource(R.drawable.subbackrepeatnor);
			 wallcladdingclear();
		}
		else
		{
			ISwallcladdingpresent=0;
			 tablelayout_hide();wctbl.setVisibility(visibility);
			 wcrw.setBackgroundResource(R.drawable.subbackrepeatnor);
		}
		break;
	
	case R.id.iso_chkbx:
		if(cb_iso.isChecked())
		{
			ISisopresent=1;
			 tablelayout_hide();gentbl.setVisibility(visibility);
			 genrw.setBackgroundResource(R.drawable.subbackrepeatnor);
			 isoclear();
		}
		else
		{
			ISisopresent=0;
			 tablelayout_hide();isotbl.setVisibility(visibility);
			 isorw.setBackgroundResource(R.drawable.subbackrepeatnor);
		}
		break;
	case R.id.GCH_B_ISO_Chk1:
		if(((CheckBox)v).isChecked())
		{
			etframe.setEnabled(true);
		}
		else
		{
			etframe.setText("");
			etframe.setEnabled(false);
		}
		break;
	case R.id.GCH_B_ISO_Chk2:
		if(((CheckBox)v).isChecked())
		{
			etjoistmasonry.setEnabled(true);
		}
		else
		{
			etjoistmasonry.setText("");
			etjoistmasonry.setEnabled(false);
		}
		break;
	case R.id.GCH_B_ISO_Chk3:
		if(((CheckBox)v).isChecked())
		{
			etnoncombustib.setEnabled(true);
		}
		else
		{
			etnoncombustib.setText("");
			etnoncombustib.setEnabled(false);
		}
		break;
	case R.id.GCH_B_ISO_Chk4:
		if(((CheckBox)v).isChecked())
		{
			etmasonrynoncomb.setEnabled(true);
		}
		else
		{
			etmasonrynoncomb.setText("");
			etmasonrynoncomb.setEnabled(false);
		}
		break;
	case R.id.GCH_B_ISO_Chk5:
		if(((CheckBox)v).isChecked())
		{
			etmodifyfiresistance.setEnabled(true);
		}
		else
		{
			etmodifyfiresistance.setText("");
			etmodifyfiresistance.setEnabled(false);
		}
		break;
	case R.id.GCH_B_ISO_Chk6:
		if(((CheckBox)v).isChecked())
		{
			etfiresistance.setEnabled(true);
		}
		else
		{
			etfiresistance.setText("");
			etfiresistance.setEnabled(false);
		}
		break;
	}
	
}
private void wallstructureclear()
{

	BL_CONC_UNREIN="";GCH_BI_CONC_REIN="";BI_SOLID_CONC="";BI_WFRAME="";BI_REIN_MASONRY="";BI_UNREIN_MASONRY="";BI_OTHER_WSTITLE="";BI_OTHER_WS="";
	ws1=0;ws2=0;ws3=0;ws4=0;ws5=0;ws6=0;ws7=0;
	for (int i=0;i<BI_WSC_QUES1_chk.length;i++)
	{
		BI_WSC_QUES1_chk[i].setChecked(false);
		BI_WSC_QUES2_chk[i].setChecked(false);
		BI_WSC_QUES3_chk[i].setChecked(false);
		BI_WSC_QUES4_chk[i].setChecked(false);
		BI_WSC_QUES5_chk[i].setChecked(false);
		BI_WSC_QUES6_chk[i].setChecked(false);
		BI_WSC_QUES7_chk[i].setChecked(false);
	}
	/*BI_WSC_QUES1_chk[0].setChecked(false);BI_WSC_QUES1_chk[1].setChecked(false);BI_WSC_QUES1_chk[2].setChecked(false);BI_WSC_QUES1_chk[3].setChecked(false);BI_WSC_QUES1_chk[4].setChecked(false);BI_WSC_QUES1_chk[5].setChecked(false);
	BI_WSC_QUES2_chk[0].setChecked(false);BI_WSC_QUES2_chk[1].setChecked(false);BI_WSC_QUES2_chk[2].setChecked(false);BI_WSC_QUES2_chk[3].setChecked(false);BI_WSC_QUES2_chk[4].setChecked(false);BI_WSC_QUES2_chk[5].setChecked(false);
	BI_WSC_QUES3_chk[0].setChecked(false);BI_WSC_QUES3_chk[1].setChecked(false);BI_WSC_QUES3_chk[2].setChecked(false);BI_WSC_QUES3_chk[3].setChecked(false);BI_WSC_QUES3_chk[4].setChecked(false);BI_WSC_QUES3_chk[5].setChecked(false);
	BI_WSC_QUES4_chk[0].setChecked(false);BI_WSC_QUES4_chk[1].setChecked(false);BI_WSC_QUES4_chk[2].setChecked(false);BI_WSC_QUES4_chk[3].setChecked(false);BI_WSC_QUES4_chk[4].setChecked(false);BI_WSC_QUES4_chk[5].setChecked(false);
	BI_WSC_QUES5_chk[0].setChecked(false);BI_WSC_QUES5_chk[1].setChecked(false);BI_WSC_QUES5_chk[2].setChecked(false);BI_WSC_QUES5_chk[3].setChecked(false);BI_WSC_QUES5_chk[4].setChecked(false);BI_WSC_QUES5_chk[5].setChecked(false);
	BI_WSC_QUES6_chk[0].setChecked(false);BI_WSC_QUES6_chk[1].setChecked(false);BI_WSC_QUES6_chk[2].setChecked(false);BI_WSC_QUES6_chk[3].setChecked(false);BI_WSC_QUES6_chk[4].setChecked(false);BI_WSC_QUES6_chk[5].setChecked(false);
	BI_WSC_QUES7_chk[0].setChecked(false);BI_WSC_QUES7_chk[1].setChecked(false);BI_WSC_QUES7_chk[2].setChecked(false);BI_WSC_QUES7_chk[3].setChecked(false);BI_WSC_QUES7_chk[4].setChecked(false);BI_WSC_QUES7_chk[5].setChecked(false);
	*/etwallstrucoptionopt1.setText("");etwallstrucoptionopt2.setText("");etwallstrucoptionopt3.setText("");etwallstrucoptionopt4.setText("");
	etwallstrucoptionopt5.setText("");etwallstrucoptionopt6.setText("");etwallstrucoptionopt7.setText("");etwallstrucothertxt.setText("");
}
private void wallcladdingclear()
{
	
	BI_WC_ALUMISLIDING="";BI_WC_VLUMISLIDING="";BI_WC_WLUMISLIDING="";BI_WC_CEMFIBSLIDING="";
	BI_WC_Stucco="";BL_WC_BVeneer="";BI_WC_Paintedblock="";BI_OTHER_WCLADTITLE1="";
	BI_WC_list1_otheropt="";BI_OTHER_WCLADTITLE2="";BI_WC_list2_otheropt="";BI_OTHER_WCLADTITLE3="";
	for (int i=0;i<BI_WSC_QUES1_chk.length;i++)
	{
		BI_WC_ALSlid_opt[i].setChecked(false);
		BI_WC_VLSlid_opt[i].setChecked(false);
		BI_WC_WLSlid_opt[i].setChecked(false);
		BI_WC_CEMFIBER_opt[i].setChecked(false);
		BI_WC_list1_otheropt_chk[i].setChecked(false);
		BI_WC_Stucc_opt[i].setChecked(false);
		BI_WC_BrickVeneer_opt[i].setChecked(false);
		BI_WC_PaintedBlock_opt[i].setChecked(false);
		BI_WC_list2_otheropt_chk[i].setChecked(false);
		BI_WC_list3_otheropt_chk[i].setChecked(false);
		
		
	}
	
	/*BI_WC_ALSlid_opt[0].setChecked(false);BI_WC_ALSlid_opt[1].setChecked(false);BI_WC_ALSlid_opt[2].setChecked(false);BI_WC_ALSlid_opt[3].setChecked(false);BI_WC_ALSlid_opt[4].setChecked(false);BI_WC_ALSlid_opt[5].setChecked(false);
	BI_WC_VLSlid_opt[0].setChecked(false);BI_WC_VLSlid_opt[1].setChecked(false);BI_WC_VLSlid_opt[2].setChecked(false);BI_WC_VLSlid_opt[3].setChecked(false);BI_WC_VLSlid_opt[4].setChecked(false);BI_WC_VLSlid_opt[5].setChecked(false);
	BI_WC_WLSlid_opt[0].setChecked(false);BI_WC_WLSlid_opt[1].setChecked(false);BI_WC_WLSlid_opt[2].setChecked(false);BI_WC_WLSlid_opt[3].setChecked(false);BI_WC_WLSlid_opt[4].setChecked(false);BI_WC_WLSlid_opt[5].setChecked(false);
	BI_WC_CEMFIBER_opt[0].setChecked(false);BI_WC_CEMFIBER_opt[1].setChecked(false);BI_WC_CEMFIBER_opt[2].setChecked(false);BI_WC_CEMFIBER_opt[3].setChecked(false);BI_WC_CEMFIBER_opt[4].setChecked(false);BI_WC_CEMFIBER_opt[5].setChecked(false);
	BI_WC_list1_otheropt_chk[0].setChecked(false);BI_WC_list1_otheropt_chk[1].setChecked(false);BI_WC_list1_otheropt_chk[2].setChecked(false);BI_WC_list1_otheropt_chk[3].setChecked(false);BI_WC_list1_otheropt_chk[4].setChecked(false);BI_WC_list1_otheropt_chk[5].setChecked(false);
	BI_WC_Stucc_opt[0].setChecked(false);BI_WC_Stucc_opt[1].setChecked(false);BI_WC_Stucc_opt[2].setChecked(false);BI_WC_Stucc_opt[3].setChecked(false);BI_WC_Stucc_opt[4].setChecked(false);BI_WC_Stucc_opt[5].setChecked(false);
	BI_WC_BrickVeneer_opt[0].setChecked(false);BI_WC_BrickVeneer_opt[1].setChecked(false);BI_WC_BrickVeneer_opt[2].setChecked(false);BI_WC_BrickVeneer_opt[3].setChecked(false);BI_WC_BrickVeneer_opt[4].setChecked(false);BI_WC_BrickVeneer_opt[5].setChecked(false);
	BI_WC_PaintedBlock_opt[0].setChecked(false);BI_WC_PaintedBlock_opt[1].setChecked(false);BI_WC_PaintedBlock_opt[2].setChecked(false);BI_WC_PaintedBlock_opt[3].setChecked(false);BI_WC_PaintedBlock_opt[4].setChecked(false);BI_WC_PaintedBlock_opt[5].setChecked(false);
	BI_WC_list2_otheropt_chk[0].setChecked(false);BI_WC_list2_otheropt_chk[1].setChecked(false);BI_WC_list2_otheropt_chk[2].setChecked(false);BI_WC_list2_otheropt_chk[3].setChecked(false);BI_WC_list2_otheropt_chk[4].setChecked(false);BI_WC_list2_otheropt_chk[5].setChecked(false);
	BI_WC_list3_otheropt_chk[0].setChecked(false);BI_WC_list3_otheropt_chk[1].setChecked(false);BI_WC_list3_otheropt_chk[2].setChecked(false);BI_WC_list3_otheropt_chk[3].setChecked(false);BI_WC_list3_otheropt_chk[4].setChecked(false);BI_WC_list3_otheropt_chk[5].setChecked(false);*/
	etlist1other.setText("");etlist2other.setText("");etlist3other.setText("");
}
private void isoclear()
{
	frameval=0;joistedmasonryval=0;noncombustibleval=0;masonryoncomval=0;modifiedfireresistiveval=0;fireresistveval=0;
	iso1=0;iso2=0;iso3=0;iso4=0;iso5=0;iso6=0;
	 etframe.setText(""); etjoistmasonry.setText(""); etnoncombustib.setText(""); etmasonrynoncomb.setText(""); 
	 etmodifyfiresistance.setText(""); etfiresistance.setText("");
	 ((CheckBox) findViewById(R.id.GCH_B_ISO_Chk1)).setChecked(false);
	 ((CheckBox) findViewById(R.id.GCH_B_ISO_Chk2)).setChecked(false);
	 ((CheckBox) findViewById(R.id.GCH_B_ISO_Chk3)).setChecked(false);
	 ((CheckBox) findViewById(R.id.GCH_B_ISO_Chk4)).setChecked(false);
	 ((CheckBox) findViewById(R.id.GCH_B_ISO_Chk5)).setChecked(false);
	 ((CheckBox) findViewById(R.id.GCH_B_ISO_Chk6)).setChecked(false);
	 
}
class BI_clicker implements OnClickListener	  {

	  private static final int visibility = 0;

	@Override
	  public void onClick(View v) {
		switch (v.getId()) {
		

		case R.id.BI_BUIL_OCCU_chk1:
			if(BI_BUIL_OCCU_chk[0].isChecked()==true)
			{
				BI_BUIL_OCCU_chk[0].setChecked(true);
				etbuild_occup1.requestFocus();
				etbuild_occup1.setCursorVisible(true);
				etbuild_occup1.setEnabled(true);
			}
			else if(BI_BUIL_OCCU_chk[0].isChecked()==false)
			{
				etbuild_occup1.setCursorVisible(false);
				etbuild_occup1.setEnabled(false);
				etbuild_occup1.setText("");
			}
			if(BI_BUIL_OCCU_chk[2].isChecked()==true)
			{
				BI_BUIL_OCCU_chk[0].setChecked(false);
				BI_BUIL_OCCU_chk[1].setChecked(false);
				etbuild_occup1.setCursorVisible(false);
				etbuild_occup1.setEnabled(false);
				etbuild_occup2.setCursorVisible(false);
				etbuild_occup2.setEnabled(false);
			}
			break;
		case R.id.BI_BUIL_OCCU_chk2:
			if(BI_BUIL_OCCU_chk[1].isChecked()==true){
				BI_BUIL_OCCU_chk[1].setChecked(true);
				etbuild_occup2.requestFocus();
				etbuild_occup2.setCursorVisible(true);
				etbuild_occup2.setEnabled(true);
			}
			
			else if(BI_BUIL_OCCU_chk[1].isChecked()==false){
				etbuild_occup2.setText("");
			etbuild_occup2.setCursorVisible(false);
			etbuild_occup2.setEnabled(false);
			
			}
			if(BI_BUIL_OCCU_chk[2].isChecked()==true)
			{
				BI_BUIL_OCCU_chk[0].setChecked(false);
				BI_BUIL_OCCU_chk[1].setChecked(false);
				etbuild_occup1.setCursorVisible(false);
				etbuild_occup1.setEnabled(false);
				etbuild_occup2.setCursorVisible(false);
				etbuild_occup2.setEnabled(false);
				etbuild_occup2.setText("");
			}
			break;
		case R.id.BI_BUIL_OCCU_chk3:
			
			BI_BUIL_OCCU_chk[0].setChecked(false);
			BI_BUIL_OCCU_chk[1].setChecked(false);
			etbuild_occup1.setText("");
			etbuild_occup1.setEnabled(false);
			etbuild_occup1.setCursorVisible(false);
			etbuild_occup2.setText("");
			etbuild_occup2.setEnabled(false);
			etbuild_occup2.setCursorVisible(false);
			
			break;
		case R.id.BI_BALPRES_YES:
			
			BI_BALPRES_YES.setChecked(true);BI_BALPRES_NO.setChecked(false);
			txtbpresent.setVisibility(visibility);etbalconiespresent.setVisibility(visibility);
			
			etbalconiespresent.setFocusableInTouchMode(true);
			etbalconiespresent.requestFocus();
			etbalconiespresent.setCursorVisible(true);
			
			tblrwrail.setVisibility(visibility);
			break;
		case R.id.BI_BALPRES_NO:
			etbalconiespresent.setFocusable(false);
			BI_BALPRES_YES.setChecked(false);BI_BALPRES_NO.setChecked(true);
			etbalconiespresent.setText("");
			txtbpresent.setVisibility(v1.GONE);etbalconiespresent.setVisibility(v1.GONE);
			tblrwrail.setVisibility(v1.GONE);
			break;
		case R.id.BI_Balc_Rail_pres_yes:
			BI_Balc_Rail_pres_yes.setChecked(true);
			BI_Balc_Rail_pres_no.setChecked(false);
			break;
		case R.id.BI_Balc_Rail_pres_no:
			BI_Balc_Rail_pres_no.setChecked(true);
			BI_Balc_Rail_pres_yes.setChecked(false);
		
			break;
		case R.id.BI_Permit_Confirmed_yes:
			BI_Permit_Confirmed_No.setChecked(false);
			BI_Permit_Confirmed_BSI.setChecked(false);
			etpermitconfirmed.setFocusableInTouchMode(true);
			etpermitconfirmed.requestFocus();
			etpermitconfirmed.setCursorVisible(true);
			etpermitconfirmed.setText("");
			permiconf_lin.setVisibility(View.VISIBLE);
			break;
		case R.id.BI_Permit_Confirmed_No:
			 etpermitconfirmed.setFocusable(false);
			 BI_Permit_Confirmed_yes.setChecked(false);
			 BI_Permit_Confirmed_BSI.setChecked(false);
			 etpermitconfirmed.setText("");
			 permiconf_lin.setVisibility(View.GONE);
			 
			break;
		case R.id.BI_Permit_Confirmed_BSI:
			 etpermitconfirmed.setFocusable(false);
			 BI_Permit_Confirmed_yes.setChecked(false);
			 BI_Permit_Confirmed_No.setChecked(false);
			 BI_Permit_Confirmed_BSI.setChecked(true);
			 etpermitconfirmed.setText("");
			 permiconf_lin.setVisibility(View.GONE);
			break;
		case R.id.BI_additionalstru_yes:
			BI_additionalstru_no.setChecked(false);
			addistru_lin.setVisibility(v.VISIBLE);
			
			etadditionalstructures.setFocusableInTouchMode(true);
			etadditionalstructures.requestFocus();
			etadditionalstructures.setCursorVisible(true);
			break;
			
		case R.id.BI_additionalstru_no:
			etadditionalstructures.setFocusable(false);
			BI_additionalstru_yes.setChecked(false);
			addistru_lin.setVisibility(v.GONE);
			
			break;
		/*case R.id.BI_Loc_txt1_yes:
			BI_Loc_txt1_no.setChecked(false);
		loc_rw1_lin.setVisibility(View.GONE);
		etloc1desc.setText("");
			break;
		case R.id.BI_Loc_txt1_no:
			BI_Loc_txt1_yes.setChecked(false);
			loc_rw1_lin.setVisibility(View.VISIBLE);
			etloc1desc.setText("Beyond scope of inspection");
			etloc1desc.setFocusableInTouchMode(true);
			etloc1desc.requestFocus();
			break;
		case R.id.BI_Loc_txt1_notdetemined:
			loc_rw1_lin.setVisibility(View.GONE);
			etloc1desc.setText("");
			break;
		case R.id.BI_Loc_txt2_yes:
			BI_Loc_txt2_no.setChecked(false);
			loc_rw2_lin.setVisibility(View.GONE);
			etloc2desc.setText("");
			break;
		case R.id.BI_Loc_txt2_no:
			BI_Loc_txt2_yes.setChecked(false);
			BI_Loc_txt2_yes.setChecked(false);
			loc_rw2_lin.setVisibility(View.VISIBLE);
			etloc2desc.setText("Beyond scope of inspection");
			etloc2desc.setFocusableInTouchMode(true);
			etloc2desc.requestFocus();
			break;
		case R.id.BI_Loc_txt2_notdetemined:
			loc_rw2_lin.setVisibility(View.GONE);
			etloc2desc.setText("");
			break;
		case R.id.BI_Loc_txt3_yes:
			BI_Loc_txt3_no.setChecked(false);
			loc_rw3_lin.setVisibility(View.GONE);
			etloc3desc.setText("");
			break;
		case R.id.BI_Loc_txt3_no:
			BI_Loc_txt3_yes.setChecked(false);
			BI_Loc_txt3_yes.setChecked(false);
			loc_rw3_lin.setVisibility(View.VISIBLE);
			etloc3desc.setText("Beyond scope of inspection");
			etloc3desc.setFocusableInTouchMode(true);
			etloc3desc.requestFocus();
			break;
		case R.id.BI_Loc_txt3_notdetemined:
			loc_rw3_lin.setVisibility(View.GONE);
			etloc3desc.setText("");
			break;
		case R.id.BI_Loc_txt4_yes:
			BI_Loc_txt4_no.setChecked(false);
			loc_rw4_lin.setVisibility(View.GONE);
			etloc4desc.setText("");
			break;
		case R.id.BI_Loc_txt4_no:
			BI_Loc_txt4_yes.setChecked(false);
			BI_Loc_txt4_yes.setChecked(false);
			loc_rw4_lin.setVisibility(View.VISIBLE);
			etloc4desc.setText("Beyond scope of inspection");
			etloc4desc.setFocusableInTouchMode(true);
			etloc4desc.requestFocus();
			break;
		case R.id.BI_Loc_txt4_notdetemined:
			loc_rw4_lin.setVisibility(View.GONE);
			etloc4desc.setText("");
			break;*/
			
		case R.id.BI_OBSERV_txt1_yes:
			BI_OBSERV_txt1_yes.setChecked(true);
			BI_OBSERV_txt1_no.setChecked(false);
			break;
			
		case R.id.BI_OBSERV_txt1_no:
			BI_OBSERV_txt1_yes.setChecked(false);
			BI_OBSERV_txt1_no.setChecked(true);
			break;
		case R.id.BI_OBSERV_txt2_yes:
				BI_OBSERV_txt2_yes.setChecked(true);
				obs_rw2lin.setVisibility(v.VISIBLE);
				etobserv2desc.setFocusableInTouchMode(true);
				etobserv2desc.requestFocus();
				etobserv2desc.setCursorVisible(true);
				etobserv2desc.setVisibility(View.VISIBLE);
	     break;
		case R.id.BI_OBSERV_txt2_no:
				BI_OBSERV_txt2_no.setChecked(true);
				obs_rw2lin.setVisibility(v.GONE);
				etobserv2desc.setFocusable(false);
				etobserv2desc.setVisibility(View.GONE);
				etobserv2desc.setText("");
		break;
		case R.id.BI_OBSERV_txt2_notdeter:
				BI_OBSERV_txt2_notdeter.setChecked(true);
				obs_rw2lin.setVisibility(v.GONE);
				etobserv2desc.setFocusable(false);
				etobserv2desc.setVisibility(View.GONE);
				etobserv2desc.setText("");
			
			break;
			
		 case R.id.BI_OBSERV_txt3_yes:
				
				 BI_OBSERV_txt3_yes.setChecked(true);
				 obs_rw3lin.setVisibility(v.VISIBLE);
				 etobserv3desc.setFocusableInTouchMode(true);
				 etobserv3desc.requestFocus();
				 etobserv3desc.setCursorVisible(true);
				 etobserv3desc.setVisibility(View.VISIBLE);
				
			
			break;
		 case R.id.BI_OBSERV_txt3_no:
			 BI_OBSERV_txt3_no.setChecked(true);
				
				obs_rw3lin.setVisibility(v.GONE);
				etobserv3desc.setFocusable(false);
				etobserv3desc.setVisibility(View.GONE);
				etobserv3desc.setText("");
		 break;
		 case R.id.BI_OBSERV_txt3_BSI:
			    BI_OBSERV_txt3_BSI.setChecked(true);
				obs_rw3lin.setVisibility(v.GONE);
				etobserv3desc.setFocusable(false);
				etobserv3desc.setVisibility(View.GONE);
				etobserv3desc.setText("");
			
		 break;

		
		 case R.id.BI_OBSERV_txt4_yes:
					BI_OBSERV_txt4_yes.setChecked(true);
					/*obs_rw4lin.setVisibility(v.VISIBLE);
					etobserv4desc.setFocusableInTouchMode(true);
					etobserv4desc.requestFocus();
					etobserv4desc.setCursorVisible(true);
					etobserv4desc.setVisibility(View.VISIBLE);*/
					
				
				break;
			 case R.id.BI_OBSERV_txt4_no:
				    BI_OBSERV_txt4_no.setChecked(true);
					/*obs_rw4lin.setVisibility(v.GONE);
					etobserv4desc.setFocusable(false);
					etobserv4desc.setVisibility(View.GONE);
					etobserv4desc.setText("");*/
				
			 break;
			 case R.id.BI_OBSERV_txt5_yes:
				 
				    	obs_rw5lin.setVisibility(v.VISIBLE);
				    	
						 etobserv5desc.setText("");
					
			break;
			 case R.id.BI_OBSERV_txt5_no:
						
			    	obs_rw5lin.setVisibility(v.GONE);
			    	
					 etobserv5desc.setText("");
				
			break;
			 case R.id.BI_OBSERV_txt5_BSI:
					
			    	obs_rw5lin.setVisibility(v.GONE);
			    	
					 etobserv5desc.setText("");
				
			break;
			default:
				 
				 break;
		
		}
	}
};
protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	switch (resultCode) {
		case 0:
			break;
		case -1:
			try {

				String[] projection = { MediaStore.Images.Media.DATA };
				Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
						null, null, null);
				int column_index_data = cursor
						.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
				cursor.moveToFirst();
				String capturedImageFilePath = cursor.getString(column_index_data);
				cf.showselectedimage(capturedImageFilePath);
			} catch (Exception e) {
				
			}
			
			break;

	}

}
public boolean onKeyDown(int keyCode, KeyEvent event) {
	// replaces the default 'Back' button action
	if (keyCode == KeyEvent.KEYCODE_BACK) {
		if(cf.strschdate.equals("")){
			cf.ShowToast("Layout navigation without scheduling not allowed. ", 1);
		}else{
			Intent  myintent = new Intent(getApplicationContext(),PolicyHolder.class);
			cf.putExtras(myintent);
			startActivity(myintent);
		}
		return true;
	}
	if (keyCode == KeyEvent.KEYCODE_MENU) {

	}
	return super.onKeyDown(keyCode, event);
}
public void onWindowFocusChanged(boolean hasFocus) {

    super.onWindowFocusChanged(hasFocus);
    if(focus==1)
    {
    	focus=0;
    
    if(permiconf_lin.getVisibility()==View.VISIBLE)
    {
    	etpermitconfirmed.setText(etpermitconfirmed.getText().toString());
    }
    if(addistru_lin.getVisibility()==View.VISIBLE)
    {
    	etadditionalstructures.setText(etadditionalstructures.getText().toString());
    }
    if(obs_rw3lin.getVisibility()==View.VISIBLE)
    {
    	etobserv3desc.setText(etobserv3desc.getText().toString());
    }
   /* if(obs_rw4lin.getVisibility()==View.VISIBLE)
    {
    	etobserv4desc.setText(etobserv4desc.getText().toString());
    }*/
    if(loctbl.getVisibility()==View.VISIBLE)
    {
    	etlocationcomments.setText(etlocationcomments.getText().toString());
    	
    }
    overallcomment.setText(overallcomment.getText().toString());    }
 }         

public void  DisableWS(int cnt,boolean b)
{
	for (int i=cnt;i<BI_WSC_QUES1_chk.length;i++)
	{
		
		/*Reomve the selection**/
		if(!b)
		{
		BI_WC_ALSlid_opt[i].setChecked(false);
		BI_WC_VLSlid_opt[i].setChecked(false);
		BI_WC_WLSlid_opt[i].setChecked(false);
		BI_WC_CEMFIBER_opt[i].setChecked(false);
		BI_WC_list1_otheropt_chk[i].setChecked(false);
		BI_WC_Stucc_opt[i].setChecked(false);
		BI_WC_BrickVeneer_opt[i].setChecked(false);
		BI_WC_PaintedBlock_opt[i].setChecked(false);
		BI_WC_list2_otheropt_chk[i].setChecked(false);
		BI_WC_list3_otheropt_chk[i].setChecked(false);
		
		BI_WSC_QUES1_chk[i].setChecked(false);
		BI_WSC_QUES2_chk[i].setChecked(false);
		BI_WSC_QUES3_chk[i].setChecked(false);
		BI_WSC_QUES4_chk[i].setChecked(false);
		BI_WSC_QUES5_chk[i].setChecked(false);
		BI_WSC_QUES6_chk[i].setChecked(false);
		BI_WSC_QUES7_chk[i].setChecked(false);
		}
		/*Reomve the selection**/
		
		/**Disable all **/
			BI_WC_ALSlid_opt[i].setEnabled(b);
			BI_WC_VLSlid_opt[i].setEnabled(b);
			BI_WC_WLSlid_opt[i].setEnabled(b);
			BI_WC_CEMFIBER_opt[i].setEnabled(b);
			BI_WC_list1_otheropt_chk[i].setEnabled(b);
			BI_WC_Stucc_opt[i].setEnabled(b);
			BI_WC_BrickVeneer_opt[i].setEnabled(b);
			BI_WC_PaintedBlock_opt[i].setEnabled(b);
			BI_WC_list2_otheropt_chk[i].setEnabled(b);
			BI_WC_list3_otheropt_chk[i].setEnabled(b);
			
			BI_WSC_QUES1_chk[i].setEnabled(b);
			BI_WSC_QUES2_chk[i].setEnabled(b);
			BI_WSC_QUES3_chk[i].setEnabled(b);
			BI_WSC_QUES4_chk[i].setEnabled(b);
			BI_WSC_QUES5_chk[i].setEnabled(b);
			BI_WSC_QUES6_chk[i].setEnabled(b);
			BI_WSC_QUES7_chk[i].setEnabled(b);
			
			
			/**Disable all ends **/
			
		
	}
	
}
}