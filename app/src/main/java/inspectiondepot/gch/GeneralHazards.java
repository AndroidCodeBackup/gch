package inspectiondepot.gch;


import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.GetChars;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class GeneralHazards extends Activity {
	private static final int visibility = 0;
	boolean b[] =new boolean[10];
	String GHC_ISPoolPresent,GHC_PoolPresent,GHC_Perimeterpool,GHC_PPFECNCE_VAL,GHC_HOTTUB,GHC_PPS,GHC_HOTTUBCOVER,GHC_POOLSLIDE,GHC_DIVINGBOARD,GHC_PERIPOOL,GHC_POOLENCLOS,GCH_EMPTYPOOLPRESENT,
	GHC_PERIPF,GHC_SELFLATCH,GHC_PROF_INST,GHC_PFDESREPAIR,GHC_VICIOUS,GCH_VICIOUSDESC,GCH_LIVESTOCK,GCH_LIVESTOCKDESC,GCH_OVERHANGING,GCH_TRAMPOLINE,GCH_SKATEBOARD,GCH_BICYCLE,
	GCH_GROUNDPOOL,GCH_TRIPHAZARD,GCH_TRIPHAZARDNOTES,GCH_UNSAFESTAIRWAY,GCH_PORCHANDDESK,GCH_NONSTDCONST,GCH_OUTDOORAPPL,GCH_OPENFOUND,GCH_WOODSINGHLED,
	GCH_EXCESSDEBRIS,GCH_BUISPREMISES,GCH_BUISPREMDESC,GCH_GENDISREPAIR,GCH_PROP_DAMAGE,GCH_STRUPARTIAL,GHC_PERIMETERPOOLFENCE,GCH_OTHERPERI,
	 GCH_INOPERATIVE,GCH_RECDRYWALL,GCH_CHINESEDRYWALL,GCH_CONFDRYWALL,GCH_NONSECURITY,GCH_NONSMOKE,GCH_RECDRYWALL_BSI,GCH_CHINESEDRYWALL_BSI,GCH_CONFDRYWALL_BSI,GCH_NONSECURITY_BSI,GCH_NONSMOKE_BSI,GCH_COMMENTS;
	View v1;
	CheckBox HZ_PERI_opt[]= new CheckBox[7];
	RelativeLayout HZ_general_header,HZ_perimeterpoolfence_header,perimeterpoolfenceheader,summaryheader;
	CheckBox ISPoolPresent,ISPermiterpoolPresent;
	RadioGroup PP_RG_GROUNDLEVEL,PP_RGRP1,PP_RGRP2,PP_RGPPS,PP_RGRP3,PP_RGRP4,PP_RGRP5,PP_RGRP6,PP_RGRP7,PF_RG_POOLPRESENT,PF_RG_SELFLATCHES,PF_RG_PROFINST,PF_RG_POOLFENCE,PF_RG_TRIPHAZ;
	int PP_RG_GROUNDLEVEL_ID,PP_RGRP1_ID,PP_RGRP2_ID,PP_RGPPS_ID,PP_RGRP3_ID,PP_RGRP4_ID,PP_RGRP5_ID,PP_RGRP6_ID,PP_RGRP7_ID,PF_RG_POOLPRESENT_ID,PF_RG_SELFLATCHES_ID,PF_RG_PROFINST_ID,PF_RG_POOLFENCE_ID,PF_RG_TRIPHAZ_ID;
	String HZ_commentsval,PP_RG_GROUNDLEVEL_val,PP_RGRP1_val,PP_RGRP2_val,PP_RGPPS_val,PP_RGRP3_val,PP_RGRP4_val,PP_RGRP5_val,PP_RGRP6_val,PP_RGRP7_val,PF_RG_SELFLATCHES_val,PF_RG_PROFINST_val,PF_RG_POOLFENCE_val,PF_RG_TRIPHAZ_val;
	CommonFunctions cf;
	
	RadioButton HZ_SUMM_VICIOUS_YES,HZ_SUMM_VICIOUS_NO,HZ_SUMM_HORSES_YES,HZ_SUMM_HORSES_NO,HZ_SUMM_BUISPREM_YES,HZ_SUMM_BUISPREM_NO;
	TableRow trvicioustext,trhorsestext,hottubcoveredtr,PP_TR_hottub,trpoolfenceother,trsummbusiprem;
	int ISPoolpresentval=1,Isperimeterpoolfenceval=1;
	EditText ettrpihazards,etvicious,ethorses,etpoolfenceother,etsummbuisprem;
	RadioGroup PP_RG_SUMMHAZ1_Q1,PP_RG_SUMMHAZ1_Q2,PP_RG_SUMMHAZ1_Q3,PP_RG_SUMMHAZ1_Q4,PP_RG_SUMMHAZ1_Q5,PP_RG_SUMMHAZ1_Q6,
	PP_RG_SUMMHAZ2_Q1,PP_RG_SUMMHAZ2_Q2,PP_RG_SUMMHAZ2_Q3,PP_RG_SUMMHAZ2_Q4,PP_RG_SUMMHAZ2_Q5,PP_RG_SUMMHAZ2_Q6,PP_RG_SUMMHAZ2_Q7,
	PP_RG_SUMMHAZ3_Q1,PP_RG_SUMMHAZ3_Q2,PP_RG_SUMMHAZ3_Q3,PP_RG_SUMMHAZ3_Q4,PP_RG_SUMMHAZ3_Q5,PP_RG_SUMMHAZ3_Q6,PP_RG_SUMMHAZ3_Q7,PP_RG_SUMMHAZ3_Q8,PP_RG_SUMMHAZ3_Q9,PP_RG_SUMMHAZ3_Q10;
	
	int PP_RG_SUMMHAZ1_Q1_ID,PP_RG_SUMMHAZ1_Q2_ID,PP_RG_SUMMHAZ1_Q3_ID,PP_RG_SUMMHAZ1_Q4_ID,PP_RG_SUMMHAZ1_Q5_ID,PP_RG_SUMMHAZ1_Q6_ID,
	PP_RG_SUMMHAZ2_Q1_ID,PP_RG_SUMMHAZ2_Q2_ID,PP_RG_SUMMHAZ2_Q3_ID,PP_RG_SUMMHAZ2_Q4_ID,PP_RG_SUMMHAZ2_Q5_ID,PP_RG_SUMMHAZ2_Q6_ID,PP_RG_SUMMHAZ2_Q7_ID,
	PP_RG_SUMMHAZ3_Q1_ID,PP_RG_SUMMHAZ3_Q2_ID,PP_RG_SUMMHAZ3_Q3_ID,PP_RG_SUMMHAZ3_Q4_ID,PP_RG_SUMMHAZ3_Q5_ID,PP_RG_SUMMHAZ3_Q6_ID,PP_RG_SUMMHAZ3_Q7_ID,PP_RG_SUMMHAZ3_Q8_ID,PP_RG_SUMMHAZ3_Q9_ID,PP_RG_SUMMHAZ3_Q10_ID;
	TextView HORSES_TV_type1,BUIS_TV_type1,VICIOUS_TV_type1; 
	String PP_RG_SUMMHAZ1_Q1_val,PP_RG_SUMMHAZ1_Q2_val,PP_RG_SUMMHAZ1_Q3_val,PP_RG_SUMMHAZ1_Q4_val,PP_RG_SUMMHAZ1_Q5_val,PP_RG_SUMMHAZ1_Q6_val,
	PP_RG_SUMMHAZ2_Q1_val,PP_RG_SUMMHAZ2_Q2_val,PP_RG_SUMMHAZ2_Q3_val,PP_RG_SUMMHAZ2_Q4_val,PP_RG_SUMMHAZ2_Q5_val,PP_RG_SUMMHAZ2_Q6_val,PP_RG_SUMMHAZ2_Q7_val,
	PP_RG_SUMMHAZ3_Q1_val,PP_RG_SUMMHAZ3_Q2_val,PP_RG_SUMMHAZ3_Q3_val,PP_RG_SUMMHAZ3_Q4_val,PP_RG_SUMMHAZ3_Q5_val,PP_RG_SUMMHAZ3_Q6_val,PP_RG_SUMMHAZ3_Q7_val,PP_RG_SUMMHAZ3_Q8_val,PP_RG_SUMMHAZ3_Q9_val,PP_RG_SUMMHAZ3_Q10_val;
	String HZ_SUMM_VICICOMM,PF_HZ_OTHERPERI="",HZ_VICIOUSDESCVAL="",HZ_HORSESDESCVAL="",HZ_TRIPHAZARDNOTES="",HZ_BUISNPREM_DESC="";
	CheckBox HZ_ppnotapplicable,HZ_pfnotapplicable;
	LinearLayout HORSES_ED_type1,HORSES_ED_type1_parrant,horses_type,vicious_lin,horses_lin,business_lin,BUIS_ED_type1_parrant,BUIS_ED_type1,VICIOUS_ED_type1_parrant,VICIOUS_ED_type1,poollist1,poollist2,triphazardlin,summaryhazardoptionlist1,summaryhazardoptionlist2,summaryhazardoptionlist3,poolfencelist1,poolfencelist2,generalheader;
	EditText HZ_comments;
	public int focus=0;
	
	/**Beyond the scope of inspection varible starts **/
	/*LinearLayout SH_rw20_lin,SH_rw20_lin_parrent,SH_rw20_lin_chaild,SH_rw21_lin,SH_rw21_lin_parrent,SH_rw21_lin_chaild,SH_rw22_lin,SH_rw22_lin_parrent,SH_rw22_lin_chaild,SH_rw23_lin,SH_rw23_lin_parrent,SH_rw23_lin_chaild,SH_rw24_lin,SH_rw24_lin_parrent,SH_rw24_lin_chaild;
	EditText SH_rw20_ed,SH_rw21_ed,SH_rw22_ed,SH_rw23_ed,SH_rw24_ed;
	TextView SH_rw20_txt,SH_rw21_txt,SH_rw22_txt,SH_rw23_txt,SH_rw24_txt;*/
	/**Beyond the scope of inspection varible Ends **/
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cf = new CommonFunctions(this);
        
        Bundle extras = getIntent().getExtras();
		if (extras != null) {
			cf.selectedhomeid = extras.getString("homeid");
			cf.onlinspectionid = extras.getString("InspectionType");
		    cf.onlstatus = extras.getString("status");
    	}
		
		setContentView(R.layout.hazards);
		cf.getInspectorId();
	    cf.getDeviceDimensions();
	    LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	    mainmenu_layout.setMinimumWidth(cf.wd);
	    TableRow tbrw=(TableRow)findViewById(R.id.row2);
	    tbrw.setMinimumHeight(cf.ht);
		mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 3, 0,cf));
		
		((TextView)findViewById(R.id.PP_txt)).setText(Html.fromHtml(cf.redcolor+" Swimming Pool Present :  "));
		((TextView)findViewById(R.id.PP_HotTub)).setText(Html.fromHtml(cf.redcolor+" Hot Tub Present :  "));
		((TextView)findViewById(R.id.PP_PPS)).setText(Html.fromHtml(cf.redcolor+" Part of Pool Structure :  "));
	    ((TextView)findViewById(R.id.PP_emptyin)).setText(Html.fromHtml(cf.redcolor+" Empty in Ground Pool Present :   "));
	    ((TextView)findViewById(R.id.PF_txt_POOLPRESENT)).setText(Html.fromHtml(cf.redcolor+"  Perimeter Pool Fence :  "));
		((TextView)findViewById(R.id.HZ_Perim_txt_opt1)).setText(Html.fromHtml(cf.redcolor+" Self Latches / Locks to Gate :    "));
		((TextView)findViewById(R.id.HZ_Perim_txt_opt2)).setText(Html.fromHtml(cf.redcolor+" Professionally Installed :    "));
		((TextView)findViewById(R.id.HZ_Perim_txt_opt3)).setText(Html.fromHtml(cf.redcolor+"  Pool Fence in Disrepair :  "));
		((TextView)findViewById(R.id.HZ_Summ_opt1)).setText(Html.fromHtml("Vicious / Exotic Pets"));
		((TextView)findViewById(R.id.HZ_Summ_opt2)).setText(Html.fromHtml("Horses/Livestock On Premises"));
		((TextView)findViewById(R.id.HZ_Summ_opt3)).setText(Html.fromHtml("Over Hanging Limbs to Roof"));
		((TextView)findViewById(R.id.HZ_Summ_opt4)).setText(Html.fromHtml("Trampoline Present"));
		((TextView)findViewById(R.id.HZ_Summ_opt5)).setText(Html.fromHtml("Skateboard Ramp Present"));
		((TextView)findViewById(R.id.HZ_Summ_opt6)).setText(Html.fromHtml("Bicycle Ramp Present"));
		((TextView)findViewById(R.id.HZ_tvtriphazards)).setText(Html.fromHtml("Trip Hazards Noted"));
		((TextView)findViewById(R.id.HZ_Summ2_opt1)).setText(Html.fromHtml("Unsafe Stairway Noted"));
		((TextView)findViewById(R.id.HZ_Summ2_opt2)).setText(Html.fromHtml("Porch/Deck >2' off Grade (or 3 steps)  No Hand Railings"));
		((TextView)findViewById(R.id.HZ_Summ2_opt3)).setText(Html.fromHtml("Non-Standard Construction"));
		((TextView)findViewById(R.id.HZ_Summ2_opt4)).setText(Html.fromHtml("Outdoor Appliances Noted"));
		((TextView)findViewById(R.id.HZ_Summ2_opt5)).setText(Html.fromHtml("Open Foundation Present"));
		((TextView)findViewById(R.id.HZ_Summ2_opt6)).setText(Html.fromHtml("Wood Shingled Roof"));
		((TextView)findViewById(R.id.HZ_Summ2_opt7)).setText(Html.fromHtml("Excess Debris / Garbage /Trash"));
		((TextView)findViewById(R.id.HZ_Summ3_opt1)).setText(Html.fromHtml("Business on Premises"));
		((TextView)findViewById(R.id.HZ_Summ3_opt2)).setText(Html.fromHtml("Building in General Disrepair"));
		((TextView)findViewById(R.id.HZ_Summ3_opt3)).setText(Html.fromHtml("Visual Property Damage Noted"));
		((TextView)findViewById(R.id.HZ_Summ3_opt4)).setText(Html.fromHtml("Structure Partially / Entirely Under Water "));
		((TextView)findViewById(R.id.HZ_Summ3_opt5)).setText(Html.fromHtml("Inoperative / Non-Starting Motors Vehicle Noted"));
		((TextView)findViewById(R.id.HZ_Summ3_opt6)).setText(Html.fromHtml("Recent Drywall Repair Visible/Observed"));
		((TextView)findViewById(R.id.HZ_Summ3_opt7)).setText(Html.fromHtml("Possible Chinese Drywall Product"));
		((TextView)findViewById(R.id.HZ_Summ3_opt8)).setText(Html.fromHtml("Owner Confirm Chinese Drywall Product"));
		((TextView)findViewById(R.id.HZ_Summ3_opt9)).setText(Html.fromHtml("Non Working Security System"));
		((TextView)findViewById(R.id.HZ_Summ3_opt10)).setText(Html.fromHtml("Non Working Smoke Dedicator"));
		
		cf.setupUI((ScrollView) findViewById(R.id.scr));//Hide the key board when it will cliked 
		hottubcoveredtr= (TableRow)findViewById(R.id.hottubcoveredtr);
		PP_TR_hottub= (TableRow)findViewById(R.id.PP_TR_hottub);
		
		HZ_SUMM_VICIOUS_YES = (RadioButton)findViewById(R.id.HZ_SUMM_VICIOUS_YES);HZ_SUMM_VICIOUS_YES.setOnClickListener(new GH_clicker());
		HZ_SUMM_VICIOUS_NO = (RadioButton)findViewById(R.id.HZ_SUMM_VICIOUS_NO);HZ_SUMM_VICIOUS_NO.setOnClickListener(new GH_clicker());
		
		HZ_SUMM_HORSES_YES = (RadioButton)findViewById(R.id.HZ_SUMM_HORSES_YES);HZ_SUMM_HORSES_YES.setOnClickListener(new GH_clicker());
		HZ_SUMM_HORSES_NO = (RadioButton)findViewById(R.id.HZ_SUMM_HORSES_NO);HZ_SUMM_HORSES_NO.setOnClickListener(new GH_clicker());
		
		HZ_SUMM_BUISPREM_YES= (RadioButton)findViewById(R.id.HZ_SUMM_BUISPREM_YES);HZ_SUMM_BUISPREM_YES.setOnClickListener(new GH_clicker());
		HZ_SUMM_BUISPREM_NO= (RadioButton)findViewById(R.id.HZ_SUMM_BUISPREM_NO);HZ_SUMM_BUISPREM_NO.setOnClickListener(new GH_clicker());
		
		HZ_PERI_opt[0]=(CheckBox) findViewById(R.id.HZ_PERI_opt1);
	    HZ_PERI_opt[1]=(CheckBox) findViewById(R.id.HZ_PERI_opt2);
	    HZ_PERI_opt[2]=(CheckBox) findViewById(R.id.HZ_PERI_opt3);
	    HZ_PERI_opt[3]=(CheckBox) findViewById(R.id.HZ_PERI_opt4);
	    HZ_PERI_opt[4]=(CheckBox) findViewById(R.id.HZ_PERI_opt5);HZ_PERI_opt[4].setText("Enclosure >4FT");
	    HZ_PERI_opt[5]=(CheckBox) findViewById(R.id.HZ_PERI_opt6);HZ_PERI_opt[5].setText("Enclosure <4FT");
	    HZ_PERI_opt[6]=(CheckBox) findViewById(R.id.HZ_PERI_opt7);
	    etpoolfenceother= (EditText)findViewById(R.id.etpoolfenceother);
	    HZ_PERI_opt[6].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(((CheckBox) v).isChecked())
				{
					etpoolfenceother.setVisibility(v.VISIBLE);
				}
				else
				{
					etpoolfenceother.setVisibility(v.GONE);
					etpoolfenceother.setText("");
				}
			}
		});
	    //ArrayListenerPerimeter(HZ_PERI_opt);
        ethorses = (EditText)findViewById(R.id.ethorses);
		
		 
		 
		 vicious_lin = (LinearLayout)findViewById(R.id.vicious_lin);
		 horses_lin = (LinearLayout)findViewById(R.id.horses_lin);
		 business_lin = (LinearLayout)findViewById(R.id. busin_lin);
		 summaryhazardoptionlist1 = (LinearLayout)findViewById(R.id.HZ_summary_optlist1);
		
		 poolfencelist1 = (LinearLayout)findViewById(R.id.HZ_Poolfence_optlist1);
		 
		 HZ_general_header = (RelativeLayout)findViewById(R.id.HZ_general_header);HZ_general_header.setOnClickListener(new GH_clicker());
		 perimeterpoolfenceheader= (RelativeLayout)findViewById(R.id.HZ_perimeterpoolfence_header);perimeterpoolfenceheader.setOnClickListener(new GH_clicker());
		 summaryheader= (RelativeLayout)findViewById(R.id.HZ_summary_header);summaryheader.setOnClickListener(new GH_clicker());
		
		/*    HZ_PERI_opt[0]=(RadioButton) findViewById(R.id.HZ_PERI_opt1);
		    HZ_PERI_opt[1]=(RadioButton) findViewById(R.id.HZ_PERI_opt2);
		    HZ_PERI_opt[2]=(RadioButton) findViewById(R.id.HZ_PERI_opt3);
		    HZ_PERI_opt[3]=(RadioButton) findViewById(R.id.HZ_PERI_opt4);
		    HZ_PERI_opt[4]=(RadioButton) findViewById(R.id.HZ_PERI_opt5);
		    HZ_PERI_opt[5]=(RadioButton) findViewById(R.id.HZ_PERI_opt6);
		    HZ_PERI_opt[6]=(RadioButton) findViewById(R.id.HZ_PERI_opt7);
		    ArrayListenerPerimeter(HZ_PERI_opt);
		    
		*/    PP_RG_GROUNDLEVEL=(RadioGroup)findViewById(R.id.PP_RG_GROUNDLEVEL);
		    PP_RGRP1=(RadioGroup) findViewById(R.id.PP_RGRP1);
		    PP_RGPPS=(RadioGroup) findViewById(R.id.PP_RGPPS);
		    PP_RGRP2=(RadioGroup) findViewById(R.id.PP_RGRP2);
		    PP_RGRP3=(RadioGroup) findViewById(R.id.PP_RGRP3);
		    PP_RGRP4=(RadioGroup) findViewById(R.id.PP_RGRP4);
		    PP_RGRP5=(RadioGroup) findViewById(R.id.PP_RGRP5);
		    PP_RGRP6=(RadioGroup) findViewById(R.id.PP_RGRP6);
		    PP_RGRP7=(RadioGroup) findViewById(R.id.PP_RGRP7); 
		    PF_RG_POOLPRESENT=(RadioGroup) findViewById(R.id.PF_RG_POOLPRESENT);
		    
		    PF_RG_SELFLATCHES=(RadioGroup) findViewById(R.id.PF_RG_SELFLATCHES);
		    PF_RG_PROFINST=(RadioGroup) findViewById(R.id.PF_RG_PROFINST);
		    PF_RG_POOLFENCE=(RadioGroup) findViewById(R.id.PF_RG_POOLFENCE);  
		    ettrpihazards= (EditText)findViewById(R.id.ettrpihazards);
		    ettrpihazards.setOnTouchListener(new OnTouchListener() {
				
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					cf.setFocus(ettrpihazards);
					
					return false;
				}
			});

		    PF_RG_TRIPHAZ=(RadioGroup) findViewById(R.id.PF_RG_TRIPHAZ); 
		    
		    PF_RG_TRIPHAZ.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					// TODO Auto-generated method stub
					if(((RadioButton) findViewById(checkedId)).getText().toString().trim().equals("None Observed"))
					{
						ettrpihazards.setVisibility(View.GONE);
					}
					else
					{
						ettrpihazards.setVisibility(View.VISIBLE);
					}
				}
			});
		    
		    PP_RG_SUMMHAZ1_Q1=(RadioGroup) findViewById(R.id.PP_RG_SUMMHAZ1_Q1);
		    PP_RG_SUMMHAZ1_Q2=(RadioGroup) findViewById(R.id.PP_RG_SUMMHAZ1_Q2);
		    PP_RG_SUMMHAZ1_Q3=(RadioGroup) findViewById(R.id.PP_RG_SUMMHAZ1_Q3);
		    PP_RG_SUMMHAZ1_Q4=(RadioGroup) findViewById(R.id.PP_RG_SUMMHAZ1_Q4);
		    PP_RG_SUMMHAZ1_Q5=(RadioGroup) findViewById(R.id.PP_RG_SUMMHAZ1_Q5);
		    PP_RG_SUMMHAZ1_Q6=(RadioGroup) findViewById(R.id.PP_RG_SUMMHAZ1_Q6);
		    
		    PP_RG_SUMMHAZ2_Q1=(RadioGroup) findViewById(R.id.PP_RG_SUMMHAZ2_Q1);
		    PP_RG_SUMMHAZ2_Q2=(RadioGroup) findViewById(R.id.PP_RG_SUMMHAZ2_Q2);
		    PP_RG_SUMMHAZ2_Q3=(RadioGroup) findViewById(R.id.PP_RG_SUMMHAZ2_Q3);
		    PP_RG_SUMMHAZ2_Q4=(RadioGroup) findViewById(R.id.PP_RG_SUMMHAZ2_Q4);
		    PP_RG_SUMMHAZ2_Q5=(RadioGroup) findViewById(R.id.PP_RG_SUMMHAZ2_Q5);
		    PP_RG_SUMMHAZ2_Q6=(RadioGroup) findViewById(R.id.PP_RG_SUMMHAZ2_Q6);
		    PP_RG_SUMMHAZ2_Q7=(RadioGroup) findViewById(R.id.PP_RG_SUMMHAZ2_Q7);
		    
		    PP_RG_SUMMHAZ3_Q1=(RadioGroup) findViewById(R.id.PP_RG_SUMMHAZ3_Q1);
		    PP_RG_SUMMHAZ3_Q2=(RadioGroup) findViewById(R.id.PP_RG_SUMMHAZ3_Q2);
		    PP_RG_SUMMHAZ3_Q3=(RadioGroup) findViewById(R.id.PP_RG_SUMMHAZ3_Q3);
		    PP_RG_SUMMHAZ3_Q4=(RadioGroup) findViewById(R.id.PP_RG_SUMMHAZ3_Q4);
		    PP_RG_SUMMHAZ3_Q5=(RadioGroup) findViewById(R.id.PP_RG_SUMMHAZ3_Q5);
		    PP_RG_SUMMHAZ3_Q6=(RadioGroup) findViewById(R.id.PP_RG_SUMMHAZ3_Q6);
		    PP_RG_SUMMHAZ3_Q7=(RadioGroup) findViewById(R.id.PP_RG_SUMMHAZ3_Q7);
		    PP_RG_SUMMHAZ3_Q8=(RadioGroup) findViewById(R.id.PP_RG_SUMMHAZ3_Q8);
		    PP_RG_SUMMHAZ3_Q9=(RadioGroup) findViewById(R.id.PP_RG_SUMMHAZ3_Q9);
		    PP_RG_SUMMHAZ3_Q10=(RadioGroup) findViewById(R.id.PP_RG_SUMMHAZ3_Q10);
		
		    HZ_comments=(EditText) this.findViewById(R.id.comment);
		   // HZ_comments.addTextChangedListener(new GHC_textwatcher());
		    
		    
		    cf.SQ_ED_type1_parrant = (LinearLayout)this.findViewById(R.id.SQ_ED_type1_parrant); 
		    cf.SQ_ED_type1=(LinearLayout) findViewById(R.id.SQ_ED_type1);
			cf.SQ_TV_type1 = (TextView) findViewById(R.id.SQ_TV_type1);
			
			BUIS_ED_type1_parrant = (LinearLayout)this.findViewById(R.id.buisprem_parrent);
		    BUIS_ED_type1=(LinearLayout) findViewById(R.id.buisprem_type);
			BUIS_TV_type1 = (TextView) findViewById(R.id.buisprem_txt);
			
			HORSES_ED_type1_parrant = (LinearLayout)this.findViewById(R.id.horses_parrent);
			HORSES_ED_type1=(LinearLayout) findViewById(R.id.horses_type);
			HORSES_TV_type1 = (TextView) findViewById(R.id.horses_txt);
			
			
			VICIOUS_ED_type1_parrant = (LinearLayout)this.findViewById(R.id.vicious_parrent);
			VICIOUS_ED_type1=(LinearLayout) findViewById(R.id.vicious_type);
			VICIOUS_TV_type1 = (TextView) findViewById(R.id.vicious_txt);
			
			/**Beyond the scope of inspection declaration Starts **/
			/*SH_rw20_lin = (LinearLayout) findViewById(R.id.SH_rw20_lin);
			SH_rw21_lin = (LinearLayout) findViewById(R.id.SH_rw21_lin);
			SH_rw22_lin = (LinearLayout) findViewById(R.id.SH_rw22_lin);
			SH_rw23_lin = (LinearLayout) findViewById(R.id.SH_rw23_lin);
			SH_rw24_lin = (LinearLayout) findViewById(R.id.SH_rw24_lin);
			
			SH_rw20_lin_parrent = (LinearLayout) findViewById(R.id.SH_rw20_lin_parrent);
			SH_rw21_lin_parrent = (LinearLayout) findViewById(R.id.SH_rw21_lin_parrent);
			SH_rw22_lin_parrent = (LinearLayout) findViewById(R.id.SH_rw22_lin_parrent);
			SH_rw23_lin_parrent = (LinearLayout) findViewById(R.id.SH_rw23_lin_parrent);
			SH_rw24_lin_parrent = (LinearLayout) findViewById(R.id.SH_rw24_lin_parrent);
			
			SH_rw20_lin_chaild = (LinearLayout) findViewById(R.id.SH_rw20_lin_chaild);
			SH_rw21_lin_chaild = (LinearLayout) findViewById(R.id.SH_rw21_lin_chaild);
			SH_rw22_lin_chaild = (LinearLayout) findViewById(R.id.SH_rw22_lin_chaild);
			SH_rw23_lin_chaild = (LinearLayout) findViewById(R.id.SH_rw23_lin_chaild);
			SH_rw24_lin_chaild = (LinearLayout) findViewById(R.id.SH_rw24_lin_chaild);
			
			SH_rw20_ed = (EditText) findViewById(R.id.SH_rw20_ed);
			SH_rw21_ed = (EditText) findViewById(R.id.SH_rw21_ed);
			SH_rw22_ed = (EditText) findViewById(R.id.SH_rw22_ed);
			SH_rw23_ed = (EditText) findViewById(R.id.SH_rw23_ed);
			SH_rw24_ed = (EditText) findViewById(R.id.SH_rw24_ed);
			
			SH_rw20_ed.addTextChangedListener(new GHC_textwatcher(5));
			SH_rw21_ed.addTextChangedListener(new GHC_textwatcher(6));
			SH_rw22_ed.addTextChangedListener(new GHC_textwatcher(7));
			SH_rw23_ed.addTextChangedListener(new GHC_textwatcher(8));
			SH_rw24_ed.addTextChangedListener(new GHC_textwatcher(9));
			
			SH_rw20_ed.setOnTouchListener(new Touch_Listener(5));
			SH_rw21_ed.setOnTouchListener(new Touch_Listener(6));
			SH_rw22_ed.setOnTouchListener(new Touch_Listener(7));
			SH_rw23_ed.setOnTouchListener(new Touch_Listener(8));
			SH_rw24_ed.setOnTouchListener(new Touch_Listener(9));
			
			SH_rw20_txt = (TextView) findViewById(R.id.SH_rw20_txt);
			SH_rw21_txt = (TextView) findViewById(R.id.SH_rw21_txt);
			SH_rw22_txt = (TextView) findViewById(R.id.SH_rw22_txt);
			SH_rw23_txt = (TextView) findViewById(R.id.SH_rw23_txt);
			SH_rw24_txt = (TextView) findViewById(R.id.SH_rw24_txt);*/
			
			/**Beyond the scope of inspection declaration ends **/
			
			etvicious = (EditText)findViewById(R.id.etvicious);
			etsummbuisprem= (EditText)findViewById(R.id.etsummbuisprem);
			etvicious.addTextChangedListener(new GHC_textwatcher(1));
			etsummbuisprem.addTextChangedListener(new GHC_textwatcher(2));
			ethorses.addTextChangedListener(new GHC_textwatcher(3));
			HZ_comments.addTextChangedListener(new GHC_textwatcher(4));
		    poollist1= (LinearLayout)findViewById(R.id.GCH_Haz_PP_content);
		   
		    etvicious.setOnTouchListener(new Touch_Listener(1));
		    etsummbuisprem.setOnTouchListener(new Touch_Listener(2));	
		    ethorses.setOnTouchListener(new Touch_Listener(3));
		    HZ_comments.setOnTouchListener(new Touch_Listener(4));
		    ettrpihazards.setOnTouchListener(new Touch_Listener(5));
		    etpoolfenceother.setOnTouchListener(new Touch_Listener(6));
		    
		    
			 ISPoolPresent = (CheckBox)findViewById(R.id.HZ_PP_Notappl);
			 ISPoolPresent.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						if (((CheckBox) v).isChecked()) {
							ISPoolpresentval=0;
							poollist1.setVisibility(v.GONE);
							HZ_general_header.setBackgroundResource(R.drawable.backrepeatnorobs);
							clear();
							/**Michele request us to change if the No clicked need to auto select the N/a of Isperimeterpoolfenceval **/
							ISPermiterpoolPresent.setChecked(true);
							ISPermiterpoolPresent.setEnabled(false);
							Isperimeterpoolfenceval=0;
							poolfencelist1.setVisibility(v.GONE);
							perimeterpoolfenceheader.setBackgroundResource(R.drawable.backrepeatnorobs);
							perimeterclear();
							((TextView) findViewById(R.id.GCH_GH_C_S2)).setVisibility(View.GONE);
							((TextView) findViewById(R.id.GCH_GH_C_S1)).setVisibility(View.GONE);
							
						}
						else
						{
							defaultlayout();
							
							/**Set the default value for the pool present **/
							((RadioButton) PP_RGPPS.findViewWithTag("No")).setChecked(true);
						 	PP_TR_hottub.setVisibility(View.GONE);
						 	hottubcoveredtr.setVisibility(View.GONE);
						 	 	
						 	((RadioButton) PP_RGRP1.findViewWithTag("No")).setChecked(true);
						 	((RadioButton) PP_RGRP2.findViewWithTag("No")).setChecked(true);
						 	((RadioButton) PP_RGRP3.findViewWithTag("No")).setChecked(true);
						 	((RadioButton) PP_RGRP4.findViewWithTag("No")).setChecked(true);
						 	((RadioButton) PP_RGRP5.findViewWithTag("No")).setChecked(true);
						 	((RadioButton) PP_RGRP6.findViewWithTag("No")).setChecked(true);
						 	((RadioButton) PP_RGRP7.findViewWithTag("No")).setChecked(true);
						 	
						 	/**Set the default value for the pool present Ends **/
						
							ISPoolpresentval=1;
							poollist1.setVisibility(v.VISIBLE);
							ISPermiterpoolPresent.setEnabled(true);
							HZ_general_header.setBackgroundResource(R.drawable.subbackrepeatnor);
							ISPermiterpoolPresent.setChecked(false);
							perimeterclear();
							 /**Set the default value for the pool fence Starts **/
						    ((RadioButton) PF_RG_SELFLATCHES.findViewWithTag("No")).setChecked(true);
							((RadioButton) PF_RG_PROFINST.findViewWithTag("No")).setChecked(true);
							((RadioButton) PF_RG_POOLFENCE.findViewWithTag("No")).setChecked(true);
						/**Set the default value for the pool fence Ends **/
						}
					}
			 });
			 
			 ISPermiterpoolPresent = (CheckBox)findViewById(R.id.HZ_PF_Notappl);
			 ISPermiterpoolPresent.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						if (((CheckBox) v).isChecked()) {
							Isperimeterpoolfenceval=0;
							poolfencelist1.setVisibility(v.GONE);
							perimeterpoolfenceheader.setBackgroundResource(R.drawable.backrepeatnorobs);
							((TextView) findViewById(R.id.GCH_GH_C_S2)).setVisibility(View.GONE);
							perimeterclear();
						}
						else
						{	defaultlayout();
						 /**Set the default value for the pool fence Starts **/
					    ((RadioButton) PF_RG_SELFLATCHES.findViewWithTag("No")).setChecked(true);
						((RadioButton) PF_RG_PROFINST.findViewWithTag("No")).setChecked(true);
						((RadioButton) PF_RG_POOLFENCE.findViewWithTag("No")).setChecked(true);
					/**Set the default value for the pool fence Ends **/
							Isperimeterpoolfenceval=1;
							poolfencelist1.setVisibility(v.VISIBLE);
							perimeterpoolfenceheader.setBackgroundResource(R.drawable.subbackrepeatnor);
						}
					}
			 });
			 focus=1;
			 summaryhazardoptionlist1.setVisibility(View.VISIBLE);
			 GHC_setValue();
			 ((ScrollView) findViewById(R.id.scr)).scrollTo(0, 0);
	}
	class GHC_textwatcher implements TextWatcher
	{
	     public int type;
	     GHC_textwatcher(int type)
		{
			this.type=type;
		}
		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			if(this.type==1)
			{
				cf.showing_limit(s.toString(),VICIOUS_ED_type1_parrant,VICIOUS_ED_type1,VICIOUS_TV_type1,"150");
				
			}
			else if(this.type==2)
			{
				cf.showing_limit(s.toString(),BUIS_ED_type1_parrant,BUIS_ED_type1,BUIS_TV_type1,"150"); 
			}
			else if(this.type==3)
			{
				cf.showing_limit(s.toString(),HORSES_ED_type1_parrant,HORSES_ED_type1,HORSES_TV_type1,"150");
				
			}
			else if(this.type==4)
			{
				cf.showing_limit(s.toString(),cf.SQ_ED_type1_parrant,cf.SQ_ED_type1,cf.SQ_TV_type1,"1000");
			}
			/*else if(this.type==5)
			{
				cf.showing_limit(s.toString(),SH_rw20_lin_parrent,SH_rw20_lin_chaild,SH_rw20_txt,"199");
			}
			else if(this.type==6)
			{
				cf.showing_limit(s.toString(),SH_rw21_lin_parrent,SH_rw21_lin_chaild,SH_rw21_txt,"199");
			}
			else if(this.type==7)
			{
				cf.showing_limit(s.toString(),SH_rw22_lin_parrent,SH_rw22_lin_chaild,SH_rw22_txt,"199");
			}
			else if(this.type==8)
			{
				cf.showing_limit(s.toString(),SH_rw23_lin_parrent,SH_rw23_lin_chaild,SH_rw23_txt,"199");
			}
			else if(this.type==9)
			{
				cf.showing_limit(s.toString(),SH_rw24_lin_parrent,SH_rw24_lin_chaild,SH_rw24_txt,"199");
			}
		*/
		}
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			
		}
	}
	protected void perimeterclear() {
		// TODO Auto-generated method stub
		GHC_PPFECNCE_VAL="";PF_HZ_OTHERPERI="";PF_RG_SELFLATCHES_val="";PF_RG_PROFINST_val="";PF_RG_POOLFENCE_val="";			
		HZ_PERI_opt[0].setChecked(false);HZ_PERI_opt[1].setChecked(false);HZ_PERI_opt[2].setChecked(false);HZ_PERI_opt[3].setChecked(false);
		HZ_PERI_opt[4].setChecked(false);HZ_PERI_opt[5].setChecked(false);HZ_PERI_opt[6].setChecked(false);	
		PF_RG_SELFLATCHES.clearCheck();	PF_RG_PROFINST.clearCheck();PF_RG_POOLFENCE.clearCheck();
		etpoolfenceother.setText("");
	}
	protected void clear() {
		// TODO Auto-generated method stub
				PP_RG_GROUNDLEVEL_val="";
				PP_RGRP1_val="";PP_RGRP3_val="";PP_RGRP4_val="";PP_RGRP5_val="";PP_RGRP6_val="";PP_RGRP2_val="";PP_RGRP7_val="";PP_RGPPS_val="";
				PP_RG_GROUNDLEVEL.clearCheck();PP_RGRP1.clearCheck();PP_RGRP2.clearCheck();PP_RGRP3.clearCheck();
				PP_RGRP4.clearCheck();PP_RGRP5.clearCheck();PP_RGRP6.clearCheck();PP_RGRP7.clearCheck();				
	}
	class Touch_Listener implements OnTouchListener
	{
		   public int type;
		   Touch_Listener(int type)
			{
				this.type=type;
				
			}
		    @Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
		    	((EditText) findViewById(v.getId())).setFocusableInTouchMode(true);
		    	((EditText) findViewById(v.getId())).requestFocus();
		    	((EditText) findViewById(v.getId())).setSelection(((EditText) findViewById(v.getId())).length());   	
				return false;
		    }
	}

	private void GHC_setValue() {
		// TODO Auto-generated method stub
		Cursor BI_retrive;
		   try
			{
			   cf.Create_Table(7);
			    BI_retrive=cf.SelectTablefunction(cf.GHC_table, " where GCH_HZ_Homeid='"+cf.encode(cf.selectedhomeid)+"'");
				 if(BI_retrive.getCount()>0)
				 {  
					 BI_retrive.moveToFirst();
					 GHC_ISPoolPresent = BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_ISPOOL_PRES"));
					 GHC_PoolPresent=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_POOLPRES")));
					 GHC_Perimeterpool=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_PERI_PF")));
					 GHC_HOTTUB=BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_HOTTUB"));
					 GHC_PPS=BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_PPS"));
					 GHC_HOTTUBCOVER=BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_HOTTUBCOVER"));
					 GHC_POOLSLIDE=BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_POOLSLIDE"));
					 GHC_DIVINGBOARD=BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_DIVINGBOARD"));
					 GHC_PERIPOOL=BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_PERIPOOL"));
					 GHC_POOLENCLOS=BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_POOLENCLOS"));
					 GCH_EMPTYPOOLPRESENT=BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_EMPTYPOOLPRESENT"));
					 GHC_PERIMETERPOOLFENCE=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_PERI_PF")));
					 GCH_OTHERPERI=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_OTHERPERI_PF")));
					 GHC_SELFLATCH=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_SELFLATCH")));	
					 GHC_PROF_INST=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_PROF_INST")));	
					 GHC_PFDESREPAIR=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_PF_DISREPAIR")));
					 GHC_VICIOUS=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_VICIOUS")));
					 GCH_VICIOUSDESC=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_VICIOUSDESC")));
					 GCH_LIVESTOCK=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_LIVESTOCK")));
					 GCH_LIVESTOCKDESC=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_LIVESTOCKDESC")));
					 GCH_OVERHANGING=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_OVERHANGING")));
					 GCH_TRAMPOLINE=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_TRAMPOLINE")));
					 GCH_SKATEBOARD=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_SKATEBOARD")));
					 GCH_BICYCLE=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_BICYCLE")));
					 
					 GCH_GROUNDPOOL=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_GROUNDPOOL")));
					 GCH_TRIPHAZARD=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_TRIPHAZARD")));
					 GCH_TRIPHAZARDNOTES=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_TRIPHAZARDNOTES")));
					 GCH_UNSAFESTAIRWAY=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_UNSAFESTAIRWAY")));
					 GCH_PORCHANDDESK=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_PORCHANDDESK")));
					 GCH_NONSTDCONST=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_NONSTDCONST")));
					 GCH_OUTDOORAPPL=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_OUTDOORAPPL")));
					 GCH_OPENFOUND=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_OPENFOUND")));
					 GCH_WOODSINGHLED=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_WOODSINGHLED")));
					 GCH_EXCESSDEBRIS=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_EXCESSDEBRIS")));
					 GCH_BUISPREMISES=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_BUISPREMISES")));
					 GCH_BUISPREMDESC=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_BUISPREMDESC")));
					 GCH_GENDISREPAIR=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_GENDISREPAIR")));
					 GCH_PROP_DAMAGE=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_PROP_DAMAGE")));	
					 GCH_STRUPARTIAL=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_STRUPARTIAL")));
					 GCH_INOPERATIVE=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_INOPERATIVE")));
					 GCH_RECDRYWALL=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_RECDRYWALL")));
					 GCH_CHINESEDRYWALL=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_CHINESEDRYWALL")));
					 GCH_CONFDRYWALL=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_CONFDRYWALL")));
					 GCH_NONSECURITY=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_NONSECURITY")));
					 GCH_NONSMOKE=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_NONSMOKE")));
					 
					 GCH_RECDRYWALL_BSI=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_RECDRYWALL_BSI")));
					 GCH_CHINESEDRYWALL_BSI=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_CHINESEDRYWALL_BSI")));
					 GCH_CONFDRYWALL_BSI=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_CONFDRYWALL_BSI")));
					 GCH_NONSECURITY_BSI=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_NONSECURITY_BSI")));
					 GCH_NONSMOKE_BSI=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_NONSMOKE_BSI")));
					 
					 
					 GCH_COMMENTS=cf.decode(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_COMMENTS")));
					 
					
					 if(GHC_ISPoolPresent.equals("1")){
					  poollist1.setVisibility(v1.VISIBLE);
					  ISPoolPresent.setChecked(false);
					 if(!GHC_PoolPresent.trim().equals("")) 
						 ((RadioButton) PP_RG_GROUNDLEVEL.findViewWithTag(GHC_PoolPresent)).setChecked(true);
					 
					 
					 if(!GHC_HOTTUB.trim().equals(""))
					 {
						 ((RadioButton) PP_RGRP1.findViewWithTag(GHC_HOTTUB)).setChecked(true);
						((TextView) findViewById(R.id.GCH_GH_C_S1)).setVisibility(View.VISIBLE);  /** show the completed and saved text in the top**/
					 }
					 else
					 {
						 ((RadioButton) PP_RGRP1.findViewWithTag("No")).setChecked(true);
						 ((RadioButton) PP_RGRP3.findViewWithTag("No")).setChecked(true);
						  ((RadioButton) PP_RGPPS.findViewWithTag("No")).setChecked(true);
					 }
					 
						if(GHC_HOTTUB.equals("Yes")){
							PP_TR_hottub.setVisibility(View.VISIBLE);
							hottubcoveredtr.setVisibility(v1.VISIBLE);
							if(!GHC_HOTTUBCOVER.trim().equals(""))
							{
							((RadioButton) PP_RGRP3.findViewWithTag(GHC_HOTTUBCOVER)).setChecked(true);
							}
						}
						else
						{
							PP_TR_hottub.setVisibility(View.GONE);
							  ((RadioButton) PP_RGRP3.findViewWithTag("No")).setChecked(true);
							  ((RadioButton) PP_RGPPS.findViewWithTag("No")).setChecked(true);
								 
							hottubcoveredtr.setVisibility(v1.GONE);
						}
						if(!GHC_PPS.trim().equals(""))
						{
							((RadioButton) PP_RGPPS.findViewWithTag(GHC_PPS)).setChecked(true);
						}
						else
							 ((RadioButton) PP_RGPPS.findViewWithTag("No")).setChecked(true);
						 
						
					
						
					if(!GCH_EMPTYPOOLPRESENT.trim().equals(""))	
					((RadioButton) PP_RGRP2.findViewWithTag(GCH_EMPTYPOOLPRESENT)).setChecked(true);
					 else
						 ((RadioButton) PP_RGRP2.findViewWithTag("No")).setChecked(true);
					 
					if(!GHC_POOLSLIDE.trim().equals(""))
					((RadioButton) PP_RGRP4.findViewWithTag(GHC_POOLSLIDE)).setChecked(true);
					 else
						 ((RadioButton) PP_RGRP4.findViewWithTag("No")).setChecked(true);
					 
					if(!GHC_DIVINGBOARD.trim().equals(""))
					((RadioButton) PP_RGRP5.findViewWithTag(GHC_DIVINGBOARD)).setChecked(true);
					 else
						 ((RadioButton) PP_RGRP5.findViewWithTag("No")).setChecked(true);
					 
					if(!GHC_PERIPOOL.trim().equals(""))
					((RadioButton) PP_RGRP6.findViewWithTag(GHC_PERIPOOL)).setChecked(true);
					 else
						 ((RadioButton) PP_RGRP6.findViewWithTag("No")).setChecked(true);
					 
					if(!GHC_POOLENCLOS.trim().equals(""))
					((RadioButton) PP_RGRP7.findViewWithTag(GHC_POOLENCLOS)).setChecked(true);
					 else
						 ((RadioButton) PP_RGRP7.findViewWithTag("No")).setChecked(true);
					 
					ISPermiterpoolPresent.setEnabled(true);

					 }
					 else
					 { 
						 ((TextView) findViewById(R.id.GCH_GH_C_S1)).setVisibility(View.VISIBLE); /** show the completed and saved text in the top**/
						 ISPoolPresent.setChecked(true);
						 HZ_general_header.setBackgroundResource(R.drawable.backrepeatnorobs);
						 poollist1.setVisibility(v1.GONE);
						 /**Michele request us to change if the No clicked need to auto select the N/a of Isperimeterpoolfenceval **/
							ISPermiterpoolPresent.setChecked(true);
							ISPermiterpoolPresent.setEnabled(false);
							Isperimeterpoolfenceval=0;
							poolfencelist1.setVisibility(View.GONE);
							perimeterpoolfenceheader.setBackgroundResource(R.drawable.backrepeatnorobs);
							perimeterclear();
						/**Set the default value for the pool present **/
							((RadioButton) PP_RGPPS.findViewWithTag("No")).setChecked(true);
						 	PP_RGRP1.setVisibility(View.GONE);
						 
						 	((RadioButton) PP_RGRP1.findViewWithTag("No")).setChecked(true);
						 	((RadioButton) PP_RGRP2.findViewWithTag("No")).setChecked(true);
						 	((RadioButton) PP_RGRP3.findViewWithTag("No")).setChecked(true);
						 	((RadioButton) PP_RGRP4.findViewWithTag("No")).setChecked(true);
						 	((RadioButton) PP_RGRP5.findViewWithTag("No")).setChecked(true);
						 	((RadioButton) PP_RGRP6.findViewWithTag("No")).setChecked(true);
						 	((RadioButton) PP_RGRP7.findViewWithTag("No")).setChecked(true);
						 	
						 	/**Set the default value for the pool present Ends **/
						
						
					 }
					 
					 if(BI_retrive.getString(BI_retrive.getColumnIndex("GCH_HZ_ISPERI_PF")).equals("1")){
					//poolfencelist1.setVisibility(v1.VISIBLE); /**Hide fields for the first time loading **/
					ISPermiterpoolPresent.setChecked(false);
					if(!GHC_SELFLATCH.trim().equals("")) 
						((RadioButton) PF_RG_SELFLATCHES.findViewWithTag(GHC_SELFLATCH)).setChecked(true);
					else
						 ((RadioButton) PF_RG_SELFLATCHES.findViewWithTag("No")).setChecked(true);
					if(!GHC_PROF_INST.trim().equals("")) 
						((RadioButton) PF_RG_PROFINST.findViewWithTag(GHC_PROF_INST)).setChecked(true);
					else
						 ((RadioButton) PF_RG_PROFINST.findViewWithTag("No")).setChecked(true);
					
					if(!GHC_PFDESREPAIR.trim().equals("")) 
						((RadioButton) PF_RG_POOLFENCE.findViewWithTag(GHC_PFDESREPAIR)).setChecked(true);
					else
						 ((RadioButton) PF_RG_POOLFENCE.findViewWithTag("No")).setChecked(true);
					
					if(!GHC_PERIMETERPOOLFENCE.trim().equals(""))
						cf.setvaluechk(GHC_PERIMETERPOOLFENCE,HZ_PERI_opt);
						//((RadioButton) PF_RG_POOLPRESENT.findViewWithTag(GHC_PERIMETERPOOLFENCE)).setChecked(true);
					if(!GHC_SELFLATCH.trim().equals(""))
					{
						((TextView) findViewById(R.id.GCH_GH_C_S2)).setVisibility(View.VISIBLE);  /** show the completed and saved text in the top**/
					}
					
					if(GHC_PERIMETERPOOLFENCE.contains("Other")){
						etpoolfenceother.setFocusableInTouchMode(true);
						etpoolfenceother.requestFocus();
						etpoolfenceother.setCursorVisible(true);
						etpoolfenceother.setVisibility(v1.VISIBLE);etpoolfenceother.setText(GCH_OTHERPERI);}
					else{etpoolfenceother.setVisibility(v1.GONE);}
					 }
					 else
					 {
								((TextView) findViewById(R.id.GCH_GH_C_S2)).setVisibility(View.VISIBLE);  /** show the completed and saved text in the top**/
							
						 ISPermiterpoolPresent.setChecked(true);
						 perimeterpoolfenceheader.setBackgroundResource(R.drawable.backrepeatnorobs);

						 poolfencelist1.setVisibility(v1.GONE);
							
						 /**Set the default value for the pool fence Starts **/
						    ((RadioButton) PF_RG_SELFLATCHES.findViewWithTag("No")).setChecked(true);
							((RadioButton) PF_RG_PROFINST.findViewWithTag("No")).setChecked(true);
							((RadioButton) PF_RG_POOLFENCE.findViewWithTag("No")).setChecked(true);
						/**Set the default value for the pool fence Ends **/
						 	
					 }
					 
					 if(!GHC_VICIOUS.trim().equals(""))
						{
							((TextView) findViewById(R.id.GCH_GH_C_S3)).setVisibility(View.VISIBLE);  /** show the completed and saved text in the top**/
							
						}
					 if(!GHC_VICIOUS.trim().equals(""))  
						 ((RadioButton) PP_RG_SUMMHAZ1_Q1.findViewWithTag(GHC_VICIOUS)).setChecked(true);
					 else
						 ((RadioButton) PP_RG_SUMMHAZ1_Q1.findViewWithTag("No")).setChecked(true);
					
					 if(!GCH_LIVESTOCK.trim().equals("")) 
						 ((RadioButton) PP_RG_SUMMHAZ1_Q2.findViewWithTag(GCH_LIVESTOCK)).setChecked(true);
					 else
						 ((RadioButton) PP_RG_SUMMHAZ1_Q2.findViewWithTag("No")).setChecked(true);
					 if(!GCH_OVERHANGING.trim().equals("")) 
						 ((RadioButton) PP_RG_SUMMHAZ1_Q3.findViewWithTag(GCH_OVERHANGING)).setChecked(true);
					 else
						 ((RadioButton) PP_RG_SUMMHAZ1_Q3.findViewWithTag("No")).setChecked(true);
					 if(!GCH_TRAMPOLINE.trim().equals("")) 
						 ((RadioButton) PP_RG_SUMMHAZ1_Q4.findViewWithTag(GCH_TRAMPOLINE)).setChecked(true);
					 else
						 ((RadioButton) PP_RG_SUMMHAZ1_Q4.findViewWithTag("No")).setChecked(true);
					 if(!GCH_SKATEBOARD.trim().equals("")) 
						 ((RadioButton) PP_RG_SUMMHAZ1_Q5.findViewWithTag(GCH_SKATEBOARD)).setChecked(true);
					 else
						 ((RadioButton) PP_RG_SUMMHAZ1_Q5.findViewWithTag("No")).setChecked(true);
					 if(!GCH_BICYCLE.trim().equals("")) 
						 ((RadioButton) PP_RG_SUMMHAZ1_Q6.findViewWithTag(GCH_BICYCLE)).setChecked(true);
					 else
						 ((RadioButton) PP_RG_SUMMHAZ1_Q6.findViewWithTag("No")).setChecked(true);
					 if(!GCH_TRIPHAZARD.trim().equals("")) 
						 ((RadioButton) PF_RG_TRIPHAZ.findViewWithTag(GCH_TRIPHAZARD)).setChecked(true);
					 
					 if(!GCH_UNSAFESTAIRWAY.trim().equals("")) 
						 ((RadioButton) PP_RG_SUMMHAZ2_Q1.findViewWithTag(GCH_UNSAFESTAIRWAY)).setChecked(true);
					 else
						 ((RadioButton) PP_RG_SUMMHAZ2_Q1.findViewWithTag("No")).setChecked(true);
					 if(!GCH_PORCHANDDESK.trim().equals("")) 
					 	((RadioButton) PP_RG_SUMMHAZ2_Q2.findViewWithTag(GCH_PORCHANDDESK)).setChecked(true);
					 else
						 ((RadioButton) PP_RG_SUMMHAZ2_Q2.findViewWithTag("No")).setChecked(true);
					 if(!GCH_NONSTDCONST.trim().equals("")) 
					 	((RadioButton) PP_RG_SUMMHAZ2_Q3.findViewWithTag(GCH_NONSTDCONST)).setChecked(true);
					 else
						 ((RadioButton) PP_RG_SUMMHAZ2_Q3.findViewWithTag("No")).setChecked(true);
					 if(!GCH_OUTDOORAPPL.trim().equals("")) 
					 	((RadioButton) PP_RG_SUMMHAZ2_Q4.findViewWithTag(GCH_OUTDOORAPPL)).setChecked(true);
					 else
						 ((RadioButton) PP_RG_SUMMHAZ2_Q4.findViewWithTag("No")).setChecked(true);
					 if(!GCH_OPENFOUND.trim().equals("")) 
					 	((RadioButton) PP_RG_SUMMHAZ2_Q5.findViewWithTag(GCH_OPENFOUND)).setChecked(true);
					 else
						 ((RadioButton) PP_RG_SUMMHAZ2_Q5.findViewWithTag("No")).setChecked(true);
					 if(!GCH_WOODSINGHLED.trim().equals("")) 
					 	((RadioButton) PP_RG_SUMMHAZ2_Q6.findViewWithTag(GCH_WOODSINGHLED)).setChecked(true);
					 else
						 ((RadioButton) PP_RG_SUMMHAZ2_Q6.findViewWithTag("No")).setChecked(true);
					 if(!GCH_EXCESSDEBRIS.trim().equals("")) 
					 	((RadioButton) PP_RG_SUMMHAZ2_Q7.findViewWithTag(GCH_EXCESSDEBRIS)).setChecked(true);
					 else
						 ((RadioButton) PP_RG_SUMMHAZ2_Q7.findViewWithTag("No")).setChecked(true);
					
					 if(!GCH_BUISPREMISES.trim().equals("")) 
						 ((RadioButton) PP_RG_SUMMHAZ3_Q1.findViewWithTag(GCH_BUISPREMISES)).setChecked(true);
					 else
						 ((RadioButton) PP_RG_SUMMHAZ3_Q1.findViewWithTag("No")).setChecked(true);
					 if(!GCH_GENDISREPAIR.trim().equals("")) 
					 	((RadioButton) PP_RG_SUMMHAZ3_Q2.findViewWithTag(GCH_GENDISREPAIR)).setChecked(true);
					 else
						 ((RadioButton) PP_RG_SUMMHAZ3_Q2.findViewWithTag("No")).setChecked(true);
					 
					 if(!GCH_PROP_DAMAGE.trim().equals("")) 
					 	((RadioButton) PP_RG_SUMMHAZ3_Q3.findViewWithTag(GCH_PROP_DAMAGE)).setChecked(true);
					 else
						 ((RadioButton) PP_RG_SUMMHAZ3_Q3.findViewWithTag("No")).setChecked(true);
					 
					 if(!GCH_STRUPARTIAL.trim().equals("")) 
					 	((RadioButton) PP_RG_SUMMHAZ3_Q4.findViewWithTag(GCH_STRUPARTIAL)).setChecked(true);
					 else
						 ((RadioButton) PP_RG_SUMMHAZ3_Q4.findViewWithTag("No")).setChecked(true);
					 
					 if(!GCH_INOPERATIVE.trim().equals("")) 
						 ((RadioButton) PP_RG_SUMMHAZ3_Q5.findViewWithTag(GCH_INOPERATIVE)).setChecked(true);
					 else
						 ((RadioButton) PP_RG_SUMMHAZ3_Q5.findViewWithTag("No")).setChecked(true);
					 
					
					
					 if(!GCH_RECDRYWALL.trim().equals("")) 
						 ((RadioButton) PP_RG_SUMMHAZ3_Q6.findViewWithTag(GCH_RECDRYWALL)).setChecked(true);
					 else
						 ((RadioButton) PP_RG_SUMMHAZ3_Q6.findViewWithTag("Beyond Scope of Inspection")).setChecked(true);
					 
					 System.out.println("GCH_CHINESEDRYWALL ="+GCH_CHINESEDRYWALL);
					 if(!GCH_CHINESEDRYWALL.trim().equals("")) 
					 	((RadioButton) PP_RG_SUMMHAZ3_Q7.findViewWithTag(GCH_CHINESEDRYWALL)).setChecked(true);
					 else
						 ((RadioButton) PP_RG_SUMMHAZ3_Q7.findViewWithTag("Beyond Scope of Inspection")).setChecked(true);
					 if(!GCH_CONFDRYWALL.trim().equals("")) 
					 	((RadioButton) PP_RG_SUMMHAZ3_Q8.findViewWithTag(GCH_CONFDRYWALL)).setChecked(true);
					 else
						 ((RadioButton) PP_RG_SUMMHAZ3_Q8.findViewWithTag("Beyond Scope of Inspection")).setChecked(true);
					 if(!GCH_NONSECURITY.trim().equals("")) 
					 	((RadioButton) PP_RG_SUMMHAZ3_Q9.findViewWithTag(GCH_NONSECURITY)).setChecked(true);
					 else
						 ((RadioButton) PP_RG_SUMMHAZ3_Q9.findViewWithTag("Beyond Scope of Inspection")).setChecked(true);
					 if(!GCH_NONSMOKE.trim().equals("")) 
					 	((RadioButton) PP_RG_SUMMHAZ3_Q10.findViewWithTag(GCH_NONSMOKE)).setChecked(true);
					 else
						 ((RadioButton) PP_RG_SUMMHAZ3_Q10.findViewWithTag("Beyond Scope of Inspection")).setChecked(true);
				/*	if(GCH_RECDRYWALL.trim().equals("No"))
					{
						SH_rw20_lin.setVisibility(View.VISIBLE);
						SH_rw20_ed.setText(GCH_RECDRYWALL_BSI);
					}
					if(GCH_CHINESEDRYWALL.trim().equals("No"))
					{
						SH_rw21_lin.setVisibility(View.VISIBLE);
						SH_rw21_ed.setText(GCH_CHINESEDRYWALL_BSI);
					}
					if(GCH_CONFDRYWALL.trim().equals("No"))
					{
						SH_rw22_lin.setVisibility(View.VISIBLE);
						SH_rw22_ed.setText(GCH_CONFDRYWALL_BSI);
					}
					if(GCH_NONSECURITY.trim().equals("No"))
					{
						SH_rw23_lin.setVisibility(View.VISIBLE);
						SH_rw23_ed.setText(GCH_NONSECURITY_BSI);
					}
					if(GCH_NONSMOKE.trim().equals("No"))
					{
						SH_rw24_lin.setVisibility(View.VISIBLE);
						SH_rw24_ed.setText(GCH_NONSMOKE_BSI);
					}
				*/		
					if(GHC_VICIOUS.equals("Yes")){
						/*etvicious.setFocusableInTouchMode(true);
						etvicious.requestFocus();
						etvicious.setCursorVisible(true);
						*/
						
						vicious_lin.setVisibility(v1.VISIBLE); 
						etvicious.setText(GCH_VICIOUSDESC);
						((TextView) findViewById(R.id.GCH_S_D1)).setVisibility(View.INVISIBLE); }
					else {	vicious_lin.setVisibility(v1.GONE);} 
					if(GCH_LIVESTOCK.equals("Yes")){
						/*ethorses.setFocusableInTouchMode(true);
						ethorses.requestFocus();
						ethorses.setCursorVisible(true);*/
						horses_lin.setVisibility(v1.VISIBLE); 
						ethorses.setText(GCH_LIVESTOCKDESC);
						((TextView) findViewById(R.id.GCH_S_D2)).setVisibility(View.INVISIBLE);}
					
					else{	horses_lin.setVisibility(v1.GONE); }
					if(GCH_BUISPREMISES.equals("Yes")){
						/*etsummbuisprem.setFocusableInTouchMode(true);
						etsummbuisprem.requestFocus();
						etsummbuisprem.setCursorVisible(true);
						*/
						business_lin.setVisibility(v1.VISIBLE);  etsummbuisprem.setText(GCH_BUISPREMDESC);
						((TextView) findViewById(R.id.GCH_S_D3)).setVisibility(View.INVISIBLE);}
					else{business_lin.setVisibility(v1.GONE); }
					
					
					if(!"".equals(GCH_TRIPHAZARDNOTES)){ettrpihazards.setText(GCH_TRIPHAZARDNOTES);}
					HZ_comments.setText(GCH_COMMENTS);
				 }	
				 else
				 {
					 
					 System.out.println("no moer issues comes in the correct part ");
					 	((RadioButton) PP_RGPPS.findViewWithTag("No")).setChecked(true);
					 	
					 	PP_TR_hottub.setVisibility(View.GONE);
					 	hottubcoveredtr.setVisibility(View.GONE);
					 	
					 	((RadioButton) PP_RGRP1.findViewWithTag("No")).setChecked(true);
					 	((RadioButton) PP_RGRP2.findViewWithTag("No")).setChecked(true);
					 	((RadioButton) PP_RGRP3.findViewWithTag("No")).setChecked(true);
					 	((RadioButton) PP_RGRP4.findViewWithTag("No")).setChecked(true);
					 	((RadioButton) PP_RGRP5.findViewWithTag("No")).setChecked(true);
					 	((RadioButton) PP_RGRP6.findViewWithTag("No")).setChecked(true);
					 	((RadioButton) PP_RGRP7.findViewWithTag("No")).setChecked(true);
					 	
					 	
					    ((RadioButton) PF_RG_SELFLATCHES.findViewWithTag("No")).setChecked(true);
						((RadioButton) PF_RG_PROFINST.findViewWithTag("No")).setChecked(true);
						((RadioButton) PF_RG_POOLFENCE.findViewWithTag("No")).setChecked(true);
					 	
					 
					    ((RadioButton) PP_RG_SUMMHAZ1_Q1.findViewWithTag("No")).setChecked(true);
						((RadioButton) PP_RG_SUMMHAZ1_Q2.findViewWithTag("No")).setChecked(true);
						((RadioButton) PP_RG_SUMMHAZ1_Q3.findViewWithTag("No")).setChecked(true);
						((RadioButton) PP_RG_SUMMHAZ1_Q4.findViewWithTag("No")).setChecked(true);
						((RadioButton) PP_RG_SUMMHAZ1_Q5.findViewWithTag("No")).setChecked(true);
						((RadioButton) PP_RG_SUMMHAZ1_Q6.findViewWithTag("No")).setChecked(true);
						((RadioButton) PP_RG_SUMMHAZ2_Q1.findViewWithTag("No")).setChecked(true);
						((RadioButton) PP_RG_SUMMHAZ2_Q2.findViewWithTag("No")).setChecked(true);
						((RadioButton) PP_RG_SUMMHAZ2_Q3.findViewWithTag("No")).setChecked(true);
						((RadioButton) PP_RG_SUMMHAZ2_Q4.findViewWithTag("No")).setChecked(true);
						((RadioButton) PP_RG_SUMMHAZ2_Q5.findViewWithTag("No")).setChecked(true);
						((RadioButton) PP_RG_SUMMHAZ2_Q6.findViewWithTag("No")).setChecked(true);
						((RadioButton) PP_RG_SUMMHAZ2_Q7.findViewWithTag("No")).setChecked(true);
						
						
						((RadioButton) PP_RG_SUMMHAZ3_Q1.findViewWithTag("No")).setChecked(true);
						((RadioButton) PP_RG_SUMMHAZ3_Q2.findViewWithTag("No")).setChecked(true);
						((RadioButton) PP_RG_SUMMHAZ3_Q3.findViewWithTag("No")).setChecked(true);
						((RadioButton) PP_RG_SUMMHAZ3_Q4.findViewWithTag("No")).setChecked(true);
						((RadioButton) PP_RG_SUMMHAZ3_Q5.findViewWithTag("No")).setChecked(true);
						
						((RadioButton) PP_RG_SUMMHAZ3_Q6.findViewWithTag("Beyond Scope of Inspection")).setChecked(true);
						((RadioButton) PP_RG_SUMMHAZ3_Q7.findViewWithTag("Beyond Scope of Inspection")).setChecked(true);
						((RadioButton) PP_RG_SUMMHAZ3_Q8.findViewWithTag("Beyond Scope of Inspection")).setChecked(true);
						((RadioButton) PP_RG_SUMMHAZ3_Q9.findViewWithTag("Beyond Scope of Inspection")).setChecked(true);
						((RadioButton) PP_RG_SUMMHAZ3_Q10.findViewWithTag("Beyond Scope of Inspection")).setChecked(true);
						
						/*SH_rw20_lin.setVisibility(View.VISIBLE);
						SH_rw20_ed.setText("Beyond scope of inspection");
						SH_rw21_lin.setVisibility(View.VISIBLE);
						SH_rw21_ed.setText("Beyond scope of inspection");
						SH_rw22_lin.setVisibility(View.VISIBLE);
						SH_rw22_ed.setText("Beyond scope of inspection");
						SH_rw23_lin.setVisibility(View.VISIBLE);
						SH_rw23_ed.setText("Beyond scope of inspection");
						SH_rw24_lin.setVisibility(View.VISIBLE);
						SH_rw24_ed.setText("Beyond scope of inspection");
						*/
				 }
			}
		catch (Exception E)
		{
			System.out.println("EC "+E.getMessage());
			String strerrorlog="Selection of the Building information   table not working ";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Select tabele at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
		private void savevalidation()
	{
			 
		 HZ_commentsval= HZ_comments.getText().toString();
		if(savevalidation_poolpresents())
		{
			if(savevalidation_poolfence())
			{
				if(savevalidation_SummeryHazards())
				{
					if(!HZ_commentsval.trim().equals(""))
					{
						InsertValues();
					}
					else
					{
						cf.ShowToast("Please enter the General Condition Comments", 0);
					}
					
				}
				
			}
			
		}
			
		 
		 		 		
	}
	
	private boolean savevalidation_poolfence() {
		// TODO Auto-generated method stub
		if(ISPermiterpoolPresent.isChecked())
		{
			perimeterclear();
			return true;
		}else{
			
			
		 PF_RG_SELFLATCHES_ID =PF_RG_SELFLATCHES.getCheckedRadioButtonId();
		 PF_RG_PROFINST_ID = PF_RG_PROFINST.getCheckedRadioButtonId();
		 PF_RG_POOLFENCE_ID =PF_RG_POOLFENCE.getCheckedRadioButtonId();
		 System.out.println("PF_RG_POOLFENCE_ID"+PF_RG_POOLFENCE_ID);
		 
		  GHC_PPFECNCE_VAL=cf.getselected_chk(HZ_PERI_opt);	
		  
		  PF_RG_SELFLATCHES_val = (PF_RG_SELFLATCHES_ID==-1)? "":((RadioButton) findViewById(PF_RG_SELFLATCHES_ID)).getText().toString();
		  PF_RG_PROFINST_val = (PF_RG_PROFINST_ID==-1)? "":((RadioButton) findViewById(PF_RG_PROFINST_ID)).getText().toString();
		  PF_RG_POOLFENCE_val = (PF_RG_POOLFENCE_ID==-1)? "":((RadioButton) findViewById(PF_RG_POOLFENCE_ID)).getText().toString();
		  System.out.println("PF_RG_POOLFENCE_val"+PF_RG_POOLFENCE_val);
		  PF_HZ_OTHERPERI = etpoolfenceother.getText().toString();
		  //(HZ_PERI_opt[6].isChecked())? ((etpoolfenceother.getText().toString().equals(""))? false:true):true;
		  if(!GHC_PPFECNCE_VAL.equals(""))
		  {
			if((HZ_PERI_opt[6].isChecked() && !etpoolfenceother.getText().toString().trim().equals("")) || !HZ_PERI_opt[6].isChecked() )
			{
				if(!PF_RG_SELFLATCHES_val.trim().equals(""))
				{
					if(!PF_RG_PROFINST_val.trim().equals(""))
					{
						if(!PF_RG_POOLFENCE_val.trim().equals(""))
						{
							return true;
						}
						else
						 {
							 cf.ShowToast("Please select Pool Fence in disrepair under Perimeter Pool Fence", 0);
							 return false;
						 }
						
					}
					else
					 {
						 cf.ShowToast("Please select Self Professionally installed under Perimeter Pool Fence", 0);
						 return false;
					 } 
					
				}
				else
				 {
					 cf.ShowToast("Please select Self Latahces under Perimeter Pool Fence", 0);
					 return false;
				 } 
			}
			else
			{
				 cf.ShowToast("Please Enter the Other text of Perimeter Pool Fence  under Perimeter Pool Fence", 0);
				 return false;
			}
		  }
		  else
		  {
			  cf.ShowToast("Please Select the  Perimeter Pool Fence", 0);
			  return false;
		  }
			  
		  
	 
	}

		
	}
	private boolean savevalidation_SummeryHazards() {
		// TODO Auto-generated method stub
		
		 PP_RG_SUMMHAZ1_Q1_ID=PP_RG_SUMMHAZ1_Q1.getCheckedRadioButtonId();
	     PP_RG_SUMMHAZ1_Q2_ID=PP_RG_SUMMHAZ1_Q2.getCheckedRadioButtonId();
	     PP_RG_SUMMHAZ1_Q3_ID=PP_RG_SUMMHAZ1_Q3.getCheckedRadioButtonId();
	     PP_RG_SUMMHAZ1_Q4_ID=PP_RG_SUMMHAZ1_Q4.getCheckedRadioButtonId();
	     PP_RG_SUMMHAZ1_Q5_ID=PP_RG_SUMMHAZ1_Q5.getCheckedRadioButtonId();
	     PP_RG_SUMMHAZ1_Q6_ID=PP_RG_SUMMHAZ1_Q6.getCheckedRadioButtonId();
		 
    	PP_RG_SUMMHAZ2_Q1_ID=PP_RG_SUMMHAZ2_Q1.getCheckedRadioButtonId();
	    PP_RG_SUMMHAZ2_Q2_ID=PP_RG_SUMMHAZ2_Q2.getCheckedRadioButtonId();
	    PP_RG_SUMMHAZ2_Q3_ID=PP_RG_SUMMHAZ2_Q3.getCheckedRadioButtonId();
	    PP_RG_SUMMHAZ2_Q4_ID=PP_RG_SUMMHAZ2_Q4.getCheckedRadioButtonId();
	    PP_RG_SUMMHAZ2_Q5_ID=PP_RG_SUMMHAZ2_Q5.getCheckedRadioButtonId();
	    PP_RG_SUMMHAZ2_Q6_ID=PP_RG_SUMMHAZ2_Q6.getCheckedRadioButtonId();
	    PP_RG_SUMMHAZ2_Q7_ID=PP_RG_SUMMHAZ2_Q7.getCheckedRadioButtonId();
	    
	    PP_RG_SUMMHAZ3_Q1_ID=PP_RG_SUMMHAZ3_Q1.getCheckedRadioButtonId();
	    PP_RG_SUMMHAZ3_Q2_ID=PP_RG_SUMMHAZ3_Q2.getCheckedRadioButtonId();
	    PP_RG_SUMMHAZ3_Q3_ID=PP_RG_SUMMHAZ3_Q3.getCheckedRadioButtonId();
	    PP_RG_SUMMHAZ3_Q4_ID=PP_RG_SUMMHAZ3_Q4.getCheckedRadioButtonId();
	    PP_RG_SUMMHAZ3_Q5_ID=PP_RG_SUMMHAZ3_Q5.getCheckedRadioButtonId();
	    PP_RG_SUMMHAZ3_Q6_ID=PP_RG_SUMMHAZ3_Q6.getCheckedRadioButtonId();
	    PP_RG_SUMMHAZ3_Q7_ID=PP_RG_SUMMHAZ3_Q7.getCheckedRadioButtonId();
	    
	    PP_RG_SUMMHAZ3_Q8_ID=PP_RG_SUMMHAZ3_Q8.getCheckedRadioButtonId();
	    PP_RG_SUMMHAZ3_Q9_ID=PP_RG_SUMMHAZ3_Q9.getCheckedRadioButtonId();
	    PP_RG_SUMMHAZ3_Q10_ID=PP_RG_SUMMHAZ3_Q10.getCheckedRadioButtonId();
	    PF_RG_TRIPHAZ_ID=PF_RG_TRIPHAZ.getCheckedRadioButtonId(); 
		PF_RG_TRIPHAZ_val = (PF_RG_TRIPHAZ_ID==-1)? "":((RadioButton) findViewById(PF_RG_TRIPHAZ_ID)).getText().toString();
		  
		 PP_RG_SUMMHAZ1_Q1_val=(PP_RG_SUMMHAZ1_Q1_ID==-1) ? "":((RadioButton) findViewById(PP_RG_SUMMHAZ1_Q1_ID)).getText().toString();
	     PP_RG_SUMMHAZ1_Q2_val=(PP_RG_SUMMHAZ1_Q2_ID==-1) ? "":((RadioButton) findViewById(PP_RG_SUMMHAZ1_Q2_ID)).getText().toString();
	     PP_RG_SUMMHAZ1_Q3_val=(PP_RG_SUMMHAZ1_Q3_ID==-1) ? "":((RadioButton) findViewById(PP_RG_SUMMHAZ1_Q3_ID)).getText().toString();
	     PP_RG_SUMMHAZ1_Q4_val=(PP_RG_SUMMHAZ1_Q4_ID==-1) ? "":((RadioButton) findViewById(PP_RG_SUMMHAZ1_Q4_ID)).getText().toString();
	     PP_RG_SUMMHAZ1_Q5_val=(PP_RG_SUMMHAZ1_Q5_ID==-1) ? "":((RadioButton) findViewById(PP_RG_SUMMHAZ1_Q5_ID)).getText().toString();
	     PP_RG_SUMMHAZ1_Q6_val=(PP_RG_SUMMHAZ1_Q6_ID==-1) ? "":((RadioButton) findViewById(PP_RG_SUMMHAZ1_Q6_ID)).getText().toString();
    	PP_RG_SUMMHAZ2_Q1_val=(PP_RG_SUMMHAZ2_Q1_ID==-1) ? "":((RadioButton) findViewById(PP_RG_SUMMHAZ2_Q1_ID)).getText().toString();
	    PP_RG_SUMMHAZ2_Q2_val=(PP_RG_SUMMHAZ2_Q2_ID==-1) ? "":((RadioButton) findViewById(PP_RG_SUMMHAZ2_Q2_ID)).getText().toString();
	    PP_RG_SUMMHAZ2_Q3_val=(PP_RG_SUMMHAZ2_Q3_ID==-1) ? "":((RadioButton) findViewById(PP_RG_SUMMHAZ2_Q3_ID)).getText().toString();
	    PP_RG_SUMMHAZ2_Q4_val=(PP_RG_SUMMHAZ2_Q4_ID==-1) ? "":((RadioButton) findViewById(PP_RG_SUMMHAZ2_Q4_ID)).getText().toString();
	    PP_RG_SUMMHAZ2_Q5_val=(PP_RG_SUMMHAZ2_Q5_ID==-1) ? "":((RadioButton) findViewById(PP_RG_SUMMHAZ2_Q5_ID)).getText().toString();
	    PP_RG_SUMMHAZ2_Q6_val=(PP_RG_SUMMHAZ2_Q6_ID==-1) ? "":((RadioButton) findViewById(PP_RG_SUMMHAZ2_Q6_ID)).getText().toString();
	    PP_RG_SUMMHAZ2_Q7_val=(PP_RG_SUMMHAZ2_Q7_ID==-1) ? "":((RadioButton) findViewById(PP_RG_SUMMHAZ2_Q7_ID)).getText().toString();
	    PP_RG_SUMMHAZ3_Q1_val=(PP_RG_SUMMHAZ3_Q1_ID==-1) ? "":((RadioButton) findViewById(PP_RG_SUMMHAZ3_Q1_ID)).getText().toString();
	    PP_RG_SUMMHAZ3_Q2_val=(PP_RG_SUMMHAZ3_Q2_ID==-1) ? "":((RadioButton) findViewById(PP_RG_SUMMHAZ3_Q2_ID)).getText().toString();
	    PP_RG_SUMMHAZ3_Q3_val=(PP_RG_SUMMHAZ3_Q3_ID==-1) ? "":((RadioButton) findViewById(PP_RG_SUMMHAZ3_Q3_ID)).getText().toString();
	    PP_RG_SUMMHAZ3_Q4_val=(PP_RG_SUMMHAZ3_Q4_ID==-1) ? "":((RadioButton) findViewById(PP_RG_SUMMHAZ3_Q4_ID)).getText().toString();
	    PP_RG_SUMMHAZ3_Q5_val=(PP_RG_SUMMHAZ3_Q5_ID==-1) ? "":((RadioButton) findViewById(PP_RG_SUMMHAZ3_Q5_ID)).getText().toString();
	    PP_RG_SUMMHAZ3_Q6_val=(PP_RG_SUMMHAZ3_Q6_ID==-1) ? "":((RadioButton) findViewById(PP_RG_SUMMHAZ3_Q6_ID)).getText().toString();
	    PP_RG_SUMMHAZ3_Q7_val=(PP_RG_SUMMHAZ3_Q7_ID==-1) ? "":((RadioButton) findViewById(PP_RG_SUMMHAZ3_Q7_ID)).getText().toString();
	    PP_RG_SUMMHAZ3_Q8_val=(PP_RG_SUMMHAZ3_Q8_ID==-1) ? "":((RadioButton) findViewById(PP_RG_SUMMHAZ3_Q8_ID)).getText().toString();
	    PP_RG_SUMMHAZ3_Q9_val=(PP_RG_SUMMHAZ3_Q9_ID==-1) ? "":((RadioButton) findViewById(PP_RG_SUMMHAZ3_Q9_ID)).getText().toString();
	    PP_RG_SUMMHAZ3_Q10_val=(PP_RG_SUMMHAZ3_Q10_ID==-1) ? "":((RadioButton) findViewById(PP_RG_SUMMHAZ3_Q10_ID)).getText().toString();
		 
	 
		 HZ_VICIOUSDESCVAL = etvicious.getText().toString();
		 HZ_HORSESDESCVAL = ethorses.getText().toString();
		 HZ_TRIPHAZARDNOTES = ettrpihazards.getText().toString();
		
		 
		 HZ_BUISNPREM_DESC=  etsummbuisprem.getText().toString();
		 
		 b[0]=(HZ_SUMM_VICIOUS_YES.isChecked())? ((etvicious.getText().toString().equals(""))? false:true):true;
		 b[1]=(HZ_SUMM_HORSES_YES.isChecked())? ((ethorses.getText().toString().equals(""))? false:true):true;
		 b[2]=(!PF_RG_TRIPHAZ_val.equals("-1"))? ((ettrpihazards.getText().toString().equals("") && !PF_RG_TRIPHAZ_val.equals("None Observed") )? false:true):true;
		 b[3]=(!PF_RG_TRIPHAZ_val.equals("-1"))? ((ettrpihazards.getText().toString().equals("") && !PF_RG_TRIPHAZ_val.equals("None Observed"))? false:true):true;
		 b[4]=(HZ_SUMM_BUISPREM_YES.isChecked())? ((etsummbuisprem.getText().toString().equals(""))? false:true):true;
		 
		/* b[5]=(PP_RG_SUMMHAZ3_Q6_val.equals("No"))? ((SH_rw20_ed.getText().toString().trim().equals(""))? false:true):true;
		 b[6]=(PP_RG_SUMMHAZ3_Q7_val.equals("No"))? ((SH_rw21_ed.getText().toString().trim().equals(""))? false:true):true;
		 b[7]=(PP_RG_SUMMHAZ3_Q8_val.equals("No"))? ((SH_rw22_ed.getText().toString().trim().equals(""))? false:true):true;
		 b[8]=(PP_RG_SUMMHAZ3_Q9_val.equals("No"))? ((SH_rw23_ed.getText().toString().trim().equals(""))? false:true):true;
		 b[9]=(PP_RG_SUMMHAZ3_Q10_val.equals("No"))? ((SH_rw24_ed.getText().toString().trim().equals(""))? false:true):true;
		*/ 
		 //HZ_commentsval= HZ_comments.getText().toString();

		 if(!"".equals(PP_RG_SUMMHAZ1_Q1_val))
		 {
			 if(b[0])
			 {
			 
			 if(!"".equals(PP_RG_SUMMHAZ1_Q2_val))
			 {
				 if(b[1])
				 {
				 
				 if(!"".equals(PP_RG_SUMMHAZ1_Q3_val))
				 {
					 if(!"".equals(PP_RG_SUMMHAZ1_Q4_val))
					 {
						 if(!"".equals(PP_RG_SUMMHAZ1_Q5_val))
						 {
							 if(!"".equals(PP_RG_SUMMHAZ1_Q6_val))
							 {
								if(!"".equals(PF_RG_TRIPHAZ_val))
								{
									if(b[2])
									{
										if(!"".equals(PP_RG_SUMMHAZ2_Q1_val))
										{	
											if(!"".equals(PP_RG_SUMMHAZ2_Q2_val))
											{	
												if(!"".equals(PP_RG_SUMMHAZ2_Q3_val))
												{
													if(!"".equals(PP_RG_SUMMHAZ2_Q4_val))
													{
														if(!"".equals(PP_RG_SUMMHAZ2_Q5_val))
														{
															if(!"".equals(PP_RG_SUMMHAZ2_Q6_val))
															{
																if(!"".equals(PP_RG_SUMMHAZ2_Q7_val))
																{
																	if(!"".equals(PP_RG_SUMMHAZ3_Q1_val))
																	{
																		if(b[4])
																		{
																		if(!"".equals(PP_RG_SUMMHAZ3_Q2_val))
																		{
																			if(!"".equals(PP_RG_SUMMHAZ3_Q3_val))
																			{
																				if(!"".equals(PP_RG_SUMMHAZ3_Q4_val))
																				{
																					if(!"".equals(PP_RG_SUMMHAZ3_Q5_val))
																					{
																						if(!"".equals(PP_RG_SUMMHAZ3_Q6_val))
																						{
																							if(!"".equals(PP_RG_SUMMHAZ3_Q7_val))
																							{
																								if(!"".equals(PP_RG_SUMMHAZ3_Q8_val))
																								{
																									if(!"".equals(PP_RG_SUMMHAZ3_Q9_val))
																									{
																										if(!"".equals(PP_RG_SUMMHAZ3_Q10_val))
																										{
																												
																													return true;	
																												
																											/*}
																											else
																											{
																												if(!b[5])
																												{
																													cf.ShowToast("Please enter comments for the recent drywall repair under summary hazards", 0);
																													return false;
																												}
																												else if(!b[6])
																												{
																													cf.ShowToast("Please enter comments for the possible chinese drywall product under summary hazards", 0);
																													return false;
																												}
																												else if(!b[7])
																												{
																													cf.ShowToast("Please enter comments for the owner confirm chinese drywall product under summary hazards", 0);
																													return false;
																												}
																												else if(!b[8])
																												{
																													cf.ShowToast("Please enter comments for the non working security system under summary hazards", 0);
																													return false;
																												}
																												else if(!b[9])
																												{
																													cf.ShowToast("Please enter comments for the non working smoke dedicator under summary hazards", 0);
																													return false;
																												}
																												return false;
																											}
																											*/
																										}
																										else
																										{
																											cf.ShowToast("Please select Non Working Smoke Dedicator under Summary Hazards", 0);
																											return false;
																										}
																									}
																									else
																									{
																										cf.ShowToast("Please select Non Working Security system under Summary Hazards", 0);
																										return false;
																									}
																								}
																								else
																								{
																									cf.ShowToast("Please select Owner Confirm chinese drywall product under Summary Hazards", 0);
																									return false;
																								}
																							}
																							else
																							{
																								cf.ShowToast("Please select Possible Chinese Drywall product under Summary Hazards", 0);
																								return false;
																							}
																							
																						}
																						else
																						{
																							cf.ShowToast("Please select Recent Drywall Repair under Summary Hazards", 0);
																							return false;
																						}
																					}
																					else
																					{
																						cf.ShowToast("Please select Inoperative/Non-starting Motors Vehicle noted under Summary Hazards", 0);
																						return false;
																					}
																				}
																				else
																				{
																					cf.ShowToast("Please select Structure Partially under Summary Hazards", 0);
																					return false;
																					
																				}
																			}
																			else
																			{
																				cf.ShowToast("Please select Property Damage Noted under Summary Hazards", 0);
																				return false;
																			}
																		}
																		else
																		{
																			cf.ShowToast("Please select Buildnig in General Disrepair under Summary Hazards", 0);
																			return false;
																		}
																	}
																	else
																	{
																		cf.ShowToast("Please enter text for Business on Premises under Summary Hazards", 0);
																		return false;
																	}
																	}
																	else
																	{
																		cf.ShowToast("Please select Business on Premises under Summary Hazards", 0);
																		return false;
																	}
																}
																else
																{
																	cf.ShowToast("Please select Excess Debris under Summary Hazards", 0);
																	return false;
																}
															}
															else
															{
																cf.ShowToast("Please select Wood Singhled Roofs under Summary Hazards", 0);
																return false;
															}
														}
														else
														{
															cf.ShowToast("Please select Open Foundation Present under Summary Hazards", 0);
															return false;
														}
													}
													else
													{
														cf.ShowToast("Please select Outdoor Applicances Noted under Summary Hazards", 0);
														return false;
													}
												}
												else
												{
													cf.ShowToast("Please select Non Standard Construction under Summary Hazards", 0);
													return false;
												}
											}
											else
											{
												cf.ShowToast("Please select Porch Deck under Summary Hazards", 0);
												return false;
											}
										}
										else
										{
											cf.ShowToast("Please select Unsafe Stairway noted under Summary Hazards", 0);
											return false;
										}
									
										
									}
									else
									{
										cf.ShowToast("Please enter text for Trip Hazards noted under Summary Hazards", 0);
										return false;
									}
								}
								else
								{
									cf.ShowToast("Please select Trip Hazards noted under Summary Hazards", 0);
									return false;
								}
							 }
							 else
							 {
								 cf.ShowToast("Please select Bicycle Ramp Pressnt under Summary Hazards", 0);
								 return false;
							 } 
						 }
						 else
						 {
							 cf.ShowToast("Please select Skateboard Ramp Present under Summary Hazards", 0);	
							 return false;
						 } 
					 }
					 else
					 {
						 cf.ShowToast("Please select Trampoline Present under Summary Hazards", 0);	
						 return false;
					 } 
				 }
				 else
				 {
					 cf.ShowToast("Please select Over Hanging under Summary Hazards", 0);
					 return false;
				 } 
			 }
			 else
			 {
				 cf.ShowToast("Please enter the text for Horses Livestock under Summary Hazards", 0);
				 return false;
				 
			 } 
			 }
			 else
			 {
				 cf.ShowToast("Please select Horses/Livestock under Summary Hazards", 0);	
				 return false;
			 }
		 }
		 else
		 {
			 cf.ShowToast("Please enter the text for Vicious under Summary Hazards", 0);
			 return false;
		 } 
			 
		 }
		 else
		 {

			 cf.ShowToast("Please select Vicious under Summary Hazards", 0);
			 return false;
		 }	 
	 
		 
		
	}
	private boolean savevalidation_poolpresents()
	{
		if(ISPoolPresent.isChecked())
		{
			clear();
			 return true;
		}else{
		 PP_RG_GROUNDLEVEL_ID=PP_RG_GROUNDLEVEL.getCheckedRadioButtonId();	
		 PP_RGRP1_ID = PP_RGRP1.getCheckedRadioButtonId();
		 PP_RGPPS_ID = PP_RGPPS.getCheckedRadioButtonId();
		 PP_RGRP2_ID = PP_RGRP2.getCheckedRadioButtonId();
		 PP_RGRP3_ID = PP_RGRP3.getCheckedRadioButtonId();
		 PP_RGRP4_ID = PP_RGRP4.getCheckedRadioButtonId();
		 PP_RGRP5_ID = PP_RGRP5.getCheckedRadioButtonId();
		 PP_RGRP6_ID = PP_RGRP6.getCheckedRadioButtonId();
		 PP_RGRP7_ID = PP_RGRP7.getCheckedRadioButtonId();
		 
		  
	     PP_RG_GROUNDLEVEL_val = (PP_RG_GROUNDLEVEL_ID==-1)? "":((RadioButton) findViewById(PP_RG_GROUNDLEVEL_ID)).getText().toString();
		 PP_RGRP1_val = (PP_RGRP1_ID==-1)? "":((RadioButton) findViewById(PP_RGRP1_ID)).getText().toString();
		 PP_RGPPS_val = (PP_RGPPS_ID==-1)? "":((RadioButton) findViewById(PP_RGPPS_ID)).getText().toString();
		 PP_RGRP2_val = (PP_RGRP2_ID==-1)? "":((RadioButton) findViewById(PP_RGRP2_ID)).getText().toString();
		 PP_RGRP3_val = (PP_RGRP3_ID==-1)? "":((RadioButton) findViewById(PP_RGRP3_ID)).getText().toString();
		 PP_RGRP4_val = (PP_RGRP4_ID==-1)? "":((RadioButton) findViewById(PP_RGRP4_ID)).getText().toString();
		 PP_RGRP5_val = (PP_RGRP5_ID==-1)? "":((RadioButton) findViewById(PP_RGRP5_ID)).getText().toString();
		 PP_RGRP6_val = (PP_RGRP6_ID==-1)? "":((RadioButton) findViewById(PP_RGRP6_ID)).getText().toString();
		 PP_RGRP7_val = (PP_RGRP7_ID==-1)? "":((RadioButton) findViewById(PP_RGRP7_ID)).getText().toString();
		
		 if(ISPoolpresentval==0 || ISPoolPresent.isChecked()==true) /**Check if the No check box selected **/
		 {
			 return true;
		 }
		 else
		 {
			 if(!PP_RG_GROUNDLEVEL_val.trim().equals(""))
			 {
				 if(!PP_RGPPS_val.trim().equals(""))
				 {
					if((PP_RGPPS_val.trim().equals("Yes") && !PP_RGRP1_val.trim().equals("")) || PP_RGPPS_val.trim().equals("No"))
					{
						if( !PP_RGRP2_val.trim().equals(""))
						{
							return true;
						}
						else
						{
							 cf.ShowToast("Please select Empty in Ground Present", 0);
							 return false;
						}
					}
					else
					{
						 cf.ShowToast("Please select Hot Tub Present", 0);
						 return false;
					}
				 }
				 else
				 {
					 cf.ShowToast("Please select Part of Pool Structure ", 0);
					 return false;
				 }
				
			 }
			 else
			 {
				 cf.ShowToast("Please select Swimming Pool Present ", 0);
				 return false; 
			 }
			
		 }
			
		}
		
	}
	private void InsertValues()
    {
		if(ISPoolPresent.isChecked()==true){ISPoolpresentval=0;}else if(ISPoolPresent.isChecked()==false){ISPoolpresentval=1;}
		if(ISPermiterpoolPresent.isChecked()==true){Isperimeterpoolfenceval=0;}else if(ISPermiterpoolPresent.isChecked()==false){Isperimeterpoolfenceval=1;}
	cf.Create_Table(7);
	Cursor BI_save=null;
	try
	{
		 BI_save=cf.SelectTablefunction(cf.GHC_table, " where GCH_HZ_Homeid='"+cf.encode(cf.selectedhomeid)+"'");
		 /**Get the value for the Beyond the scope of inspection **/
		 GCH_RECDRYWALL_BSI="";//SH_rw20_ed.getText().toString().trim();
		 GCH_CHINESEDRYWALL_BSI="";//=SH_rw21_ed.getText().toString().trim();
		 GCH_CONFDRYWALL_BSI="";//=SH_rw22_ed.getText().toString().trim();
		 GCH_NONSECURITY_BSI="";//=SH_rw23_ed.getText().toString().trim();
		 GCH_NONSMOKE_BSI="";//=SH_rw24_ed.getText().toString().trim();
		 /**Get the value for the Beyond the scope of inspection Ends **/
		 if(BI_save.getCount()>0)
			{
				try
				{
					cf.gch_db.execSQL("UPDATE "+cf.GHC_table+ " set GCH_HZ_Inspectorid='"+cf.Insp_id+"',GCH_HZ_Homeid='"+cf.selectedhomeid+"',GCH_HZ_ISPOOL_PRES='"+ISPoolpresentval+"'," +
							"GCH_HZ_POOLPRES='"+cf.encode(PP_RG_GROUNDLEVEL_val)+"',GCH_HZ_HOTTUB='"+cf.encode(PP_RGRP1_val)+"',GCH_HZ_PPS='"+cf.encode(PP_RGPPS_val)+"',GCH_HZ_HOTTUBCOVER='"+cf.encode(PP_RGRP3_val)+"'," +
							"GCH_HZ_POOLSLIDE='"+cf.encode(PP_RGRP4_val)+"',GCH_HZ_DIVINGBOARD='"+cf.encode(PP_RGRP5_val)+"',GCH_HZ_PERIPOOL='"+cf.encode(PP_RGRP6_val)+"',GCH_HZ_EMPTYPOOLPRESENT='"+cf.encode(PP_RGRP2_val)+"',GCH_HZ_ISPERI_PF='"+Isperimeterpoolfenceval+"'," +
							"GCH_HZ_POOLENCLOS='"+cf.encode(PP_RGRP7_val)+"',GCH_HZ_PERI_PF='"+cf.encode(GHC_PPFECNCE_VAL)+"',GCH_HZ_OTHERPERI_PF='"+cf.encode(PF_HZ_OTHERPERI)+"'," +
							"GCH_HZ_SELFLATCH='"+cf.encode(PF_RG_SELFLATCHES_val)+"'," +
							"GCH_HZ_PROF_INST='"+cf.encode(PF_RG_PROFINST_val)+"',GCH_HZ_PF_DISREPAIR='"+cf.encode(PF_RG_POOLFENCE_val)+"',GCH_HZ_VICIOUS='"+cf.encode(PP_RG_SUMMHAZ1_Q1_val)+"'," +
							"GCH_HZ_VICIOUSDESC='"+cf.encode(HZ_VICIOUSDESCVAL)+"',GCH_HZ_LIVESTOCK='"+cf.encode(PP_RG_SUMMHAZ1_Q2_val)+"',GCH_HZ_LIVESTOCKDESC='"+cf.encode(HZ_HORSESDESCVAL)+"',GCH_HZ_OVERHANGING='"+cf.encode(PP_RG_SUMMHAZ1_Q3_val)+"'," +
							"GCH_HZ_TRAMPOLINE='"+cf.encode(PP_RG_SUMMHAZ1_Q4_val)+"',GCH_HZ_SKATEBOARD='"+cf.encode(PP_RG_SUMMHAZ1_Q5_val)+"',GCH_HZ_BICYCLE='"+cf.encode(PP_RG_SUMMHAZ1_Q6_val)+"'," +
							"GCH_HZ_GROUNDPOOL='',GCH_HZ_TRIPHAZARD='"+cf.encode(PF_RG_TRIPHAZ_val)+"'," +
							"GCH_HZ_TRIPHAZARDNOTES='"+cf.encode(HZ_TRIPHAZARDNOTES)+"',GCH_HZ_UNSAFESTAIRWAY='"+cf.encode(PP_RG_SUMMHAZ2_Q1_val)+"'," +
							"GCH_HZ_PORCHANDDESK='"+cf.encode(PP_RG_SUMMHAZ2_Q2_val)+"',GCH_HZ_NONSTDCONST='"+cf.encode(PP_RG_SUMMHAZ2_Q3_val)+"',GCH_HZ_OUTDOORAPPL='"+cf.encode(PP_RG_SUMMHAZ2_Q4_val)+"'," +
							"GCH_HZ_OPENFOUND='"+cf.encode(PP_RG_SUMMHAZ2_Q5_val)+"',GCH_HZ_WOODSINGHLED='"+cf.encode(PP_RG_SUMMHAZ2_Q6_val)+"',GCH_HZ_EXCESSDEBRIS='"+cf.encode(PP_RG_SUMMHAZ2_Q7_val)+"',GCH_HZ_BUISPREMISES='"+cf.encode(PP_RG_SUMMHAZ3_Q1_val)+"',GCH_HZ_BUISPREMDESC='"+cf.encode(HZ_BUISNPREM_DESC)+"'," +
							"GCH_HZ_GENDISREPAIR='"+cf.encode(PP_RG_SUMMHAZ3_Q2_val)+"',GCH_HZ_PROP_DAMAGE='"+cf.encode(PP_RG_SUMMHAZ3_Q3_val)+"',GCH_HZ_STRUPARTIAL='"+cf.encode(PP_RG_SUMMHAZ3_Q4_val)+"'," +
							"GCH_HZ_INOPERATIVE='"+cf.encode(PP_RG_SUMMHAZ3_Q5_val)+"',GCH_HZ_RECDRYWALL='"+cf.encode(PP_RG_SUMMHAZ3_Q6_val)+"',GCH_HZ_CHINESEDRYWALL='"+cf.encode(PP_RG_SUMMHAZ3_Q7_val)+"'," +
							"GCH_HZ_CONFDRYWALL='"+cf.encode(PP_RG_SUMMHAZ3_Q8_val)+"',GCH_HZ_NONSECURITY='"+cf.encode(PP_RG_SUMMHAZ3_Q9_val)+"',GCH_HZ_NONSMOKE='"+cf.encode(PP_RG_SUMMHAZ3_Q10_val)+"',GCH_HZ_RECDRYWALL_BSI='"+cf.encode(GCH_RECDRYWALL_BSI)+"',GCH_HZ_CHINESEDRYWALL_BSI='"+cf.encode(GCH_CHINESEDRYWALL_BSI)+"'," +
							"GCH_HZ_CONFDRYWALL_BSI='"+cf.encode(GCH_CONFDRYWALL_BSI)+"',GCH_HZ_NONSECURITY_BSI='"+cf.encode(GCH_NONSECURITY_BSI)+"',GCH_HZ_NONSMOKE_BSI='"+cf.encode(GCH_NONSMOKE_BSI)+"'," +
							"GCH_HZ_COMMENTS='"+cf.encode(HZ_comments.getText().toString())+"' where GCH_HZ_Homeid='"+cf.selectedhomeid+"' ");

					
					cf.ShowToast("General Condition  information has been updated successfully.", 1);
						// move the page to next page // 
						 nextlayout();
						
				}
				catch (Exception E)
				{
					String strerrorlog="update the Builing question  table not working ";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table update at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			}
			else
			{
					
					try
					{
						cf.gch_db.execSQL("INSERT INTO "
								+ cf.GHC_table
								+ " (GCH_HZ_Inspectorid,GCH_HZ_Homeid,GCH_HZ_ISPOOL_PRES,GCH_HZ_POOLPRES,GCH_HZ_HOTTUB,GCH_HZ_PPS,GCH_HZ_HOTTUBCOVER,GCH_HZ_POOLSLIDE,GCH_HZ_DIVINGBOARD,GCH_HZ_PERIPOOL,GCH_HZ_POOLENCLOS,GCH_HZ_EMPTYPOOLPRESENT,GCH_HZ_ISPERI_PF,GCH_HZ_PERI_PF,GCH_HZ_OTHERPERI_PF,GCH_HZ_SELFLATCH,GCH_HZ_PROF_INST,GCH_HZ_PF_DISREPAIR,GCH_HZ_VICIOUS,GCH_HZ_VICIOUSDESC,GCH_HZ_LIVESTOCK,GCH_HZ_LIVESTOCKDESC,GCH_HZ_OVERHANGING,GCH_HZ_TRAMPOLINE,GCH_HZ_SKATEBOARD,GCH_HZ_BICYCLE,GCH_HZ_GROUNDPOOL,GCH_HZ_TRIPHAZARD,GCH_HZ_TRIPHAZARDNOTES,GCH_HZ_UNSAFESTAIRWAY,GCH_HZ_PORCHANDDESK,GCH_HZ_NONSTDCONST,GCH_HZ_OUTDOORAPPL,GCH_HZ_OPENFOUND,GCH_HZ_WOODSINGHLED,GCH_HZ_EXCESSDEBRIS,GCH_HZ_BUISPREMISES,GCH_HZ_BUISPREMDESC,GCH_HZ_GENDISREPAIR,GCH_HZ_PROP_DAMAGE,GCH_HZ_STRUPARTIAL,GCH_HZ_INOPERATIVE,GCH_HZ_RECDRYWALL,GCH_HZ_CHINESEDRYWALL,GCH_HZ_CONFDRYWALL,GCH_HZ_NONSECURITY,GCH_HZ_NONSMOKE,GCH_HZ_RECDRYWALL_BSI,GCH_HZ_CHINESEDRYWALL_BSI,GCH_HZ_CONFDRYWALL_BSI,GCH_HZ_NONSECURITY_BSI,GCH_HZ_NONSMOKE_BSI,GCH_HZ_COMMENTS)"
								+ "VALUES ('"+cf.Insp_id+"','"+cf.selectedhomeid+"','"+ISPoolpresentval+"','"+cf.encode(PP_RG_GROUNDLEVEL_val)+"','"+cf.encode(PP_RGRP1_val)+"','"+cf.encode(PP_RGPPS_val)+"','"+cf.encode(PP_RGRP3_val)+"','"+cf.encode(PP_RGRP4_val)+"','"+cf.encode(PP_RGRP5_val)+"','"+cf.encode(PP_RGRP6_val)+"','"+cf.encode(PP_RGRP7_val)+"','"+cf.encode(PP_RGRP2_val)+"','"+Isperimeterpoolfenceval+"','"+cf.encode(GHC_PPFECNCE_VAL)+"','"+cf.encode(PF_HZ_OTHERPERI)+"','"+cf.encode(PF_RG_SELFLATCHES_val)+"','"+cf.encode(PF_RG_PROFINST_val)+"','"+cf.encode(PF_RG_POOLFENCE_val)+"','"+cf.encode(PP_RG_SUMMHAZ1_Q1_val)+"','"+cf.encode(HZ_VICIOUSDESCVAL)+"','"+cf.encode(PP_RG_SUMMHAZ1_Q2_val)+"','"+cf.encode(HZ_HORSESDESCVAL)+"','"+cf.encode(PP_RG_SUMMHAZ1_Q3_val)+"','"+cf.encode(PP_RG_SUMMHAZ1_Q4_val)+"','"+cf.encode(PP_RG_SUMMHAZ1_Q5_val)+"','"+cf.encode(PP_RG_SUMMHAZ1_Q6_val)+"','','"+cf.encode(PF_RG_TRIPHAZ_val)+"','"+cf.encode(HZ_TRIPHAZARDNOTES)+"','"+cf.encode(PP_RG_SUMMHAZ2_Q1_val)+"','"+cf.encode(PP_RG_SUMMHAZ2_Q2_val)+"','"+cf.encode(PP_RG_SUMMHAZ2_Q3_val)+"','"+cf.encode(PP_RG_SUMMHAZ2_Q4_val)+"','"+cf.encode(PP_RG_SUMMHAZ2_Q5_val)+"','"+cf.encode(PP_RG_SUMMHAZ2_Q6_val)+"','"+cf.encode(PP_RG_SUMMHAZ2_Q7_val)+"','"+cf.encode(PP_RG_SUMMHAZ3_Q1_val)+"','"+cf.encode(HZ_BUISNPREM_DESC)+"','"+cf.encode(PP_RG_SUMMHAZ3_Q2_val)+"','"+cf.encode(PP_RG_SUMMHAZ3_Q3_val)+"','"+cf.encode(PP_RG_SUMMHAZ3_Q4_val)+"','"+cf.encode(PP_RG_SUMMHAZ3_Q5_val)+"','"+cf.encode(PP_RG_SUMMHAZ3_Q6_val)+"','"+cf.encode(PP_RG_SUMMHAZ3_Q7_val)+"','"+cf.encode(PP_RG_SUMMHAZ3_Q8_val)+"','"+cf.encode(PP_RG_SUMMHAZ3_Q9_val)+"','"+cf.encode(PP_RG_SUMMHAZ3_Q10_val)+"','"+cf.encode(GCH_RECDRYWALL_BSI)+"','"+cf.encode(GCH_CHINESEDRYWALL_BSI)+"','"+cf.encode(GCH_CONFDRYWALL_BSI)+"','"+cf.encode(GCH_NONSECURITY_BSI)+"','"+cf.encode(GCH_NONSMOKE_BSI)+"','"+cf.encode(HZ_comments.getText().toString())+"')");

							 cf.ShowToast("General Condition  information has been updated successfully.", 1);
							// move the page to next page // 
							 nextlayout();
							
					}
					catch (Exception E)
					{
						String strerrorlog="Insert the Building  question  table not working ";
						cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
					}
					
				
			}
	}
	catch (Exception E)
	{
		String strerrorlog="Selection of the Building information   table not working ";
		cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Select tabele at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
	}
	
	
}
	private void nextlayout() {
		// TODO Auto-generated method stub
		Intent intimg = new Intent(GeneralHazards.this,
				RoofSection.class);
		cf.putExtras(intimg);;
		//intimg.putExtra("type",51);
	    startActivity(intimg);
		
		
	}
	public void clicker(View v)
	{
		  switch(v.getId())
		  {
		  case R.id.homebtn:
			  cf.gohome();
			  break;
		  case R.id.savebtn:
			  savevalidation();

			  break;
		  case R.id.hottuby:
			  PP_TR_hottub.setVisibility(View.VISIBLE);
			  hottubcoveredtr.setVisibility(visibility);
			  ((RadioButton) PP_RGPPS.findViewWithTag("No")).setChecked(true);
			  ((RadioButton) PP_RGRP3.findViewWithTag("No")).setChecked(true);
			  break;
		  case R.id.hottubn:
			  PP_TR_hottub.setVisibility(View.GONE);
			  hottubcoveredtr.setVisibility(v.GONE);
			  ((RadioButton) PP_RGPPS.findViewWithTag("No")).setChecked(true);
			  ((RadioButton) PP_RGRP3.findViewWithTag("No")).setChecked(true);
			  break;
			 
		  case R.id.PPSy:
			  
			  //hottubcoveredtr.setVisibility(v.VISIBLE);
			  //((RadioButton) PP_RGRP1.findViewWithTag("No")).setChecked(true);
			  
			  break;
		  case R.id.PPSn:
			  
			  //((RadioButton) PP_RGRP1.findViewWithTag("No")).setChecked(true);
			  //hottubcoveredtr.setVisibility(v.GONE);
			  break;
			 
			 
		/*  case R.id.SH_rw20_RD_y:
			  if(SH_rw20_lin.getVisibility()==View.VISIBLE)
			  {
			  SH_rw20_ed.setFocusable(false);
			  SH_rw20_ed.setText("");
			  SH_rw20_lin.setVisibility(v.GONE);
			  }
			 break;
		  case R.id.SH_rw20_RD_n:
			  if(SH_rw20_lin.getVisibility()!=View.VISIBLE)
			  {
			  SH_rw20_ed.setFocusable(true);
			  SH_rw20_ed.setText("Beyond scope of inspection");
			  SH_rw20_ed.requestFocus();
			  SH_rw20_lin.setVisibility(v.VISIBLE);}
			 break;
		  case R.id.SH_rw21_RD_y:
			  if(SH_rw21_lin.getVisibility()==View.VISIBLE)
			  {
			  SH_rw21_ed.setFocusable(false);
			  SH_rw21_ed.setText("");
			  SH_rw21_lin.setVisibility(v.GONE);
			  }
			 break;
		  case R.id.SH_rw21_RD_n:
			  if(SH_rw21_lin.getVisibility()!=View.VISIBLE)
			  {
			  SH_rw21_ed.setFocusable(true);
			  SH_rw21_ed.setText("Beyond scope of inspection");
			  SH_rw21_ed.requestFocus();
			  SH_rw21_lin.setVisibility(v.VISIBLE);
			  }
			 break;
		  case R.id.SH_rw22_RD_y:
			  if(SH_rw22_lin.getVisibility()==View.VISIBLE)
			  {
			  SH_rw22_ed.setFocusable(false);
			  SH_rw22_ed.setText("");
			  SH_rw22_lin.setVisibility(v.GONE);
			  }
			 break;
		  case R.id.SH_rw22_RD_n:
			  if(SH_rw22_lin.getVisibility()!=View.VISIBLE)
			  {
			  SH_rw22_ed.setFocusable(true);
			  SH_rw22_ed.setText("Beyond scope of inspection");
			  SH_rw22_ed.requestFocus();
			  SH_rw22_lin.setVisibility(v.VISIBLE);
			  }
			 break;
		  case R.id.SH_rw23_RD_y:
			  if(SH_rw23_lin.getVisibility()==View.VISIBLE)
			  {
			  SH_rw23_ed.setFocusable(false);
			  SH_rw23_ed.setText("");
			  SH_rw23_lin.setVisibility(v.GONE);
			  }
			 break;
		  case R.id.SH_rw23_RD_n:
			  if(SH_rw23_lin.getVisibility()!=View.VISIBLE)
			  {
			  SH_rw23_ed.setFocusable(true);
			  SH_rw23_ed.setText("Beyond scope of inspection");
			  SH_rw23_ed.requestFocus();
			  SH_rw23_lin.setVisibility(v.VISIBLE);
			  }
			 break;
		  case R.id.SH_rw24_RD_y:
			  if(SH_rw24_lin.getVisibility()==View.VISIBLE)
			  {
			  SH_rw24_ed.setFocusable(false);
			  SH_rw24_ed.setText("");
			  SH_rw24_lin.setVisibility(v.GONE);
			  }
			 break;
		  case R.id.SH_rw24_RD_n:
			  if(SH_rw24_lin.getVisibility()!=View.VISIBLE)
			  {
			  SH_rw24_ed.setFocusable(true);
			  SH_rw24_ed.setText("Beyond scope of inspection");
			  SH_rw24_ed.requestFocus();
			  SH_rw24_lin.setVisibility(v.VISIBLE);
			  }
			 break;*/
		  case R.id.GCH_GH_S_N1:
			  if(savevalidation_poolpresents()) /** Validate the inputs of pool presents **/
			  {
				  try{
					  
				
					  if(ISPoolPresent.isChecked()==true){ISPoolpresentval=0;}else if(ISPoolPresent.isChecked()==false){ISPoolpresentval=1;}
				
					  cf.Create_Table(7);
						Cursor BI_save=cf.SelectTablefunction(cf.GHC_table, " where GCH_HZ_Homeid='"+cf.encode(cf.selectedhomeid)+"'");
						 
						 if(BI_save.getCount()>0)
							{
								
									cf.gch_db.execSQL("UPDATE "+cf.GHC_table+ " set GCH_HZ_ISPOOL_PRES='"+ISPoolpresentval+"',GCH_HZ_POOLPRES='"+cf.encode(PP_RG_GROUNDLEVEL_val)+"',GCH_HZ_HOTTUB='"+cf.encode(PP_RGRP1_val)+"',GCH_HZ_PPS='"+cf.encode(PP_RGPPS_val)+"',GCH_HZ_HOTTUBCOVER='"+cf.encode(PP_RGRP3_val)+"'," +
											"GCH_HZ_POOLSLIDE='"+cf.encode(PP_RGRP4_val)+"',GCH_HZ_DIVINGBOARD='"+cf.encode(PP_RGRP5_val)+"',GCH_HZ_PERIPOOL='"+cf.encode(PP_RGRP6_val)+"',GCH_HZ_EMPTYPOOLPRESENT='"+cf.encode(PP_RGRP2_val)+"',GCH_HZ_POOLENCLOS='"+cf.encode(PP_RGRP7_val)+"' where GCH_HZ_Homeid='"+cf.selectedhomeid+"' ");

									
									cf.ShowToast("Swimming  Pool Presents updated sucessfully.", 1);
									((TextView) findViewById(R.id.GCH_GH_C_S1)).setVisibility(View.VISIBLE);
									defaultlayout();
									if(ISPermiterpoolPresent.isChecked())
									{
										
										poollist1.setVisibility(v.GONE);
										summaryheader.setBackgroundResource(R.drawable.subbackrepeatnor);
										summaryhazardoptionlist1.setVisibility(v.VISIBLE);
									}
									else
									{
										poolfencelist1.setVisibility(v.VISIBLE);
										perimeterpoolfenceheader.setBackgroundResource(R.drawable.subbackrepeatnor);
										poollist1.setVisibility(v.GONE);
									}
									
									
									
								
							}
							else
							{
									
										cf.gch_db.execSQL("INSERT INTO "
												+ cf.GHC_table
												+ " (GCH_HZ_Inspectorid,GCH_HZ_Homeid,GCH_HZ_ISPOOL_PRES,GCH_HZ_POOLPRES,GCH_HZ_HOTTUB,GCH_HZ_PPS,GCH_HZ_HOTTUBCOVER,GCH_HZ_POOLSLIDE,GCH_HZ_DIVINGBOARD,GCH_HZ_PERIPOOL,GCH_HZ_POOLENCLOS,GCH_HZ_EMPTYPOOLPRESENT)"
												+ "VALUES ('"+cf.Insp_id+"','"+cf.selectedhomeid+"','"+ISPoolpresentval+"','"+cf.encode(PP_RG_GROUNDLEVEL_val)+"','"+cf.encode(PP_RGRP1_val)+"','"+cf.encode(PP_RGPPS_val)+"','"+cf.encode(PP_RGRP3_val)+"','"+cf.encode(PP_RGRP4_val)+"','"+cf.encode(PP_RGRP5_val)+"','"+cf.encode(PP_RGRP6_val)+"','"+cf.encode(PP_RGRP7_val)+"','"+cf.encode(PP_RGRP2_val)+"')");

										cf.ShowToast("Swimming  Pool Presents updated sucessfully.", 1);
										((TextView) findViewById(R.id.GCH_GH_C_S1)).setVisibility(View.VISIBLE);
										defaultlayout();
										
										
										if(ISPermiterpoolPresent.isChecked())
										{
											
											poollist1.setVisibility(v.GONE);
											summaryheader.setBackgroundResource(R.drawable.subbackrepeatnor);
											summaryhazardoptionlist1.setVisibility(v.VISIBLE);
										}
										else
										{
											poolfencelist1.setVisibility(v.VISIBLE);
											perimeterpoolfenceheader.setBackgroundResource(R.drawable.subbackrepeatnor);
											poollist1.setVisibility(v.GONE);
										}
										
										
							}
					}
					catch (Exception E)
					{
						System.out.println("error in the pool "+E.getMessage());
					}
				   
				  
			  }
			  
		  break;
		  case R.id.GCH_GH_S_N2:
			  if(savevalidation_poolfence()) /** Validate the inputs of pool fence presents **/
			  {
				  try
					{
					  cf.Create_Table(7);
						Cursor BI_save=cf.SelectTablefunction(cf.GHC_table, " where GCH_HZ_Homeid='"+cf.encode(cf.selectedhomeid)+"'");
						
						
						
						if(ISPermiterpoolPresent.isChecked()==true){Isperimeterpoolfenceval=0;}else if(ISPermiterpoolPresent.isChecked()==false){Isperimeterpoolfenceval=1;}
						
						 if(BI_save.getCount()>0)
							{
								
									cf.gch_db.execSQL("UPDATE "+cf.GHC_table+ " set GCH_HZ_ISPERI_PF='"+Isperimeterpoolfenceval+"',GCH_HZ_PERI_PF='"+cf.encode(GHC_PPFECNCE_VAL)+"',GCH_HZ_OTHERPERI_PF='"+cf.encode(PF_HZ_OTHERPERI)+"',GCH_HZ_SELFLATCH='"+cf.encode(PF_RG_SELFLATCHES_val)+"',GCH_HZ_PROF_INST='"+cf.encode(PF_RG_PROFINST_val)+"',GCH_HZ_PF_DISREPAIR='"+cf.encode(PF_RG_POOLFENCE_val)+"' where GCH_HZ_Homeid='"+cf.encode(cf.selectedhomeid)+"'");

									
									cf.ShowToast("Permiter Pool Fence updated sucessfully.", 1);
									((TextView) findViewById(R.id.GCH_GH_C_S2)).setVisibility(View.VISIBLE);
									defaultlayout();
									poolfencelist1.setVisibility(v.GONE);
									summaryheader.setBackgroundResource(R.drawable.subbackrepeatnor);
									summaryhazardoptionlist1.setVisibility(v.VISIBLE);
									
								
							}
							else
							{
									
							
										cf.gch_db.execSQL("INSERT INTO "
												+ cf.GHC_table
												+ " (GCH_HZ_Inspectorid,GCH_HZ_Homeid,GCH_HZ_ISPERI_PF,GCH_HZ_PERI_PF,GCH_HZ_OTHERPERI_PF,GCH_HZ_SELFLATCH,GCH_HZ_PROF_INST,GCH_HZ_PF_DISREPAIR)"
												+ "VALUES ('"+cf.Insp_id+"','"+cf.selectedhomeid+"','"+Isperimeterpoolfenceval+"','"+cf.encode(GHC_PPFECNCE_VAL)+"','"+cf.encode(PF_HZ_OTHERPERI)+"','"+cf.encode(PF_RG_SELFLATCHES_val)+"','"+cf.encode(PF_RG_PROFINST_val)+"','"+cf.encode(PF_RG_POOLFENCE_val)+"')");

										cf.ShowToast("Permiter Pool Fence updated sucessfully.", 1);
										((TextView) findViewById(R.id.GCH_GH_C_S2)).setVisibility(View.VISIBLE);
										defaultlayout();
										poolfencelist1.setVisibility(v.GONE);
										
										summaryhazardoptionlist1.setVisibility(v.VISIBLE);
										summaryheader.setBackgroundResource(R.drawable.subbackrepeatnor);
								
							}
					}
					catch (Exception E)
					{
						System.out.println("error in the pool "+E.getMessage());
					}  
			  }
			 
			  break;
		  case R.id.GCH_GH_S_N3:
			  
			  if(savevalidation_SummeryHazards()) /** Validate the inputs of summery hazards **/
			  {
				  try
					{
					  cf.Create_Table(7);
						Cursor BI_save=cf.SelectTablefunction(cf.GHC_table, " where GCH_HZ_Homeid='"+cf.encode(cf.selectedhomeid)+"'");
						 /**Get the value for the Beyond the scope of inspection **/
						 GCH_RECDRYWALL_BSI="";//=SH_rw20_ed.getText().toString().trim();
						 GCH_CHINESEDRYWALL_BSI="";//=SH_rw21_ed.getText().toString().trim();
						 GCH_CONFDRYWALL_BSI="";//=SH_rw22_ed.getText().toString().trim();
						 GCH_NONSECURITY_BSI="";//=SH_rw23_ed.getText().toString().trim();
						 GCH_NONSMOKE_BSI="";//=SH_rw24_ed.getText().toString().trim();
						 /**Get the value for the Beyond the scope of inspection Ends **/
					System.out.println("Bfr save PP_RG_SUMMHAZ3_Q7_val="+PP_RG_SUMMHAZ3_Q7_val);
						 if(BI_save.getCount()>0)
							{
								
									cf.gch_db.execSQL("UPDATE "+cf.GHC_table+ " set GCH_HZ_VICIOUS='"+cf.encode(PP_RG_SUMMHAZ1_Q1_val)+"'," +
							"GCH_HZ_VICIOUSDESC='"+cf.encode(HZ_VICIOUSDESCVAL)+"',GCH_HZ_LIVESTOCK='"+cf.encode(PP_RG_SUMMHAZ1_Q2_val)+"',GCH_HZ_LIVESTOCKDESC='"+cf.encode(HZ_HORSESDESCVAL)+"',GCH_HZ_OVERHANGING='"+cf.encode(PP_RG_SUMMHAZ1_Q3_val)+"'," +
							"GCH_HZ_TRAMPOLINE='"+cf.encode(PP_RG_SUMMHAZ1_Q4_val)+"',GCH_HZ_SKATEBOARD='"+cf.encode(PP_RG_SUMMHAZ1_Q5_val)+"',GCH_HZ_BICYCLE='"+cf.encode(PP_RG_SUMMHAZ1_Q6_val)+"'," +
							"GCH_HZ_GROUNDPOOL='',GCH_HZ_TRIPHAZARD='"+cf.encode(PF_RG_TRIPHAZ_val)+"'," +
							"GCH_HZ_TRIPHAZARDNOTES='"+cf.encode(HZ_TRIPHAZARDNOTES)+"',GCH_HZ_UNSAFESTAIRWAY='"+cf.encode(PP_RG_SUMMHAZ2_Q1_val)+"'," +
							"GCH_HZ_PORCHANDDESK='"+cf.encode(PP_RG_SUMMHAZ2_Q2_val)+"',GCH_HZ_NONSTDCONST='"+cf.encode(PP_RG_SUMMHAZ2_Q3_val)+"',GCH_HZ_OUTDOORAPPL='"+cf.encode(PP_RG_SUMMHAZ2_Q4_val)+"'," +
							"GCH_HZ_OPENFOUND='"+cf.encode(PP_RG_SUMMHAZ2_Q5_val)+"',GCH_HZ_WOODSINGHLED='"+cf.encode(PP_RG_SUMMHAZ2_Q6_val)+"',GCH_HZ_EXCESSDEBRIS='"+cf.encode(PP_RG_SUMMHAZ2_Q7_val)+"',GCH_HZ_BUISPREMISES='"+cf.encode(PP_RG_SUMMHAZ3_Q1_val)+"',GCH_HZ_BUISPREMDESC='"+cf.encode(HZ_BUISNPREM_DESC)+"'," +
							"GCH_HZ_GENDISREPAIR='"+cf.encode(PP_RG_SUMMHAZ3_Q2_val)+"',GCH_HZ_PROP_DAMAGE='"+cf.encode(PP_RG_SUMMHAZ3_Q3_val)+"',GCH_HZ_STRUPARTIAL='"+cf.encode(PP_RG_SUMMHAZ3_Q4_val)+"'," +
							"GCH_HZ_INOPERATIVE='"+cf.encode(PP_RG_SUMMHAZ3_Q5_val)+"',GCH_HZ_RECDRYWALL='"+cf.encode(PP_RG_SUMMHAZ3_Q6_val)+"',GCH_HZ_CHINESEDRYWALL='"+cf.encode(PP_RG_SUMMHAZ3_Q7_val)+"'," +
							"GCH_HZ_CONFDRYWALL='"+cf.encode(PP_RG_SUMMHAZ3_Q8_val)+"',GCH_HZ_NONSECURITY='"+cf.encode(PP_RG_SUMMHAZ3_Q9_val)+"',GCH_HZ_NONSMOKE='"+cf.encode(PP_RG_SUMMHAZ3_Q10_val)+"',GCH_HZ_RECDRYWALL_BSI='"+cf.encode(GCH_RECDRYWALL_BSI)+"',GCH_HZ_CHINESEDRYWALL_BSI='"+cf.encode(GCH_CHINESEDRYWALL_BSI)+"'," +
							"GCH_HZ_CONFDRYWALL_BSI='"+cf.encode(GCH_CONFDRYWALL_BSI)+"',GCH_HZ_NONSECURITY_BSI='"+cf.encode(GCH_NONSECURITY_BSI)+"',GCH_HZ_NONSMOKE_BSI='"+cf.encode(GCH_NONSMOKE_BSI)+"' where GCH_HZ_Homeid='"+cf.encode(cf.selectedhomeid)+"'");

									
									cf.ShowToast("Summary of Hazards/Concerns updated sucessfully.", 1);
									((TextView) findViewById(R.id.GCH_GH_C_S3)).setVisibility(View.VISIBLE);
									defaultlayout();
									//poollist1.setVisibility(v.VISIBLE);
									//HZ_general_header.setBackgroundResource(R.drawable.subbackrepeatnor);
									summaryhazardoptionlist1.setVisibility(v.GONE);
									
								
							}
							else
							{
											cf.gch_db.execSQL("INSERT INTO "+ cf.GHC_table + " (GCH_HZ_Inspectorid,GCH_HZ_Homeid,GCH_HZ_VICIOUS,GCH_HZ_VICIOUSDESC,GCH_HZ_LIVESTOCK,GCH_HZ_LIVESTOCKDESC,GCH_HZ_OVERHANGING,GCH_HZ_TRAMPOLINE,GCH_HZ_SKATEBOARD,GCH_HZ_BICYCLE,GCH_HZ_GROUNDPOOL,GCH_HZ_TRIPHAZARD,GCH_HZ_TRIPHAZARDNOTES,GCH_HZ_UNSAFESTAIRWAY,GCH_HZ_PORCHANDDESK,GCH_HZ_NONSTDCONST,GCH_HZ_OUTDOORAPPL,GCH_HZ_OPENFOUND,GCH_HZ_WOODSINGHLED,GCH_HZ_EXCESSDEBRIS,GCH_HZ_BUISPREMISES,GCH_HZ_BUISPREMDESC,GCH_HZ_GENDISREPAIR,GCH_HZ_PROP_DAMAGE,GCH_HZ_STRUPARTIAL,GCH_HZ_INOPERATIVE,GCH_HZ_RECDRYWALL,GCH_HZ_CHINESEDRYWALL,GCH_HZ_CONFDRYWALL,GCH_HZ_NONSECURITY,GCH_HZ_NONSMOKE,GCH_HZ_RECDRYWALL_BSI,GCH_HZ_CHINESEDRYWALL_BSI,GCH_HZ_CONFDRYWALL_BSI,GCH_HZ_NONSECURITY_BSI,GCH_HZ_NONSMOKE_BSI)"
												+ "VALUES ('"+cf.Insp_id+"','"+cf.selectedhomeid+"','"+cf.encode(PP_RG_SUMMHAZ1_Q1_val)+"','"+cf.encode(HZ_VICIOUSDESCVAL)+"','"+cf.encode(PP_RG_SUMMHAZ1_Q2_val)+"','"+cf.encode(HZ_HORSESDESCVAL)+"','"+cf.encode(PP_RG_SUMMHAZ1_Q3_val)+"','"+cf.encode(PP_RG_SUMMHAZ1_Q4_val)+"','"+cf.encode(PP_RG_SUMMHAZ1_Q5_val)+"','"+cf.encode(PP_RG_SUMMHAZ1_Q6_val)+"','','"+cf.encode(PF_RG_TRIPHAZ_val)+"','"+cf.encode(HZ_TRIPHAZARDNOTES)+"','"+cf.encode(PP_RG_SUMMHAZ2_Q1_val)+"','"+cf.encode(PP_RG_SUMMHAZ2_Q2_val)+"','"+cf.encode(PP_RG_SUMMHAZ2_Q3_val)+"','"+cf.encode(PP_RG_SUMMHAZ2_Q4_val)+"','"+cf.encode(PP_RG_SUMMHAZ2_Q5_val)+"','"+cf.encode(PP_RG_SUMMHAZ2_Q6_val)+"','"+cf.encode(PP_RG_SUMMHAZ2_Q7_val)+"','"+cf.encode(PP_RG_SUMMHAZ3_Q1_val)+"','"+cf.encode(HZ_BUISNPREM_DESC)+"','"+cf.encode(PP_RG_SUMMHAZ3_Q2_val)+"','"+cf.encode(PP_RG_SUMMHAZ3_Q3_val)+"','"+cf.encode(PP_RG_SUMMHAZ3_Q4_val)+"','"+cf.encode(PP_RG_SUMMHAZ3_Q5_val)+"','"+cf.encode(PP_RG_SUMMHAZ3_Q6_val)+"','"+cf.encode(PP_RG_SUMMHAZ3_Q7_val)+"','"+cf.encode(PP_RG_SUMMHAZ3_Q8_val)+"','"+cf.encode(PP_RG_SUMMHAZ3_Q9_val)+"','"+cf.encode(PP_RG_SUMMHAZ3_Q10_val)+"','"+cf.encode(GCH_RECDRYWALL_BSI)+"','"+cf.encode(GCH_CHINESEDRYWALL_BSI)+"','"+cf.encode(GCH_CONFDRYWALL_BSI)+"','"+cf.encode(GCH_NONSECURITY_BSI)+"','"+cf.encode(GCH_NONSMOKE_BSI)+"')");

										cf.ShowToast("Summary of Hazards/Concerns updated sucessfully.", 1);
										((TextView) findViewById(R.id.GCH_GH_C_S3)).setVisibility(View.VISIBLE);
										defaultlayout();
										poollist1.setVisibility(v.VISIBLE);
										HZ_general_header.setBackgroundResource(R.drawable.subbackrepeatnor);
										
										summaryhazardoptionlist1.setVisibility(v.GONE);
										
											
								
							}
					}
					catch (Exception E)
					{
						System.out.println("error in the pool "+E.getMessage());
					}  
				  
			  }
			
			  break;
		  }
	}
	

	public void defaultlayout() {
		// TODO Auto-generated method stub
		poollist1.setVisibility(cf.show.GONE);
		poolfencelist1.setVisibility(cf.show.GONE);
		summaryhazardoptionlist1.setVisibility(cf.show.GONE);		
		summaryheader.setBackgroundResource(R.drawable.backrepeatnorobs);
		perimeterpoolfenceheader.setBackgroundResource(R.drawable.backrepeatnorobs);
		HZ_general_header.setBackgroundResource(R.drawable.backrepeatnorobs);
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if(cf.strschdate.equals("")){
				cf.ShowToast("Layout navigation without scheduling not allowed. ", 1);
			}else{
				Intent  myintent = new Intent(getApplicationContext(),BuildingInformation.class);
				cf.putExtras(myintent);
				startActivity(myintent);
			}
			return true;
		}
		if (keyCode == KeyEvent.KEYCODE_MENU) {

		}
		return super.onKeyDown(keyCode, event);
	}
	class GH_clicker implements OnClickListener	  {

		  private static final int visibility = 0;

		@Override
		  public void onClick(View v) {
			switch (v.getId()) {
			

			case R.id.HZ_general_header:
				if(ISPoolPresent.isChecked()){poollist1.setVisibility(v.GONE);}
				else{
					defaultlayout();
					
				poollist1.setVisibility(v.VISIBLE);
				HZ_general_header.setBackgroundResource(R.drawable.subbackrepeatnor);
				((RelativeLayout) v).requestFocus();  }
				
			break;
		  case R.id.HZ_perimeterpoolfence_header:
			  if(ISPermiterpoolPresent.isChecked()){poolfencelist1.setVisibility(v.GONE);}
			  else{
				defaultlayout();
				poolfencelist1.setVisibility(v.VISIBLE);
				perimeterpoolfenceheader.setBackgroundResource(R.drawable.subbackrepeatnor);	
				((RelativeLayout) v).requestFocus();
			  }
				
				break;
		  case R.id.HZ_summary_header:
				defaultlayout();
				
				summaryhazardoptionlist1.setVisibility(v.VISIBLE);
				summaryheader.setBackgroundResource(R.drawable.subbackrepeatnor);	
				((RelativeLayout) v).requestFocus();
				break;
		  case R.id.HZ_SUMM_VICIOUS_YES:
			  vicious_lin.setVisibility(v.VISIBLE);
			  etvicious.setFocusableInTouchMode(true);
			  etvicious.requestFocus();
			  etvicious.setCursorVisible(true);
			  ((TextView) findViewById(R.id.GCH_S_D1)).setVisibility(View.INVISIBLE);
			  
		  break;
		  case R.id.HZ_SUMM_VICIOUS_NO:
			  etvicious.setFocusable(false);
			   vicious_lin.setVisibility(v.GONE);
			   ((TextView) findViewById(R.id.GCH_S_D1)).setVisibility(View.GONE);

			  break;
		  case R.id.HZ_SUMM_HORSES_YES:
			  ethorses.setFocusableInTouchMode(true);
			  ethorses.requestFocus();
			  ethorses.setCursorVisible(true);
			  horses_lin.setVisibility(v.VISIBLE);
			  ((TextView) findViewById(R.id.GCH_S_D2)).setVisibility(View.INVISIBLE);
			  break;
		  case R.id.HZ_SUMM_HORSES_NO:
			  ethorses.setFocusable(false);
			  horses_lin.setVisibility(v.GONE);
			  ((TextView) findViewById(R.id.GCH_S_D2)).setVisibility(View.GONE);
			  break;
			  
		  case R.id.HZ_hottub_covered_yes:
			  hottubcoveredtr.setVisibility(v.VISIBLE);
		  break;
		  case R.id.HZ_hottub_covered_no:
			  hottubcoveredtr.setVisibility(v.GONE);
		  break;
	
		  case R.id.HZ_SUMM_BUISPREM_YES:
			  etsummbuisprem.setFocusableInTouchMode(true);
			  etsummbuisprem.requestFocus();
			  etsummbuisprem.setCursorVisible(true);
			  business_lin.setVisibility(v.VISIBLE);
			  ((TextView) findViewById(R.id.GCH_S_D3)).setVisibility(View.INVISIBLE);
			  break;
		  case R.id.HZ_SUMM_BUISPREM_NO:
			  etsummbuisprem.setFocusable(false);
			  business_lin.setVisibility(v.GONE);
			  ((TextView) findViewById(R.id.GCH_S_D3)).setVisibility(View.GONE);
			  break;
		 
			}
		}
	}
	
protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
			case 0:
				break;
			case -1:
				try {

					String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();
					String capturedImageFilePath = cursor.getString(column_index_data);
					cf.showselectedimage(capturedImageFilePath);
				} catch (Exception e) {
					
				}
				
				break;

		}

	}

	public void onWindowFocusChanged(boolean hasFocus) {

        super.onWindowFocusChanged(hasFocus);
        if(focus==1)
        {
        	focus=0;
        
        if(vicious_lin.getVisibility()==View.VISIBLE)
        {
        	etvicious.setText(etvicious.getText().toString());
        }
        if(horses_lin.getVisibility()==View.VISIBLE)
        {
        	ethorses.setText(ethorses.getText().toString());
        }
        if(business_lin.getVisibility()==View.VISIBLE)
        {
        	etsummbuisprem.setText(etsummbuisprem.getText().toString());
        }
        HZ_comments.setText(HZ_comments.getText().toString());
        summaryhazardoptionlist1.setVisibility(View.GONE);
        }         
}
	
}