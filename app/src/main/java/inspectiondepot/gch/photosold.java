package inspectiondepot.gch;

import inspectiondepot.gch.R;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;
import org.w3c.dom.Text;
import android.R.string;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class photosold extends Activity implements Runnable {

	private static final String TAG = null;
	CommonFunctions cf;
	public int elev = 0;
	public int t = 0;
	public Uri CapturedImageURI;
	public String name = "Other";int etchangeordermaxLength = 1;

	public int maxlevel;
	public TextView txthdr;
	Map<String, String[]> elevationcaption_map = new LinkedHashMap<String, String[]>();
	String[] arrpath, arrpathdesc, arrimgord;
	public LinearLayout lnrlayout;
	public String imagorder, updstr;;
	public int chk = 0;
	int globalvar=0;
	Spinner[] spnelevation;
	TextView[] tvstatus;
	/**Rotating the image variable declaration **/	 Bitmap rotated_b;
	 int  currnet_rotated=0;
	 /**Rotating the image variable declaration ends **/
	/** Variable declaration for the multi image selection **/
	String[] pieces1,pieces3;
	protected Button selectImgBtn;
	protected Button cancelBtn;
	protected Button backbtn;
	protected TextView chaild_titleheade;
	protected TextView titlehead;
	protected LinearLayout lnrlayout2_FI;
	protected LinearLayout lnrlayout1_FI;
	protected Dialog pd;
	public String finaltext="";
	public int selectedcount;
	public String[] selectedtestcaption;
	public int maximumindb;
	public int backclick_FI;
	public String path="";
	Map<String, TextView> map1 = new LinkedHashMap<String, TextView>();
	
	/**Newly added for limiting option **/
	public int limit_start=0; 
	RelativeLayout Lin_Pre_nex=null;
	ImageView prev=null,next=null;
	File[] Currentfile_list=null;
	private File images=null;
	public  ScrollView scr2=null;
	protected RelativeLayout Rec_count_lin=null;
	protected TextView Total_Rec_cnt=null;
	protected TextView showing_rec_cnt;
	/** Variable declaration for the multi image selection ends **/

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cf = new CommonFunctions(this);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			try {
				cf.ph_type = extras.getInt("type");
				/** Find the elevation type from the type **/
				elev = cf.ph_type - 50;
				cf.selectedhomeid = extras.getString("homeid");
				cf.onlinspectionid = extras.getString("InspectionType");
				cf.onlstatus = extras.getString("status");
			} catch (Exception e) {
				// TODO: handle exception

			}
		}
		setContentView(R.layout.photos);
		cf.getDeviceDimensions();
		LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
		mainmenu_layout.setMinimumWidth(cf.wd);
		mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(),
				5, 0, cf));
		LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.generalsubmenu);
		submenu_layout.addView(new MyOnclickListener(getApplicationContext(),
				cf.ph_type, 1, cf));
		ScrollView scr = (ScrollView) findViewById(R.id.scr);
		scr.setMinimumHeight(cf.ht);
		cf.getInspectorId();
		cf.getinspectiondate();
		cf.Create_Table(16);
		cf.Create_Table(15);
		/** Declaring the name of elevation **/
		switch (elev) {
		case 1:
			name = "Front";
			break;
		case 2:
			name = "Right";
			break;
		case 3:
			name = "Back";
			break;
		case 4:
			name = "Left";
			break;
		case 5:
			name = "Interior and Other";
			break;

		}
		/** Declaring the name of elevation **/
		/** Declaration objects starts **/
		txthdr = (TextView) findViewById(R.id.titlehdr);
		lnrlayout = (LinearLayout) this.findViewById(R.id.linscrollview);

		/** Declaration objects ends **/

		/** Show the saved values **/
		show_savedvalue();
		/** Show the saved values ends **/
	}

	public void show_savedvalue() {
		// TODO Auto-generated method stub
		try {
			int rws = 0;
			Cursor c11 = cf.gch_db.rawQuery("SELECT * FROM " + cf.ImageTable
					+ " WHERE GCH_IM_SRID='" + cf.selectedhomeid
					+ "' and GCH_IM_Elevation='" + elev
					+ "' order by GCH_IM_ImageOrder", null);
			maxlevel = rws = c11.getCount();

			int rem = 4 - rws;
			arrpath = new String[rws];
			arrpathdesc = new String[rws];
			arrimgord = new String[rws];
			String source = "<b><font color=#000000> Number of uploaded images : "
					+ "</font><font color=#DAA520>"
					+ rws
					+ "</font><font color=#000000> Remaining : "
					+ "</font><font color=#DAA520>" + rem + "</font>";

			txthdr.setText(Html.fromHtml(source));
			if (rws == 0) {

				lnrlayout.removeAllViews();
				/*
				 * firsttxt.setVisibility(v1.GONE);
				 * fstimg.setVisibility(v1.GONE); updat.setVisibility(v1.GONE);
				 * del.setVisibility(v1.GONE);
				 */
			} else {
				int Column1 = c11.getColumnIndex("GCH_IM_ImageName");
				int Column2 = c11.getColumnIndex("GCH_IM_Description");
				int Column3 = c11.getColumnIndex("GCH_IM_ImageOrder");
				c11.moveToFirst();
				if (c11 != null) {
					int i = 0;
					do {
						commonelevation(cf.decode(c11.getString(Column3)), i); // set
																				// the
																				// caption
																				// form
																				// the
																				// database
						arrpath[i] = cf.decode(c11.getString(Column1));
						arrpathdesc[i] = cf.decode(c11.getString(Column2));
						arrimgord[i] = cf.decode(c11.getString(Column3));
						ColorDrawable sage = new ColorDrawable(this
								.getResources().getColor(R.color.sage));
						i++;
					} while (c11.moveToNext());
					dynamicview();

				}
			}
		} catch (Exception e) {
			// Log.i(TAG, "error correct= " + e.getMessage());
		}
	}

	private void commonelevation(String imgor, int spin) {
		// TODO Auto-generated method stub
		int imgorder = 0;
		try {
			imgorder = Integer.parseInt(imgor);
		} catch (Exception e) {

		}
		Cursor c1;

		c1 = cf.SelectTablefunction(cf.Photo_caption,
				" WHERE GCH_IMP_Elevation='" + elev
						+ "' and GCH_IMP_ImageOrder='" + imgor + "'");
		String[] temp;
		if (c1.getCount() > 0) {
			temp = new String[c1.getCount() + 2];
			temp[0] = "Select";
			temp[1] = "ADD PHOTO CAPTION";
			int i = 2;
			c1.moveToFirst();
			do {
				temp[i] = cf.decode(c1.getString(c1
						.getColumnIndex("GCH_IMP_Caption")));
				i++;
			} while (c1.moveToNext());
		} else {
			temp = new String[2];
			temp[0] = "Select";
			temp[1] = "ADD PHOTO CAPTION";
		}
		elevationcaption_map.put("spiner" + spin, temp);
	}

	private void dynamicview() throws FileNotFoundException {
		// TODO Auto-generated method stub

		
		ImageView[] elevationimage;

		lnrlayout.removeAllViews();
		ScrollView sv = new ScrollView(this);
		lnrlayout.addView(sv);

		LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(l1);
		// l1.setMinimumWidth(925);

		tvstatus = new TextView[arrpath.length];
		elevationimage = new ImageView[arrpath.length];
		spnelevation = new Spinner[arrpath.length];

		for (int i = 0; i < arrpath.length; i++) {

			LinearLayout l2 = new LinearLayout(this);
			l2.setOrientation(LinearLayout.HORIZONTAL);
			l1.addView(l2);

			LinearLayout lchkbox = new LinearLayout(this);
			LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
					175, ViewGroup.LayoutParams.WRAP_CONTENT);
			paramschk.topMargin = 0;
			paramschk.leftMargin = 10;
			l2.addView(lchkbox);

			tvstatus[i] = new TextView(this);
			tvstatus[i].setMinimumWidth(175);
			tvstatus[i].setMaxWidth(175);
			tvstatus[i].setText(arrpathdesc[i]);
			tvstatus[i].setTextColor(Color.BLACK);
			tvstatus[i].setTag("imagechange" + i);
			tvstatus[i].setTextSize(TypedValue.COMPLEX_UNIT_PX, 14);
			lchkbox.addView(tvstatus[i], paramschk);

			String j = "0";
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			try {
				BitmapFactory.decodeStream(new FileInputStream(arrpath[i]),
						null, o);
				j = "1";
			} catch (FileNotFoundException e) {
				j = "2";
			}
			if (j.equals("1")) {
				final int REQUIRED_SIZE = 100;
				int width_tmp = o.outWidth, height_tmp = o.outHeight;
				int scale = 1;
				while (true) {
					if (width_tmp / 2 < REQUIRED_SIZE
							|| height_tmp / 2 < REQUIRED_SIZE)
						break;
					width_tmp /= 2;
					height_tmp /= 2;
					scale *= 2;
				}

				// Decode with inSampleSize
				BitmapFactory.Options o2 = new BitmapFactory.Options();
				o2.inSampleSize = scale;
				Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(
						arrpath[i]), null, o2);
				BitmapDrawable bmd = new BitmapDrawable(bitmap);

				LinearLayout lelevimage = new LinearLayout(this);
				LinearLayout.LayoutParams paramselevimg = new LinearLayout.LayoutParams(
						100, 50);
				paramselevimg.topMargin = 5;
				paramselevimg.leftMargin = 20;
				l2.addView(lelevimage);

				elevationimage[i] = new ImageView(this);
				elevationimage[i].setMinimumWidth(75);
				elevationimage[i].setMaxWidth(75);
				elevationimage[i].setMinimumHeight(75);
				elevationimage[i].setMaxHeight(75);
				elevationimage[i].setImageDrawable(bmd);
				elevationimage[i].setTag("imagechange" + i);
				lelevimage.addView(elevationimage[i], paramselevimg);
			}

			LinearLayout lspinner = new LinearLayout(this);
			LinearLayout.LayoutParams paramsspinn = new LinearLayout.LayoutParams(
					200, LayoutParams.WRAP_CONTENT);
			paramsspinn.topMargin = 5;
			paramsspinn.leftMargin = 20;
			l2.addView(lspinner);
			tvstatus[i].setOnClickListener(new View.OnClickListener() {

				public void onClick(final View v) {

					String getidofselbtn = v.getTag().toString();
					final String repidofselbtn = getidofselbtn.replace(
							"imagechange", "");
					final int cvrtstr = Integer.parseInt(repidofselbtn);

					String selpath = arrpath[cvrtstr];
					String seltxt = arrpathdesc[cvrtstr];

					Cursor c11 = cf.gch_db.rawQuery(
							"SELECT GCH_IM_ImageOrder FROM " + cf.ImageTable
									+ " WHERE GCH_IM_SRID='"
									+ cf.selectedhomeid
									+ "' and GCH_IM_Elevation='" + elev
									+ "' and GCH_IM_ImageName='"
									+ cf.encode(arrpath[cvrtstr]) + "'", null);
					c11.moveToFirst();
				   imagorder= c11.getString(c11.getColumnIndex("GCH_IM_ImageOrder"));
					String a;
					chk = 1;
					// dispfirstimg(cvrtstr);
					// dispfirstimg(cvrtstr,seltxt);

				}
			});
			elevationimage[i].setOnClickListener(new View.OnClickListener() {
				public void onClick(final View v) {

					String getidofselbtn = v.getTag().toString();
					final String repidofselbtn = getidofselbtn.replace(
							"imagechange", "");
					final int cvrtstr = Integer.parseInt(repidofselbtn);

					String selpath = arrpath[cvrtstr];
					String seltxt = arrpathdesc[cvrtstr];

					Cursor c11 = cf.gch_db.rawQuery(
							"SELECT GCH_IM_ImageOrder FROM " + cf.ImageTable
									+ " WHERE GCH_IM_SRID='"
									+ cf.selectedhomeid
									+ "' and GCH_IM_Elevation='" + elev
									+ "' and GCH_IM_ImageName='"
									+ cf.encode(arrpath[cvrtstr]) + "'", null);
					c11.moveToFirst();
					imagorder = c11.getString(c11
							.getColumnIndex("GCH_IM_ImageOrder"));
					String a;
					chk = 1;
					// dispfirstimg(cvrtstr);
					dispfirstimg(cvrtstr, seltxt);

				}
			});
			int n = i + 1;

			String[] SH_IM_Elevation = elevationcaption_map.get("spiner" + i);
			spnelevation[i] = new Spinner(this);
			spnelevation[i].setId(i);
			ArrayAdapter adapter2 = new ArrayAdapter(photosold.this,
					android.R.layout.simple_spinner_item, SH_IM_Elevation);
			adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spnelevation[i].setAdapter(adapter2);
			lspinner.addView(spnelevation[i], paramsspinn);
			spnelevation[i]
					.setOnItemSelectedListener(new MyOnItemSelectedListener1(i));

		}
	}
/** Showing the image for the Update starts **/
	public void dispfirstimg(final int selid, String photocaptions) {
		 System.gc();
		currnet_rotated=0;
		
		final int delimagepos = selid;
		String phtodesc = photocaptions;

		String k;
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;

		final Dialog dialog1 = new Dialog(photosold.this,
				android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alert);

		/**
		 * get the help and update relative layou and set the visbilitys for the
		 * respective relative layout
		 */
		LinearLayout Re = (LinearLayout) dialog1.findViewById(R.id.maintable);
		Re.setVisibility(View.GONE);
		LinearLayout Reup = (LinearLayout) dialog1
				.findViewById(R.id.updateimage);
		Reup.setVisibility(View.VISIBLE);
		/**
		 * get the help and update relative layou and set the visbilitys for the
		 * respective relative layout
		 */

		final ImageView upd_img = (ImageView) dialog1.findViewById(R.id.firstimg);
		final EditText upd_Ed = (EditText) dialog1.findViewById(R.id.firsttxt);
		upd_Ed.setFocusable(false);
		//upd_Ed.setFocusableInTouchMode(true);
		Button btn_helpclose = (Button) dialog1
				.findViewById(R.id.imagehelpclose);
		Button btn_up = (Button) dialog1.findViewById(R.id.update);
		Button btn_del = (Button) dialog1.findViewById(R.id.delete);
		Button rotateleft = (Button) dialog1.findViewById(R.id.rotateleft);
		Button rotateright = (Button) dialog1.findViewById(R.id.rotateright);
		 upd_Ed.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				 
				cf.setFocus(upd_Ed);
				return false;
			}
		});
		// update and delete button function

	
		btn_up.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				updstr = upd_Ed.getText().toString();

				try {
					if (!updstr.trim().equals("")) {
						dialog1.setCancelable(true);
						dialog1.dismiss();

						cf.gch_db.execSQL("UPDATE " + cf.ImageTable
								+ " SET GCH_IM_Description='"
								+ cf.encode(updstr)
								+ "',GCH_IM_ModifiedOn='' WHERE GCH_IM_SRID ='"
								+ cf.selectedhomeid
								+ "' and GCH_IM_Elevation='" + elev
								+ "' and GCH_IM_ImageOrder='" + imagorder + "'");
						
						cf.ShowToast("Updated sucessfully.", 1);

					} else {
						cf.ShowToast("Please enter the caption.", 1);cf.hidekeyboard();
					}
					/**Save the rotated value in to the external stroage place **/
					if(currnet_rotated>0)
					{ 

						try
						{
							/**Create the new image with the rotation **/
							String current=MediaStore.Images.Media.insertImage(getContentResolver(), rotated_b, "My bitmap", "My rotated bitmap");
							  ContentValues values = new ContentValues();
							  values.put(MediaStore.Images.Media.ORIENTATION, 0);
							  photosold.this.getContentResolver().update(Uri.parse(current), values, MediaStore.Images.Media.DATA+ "=?", new String[] { current } );
							
							
							if(current!=null)
							{
							String path=getPath(Uri.parse(current));
							File fout = new File(arrpath[selid]);
							fout.delete();
							/** delete the selected image **/
							File fin = new File(path);
							/** move the newly created image in the slected image pathe ***/
							fin.renameTo(new File(arrpath[selid]));
							
							
						}
						} catch(Exception e)
						{
							System.out.println("Error occure while rotate the image "+e.getMessage());
						}
						
					}
				} catch (Exception e) {
					System.out.println("erre " + e.getMessage());
				}
				/**Save the rotated value in to the external stroage place ends **/
				show_savedvalue();
			}

		});
		
		btn_del.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog1.setCancelable(true);
				dialog1.dismiss();
				AlertDialog.Builder builder = new AlertDialog.Builder(
						photosold.this);
				builder.setMessage(
						"Are you sure, Do you want to delete the image?")
						.setCancelable(false)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int id) {
										try {
											Cursor or = cf.gch_db
													.rawQuery(
															"select GCH_IM_ImageOrder from "
																	+ cf.ImageTable
																	+ " WHERE  GCH_IM_SRID ='"
																	+ cf.selectedhomeid
																	+ "' and GCH_IM_Elevation='"
																	+ elev
																	+ "' and GCH_IM_ImageName='"
																	+ cf.encode(arrpath[delimagepos])
																	+ "'", null);
											or.moveToFirst();
											int image_order = or.getInt(or
													.getColumnIndex("GCH_IM_ImageOrder"));

											cf.gch_db.execSQL("DELETE FROM "
													+ cf.ImageTable
													+ " WHERE GCH_IM_SRID ='"
													+ cf.selectedhomeid
													+ "' and GCH_IM_Elevation='"
													+ elev
													+ "' and GCH_IM_ImageName='"
													+ cf.encode(arrpath[delimagepos])
													+ "'");

											// Get the total count of images in
											// the table
											Cursor c3 = cf
													.SelectTablefunction(
															cf.ImageTable,
															" WHERE GCH_IM_SRID ='"
																	+ cf.selectedhomeid
																	+ "' and GCH_IM_Elevation='"
																	+ elev
																	+ "'");
											for (int m = image_order; m <= c3
													.getCount(); m++) {
												int k = m + 1;
												cf.gch_db.execSQL("UPDATE "
														+ cf.ImageTable
														+ " SET GCH_IM_ImageOrder='"
														+ m
														+ "' WHERE GCH_IM_SRID ='"
														+ cf.selectedhomeid
														+ "' and GCH_IM_Elevation='"
														+ elev
														+ "' and GCH_IM_ImageOrder='"
														+ k + "'");

											}

										} catch (Exception e) {
											System.out.println("exception e  "
													+ e);
										}
										cf.ShowToast(
												"Image has been deleted sucessfully.",
												1);

										chk = 0;
										try {
											Cursor c11 = cf.gch_db
													.rawQuery(
															"SELECT * FROM "
																	+ cf.ImageTable
																	+ " WHERE GCH_IM_SRID='"
																	+ cf.selectedhomeid
																	+ "' and GCH_IM_Elevation='"
																	+ elev
																	+ "'  ",
															null);
											int delchkrws = c11.getCount();

										} catch (Exception e) {

										}
										show_savedvalue();
										// showimages();
									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

										dialog.cancel();
									}
								});
				builder.show();
			}
		});
		btn_helpclose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog1.setCancelable(true);
				dialog1.dismiss();
			}
		});
		
		rotateright.setOnClickListener(new OnClickListener() {  
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.gc();
				currnet_rotated+=90;
				if(currnet_rotated>=360)
				{
					currnet_rotated=0;
				}
				
				Bitmap myImg;
				try {
					myImg = BitmapFactory.decodeStream(new FileInputStream(arrpath[selid]));
					Matrix matrix =new Matrix();
					matrix.reset();
					//matrix.setRotate(currnet_rotated);
					System.out.println("Ther is no more issues top ");
					matrix.postRotate(currnet_rotated);
					
					 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
					        matrix, true);
					 System.gc();
					 upd_img.setImageBitmap(rotated_b);

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (Exception e) {
					
				}
				catch (OutOfMemoryError e) {
					System.out.println("comes in to out ot mem exception");
					System.gc();
					try {
						myImg=null;
						System.gc();
						Matrix matrix =new Matrix();
						matrix.reset();
						//matrix.setRotate(currnet_rotated);
						matrix.postRotate(currnet_rotated);
						myImg= cf.ShrinkBitmap(arrpath[selid], 800, 800);
						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
						
						System.gc();
						upd_img.setImageBitmap(rotated_b); 

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 catch (OutOfMemoryError e1) {
							// TODO Auto-generated catch block
						 cf.ShowToast("You can not rotate this image. Image size is too large.",1);
					}
				}

			}
		});
		rotateleft.setOnClickListener(new OnClickListener() {  			
			public void onClick(View v) {

				// TODO Auto-generated method stub
			
				System.gc();
				currnet_rotated-=90;
				if(currnet_rotated<=0)
				{
					currnet_rotated=270;
				}

				
				Bitmap myImg;
				try {
					myImg = BitmapFactory.decodeStream(new FileInputStream(arrpath[selid]));
					Matrix matrix =new Matrix();
					matrix.reset();
					//matrix.setRotate(currnet_rotated);
					System.out.println("Ther is no more issues top ");
					matrix.postRotate(currnet_rotated);
					
					 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
					        matrix, true);
					 
					 upd_img.setImageBitmap(rotated_b);

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (Exception e) {
					
				}
				catch (OutOfMemoryError e) {
					System.out.println("comes in to out ot mem exception");
					System.gc();
					try {
						myImg=null;
						System.gc();
						Matrix matrix =new Matrix();
						matrix.reset();
						//matrix.setRotate(currnet_rotated);
						matrix.postRotate(currnet_rotated);
						myImg= cf.ShrinkBitmap(arrpath[selid], 800, 800);
						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
						System.gc();
						upd_img.setImageBitmap(rotated_b); 

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 catch (OutOfMemoryError e1) {
							// TODO Auto-generated catch block
						 cf.ShowToast("You can not rotate this image. Image size is too large.",1);
					}
				}

			
			}
		});
		
		// update and delete button function ends
		try {
			if (chk == 1) {
				BitmapFactory.decodeStream(new FileInputStream(arrpath[selid]),
						null, o);
			} else {
				BitmapFactory.decodeStream(new FileInputStream(arrpath[0]),
						null, o);
			}
			k = "1";

		} catch (FileNotFoundException e) {
			k = "2";

		}
		if (k.equals("1")) {
			final int REQUIRED_SIZE = 400;
			int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;
			while (true) {
				if (width_tmp / 2 < REQUIRED_SIZE
						|| height_tmp / 2 < REQUIRED_SIZE)
					break;
				width_tmp /= 2;
				height_tmp /= 2;
				scale *= 2;
			}

			// Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			Bitmap bitmap = null;
			try {
				Cursor c11 = cf.gch_db.rawQuery("SELECT * FROM " + cf.ImageTable
						+ " WHERE GCH_IM_SRID='" + cf.selectedhomeid
						+ "' and GCH_IM_Elevation='" + elev
						+ "' and GCH_IM_ImageName='" + cf.encode(arrpath[selid])
						+ "' ", null);
				c11.moveToFirst();
				String SH_IM_Description = c11.getString(c11
						.getColumnIndex("GCH_IM_Description"));

				if (chk == 1) {
					bitmap = BitmapFactory.decodeStream(new FileInputStream(
							arrpath[selid]), null, o2);
					upd_Ed.setText(cf.decode(SH_IM_Description));

				} else {
					bitmap = BitmapFactory.decodeStream(new FileInputStream(
							arrpath[0]), null, o2);
					upd_Ed.setText(cf.decode(arrpathdesc[0]));
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			rotated_b=bitmap;
			BitmapDrawable bmd = new BitmapDrawable(bitmap);
			
			upd_img.setImageDrawable(bmd);

			dialog1.setCancelable(false);
			dialog1.show();
		} else {
			dialog1.setCancelable(false);
			dialog1.show();
		}
		upd_Ed.clearFocus();
      //cf.hidekeyboard();
	}
/** Showing the image for the Update starts **/
	private class MyOnItemSelectedListener1 implements OnItemSelectedListener {

		public final int spinnerid;

		MyOnItemSelectedListener1(int id) {
			this.spinnerid = id;
		}

		public void onItemSelected(AdapterView<?> parent, View view,
				final int pos,

				long id) {
			if (pos == 0) {
			} else {

				int maxLength = 99;
				try {

					final int j = spinnerid + 1;
					final String[] photocaption = elevationcaption_map
							.get("spiner" + spinnerid);

					String[] bits = arrpath[spinnerid].split("/");
					final String picname = bits[bits.length - 1];

					if (photocaption[pos].equals("ADD PHOTO CAPTION")) {
						final AlertDialog.Builder alert = new AlertDialog.Builder(
								photosold.this);
						alert.setTitle("ADD PHOTO CAPTION");
						final EditText input = new EditText(photosold.this);
						alert.setView(input);
						InputFilter[] FilterArray = new InputFilter[1];
						FilterArray[0] = new InputFilter.LengthFilter(maxLength);
						input.setFilters(FilterArray);
						input.setTextSize(14);
						input.setWidth(800);

						alert.setPositiveButton("Add",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int whichButton) {
										String commentsadd = input.getText()
												.toString();
										if (input.length() > 0
												&& !commentsadd.trim().equals(
														"")) {
											cf.gch_db.execSQL("INSERT INTO "
													+ cf.Photo_caption
													+ " (GCH_IMP_INSP_ID,GCH_IMP_Caption,GCH_IMP_Elevation,GCH_IMP_ImageOrder)"
													+ " VALUES ('" + cf.Insp_id
													+ "','"
													+ cf.encode(commentsadd)
													+ "','" + elev + "','" + j
													+ "')");
											show_savedvalue();
											cf.ShowToast(
													"Photo Caption added successfully.",
													1);
											cf.hidekeyboard();

										} else {
											cf.ShowToast(
													"Please enter the caption.",
													1);cf.hidekeyboard();
											spnelevation[spinnerid]
													.setSelection(0);
										}
									}
								});

						alert.setNegativeButton("Cancel",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int whichButton) {
										dialog.cancel();
										spnelevation[spinnerid].setSelection(0);
										cf.hidekeyboard();
									}
								});

						alert.show();

					} else {
						final Dialog dialog = new Dialog(photosold.this);
						dialog.setContentView(R.layout.alertfront);

						dialog.setTitle("Choose Option");
						dialog.setCancelable(true);
						final EditText edit_desc = (EditText) dialog
								.findViewById(R.id.edittxtdesc);

						Button button_close = (Button) dialog
								.findViewById(R.id.Button01);
						final Button button_sel = (Button) dialog
								.findViewById(R.id.Button02);
						Button button_edit = (Button) dialog
								.findViewById(R.id.Button03);
						Button button_del = (Button) dialog
								.findViewById(R.id.Button04);
						final Button button_upd = (Button) dialog
								.findViewById(R.id.Button05);

						button_close.setOnClickListener(new OnClickListener() {
							public void onClick(View v) {
								spnelevation[spinnerid].setSelection(0);
								dialog.cancel();
							}

							public void onNothingSelected(AdapterView<?> arg0) {
								// TODO Auto-generated method stub

							}
						});
						button_sel.setOnClickListener(new OnClickListener() {
							public void onClick(View v) {
								try {

									cf.gch_db.execSQL("UPDATE "
											+ cf.ImageTable
											+ " SET GCH_IM_Description='"
											+ cf.encode(photocaption[pos])
											+ "',GCH_IM_Nameext='"
											+ cf.encode(picname)
											+ "',GCH_IM_ModifiedOn='"
											+ cf.encode(cf.datewithtime
													.toString())
											+ "' WHERE GCH_IM_SRID ='"
											+ cf.selectedhomeid
											+ "' and GCH_IM_Elevation='" + elev
											+ "' and GCH_IM_ImageOrder='"
											+ arrimgord[spinnerid] + "'");
									spnelevation[spinnerid].setSelection(0);
								} catch (Exception e) {
									cf.Error_LogFile_Creation("Problem in select the caption in the photos at the elevation "
											+ elev
											+ " error  ="
											+ e.getMessage());
								}

								tvstatus[spinnerid].setText(photocaption[pos]);
								dialog.cancel();
							}
						});

						button_edit.setOnClickListener(new OnClickListener() {

							public void onClick(View v) {

								button_sel.setVisibility(v.GONE);

								edit_desc.setVisibility(v.VISIBLE);
								edit_desc.setText(photocaption[pos]);

								button_upd.setVisibility(v.VISIBLE);
								System.out.println("edit_desc = ");
								button_upd
										.setOnClickListener(new OnClickListener() {

											//
											public void onClick(View v) {
												if (!edit_desc.getText()
														.toString().trim()
														.equals("")) {
													cf.gch_db.execSQL("UPDATE "
															+ cf.Photo_caption
															+ " set GCH_IMP_Caption = '"
															+ cf.encode(edit_desc
																	.getText()
																	.toString())
															+ "' WHERE GCH_IMP_Elevation ='"
															+ elev
															+ "' and GCH_IMP_ImageOrder='"
															+ arrimgord[j - 1]
															+ "' and GCH_IMP_Caption='"
															+ cf.encode(photocaption[pos])
															+ "'");
													cf.hidekeyboard();
													show_savedvalue();
													cf.ShowToast("Photo Caption has been updated sucessfully.",1);
													dialog.cancel();
												} else {
													cf.ShowToast("Please enter the caption.",1);cf.hidekeyboard();
												}
											}
										});
							}
						});

						button_del.setOnClickListener(new OnClickListener() {
							public void onClick(View v) {

								AlertDialog.Builder builder = new AlertDialog.Builder(
										photosold.this);
								builder.setMessage(
										"Are you sure, Do you want to delete?")
										.setCancelable(false)
										.setPositiveButton(
												"Yes",
												new DialogInterface.OnClickListener() {
													public void onClick(
															DialogInterface dialog,
															int id) {
														// SH_IMP_INSP_ID,SH_IMP_Caption,SH_IMP_Elevation,SH_IMP_ImageOrder
														cf.gch_db.execSQL("Delete From "
																+ cf.Photo_caption
																+ "  WHERE  GCH_IMP_Elevation ='"
																+ elev
																+ "' and GCH_IMP_ImageOrder='"
																+ arrimgord[j - 1]
																+ "' and GCH_IMP_Caption='"
																+ cf.encode(photocaption[pos])
																+ "'");
														show_savedvalue();
														cf.ShowToast(
																"Photo Caption has been deleted sucessfully.",
																1);

													}
												})
										.setNegativeButton(
												"No",
												new DialogInterface.OnClickListener() {
													public void onClick(
															DialogInterface dialog,
															int id) {
														spnelevation[spinnerid]
																.setSelection(0);
														dialog.cancel();
													}
												});

								builder.show();
								dialog.cancel();
							}
						});

						dialog.show();
					}

				} catch (Exception e) {
					System.out.println("e + " + e.getMessage());
				}

			}
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.

		}

	}
	private Cursor getVideoIdFromFilePath(String filePath,
	        ContentResolver contentResolver) {


	    long videoId;
	    Log.d(TAG,"Loading file " + filePath);

	            // This returns us content://media/external/videos/media (or something like that)
	            // I pass in "external" because that's the MediaStore's name for the external
	            // storage on my device (the other possibility is "internal")
	    Uri imageUri = MediaStore.Images.Media.getContentUri("external");

	    

	    String[] projection = {MediaStore.Images.ImageColumns._ID,MediaStore.Images.Media.ORIENTATION};

	    // TODO This will break if we have no matching item in the MediaStore.
	    Cursor cursor = contentResolver.query(imageUri, projection, MediaStore.Images.ImageColumns.DATA + " LIKE ?", new String[] { filePath }, null);
	    cursor.moveToFirst();

	   
	    return cursor;
	}

	public void clicker(View v) {
		switch (v.getId()) {
		

		case R.id.browsecamera:
			Cursor c12 = cf.gch_db.rawQuery("SELECT * FROM " + cf.ImageTable
					+ " WHERE GCH_IM_SRID='" + cf.selectedhomeid
					+ "' and GCH_IM_Elevation='" + elev
					+ "' order by GCH_IM_ImageOrder", null);
			int chkrws1 = c12.getCount();
			if (chkrws1 < 4) {
				t = 1;
				startCameraActivity();
			} else {
				cf.ShowToast("You only upload 4 photos per Elevation", 1);
			}

			break;
		case R.id.browsetxt:
			Cursor c11 = cf.gch_db.rawQuery("SELECT * FROM " + cf.ImageTable
 					+ " WHERE GCH_IM_SRID='" + cf.selectedhomeid + "' and GCH_IM_Elevation='" + elev
 					+ "' order by GCH_IM_ImageOrder", null);
 			int chkrws = c11.getCount();
 			
 			maximumindb=chkrws;
 			if(maximumindb>=4)
 			{
 				cf.ShowToast("You can upload only 4 images", 1);
 			}else
 			{
 				/*c11.moveToFirst();
 				String[] selected_paht = new String[c11.getCount()];
 				
 				for(int i=0;i<c11.getCount();i++)
 				{
 					selected_paht[i]=c11.getString(c11.getColumnIndex("GCH_IM_ImageName"));
 					
 				}
 				t=0;
 				Intent reptoit1 = new Intent(ApplicationMenu.this, Select_phots.class);
 				startActivityForResult(reptoit1,121);
 				Intent reptoit1 = new Intent();
 				Bundle b=new Bundle();

 				//reptoit1.putExtra();
 				b.putStringArray("Selected_value ", selected_paht);
 				b.putInt("Maximumcount", maximumindb);
 				reptoit1.putExtras(b);
 				reptoit1.setClassName("com.idinspection","com.idinspection.Select_phots");
 				startActivityForResult(reptoit1,121);*/
 				 String source = "<b><font color=#00FF33>Loading Images. Please wait..."+ "</font></b>";
 				 pd = ProgressDialog.show(photosold.this, "",
 					Html.fromHtml(source), true);
 				 Thread thread = new Thread(photosold.this);
 				 thread.start();
 				
 			}
 	   break;
		case R.id.home:
			cf.gohome();
			break;
		case R.id.save:
			cf.ShowToast("Saved succesfully", 1);
			nxtintent();
			break;
		case R.id.changeimageorder:
			change_image_order();
			break;
		
		}
	}

	protected void startCameraActivity() {
		String fileName = "temp.jpg";
		ContentValues values = new ContentValues();
		values.put(MediaStore.Images.Media.TITLE, fileName);
		CapturedImageURI = getContentResolver().insert(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, CapturedImageURI);
		startActivityForResult(intent, 0);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		String selectedImagePath = "", picname = "";
		if (t == 0) {

			if (resultCode == RESULT_OK) {
				if (requestCode == 0) {
					Uri selectedImageUri = data.getData();
					selectedImagePath = getPath(selectedImageUri);

					String[] bits = selectedImagePath.split("/");
					picname = bits[bits.length - 1];

				}
			} else {
				selectedImagePath = "";
				// edbrowse.setText("");
			}
		} else if (t == 1) {
			switch (resultCode) {
			case 0:
				selectedImagePath = "";

				break;
			case -1:
				try {
					String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = managedQuery(CapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();
					String capturedImageFilePath = cursor
							.getString(column_index_data);
					selectedImagePath = capturedImageFilePath;
					display_taken_image(selectedImagePath);
										 show_savedvalue();
				} catch (Exception e) {
					System.out.println("my exception " + e);
				}

				break;

			}

		}

	}

	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	public int getImageOrder(int Elevationtype, String GCH_IM_SRID) {
		int ImgOrder = 0;
		Cursor c11 = cf.gch_db.rawQuery("SELECT * FROM " + cf.ImageTable
				+ " WHERE GCH_IM_SRID='" + GCH_IM_SRID
				+ "' and GCH_IM_Elevation='" + Elevationtype
				+ "' order by GCH_IM_CreatedOn DESC", null);
		int imgrws = c11.getCount();
		if (imgrws == 0) {
			ImgOrder = imgrws + 1;
		} else {
			ImgOrder = imgrws + 1;

		}
		return ImgOrder;
	}
/** change the image order starts**/
	protected void change_image_order() {
 		// TODO Auto-generated method stub
		 final Dialog dialog;
		 Bitmap[] thumbnails1;
		 String[] arrPath1,str;
		 final int[] imageid;
		int[] SH_IM_ImageOrder;
		 ImageView[] chag_img;
		 final EditText[]  chan_ed;
 		Cursor d11 = cf.gch_db.rawQuery("SELECT * FROM " + cf.ImageTable
 				+ " WHERE GCH_IM_SRID='" + cf.selectedhomeid + "' and GCH_IM_Elevation='" + elev
 				+ "' order by GCH_IM_ImageOrder", null);
 		final int rws = d11.getCount();
 		if (rws == 0) {
 			cf.ShowToast("There is no Image, So please upload the Image.",1);
 		} else {
 			String Imagepath[] = new String[rws];

 		  EditText[] et = new EditText[rws];

 		    dialog = new Dialog(photosold.this);
 			dialog.setContentView(R.layout.changeimageorder);
 			dialog.setTitle("Change Image Order");
 			dialog.setCancelable(false);
 			Button save=(Button) dialog.findViewById(R.id.Save);
 			Button close=(Button) dialog.findViewById(R.id.close);
 			
            TableLayout ln_main=(TableLayout) dialog.findViewById(R.id.changeorder);
 			try {

 				//this.count1 = d11.getCount();
 				thumbnails1 = new Bitmap[rws];
 				arrPath1 = new String[rws];
 				imageid = new int[rws];
 				SH_IM_ImageOrder = new int[rws];
 				str = new String[rws];
 				chag_img = new ImageView[rws];
 				chan_ed=new EditText[rws]; 
 				TableRow ln_sub=null;
 				d11.moveToFirst();

 				for (int i = 0; i < rws; i++) {
 					/**We create the new check box and edit box for the change image order **/
 					chag_img[i]=new ImageView(photosold.this); 
 					TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
 					TableRow.LayoutParams lp2 = new TableRow.LayoutParams(100, 100);
 					lp.setMargins(5, 10, 5, 10);
 					chag_img[i].setLayoutParams(lp2);
 					chan_ed[i]=new EditText(photosold.this);
 					//chan_ed[i].setMa))(2);
 					chan_ed[i].setInputType(InputType.TYPE_CLASS_NUMBER);
 					chan_ed[i].setLayoutParams(lp);
 					
 					InputFilter[] fArray = new InputFilter[1];
 					fArray[0] = new InputFilter.LengthFilter(etchangeordermaxLength);
 					chan_ed[i].setFilters(fArray);
 					LinearLayout li_tmp=new LinearLayout(photosold.this);
 					li_tmp.setLayoutParams(lp);
 					
 					/**We create the new check box and edit box for the change image order ends  **/
 					imageid[i] = Integer.parseInt(d11.getString(0)
 							.toString());
 					SH_IM_ImageOrder[i] = d11.getInt(d11
 							.getColumnIndex("GCH_IM_ImageOrder"));
 					arrPath1[i] = cf.decode(d11.getString(d11
 							.getColumnIndex("GCH_IM_ImageName")));
 					Bitmap b = cf.ShrinkBitmap(arrPath1[i], 130, 130);
 					thumbnails1[i] = b;
 					if(i==0)
 					{
 						
 						LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
							ln_sub = new TableRow(photosold.this);
							ln_sub.setLayoutParams(lp1);  
 					}
 					li_tmp.addView(chag_img[i]);
 					li_tmp.addView(chan_ed[i]);
 					ln_sub.addView(li_tmp);
 					chag_img[i].setImageBitmap(b);
 					chan_ed[i].setText(String.valueOf(SH_IM_ImageOrder[i]));
 					d11.moveToNext();
 					if((((i+1)%3)==0 || (i+1)==rws)&& ln_sub!=null )
						{
							ln_main.addView(ln_sub);
							LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
							ln_sub = new TableRow(photosold.this);
							ln_sub.setLayoutParams(lp1);  
						}
 					
 				}
 				close.setOnClickListener(new OnClickListener() {

 					public void onClick(View v) {
 						// TODO Auto-generated method stub
 						dialog.setCancelable(true);
 						dialog.cancel();
 					}
 				});

 				save.setOnClickListener(new OnClickListener() {

 					public void onClick(View v) {
 						// TODO Auto-generated method stub
 						// dialog.cancel();
 						Boolean boo = true;
 						String temp[] = new String[rws];
 						try {
 							for (int i = 0; i < imageid.length; i++) {
 								temp[i] = chan_ed[i].getText().toString();
 								int myorder = 0;
 								if (temp[i] != null && !temp[i].equals("")) {
 									myorder = Integer.parseInt(temp[i]);
 									if (myorder <= imageid.length
 											&& myorder != 0) {

 									} else {
 										boo = false;
 									}

 								} else {
 									boo = false;
 								}

 							}
 							
 							int length = temp.length;
 							for (int i = 0; i < length; i++) {
 								for (int j = 0; j < length; j++) {
 									if (temp[i].equals(temp[j]) && i != j) {
 										boo = false;
 									}

 								}

 							}
 							if (!boo) {
 								cf.ShowToast("Please select different Image Order.",1);
 							} else {
 								for (int i = 0; i < imageid.length; i++) {
 									cf.gch_db.execSQL("UPDATE " + cf.ImageTable
 											+ " SET GCH_IM_ImageOrder='" + temp[i]
 											+ "' WHERE GCH_IM_ID='" + imageid[i]
 											+ "'");
 								}
 								dialog.setCancelable(true);
 								dialog.cancel();
 								cf.ShowToast("Image Order saved successfully.",1);
 								show_savedvalue();
 							}
 						}

 						catch (Exception e) {

 						}
 					}
 				});
                
 				dialog.show();

 				
 			} catch (Exception m) {
 				
 			}
 		}

 	}
/** change the image order Ends**/	
/**multi image selection starts **/
	@Override
	public void run() {
		// TODO Auto-generated method stub
		File images = Environment.getExternalStorageDirectory(); 
		walkdir(images);
	    handler.sendEmptyMessage(0);
	}
	private void walkdir(File imaFile) {
		/** Get only the folders form the File arra list starts  **/
		/***directoryFilter was a filter that filter only the folders form the given folders ***/
		File[] current_fold_list =imaFile.listFiles(directoryFilter); 
		if(current_fold_list!=null)
		{
			
			pieces1=new String[current_fold_list.length];
			for (int i = 0; i < current_fold_list.length; i++) {
				   
				   if (current_fold_list[i].isDirectory()) {
	            
	            
	            pieces1[i]=current_fold_list[i].getName();
	         }
				   else
  			 	 {
  			 		
  			 	 }
			}
			
		}
		/** Get only the folders form the File array list Ends   **/
		/** Get only the Images  form the File array list Starts   **/
		Currentfile_list=imaFile.listFiles(imageFilter);
	
	
		   if (Currentfile_list != null)
		   {
			  int j=0; 
			   
			  
			  if(Currentfile_list.length<=20)
				  
			  pieces3=new String[Currentfile_list.length];
			  else
			  {
				  pieces3=new String[20];
			  }
			  /*****limit_start used for showing the next twenty photos every time we click the next button we will increase the 20 ****/  
			 
			   for (int i = limit_start; i < Currentfile_list.length && j < 20 ; i++,j++) {
				   
				 
	    		
	    			 	 if ((Currentfile_list[i].getName().endsWith(".jpg"))||(Currentfile_list[i].getName().endsWith(".jpeg"))||(Currentfile_list[i].getName().endsWith(".png"))||(Currentfile_list[i].getName().endsWith(".gif")||(Currentfile_list[i].getName().endsWith(".JPG"))
	            				 ||(Currentfile_list[i].getName().endsWith(".JPEG"))||(Currentfile_list[i].getName().endsWith(".PNG")))||(Currentfile_list[i].getName().endsWith(".gif"))||(Currentfile_list[i].getName().endsWith(".bmp"))){
	    	            	 
	    			 		 pieces3[j]=Currentfile_list[i].getName();
	    	            
	            		 }
	    			 	 else
	    			 	 {
	    			 		
	    			 	 }
	    		 
		   }
			  
		 }
		   /** Get only the Images  form the File array list Ends   **/
		   /** Set the visisbolity to  the Prev and next button layout Starts ***/
		   if(Lin_Pre_nex !=null && Currentfile_list.length>20 )
		{
			
			Lin_Pre_nex.setVisibility(View.VISIBLE);
			Rec_count_lin.setVisibility(View.VISIBLE);
 			Total_Rec_cnt.setText("Total Records : "+Currentfile_list.length+" ");
 			showing_rec_cnt.setText("Showing Record from 1 to 20");
 			prev.setVisibility(View.GONE);
 			next.setVisibility(View.VISIBLE);
			
		}
		else if(Lin_Pre_nex !=null)
		{System.out.println("Its comes into the else visible false ");

			Lin_Pre_nex.setVisibility(View.GONE);
			Rec_count_lin.setVisibility(View.VISIBLE);
			Total_Rec_cnt.setText("Total Records : "+Currentfile_list.length+" ");
 			showing_rec_cnt.setText("");
			
		}
		   /** Set the visisbolity to  the Prev and next button layout Endss ***/   
	    	 
	}
	private String[] dynamicarraysetting(String ErrorMsg,String[] pieces3) {
		// TODO Auto-generated method stub
		 try
		 {
		if(pieces3==null)
		{
			pieces3=new String[1];
			
			pieces3[0]=ErrorMsg;
		}
		else
		{
			
			String tmp[]=new String[pieces3.length+1];
			int i;
			for(i =0;i<pieces3.length;i++)
			{
				
				tmp[i]=pieces3[i];
			}
		
			tmp[tmp.length-1]=ErrorMsg;
			pieces3=null;
			pieces3=tmp.clone();
		}
		 }
		 catch(Exception e)
		 {
			 
		 }
		return pieces3;
	}
	private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

	 			final Dialog dialog = new Dialog(photosold.this);
	 			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

	 			dialog.setContentView(R.layout.selectimage);
	 	 		
	 	 		dialog.setCancelable(true);
	 	 		
	 	 		lnrlayout1_FI = (LinearLayout) dialog.findViewById(R.id.linscrollview2);
	 	 		lnrlayout2_FI = (LinearLayout) dialog.findViewById(R.id.linscrollview4);
	 	 		/***For the limit the images **/
	 	 		Lin_Pre_nex = (RelativeLayout) dialog.findViewById(R.id.linscrollview_Next);
	 	 		prev =(ImageView) dialog.findViewById(R.id.S_I_Prev);
	 	 		prev.setVisibility(View.GONE);
	 	 		next =(ImageView) dialog.findViewById(R.id.S_I_Next);
	 	 		scr2 =(ScrollView) dialog.findViewById(R.id.scr2);
	 	 		Rec_count_lin =(RelativeLayout) dialog.findViewById(R.id.Rec_count_lin);
	 	 		Total_Rec_cnt =(TextView) dialog.findViewById(R.id.Total_rec_cnt);
	 	 		showing_rec_cnt =(TextView) dialog.findViewById(R.id.showing_rec_cnt);
	 	 		ImageView Search = (ImageView) dialog.findViewById(R.id.search);
	 	 		Search.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						 //final Dialog dialog1 = new Dialog(photos.this);
						/**Search based on the folder name starts here **/
						Search_by_folder(); 
		    	       /**Search based on the folder name Ends here**/
		    			
					}
				});
	 	 		if(Currentfile_list.length<=20)
	 	 		{
	 	 			Lin_Pre_nex.setVisibility(View.GONE);
	 	 			Rec_count_lin.setVisibility(View.VISIBLE);
	 	 			Total_Rec_cnt.setText("Total Records : "+Currentfile_list.length+" ");
	 	 			showing_rec_cnt.setText("");
	 	 			
	 	 		}
	 	 		else
	 	 		{
	 	 			Lin_Pre_nex.setVisibility(View.VISIBLE);
	 	 			Rec_count_lin.setVisibility(View.VISIBLE);
	 	 			Total_Rec_cnt.setText("Total Records : "+Currentfile_list.length+" ");
	 	 			showing_rec_cnt.setText("Showing Record from 1 to 20");
	 	 		}
	 	 		prev.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						int end =limit_start;
						limit_start-=20;
						int start =limit_start;
						showing_rec_cnt.setText("Showing Records from "+(start+1) +" to "+end);
						if((limit_start)<=0)
						{
							prev.setVisibility(View.GONE);
						}
						else
						{
							prev.setVisibility(View.VISIBLE);
						}
						if((limit_start+20)<=Currentfile_list.length)
						{
							next.setVisibility(View.VISIBLE);
						}
						else
						{
							next.setVisibility(View.GONE);
						}	
						if (Currentfile_list != null)
						   {
							  int j=0; 
							   
							
							 
							  /*****limit_start used for showing the next twenty photos every time we click the next button we will increase the 20 ****/  
							  if(limit_start>=0)
							  {
								  pieces3=new String[20];
							   for (int i = limit_start; i < Currentfile_list.length && j < 20 ; i++,j++) {
								   
								 
					    		
					    			 	 if ((Currentfile_list[i].getName().endsWith(".jpg"))||(Currentfile_list[i].getName().endsWith(".jpeg"))||(Currentfile_list[i].getName().endsWith(".png"))||(Currentfile_list[i].getName().endsWith(".gif")||(Currentfile_list[i].getName().endsWith(".JPG"))
					            				 ||(Currentfile_list[i].getName().endsWith(".JPEG"))||(Currentfile_list[i].getName().endsWith(".PNG")))||(Currentfile_list[i].getName().endsWith(".gif"))||(Currentfile_list[i].getName().endsWith(".bmp"))){
					    	            	 
					    			 		 pieces3[j]=Currentfile_list[i].getName();
					    	            
					            		 }
					    		 
						   }
							  }  
						 }
						
					//	walkdir(images);
						
						dynamicimagesparent();
					
						 dynamicimageschild();
					}
				});
	 	 		next.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						
						limit_start+=20;
						int start =limit_start;
						int end=0;
						
						
						
						if((limit_start+20)<=Currentfile_list.length)
						{
							next.setVisibility(View.VISIBLE);
						
						}
						else
						{
							next.setVisibility(View.GONE);
						}
						if((limit_start)<=0)
						{
							prev.setVisibility(View.GONE);
						}
						else
						{
							prev.setVisibility(View.VISIBLE);
						}
							
						if (Currentfile_list != null)
						   {
							  int j=0; 
							   
							
							 if(Currentfile_list.length>(limit_start+20))
							 {
								 pieces3=new String[20];
								 end =limit_start+20;
							 }
							 else
							 {
								 pieces3=new String[(Currentfile_list.length-limit_start)];
								 end =limit_start+( Currentfile_list.length-limit_start);
							 }
							  /*****limit_start used for showing the next twenty photos every time we click the next button we will increase the 20 ****/  
							  if(limit_start>=0)
							  {
								 
							   for (int i = limit_start; i < Currentfile_list.length && j < 20 ; i++,j++) {
								   
								 
					    		
					    			 	 if ((Currentfile_list[i].getName().endsWith(".jpg"))||(Currentfile_list[i].getName().endsWith(".jpeg"))||(Currentfile_list[i].getName().endsWith(".png"))||(Currentfile_list[i].getName().endsWith(".gif")||(Currentfile_list[i].getName().endsWith(".JPG"))
					            				 ||(Currentfile_list[i].getName().endsWith(".JPEG"))||(Currentfile_list[i].getName().endsWith(".PNG")))||(Currentfile_list[i].getName().endsWith(".gif"))||(Currentfile_list[i].getName().endsWith(".bmp"))){
					    	            	 
					    			 		 pieces3[j]=Currentfile_list[i].getName();
					    	            
					            		 }
					    		 
						   }
							  }  
						 }
						
						showing_rec_cnt.setText("Showing Records from "+(start+1) +" to "+end);
						dynamicimagesparent();
					
						 dynamicimageschild();
			
					}
				});
				
	 	 		/***For the limit the images ends  **/
	 	 		titlehead=(TextView) dialog.findViewById(R.id.titleheade);
	 	 		chaild_titleheade=(TextView) dialog.findViewById(R.id.chaild_titleheade);
	 	 		backbtn = (Button) dialog.findViewById(R.id.back);
	 	 		cancelBtn = (Button) dialog.findViewById(R.id.cancel);
	 	 		selectImgBtn = (Button) dialog.findViewById(R.id.savenext);
	 	 		pd.dismiss();
	 	 		if(dialog.isShowing())
	 	 		{
	 	 			dialog.setCancelable(true);
	 	 			dialog.cancel();
	 	 		}
	 	 			dialog.setCancelable(false);
	 	 			dialog.show();
	 	 			
	 		         dynamicimagesparent();
	 		    	 dynamicimageschild();
	 	 		
	 	 		cancelBtn.setOnClickListener(new OnClickListener() {
	 								
					public void onClick(View v) {				
	 					
	 					if(!"".equals(finaltext)){
	 						finaltext="";
	 					}
	 					selectedcount=0;
	 	 				selectedtestcaption=null;		
	 	 				maximumindb=0;
	 	 				pieces1=null;
	 	 				limit_start=0;
	 	 				pieces3=null;
	 	 				dialog.setCancelable(true);
	 	 				cf.ShowToast("Images has not been selected.", 1);
	 					dialog.cancel();
	 				}
	 	 		});
	 	 		backbtn.setOnClickListener(new OnClickListener() {
	 	 			

					public void onClick(View v) { 
	 	 				if(!titlehead.getText().toString().equals("/mnt/sdcard/")){
	 	 				backclick_FI=1;
	 				pieces1=null;pieces3=null;limit_start=0;
	 				
	 				
	 	 				String value = titlehead.getText().toString();
	 					String[] bits = value.split("/");
	 					String picname = bits[bits.length - 1];
	 					
	 					  
	 					
	 					if(value.equals("mnt/sdcard/"))
	 					{
	 						finaltext="";
	 						path  = value.replace(picname+"/", "");
	 						//path = value;
	 						
	 					}
	 					else
	 					{
	 						String tempstr = value.substring(0,value.length()-1);
		 					int bits1 = tempstr.lastIndexOf("/");
		 					String pathofimage1=tempstr.substring(0,bits1+1);
		 					path  = pathofimage1;
	 						 path = value.replace(picname+"/", "");
	 						 System.out.println("Back pressed "+path);
	 					}
	 					
	 					titlehead.setText(path);
	 					finaltext=path;
	 					 images = new File(path);
	 					//Currentfile_list=f.listFiles();
	 					String[] bits1 = path.split("/");
						String picname1 = bits1[bits1.length - 1];
						chaild_titleheade.setText(picname1); // set the chaild title
	 					walkdir(images);
	 					dynamicimagesparent();
	 					dynamicimageschild();
	 				}
	 	 				else
	 	 				{
	 	 					cf.ShowToast("You can not go back ", 1);
	 	 					
	 	 				}
	 	 			}
	 			});

	 	 		selectImgBtn.setOnClickListener(new OnClickListener() {
	 	 			public void onClick(View v) {
	 	 				if(selectedcount!=0)
	 	 				{
		 	 				for(int i=0;i<selectedcount;i++)
		 	 				{
		 	 					cf.gch_db.execSQL("INSERT INTO "
			 							+ cf.ImageTable
			 							+ " (GCH_IM_SRID,GCH_IM_Ques_Id,GCH_IM_Elevation,GCH_IM_ImageName,GCH_IM_Description,GCH_IM_Nameext,GCH_IM_CreatedOn,GCH_IM_ModifiedOn,GCH_IM_ImageOrder)"
			 							+ " VALUES ('" + cf.selectedhomeid + "','','"
			 							+ elev + "','"
			 							+ cf.encode(selectedtestcaption[i]) + "','"
			 							+ cf.encode(name + (maximumindb+i+1))
			 							+ "','','"
			 							+ cf.datewithtime + "','" +cf.datewithtime + "','" + (maximumindb+1+i) + "')");
		 	 					
		 	 					
		 	 				}
		 	 				cf.ShowToast("Image saved successfully", 1);
		 	 				selectedcount=0;
		 	 				selectedtestcaption=null;		
		 	 				maximumindb=0;
		 	 				pieces1=null;
		 	 				
		 	 				pieces3=null;
		 					if(!"".equals(finaltext)){
		 						finaltext="";
		 					}
		 					dialog.setCancelable(true);
		 					dialog.cancel();
		 					show_savedvalue();
	 	 				}else
	 	 				{
	 	 					cf.ShowToast("Please select atleast one image to upload .", 1);
	 	 				}
		 	 			}
		 	 		}); /**Select ends**/
	 	
        }
};
	
	private void dynamicimagesparent() {
	// TODO Auto-generated method stub
	ImageView[] imgview;
	TextView[] tvstatusparent;
	lnrlayout1_FI.removeAllViews();
	
	if(finaltext.equals("")||finaltext.equals("null"))
	{			
		titlehead.setText("/mnt/sdcard/");
		chaild_titleheade.setText("sdcard");
	}
	else
	{
		
		if(backclick_FI==1)
		{
			titlehead.setText(path);backclick_FI=0;
		}
		else
		{
			titlehead.setText(finaltext);
		}

	}
	
	
	if(pieces1!=null && pieces1.length>0)
	{
		tvstatusparent = new TextView[pieces1.length];
		imgview = new ImageView[pieces1.length];		
		
		
	for (int i = 0; i < pieces1.length; i++) {
		LinearLayout.LayoutParams paramschk5 = new LinearLayout.LayoutParams(
				ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);	
		LinearLayout l2 = new LinearLayout(this);
		l2.setOrientation(LinearLayout.HORIZONTAL);
		lnrlayout1_FI.addView(l2,paramschk5);

		LinearLayout lchkbox = new LinearLayout(this);
		LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
				50, ViewGroup.LayoutParams.WRAP_CONTENT);
		paramschk.leftMargin = 10;
		paramschk.topMargin = 5;
		paramschk.bottomMargin = 15;
		l2.addView(lchkbox);

		imgview[i] = new ImageView(this);//imgview[i].setPadding(30, 0, 0, 0);
		imgview[i].setBackgroundResource(R.drawable.allfilesicon);
		
		lchkbox.addView(imgview[i], paramschk);

		LinearLayout ltextbox = new LinearLayout(this);
		LinearLayout.LayoutParams paramstext = new LinearLayout.LayoutParams(
				ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		paramstext.topMargin = 5;
		paramstext.leftMargin = 15;
		l2.addView(ltextbox);
		
		tvstatusparent[i] = new TextView(this);
		tvstatusparent[i].setText(pieces1[i]);
		tvstatusparent[i].setTextColor(Color.BLACK);
		tvstatusparent[i].setMinimumWidth(100);
		map1.put("tvstatusparent" + i, tvstatusparent[i]);
		tvstatusparent[i].setTag("tvstatusparent" + i);
		imgview[i].setTag("tvstatusparent" + i);
		ltextbox.addView(tvstatusparent[i], paramstext);
		
		tvstatusparent[i].setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				pieces1=null;pieces3=null;
				String source = "<b><font color=#00FF33>Loading Images. Please wait..."+ "</font></b>";
				pd = ProgressDialog.show(photosold.this, "",
 					Html.fromHtml(source), true);
				pd.setCancelable(false);
				String getidofselbtn = v.getTag().toString();
				String repidofselbtn = getidofselbtn.replace("tvstatusparent","");
				final int cvrtstr = Integer.parseInt(repidofselbtn) + 1;
				System.out.println("cvrtstr "+cvrtstr);
				TextView temp_st = map1.get(getidofselbtn);
				
				imagessub(cvrtstr, temp_st);
				pd.setCancelable(true);
				pd.dismiss();
			}
			
		});
		imgview[i].setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				pieces1=null;pieces3=null;
				//pieces3=null;
				String source = "<b><font color=#00FF33>Loading Images. Please wait..."+ "</font></b>";
 				 pd = ProgressDialog.show(photosold.this, "",
 					Html.fromHtml(source), true);
 				 pd.setCancelable(false);
 				 
				String getidofselbtn = v.getTag().toString();
				String repidofselbtn = getidofselbtn.replace("tvstatusparent","");
				final int cvrtstr = Integer.parseInt(repidofselbtn) + 1;
				TextView temp_st = map1.get(getidofselbtn);
				System.out.println("bac"+backclick_FI);
				if(backclick_FI==1)
				{
					System.out.println("bac111");
				String[] bits = finaltext.split("/");
				String picname1 = bits[bits.length - 1];
				chaild_titleheade.setText(picname1); // set the chaild title 
				}
				else
				{
					System.out.println("bac111 else"+path);
					String[] bits = path.split("/");	System.out.println("bits images ");
					String picname1 = bits[bits.length - 1];	System.out.println("picname1 images "+picname1);
					chaild_titleheade.setText(picname1); 	System.out.println("bac111 chaild_titleheade "); // set the chaild title 
				}
				System.out.println("bac111 images ");
				imagessub(cvrtstr, temp_st);System.out.println("bac111 imagecompleted");
				pd.setCancelable(true);
				 pd.dismiss();
			}
			
		});
		
	}
	}	
	else
	{
		LinearLayout l2 = new LinearLayout(this);
		l2.setOrientation(LinearLayout.HORIZONTAL);
		lnrlayout1_FI.addView(l2);

		LinearLayout lchkbox = new LinearLayout(this);
		LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
				ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		paramschk.leftMargin = 15;
		l2.addView(lchkbox);
		
		
		TextView tvstatus1 = new TextView(this);
		tvstatus1.setText("No Sub Folders");
		tvstatus1.setTextColor(Color.BLACK);
		tvstatus1.setMinimumWidth(200);
		lchkbox.addView(tvstatus1,paramschk);
		
	}
}
	private void dynamicimageschild() {
	
	ImageView[] imgview,imgview1;
	final CheckBox[] chkbox;
	Bitmap[] thumbnails1;
	lnrlayout2_FI.removeAllViews();
	
	
	if((pieces3==null || pieces3.length<1))
	{
		LinearLayout l2 = new LinearLayout(this);
		l2.setOrientation(LinearLayout.HORIZONTAL);
		lnrlayout2_FI.addView(l2);

		LinearLayout lchkbox = new LinearLayout(this);
		LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
				ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		paramschk.leftMargin = 15;
		l2.addView(lchkbox);
		
		
		TextView tvstatus1 = new TextView(this);
		tvstatus1.setText("No Records Found");
		tvstatus1.setTextColor(Color.BLACK);
		tvstatus1.setMinimumWidth(200);
		lchkbox.addView(tvstatus1,paramschk);
	}
	
	else
	{	
		tvstatus = new TextView[pieces3.length];
		//imgview = new ImageView[pieces3.length];
		chkbox = new CheckBox[pieces3.length];
		imgview1 = new ImageView[pieces3.length];
		thumbnails1 = new Bitmap[pieces3.length];
		for (int i = 0; i < pieces3.length; i++) {
			LinearLayout.LayoutParams paramschk5 = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);	
			LinearLayout l2 = new LinearLayout(this);
			l2.setOrientation(LinearLayout.HORIZONTAL);
			lnrlayout2_FI.addView(l2,paramschk5);
			LinearLayout lchkbox = new LinearLayout(this);
			LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
					50, ViewGroup.LayoutParams.WRAP_CONTENT);
			paramschk.leftMargin = 5;
			paramschk.topMargin = 5;
			paramschk.bottomMargin = 15;
			l2.addView(lchkbox);
			LinearLayout ltextbox = new LinearLayout(this);
			LinearLayout.LayoutParams paramstext = new LinearLayout.LayoutParams(
					300, ViewGroup.LayoutParams.WRAP_CONTENT);
			imgview1[i] = new ImageView(this);
			chkbox[i] = new CheckBox(this);
		
				chkbox[i].setPadding(30, 0, 0, 0);
				lchkbox.addView(chkbox[i], paramschk);
				paramstext.topMargin = 5;
				paramstext.leftMargin = 15;
				l2.addView(ltextbox);
				
				LinearLayout ltextbox6= new LinearLayout(this);
				LinearLayout.LayoutParams paramstext6 = new LinearLayout.LayoutParams(
						75, 75);
				//paramstext6.topMargin = 5;
				paramstext6.leftMargin = 10;
				ltextbox.addView(ltextbox6);
				String j="0";
				BitmapFactory.Options o = new BitmapFactory.Options();
				o.inJustDecodeBounds = true;
				try {
					BitmapFactory.decodeStream(new FileInputStream(titlehead.getText().toString()+pieces3[i]),
							null, o);
					j = "1";
				} catch (FileNotFoundException e) {
					j = "2";
				}
				if (j.equals("1")) {
					final int REQUIRED_SIZE = 100;
					int width_tmp = o.outWidth, height_tmp = o.outHeight;
					int scale = 1;
					while (true) {
						if (width_tmp / 2 < REQUIRED_SIZE
								|| height_tmp / 2 < REQUIRED_SIZE)
							break;
						width_tmp /= 2;
						height_tmp /= 2;
						scale *= 2;
					}
					// Decode with inSampleSize
					BitmapFactory.Options o2 = new BitmapFactory.Options();
					o2.inSampleSize = scale;
					Bitmap bitmap = null;
					try {
						
						bitmap = BitmapFactory.decodeStream(new FileInputStream(
								titlehead.getText().toString()+pieces3[i]), null, o2);
						
					
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					BitmapDrawable bmd = new BitmapDrawable(bitmap);
					imgview1[i].setImageDrawable(bmd);
				ltextbox6.addView(imgview1[i], paramstext6);
				
				}
				LinearLayout ltextbox1= new LinearLayout(this);
				LinearLayout.LayoutParams paramstext1 = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				paramstext1.topMargin = 5;
				paramstext1.leftMargin = 15;
				ltextbox6.addView(ltextbox1);
				tvstatus[i] = new TextView(this);
				tvstatus[i].setText(pieces3[i]);
				tvstatus[i].setTextColor(Color.BLACK);
				tvstatus[i].setMinimumWidth(100);
				tvstatus[i].setTag("tvstatus" + i);
				imgview1[i].setTag("tvstatus" + i);
				chkbox[i].setTag("tvstatus" + i);
				for(int k=0;k<selectedcount;k++)
				{
				if(selectedtestcaption[k].equals(titlehead.getText().toString()+pieces3[i]))
					{
						
						chkbox[i].setChecked(true);
					}
				}
				map1.put("tvstatus" + i, tvstatus[i]);
				ltextbox1.addView(tvstatus[i], paramstext1);
				
				
				
			chkbox[i].setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					String getidofselbtn = v.getTag().toString();
					String repidofselbtn = getidofselbtn.replace("tvstatus","");
					int chk = Integer.parseInt(repidofselbtn);
					final int cvrtstr = Integer.parseInt(repidofselbtn) + 1;
					TextView temp_st = map1.get(getidofselbtn);
					
					if(chkbox[chk].isChecked()==false)
					{
						selectedtestcaption=Delete_from_array(selectedtestcaption,titlehead.getText().toString()+temp_st.getText().toString());
						selectedcount=selectedcount-1;
						
					}
					else
					{
						Bitmap b=ShrinkBitmap(titlehead.getText().toString()+temp_st.getText().toString(), 400, 400);
						if(b!=null){		
						Cursor c11=null;
						int isexist=0;
							try{
								c11 =cf.gch_db.rawQuery("SELECT * FROM "+ cf.ImageTable + " WHERE GCH_IM_SRID='"+cf.selectedhomeid+"' and " +
										"GCH_IM_ImageName='"+cf.encode(titlehead.getText().toString()+temp_st.getText().toString())+ "'  and GCH_IM_Elevation='"+elev+"' order by GCH_IM_ImageOrder",null);
								
											
											isexist = c11.getCount();
							}catch(Exception e)
							{
								cf.Error_LogFile_Creation("Problem in checking for the image is exisit or not in image selection in galary filee multi selection page photos Error="+e.getMessage());
							}
					
									if (isexist == 0) {
										if(cf.common(titlehead.getText().toString()+pieces3[chk]))
										{
											if(maximumindb+selectedcount>=4)
											{
												chkbox[chk].setChecked(false);
												cf.ShowToast("You can upload only 4 images.", 1);
											}
											else
											{
																							
											//maximumindb
											selectedtestcaption=dynamicarraysetting(titlehead.getText().toString()+pieces3[chk],selectedtestcaption);
											selectedcount++;
											}
										}
										else
										{
											chkbox[chk].setChecked(false);
											cf.ShowToast("Please select image less than 2MB.",1);
										}
									
								 
									} else {
										
										chkbox[chk].setChecked(false);
										cf.ShowToast("Selected Image has been uploaded already. Please select another Image.",1);
									}
					
					}
						
					else
					{
						chkbox[chk].setChecked(false);
						cf.ShowToast("This image is not a supported format. You can'table to upload.", 1);
					}
					}
				}
				});
			imgview1[i].setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					String getidofselbtn = v.getTag().toString();
					final String repidofselbtn1= getidofselbtn.replace("tvstatus","");
					int cvrtstr1 = Integer.parseInt(repidofselbtn1) + 1;
					TextView temp_st = map1.get(getidofselbtn);
					if ((titlehead.getText().toString().endsWith(".jpg"))||(titlehead.getText().toString().endsWith(".jpeg"))||(titlehead.getText().toString().endsWith(".png"))||(titlehead.getText().toString().endsWith(".gif")||(titlehead.getText().toString().endsWith(".JPG"))
			   				 ||(titlehead.getText().toString().endsWith(".JPEG"))||(titlehead.getText().toString().endsWith(".PNG")))||(titlehead.getText().toString().endsWith(".gif"))||(titlehead.getText().toString().endsWith(".bmp"))){
						
						
						String[] bits = titlehead.getText().toString().split("/");
						
						
						String picname = bits[bits.length - 1];
						String val = titlehead.getText().toString().replace(picname, temp_st.getText().toString());
						
						titlehead.setText(val);
					
					}
					else
					{
						if(titlehead.getText().toString()=="mnt/sdcard/")
						{
							titlehead.setText(titlehead.getText().toString()+temp_st.getText().toString());
						}
						else
						{
							titlehead.setText(titlehead.getText().toString()+temp_st.getText().toString());
						}
							
					}
					final Dialog dialog1 = new Dialog(photosold.this);
	    	 		dialog1.setContentView(R.layout.listview1);
	    	 		dialog1.setTitle("Select Images");
	    	 		dialog1.setCancelable(true);
	    	 		ImageView imgview2 = (ImageView) dialog1.findViewById(R.id.full_image);
	    	 		Button selectBtn = (Button) dialog1.findViewById(R.id.selectBtn);
	    	 		Button cancelBtn = (Button) dialog1.findViewById(R.id.cancelBtn);
	    	 		final TextView pathofimage = (TextView) dialog1.findViewById(R.id.imagepath);
	    	 		pathofimage.setText(titlehead.getText().toString());
	    	 		
	    			Bitmap b = cf.ShrinkBitmap(titlehead.getText().toString(), 130, 130);
	    			imgview2.setImageBitmap(b);
	    			
	    			cancelBtn.setOnClickListener(new OnClickListener() {
	    				public void onClick(View v) {
	    					//pieces1=null;pieces2=null;pieces3=null;
	    					String value = titlehead.getText().toString();
	    					String[] bits = value.split("/");
	    					String picname = bits[bits.length - 1];
	    					
	    					titlehead.setText(value.replace(picname, ""));
	    					dialog1.setCancelable(true);
	    					dialog1.cancel();
	    				}
	    			});
	    			selectBtn.setOnClickListener(new OnClickListener() {
	    				public void onClick(View v) { 
	    					
	    						Cursor c11=null;
								int isexist=0;
							try{
								c11 =cf.gch_db.rawQuery("SELECT * FROM "+ cf.ImageTable + " WHERE GCH_IM_SRID='"+cf.selectedhomeid+"' and " +
										"GCH_IM_ImageName='"+cf.encode(titlehead.getText().toString())+ "' and GCH_IM_Elevation='"+elev+"' order by GCH_IM_ImageOrder",null);
											
											isexist = c11.getCount();
							}catch(Exception e)
							{
								cf.Error_LogFile_Creation("Problem in checking for the image is exisit or not in image selection in galary filee multi selection page photos Error="+e.getMessage());
							}
	    						 isexist = c11.getCount();
								
								if (isexist == 0) {
									
									if(chkbox[Integer.parseInt(repidofselbtn1)].isChecked())
									{
										cf.ShowToast("You have already slected this ", 1);
									}
									else
									{
										if(cf.common(pathofimage.getText().toString()))
										{
											if(maximumindb+selectedcount>=4)
											{
												cf.ShowToast("You can upload only 4 images.", 1);
											}
											else
											{
											selectedtestcaption=dynamicarraysetting(pathofimage.getText().toString(),selectedtestcaption );
											selectedcount++;
											chkbox[Integer.parseInt(repidofselbtn1)].setChecked(true);
											}
										}
										else
										{
											cf.ShowToast("Please select image less than 2MB",1);
											chkbox[Integer.parseInt(repidofselbtn1)].setChecked(false);
										}
									}
							 
								} else {
									
									chkbox[Integer.parseInt(repidofselbtn1)].setChecked(false);
									cf.ShowToast("Selected Image has been uploaded already. Please select another Image.",1);
								}
	    					String value = titlehead.getText().toString();
	    					String[] bits = value.split("/");
	    					String picname = bits[bits.length - 1];
	    					
	    					titlehead.setText(value.replace(picname, ""));
	    					dialog1.setCancelable(true);
	    					dialog1.cancel();
	    				}
	    			});
	    			dialog1.setCancelable(false);
	    			dialog1.show();
				}
			});
		}
		if(scr2!=null)
		{
			scr2.scrollTo(0, 0);
		
		}
		
		}
	

}
	protected String[] Delete_from_array(String[] selectedtestcaption2,
		String txt) {
	
	// TODO Auto-generated method stub
	String tmp[]=new String[selectedtestcaption2.length-1];
	int j=0;
	if(selectedtestcaption2!=null)
	{
		for(int i=0;i<selectedtestcaption2.length;i++)
		{
			if(!selectedtestcaption2[i].equals(txt))
			{
				tmp[j]=selectedtestcaption2[i];
				j++;
			}
			else
			{
				
			}
		}
		
	}
	return tmp;
}
	protected void imagessub(int cvrtstr, TextView temp_st2) {
	String value = titlehead.getText().toString();
	
	if(value=="/mnt/sdcard/")
	{
		
	titlehead.setText(value+temp_st2.getText().toString()+"/");
	}
	else
	{
		
		
		if ((value.endsWith(".jpg"))||(value.endsWith(".jpeg"))||(value.endsWith(".png"))||(value.endsWith(".gif")||(value.endsWith(".JPG"))
				 ||(value.endsWith(".JPEG"))||(value.endsWith(".PNG")))||(value.endsWith(".gif"))||(value.endsWith(".bmp")))
		{
			
			System.out.println("end sithe");
			
			String[] bits = value.split("/");
			String picname = bits[bits.length - 1];


				String val = value.replace(picname, temp_st2.getText().toString());
				titlehead.setText(val);
			}
			else
			{
				System.out.println("dfdfd sithe");
				titlehead.setText(value+temp_st2.getText().toString()+"/");
			}
	} 
		if ((value.endsWith(".jpg"))||(value.endsWith(".jpeg"))||(value.endsWith(".png"))||(value.endsWith(".gif")||(value.endsWith(".JPG"))
				 ||(value.endsWith(".JPEG"))||(value.endsWith(".PNG")))||(value.endsWith(".gif"))||(value.endsWith(".bmp"))){
			final Dialog dialog = new Dialog(this);
	 		dialog.setContentView(R.layout.listview1);
	 		dialog.setTitle("Select Images");
	 		dialog.setCancelable(true);
	 		ImageView imgview2 = (ImageView) dialog.findViewById(R.id.full_image);
	 		Button selectBtn = (Button) dialog.findViewById(R.id.selectBtn);
	 		cancelBtn = (Button) dialog.findViewById(R.id.cancelBtn);
	 	
	 		BitmapFactory.Options o2 = new BitmapFactory.Options();
			int scale = 0;
			o2.inSampleSize = scale;
			Bitmap bitmap = null;
			try {
				bitmap = BitmapFactory.decodeStream(new FileInputStream(titlehead.getText().toString()), null, o2);
				BitmapDrawable bmd = new BitmapDrawable(bitmap);
				imgview2.setImageDrawable(bmd);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			cancelBtn.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dialog.cancel();
				}
			});
			dialog.show();
		}
		else
		{
			System.out.println("dfdfd sithdsfdfdfe"+titlehead.getText().toString());
			finaltext = titlehead.getText().toString();//System.out.println("finaltext"+finaltext);
			 images = new File(titlehead.getText().toString());
		
			String[] bits = titlehead.getText().toString().split("/");
			String picname1 = bits[bits.length - 1];System.out.println("picnamef"+picname1);
			chaild_titleheade.setText(picname1); // set the chaild title
			//gete
			//getExternalFilesDir();
			//File f = new File("."); // current directory

			
			walkdir(images);   	
			dynamicimagesparent();
		}
			 dynamicimageschild();
}
	public 	Bitmap ShrinkBitmap(String file, int width, int height) { try {
		BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
		bmpFactoryOptions.inJustDecodeBounds = true;
		Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
	
		int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
				/ (float) height);
		int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
				/ (float) width);
	
		if (heightRatio > 1 || widthRatio > 1) {
			if (heightRatio > widthRatio) {
				bmpFactoryOptions.inSampleSize = heightRatio;
			} else {
				bmpFactoryOptions.inSampleSize = widthRatio;
			}
		}
	
		bmpFactoryOptions.inJustDecodeBounds = false;
		bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
		return bitmap;
	} catch (Exception e) {
		return null;
	}
	
	}
/**multi image selection ends  Ends**/
	private void nxtintent() {
 		// TODO Auto-generated method stub
 	
 		if (elev == 1) {
 		  Intent myintent =new Intent(photosold.this,photosold.class);
 		  cf.putExtras(myintent);
 		  myintent.putExtra("type", 52);
 		 startActivity(myintent);
 		} else if (elev == 2) {
 			Intent myintent =new Intent(photosold.this,photosold.class);
	 		  cf.putExtras(myintent);
	 		 myintent.putExtra("type", 53);
	 		 startActivity(myintent);
 		} else if (elev == 3) {
 			Intent myintent =new Intent(photosold.this,photosold.class);
	 		  cf.putExtras(myintent);
	 		 myintent.putExtra("type", 54);
	 		startActivity(myintent);
 		} else if (elev == 4) {
 			Intent myintent =new Intent(photosold.this,photosold.class);
	 		  cf.putExtras(myintent);
	 		 myintent.putExtra("type", 55);
	 		startActivity(myintent);
 		} else if (elev == 5) {
 			Intent myintent =new Intent(photosold.this,Feedback.class);
	 		  cf.putExtras(myintent);
	 		 startActivity(myintent);
	 		 
 		}
 	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
 		// replaces the default 'Back' button action
 		if (keyCode == KeyEvent.KEYCODE_BACK) {
 			if(cf.strschdate.equals("")){
 				cf.ShowToast("Layout navigation without scheduling not allowed. ", 1);
 			}else{
 			if (elev == 1) {
		 		  Intent myintent =new Intent(photosold.this,GeneralHazards.class);
		 		  cf.putExtras(myintent);
		 		 
		 		startActivity(myintent);
		 		} else if (elev == 2) {
		 			Intent myintent =new Intent(photosold.this,photosold.class);
			 		  cf.putExtras(myintent);
			 		 myintent.putExtra("type", 51);
			 		startActivity(myintent);
		 		} else if (elev == 3) {
		 			Intent myintent =new Intent(photosold.this,photosold.class);
			 		  cf.putExtras(myintent);
			 		 myintent.putExtra("type", 52);
			 		startActivity(myintent);
		 		} else if (elev == 4) {
		 			Intent myintent =new Intent(photosold.this,photosold.class);
			 		  cf.putExtras(myintent);
			 		 myintent.putExtra("type", 53);
			 		startActivity(myintent);
		 		} else if (elev == 5) {
		 			Intent myintent =new Intent(photosold.this,photosold.class);
			 		  cf.putExtras(myintent);
			 		 myintent.putExtra("type", 54);
			 		 startActivity(myintent);
			 		 
		 		}
 			}
 			return true;
 		}
 		return super.onKeyDown(keyCode, event);
 	}
	
	public void display_taken_image(final String slectimage)
	{
		 System.gc();
		 globalvar=0;
		
		BitmapFactory.Options o = new BitmapFactory.Options();
 		o.inJustDecodeBounds = true;
	
 		final Dialog dialog1 = new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alert);
		
		/** get the help and update relative layou and set the visbilitys for the respective relative layout */
		LinearLayout Re=(LinearLayout) dialog1.findViewById(R.id.maintable);
		Re.setVisibility(View.GONE);
		LinearLayout Reup=(LinearLayout) dialog1.findViewById(R.id.updateimage);
		Reup.setVisibility(View.GONE);
		LinearLayout camerapic=(LinearLayout) dialog1.findViewById(R.id.cameraimage);
		camerapic.setVisibility(View.VISIBLE);
		TextView tvcamhelp = (TextView)dialog1.findViewById(R.id.camtxthelp);
		tvcamhelp.setText("Save Picture");
		tvcamhelp.setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
		 ((RelativeLayout)dialog1.findViewById(R.id.cam_photo_update)).setVisibility(View.VISIBLE);
		((RelativeLayout)dialog1.findViewById(R.id.camaddcaption)).setVisibility(View.GONE);
		((TextView) dialog1.findViewById(R.id.elevtxt)).setVisibility(View.GONE);
		((Spinner) dialog1.findViewById(R.id.cameraelev)).setVisibility(View.GONE);
		/** get the help and update relative layou and set the visbilitys for the respective relative layout */
		
		final ImageView upd_img = (ImageView) dialog1.findViewById(R.id.cameraimg);
		Button btn_helpclose = (Button) dialog1.findViewById(R.id.camhelpclose);
		//((Button) dialog1.findViewById(R.id.camsave)).setVisibility(View.GONE);
		
		final Button btn_save = (Button) dialog1.findViewById(R.id.camsave);
		
		Button rotatecamleft = (Button) dialog1.findViewById(R.id.rotatecamimgleft);
		Button rotatecamright = (Button) dialog1.findViewById(R.id.rotatecamright);
		
		try {
				BitmapFactory.decodeStream(new FileInputStream(slectimage),
						null, o);
				final int REQUIRED_SIZE = 400;
 			int width_tmp = o.outWidth, height_tmp = o.outHeight;
 			int scale = 1;
 			while (true) {
 				if (width_tmp / 2 < REQUIRED_SIZE
 						|| height_tmp / 2 < REQUIRED_SIZE)
 					break;
 				width_tmp /= 2;
 				height_tmp /= 2;
 				scale *= 2;
 				BitmapFactory.Options o2 = new BitmapFactory.Options();
	 			o2.inSampleSize = scale;
	 			Bitmap bitmap = null;
	 	    	bitmap = BitmapFactory.decodeStream(new FileInputStream(
	 	    			slectimage), null, o2);
	 	       BitmapDrawable bmd = new BitmapDrawable(bitmap);
 			   upd_img.setImageDrawable(bmd);
 			}
		

	}
		catch(Exception e)
		{
			
		}
		btn_save.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	
            	if(globalvar==0)
        		{

            	globalvar=1;
            	btn_save.setVisibility(v.GONE);
            	String[] bits = slectimage.split("/");
        		String picname = bits[bits.length - 1];
        		Cursor c15 = cf.gch_db.rawQuery("SELECT * FROM "
        				+ cf.ImageTable + " WHERE GCH_IM_SRID='"
        				+ cf.selectedhomeid + "' and GCH_IM_Elevation='"
        				+ elev + "'", null);
        		int count_tot = c15.getCount();
        		int ImgOrder = getImageOrder(elev, cf.selectedhomeid);
        		try {
        			cf.gch_db.execSQL("INSERT INTO "
        					+ cf.ImageTable
        					+ " (GCH_IM_SRID,GCH_IM_Ques_Id,GCH_IM_Elevation,GCH_IM_ImageName,GCH_IM_Description,GCH_IM_Nameext,GCH_IM_CreatedOn,GCH_IM_ModifiedOn,GCH_IM_ImageOrder)"
        					+ " VALUES ('" + cf.selectedhomeid + "','','"
        					+ elev + "','" + cf.encode(slectimage)
        					+ "','" + cf.encode(name + (count_tot + 1))
        					+ "','" + cf.encode(picname) + "','"
        					+ cf.datewithtime + "','" + cf.datewithtime
        					+ "','" + ImgOrder + "')");
        			//dialog1.dismiss();
        			
        			/**Save the rotated value in to the external stroage place **/
					if(currnet_rotated>0)
					{ 

						try
						{
							/**Create the new image with the rotation **/
							String current=MediaStore.Images.Media.insertImage(getContentResolver(), rotated_b, "My bitmap", "My rotated bitmap");
							 ContentValues values = new ContentValues();
							  values.put(MediaStore.Images.Media.ORIENTATION, 0);
							  photosold.this.getContentResolver().update(Uri.parse(current), values, MediaStore.Images.Media.DATA+ "=?", new String[] { current } );
							
							
							if(current!=null)
							{
							String path=getPath(Uri.parse(current));
							File fout = new File(slectimage);
							fout.delete();
							/** delete the selected image **/
							File fin = new File(path);
							/** move the newly created image in the slected image pathe ***/
							fin.renameTo(new File(slectimage));
							show_savedvalue();
							
						}
						} catch(Exception e)
						{
							System.out.println("Error occure while rotate the image "+e.getMessage());
						}
						
					}
				
				/**Save the rotated value in to the external stroage place ends **/
					cf.ShowToast("Selected image saved successfully", 1);
					dialog1.setCancelable(true);
	            	dialog1.dismiss();
        		} catch (Exception e) {
        			System.out.println("e= " + e.getMessage());
        		}
        		}
            }
        });
		btn_helpclose.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	dialog1.setCancelable(true);
            	dialog1.dismiss();
            }
		});
		rotatecamleft.setOnClickListener(new OnClickListener() {  
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 System.gc();
				 currnet_rotated-=90;
					if(currnet_rotated<=0)
					{
						currnet_rotated=270;
					}	
				
				Bitmap myImg;
				try {
					myImg = BitmapFactory.decodeStream(new FileInputStream(slectimage));
					Matrix matrix =new Matrix();
					matrix.reset();
					matrix.postRotate(currnet_rotated);
					rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),matrix, true);
					System.gc();
					upd_img.setImageBitmap(rotated_b);

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch(Exception e)
				{
					System.out.println("rota eeee "+e.getMessage());
				}

				catch (OutOfMemoryError e) {
					System.out.println("comes in to out ot mem exception");
					System.gc();
					try {
						myImg=null;
						System.gc();
						Matrix matrix =new Matrix();
						matrix.reset();
						matrix.postRotate(currnet_rotated);
						myImg= cf.ShrinkBitmap(slectimage, 800, 800);
						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
						System.gc();
						upd_img.setImageBitmap(rotated_b); 

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 catch (OutOfMemoryError e1) {
							// TODO Auto-generated catch block
						 cf.ShowToast("You can not rotate this image. Image size is too large.",1);
					}
				}
			}
		});
		rotatecamright.setOnClickListener(new OnClickListener() {  
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 System.gc();
				currnet_rotated+=90;
				if(currnet_rotated>=360)
				{
					currnet_rotated=0;
				}
				
				Bitmap myImg;
				try {
					myImg = BitmapFactory.decodeStream(new FileInputStream(slectimage));
					Matrix matrix =new Matrix();
					matrix.reset();
					matrix.postRotate(currnet_rotated);
					rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),matrix, true);
					System.gc();
					upd_img.setImageBitmap(rotated_b);

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch(Exception e)
				{
					System.out.println("rota eeee "+e.getMessage());
				}

				catch (OutOfMemoryError e) {
					System.out.println("comes in to out ot mem exception");
					System.gc();
					try {
						myImg=null;
						System.gc();
						Matrix matrix =new Matrix();
						matrix.reset();
						matrix.postRotate(currnet_rotated);
						myImg= cf.ShrinkBitmap(slectimage, 800, 800);
						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
						System.gc();
						upd_img.setImageBitmap(rotated_b); 

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 catch (OutOfMemoryError e1) {
							// TODO Auto-generated catch block
						 cf.ShowToast("You can not rotate this image. Image size is too large.",1);
					}
				}
			}
		});
		dialog1.setCancelable(false);
		dialog1.show();
	}
	/** File filter to get only the directories from the folder **/
	FileFilter directoryFilter = new FileFilter() {
		public boolean accept(File file) {
			return file.isDirectory();
		}
	};
	/** File filter to get only the directories from the folder **/
	/** File filter to get only the directories from the folder **/
	FileFilter imageFilter = new FileFilter() {
		 
		public boolean accept(File file) {
			
			 if ((file.getName().endsWith(".jpg"))||(file.getName().endsWith(".jpeg"))||(file.getName().endsWith(".png"))||(file.getName().endsWith(".gif")||(file.getName().endsWith(".JPG"))
	            				 ||(file.getName().endsWith(".JPEG"))||(file.getName().endsWith(".PNG")))||(file.getName().endsWith(".gif"))||(file.getName().endsWith(".bmp"))){
				 
				 
				 
				 return true;
			 }
			 else
			 {
				 return false;
			 }
				 
		}
	};
	/** File filter to get only the directories from the folder **/
	public void Search_by_folder()
	{
		 final Dialog dialog1 = new Dialog(photosold.this,android.R.style.Theme_Translucent_NoTitleBar);
			dialog1.getWindow().setContentView(R.layout.alert);
			((LinearLayout) dialog1.findViewById(R.id.maintable)).setVisibility(View.GONE);
			((LinearLayout) dialog1.findViewById(R.id.Search_folder)).setVisibility(View.VISIBLE);
			dialog1.setCancelable(true);
			final Button selectBtn = (Button) dialog1.findViewById(R.id.SF_Search);
			Button cancelBtn = (Button) dialog1.findViewById(R.id.SF_cancel);
			Button Close = (Button) dialog1.findViewById(R.id.SF_close);
			final AutoCompleteTextView name_txt =(AutoCompleteTextView) dialog1.findViewById(R.id.SF_Name);
			final TextView finale_txt = (TextView) dialog1.findViewById(R.id.finale_txt);
			name_txt.setFocusable(false);
			name_txt.setText((titlehead.getText().toString().substring(12,titlehead.getText().toString().length())));
			name_txt.setOnTouchListener(new OnTouchListener() {
				
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					cf.setFocus(name_txt);
					name_txt.setSelection(name_txt.getText().length());
					return false;
				}
			});
			
			cancelBtn.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					//pieces1=null;pieces2=null;pieces3=null;
					
					dialog1.setCancelable(true);
					dialog1.cancel();
				}
			});
			Close.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					//pieces1=null;pieces2=null;pieces3=null;
					
					dialog1.setCancelable(true);
					dialog1.cancel();
				}
			});
			selectBtn.setOnClickListener(new OnClickListener() {
				public void onClick(View v) { 
					selectBtn.setVisibility(View.GONE);
					String Src_Txt =name_txt.getText().toString();
					//System.out.println("The selected folder name was "+name_txt.getText().toString()+"Sub string "+Src_Txt.substring(0, 11));
					System.out.println("The txt "+(Src_Txt.substring(Src_Txt.lastIndexOf("/")+1,Src_Txt.length())).equals(""));
					if(Src_Txt.equals(""))
					{
						titlehead.setText("/mnt/sdcard/");
						finaltext="/mnt/sdcard/";
						 images = new File("/mnt/sdcard/");
						//Currentfile_list=f.listFiles();
						chaild_titleheade.setText("sdcard"); // set the chaild title
						walkdir(images);
						dynamicimagesparent();
						dynamicimageschild();
					}
					else if((Src_Txt.substring(Src_Txt.lastIndexOf("/")+1,Src_Txt.length())).equals(""))
					{
						cf.ShowToast("Please enter the valid foldername ", 1);
					}
					else 
					{	Src_Txt="/mnt/sdcard/"+Src_Txt;
						File f = new File(Src_Txt);
						if(f.exists())
						{
							if((Src_Txt.substring(Src_Txt.lastIndexOf("/"), Src_Txt.length())).equals(""))
							{
								titlehead.setText(Src_Txt);
			 					finaltext=Src_Txt;
			 					 images = new File(Src_Txt);
			 					//Currentfile_list=f.listFiles();
			 					Src_Txt=Src_Txt.substring(0, Src_Txt.length()-1);
			 					chaild_titleheade.setText(Src_Txt.substring(Src_Txt.lastIndexOf("/")+1,Src_Txt.length())); // set the chaild title
							}
							else
							{
								titlehead.setText(Src_Txt+"/");
			 					finaltext=Src_Txt+"/";
			 					 images = new File(Src_Txt+"/");
			 					//Currentfile_list=f.listFiles();
			 					
			 					chaild_titleheade.setText(Src_Txt.substring(Src_Txt.lastIndexOf("/")+1,Src_Txt.length())); // set the chaild title
							}
							
		 					walkdir(images);
		 					dynamicimagesparent();
		 					dynamicimageschild();
							
						}
						else
						{
							cf.ShowToast("Sorry no files found", 1);
						}
						
					}
					dialog1.setCancelable(true);
					dialog1.cancel();
				}
			});
			name_txt.addTextChangedListener(new TextWatcher() {
				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					try
					{
						String s1=s.toString();
						System.out.println("The issues not in the set "+s1);
						String file_name,file_txt,Requested_name;
						 if((s1.substring(0,1)).equals("/") )
							{
								name_txt.setText("");
								cf.ShowToast("Please Enter valid text ", 1);
								System.out.println("The issues not in the set ");
							}
						 else if((s1.length()>1) && (s1.substring(s.length()-1,s.length())).equals("/") && (s1.substring(s.length()-2,s.length()-1)).equals("/"))
						{
							name_txt.setText(s1.substring(0,s1.length()-1));
							name_txt.setSelection(name_txt.getText().length());
							cf.ShowToast("Please Enter valid text ", 1);
						}
						
						else if(name_txt.getText().toString().equals(""))
						{
							file_name="/mnt/sdcard/";
							 file_txt= "/mnt/sdcard";
						}
						else
						{
							file_name="/mnt/sdcard/"+name_txt.getText().toString();
							
							 file_txt= file_name.substring(0,file_name.lastIndexOf("/"));
//							 if(file_txt.equals("/mnt/sdcard"))
//							 {
//								 file_txt+="/"; 
//							 }
							 Requested_name=file_name.substring(file_name.lastIndexOf("/")+1,file_name.length());
							 System.out.println("The file name and the texdt was file_name "+file_name+" file_txt ="+file_txt+"Requested_name ="+Requested_name);
							File f = new File(file_txt);
							if(f.exists())
							{
								System.out.println("File xeists");
								File[] file_list= f.listFiles(directoryFilter);

								String[] autousername = new String[file_list.length];
								String[] filename=null;
								System.out.println("File xeists 1"+file_list.length);
									if(file_list.length>0)
									{
										System.out.println("comes in to if");
									int k=0;
											for(int i=0;i<file_list.length;i++)
											{
												String Tmp=file_list[i].getAbsolutePath();
												
												Tmp=Tmp.substring(Tmp.lastIndexOf("/")+1,Tmp.length());
												
												
												System.out.println(" Tmp = "+Tmp+" Requested_name = "+Requested_name+" (Tmp.substring(0,Requested_name.length())) = "+(Tmp.substring(0,Requested_name.length())));
												if((Tmp.substring(0,Requested_name.length())).toLowerCase().equals(Requested_name.toLowerCase()))
												{
													
													String temp= name_txt.getText().toString();
													if(temp.contains("/"))
													{
														temp= temp.substring(0,name_txt.getText().toString().lastIndexOf("/"));
														autousername[k]=temp+"/"+Tmp;
													}
													else
													{
														autousername[k] = Tmp;	
													}
													
													System.out.println("text for the file was autousername[k]= "+autousername[k]);
													k++;
													
												}
												
												
												
												
											}
											filename=new String[k];
											
											for(int m=0;m<k;m++)
											{
												
												filename[m]=autousername[m];
												System.out.println("text for the file was filename[i]= "+filename[m]);
												
											}
											
											
									}
									 ArrayAdapter<String> adapter = new ArrayAdapter<String>(photosold.this,R.layout.loginnamelist,filename);
									 name_txt.setThreshold(1);
									 name_txt.setAdapter(adapter);
						}
					
					
					
							 
					}
					
					}catch (Exception e) {
						// TODO: handle exception
						System.out.println("issues happen in cathce "+e.getMessage());
					}
				}
			});
			dialog1.setCancelable(false);
			dialog1.show();
			

	}
}
