package inspectiondepot.gch;

import inspectiondepot.gch.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeoutException;

import org.kobjects.base64.Base64;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;



import android.R.string;
import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.hardware.Camera.Parameters;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.opengl.Visibility;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.text.Html;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;

public class ExportInspection extends Activity implements Runnable {
	CommonFunctions cf;
	TextView title;
	/** STRING FOR THE EXPORT OPTION CHECKING **/
	String Export_chk[]={"0","0","0","0","0","0","0"};
	boolean Export_status[]={false,false,false,false,false,false,false};
	String H_NSTORIES="",YBUILT="";
	CheckBox alert_chk[]=new CheckBox[10];
	/** STRING FOR THE EXPORT OPTION CHECKING ENDS **/
	Button completed_insp,export;
	double incre_unit;
	/**STRING THATS WE ARE USED FOR THE PROGRESS BAR **/
	Dialog dialog1,dialog2;
	public ProgressThread progThread;
	ProgressDialog progDialog;
	int move_to_pre=0;
	int vcode;
	int mState;
    int typeBar = 1;
	double total; // Determines type progress bar: 0 = spinner, 1 = horizontal
	int delay = 40; // Milliseconds of delay in the update loop
	int maxBarValue = 0,show_handler; // Maximum value of horizontal progress bar
	final CheckBox sta_chk[]= new CheckBox[9];;
	int RUNNING = 1;
    String Exportbartext="",dt;
	private String[] policyinformation=null;
	private String[] policyinformation_mail;
	private String[] S_Q_information;
	private String[] B_I_Q_information;
	private String[] G_H_C_information;
	private String[] OB1;
    public String[] erro_trce=null;
	private String erromsg;
	private String[] feed_info;
	private Object strBase64;
	private String[] OB2,OB1T;
	private String[] OB3;
	private String[] OB4;
	 String status="Uploading Inspection Completed ";
	private double increment_unit;
	boolean tochkfileexists,feedback_fileexits;
	PowerManager.WakeLock wl=null;
    /**STRING THATS WE ARE USED FOR THE PROGRESS BAR ENDS **/
	 @Override
    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				//cf.onlinspectionid = extras.getString("InspectionType");
				cf.onlstatus = extras.getString("type");
				
			}
			cf.Create_Table(3);
			cf.Create_Table(7);
			cf.getInspectorId();
			setContentView(R.layout.completedinspectionlist);
			cf.setupUI((LinearLayout)findViewById(R.id.lin_h));
			TextView tvheader = (TextView) findViewById(R.id.information);
			completed_insp = (Button) findViewById(R.id.completed_insp);
			((Button) findViewById(R.id.deleteall)).setVisibility(View.INVISIBLE);
			completed_insp.setOnClickListener(new clicker());
			export = (Button) findViewById(R.id.export_insp);
			export.setOnClickListener(new clicker());
			completed_insp.setVisibility(cf.show.VISIBLE);
			export.setVisibility(cf.show.VISIBLE);
			
			if(cf.onlstatus.equals("export")) {
				tvheader.setText("Export inspection");
				export.setBackgroundResource(R.drawable.button_back_blue);
			}
			else if(cf.onlstatus.equals("Reexport")) {
				tvheader.setText("Reexport inspection");
				completed_insp.setBackgroundResource(R.drawable.button_back_blue);
			}
			cf.onlinspectionlist = (LinearLayout) findViewById(R.id.linlayoutdyn);
			cf.releasecode = (TextView)findViewById(R.id.releasecode);
	        cf.releasecode.setText(cf.apkrc);
	        cf.getInspectorId();
	        cf.welcome = (TextView) this.findViewById(R.id.welcomename);
	        cf.welcome.setText(cf.Insp_firstname+" "+cf.Insp_lastname);
	        title = (TextView) this.findViewById(R.id.deleteiinformation);
			cf.search = (Button) this.findViewById(R.id.search);
			 Button home = (Button) this.findViewById(R.id.deletehome);
			cf.search_text = (EditText)findViewById(R.id.search_text);
			cf.search_clear_txt = (Button)findViewById(R.id.search_clear_txt);
			cf.search_clear_txt.setOnClickListener(new OnClickListener() {
            	public void onClick(View arg0) {
					// TODO Auto-generated method stub
					cf.search_text.setText("");
					cf.res = "";
					dbquery();
            	}
          });
			home.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
					// TODO Auto-generated method stub

					cf.gohome();

				}

			});
			cf.search.setOnClickListener(new OnClickListener() {
                   public void onClick(View v) {
					// TODO Auto-generated method stub

					String temp = cf.encode(cf.search_text.getText()
							.toString());
					if (temp.equals("")) {
						cf.ShowToast("Please enter the Name or Policy Number to search.", 1);
						cf.search_text.requestFocus();
					} else {
						cf.res = temp;
						dbquery();
					}

				}

			});
			completed_insp = (Button) findViewById(R.id.completed_insp);
			completed_insp.setOnClickListener(new clicker());
			export = (Button) findViewById(R.id.export_insp);
			export.setOnClickListener(new clicker());
			getNetConnectDetail();
			dbquery();
			
	 }
	public boolean getFromArray(String GCH_BI_GBI_YOC2, String[] bI_GBI_year) 
{
	for(int i=0;i < bI_GBI_year.length;i++)
	 {
		 if(GCH_BI_GBI_YOC2.equals(bI_GBI_year[i]))
		 {
			 return false;
		 }
	 }
	return true;
}
	 /** Export function starts here 
		 * @param inc_u **/
	private void StartExport(String dt, final double inc_u) {
				// TODO Auto-generated method stub
			 increment_unit=inc_u;
			 cf.selectedhomeid=dt;
			
			 if (cf.isInternetOn() == true) {
				 PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
				 wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
				 wl.acquire();
				 			 
				 getNetConnectDetail();
                   total=0.0;
                   /**We need to delete the preivious progress bar then we start new one code starts herer**/
					try
					{
					removeDialog(1);
					}
					catch (Exception e)
					{
						
					}
					/**We need to delete the preivious progress bar then we start new one code Ends herer**/
                    progDialog = new ProgressDialog(ExportInspection.this); // we creat dilog box 
					showDialog(1);
					
					new Thread() {

						private String elevvetiondes;
						private int usercheck;

						public void run() {
							Looper.prepare();

							try {
								
								
							String	selectedItem = cf.selectedhomeid;
								total = 1.0;
								Exportbartext="Check the status of inspection ";
								total=5.0;
								if (!"".equals(cf.selectedhomeid)) {
									//Check if the record in the correct status for uploading
									
									cf.Chk_Inspector=cf.fn_CurrentInspector(cf.Insp_id,cf.selectedhomeid);
									if(cf.Chk_Inspector.equals("true"))
									{
									    boolean insp_sta=false;
										try
										{
											 insp_sta=cf.getinspectionstatus(cf.Insp_id,"GetInspectionStatus");
										}
										catch (NetworkErrorException n) 
										{
											String	strerrorlog="Network exception happens while retrieving the Inspection status Error= "+n;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											
											throw n;
										}
										catch (SocketTimeoutException s) 
										{
											String	strerrorlog="Socket Timeout exception happens while retrieving the Inspection status Error ="+s;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											
											throw s;
										}
										catch (IOException io) 
										{
	
											String	strerrorlog="Inputoutput exception happens while retrieving the Inspection status Error ="+io;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem in retrieving the Inspection status in the server please contact paperless admin ");
											
										}
										catch (XmlPullParserException x) 
										{
											String	strerrorlog="Xmlparser  exception happens while retrieving the Inspection status Error ="+x;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem retrieving the Inspection status information ");
											
										}
										catch (Exception ex) 
										{
											String	strerrorlog="Exception happens while Uploading the policy holder information Error ="+ex;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem  retrieving the Inspection status information ");
											
										}
									
								if (insp_sta) {
									
									// Checking The option which are selected 
									if(Export_chk[0].equals("1")||Export_chk[1].equals("1"))
									{   Exportbartext="Uploading Policy holder information ";
										try
										{
											cf.Create_Table(3);
											
											Export_status[0]=(Upload_Policyholder()) ? true:false;
											getNetConnectDetail();
											//Error_traker("Data moved success fully ");
										}
										catch (NetworkErrorException n) 
										{
											String	strerrorlog="Network exception happens while Uploading the policy holder information Error= "+n;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											
											throw n;
										}
										catch (SocketTimeoutException s) 
										{
											String	strerrorlog="Socket Timeout exception happens while Uploading the policy holder information Error ="+s;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw s;
										}
										catch (IOException io) 
										{

											String	strerrorlog="Inputoutput exception happens while Uploading the policy holder information Error ="+io;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem  in the server please contact paperless admin");
										}
										catch (XmlPullParserException x) 
										{

											String	strerrorlog="Xmlparser  exception happens while Uploading the policy holder information Error ="+x;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem uploadin the Policy holder information ");
										}
										catch (Exception ex) 
										{

											String	strerrorlog="Exception happens while Uploading the policy holder information Error ="+ex;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem uploadin the Policy holder information ");
										}
										total=total+inc_u;
									}
									
									if(Export_chk[0].equals("1")||Export_chk[2].equals("1"))
									{	 Exportbartext="Uploading Building  information ";
									
									
										try
										{   cf.Create_Table(5);
											Export_status[1]=(Upload_Buildinginformation()) ? true:false;
											getNetConnectDetail();
													
										}
										catch (NetworkErrorException n) 
										{
											String	strerrorlog="Network exception happens while Uploading the Upload_Buildinginformation information Error= "+n;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw n;
										}
										catch (SocketTimeoutException s) 
										{
											String	strerrorlog="Socket Timeout exception happens while Uploading the Upload_Buildinginformation information Error ="+s;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw s;
										}
										catch (IOException io) 
										{

											String	strerrorlog="Inputoutput exception happens while Uploading the Upload_Buildinginformation information Error ="+io;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem  in the server please contact paperless admin");
											throw io;
										}
										catch (XmlPullParserException x) 
										{

											String	strerrorlog="Xmlparser  exception happens while Uploading the Upload_Buildinginformation information Error ="+x;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem uploadin the Building  information ");
										}
										catch (Exception ex) 
										{

											String	strerrorlog="Exception happens while Uploading the Upload_Buildinginformation information Error ="+ex;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem uploadin the Building  information ");
										}
										total=total+inc_u;
									}
									if(Export_chk[0].equals("1")||Export_chk[3].equals("1"))
									{	 Exportbartext="Uploading General Hazards information ";
										try
										{   cf.Create_Table(5);
											Export_status[2]=(Upload_GeneralHazardsInformation()) ? true:false;
											getNetConnectDetail();
											
										}
										catch (NetworkErrorException n) 
										{
											String	strerrorlog="Network exception happens while Uploading the Upload_Buildinginformation information Error= "+n;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw n;
										}
										catch (SocketTimeoutException s) 
										{
											String	strerrorlog="Socket Timeout exception happens while Uploading the Upload_Buildinginformation information Error ="+s;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw s;
										}
										catch (IOException io) 
										{

											String	strerrorlog="Inputoutput exception happens while Uploading the Upload_Buildinginformation information Error ="+io;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem  in the server please contact paperless admin");
											throw io;
										}
										catch (XmlPullParserException x) 
										{

											String	strerrorlog="Xmlparser  exception happens while Uploading the Upload_Buildinginformation information Error ="+x;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem uploadin the Building  information ");
										}
										catch (Exception ex) 
										{

											String	strerrorlog="Exception happens while Uploading the Upload_Buildinginformation information Error ="+ex;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem uploadin the Building  information ");
										}
										total=total+inc_u;
									}
									if(Export_chk[0].equals("1")||Export_chk[6].equals("1"))
									{	 Exportbartext="Uploading General Roof information ";
									
										try
										{   cf.Create_Table(21);
										    cf.Create_Table(22);
										    
											Export_status[5]=(GeneralRoofInformation()) ? true:false;
											getNetConnectDetail();
											
										}
										catch (NetworkErrorException n) 
										{
											System.out.println("The error was in n "+n.getMessage());
											String	strerrorlog="Network exception happens while Uploading the Upload_Roof  information Error= "+n;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw n;
										}
										catch (SocketTimeoutException s) 
										{
											System.out.println("The error was in s "+s.getMessage());
											String	strerrorlog="Socket Timeout exception happens while Uploading the Upload_Roof information Error ="+s;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw s;
										}
										catch (IOException io) 
										{
											System.out.println("The error was in io "+io.getMessage());

											String	strerrorlog="Inputoutput exception happens while Uploading the Upload_Roof information Error ="+io;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem  in the server please contact paperless admin");
											throw io;
										}
										catch (XmlPullParserException x) 
										{
											

											String	strerrorlog="Xmlparser  exception happens while Uploading the Upload_Roof information Error ="+x;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem uploadin the Roof  information ");
										}
										catch (Exception ex) 
										{
											

											String	strerrorlog="Exception happens while Uploading the Upload_Roof information Error ="+ex;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem uploadin the Roof  information ");
										}
										total=total+inc_u;
									}
									if(Export_chk[0].equals("1")||Export_chk[4].equals("1"))
									{  Exportbartext="Uploading Photos ";
										try
										{
											cf.Create_Table(15);
											Export_status[3]=(Uploading_Photos()) ? true:false;
										}
										catch (NetworkErrorException n) 
										{
											String	strerrorlog="Network exception happens while Uploading the Uploading_Photos(); information Error= "+n;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw n;
										}
										catch (SocketTimeoutException s) 
										{
											String	strerrorlog="Socket Timeout exception happens while Uploading the Uploading_Photos(); information Error ="+s;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have Internet Connection problem please check your networ connection ");
											throw s;
										}
										catch (IOException io) 
										{

											String	strerrorlog="Inputoutput exception happens while Uploading the Uploading_Photos(); information Error ="+io;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have problem  in the server please contact paperless admin");
											throw io;
										}
										catch (XmlPullParserException x) 
										{

											String	strerrorlog="Xmlparser  exception happens while Uploading the Uploading_Photos(); information Error ="+x;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have problem while uploading the photos information. ");
										}
										catch (Exception ex) 
										{

											String	strerrorlog="Exception happens while Uploading the Uploading_Photos(); information Error ="+ex;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem while uploading the photos information. ");
										}
									
									}
									if(Export_chk[0].equals("1")||Export_chk[5].equals("1"))
									{	Exportbartext="Uploading Feed back document ";
										
										try
										{
											cf.Create_Table(13);
											cf.Create_Table(14);
											Export_status[4]=(Uploading_feedback()) ? true:false;
											getNetConnectDetail();
										}
										catch (NetworkErrorException n) 
										{
											String	strerrorlog="Network exception happens while Uploading the Uploading_feedback information Error= "+n;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw n;
										}
										catch (SocketTimeoutException s) 
										{
											String	strerrorlog="Socket Timeout exception happens while Uploading the Uploading_feedback information Error ="+s;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw s;
										}
										catch (IOException io) 
										{

											String	strerrorlog="Inputoutput exception happens while Uploading the Uploading_feedback information Error ="+io;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have problem  in the server please contact paperless admin");
											throw io;
										}
										catch (XmlPullParserException x) 
										{

											String	strerrorlog="Xmlparser  exception happens while Uploading the Uploading_feedback information Error ="+x;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have problem while uploading the feed back  information. ");
										}
										catch (Exception ex) 
										{

											String	strerrorlog="Exception happens while Uploading the Uploading_feedback information Error ="+ex;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection   in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have problem while uploading the feed back information. ");
										}
										
									}
									
										Exportbartext="Changing Status from schedule to preinspected";
									
										try
										{
											show_staus();
										}
										catch (NetworkErrorException n) 
										{
											String	strerrorlog="Network exception happens while getting the status  information Error= "+n;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											
											throw n;
										}
										catch (SocketTimeoutException s) 
										{
											String	strerrorlog="Socket Timeout exception happens while  getting the  Error ="+s;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											
											throw s;
										}
										catch (IOException io) 
										{

											String	strerrorlog="Inputoutput exception happens  gettting the status Error ="+io;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											
											throw io;
										}
										catch (XmlPullParserException x) 
										{

											String	strerrorlog="Xmlparser  exception happens while  getting the status  Error ="+x;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											
										}
										catch (Exception ex) 
										{

											String	strerrorlog="Exception happens while getting the status information Error ="+ex;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection   in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											
										}
										
										try
										{
											Status_change();
										}
										catch (NetworkErrorException n) 
										{
											String	strerrorlog="Network exception happens while changing the status in schedule to pre inspected information Error= "+n;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw n;
										}
										catch (SocketTimeoutException s) 
										{
											String	strerrorlog="Socket Timeout exception happens while  changing the status in schedule to pre inspected Error ="+s;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("You have internet connection problem please check your networ connection ");
											throw s;
										}
										catch (IOException io) 
										{

											String	strerrorlog="Inputoutput exception happens  changing the status in schedule to pre inspected Error ="+io;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem  in the server please contact paperless admin");
											throw io;
										}
										catch (XmlPullParserException x) 
										{

											String	strerrorlog="Xmlparser  exception happens while  changing the status in shedule to pre inspected Error ="+x;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem in moving  schedule to pre inspected ");
										}
										catch (Exception ex) 
										{

											String	strerrorlog="Exception happens while Uploading the Uploading_feedback information Error ="+ex;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection   in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											Error_traker("you have problem uploadin the feed back information ");
										}
										total=total+5;
										try
										{
											Error_logupload();
										}
										catch (NetworkErrorException n) 
										{
											String	strerrorlog="Network exception happens while uploading error log file information Error= "+n;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											
											throw n;
										}
										catch (SocketTimeoutException s) 
										{
											String	strerrorlog="Socket Timeout exception happens while uploading error log file Error ="+s;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											
											throw s;
										}
										catch (IOException io) 
										{

											String	strerrorlog="Inputoutput exception happens uploading error log file Error ="+io;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
									
											throw io;
										}
										catch (XmlPullParserException x) 
										{

											String	strerrorlog="Xmlparser  exception happens while  uploading error log file Error ="+x;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											
										}
										catch (Exception ex) 
										{

											String	strerrorlog="Exception happens whileuploading error log file information Error ="+ex;
											cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection   in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
											
										}
										if(wl!=null)
										{
										wl.release();
										wl=null;
										}
										total=total+5;
										total=100; // new added plz change once remove the comments on the Feed back 
								//show_staus
										
									
								

								}
								else
								{
									total=100;
									if(wl!=null)
									{
									wl.release();
									wl=null;
									}
									Error_traker("Selected Inspection Status has been changed so you cannot able to upload this Record");
									
								}
									
							     }
								}
								else
								{
									total=100;
									if(wl!=null)
									{
									wl.release();
									wl=null;
									}
									Error_traker("Selected Inspection moved to some other Inspector's");
										}
							}
							catch(Exception e)
								{
								String	strerrorlog="Exception happens in the overall exception Error ="+e;
								cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection   in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
								if(wl!=null)
								{
								wl.release();
								wl=null;
								}
								}
							if(wl!=null)
							{
							wl.release();
							wl=null;
							}
							total=100.00;
							}

						

						
						}.start();
			 }
			 else
			 {
				 cf.ShowToast("Internet connection is not available.", 1);
			 }
						
			}   
    protected boolean GeneralRoofInformation() throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException {
			// TODO Auto-generated method stub
    	SoapObject request = new SoapObject(cf.NAMESPACE,"ExportGenCondnAndHazardsRoofInfo");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		cf.getInspecionTypeid(cf.selectedhomeid);
		/** set the value get from the policy holder table starts **/
		
		Cursor C_GRI_1= cf.SelectTablefunction(cf.General_Roof_information, " where RC_SRID='" + cf.selectedhomeid + "' ");
		if(C_GRI_1.getCount()>=1)
		{
			
			String s[]={"Roof Damage Noted","Roof Repairs Noted","Roof Leakage Noted","Other Condition Present"};
			C_GRI_1.moveToFirst();
			int m=1;/**For the image order of dynamicaly created roof **/
			int error=0;
		    for(int i=0;i<C_GRI_1.getCount();i++)
		    {
		    	
		    	
		    	
		    	
		    	if(cf.decode(C_GRI_1.getString(2)).equals(s[0]))
		    	{
		    		request.addProperty("roofdamage",cf.decode(C_GRI_1.getString(4)));
		    		request.addProperty("roofdameagecmd",cf.decode(C_GRI_1.getString(3)));
		    		System.out.println("roof valu"+request);
		    	}
		    	else if(cf.decode(C_GRI_1.getString(2)).equals(s[1]))
		    	{
		    		request.addProperty("roofrepairs",cf.decode(C_GRI_1.getString(4)));
		    		request.addProperty("roofrepairscmd",cf.decode(C_GRI_1.getString(3)));
		    	}
		    	else if(cf.decode(C_GRI_1.getString(2)).equals(s[2]))
		    	{
		    		request.addProperty("roofleekage",cf.decode(C_GRI_1.getString(4)));
		    		request.addProperty("roofleekagecmd",cf.decode(C_GRI_1.getString(3)));
		    	}
		    	else if(cf.decode(C_GRI_1.getString(2)).equals(s[3]))
		    	{
		    		request.addProperty("Othercondition",cf.decode(C_GRI_1.getString(4)));
		    		request.addProperty("Otherconditioncmd",cf.decode(C_GRI_1.getString(3)));
		    	}
		    	else if(cf.decode(C_GRI_1.getString(2)).equals("Excessive Roof Aging evident")&& cf.decode(C_GRI_1.getString(6)).equals("10"))
		    	{
		    		request.addProperty("roofExcessiver",cf.decode(C_GRI_1.getString(4)));
		    	}
		    	else if(cf.decode(C_GRI_1.getString(2)).equals("3 Year Replacement Probability") && cf.decode(C_GRI_1.getString(6)).equals("10"))
		    	{
		    		request.addProperty("Yearreplace",cf.decode(C_GRI_1.getString(4)));
		    	}
		    	else if(cf.decode(C_GRI_1.getString(2)).equals("Comments") && cf.decode(C_GRI_1.getString(6)).equals("10"))
		    	{
		    		request.addProperty("Roofcomments",cf.decode(C_GRI_1.getString(3)));
		    	}
		    	else
		    	{
		    		String tmp=cf.decode(C_GRI_1.getString(4));
		    		
		    		tmp=(tmp.equals("Yes"))?"1":(tmp.equals("No"))?"0":(tmp.equals("Not Determined"))?"2":(tmp.equals("Beyond Scope of Inspection"))?"3":"0";
		    		SoapObject request_RCT = new SoapObject(cf.NAMESPACE,"ExportGenHazardsRoofInfo_Qusans");
		    		SoapSerializationEnvelope envelope_RCT = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		    		envelope_RCT.dotNet = true;
		    		request_RCT.addProperty("InspectorID",cf.Insp_id);
		    		request_RCT.addProperty("SRID",C_GRI_1.getString(1));
		    		request_RCT.addProperty("Inspectiotype",cf.Inspectiontypeid);
		    		request_RCT.addProperty("Question",cf.decode(C_GRI_1.getString(2)));
		    		request_RCT.addProperty("Answer",tmp);
		    		request_RCT.addProperty("Comments",cf.decode(C_GRI_1.getString(3)));
		    		request_RCT.addProperty("OrderBy",m);
		    		try
		    		{
		    		envelope_RCT.setOutputSoapObject(request_RCT);
		  			System.out.println("request of RCT"+i+"="+request_RCT);
		  			HttpTransportSE androidHttpTransport_RCT = new HttpTransportSE(cf.URL);
		  			androidHttpTransport_RCT.call(cf.NAMESPACE+"ExportGenHazardsRoofInfo_Qusans",envelope_RCT);
		  			String result_RCT =  envelope_RCT.getResponse().toString();
		  			
		  			System.out.println("result="+i+"="+result_RCT);
		  			
		  			error=0;
		  			m++;
		    		}
		    		catch (IOException e) {
					// TODO: handle exception
					//Error_traker("comes in to the io exception "+error+"  i="+i);
		    			/**When will the net work error occure we try to re send the same record for two times starts**/
					if(error==0 || error==1 )
					{
						C_GRI_1.moveToPrevious(); 	
					i=i-1;
					error++;
					}
					else
					{
						throw e;
					}
					/**When will the net work error occure we try to re send the same record for two times ends**/
				}
		    	}
		    	System.out.println("roof valu"+request);
		    	C_GRI_1.moveToNext();	
		    	
		    	
		    }
		    
		    
					
			
			
			/**Adding General Roof condition  values Starts***/
			

			/**Adding General Roof condition  values Endss***/
			request.addProperty("InspectorID",cf.Insp_id);
			request.addProperty("SRID",cf.selectedhomeid); 
			/**Adding RCT values starts***/
			Cursor GRI= cf.SelectTablefunction(cf.Roof_cover_type, " where RC_RCT_SRID='" + cf.selectedhomeid + "' ");
			//GRI=cf.setvalueToArray_morerow(C_GRI);/** set the value to the array for the easy usage **/
			GRI.moveToFirst();
			Adapter adapter1 = new ArrayAdapter(this,android.R.layout.simple_spinner_item, cf.year);
			for(int i=0;i<GRI.getCount();i++)
			{
				
				String yr="",yr_o="";
				if(cf.decode(GRI.getString(5)).trim().equals(""))
				{
					 yr="0";
					 yr_o="";
				}
				else if(!getFromArray(cf.decode(GRI.getString(5)),cf.year))
				{
					 yr=(cf.decode(GRI.getString(5)).trim().equals(""))? "0":cf.decode(GRI.getString(5));
					 yr_o="";
				}else
				{
					yr="Other";
					yr_o=cf.decode(GRI.getString(5));
					
				}
				String RS="0";
				if(!cf.decode(GRI.getString(6)).trim().equals(""))
				{
					RS=cf.decode(GRI.getString(6));
				}
				String RSH="0";
				if(!cf.decode(GRI.getString(7)).trim().equals(""))
				{
					RSH=cf.decode(GRI.getString(7));
				}
				String RSt="0";
				if(!cf.decode(GRI.getString(9)).trim().equals(""))
				{
					RSt=cf.decode(GRI.getString(9));
				}
				
				String Rbv="0";
				if(!cf.decode(GRI.getString(11)).trim().equals(""))
				{
					Rbv=cf.decode(GRI.getString(11));
				}
				/**Set the Saved value to the RCT webservice Starts **/
				
				if(cf.decode(GRI.getString(2)).trim().equals("Asphalt/Fiberglass"))
				{
					
					 
					request.addProperty("Asphalttype",1);
					request.addProperty("Asphaltappdate",cf.decode(GRI.getString(4)));
					request.addProperty("Asphaltyear",yr);
					request.addProperty("AsphaltyearOther",yr_o);
					request.addProperty("Asphaltroofslope",RS);
					request.addProperty("Asphaltroofshape",RSH);
					request.addProperty("AsphaltroofshapeOther",cf.decode(GRI.getString(8)));//need to change that
					request.addProperty("Asphaltroofshapeval",cf.decode(GRI.getString(12)));
					request.addProperty("Asphaltroofstu",RSt);
					request.addProperty("AsphaltroofstuOther",cf.decode(GRI.getString(10)));//Need tho change theat 
					request.addProperty("Asphaltpredo",Rbv);
					request.addProperty("Asphaltnoinfo",(cf.decode(GRI.getString(13)).equals("1"))? true:false);
					request.addProperty("RdoPredominant1",(cf.decode(GRI.getString(14)).equals("1"))? true:false);
					
				}
				
				if(cf.decode(GRI.getString(2)).equals("Concrete/Clay Tile"))
				{
					request.addProperty("Concretetype",1);
					request.addProperty("Concreteappdate",cf.decode(GRI.getString(4)));
					request.addProperty("Concretetyear",yr);
					request.addProperty("ConcretetyearOther",yr_o);
					request.addProperty("Concreteroofslope",RS);
					request.addProperty("Concreteroofshape",RSH);
					request.addProperty("ConcreteroofshapeOther",cf.decode(GRI.getString(8)));//need to change that
					request.addProperty("Concreteroofshapeval",cf.decode(GRI.getString(12)));
					request.addProperty("Concreteroofstu",RSt);
					request.addProperty("ConcreteroofstuOther",cf.decode(GRI.getString(10)));//Need tho change theat
					request.addProperty("Concretepredo",Rbv);
					request.addProperty("Concretenoinfo",(cf.decode(GRI.getString(13)).equals("1"))? true:false);
					request.addProperty("RdoPredominant2",(cf.decode(GRI.getString(14)).equals("1"))? true:false);
				}
				
				if(cf.decode(GRI.getString(2)).equals("Metal"))
				{
					  
				
					request.addProperty("Metaltype",1);
					request.addProperty("Metalappdate",cf.decode(GRI.getString(4)));
					request.addProperty("Metalyear",yr);
					request.addProperty("MetalyearOther",yr_o);
					request.addProperty("Metalroofslope",RS);
					request.addProperty("Metalroofshape",RSH);
					request.addProperty("MetalroofshapeOther",cf.decode(GRI.getString(8)));//need to change that
					request.addProperty("Metalroofshapeval",cf.decode(GRI.getString(12)));
					request.addProperty("Metalroofstu",RSt);
					request.addProperty("MetalroofstuOther",cf.decode(GRI.getString(10)));//Need tho change theat
					request.addProperty("Metalpredo",Rbv);
					request.addProperty("Metalnoinfo",(cf.decode(GRI.getString(13)).equals("1"))? true:false);
					request.addProperty("RdoPredominant3",(cf.decode(GRI.getString(14)).equals("1"))? true:false);
				}
				
				if(cf.decode(GRI.getString(2)).equals("Built Up"))
				{
				
					request.addProperty("Builtuptype",1);
					request.addProperty("Builtupappdate",cf.decode(GRI.getString(4)));
					request.addProperty("Builtupyear",yr);
					request.addProperty("BuiltupyearOther",yr_o);
					request.addProperty("Builtuproofslope",RS);
					request.addProperty("Builtuproofshape",RSH);
					request.addProperty("BuiltuproofshapeOther",cf.decode(GRI.getString(8)));//need to change that
					request.addProperty("Builtuproofshapeval",cf.decode(GRI.getString(12)));
					request.addProperty("Builtuproofstu",RSt);
					request.addProperty("BuiltuproofstuOther",cf.decode(GRI.getString(10)));//Need tho change theat
					request.addProperty("Builtuppredo",Rbv);
					request.addProperty("Builtupnoinfo",(cf.decode(GRI.getString(13)).equals("1"))? true:false);
					request.addProperty("RdoPredominant4",(cf.decode(GRI.getString(14)).equals("1"))? true:false);
				}
				
				if(cf.decode(GRI.getString(2)).equals("Membrane"))
				{
				
				
					request.addProperty("Membranetype",1);
					request.addProperty("Membraneappdate",cf.decode(GRI.getString(4)));
					request.addProperty("Membraneyear",yr);
					request.addProperty("MembraneyearOther",yr_o);
					request.addProperty("Membraneroofslope",RS);
					request.addProperty("Membraneroofshape",RSH);
					request.addProperty("MembraneroofshapeOther",cf.decode(GRI.getString(8)));//need to change that
					request.addProperty("Membraneroofshapeval",cf.decode(GRI.getString(12)));
					request.addProperty("Membraneroofstu",RSt);
					request.addProperty("MembraneroofstuOther",cf.decode(GRI.getString(10)));//Need tho change theat
					request.addProperty("Membranepredo",Rbv);
					request.addProperty("Membranenoinfo",(cf.decode(GRI.getString(13)).equals("1"))? true:false);
					request.addProperty("RdoPredominant5",(cf.decode(GRI.getString(14)).equals("1"))? true:false);
				}
				if(cf.decode(GRI.getString(2)).equals("Other"))
				{
					
					
					request.addProperty("Othertype",1);
					request.addProperty("Otherothername",cf.decode(GRI.getString(3)));
					request.addProperty("Otherappdate",cf.decode(GRI.getString(4)));
					request.addProperty("Otheryear",yr);
					request.addProperty("Otheryear_Other",yr_o);
					request.addProperty("Otherroofslope",RS);
					request.addProperty("Otherroofshape",RSH);
					request.addProperty("OtherroofshapeOther",cf.decode(GRI.getString(8)));//Need to change that
					request.addProperty("Otherroofshapeval",cf.decode(GRI.getString(12)));
					request.addProperty("Otherroofstu",RSt);
					request.addProperty("OtherroofstuOther",cf.decode(GRI.getString(10)));//Need tho change theat
					request.addProperty("Otherpredo",Rbv);
					request.addProperty("Othernoinfo",(cf.decode(GRI.getString(13)).equals("1"))? true:false);
					request.addProperty("RdoPredominant6",(cf.decode(GRI.getString(14)).equals("1"))? true:false);
				}
				
				GRI.moveToNext();
				/**Set the Saved value to the RCT webservice Ends **/
			}
			/**Adding RCT values Endss***/
		/**Set the Default value to the RCT webservice **/
			System.out.println("roof valu final"+request);
	
			request.addProperty("Asphalttype",0);
			request.addProperty("Asphaltappdate","");
			request.addProperty("Asphaltyear","");
			request.addProperty("AsphaltyearOther","");
			request.addProperty("Asphaltroofslope","");
			request.addProperty("Asphaltroofshape","");
			request.addProperty("AsphaltroofshapeOther","");//need to change that
			request.addProperty("Asphaltroofshapeval","");
			request.addProperty("Asphaltroofstu","");
			request.addProperty("AsphaltroofstuOther","");//Need tho change theat 
			request.addProperty("Asphaltpredo","");
			request.addProperty("Concretetype",0);
			request.addProperty("Concreteappdate","");
			request.addProperty("Concretetyear","");
			request.addProperty("ConcretetyearOther","");
			request.addProperty("Concreteroofslope","");
			request.addProperty("Concreteroofshape","");
			request.addProperty("ConcreteroofshapeOther","");//need to change that
			request.addProperty("Concreteroofshapeval","");
			request.addProperty("Concreteroofstu","");
			request.addProperty("ConcreteroofstuOther","");//Need tho change theat
			request.addProperty("Concretepredo","");
			request.addProperty("Metaltype",0);
			request.addProperty("Metalappdate","");
			request.addProperty("Metalyear","");
			request.addProperty("MetalyearOther","");
			request.addProperty("Metalroofslope","");
			request.addProperty("Metalroofshape","");
			request.addProperty("MetalroofshapeOther","");//need to change that
			request.addProperty("Metalroofshapeval","");
			request.addProperty("Metalroofstu","");
			request.addProperty("MetalroofstuOther","");//Need tho change theat
			request.addProperty("Metalpredo","");
			request.addProperty("Builtuptype",0);
			request.addProperty("Builtupappdate","");
			request.addProperty("Builtupyear","");
			request.addProperty("BuiltupyearOther","");
			request.addProperty("Builtuproofslope","");
			request.addProperty("Builtuproofshape","");
			request.addProperty("BuiltuproofshapeOther","");//need to change that
			request.addProperty("Builtuproofshapeval","");
			request.addProperty("Builtuproofstu","");
			request.addProperty("BuiltuproofstuOther","");//Need tho change theat
			request.addProperty("Builtuppredo","");
			request.addProperty("Membranetype",0);
			request.addProperty("Membraneappdate","");
			request.addProperty("Membraneyear","");
			request.addProperty("MembraneyearOther","");
			request.addProperty("Membraneroofslope","");
			request.addProperty("Membraneroofshape","");
			request.addProperty("MembraneroofshapeOther","");//need to change that
			request.addProperty("Membraneroofshapeval","");
			request.addProperty("Membraneroofstu","");
			request.addProperty("MembraneroofstuOther","");//Need tho change theat
			request.addProperty("Membranepredo","");
			request.addProperty("Othertype",0);
			request.addProperty("Otherothername","");
			request.addProperty("Otherappdate","");
			request.addProperty("Otheryear","");
			request.addProperty("OtheryearOther","");
			request.addProperty("Otherroofslope","");
			request.addProperty("Otherroofshape","");
			request.addProperty("OtherroofshapeOther","");//need to change that
			request.addProperty("Otherroofshapeval","");
			request.addProperty("Otherroofstu","");
			request.addProperty("OtherroofstuOther","");//Need tho change theat
			request.addProperty("Otherpredo","");
			request.addProperty("Asphaltnoinfo",false);
			request.addProperty("Concretenoinfo",false);
			request.addProperty("Metalnoinfo",false);
			request.addProperty("Builtupnoinfo",false);
			request.addProperty("Membranenoinfo",false);
			request.addProperty("Othernoinfo",false);
			request.addProperty("RdoPredominant1",false);
			request.addProperty("RdoPredominant2",false);
			request.addProperty("RdoPredominant3",false);
			request.addProperty("RdoPredominant4",false);
			request.addProperty("RdoPredominant5",false);
			request.addProperty("RdoPredominant6",false);
			
			 
			
			/**Set the Default value to the RCT webservice ends **/
	
			envelope.setOutputSoapObject(request);
			System.out.println("request="+request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
			androidHttpTransport.call(cf.NAMESPACE+"ExportGenCondnAndHazardsRoofInfo",envelope);
			String result =  envelope.getResponse().toString();
			System.out.println("result="+result);
			return cf.check_Status(result);
			//return true;
		}
		return true;
		}

	protected void Error_logupload()throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException  {
			// TODO Auto-generated method stub
    	
    	File assist = new File(this.getFilesDir()+"/errorlogfile.txt");
    	
    	if(cf.common(this.getFilesDir()+"/errorlogfile.txt"))
    	{
	    	SoapObject request = new SoapObject(cf.NAMESPACE,"ErrorLogFile_Tab");
	    	SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
	    	envelope.dotNet = true;
	    	request.addProperty("InspectorId",cf.Insp_id);
	    	
	    	if (assist.exists()) 
			{
	    		try
					{
						
						InputStream fis = new FileInputStream(assist);
						long length = assist.length();
						byte[] bytes = new byte[(int) length];
						int offset = 0;
						int numRead = 0;
						while (offset < bytes.length && (numRead = fis.read(bytes, offset, bytes.length - offset)) >= 0)
						{
							offset += numRead; 
						}
						strBase64 = Base64.encode(bytes);
						request.addProperty("imgByte", strBase64);
						envelope.setOutputSoapObject(request);
						HttpTransportSE httpTransport = new HttpTransportSE(cf.URL);
						HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
						androidHttpTransport.call(cf.NAMESPACE+"ErrorLogFile_Tab",envelope);
						String result =  envelope.getResponse().toString();
						if(cf.check_Status(result))
						{
							
							assist.delete();
							
						}
						
					}
					catch (Exception e) 
					{
					}
						
			
		}
    	}
		}

	protected boolean Status_change() throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException {

			// TODO Auto-generated method stub


			/*SoapObject request = new SoapObject(cf.NAMESPACE,"UpdateInspectionStatus");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("InspectorID",cf.Insp_id);
			request.addProperty("SRID",cf.selectedhomeid);
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
			androidHttpTransport.call(cf.NAMESPACE+"UpdateInspectionStatus",envelope);
			String result =  envelope.getResponse().toString();
			
			if(cf.check_Status(result))
			{
				status="Uploading inspection completed.Inspection Move to Pre - Inspected status";
				cf.gch_db.execSQL("UPDATE "+ cf.policyholder+ " SET GCH_PH_IsInspected=2,GCH_PH_IsUploaded=1,GCH_PH_SubStatus=41 WHERE GCH_PH_SRID ='"
						+ cf.selectedhomeid
						+ "'");
				move_to_pre=1;
				return true;
				
				
			}
			else 
			{
				status="Exporting inspection completed. Inspection in scheduled  status";
				return false;
			}
			    
			*/
return true;
		}

		/** Export summary question inforamtion Ends here **/
	/** Upload policy holder information Starts **/
	private boolean Upload_Policyholder() throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException{ 
		//uploding the policy holder information  
		// TODO Auto-generated method stub
		
		SoapObject request = new SoapObject(cf.NAMESPACE,"ExportData");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		/** set the value get from the policy holder table starts **/
		Cursor C_policy= cf.SelectTablefunction(cf.policyholder, " where GCH_PH_SRID='" + cf.selectedhomeid + "' and GCH_PH_InspectorId='"+ cf.Insp_id + "'");
		if(C_policy.getCount()==1)
		{
			
			policyinformation=cf.setvalueToArray(C_policy);/** set the value to the array for the easy usage **/
		
			request.addProperty("InspectorID",policyinformation[1]);
			request.addProperty("SRID",policyinformation[2]);
			request.addProperty("OwnerFirstName",policyinformation[3]);
			request.addProperty("OwnerLastName",policyinformation[4]);
			request.addProperty("Address1",policyinformation[5]);
			request.addProperty("Contactperson",policyinformation[34]);
			request.addProperty("City",policyinformation[7]);
			request.addProperty("Zip",policyinformation[8]);
			request.addProperty("State",policyinformation[9]);
			request.addProperty("County",policyinformation[10]);
			request.addProperty("HomePhone",policyinformation[14]);
			request.addProperty("WorkPhone",policyinformation[15]);
			request.addProperty("CellPhone",policyinformation[16]);
			request.addProperty("Email",policyinformation[19]);		
			request.addProperty("PolicyNumber",policyinformation[11]);
			request.addProperty("YearofConstruction",policyinformation[33]);
			request.addProperty("InsuranceCarrier",policyinformation[13]);
			request.addProperty("NStorie",policyinformation[17]);
			request.addProperty("Website",policyinformation[18]);
			
					/** set the value get from the policy holder mailing  table Ends **/
			envelope.setOutputSoapObject(request);
			System.out.println("request="+request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
			androidHttpTransport.call(cf.NAMESPACE+"ExportData",envelope);
			String result =  envelope.getResponse().toString();
			System.out.println("result="+result);
			return cf.check_Status(result);
		}
		/** set the value get from the policy holder table Ends **/
		
		
		
		return true;
		
		
	}
	/** Upload policy holder information Ends **/
	protected boolean Upload_Buildinginformation() throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException {
		// TODO Auto-generated method stub
		
		int occuperc =0,vacantperc=0,noofbalconypresent;boolean occcupied,vacant,notdetermined,iswallcladding,iswallstructure,isisopresent;
		int noofbalconies;
	SoapObject request = new SoapObject(cf.NAMESPACE,"ExportGenHazardsBuildingInfo");
	SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
	envelope.dotNet = true;
	/** set the value get from the policy holder table starts **/


	Cursor C_PH= cf.SelectTablefunction(cf.policyholder, " where GCH_PH_SRID='" + cf.encode(cf.selectedhomeid) + "' and GCH_PH_InspectorId='"+ cf.encode(cf.Insp_id) + "' ");
	
	if(C_PH.getCount()>0)
	{
		
		
		C_PH.moveToFirst();
		H_NSTORIES =cf.decode(C_PH.getString(C_PH.getColumnIndex("GCH_PH_NOOFSTORIES")));
		YBUILT =cf.decode(C_PH.getString(C_PH.getColumnIndex("GCH_YearBuilt")));
		
		
	}	
	Cursor C_BQ= cf.SelectTablefunction(cf.BIQ_table, " where GCH_BI_Homeid='" + cf.encode(cf.selectedhomeid) + "' and GCH_BI_Inspectorid='"+ cf.encode(cf.Insp_id) + "' ");
	
	if(C_BQ.getCount()==1)
	{
		  B_I_Q_information=cf.setvalueToArray(C_BQ);/** set the value to the array for the easy usage **/
		  request.addProperty("InspectorID",Integer.parseInt(B_I_Q_information[1]));
		  request.addProperty("SRID",B_I_Q_information[2]);    
		  request.addProperty("InspectedDate",B_I_Q_information[3]);          
	      request.addProperty("Ins_StartTime",B_I_Q_information[4]); 
	      request.addProperty("Ins_FinishTime",B_I_Q_information[6]);
	      request.addProperty("Home_NStories",H_NSTORIES); 
	      request.addProperty("YearOfConstruction",YBUILT);
	      request.addProperty("Ins_Carrier",B_I_Q_information[9]); 
	      request.addProperty("InsuredIs",B_I_Q_information[10]); 
	      request.addProperty("InsuredOther",B_I_Q_information[11]); 
	      
	      //request.addProperty("Occupied",B_I_Q_information[12]);
	      
	      if(B_I_Q_information[12].equals("0"))
	      {
	    	  occcupied=false;
	      }
	      else
	      {  
	    	  occcupied =true;
	      }
	      
	      request.addProperty("Occupied",occcupied); 
	      
	      if(B_I_Q_information[13].equals(""))
	      {
	    	  occuperc=0;
	      }
	      else
	      {  
	    	  occuperc =Integer.parseInt(B_I_Q_information[13]);
	      }
	      request.addProperty("OccupiedPercent",occuperc);
	      
	      if(B_I_Q_information[14].equals("0"))
	      {
	    	  vacant=false;
	      }
	      else
	      {  
	    	  vacant =true;
	      }
	      
	      request.addProperty("Vacant",vacant); 
	      if(B_I_Q_information[15].equals(""))
		  {
	    	  vacantperc=0;
	      }
	      else
	      {  
	    	  vacantperc =Integer.parseInt(B_I_Q_information[15]);
	      }	      
	      request.addProperty("VacantPercent",vacantperc);
	      
	      if(B_I_Q_information[16].equals("0"))
	      {
	    	  notdetermined=false;
	      }
	      else
	      {  
	    	  notdetermined =true;
	      }
	      
	      request.addProperty("NotDetermined",notdetermined);  
	      request.addProperty("OccupancyType",B_I_Q_information[17]);
	      request.addProperty("OccupancyOther",B_I_Q_information[18]);
	      request.addProperty("ObservationType",B_I_Q_information[19]);
	      request.addProperty("ObservationOther",B_I_Q_information[20]);
	      request.addProperty("NeighborhoodIs",B_I_Q_information[21]);
	      request.addProperty("NeighborhoodOther",B_I_Q_information[22]);
	      
	      int b_size=0;
	      if(B_I_Q_information[23].equals("0"))
	      {
	    	  b_size=0;
	      }
	      else
	      {  
	    	  b_size =Integer.parseInt(B_I_Q_information[23]);;
	      }
	      request.addProperty("BuildingSize",b_size);
	      request.addProperty("NoofStories",B_I_Q_information[24]);
	      request.addProperty("BuiltYear",B_I_Q_information[25]);
	      request.addProperty("YearBuiltOption",B_I_Q_information[26]);
	      request.addProperty("YearBuiltOther",B_I_Q_information[27]);
	      request.addProperty("PermitConfirmed",B_I_Q_information[28]);
	      request.addProperty("PermitConfirmDesc",B_I_Q_information[29]);
	      request.addProperty("AdditionalStructure",B_I_Q_information[30]);
	      request.addProperty("AdditionalStructDesc",B_I_Q_information[31]);
	      request.addProperty("BalconyPresent",B_I_Q_information[32]);
	      
	      if(B_I_Q_information[33].equals(""))
		  {
	    	  noofbalconies=0;
	      }
	      else
	      {  
	    	  noofbalconies =Integer.parseInt(B_I_Q_information[33]);
	      }	    
	      
	      
	      request.addProperty("NoofBalconyPresent",noofbalconies);
	    
	      request.addProperty("BalconyRailingPresent",B_I_Q_information[34]);
	      
	      
	      request.addProperty("Location1",B_I_Q_information[35]);
	      request.addProperty("Location2",B_I_Q_information[37]);
	      request.addProperty("Location3",B_I_Q_information[39]);
	      request.addProperty("Location4",B_I_Q_information[41]);

	      /**Need to change based on the webservice ***/
	      request.addProperty("Location1Desc",B_I_Q_information[36]);
	      request.addProperty("Location2Desc",B_I_Q_information[38]);
	      request.addProperty("Location3Desc",B_I_Q_information[40]);
	      request.addProperty("Location4Desc",B_I_Q_information[42]);
	      /**Need to change based on the webservice ends ***/
	      
	      request.addProperty("OtherLocation",B_I_Q_information[43]);
	      request.addProperty("Observation1",B_I_Q_information[44]);
	      request.addProperty("Observation2",B_I_Q_information[45]);
	      /** Need to change ***/
	      request.addProperty("Observation2Desc",B_I_Q_information[46]);
	      /** Need to change ***/
	      request.addProperty("Observation3",B_I_Q_information[47]);
	      request.addProperty("Observation3Desc",B_I_Q_information[48]);
	      request.addProperty("Observation4",B_I_Q_information[49]);
	      request.addProperty("Observation4Desc",B_I_Q_information[50]);
	      request.addProperty("Observation5",B_I_Q_information[51]);
	      /** Need to change ***/
	      request.addProperty("Observation5Desc",B_I_Q_information[52]);
	      /** Need to change ***/
	     // request.addProperty("WallStructurePresent",B_I_Q_information[47]);
	     // request.addProperty("WallStructurePresent",B_I_Q_information[47]);System.out.println("request5 "+request);
	      
	     
	      if(B_I_Q_information[53].equals("0"))
	      {
	    	  iswallstructure=false;
	      }
	      else
	      {
	    	  iswallstructure=true;
	      }
	     
	      request.addProperty("WallStructurePresent",iswallstructure);
	    
	      
	      
	      request.addProperty("ConcreteUnRein",B_I_Q_information[54].replace("^",","));
	      request.addProperty("ConcreteUnReinPer",B_I_Q_information[55]);
	      request.addProperty("ConcreteRein",B_I_Q_information[56].replace("^",","));
	      request.addProperty("ConcreteReinPer",B_I_Q_information[57]);
	      request.addProperty("SolidConcrete",B_I_Q_information[58].replace("^",","));
	      request.addProperty("SolidConcretePer",B_I_Q_information[59]);    
	      request.addProperty("WoodFrame",B_I_Q_information[60].replace("^",",")); 
	      request.addProperty("WoodFramePer",B_I_Q_information[61]);
	      request.addProperty("ReinMasonry",B_I_Q_information[62].replace("^",","));
	      request.addProperty("ReinMasonryper",B_I_Q_information[63]);
	      request.addProperty("UnReinMasonry",B_I_Q_information[64].replace("^",","));
	      request.addProperty("UnReinMasonryPer",B_I_Q_information[65]);
	      request.addProperty("OtherWallStructTitle",B_I_Q_information[66]);
	      request.addProperty("OtherWallStructure",B_I_Q_information[67].replace("^",","));
	      request.addProperty("OtherWallPer",B_I_Q_information[68]);
	      //request.addProperty("WallCladdingPresent",B_I_Q_information[63]); 
	     
	      
	      if(B_I_Q_information[69].equals("0"))
	      {
	    	  iswallcladding=false;
	      }
	      else
	      {
	    	  iswallcladding=true;
	      }
	      
	      request.addProperty("WallCladdingPresent",iswallcladding);
	      request.addProperty("AluminSiding",B_I_Q_information[70].replace("^",","));
	     request.addProperty("VinylSiding",B_I_Q_information[71].replace("^",","));
	     request.addProperty("WoodSiding",B_I_Q_information[72].replace("^",","));
	      request.addProperty("CementFiber",B_I_Q_information[73].replace("^",","));
	       request.addProperty("Stucco",B_I_Q_information[74].replace("^",","));
	     request.addProperty("BrickVeneer",B_I_Q_information[75].replace("^",","));
	    request.addProperty("PaintedBlock",B_I_Q_information[76].replace("^",","));
	     request.addProperty("OtherWallCladTitle1",B_I_Q_information[77]);
	     request.addProperty("OtherWallClad1",B_I_Q_information[78].replace("^",","));
	     request.addProperty("OtherWallCladTitle2",B_I_Q_information[79]);	      
	     request.addProperty("OtherWallClad2",B_I_Q_information[80].replace("^",","));
	    request.addProperty("OtherWallCladTitle3",B_I_Q_information[81]);
	     request.addProperty("OtherWallClad3",B_I_Q_information[82].replace("^",","));
	       //request.addProperty("ISOClassifyPresent",B_I_Q_information[77]);System.out.println("request8"+request);
	   
	     if(B_I_Q_information[83].equals("0"))
	      {
	    	  //isisopresent=true;
	    	 isisopresent=false;
	      }
	      else
	      {
	    	  isisopresent=true;
	    	  //isisopresent=false;
	      }
	     
	     request.addProperty("ISOClassifyPresent",isisopresent);
	      //request.addProperty("Frame",B_I_Q_information[78]);
	      //request.addProperty("Frame",cf.decode("100");
	      request.addProperty("Frame",B_I_Q_information[84]);
	      request.addProperty("Joisted",B_I_Q_information[85]);
	      request.addProperty("NonCombust",B_I_Q_information[86]);
	      request.addProperty("MasonNonCombust",B_I_Q_information[87]);
	      request.addProperty("ModifyFireResist",B_I_Q_information[88]);
	      request.addProperty("FireResist",B_I_Q_information[89]);
	      request.addProperty("BuildingComments",B_I_Q_information[90]);
	      request.addProperty("CreatedOn",B_I_Q_information[91]); 
	      
	     
	      envelope.setOutputSoapObject(request);
		
		HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL); 
		androidHttpTransport.call(cf.NAMESPACE+"ExportGenHazardsBuildingInfo",envelope);
		String result =  envelope.getResponse().toString();
		return cf.check_Status(result);
		
		
	}else
	{
		
		return true;
	}
	
	}
	protected boolean Upload_GeneralHazardsInformation() throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException {
		// TODO Auto-generated method stub
		boolean ispoolpresent,isperimeterpoolpresent;
	SoapObject request = new SoapObject(cf.NAMESPACE,"ExportGenCondnAndHazardsInfo");
	SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
	envelope.dotNet = true;
	/** set the value get from the policy holder table starts **/
	Cursor C_GCH= cf.SelectTablefunction(cf.GHC_table, " where GCH_HZ_Homeid='" + cf.encode(cf.selectedhomeid) + "' and GCH_HZ_Inspectorid='"+ cf.encode(cf.Insp_id) + "' ");
	if(C_GCH.getCount()==1)
	{
		
			G_H_C_information=cf.setvalueToArray(C_GCH);/** set the value to the array for the easy usage **/
			
		  request.addProperty("InspectorID",G_H_C_information[1]);
		  request.addProperty("SRID",G_H_C_information[2]); ;  
		  if(G_H_C_information[3].equals("0"))
		  {
			   ispoolpresent=false;
		  }
		  else
		  {
			   ispoolpresent=true;
		  }
		  
	      request.addProperty("IsPoolPresent",ispoolpresent);    
	      request.addProperty("PoolPresent",G_H_C_information[4]);
	      request.addProperty("HotTub",G_H_C_information[5]);
	      request.addProperty("Poolstruc",G_H_C_information[6]);
	      request.addProperty("HottubCovered",G_H_C_information[7]);
	      request.addProperty("PoolSlide",G_H_C_information[8]);
	      request.addProperty("DivingBoard",G_H_C_information[9]);
	      request.addProperty("PeriPool",G_H_C_information[10]);
	      request.addProperty("PoolEnclosure",G_H_C_information[11]);
	      request.addProperty("GroundPool",G_H_C_information[12]); //Need check this value goes in to the webservice perfexctluy
	      if(G_H_C_information[13].equals("0"))
		  {
	    	  isperimeterpoolpresent=false;
		  }
		  else
		  {
			  isperimeterpoolpresent=true;
		  }
	      
	      G_H_C_information[14]=G_H_C_information[14].replace("^", ",");
	      request.addProperty("IsPerimeterPoolFence",isperimeterpoolpresent);
	      request.addProperty("PerimeterPoolFence",G_H_C_information[14]);
	      request.addProperty("OtherPeriPoolFence",G_H_C_information[15]); 
	      request.addProperty("SelfLatch",G_H_C_information[16]);
	      request.addProperty("ProfesInstall",G_H_C_information[17]);
	      request.addProperty("PoolFenceDisrepair",G_H_C_information[18]);
	     
	      request.addProperty("Vicious",G_H_C_information[19]);
	      request.addProperty("ViciousDesc",G_H_C_information[20]);
	      request.addProperty("LiveStock",G_H_C_information[21]);
	      request.addProperty("LiveStockDesc",G_H_C_information[22]);
	      request.addProperty("OverHanging",G_H_C_information[23]);
	      request.addProperty("Trampoline",G_H_C_information[24]);
	      request.addProperty("SkateBoard",G_H_C_information[25]);
	      request.addProperty("bicycle",G_H_C_information[26]);
	      
	      request.addProperty("TripHazard",G_H_C_information[28]);
	      request.addProperty("TripHazardNote",G_H_C_information[29]);
	      request.addProperty("UnsafeStairway",G_H_C_information[30]);
	      request.addProperty("PorchAndDeck",G_H_C_information[31]);
	      request.addProperty("NonStdConstruction",G_H_C_information[32]);
	      request.addProperty("OutDoorAppliances",G_H_C_information[33]);
	      request.addProperty("OpenFoundation",G_H_C_information[34]);
	      request.addProperty("WoodShingled",G_H_C_information[35]);	      
	      request.addProperty("ExcessDebris",G_H_C_information[36]);
	      request.addProperty("BusinessPremises",G_H_C_information[37]);
	      request.addProperty("BusinessPremisesDesc",G_H_C_information[38]);
	      request.addProperty("GeneralDisrepair",G_H_C_information[39]);	      
	      request.addProperty("PropertyDamage",G_H_C_information[40]);
	      request.addProperty("StructurePartial",G_H_C_information[41]);
	      request.addProperty("InOperative",G_H_C_information[42]);
	      request.addProperty("RecentDrywall",G_H_C_information[43]);	      
	      request.addProperty("ChineseDrywall",G_H_C_information[44]);	      
	      request.addProperty("ConfirmDrywall",G_H_C_information[45]);
	      request.addProperty("NonSecurity",G_H_C_information[46]);
	      request.addProperty("NonSmoke",G_H_C_information[47]);
	      /***Need to chage the property name like websrvice**/
	      request.addProperty("RecentDrywallDesc",G_H_C_information[48]);	      
	      request.addProperty("ChineseDrywallDesc",G_H_C_information[49]);	      
	      request.addProperty("ConfirmDrywallDesc",G_H_C_information[50]);
	      request.addProperty("NonSecurityDesc",G_H_C_information[51]);
	      request.addProperty("NonSmokeDesc",G_H_C_information[52]);
	      /***Need to chage the property name like websrvice Ends**/
	      request.addProperty("Comments",G_H_C_information[53]);
	      System.out.println("REQUEST "+request);
	      envelope.setOutputSoapObject(request);
		  
		HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
		androidHttpTransport.call(cf.NAMESPACE+"ExportGenCondnAndHazardsInfo",envelope);
		String result =  envelope.getResponse().toString();
		
		return cf.check_Status(result);
	}else
	{
		
		return true;
	}
	
	}
	protected boolean Uploading_Photos() throws IOException, XmlPullParserException,NetworkErrorException,SocketTimeoutException {

		// TODO Auto-generated method stub
	/** set the value get from the policy holder table starts **/
		tochkfileexists=false;
		
	Cursor C_PH= cf.SelectTablefunction(cf.ImageTable, " where GCH_IM_SRID='" + cf.encode(cf.selectedhomeid) + "'");
	

	
	if(C_PH.getCount()>=1)
	{
		
		String chkimg = "true";
	
		int countchk = C_PH.getCount();
		C_PH.moveToFirst();
		double Totaltmp = 0.0;
		double temp_inc=0.0;
		double Temp_total=total;
		Totaltmp = ((double) increment_unit / (double) countchk);
		
	//		double temptot = Totaltmp;
		int error=0;
		for (int j = 0; j < C_PH.getCount(); j++) {
			
			Exportbartext="Uploading Photos   "+(j+1)+" / "+countchk;
			String mypath = cf.decode(C_PH.getString(C_PH.getColumnIndex("GCH_IM_ImageName"))); // get the full path of the image 
			String temppath[] = mypath.split("/");
			int ss = temppath[temppath.length - 1].lastIndexOf(".");
			String tests = temppath[temppath.length - 1].substring(ss);
			String elevationttype;
			File f = new File(mypath);
			/** check the file is exist in the device **/
			if(f!=null)
			{
			if (f.exists()) {
				
				/** check the file extanstion is availabel  **/
				if (tests.equals("") || tests.equals("null")
						|| tests == null) {
					if (C_PH.getString(3).toString().equals("1")) {
						erromsg = "Front Photos Tab in the image order"
								+ C_PH.getString(9).toString() ;
					} else if (C_PH.getString(3).toString().equals("2")) {
						erromsg = "Right photos Tab in the image order"
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("3")) {
						erromsg = "Back photos Tab in the image order"
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("4")) {
						erromsg = "Left photos Tab in the image order"
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("5")) {
						erromsg = "Additional phots  Tab in the image order"
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("6")) {
						erromsg = "Roof Images Tab in the image order  "
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("7")) {
						erromsg = "Paperworksignature"
								+ C_PH.getString(9).toString();
					} else {

						

					}
					Error_traker("plroblem uploading image in "+erromsg);// calling error msg track
					//return false;
				} else {

					
					if (C_PH.getString(3).toString().equals("1")) {
						elevationttype = "FE"
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("2")) {
						elevationttype = "RE"
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("3")) {
						elevationttype = "BE"
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("4")) {
						elevationttype = "LE"
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("5")) {
						elevationttype = "IE"
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("6")) {
						elevationttype = "RS"
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("7")) {
						elevationttype = "Paperworksignature"
								+ C_PH.getString(9).toString();
					} else {

						elevationttype = "EXTRA"
								+ C_PH.getString(9).toString();

					}
					
					cf.getInspecionTypeid(cf.selectedhomeid);
					elevationttype += tests;
					Bitmap bitmap = cf.ShrinkBitmap(cf.decode(C_PH.getString(C_PH
							.getColumnIndex("GCH_IM_ImageName"))), 400, 400);

					MarshalBase64 marshal = new MarshalBase64();
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					bitmap.compress(CompressFormat.PNG, 100, out);
					byte[] raw = out.toByteArray();
					String imagetype="1";
					imagetype=C_PH.getString(C_PH.getColumnIndex("GCH_IM_Elevation"));
					
					/**Change the image type for the server side code chage the EP to type 2 and GA to type 1 **/
                    
                    
					
					SoapObject request = new SoapObject(cf.NAMESPACE,"ExportGenHazardsPicture");
					SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
					envelope.dotNet = true;
				
					request.addProperty("InspectorID",cf.Insp_id);
					request.addProperty("SRID",cf.selectedhomeid);
					//request.addProperty("inspectionTypeId", cf.Inspectiontypeid);
					
					request.addProperty("Description", cf.decode(C_PH.getString(6)));
					request.addProperty("Elevation", imagetype);
					request.addProperty("CreatedOn", cf.datewithtime);
					//request.addProperty("ModifiedDate", cf.datewithtime);
					request.addProperty("ImageOrder", C_PH.getString(9));
					request.addProperty("ImageNameWithExtension",elevationttype);
					request.addProperty("imgByte", raw);
					HttpTransportSE androidHttpTransport=null;
					try
					{
					envelope.setOutputSoapObject(request);
					marshal.register(envelope);
					System.out.println("proprer ty"+request);
					envelope.setOutputSoapObject(request);
					 androidHttpTransport = new HttpTransportSE(cf.URL);
					androidHttpTransport.call(cf.NAMESPACE+"ExportGenHazardsPicture",envelope);
					String result =  envelope.getResponse().toString();
					androidHttpTransport.reset();
					
					System.out.println("result"+result);
					error=0;
					C_PH.moveToNext();
					}
					catch (IOException e) {
						// TODO: handle exception
						//Error_traker("comes in to the io exception "+error+"  j="+j+"imag name "+mypath);
						
						if(error==0 || error==1 )
						{
						//C_PH.moveToNext();
						j=j-1;
						error++;
						}
						else
						{
							throw e;
						}
						throw e;
					}
				//	total=100;
					//return cf.check_Status(result);
					
					try {
						
						out.close();
						getNetConnectDetail();
					} catch (IOException e) {
						
					}
					 catch (Exception e)
					 {
						 
					 }
				
				}
				
			}
			else
			{
				
				if (tests.equals("") || tests.equals("null")
						|| tests == null) {
					
					if (C_PH.getString(3).toString().equals("1")) {
						erromsg = "Front Photos Tab in the image order"
								+ C_PH.getString(9).toString() ;
					} else if (C_PH.getString(3).toString().equals("2")) {
						erromsg = "Right Tab in the image order"
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("3")) {
						erromsg = "Back photos Tab in the image order"
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("4")) {
						erromsg = "Internal photos Tab in the image order"
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("5")) {
						erromsg = "Additional phots  Tab in the image order"
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("6")) {
						erromsg = "Homeownersignature  "
								+ C_PH.getString(9).toString();
					} else if (C_PH.getString(3).toString().equals("7")) {
						erromsg = "Paperworksignature"
								+ C_PH.getString(9).toString();
					} 
					Error_traker("please check the image in "+erromsg +" available " );
			}else
				{
					tochkfileexists= true;
					Error_traker("Please check the image is available in your external storage? (" + f + ") " );
				}
				
				C_PH.moveToNext();
			/** check the file is exist in the device Ends **/
		}
			}
			else
			{
				System.out.println("image not exist");
				tochkfileexists= true;
				Error_traker("Please check the image is available in your external storage? (" + f + ") " );
				C_PH.moveToNext();
			}
			
				if (total <= Temp_total+increment_unit) {
					temp_inc +=Totaltmp;
					total = Temp_total + (int) (temp_inc);
					         
				} else {
					total = Temp_total+increment_unit;
				}
			
			//Totaltmp += temptot;
	}	

		return true;

	}else
	{
		
		return true;
	}
	
		
	}
	protected boolean Uploading_feedback() throws IOException, XmlPullParserException,NetworkErrorException,SocketTimeoutException {
		// TODO Auto-generated method stub
		feedback_fileexits=false;
		Exportbartext="Uploading Feedback Information.";
			if(feedbackinfo())
			{
				
				cf.Create_Table(11);

				Bitmap bitmap;
			 /** Uploading Feed back document Starts here   **/
				
				
				Cursor c = cf.SelectTablefunction(cf.FeedBackDocumentTable,
						"where GCH_FI_D_SrID='" + cf.selectedhomeid + "'");

				
				if(c.getCount()>=1)
				{
					MarshalBase64 marshal = new MarshalBase64();
					ByteArrayOutputStream out = null;				
					c.moveToFirst();
					
					int countchk = c.getCount();
					double Totaltmp = 0.0;
					double Temp_total=total;double temp_inc=0.0;
					Totaltmp = ((double) increment_unit / (double) countchk);
					
					 
					int error=0;
					for (int j = 0; j < c.getCount(); j++) 
					{
						Exportbartext="Uploading feedback documents   "+(j+1)+" / "+countchk;
						String substring = cf.decode(c.getString(c.getColumnIndex("GCH_FI_D_FileName")));
						SoapObject request = new SoapObject(cf.NAMESPACE,"ExportGenHazardFeedbackDocument");
						SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
						envelope.dotNet = true;
						request.addProperty("InspectorID", cf.Insp_id);
						request.addProperty("SRID", c.getString(1));
						request.addProperty("InspectionTypeId", cf.Inspectiontypeid);
						request.addProperty("DocumentTitle", cf.decode(c.getString(2)));
						request.addProperty("IsOfficeUse", c.getString(6));
						request.addProperty("CreatedDate", c.getString(7));
						request.addProperty("ModifiedDate", c.getString(8));
						request.addProperty("ImageOrder", c.getInt(5));

						if (substring.endsWith(".pdf")) 
						{
							File dir = Environment.getExternalStorageDirectory();
							if (substring.startsWith("file:///")) 
							{
								substring = substring.substring(11);
							}
							else
							{
								substring = substring;
							}
							File assist = new File(substring);
							if (assist.exists()) 
							{
								String mypath = substring;
								String temppath[] = mypath.split("/");
								int ss = temppath[temppath.length - 1].lastIndexOf(".");// substring(".");
								String tests = temppath[temppath.length - 1].substring(ss);
								String namedocument;
								if (tests.equals(".pdf")) 
								{
									namedocument = "pdf_document" + j + ".pdf";
									request.addProperty("ImageNameWithExtension",namedocument);
									try
									{
										InputStream fis = new FileInputStream(assist);
										long length = assist.length();
										byte[] bytes = new byte[(int) length];
										int offset = 0;
										int numRead = 0;
										while (offset < bytes.length && (numRead = fis.read(bytes, offset, bytes.length - offset)) >= 0)
										{
											offset += numRead;
										}
										strBase64 = Base64.encode(bytes);										
										request.addProperty("imgByte", strBase64);		
										try
										{
										envelope.setOutputSoapObject(request);
										HttpTransportSE httpTransport = new HttpTransportSE(cf.URL);
										HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
										httpTransport.call(cf.NAMESPACE+"ExportGenHazardFeedbackDocument", envelope);/**Need to change based on the web method **/
										String result =  envelope.getResponse().toString();
										
										cf.check_Status(result);
										error=0;
										}
										catch (IOException e) {
											// TODO: handle exception
											if(error==0 || error==1 )
											{
												c.moveToPrevious();
											j=j-1;
											error++;
											}
											else
											{
												throw e;
											}

										}
									}
									catch (Exception e) 
									{ 
										feedback_fileexits=true;
										Error_traker("You have problem in uploading documentin the feed back document tab Document Title"+cf.decode(c.getString(2)) );
										/*cf.errorlogmanage(" Problem while opening the file ="+ e.getMessage()+ " path of the file is ="+ substring,selectedItem.toString());
										return "false";*/
									}
								}
								else
								{
									System.out.println("issues in extention missing");
									feedback_fileexits=true;
									Error_traker("You have problem in uploading documentin the feed back document tab Document Title"+cf.decode(c.getString(2)) );
									
								}

							}
							else
							{
								System.out.println("issues in error file not available");
								feedback_fileexits=true;
								Error_traker("Please check the PDF is available in your external storage? (" + substring + ")");
							}
						} 
						else
						{
							String mypath = substring;
							String temppath[] = mypath.split("/");
							int ss = temppath[temppath.length - 1].lastIndexOf(".");// substring(".");
							String tests = temppath[temppath.length - 1].substring(ss);
							String elevationttype;
							File f = new File(substring);
							if (f.exists()) 
							{
								/**check weather the extention is not null**/
								if (tests.equals("") || tests.equals("null")|| tests == null) 
								{
									feedback_fileexits=true;
									Error_traker("You have problem in uploading documentin the feed back document tab Document Title"+cf.decode(c.getString(2)) );
									
									
								}
								else
								{
									elevationttype = "feedbackimage" + j + tests;
									request.addProperty("ImageNameWithExtension",elevationttype);
									bitmap = cf.ShrinkBitmap(substring, 800, 800);
									
									out = new ByteArrayOutputStream();
									bitmap.compress(CompressFormat.PNG, 100, out);
									byte[] raw = out.toByteArray();
									System.out.println("request"+request.toString());
									request.addProperty("imgByte", raw);
									marshal.register(envelope);
								    // System.out.println("request"+request.toString());
									try
									{
									envelope.setOutputSoapObject(request);
									HttpTransportSE httpTransport = new HttpTransportSE(
											cf.URL);
									HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
									httpTransport.call(cf.NAMESPACE+"ExportGenHazardFeedbackDocument", envelope);

									String result =  envelope.getResponse().toString();
									System.out.println("Result is "+j+result);
									cf.check_Status(result);
									error=0;
									}
									catch (IOException e) {
										// TODO: handle exception
										if(error==0 || error==1 )
										{
										c.moveToPrevious();
										j=j-1;
										error++;
										}
										else
										{
											throw e;
										}
	
									}
									try 
									{
										out.close();
									}
									catch (IOException e) 
									{
									}
								}

								
							}
							else
							{
								feedback_fileexits=true;
								Error_traker("Please check the image is available in your external storage? (" + substring + ") ");
							}
							
						}						
						
						c.moveToNext();
						if (total <= Temp_total+increment_unit) {
							temp_inc +=Totaltmp;
							total = Temp_total + (int) (temp_inc);
							 
						} else {
							total = Temp_total+increment_unit;
						}
					}
			}
			else
			{
			   		
			}
			/** Uploading Feed back document Ends here   **/
			
			}
		
	return true;
	}
	private boolean feedbackinfo() throws IOException, XmlPullParserException,NetworkErrorException,SocketTimeoutException {
					
					String fdinfochk;
					cf.Create_Table(10);
					cf.getInspecionTypeid(cf.selectedhomeid);
					
					Cursor c = cf.SelectTablefunction(cf.FeedBackInfoTable,
							"where GCH_FI_Srid='" + cf.selectedhomeid + "'");
					if(c.getCount()>=1)
					{
					c.moveToFirst();
					SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
					envelope.dotNet = true;
										SoapObject ad_property = new SoapObject(cf.NAMESPACE,"ExportGenHazardsFeedbackInformation");
					ad_property.addProperty("InspectorID", cf.Insp_id);
					ad_property.addProperty("InsTypeid", cf.Inspectiontypeid);
					
					ad_property.addProperty("SRID", c.getString(1));
					ad_property.addProperty("Weather", "");
					ad_property.addProperty("Temperature", "");
					ad_property.addProperty("IsHOSatisfied", c.getInt(4));
					ad_property.addProperty("IsCusServiceCompltd",c.getInt(5));
					ad_property.addProperty("IsInspectionPaperAvbl",0);
					ad_property.addProperty("IsManufacturerInfo",0);
					ad_property.addProperty("PresentatInspection",c.getString(3));
					ad_property.addProperty("FeedbackComments",	cf.decode(c.getString(6)));
					ad_property.addProperty("Addendum", "");
					ad_property.addProperty("OfficeComments", "");
					ad_property.addProperty("CreatedOn", c.getString(7));
					ad_property.addProperty("OtherPresent",cf.decode(c.getString(8)));
					envelope.setOutputSoapObject(ad_property);
					HttpTransportSE androidHttpTransport1 = new HttpTransportSE(cf.URL);
					System.out.println("No more issues in the property"+ad_property+"Url"+cf.URL);
					androidHttpTransport1.call(cf.NAMESPACE+"ExportGenHazardsFeedbackInformation", envelope);
					String result1 = String.valueOf(envelope.getResponse());
					//c.close();
					System.out.println("result1"+result1);
					return cf.check_Status(result1);
					}
					return true;
					
					}
		
	private void Error_traker(String ErrorMsg) {
		// TODO Auto-generated method stub
		System.out.println("error msg"+ErrorMsg);
		if(erro_trce==null)
		{
			erro_trce=new String[1];
			
			erro_trce[0]=ErrorMsg;
		}
		else
		{
			
			String tmp[]=new String[this.erro_trce.length+1];
			int i;
			for(i =0;i<erro_trce.length;i++)
			{
				
				tmp[i]=erro_trce[i];
			}
		
			tmp[tmp.length-1]=ErrorMsg;
			erro_trce=null;
			erro_trce=tmp.clone();
		}
	}

	private void dbquery() {

			int k = 1;
			cf.data = null;
			cf.inspdata = "";
			cf.sql = "select * from " + cf.policyholder;
			if (!cf.res.equals("")) {

				cf.sql += " where (GCH_PH_FirstName like '%" + cf.encode(cf.res)
						+ "%' or GCH_PH_LastName like '%" + cf.encode(cf.res)
						+ "%' or GCH_PH_Policyno like '%" + cf.encode(cf.res)
						+ "%') and GCH_PH_InspectorId='" + cf.encode(cf.Insp_id) + "'";
				if(cf.onlstatus.equals("export"))
				{
					cf.sql += " and GCH_PH_IsInspected='1' ";
				}
				else if(cf.onlstatus.equals("Reexport"))
				{
					cf.sql += " and GCH_PH_IsInspected='2' and GCH_PH_IsUploaded='1' ";
				}

			} else {
				if(cf.onlstatus.equals("export"))
				{
					cf.sql += " where GCH_PH_IsInspected='1'  and GCH_PH_InspectorId='" + cf.encode(cf.Insp_id) + "' ";
				}
				else if(cf.onlstatus.equals("Reexport"))
				{
					cf.sql += " where GCH_PH_IsInspected='2' and GCH_PH_IsUploaded='1' and GCH_PH_InspectorId='" + cf.encode(cf.Insp_id) + "' ";
				}

			}
			
			Cursor cur = cf.gch_db.rawQuery(cf.sql, null);
			cf.rws = cur.getCount();
			title.setText("Total Record : " + cf.rws);
			cf.data = new String[cf.rws];
			cf.countarr = new String[cf.rws];
			
			int i=0,j = 0;
			cur.moveToFirst();
			this.cf.data = new String[cf.rws];
			if (cur.getCount() >= 1) {
				do {//
					String s =(cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_FirstName"))).trim().equals("")) ? "-":cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_FirstName")));
					cf.inspdata += " "+ s+ " ";
					s=(cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_LastName"))).trim().equals("")) ? "-":cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_LastName")));
					this.cf.data[i] += s + " | ";
					s=(cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_Policyno"))).trim().equals("")) ? "-":cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_Policyno")));
					this.cf.data[i] += s+ " \n ";
					s=(cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_Address1"))).trim().equals("")) ? "-":cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_Address1")));
					this.cf.data[i]+= s + " | ";
					s=(cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_City"))).trim().equals("")) ? "-":cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_City")));
					this.cf.data[i] += s + " | ";
					s=(cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_State"))).trim().equals("")) ? "-":cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_State")));
					this.cf.data[i] += s + " | ";
					s=(cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_County"))).trim().equals("")) ? "-":cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_County")));
					this.cf.data[i] += s + " | ";
					s=(cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_Zip"))).trim().equals("")) ? "-":cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_Zip")));
					this.cf.data[i] += s+ " \n ";
					s=(cf.decode(cur.getString(cur.getColumnIndex("GCH_Schedule_ScheduledDate"))).trim().equals("")) ? "-":cf.decode(cur.getString(cur.getColumnIndex("GCH_Schedule_ScheduledDate")));
					this.cf.data[i] += s + " | ";
					s=(cf.decode(cur.getString(cur.getColumnIndex("GCH_Schedule_InspectionStartTime"))).trim().equals("")) ? "-":cf.decode(cur.getString(cur.getColumnIndex("GCH_Schedule_InspectionStartTime")));
					this.cf.data[i] += s + " - ";
					s=(cf.decode(cur.getString(cur.getColumnIndex("GCH_Schedule_InspectionEndTime"))).trim().equals("")) ? "-":cf.decode(cur.getString(cur.getColumnIndex("GCH_Schedule_InspectionEndTime")));
					this.cf.data[i] += s ;
					
					cf.countarr[j] = cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_SRID")));
					
					j++;

					if (this.cf.data[i].contains("null")) {
						this.cf.data[i] = this.cf.data[i].replace("null", "");
					}
					if (this.cf.data[i].contains("N/A |")) {
						this.cf.data[i] = this.cf.data[i].replace("N/A |", "");
					}
					if (this.cf.data[i].contains("N/A - N/A")) {
						this.cf.data[i] = this.cf.data[i].replace("N/A - N/A", "");
					}
					if(cf.data[i].endsWith("-")) {
						cf.data[i] = cf.data[i].substring(0,cf.data[i].length()-2);
					}
					i++;
				} while (cur.moveToNext());
				cf.search_text.setText("");
				display();
			} else {
			//	cf.ShowToast("Sorry, No records found ", 1);
				cf.onlinspectionlist.removeAllViews();
			}

		}
    private void display() {
		    cf.onlinspectionlist.removeAllViews();
			cf.sv = new ScrollView(this);
			cf.onlinspectionlist.addView(cf.sv);

			final LinearLayout l1 = new LinearLayout(this);
			l1.setOrientation(LinearLayout.VERTICAL);
			cf.sv.addView(l1);

			
			cf.tvstatus = new TextView[cf.rws];
			cf.deletebtn = new Button[cf.rws];
				for (int i = 0; i < cf.data.length; i++) {
					LinearLayout l2 = new LinearLayout(this);
					LinearLayout.LayoutParams mainparamschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
					l2.setLayoutParams(mainparamschk);
					l2.setOrientation(LinearLayout.HORIZONTAL);
					l1.addView(l2);

	
	/*				LinearLayout l2 = new LinearLayout(this);
					l2.setOrientation(LinearLayout.HORIZONTAL);
					l1.addView(l2);
*/
					LinearLayout lchkbox = new LinearLayout(this);
					LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
							 ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
					paramschk.topMargin = 8;
					paramschk.leftMargin = 20;
					paramschk.bottomMargin = 10;
					l2.addView(lchkbox);

					cf.tvstatus[i] = new TextView(this);
					cf.tvstatus[i].setMinimumWidth(580);
					cf.tvstatus[i].setMaxWidth(580);
					cf.tvstatus[i].setTag("textbtn" + i);
					cf.tvstatus[i].setText(cf.data[i]);
					cf.tvstatus[i].setTextColor(Color.WHITE);
					cf.tvstatus[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
					lchkbox.addView(cf.tvstatus[i], paramschk);
					
					/*LinearLayout ldelbtn = new LinearLayout(this);
					LinearLayout.LayoutParams paramsdelbtn = new LinearLayout.LayoutParams(
							93, 37);
					paramsdelbtn.rightMargin = 10;
					paramsdelbtn.bottomMargin = 10;
					
					l2.addView(ldelbtn);*/
					
					
                 /** DELETE BUTTON NO NEED FOR THE EXPORT AND ONCLICK EVENT SETTIN INSPECTION SO I JUST HIDE IT NOW   **/
					cf.tvstatus[i].setOnClickListener(new OnClickListener() {
								
								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub
									String getidofselbtn = v.getTag().toString();
									final String repidofselbtn = getidofselbtn.replace(
											"textbtn", "");
									final int cvrtstr = Integer.parseInt(repidofselbtn);
									dt = cf.countarr[cvrtstr];
									/** Set the alert listener for the export option **/
									
									if(cf.onlstatus.equals("Reexport"))
									{
										cf.selectedhomeid=dt;
										String source = "<b><font color=#00FF33>Retrieving data. Please wait..."
												+ "</font></b>";
										cf.pd = ProgressDialog.show(ExportInspection.this, "", Html.fromHtml(source), true);
										/*cf.gch_db.execSQL("UPDATE "+ cf.policyholder+ " SET GCH_PH_IsInspected=1,GCH_PH_IsUploaded=0,GCH_PH_SubStatus=40 WHERE GCH_PH_SRID ='"
												+ cf.selectedhomeid
												+ "'");
										cf.ShowToast("The status changed", 1);*/
										Thread thread = new Thread(ExportInspection.this);
										thread.start();
										/*try {   
											cf.exprtcnt=cf.fn_CheckExportCount(cf.Insp_id,dt);
										} catch (SocketException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
											
										} catch (NetworkErrorException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										} catch (IOException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										} catch (TimeoutException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										} catch (XmlPullParserException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										} catch (Exception e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										}
										
										System.out.println("reexport"+cf.exprtcnt);
											    cf.alerttitle="Re-export";
											    System.out.println(Html.fromHtml("This inspection has been exported already.Details :"+cf.exprtcnt+
													    "Are you sure want to Re-export again?"));
											    cf.alertcontent="This inspection has been exported already."+"<br /><br />"+"<b>Details :</b>"+cf.exprtcnt+
													    "<b>Are you sure want to Re-export again?</b>";
											    final Dialog dialog1 = new Dialog(ExportInspection.this,android.R.style.Theme_Translucent_NoTitleBar);
												dialog1.getWindow().setContentView(R.layout.alertsync);
												TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
												txttitle.setText( cf.alerttitle);
												TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
												txt.setText(Html.fromHtml( cf.alertcontent));
												Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
												Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
												btn_yes.setOnClickListener(new OnClickListener()
												{
								                	@Override
													public void onClick(View arg0) {
														// TODO Auto-generated method stub
														dialog1.dismiss();
														
														fn_export();
													}
													
												});
												btn_cancel.setOnClickListener(new OnClickListener()
												{

													@Override
													public void onClick(View arg0) {
														// TODO Auto-generated method stub
														dialog1.dismiss();
														
													}
													
												});
												dialog1.setCancelable(false);
												dialog1.show();*/
										//}
										

									}
									else
									{
										fn_export();
									}
																		
								}

								
							});
					if(cf.onlstatus.equals("Reexport")) {
						LinearLayout ldelbtn = new LinearLayout(this);
						LinearLayout.LayoutParams paramsdelbtn = new LinearLayout.LayoutParams(
								ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				        paramsdelbtn.setMargins(0, 10, 10, 0); //left, top, right, bottom
						ldelbtn.setLayoutParams(mainparamschk);
						ldelbtn.setGravity(Gravity.RIGHT);
					    l2.addView(ldelbtn);
					   

					cf.deletebtn[i] = new Button(this);
					cf.deletebtn[i].setBackgroundResource(R.drawable.deletebtn1);
					cf.deletebtn[i].setTag("deletebtn" + i);
					cf.deletebtn[i].setPadding(30, 0, 0, 0);
					cf.deletebtn[i].setGravity(Gravity.RIGHT);
					ldelbtn.addView(cf.deletebtn[i], paramsdelbtn);

					cf.deletebtn[i].setOnClickListener(new View.OnClickListener() {
						public void onClick(final View v) {
							String getidofselbtn = v.getTag().toString();
							final String repidofselbtn = getidofselbtn.replace(
									"deletebtn", "");
							final int cvrtstr = Integer.parseInt(repidofselbtn);
							final String dt = cf.countarr[cvrtstr];
							 cf.alerttitle="Delete";
							  cf.alertcontent="Are you sure want to delete?";
							    final Dialog dialog1 = new Dialog(ExportInspection.this,android.R.style.Theme_Translucent_NoTitleBar);
								dialog1.getWindow().setContentView(R.layout.alertsync);
								TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
								txttitle.setText( cf.alerttitle);
								TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
								txt.setText(Html.fromHtml( cf.alertcontent));
								Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
								Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
								btn_yes.setOnClickListener(new OnClickListener()
								{
				                	@Override
									public void onClick(View arg0) {
										// TODO Auto-generated method stub
										dialog1.dismiss();
										cf.fn_delete(dt);
										dbquery();
									}
									
								});
								btn_cancel.setOnClickListener(new OnClickListener()
								{

									@Override
									public void onClick(View arg0) {
										// TODO Auto-generated method stub
										dialog1.dismiss();
										
									}
									
								});
								dialog1.setCancelable(false);
								dialog1.show();
							
						}
					});
					}
				/** DELETE BUTTON NO NEED FOR THE EXPORT INSPECTION SO I JUST HIDE IT NOW ENDS   **/
				        }
				
				
			}	
    protected void fn_export() {
		// TODO Auto-generated method stub
    	final Dialog alert=cf.showalert();
		Button btn_helpclose = (Button) alert.findViewById(R.id.helpclose_ex);
		Button btn_helpclose_t = (Button) alert.findViewById(R.id.helpclose_ex_t);
		btn_helpclose.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				for(int i=1;i<Export_chk.length;i++)
				{
					Export_chk[i]="0";
				}
				
				alert.setCancelable(true);
				alert.dismiss();
			}
			
		});
		btn_helpclose_t.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				for(int i=1;i<Export_chk.length;i++)
				{
					Export_chk[i]="0";
				}
				
				alert.setCancelable(true);
				alert.dismiss();
			}
			
		});
		alert_chk[0]=(CheckBox) alert.findViewById(R.id.Export_chk_all);
		alert_chk[1]=(CheckBox) alert.findViewById(R.id.Export_chk_GI);
		alert_chk[2]=(CheckBox) alert.findViewById(R.id.Export_chk_BI);
		alert_chk[3]=(CheckBox) alert.findViewById(R.id.Export_chk_GH);
		alert_chk[4]=(CheckBox) alert.findViewById(R.id.Export_chk_PH);
		alert_chk[5]=(CheckBox) alert.findViewById(R.id.Export_chk_FB);
		alert_chk[6]=(CheckBox) alert.findViewById(R.id.Export_chk_RS);
		
		alert_chk[0].setOnClickListener(new clicker());
		alert_chk[1].setOnClickListener(new clicker());
		alert_chk[2].setOnClickListener(new clicker());
		alert_chk[3].setOnClickListener(new clicker());
		alert_chk[4].setOnClickListener(new clicker());
		alert_chk[5].setOnClickListener(new clicker());
		alert_chk[6].setOnClickListener(new clicker());

		Button export=(Button) alert.findViewById(R.id.export_now);
		LinearLayout lin = (LinearLayout)alert.findViewById(R.id.ExportOption);
		lin.setVisibility(View.VISIBLE);
		LinearLayout lin1= (LinearLayout)alert.findViewById(R.id.maintable);
		lin1.setVisibility(View.GONE);
		export.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				int j=0;
				double incre_unit;
				for(int i=1;i<Export_chk.length;i++)
				{
					if(Export_chk[i].equals("1"))
						j++;
				}
				try
				{
					incre_unit=80.00/Double.parseDouble(String.valueOf(j));
					
				}
				catch(Exception e)
				{
					incre_unit=10.00;
				}
				if(j>=1)
				{
					
					alert.setCancelable(true);
					alert.dismiss();
					
				/** export will start here **/
				StartExport(dt,incre_unit);
				/** export will Ends here **/
				}
				else
				{
					cf.ShowToast("Please select atleast one option ", 1);
				}
			}
		});
		alert.setCancelable(false);
		alert.show();
 
	}
	private class clicker implements OnClickListener 
			{
			
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					switch (v.getId()) {
					case R.id.Export_chk_all:
						if(alert_chk[0].isChecked())
						{
							Export_chk[0]="1";
							Export_chk[1]="1";
							Export_chk[2]="1";
							Export_chk[3]="1";
							Export_chk[4]="1";
							Export_chk[5]="1";
							Export_chk[6]="1";
							
							alert_chk[1].setChecked(true);
							alert_chk[2].setChecked(true);
							alert_chk[3].setChecked(true);
							alert_chk[4].setChecked(true);
							alert_chk[5].setChecked(true);
							alert_chk[6].setChecked(true);
							alert_chk[0].setChecked(true);
							
						}
						else
						{
							Export_chk[0]="0";
							Export_chk[1]="0";
							Export_chk[2]="0";
							Export_chk[3]="0";
							Export_chk[4]="0";
							Export_chk[5]="0";
							Export_chk[6]="0";
							
							alert_chk[1].setChecked(false);
							alert_chk[2].setChecked(false);
							alert_chk[3].setChecked(false);
							alert_chk[4].setChecked(false);
							alert_chk[5].setChecked(false);
							alert_chk[6].setChecked(false);
							alert_chk[0].setChecked(false);
							
						}
						
					break;
					case R.id.Export_chk_GI:
						if(alert_chk[1].isChecked())
						{
							Export_chk[1]="1";
						}
						else
						{
							Export_chk[0]="0";
							Export_chk[1]="0";
							alert_chk[0].setChecked(false);
						}
					break;
					case R.id.Export_chk_BI:
						if(alert_chk[2].isChecked())
						{
							Export_chk[2]="1";
						}
						else
						{
							Export_chk[0]="0";
							Export_chk[2]="0";
							alert_chk[0].setChecked(false);
						}
					break;
					case R.id.Export_chk_GH:
						if(alert_chk[3].isChecked())
						{
							Export_chk[3]="1";
						}
						else
						{
							Export_chk[0]="0";
							Export_chk[3]="0";
							alert_chk[0].setChecked(false);
						}
					break;
					case R.id.Export_chk_RS:
						
						if(alert_chk[6].isChecked())
						{
							
							Export_chk[6]="1";
						}
						else
						{
							Export_chk[0]="0";
							Export_chk[6]="0";
							alert_chk[0].setChecked(false);
						}
					break;
					case R.id.Export_chk_PH:
						
						if(alert_chk[4].isChecked())
						{
							
							Export_chk[4]="1";
						}
						else
						{
							Export_chk[0]="0";
							Export_chk[4]="0";
							alert_chk[0].setChecked(false);
						}
					break;
					case R.id.Export_chk_FB:
						if(alert_chk[5].isChecked())
						{
							Export_chk[5]="1";
						}
						else
						{
							Export_chk[0]="0";
							Export_chk[5]="0";
							alert_chk[0].setChecked(false);
						}
					break;
					case R.id.completed_insp:
							Intent inte = new Intent(getApplicationContext(),ExportInspection.class);
							
							inte.putExtra("type", "Reexport");
							startActivity(inte);
					break;
					case R.id.export_insp:
						Intent inte1 = new Intent(getApplicationContext(),ExportInspection.class);
						inte1.putExtra("type", "export");
						startActivity(inte1);
						break;
					default:
						break;
					}

					
				}
			} 
/** PROGRESS DILOG WITH LODING DATA INFORMATION **/		
@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case 1:
			
			
			progDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			progDialog.setMax(maxBarValue);
			progDialog.setMessage(Html.fromHtml("Please be patient as we export your file. When complete you must log into www.paperlessinspectors.com and submit this file for auditing.  \n"+Exportbartext));
			progDialog.setCancelable(false);
			progThread = new ProgressThread(handler1);
			progThread.start();
			return progDialog;
		default:
			return null;
		}
	}
 
	class ProgressThread extends Thread {


	// Class constants defining state of the thread
	final static int DONE = 0;

	Handler mHandler;

	ProgressThread(Handler h) {
		mHandler = h;
	}

	@Override
	public void run() {
		mState = RUNNING;
		
		while (mState == RUNNING) {
			try {
				
				// Control speed of update (but precision of delay not
				// guaranteed)
				Thread.sleep(delay);
			} catch (InterruptedException e) {
				//Log.e("ERROR", "Thread was Interrupted");
			}

			Message msg = mHandler.obtainMessage();
			Bundle b = new Bundle();
			b.putInt("total", (int)total);
			msg.setData(b);
			mHandler.sendMessage(msg);

		}
	}

	public void setState(int state) {
		mState = state;
	}

}
	/** Upload policy holder information Starts **/
	final Handler handler1 = new Handler() {

		public void handleMessage(Message msg) {
			// Get the current value of the variable total from the message data
			// and update the progress bar.
			
			int total = msg.getData().getInt("total");
			
			progDialog.setProgress(total);
			progDialog.setMessage(Html.fromHtml("Please be patient as we export your file.<br> When complete you must log into www.paperlessinspector.com and submit this file for auditing. <br> Please don't lock your screen it may affect your export.<br> "+Exportbartext));
			//Exportbartext="";
			if (total == 100) {
				progDialog.setCancelable(true);
				dismissDialog(typeBar);
				
				//handler2.sendMessage(msg2);
				handler2.sendEmptyMessage(0);
				progThread.setState(ProgressThread.DONE);


			}

		}
	};
	final Handler handler2 = new Handler() {

		private AlertDialog alertDialog;

		@Override
		public void handleMessage(Message msg) {
		//	progThread.destroy();
			LinearLayout partial_head;
			if(cf.Chk_Inspector.equals("true")){
			
		
			ImageView sta_im[]=new ImageView[9];
			TableRow sta_row[]=new TableRow[9];
			// TODO Auto-generated method stub
			/** Set the  dilog to cancel if any thing shown **/
			if(dialog1!=null)
			{ 
				if(dialog1.isShowing())
				{
					dialog1.dismiss();
				}
			}
			/** Set the  dilog to cancel if any thing  shown ends **/
				dialog1 = new Dialog(ExportInspection.this,android.R.style.Theme_Translucent_NoTitleBar);
				dialog1.getWindow().setContentView(R.layout.alertexportcompleted);
				
				sta_chk[0]=(CheckBox) dialog1.findViewById(R.id.sta_chk1);
				sta_chk[1]=(CheckBox) dialog1.findViewById(R.id.sta_chk2);
				sta_chk[2]=(CheckBox) dialog1.findViewById(R.id.sta_chk3);
				sta_chk[3]=(CheckBox) dialog1.findViewById(R.id.sta_chk4);
				sta_chk[4]=(CheckBox) dialog1.findViewById(R.id.sta_chk5);
				sta_chk[5]=(CheckBox) dialog1.findViewById(R.id.sta_chk6);
			
				partial_head= (LinearLayout)  dialog1.findViewById(R.id.partial_head);
				sta_row[0]=(TableRow) dialog1.findViewById(R.id.sta_row1);
				sta_row[1]=(TableRow) dialog1.findViewById(R.id.sta_row2);
				sta_row[2]=(TableRow) dialog1.findViewById(R.id.sta_row3);
				sta_row[3]=(TableRow) dialog1.findViewById(R.id.sta_row4);
				sta_row[4]=(TableRow) dialog1.findViewById(R.id.sta_row5);
				sta_row[5]=(TableRow) dialog1.findViewById(R.id.sta_row6);
				
				
				
				sta_im[0]=(ImageView) dialog1.findViewById(R.id.sta_im1);
				sta_im[1]=(ImageView) dialog1.findViewById(R.id.sta_im2);
				sta_im[2]=(ImageView) dialog1.findViewById(R.id.sta_im3);
				sta_im[3]=(ImageView) dialog1.findViewById(R.id.sta_im4);
				sta_im[4]=(ImageView) dialog1.findViewById(R.id.sta_im5);
				sta_im[5]=(ImageView) dialog1.findViewById(R.id.sta_im6);
				
				final Button detailbut =(Button) dialog1.findViewById(R.id.error_detail); 
				
				final LinearLayout tlist=(LinearLayout) dialog1.findViewById(R.id.error_reports);
				if(erro_trce!=null)
				{
					if(erro_trce.length>=1)
					{
						/*ArrayAdapter arrayAdapter = new ArrayAdapter(ExportInspection.this, android.R.layout.simple_list_item_1,erro_trce);
						tlist.setAdapter(arrayAdapter);
*						
*/						//LinearLayout litop= new LinearLayout(ExportInspection.this);
						LinearLayout.LayoutParams lm= new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,2);
						/*litop.setLayoutParams(lm);
						litop.setBackgroundColor(Color.GREEN);*/
						detailbut.setVisibility(View.VISIBLE);
					for(int i=0;i<erro_trce.length;i++)
					{
						
						
						TextView tv1=new TextView(ExportInspection.this);
						tv1.setText(erro_trce[i]);
						tv1.setTextSize(14);
						tv1.setTextColor(Color.parseColor("#000000"));
						LinearLayout litop= new LinearLayout(ExportInspection.this);
						litop.setLayoutParams(lm);
						litop.setBackgroundColor(Color.GREEN);
						tlist.addView(litop);
						tlist.addView(tv1);
						
						
					}
					
					}
					else
					{
						detailbut.setVisibility(View.GONE);
					}
				}
				else
				{
					detailbut.setVisibility(View.GONE);
				}
				
				/** Set the tick icon , cross icon and visibility for the alert**/
				
				if(Export_chk[1].equals("1"))
				{
					sta_row[0].setVisibility(View.VISIBLE);
					if(Export_status[0])
					{
						sta_im[0].setBackgroundResource(R.drawable.tick_icon);	
					}
					else
					{
						sta_im[0].setBackgroundResource(R.drawable.cross_icon);
					}
				}
				if(Export_chk[2].equals("1"))
				{
					sta_row[1].setVisibility(View.VISIBLE);
					if(Export_status[1])
					{
						sta_im[1].setBackgroundResource(R.drawable.tick_icon);	
					}
					else
					{
						sta_im[1].setBackgroundResource(R.drawable.cross_icon);
					}
				}
				if(Export_chk[3].equals("1"))
				{
					sta_row[2].setVisibility(View.VISIBLE);
					if(Export_status[2])
					{
						sta_im[2].setBackgroundResource(R.drawable.tick_icon);	
					}
					else
					{
						sta_im[2].setBackgroundResource(R.drawable.cross_icon);
					}
				}
				if(Export_chk[4].equals("1"))
				{
					sta_row[3].setVisibility(View.VISIBLE);
					

					
					if(Export_status[3])
					{
						if(tochkfileexists==true)
						{
							partial_head.setVisibility(View.VISIBLE);
							
							sta_im[3].setBackgroundResource(R.drawable.tick_icon_partial);
						}
						else
						{
							sta_im[3].setBackgroundResource(R.drawable.tick_icon);	
						}
						//sta_im[3].setBackgroundResource(R.drawable.tick_icon);	
					}
					else
					{
						sta_im[3].setBackgroundResource(R.drawable.cross_icon);
					}
				}
				if(Export_chk[5].equals("1"))
				{
					sta_row[4].setVisibility(View.VISIBLE);
					if(Export_status[4])
					{
						if(feedback_fileexits==true)
						{
							partial_head.setVisibility(View.VISIBLE);
							sta_im[4].setBackgroundResource(R.drawable.tick_icon_partial);
						}
						else
						{
							sta_im[4].setBackgroundResource(R.drawable.tick_icon);	
						}
						
						//sta_im[4].setBackgroundResource(R.drawable.tick_icon);	
					}
					else
					{
						sta_im[4].setBackgroundResource(R.drawable.cross_icon);
					}
				}
				if(Export_chk[6].equals("1"))
				{
					sta_row[5].setVisibility(View.VISIBLE);
					if(Export_status[5])
					{
						sta_im[5].setBackgroundResource(R.drawable.tick_icon);	
					}
					else
					{
						sta_im[5].setBackgroundResource(R.drawable.cross_icon);
					}
				}
				
		
				Button btn_helpclose = (Button) dialog1.findViewById(R.id.helpclose_ex_t);
				Button btn_helpclose_x = (Button) dialog1.findViewById(R.id.helpclose_ex);
				
				
				Button select = (Button) dialog1.findViewById(R.id.re_export_now);
				btn_helpclose.setOnClickListener(new OnClickListener()
				{ 

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						for(int i=1;i<Export_chk.length;i++)
						{
							Export_chk[i]="0";
						}
						dialog1.setCancelable(true);
						dialog1.dismiss();
						dialog1.setCancelable(true);
						dialog1.dismiss();
						alertDialog = new AlertDialog.Builder(ExportInspection.this).create();
						alertDialog.setMessage(status);
						alertDialog.setButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								 Intent inte;
								 inte = new Intent(getApplicationContext(),ExportInspection.class);
								if(move_to_pre==1)
								{			  
								 
								  inte.putExtra("type", "Reexport");
								}
								else
								{
												 
									  inte.putExtra("type", "export");	
								}
								  startActivity(inte);
                                   
							}
						});
				alertDialog.show();
					
					}
					
				});
				btn_helpclose_x.setOnClickListener(new OnClickListener()
				{

					@Override 
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						for(int i=1;i<Export_chk.length;i++)
						{
							Export_chk[i]="0";
						}
						
						dialog1.setCancelable(true);
						dialog1.dismiss();
						alertDialog = new AlertDialog.Builder(ExportInspection.this)
						.create();
						alertDialog.setMessage(status);
						alertDialog.setButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								Intent inte;
								 inte = new Intent(getApplicationContext(),ExportInspection.class);
								if(move_to_pre==1)
								{			  
								 
								  inte.putExtra("type", "Reexport");
								}
								else
								{
												 
									  inte.putExtra("type", "export");	
								}
								  startActivity(inte);

							}
						});
				alertDialog.show();
						
					}

					
					
				});
				
				detailbut.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if(tlist.getVisibility()==View.VISIBLE)
						{
							detailbut.setText("Hide failure reports");
							
							tlist.setVisibility(View.GONE);
						}
						else
						{
							detailbut.setText("Show failure reports");
							tlist.setVisibility(View.VISIBLE);
						}
					
				
					}
					
				});
               select.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						
						re_export();
						
							
					}

					
					
				});
               
				dialog1.setCancelable(false);
				dialog1.show();
				
			}
			else
			{
				cf.ShowToast("Sorry. This record has been reallocated to another inspector.", 1);
				
			}
		}
		
		

		
			// TODO Auto-generated method stub
			
		
};
	

	

	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		
	}
	/** PROGRESS DILOG WITH LODING DATA INFORMATION  ENDS
	 * @throws XmlPullParserException 
	 * @throws IOException **/

	public void show_staus() throws IOException, XmlPullParserException ,SocketTimeoutException,Exception{
		// TODO Auto-generated method stub
		SoapObject request = new SoapObject(cf.NAMESPACE,"GetInspectionStatus");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("InspectorID",cf.Insp_id);
		request.addProperty("SRID",cf.selectedhomeid);
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
		androidHttpTransport.call(cf.NAMESPACE+"GetInspectionStatus",envelope);
		System.out.println("property "+request);
		String result =  envelope.getResponse().toString();
		System.out.println("statusresult "+result);
		
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			cf.exprtcnt=cf.fn_CheckExportCount(cf.Insp_id,cf.selectedhomeid);
			//System.out.println("the values "+cf.exprtcnt);
			try
			{
			cf.exprtcnt=cf.exprtcnt.replace("UpdateInspectionStatus", "General Hazards  Status Change");
			cf.exprtcnt=cf.exprtcnt.replace("ExportGenHazardsBuildingInfo", "General Hazards Building Information");
			cf.exprtcnt=cf.exprtcnt.replace("ExportGenCondnAndHazardsInfo", "General Hazards General Information");
			cf.exprtcnt=cf.exprtcnt.replace("ExportGenCondnAndHazardsRoofInfo", "General Hazards Roof Information");
			cf.exprtcnt=cf.exprtcnt.replace("ExportGenHazardsPicture", "General Hazards Picture");
			cf.exprtcnt=cf.exprtcnt.replace("ExportGenHazardFeedbackDocument", "General Hazards Feedbacks");
			}catch (Exception e) {
				// TODO: handle exception
			}
			
			//cf.exprtcnt.contains("")
			show_handler=1;
		} catch (SocketException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			show_handler=2;
			cf.Error_LogFile_Creation(e1.getMessage()+" "+" at "+ ExportInspection.this+" "+" in the stage of retrieving re-export count at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			 
			
		} catch (NetworkErrorException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			show_handler=2;
			cf.Error_LogFile_Creation(e1.getMessage()+" "+" at "+ ExportInspection.this+" "+" in the stage of retrieving re-export count at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			show_handler=2;
			cf.Error_LogFile_Creation(e1.getMessage()+" "+" at "+ ExportInspection.this+" "+" in the stage of retrieving re-export count at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		
		} catch (TimeoutException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			show_handler=2;
			cf.Error_LogFile_Creation(e1.getMessage()+" "+" at "+ ExportInspection.this+" "+" in the stage of retrieving re-export count at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		
		} catch (XmlPullParserException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			show_handler=2;
			cf.Error_LogFile_Creation(e1.getMessage()+" "+" at "+ ExportInspection.this+" "+" in the stage of retrieving re-export count at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			show_handler=2;
			cf.Error_LogFile_Creation(e1.getMessage()+" "+" at "+ ExportInspection.this+" "+" in the stage of retrieving re-export count at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		
		}
		myhandler.sendEmptyMessage(0);
	}
	private Handler myhandler = new Handler() {

		public void handleMessage(Message msg) {
			
			if(show_handler==1)
			{
				show_handler=0;cf.pd.dismiss();
				/*if(cf.exprtcnt==0)
				{*/
					
				/*}
				else if(cf.exprtcnt>0)
				{*/
				if(cf.onlstatus.equals("Reexport"))
				{}else{
					    dialog1.dismiss();}
					    
					    cf.alerttitle="Re-export";
					    cf.alertcontent="This inspection has been exported already."+"<br /><br />"+"<b>Details :</b>"+cf.exprtcnt+
							    "<b>Are you sure want to Re-export again?</b>";
					    final Dialog dialog1 = new Dialog(ExportInspection.this,android.R.style.Theme_Translucent_NoTitleBar);
						dialog1.getWindow().setContentView(R.layout.alertsync);
						TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
						txttitle.setText( cf.alerttitle);
						TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
						txt.setText(Html.fromHtml( cf.alertcontent));
						Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
						Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
						btn_yes.setOnClickListener(new OnClickListener()
						{
		                	@Override
							public void onClick(View arg0) {
								// TODO Auto-generated method stub
								dialog1.dismiss();
								
								//re_export();
								if(cf.onlstatus.equals("Reexport"))
								{
									fn_export();
									
								}
								else{
								 StartExport(cf.selectedhomeid,incre_unit);}
							}

							
						
						});
						btn_cancel.setOnClickListener(new OnClickListener()
						{

							@Override
							public void onClick(View arg0) {
								// TODO Auto-generated method stub
								dialog1.dismiss();
								
							}
							
						});
						dialog1.setCancelable(false);
						dialog1.show();
				//}
				
			}
			else if(show_handler==2)
			{
				show_handler=0;cf.pd.dismiss();
				cf.ShowToast("Please check your network connection and try again later.", 0);
			}
		}
	};
	protected void re_export() {
		// TODO Auto-generated method stub
		
		Export_chk[0]=Export_chk[1]=Export_chk[2]=Export_chk[3]=Export_chk[4]=Export_chk[5]=Export_chk[6]="0";//=Export_chk[6]=Export_chk[7]=Export_chk[8]=Export_chk[9]=Export_chk[0]="0";
			
		if(sta_chk[0].isChecked())
				Export_chk[1]="1";
			else
				Export_chk[1]="0";
			if(sta_chk[1].isChecked())
				Export_chk[2]="1";
			else
				Export_chk[2]="0";
			if(sta_chk[2].isChecked())
				Export_chk[3]="1";
			else
				Export_chk[3]="0";
			if(sta_chk[3].isChecked())
				Export_chk[4]="1";
			else
				Export_chk[4]="0";
			if(sta_chk[4].isChecked())
				Export_chk[5]="1";
			else
				Export_chk[5]="0";
			if(sta_chk[5].isChecked())
				Export_chk[6]="1";
			else
				Export_chk[6]="0";
		
			/*if(sta_chk[5].isChecked())
				Export_chk[6]="1";
			else
				Export_chk[6]="0";
			if(sta_chk[6].isChecked())
				Export_chk[7]="1";
			else
				Export_chk[7]="0";
			if(sta_chk[7].isChecked())
				Export_chk[8]="1";
			else
				Export_chk[8]="0";
			if(sta_chk[8].isChecked())
				Export_chk[9]="1";
			else
				Export_chk[9]="0";*/
			
			
			
		
			
			
			/** get the units for increament for every uploading **/
			int j=0;
		
			for(int i=1;i<Export_chk.length;i++)
			{
				if(Export_chk[i].equals("1"))
					j++;
			}
			try
			{
				incre_unit=80.00/Double.parseDouble(String.valueOf(j));
				
			}
			catch(Exception e)
			{
				incre_unit=10.00;
			}
			//int incre_unit=100/j;
			/** get the units for increament for every uploading Ends **/
			
			/** export will start here **/
			if(j>=1)
			{
				/*String source = "<b><font color=#00FF33>Retrieving data. Please wait..."
						+ "</font></b>";
				cf.pd = ProgressDialog.show(ExportInspection.this, "", Html.fromHtml(source), true);
				Thread thread = new Thread(ExportInspection.this);
				thread.start();*/
				dialog1.setCancelable(true);
				dialog1.dismiss();

				
			StartExport(cf.selectedhomeid,incre_unit);
			}
			else
			{
				cf.ShowToast("Please select atleast one option ", 1);
			}
		
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(ExportInspection.this, HomeScreen.class));
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	private void getNetConnectDetail() {
		// TODO Auto-generated method stub
		try {
			String[] da = cf.gettheWifidata();// get the detail of the data
												// connection and show it in the
												// screen

			if ((!da[0].equals("UNKNOWN") && !da[0].equals(""))) {
				TextView dataconnect = (TextView) findViewById(R.id.connectiondata);
				dataconnect.setVisibility(View.VISIBLE);
				dataconnect.setText("Connection speed : " + da[1] + da[2] + " ");
				if (da[0].equals("WIFI")) {
					TextView signallength = (TextView) findViewById(R.id.signallength);
					signallength.setVisibility(View.VISIBLE);
					signallength.setText(" Signal strength :"+da[3]);
					/*if (cf.slenght <= -80) {
						signallength.setText(" Signal strength : Poor");
					} else if (cf.slenght <= -75) {
						signallength.setText(" Signal strength : Not bad");
					} else if (cf.slenght <= -60) {
						signallength.setText(" Signal strength : Good");
					} else {
						signallength.setText(" Signal strength : Strong");
					}*/
				}
			}
		} catch (Exception e) {

		}

	} 
};


