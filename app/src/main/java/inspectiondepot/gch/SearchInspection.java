package inspectiondepot.gch;


import inspectiondepot.gch.R;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeoutException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;


import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class SearchInspection extends Activity implements Runnable {
	String fstname, lstname, pn, source = "<b><font color=#00FF33>Loading data. Please wait..."
			+ "</font></b>";
	TextView welcome;
	Button homepage, logout, search;
	CommonFunctions cf;
	LinearLayout lst;
	String[] data;
	ScrollView sv;
	String dta = "";
	int Cnt;
	ProgressDialog pd;
	SoapObject result;
	int show_handler;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		cf = new CommonFunctions(this);
		
		Bundle b = getIntent().getExtras();
		if (b != null) {
			fstname = b.getString("first");
			lstname = b.getString("last");
			pn = b.getString("policy");

		}
		setContentView(R.layout.searchinspection);
		/**Used for hide he key board when it clik out side**/

		cf.setupUI((ScrollView) findViewById(R.id.scr));

	/**Used for hide he key board when it clik out side**/
		this.welcome = (TextView) this.findViewById(R.id.welcomename);
		this.homepage = (Button) this.findViewById(R.id.deletehome);
		
		this.search = (Button) this.findViewById(R.id.srch);
		lst = (LinearLayout) findViewById(R.id.linlayoutdyn);
		cf.getInspectorId();
		pd = ProgressDialog.show(SearchInspection.this, "", Html.fromHtml(source), true);
		Thread thread = new Thread(SearchInspection.this);
		thread.start();
		welcomemessage();
		
		this.search.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				final EditText tvxtstname;
				final EditText tvlstname;
				final EditText tvploicy;
				final Dialog dialog = new Dialog(SearchInspection.this);
				dialog.setContentView(R.layout.search);
				dialog.setTitle("This is an online search. Internet connection is required.");
				dialog.setCancelable(true);
				tvxtstname = (EditText) dialog.findViewById(R.id.txtfirstname);
				tvlstname = (EditText) dialog.findViewById(R.id.txtlastname);
				tvploicy = (EditText) dialog.findViewById(R.id.txtpolicy);
				tvxtstname.requestFocusFromTouch();
				Button btnsrch = (Button) dialog
						.findViewById(R.id.search);
				btnsrch.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						cf.hidekeyboard();
						Cnt = 0;
						lst.removeAllViews();
						dta = "";
						if (tvxtstname.getText().toString().equals("")
								&& tvlstname.getText().toString().equals("")
								&& tvploicy.getText().toString().equals("")) {
							cf.ShowToast("Please enter any one of the fields to search.", 1);
									
						} else {
							fstname = tvxtstname.getText().toString();
							lstname = tvlstname.getText().toString();
							pn = tvploicy.getText().toString();
							ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
							NetworkInfo info = conMgr.getActiveNetworkInfo();
							if (info != null && info.isConnected()) {
								pd = ProgressDialog.show(SearchInspection.this, "", Html.fromHtml(source), true);
								try {
									fetchdata();show_handler=1;
								} catch (SocketTimeoutException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();show_handler=0;
								}catch(TimeoutException e)
								{
								show_handler=0;
								} catch(NetworkErrorException e)
								{
								show_handler=0;
								}catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();show_handler=0;
								} catch (XmlPullParserException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();show_handler=0;
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();show_handler=0;
								}
								handler.sendEmptyMessage(0);
								dialog.cancel();
							} else {
								cf.ShowToast("Internet connection is not available.",1);
								
							}
						}

					}

				});
				Button btncls = (Button) dialog.findViewById(R.id.cls);
				btncls.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dialog.dismiss();
					}

				});
				dialog.show();

			}
		});

		this.homepage.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				Intent homepage = new Intent(SearchInspection.this,
						HomeScreen.class);
				startActivity(homepage);

			}
		});
		
	}

	private void fetchdata() throws SocketTimeoutException,IOException,NetworkErrorException,XmlPullParserException,TimeoutException,Exception {
		// TODO Auto-generated method stub
		
				SoapObject request = new SoapObject(cf.NAMESPACE, "SearchPolicyHolderInfo");
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
						SoapEnvelope.VER11);
				envelope.dotNet = true;
				request.addProperty("InspectorID", cf.Insp_id);
				request.addProperty("OwnerFirstName", fstname);
				request.addProperty("OwnerLastName", lstname);
				request.addProperty("OwnerPolicyNumber", pn);
				envelope.setOutputSoapObject(request); 
				HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
				
			 androidHttpTransport.call(cf.NAMESPACE+"SearchPolicyHolderInfo", envelope);
			 result = (SoapObject) envelope.getResponse();
			 if(result==null)
			 {
				 show_handler=2;
			 }else{
			 int cc = result.getPropertyCount();
			if(cc==0)
			{
				show_handler=0;
			}
			else
			{
				show_handler=2;
			}
			 }
		

	}

	private void display() {
		// TODO Auto-generated method stub
		lst.removeAllViews();
		sv = new ScrollView(this);
		lst.addView(sv);

		final LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(l1);

		this.data = dta.split("~");
		for (int i = 0; i < data.length; i++) {
			TextView[] tvstatus = new TextView[Cnt];

			LinearLayout l2 = new LinearLayout(this);
			l2.setOrientation(LinearLayout.HORIZONTAL);
			l1.addView(l2);

			LinearLayout lchkbox = new LinearLayout(this);
			LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
					900, ViewGroup.LayoutParams.WRAP_CONTENT);
			paramschk.topMargin = 8;
			paramschk.leftMargin = 20;
			paramschk.bottomMargin = 10;
			l2.addView(lchkbox);

			tvstatus[i] = new TextView(this);
			tvstatus[i].setMinimumWidth(580);
			tvstatus[i].setMaxWidth(580);
			tvstatus[i].setTag("textbtn" + i);
			tvstatus[i].setText(data[i]);
			tvstatus[i].setTextColor(Color.WHITE);
			tvstatus[i].setTextSize(16);
			lchkbox.addView(tvstatus[i], paramschk);

			LinearLayout ldelbtn = new LinearLayout(this);
			LinearLayout.LayoutParams paramsdelbtn = new LinearLayout.LayoutParams(
					93, 37);
			paramsdelbtn.rightMargin = 10;
			paramsdelbtn.bottomMargin = 10;
			l2.addView(ldelbtn);

		}

	}

	private void welcomemessage() {
		try {
			Cursor c = cf.gch_db
					.rawQuery("SELECT * FROM " + cf.inspectorlogin
							+ " WHERE Fld_InspectorId='" + cf.encode(cf.Insp_id)
							+ "'", null);
			int rws1 = c.getCount();
			c.moveToFirst();
			String data = cf.decode(
					c.getString(c.getColumnIndex("Fld_InspectorFirstName")))
					.toLowerCase();
			data += " ";
			data += cf.decode(
					c.getString(c.getColumnIndex("Fld_InspectorLastName")))
					.toLowerCase();
			welcome.setText(data.toUpperCase());
			c.close();
		} catch (Exception e) {

		}
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(SearchInspection.this,HomeScreen.class));
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try{ fetchdata();show_handler=1;
		} 
		catch(SocketTimeoutException e)
		{
		 show_handler=0;
		}
		catch(TimeoutException e)
		{
		show_handler=0;
		}
		catch(NetworkErrorException e)
		{
		show_handler=0;
		}catch (IOException e) {
				// TODO Auto-generated catch block
			show_handler=0;
				e.printStackTrace(); 
			} catch (XmlPullParserException e) {
				// TODO Auto-generated catch block
				show_handler=0;
				e.printStackTrace();
			}catch (Exception e) {
				// TODO Auto-generated catch block
				show_handler=0;
				e.printStackTrace();
				
			} 
		handler.sendEmptyMessage(0);
	}
	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			pd.dismiss();
			if(show_handler==1){if (result != null) {
				if (result.toString().equals("null")
						|| result.toString().equals("")) {
					cf.ShowToast("Sorry, No record found.", 1);
				} else {
					Cnt = result.getPropertyCount();

					String[] data = new String[Cnt];

					for (int j = 0; j < Cnt; j++) {
						SoapObject result1 = (SoapObject) result.getProperty(j);

						try {

							dta += " "
									+ result1.getProperty("FirstName")
											.toString() + " ";
							dta += result1.getProperty("LastName").toString()
									+ " | ";
							dta += result1.getProperty("PolicyNumber")
									.toString() + " \n ";
							dta += result1.getProperty("Address1").toString()
									+ " | ";
							dta += result1.getProperty("City").toString()
									+ " | ";
							dta += result1.getProperty("State").toString()
									+ " | ";
							dta += result1.getProperty("County").toString()
									+ " | ";
							dta += result1.getProperty("StatusName").toString()
									+ "~";
						} catch (Exception e) {

						}
					}
					display();
				}
			} else {
				cf.ShowToast("Sorry, No record found.", 1);
				}
			}
			else if(show_handler==0)
			{
				cf.ShowToast("There is problem on the network. Please try again later.", 1);
			}
			else if(show_handler==2)
			{
				cf.ShowToast("Sorry, No record found.", 1);
			}
		}
	};
}
