package inspectiondepot.gch;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class AdditionalInfo extends Activity{
	CommonFunctions cf;
	String strhomeid;
	TextView[] additional = new TextView[13];
	public void onCreate(Bundle SavedInstanceState) {
		super.onCreate(SavedInstanceState);
		cf = new CommonFunctions(this);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			cf.selectedhomeid = strhomeid= extras.getString("homeid");
			cf.onlinspectionid = extras.getString("InspectionType");
		    cf.onlstatus = extras.getString("status");
	 	}
		setContentView(R.layout.additional);
		
		cf.getDeviceDimensions();
   	    LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
        mainmenu_layout.setMinimumWidth(cf.wd);
        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 1, 0,cf));
	    LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.generalsubmenu);
	    submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 17, 1,cf));
	    ScrollView scr = (ScrollView)findViewById(R.id.scr);
	    scr.setMinimumHeight(cf.ht);
        
		cf.getInspectorId();
		additional[5] = (TextView) findViewById(R.id.efirstname);
		additional[4] = (TextView) findViewById(R.id.title);
		additional[7] = (TextView) findViewById(R.id.txtaddr1);
		additional[8] = (TextView) findViewById(R.id.txtaddr2);
		additional[9] = (TextView) findViewById(R.id.txtcity);
		additional[11] = (TextView) findViewById(R.id.txtcountry);
		additional[6] = (TextView) findViewById(R.id.econtactperson);
		additional[10] = (TextView) findViewById(R.id.txtstate);
		additional[12] = (TextView) findViewById(R.id.txtzip);
		
		 try {
				Cursor cur = cf.SelectTablefunction(cf.Additional_table, "where SH_AD_SRID='"
						+ cf.selectedhomeid + "'");
				int rws = cur.getCount();
			 cur.moveToFirst();
             if (cur != null) {
					for (int i = 4; i <= 12; i++) {
						if(i==4)
						{
							additional[4].setText("Additional Information"+"["+cur.getString(4)+"]");
						}
						else
						{
							additional[i].setText(cur.getString(i));
						}
						if (cur.getString(i).equals("anyType{}")
								|| cur.getString(i).equals("")) {
							additional[i].setText("N/A");
						}
					}

				}while (cur.moveToNext());
		    }
		    catch(Exception e)
		    {
		    	cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ AdditionalInfo.this +" "+" in the stage of(catch) retreving  at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				
		    }
	}
	 public void clicker(View v) {
			switch (v.getId()) {

			case R.id.home:
						cf.gohome();
			break;

			}
		}
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				Intent intimg = new Intent(getApplicationContext(), CloudInfo.class);
				intimg.putExtra("homeid", cf.selectedhomeid);
				intimg.putExtra("InspectionType", cf.onlinspectionid);
				intimg.putExtra("status", cf.onlstatus);
				startActivity(intimg);
				return true;
			}
			return super.onKeyDown(keyCode, event);
		}

}
