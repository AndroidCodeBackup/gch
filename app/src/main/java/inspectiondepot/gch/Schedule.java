package inspectiondepot.gch;
/***   	Page Information
		Version :1.0
		VCode:1
		Created Date:19/10/2012
		Purpose:For Schedule Inspection
		Created By:Rakki S
		Last Updated:19/10/2012   
		Last Updatedby:Rakki s
***/

import inspectiondepot.gch.R;

import java.io.IOException;

import java.net.SocketTimeoutException;
import java.util.Calendar;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;


import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class Schedule extends Activity{
	private static final int DATE_DIALOG_ID = 0;
	private static final int SELECT_PICTURE = 0;
	CommonFunctions cf;
	public String strhomeid; 
	String isinspected;
	String startspin,endspin;
	Cursor c2;
	int spin1,spin2,verify,SP_ST,SP_ET;
	private ArrayAdapter<CharSequence> mRangeadapter;
	TextView assigndate;
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	       
	        cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.selectedhomeid = strhomeid= extras.getString("homeid");
				cf.onlinspectionid = extras.getString("InspectionType");
			    cf.onlstatus = extras.getString("status");
			    System.out.println("The status of the imag was "+ cf.onlstatus);
         	}
			
			setContentView(R.layout.scheduleinspection);
			/**Used for hide he key board when it clik out side**/

			cf.setupUI((ScrollView) findViewById(R.id.scr));

		/**Used for hide he key board when it clik out side**/
			cf.getDeviceDimensions();
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 1, 0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.generalsubmenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 11, 1,cf));
			ScrollView scr = (ScrollView)findViewById(R.id.scr);
		    scr.setMinimumHeight(cf.ht);
			TextView tv_inspectiondate = (TextView) this.findViewById(R.id.tvinspectiondate);
			tv_inspectiondate.setText(Html.fromHtml(cf.redcolor+" Inspection Date"));
			TextView tv_starttime = (TextView) this.findViewById(R.id.start_time);
			tv_starttime.setText(Html.fromHtml(cf.redcolor+" Inspection Start Time"));
			TextView tv_endtime = (TextView) this.findViewById(R.id.end_time);
			tv_endtime.setText(Html.fromHtml(cf.redcolor+" Inspection End Time"));
			TextView tv_comment = (TextView) this.findViewById(R.id.tvcomment);
			tv_comment.setText(Html.fromHtml(cf.redcolor+" Scheduling Comments"));
			assigndate = (TextView)findViewById(R.id.viewassigndate);
			
			cf.schchk = (CheckBox) findViewById(R.id.resch);
			cf.et_inspdate = (EditText)findViewById(R.id.etinspectiondate);
			cf.get_inspectiondate = (Button)findViewById(R.id.getinspectiondate);
			cf.Sch_Spinnerstart = (Spinner)findViewById(R.id.spinstarttime);
			cf.Sch_Spinnerend = (Spinner)findViewById(R.id.spinendtime);
			Spinner_SetStarttime();
			cf.Sch_Spinnerstart.setOnItemSelectedListener(new MyOnItemSelectedListenerstart());
			
			cf.et_commenttxt = (EditText)findViewById(R.id.etcommenttxt);
			cf.SQ_TV_type2 = (TextView) findViewById(R.id.SH_TV_ED);
			
		 cf.SQ_ED_type2_parrant = (LinearLayout) findViewById(R.id.SH_ED_parrent);
		      cf.SQ_ED_type2 = (LinearLayout) findViewById(R.id.SH_ED1);
		    cf.et_commenttxt.addTextChangedListener(new SH_textwatcher(1));
		   cf.getInspectorId();
			cf.getCalender();
			Spinner_SetEndtime();
			cf.Sch_Spinnerend.setOnItemSelectedListener(new MyOnItemSelectedListenerend());
		if (cf.onlstatus.equals("Assign")) {cf.schchk.setEnabled(false);}
			else
			{
				cf.schchk.setEnabled(true);
			}
			cf.schchk.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {

					// Perform action on clicks, depending on whether it's now
					// checked
				if (((CheckBox) v).isChecked()) {
						cf.et_inspdate.setEnabled(true);
						cf.get_inspectiondate.setEnabled(true);
						cf.Sch_Spinnerstart.setEnabled(true);
						cf.Sch_Spinnerend.setEnabled(true);
						cf.et_commenttxt.setEnabled(true);
					} else if (!cf.et_inspdate.getText().toString().equals("")) {
						cf.et_inspdate.setEnabled(false);
						cf.get_inspectiondate.setEnabled(false);
						cf.Sch_Spinnerstart.setEnabled(false);
						cf.Sch_Spinnerend.setEnabled(false);
						cf.et_commenttxt.setEnabled(false);
					}

				}

			});
			cf.get_inspectiondate.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					showDialog(DATE_DIALOG_ID);
					
				}
			});
			
			try {
				c2 = cf.gch_db.rawQuery("SELECT * FROM "
						+ cf.policyholder + " WHERE GCH_PH_SRID='" + cf.encode(cf.selectedhomeid)
						+ "' and GCH_PH_InspectorId='" + cf.encode(cf.Insp_id) + "'", null);
				int rws = c2.getCount();
				c2.moveToFirst();
				if (c2 != null) {
						cf.et_inspdate.setText(cf.decode(c2.getString(c2.getColumnIndex("GCH_Schedule_ScheduledDate"))));
						cf.Sh_assignedDate=cf.decode(c2.getString(c2.getColumnIndex("GCH_Schedule_AssignedDate")));
						SP_ST = mRangeadapter.getPosition(cf.decode(c2.getString(c2.getColumnIndex("GCH_Schedule_InspectionStartTime"))));
						cf.Sch_Spinnerstart.setSelection(SP_ST);
         			    SP_ET = mRangeadapter.getPosition(cf.decode(c2.getString(c2.getColumnIndex("GCH_Schedule_InspectionEndTime"))));
						cf.Sch_Spinnerend.setSelection(SP_ET);
						cf.et_commenttxt.setText(cf.decode(c2.getString(c2.getColumnIndex("GCH_Schedule_Comments"))));
						assigndate.setText(cf.decode(c2.getString(c2.getColumnIndex("GCH_Schedule_AssignedDate"))));

						if (!cf.decode(c2.getString(c2.getColumnIndex("GCH_Schedule_ScheduledDate"))).equals("") && !cf.onlstatus.equals("Assign"))
						{
							cf.et_inspdate.setEnabled(false);
							cf.get_inspectiondate.setEnabled(false);
							cf.Sch_Spinnerstart.setEnabled(false);
							cf.Sch_Spinnerend.setEnabled(false);
							cf.et_commenttxt.setEnabled(false);

						} else {
							cf.et_inspdate.setEnabled(true);
							cf.get_inspectiondate.setEnabled(true);
							cf.Sch_Spinnerstart.setEnabled(true);
							cf.Sch_Spinnerend.setEnabled(true);
							cf.et_commenttxt.setEnabled(true);
						}
				}

			} catch (Exception e) {
				cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ Schedule.this +" "+" in the processing stage of retrieving data for schedule from PH table  at "+" "+ cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				
			}
			
		
	 } 
	 class SH_textwatcher implements TextWatcher
		{
	         public int type;
	        
	         SH_textwatcher(int type)
			{
				this.type=type; 
			}
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				cf.showing_limit(s.toString(),cf.SQ_ED_type2_parrant,cf.SQ_ED_type2,cf.SQ_TV_type2,"499");
			 
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				
			}
			
		}
		private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

			public void onDateSet(DatePicker view, int year, int monthOfYear,
					int dayOfMonth) {
				cf.mYear = year;
				cf.mMonth = monthOfYear;
				cf.mDay = dayOfMonth;
				cf.et_inspdate.setText(new StringBuilder()
						// Month is 0 based so add 1
						.append(cf.mMonth + 1).append("/").append(cf.mDay).append("/")
						.append(cf.mYear).append(" "));
				
			}
		};

		@Override
		protected Dialog onCreateDialog(int id) {
			switch (id) {
			case DATE_DIALOG_ID:
				return new DatePickerDialog(this, mDateSetListener, cf.mYear, cf.mMonth,
						cf.mDay);
			}
			return null;
		}
		public void clicker(View v) {
			switch (v.getId()) {

			  case R.id.home:
						  cf.gohome();
						  break;
			  case R.id.schedulesave:
				if(!cf.onlstatus.trim().equals("CIT"))
				{
				if (!"".equals(cf.et_inspdate.getText().toString())) {
					  if (Validation_TodayDate(cf.et_inspdate.getText().toString()) == "true") {
						  if (Validation_Assigndate(cf.Sh_assignedDate, cf.et_inspdate.getText().toString()) == "true") {
							  if (!"Select".equals(cf.Sch_Spinnerstart.getSelectedItem().toString())) {
								  if (!"Select".equals(cf.Sch_Spinnerend.getSelectedItem().toString())) {
									  if (spinvalidation(spin1, spin2) == "true") {
										  //||  cf.schchk.isChecked())
										  if (cf.et_commenttxt.getText().toString().trim().equals("") || !cf.et_commenttxt.isEnabled()) {
											  if(cf.et_commenttxt.getText().toString().trim().equals(""))
											  {
												cf.ShowToast("Please enter the Scheduling Comments.", 1);
												 cf.et_commenttxt.requestFocus();
											  }
											  else if(!cf.et_commenttxt.isEnabled())
											  {
													cf.ShowToast("Please check the reschedule option ", 1);
											  }
										  	   	
											}
											else
											{
												
												try
												{
												Cursor c12 = cf.gch_db.rawQuery("SELECT * FROM "
														+ cf.policyholder + " WHERE GCH_PH_SRID='" + cf.encode(cf.selectedhomeid)
														+ "' and GCH_PH_InspectorId='" + cf.encode(cf.Insp_id) + "'", null);
												c12.moveToFirst();
												 isinspected = c12.getString(c12.getColumnIndex("GCH_PH_IsInspected"));
												}
												catch(Exception e)
												{
													System.out.println("isinpecte getting "+e.getMessage());
												}
												
											if(isinspected.equals("1"))
											{ 
												cf.ShowToast("You cannot schedule this record. This inspection has been moved to CIT status.", 1);
											}
											else
											{
												
												
												if (cf.isInternetOn()==true) {
													cf.pd = ProgressDialog
															.show(Schedule.this,
																	"", "Scheduling...");
													new Thread() {
														public void run() {
															try {
																cf.Chk_Inspector=cf.fn_CurrentInspector(cf.Insp_id,cf.selectedhomeid);
																if(cf.Chk_Inspector.equals("true"))
																{  
																
																  if (sendschedule() == "true") {
																	cf.gch_db.execSQL("UPDATE "
																				+ cf.policyholder
																				+ " SET GCH_PH_Status='40',GCH_Schedule_ScheduledDate='"
																				+ cf.encode(cf.et_inspdate.getText().toString())
																				+ "',"
																				+ "GCH_Schedule_Comments='"
																				+ cf.encode(cf.et_commenttxt.getText().toString())
																				+ "',GCH_Schedule_InspectionStartTime='"
																				+ cf.encode(cf.Sch_Spinnerstart.getSelectedItem().toString())
																				+ "',"
																				+ "GCH_Schedule_InspectionEndTime='"
																				+ cf.encode(cf.Sch_Spinnerend.getSelectedItem().toString())
																				+ "',GCH_ScheduleFlag='1' WHERE GCH_PH_InspectorId ='"
																				+ cf.encode(cf.Insp_id)
																				+ "' and GCH_PH_SRID='"
																				+ cf.encode(cf.selectedhomeid)
																				+ "'");
																	verify = 0;
																	handler.sendEmptyMessage(0);
																	cf.pd.dismiss();
																	if (cf.onlstatus
																			.equals("Assign")) {
																		
																		startActivity(new Intent(
																				Schedule.this,Dashboard.class));
																	}
																}
																else
																{
																	showerror();
																}
																}
																else
																{
																	verify = 2;
																	handler.sendEmptyMessage(0);
																	cf.pd.dismiss();
																}
															} catch (SocketTimeoutException s) {
																verify = 5;
																handler.sendEmptyMessage(0);
																cf.pd.dismiss();
															} catch (NetworkErrorException n) {
																verify = 5;
																handler.sendEmptyMessage(0);
																cf.pd.dismiss();
															} catch (IOException io) {
																verify = 5;
																handler.sendEmptyMessage(0);
																cf.pd.dismiss();
															} catch (XmlPullParserException x) {
																verify = 5;
																handler.sendEmptyMessage(0);
																cf.pd.dismiss();
															} catch (Exception e) {
																verify = 5;
																handler.sendEmptyMessage(0);
																cf.pd.dismiss();
															}
														}

														private void showerror() {
															verify = 1;
															handler.sendEmptyMessage(0);
															cf.pd.dismiss();
														}

														private Handler handler = new Handler() {
															@Override
															public void handleMessage(
																	Message msg) {
																if (verify == 0) {
																	if(cf.schchk.isChecked()){
																	cf.ShowToast("Your inspection has been successfully re-scheduled.", 1);
																	}
																	else
																	{
																		cf.ShowToast("Your inspection has been successfully scheduled.", 1);
																	}
																} else if (verify == 1) {
																	cf.ShowToast("Your inspection has not been scheduled.", 1);
																} else if (verify == 5) {
																	cf.ShowToast("Please check your network connection and try again.", 1);
																}
																else if(verify == 2)
																{
																	cf.ShowToast("Sorry. This record has been reallocated to another inspector.", 1);
																}
															}
														};
													}.start();
													
												} else {
													cf.ShowToast("Internet connection is not available.", 1);
													cf.gch_db.execSQL("UPDATE "
															+ cf.policyholder
															+ " SET GCH_PH_Status='40',GCH_Schedule_ScheduledDate='"
															+ cf.encode(cf.et_inspdate.getText().toString())
															+ "',"
															+ "GCH_Schedule_Comments='"
															+ cf.encode(cf.et_commenttxt.getText().toString())
															+ "',GCH_Schedule_InspectionStartTime='"
															+ cf.encode(cf.Sch_Spinnerstart.getSelectedItem().toString())
															+ "',"
															+ "GCH_Schedule_InspectionEndTime='"
															+ cf.encode(cf.Sch_Spinnerend.getSelectedItem().toString())
															+ "',GCH_ScheduleFlag='0' WHERE GCH_PH_InspectorId ='"
															+ cf.encode(cf.Insp_id)
															+ "' and GCH_PH_SRID='"
															+ cf.encode(cf.selectedhomeid) + "'");
													if (cf.onlstatus.equals("Assign")) {
														startActivity(new Intent(
																Schedule.this,Dashboard.class));

													}
												}
											}
											}
										} else {
											 cf.ShowToast("End Time should be greater than Start Time.", 1);
									      }
								  } else {
									   cf.ShowToast("Please select End Time.", 1);
									   cf.Sch_Spinnerend.requestFocus();
									}
							  }
							  else {
								  cf.ShowToast("Please select Start Time.", 1);
								  cf.Sch_Spinnerstart.requestFocus();
								}
						  }
						  else {
							  cf.ShowToast("Schedule Date should be greater than or equal to Assigned Date.", 1);
							  cf.et_inspdate.setText("");
							  cf.et_inspdate.requestFocus();
							}
					  }
					  else {
						    cf.ShowToast("Schedule Date should be greater than or equal to today date.", 1);
						    cf.et_inspdate.setText("");
						    cf.et_inspdate.requestFocus();
						}
				  }
				  else {
					  cf.ShowToast("Please enter the Inspection Date.", 1);
					  cf.et_inspdate.requestFocus();
					}
				}
				else
				{
					cf.ShowToast("This record in Completed in Online Status so you can not schedule .", 1);
					  cf.et_inspdate.requestFocus();
				}
				  break;
				
			}
		}
	 private String sendschedule() throws NetworkErrorException, IOException,
		SocketTimeoutException, XmlPullParserException {
			// TODO Auto-generated method stub
		    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.dotNet = true;
			SoapObject ad_property = new SoapObject(cf.NAMESPACE, "ExportScheduleInfo");

			ad_property.addProperty("Srid", cf.selectedhomeid);
			ad_property.addProperty("InspectorID", cf.Insp_id);
			ad_property.addProperty("ScheduleDate", cf.et_inspdate.getText().toString());
			int spinnerstatPosition = mRangeadapter.getPosition(startspin);
			ad_property.addProperty("ScheduleStartTime", spinnerstatPosition);
			int spinnerendPosition = mRangeadapter.getPosition(endspin);
			ad_property.addProperty("ScheduleEndTime", spinnerendPosition);
			ad_property.addProperty("Comments", cf.et_commenttxt.getText().toString());
			envelope.setOutputSoapObject(ad_property);
			System.out.println("the property "+ad_property);
			HttpTransportSE androidHttpTransport1 = new HttpTransportSE(cf.URL);

			androidHttpTransport1.call(cf.NAMESPACE+"ExportScheduleInfo", envelope);
			Object response = envelope.getResponse();
			if (response == null || response.toString().equals("")
					|| response.toString().equals("null")) {
				verify = 1;
			    return "false";
			} else if (response.toString().equals("true")) {
				verify = 0;
				return "true";
			} else {
				verify = 1;
				return "false";
			}
		}
	private String spinvalidation(int spin12, int spin22) {
			// TODO Auto-generated method stub
			if (spin22 > spin12) {
				return "true";
			} else {
				return "false";
			}
		
		}
	private String Validation_Assigndate(String sh_assignedDate,
				String string) {
			// TODO Auto-generated method stub
		 if (!sh_assignedDate.trim().equals("")
					|| sh_assignedDate.equals("N/A")
					|| sh_assignedDate.equals("Not Available")
					|| sh_assignedDate.equals("anytype")
					|| sh_assignedDate.equals("Null")) {
				String chkdate = null;
				int i1 = sh_assignedDate.indexOf("/");
				String result = sh_assignedDate.substring(0, i1);
				int i2 = sh_assignedDate.lastIndexOf("/");
				String result1 = sh_assignedDate.substring(i1 + 1, i2);
				String result2 = sh_assignedDate.substring(i2 + 1);
				result2 = result2.trim();
				int j1 = Integer.parseInt(result);
				int j2 = Integer.parseInt(result1);
				int j = Integer.parseInt(result2);

				int i3 = string.indexOf("/");
				String result3 = string.substring(0, i3);
				int i4 = string.lastIndexOf("/");
				String result4 = string.substring(i3 + 1, i4);
				String result5 = string.substring(i4 + 1);
				result5 = result5.trim();
				int k1 = Integer.parseInt(result3);
				int k2 = Integer.parseInt(result4);
				int k = Integer.parseInt(result5);
				if (j > k) {
					chkdate = "false";
				} else if (j < k) {
					chkdate = "true";
				} else if (j == k) {

					if (j1 > k1) {

						chkdate = "false";
					} else if (j1 < k1) {
						chkdate = "true";
					} else if (j1 == k1) {
						if (j2 > k2) {
							chkdate = "false";
						} else if (j2 < k2) {
							chkdate = "true";
						} else if (j2 == k2) {

							chkdate = "true";
						}

					}
		}

				return chkdate;
			} else {
				return "true";
			}
		}
	private String Validation_TodayDate(String string) {
			// TODO Auto-generated method stub
		  	int i1 = string.indexOf("/");
			String result = string.substring(0, i1);
			int i2 = string.lastIndexOf("/");
			String result1 = string.substring(i1 + 1, i2);
			String result2 = string.substring(i2 + 1);
			result2 = result2.trim();
			int j1 = Integer.parseInt(result);
			int j2 = Integer.parseInt(result1);
			int j = Integer.parseInt(result2);
			final Calendar c = Calendar.getInstance();
			int thsyr = c.get(Calendar.YEAR);
			int curmnth = c.get(Calendar.MONTH);
			int curdate = c.get(Calendar.DAY_OF_MONTH);
			int day = c.get(Calendar.DAY_OF_WEEK);
			curmnth = curmnth + 1;

			if (j > thsyr || (j1 > curmnth && j >= thsyr)
					|| (j2 >= curdate && j1 >= curmnth && j >= thsyr)) {
				return "true";
			} else {
				return "false";
			}

		}
	public class MyOnItemSelectedListenerstart implements
		OnItemSelectedListener {

		   	public void onItemSelected(AdapterView<?> parent, View view, int pos,
					long id) {
				startspin = parent.getItemAtPosition(pos).toString();
				spin1 = parent.getSelectedItemPosition();
		    }
		
			public void onNothingSelected(AdapterView parent) {
				// Do nothing.
			}
     }

   public class MyOnItemSelectedListenerend implements OnItemSelectedListener {

	     public void onItemSelected(AdapterView<?> parent, View view, int pos,
			long id) {
			endspin = parent.getItemAtPosition(pos).toString();
			spin2 = parent.getSelectedItemPosition();
        }
	
		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}
	private void Spinner_SetEndtime() {
		// TODO Auto-generated method stub
		mRangeadapter = new ArrayAdapter<CharSequence>(this,
				android.R.layout.simple_spinner_item);
		mRangeadapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		cf.Sch_Spinnerend.setAdapter(mRangeadapter);
		mRangeadapter.add("Select");
		mRangeadapter.add("8:00 AM");
		mRangeadapter.add("8:30 AM");
		mRangeadapter.add("9:00 AM");
		mRangeadapter.add("9:30 AM");
		mRangeadapter.add("10:00 AM");
		mRangeadapter.add("10:30 AM");
		mRangeadapter.add("11:00 AM");
		mRangeadapter.add("11:30 AM");
		mRangeadapter.add("12:00 PM");
		mRangeadapter.add("12:30 PM");
		mRangeadapter.add("1:00 PM");
		mRangeadapter.add("1:30 PM");
		mRangeadapter.add("2:00 PM");
		mRangeadapter.add("2:30 PM");
		mRangeadapter.add("3:00 PM");
		mRangeadapter.add("3:30 PM");
		mRangeadapter.add("4:00 PM");
		mRangeadapter.add("4:30 PM");
		mRangeadapter.add("5:00 PM");
		mRangeadapter.add("5:30 PM");
		mRangeadapter.add("6:00 PM");
		mRangeadapter.add("6:30 PM");
		mRangeadapter.add("7:00 PM");
	}
	private void Spinner_SetStarttime() {
		// TODO Auto-generated method stub
		mRangeadapter = new ArrayAdapter<CharSequence>(this,
				android.R.layout.simple_spinner_item);
		mRangeadapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		cf.Sch_Spinnerstart.setAdapter(mRangeadapter);
		mRangeadapter.add("Select");
		mRangeadapter.add("8:00 AM");
		mRangeadapter.add("8:30 AM");
		mRangeadapter.add("9:00 AM");
		mRangeadapter.add("9:30 AM");
		mRangeadapter.add("10:00 AM");
		mRangeadapter.add("10:30 AM");
		mRangeadapter.add("11:00 AM");
		mRangeadapter.add("11:30 AM");
		mRangeadapter.add("12:00 PM");
		mRangeadapter.add("12:30 PM");
		mRangeadapter.add("1:00 PM");
		mRangeadapter.add("1:30 PM");
		mRangeadapter.add("2:00 PM");
		mRangeadapter.add("2:30 PM");
		mRangeadapter.add("3:00 PM");
		mRangeadapter.add("3:30 PM");
		mRangeadapter.add("4:00 PM");
		mRangeadapter.add("4:30 PM");
		mRangeadapter.add("5:00 PM");
		mRangeadapter.add("5:30 PM");
		mRangeadapter.add("6:00 PM");
		mRangeadapter.add("6:30 PM");
		mRangeadapter.add("7:00 PM");
	}
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
			// replaces the default 'Back' button action
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				Intent  myintent = new Intent(Schedule.this,Callattempt.class);
				myintent.putExtra("InspectionType", cf.onlinspectionid);
				myintent.putExtra("status", cf.onlstatus);
				myintent.putExtra("homeid", cf.selectedhomeid);
				startActivity(myintent);
				return true;
			}
			if (keyCode == KeyEvent.KEYCODE_MENU) {

			}
			return super.onKeyDown(keyCode, event);
		}
	 protected void onActivityResult(int requestCode, int resultCode, Intent data) {
				switch (resultCode) {
	 			case 0:
	 				break;
	 			case -1:
	 				try {

	 					String[] projection = { MediaStore.Images.Media.DATA };
	 					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
	 							null, null, null);
	 					int column_index_data = cursor
	 							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
	 					cursor.moveToFirst();
	 					String capturedImageFilePath = cursor.getString(column_index_data);
	 					cf.showselectedimage(capturedImageFilePath);
	 				} catch (Exception e) {
	 					
	 				}
	 				
	 				break;

	 		}

	 	}
	
}
