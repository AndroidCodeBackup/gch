package inspectiondepot.gch;
 
import java.util.ArrayList;
 
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;
 
import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;
 
public class AddItemizedOverlay1 extends ItemizedOverlay<OverlayItem> {
 
       private ArrayList<OverlayItem> mapOverlays1 = new ArrayList<OverlayItem>();
 
       private Context context;
 
       public AddItemizedOverlay1(Drawable defaultMarker) {
            super(boundCenterBottom(defaultMarker));
       }
 
       public AddItemizedOverlay1(Drawable defaultMarker, Context context) {
            this(defaultMarker);
            this.context = context;
       }
 
       @Override
       protected OverlayItem createItem(int i) {
          return mapOverlays1.get(i);
       }
 
       @Override
       public int size() {
          return mapOverlays1.size();
       }
 
       @Override
       protected boolean onTap(int index) {
          OverlayItem item = mapOverlays1.get(index);

          AlertDialog.Builder dialog = new AlertDialog.Builder(context);
          dialog.setTitle(item.getTitle());
          dialog.setMessage(item.getSnippet());
          dialog.show();
          return true;

       }
      
 
       public void addOverlay(OverlayItem overlay) {
          mapOverlays1.add(overlay);
           this.populate();
       }
 
    }