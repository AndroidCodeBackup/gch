package inspectiondepot.gch;

import inspectiondepot.gch.ExportInspection.ProgressThread;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.kobjects.base64.Base64;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils.TruncateAt;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class HomeScreen extends Activity{
	CommonFunctions cf;
	String formattedDate;
	Thread myThread = null;
	public ProgressThread progThread;
	ProgressDialog progressDialog,progressDialog1,progressDialog2;
	int vcode;
	int k = 0,myhandler;
	ImageView img_update,check_new_insp;
	private boolean active = true;
	private AlertDialog alertDialog;
	RelativeLayout rl;
	ImageView exp,min;
	ScaleAnimation intial;
	AnimationSet animSetpar;
	ImageView inspector_photo;
	TextView f_name,l_name,address,email,cmp;
	public ImageView app_main_menu,backtomainmenu;
	 @Override
    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf = new CommonFunctions(this);
	        setContentView(R.layout.home);
	        
	        cf.getInspectorId();
	      
	        cf.releasecode = (TextView)findViewById(R.id.releasecode);
	        cf.releasecode.setText(cf.apkrc);
	        cf.getInspectorId();
	        TextView welcom = (TextView) findViewById(R.id.welcomename);
	        f_name=(TextView) findViewById(R.id.insp_frst_name);
	        l_name=(TextView) findViewById(R.id.insp_last_name);
	        address=(TextView) findViewById(R.id.insp_address);
	        email=(TextView) findViewById(R.id.insp_email);
	        cmp=(TextView) findViewById(R.id.insp_cmp);
	        
	        f_name.setText(cf.Insp_firstname.toUpperCase());
	        l_name.setText(cf.Insp_lastname.toUpperCase());
	        address.setText(cf.Insp_address.toUpperCase());
	        email.setText(cf.Insp_email.toLowerCase());
	        cmp.setText(cf.Insp_companyname.toUpperCase());
	        welcom.setText(cf.Insp_firstname.toUpperCase()+" "+cf.Insp_lastname.toUpperCase());
	        
	        cf.img_InspectorPhoto = (ImageView) findViewById(R.id.inspectorphoto);
	        inspector_photo = (ImageView) findViewById(R.id.inspectorphoto1);
	      
	               
	        cf.txtversion = (TextView) this.findViewById(R.id.versionname);
	        displayinspectorphoto();
		    cf.versionname=cf.getdeviceversionname();
	        cf.txtversion.setText("Version "+cf.versionname);
	        vcode = getcurrentversioncode(); 
	        rl=(RelativeLayout) findViewById(R.id.animating);
			 rl.setVisibility(View.INVISIBLE);  
			 System.out.println("appl "+cf.application_sta);
	        if(cf.application_sta)
	        {
	        	
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				try {
					String value = extras.get("upgrade").toString();
					final String newversion1 = extras.get("newversion").toString();
					cf.newversion=newversion1;
					cf.newcode= extras.get("versioncode").toString();
					if (value.equals("true")) {

						alertDialog = new AlertDialog.Builder(HomeScreen.this)
								.create();
						alertDialog
								.setMessage("There is a New Version ( "+newversion1 +" ) of General Conditions and Hazards available. Update now to get the Latest Features and Improvements. ");
						alertDialog.setButton(Dialog.BUTTON_POSITIVE,"OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										try {
											if (alertDialog.isShowing()) {

												alertDialog.cancel();

												alertDialog.dismiss();

											}
											progressDialog1 = ProgressDialog
													.show(HomeScreen.this,
															"",
															"Please wait  Upgrading Version  "
																	+ newversion1
																	+ " of General Conditions and Hazards will take few minutes ...");
											new Thread() {
												public void run() {
													 Looper.prepare();
													try {

														Update();

													} catch (SocketTimeoutException s) {
														k = 5;
														handler.sendEmptyMessage(0);
													} catch (NetworkErrorException n) {
														k = 5;
														handler.sendEmptyMessage(0);

													} catch (IOException io) {
														k = 5;
														handler.sendEmptyMessage(0);

													} catch (XmlPullParserException x) {
														k = 5;
														handler.sendEmptyMessage(0);

													} catch (Exception e) {
														k = 8;
														handler.sendEmptyMessage(0);
													}
												};

											

												private Handler handler = new Handler() {
													public void handleMessage(
															Message msg) {
														progressDialog1.dismiss();

														if (k == 2) {
															cf.ShowToast("Internet connection is not available.",1);

														} else if (k == 8) {cf.ShowToast("Error in login please try again latter .",1);

														} else if (k == 5) {
															cf.ShowToast("There is a problem on your Network. Please try again later with better Network..",1);

														}

													}
												};
											}.start();

										} catch (Exception e) {
										
										}
									}
								});
						alertDialog.setButton(Dialog.BUTTON_NEGATIVE,"Cancel", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								
							}
						}); 
							
						alertDialog.show();
					}

				} catch (Exception e) {
					
				}
			}
			img_update=(ImageView) findViewById(R.id.img_update);
			check_new_insp=(ImageView) findViewById(R.id.check_new_insp);
	        Calendar c = Calendar.getInstance();
			SimpleDateFormat df2 = new SimpleDateFormat("MM-dd-yyyy");
			formattedDate = df2.format(c.getTime());
			
	        Runnable runnable = new CountDownRunner();
			myThread = new Thread(runnable);
			myThread.start();
		//  app_main_menu= (ImageView) findViewById(R.id.app_main_menu);
		  backtomainmenu=(ImageView) findViewById(R.id.backtomainmenu);
	        if(cf.application_sta)
	        {
	        	//app_main_menu.setVisibility(View.VISIBLE);
	        	backtomainmenu.setVisibility(View.VISIBLE);
	        }	
	        else
	        {
	        	//app_main_menu.setVisibility(View.GONE);
	        	backtomainmenu.setVisibility(View.GONE);
	        }
	        Animating_button();
			
	      }
	        
	        else
	        {
	        	AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(HomeScreen.this);
					alertDialog1
				.setMessage("You must have IDMA application in order to use General Conditions and Hazards Inspection."
						+ "Please install it."
						+ ".\n You must download the application.");
				alertDialog1.setCancelable(false);
	          	alertDialog1.setPositiveButton("Ok",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,
							int which) {
							progressDialog2 = ProgressDialog.show(HomeScreen.this, "",
								"Downloading. Please wait...");
						new Thread() {
							private int usercheck = 0;

							public void run() {
								Looper.prepare();
								SoapObject webresult;
								SoapObject request = new SoapObject(cf.NAMESPACE, "GeAPKFile_IDMS");
								
								SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
										SoapEnvelope.VER11);
								envelope.dotNet = true;
								request.addProperty("InspectorID",cf.Insp_id);
								envelope.setOutputSoapObject(request);
								      
								HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL_IDMA);

								try {
									androidHttpTransport.call(cf.NAMESPACE+"GeAPKFile_IDMS", envelope);
									Object response = envelope.getResponse();
									byte[] b;
									if (cf.checkresponce(response)) // check the response is valid or
																	// not
									{
										b = Base64.decode(response.toString());

										try {
											String PATH = Environment.getExternalStorageDirectory()
													+ "/Download/IDMA";
											File file = new File(PATH);
											file.mkdirs();

											File outputFile = new File(PATH + "/IDMA.apk");
											FileOutputStream fileOuputStream = new FileOutputStream(
													outputFile);
											fileOuputStream.write(b);
											fileOuputStream.close();

											cf.fn_logout(cf.Insp_id);  //FOR LOGOUT
											
											myhandler=2;
											//progressDialog1.dismiss();
											
										} catch (IOException e) {
											System.out.println("catch");
											myhandler=1;
										}
									}
									else {
										//progressDialog1.dismiss();
										myhandler=0;
										}
								} catch (IOException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
									myhandler=0;
								} catch (XmlPullParserException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
									myhandler=0;
								}
								handler2.sendEmptyMessage(0);
							};
							private Handler handler2 = new Handler() {
								

								@Override
								public void handleMessage(Message msg) {
									
									progressDialog2.dismiss();
									if(myhandler==0)
									{
										cf.ShowToast("There is a problem on your Network.\n Please try again later with better Network.",1);
										
									}
									else if(myhandler==1)
									{
										cf.ShowToast("Update error!",1);
										
									}
									else if(myhandler==2)
									{
										Intent intent = new Intent(Intent.ACTION_VIEW);
										intent.setDataAndType(Uri.fromFile(new File(Environment
												.getExternalStorageDirectory()
												+ "/Download/IDMA/"
												+ "IDMA.apk")),
												"application/vnd.android.package-archive");
										startActivity(intent);
									}
								}
							};}.start();
					
	
					}
				});
	       
	          	alertDialog1.show();
	        }
			
			
	 }
	 public void Animating_button() {
		// TODO Auto-generated method stub
		/** Start animating for the  Check for updates**/
		 cf.Create_Table(2);
			Cursor c12 = cf.SelectTablefunction(cf.Version, " ");   
			c12.moveToFirst();
			System.out.println("there is no more issues"+c12.getCount());
			if (c12.getCount() >= 1) {
				System.out.println("there is no more issues cones in the count db er"+c12.getString(1)+" app ver "+vcode);
				
				if (!c12.getString(1).trim().equals(String.valueOf(vcode).trim())) {
					try
					{
						
						int v_c=Integer.parseInt(c12.getString(1).trim());
						System.out.println("v_c "+v_c + " vcode" +vcode);
						if(v_c>vcode)
						{
							System.out.println("inside andima");
							
							//animatebutton(); // use the function for change the image for a
							final ScaleAnimation zoom = new ScaleAnimation((float)1.00,(float)0.90, (float)1.00,(float)0.90,(float) 100,(float) 20); 
							final AnimationSet animSet = new AnimationSet(false);
							zoom.setRepeatMode(Animation.REVERSE);
							zoom.setRepeatCount(Animation.INFINITE);
							animSet.addAnimation(zoom);
							animSet.setDuration(300);
							AlphaAnimation alpha = new AlphaAnimation(1, (float)0.9);
					        alpha.setDuration(300); // Make animation instant
					        alpha.setRepeatMode(Animation.REVERSE);
					        alpha.setRepeatCount(Animation.INFINITE);
					        animSet.addAnimation(alpha);
					        
							img_update.setAnimation(animSet);
							animSet.start();
						}
					}
					catch(Exception e)
					{
						
					}
				
				} 
			}
			/** End animating for the  Check foru pdates**/
			/** Start animating for the  Check for new inspection starts **/
			 cf.Create_Table(20);
				Cursor c20 = cf.SelectTablefunction(cf.IDMAVersion, " ");   
				c20.moveToFirst();
				if(c20.getCount()>=1)
				{
				int old_v_c=0,new_v_c=0;
				for(int i=0;i<c20.getCount();i++)
				{/**New for the Version code in the applicaation **/
					 if(c20.getString(c20.getColumnIndex("ID_VersionType")).equals("IDMA_Old")) 
					 {
						 old_v_c=c20.getInt(c20.getColumnIndex("ID_VersionCode")); 
						 
					 } /**Old  for the Version code in the application which in the server to update**/
					 else if(c20.getString(c20.getColumnIndex("ID_VersionType")).equals("IDMA_New"))
					 {
						 new_v_c=c20.getInt(c20.getColumnIndex("ID_VersionCode")); 
					 }
					
					 c20.moveToNext();
			    }
				
					if (old_v_c<new_v_c || old_v_c==0) {
						try
						{
							
							
								//animatebutton(); // use the function for change the image for a
								final ScaleAnimation zoom1 = new ScaleAnimation((float)1.00,(float)0.90, (float)1.00,(float)0.90,(float) 100,(float) 20); 
								final AnimationSet animSet1 = new AnimationSet(false);
								zoom1.setRepeatMode(Animation.REVERSE);
								zoom1.setRepeatCount(Animation.INFINITE);
								animSet1.addAnimation(zoom1);
								animSet1.setDuration(300);
								AlphaAnimation alpha1 = new AlphaAnimation(1, (float)0.9);
						        alpha1.setDuration(300); // Make animation instant
						        alpha1.setRepeatMode(Animation.REVERSE);
						        alpha1.setRepeatCount(Animation.INFINITE);
						        animSet1.addAnimation(alpha1);
						        
								check_new_insp.setAnimation(animSet1);
								animSet1.start();
								System.out.println("No more issues ended");
							
						}
						catch(Exception e)
						{
							
						}
					
				 
				}
			}
				else
				{
					check_new_insp.setVisibility(View.INVISIBLE);
				}
			/** Ends animating for the  Check for new inspection starts **/
			/** Start animating for the  Inspector information  **/
				
				
			
			min=(ImageView)	findViewById(R.id.minimize);
		 rl.setVisibility(View.VISIBLE);
			 TranslateAnimation slide = new TranslateAnimation(0, 0, 0,-1300 );   
			 slide.setDuration(0);   
			 slide.setFillAfter(true);   
			 rl.startAnimation(slide); 
			 exp=(ImageView) findViewById(R.id.expand);
			 exp.setVisibility(View.VISIBLE);
			 ImageView log_expand=(ImageView) findViewById(R.id.ImageView01);
			 exp.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						 rl.setVisibility(View.VISIBLE);  
						 TranslateAnimation slide = new TranslateAnimation(0, 0, -(rl.getHeight()+50),0 );   
						 slide.setDuration(1500);   
						 slide.setFillAfter(true);   
						 rl.startAnimation(slide); 
						 exp.setVisibility(View.INVISIBLE);
						 min.setVisibility(View.VISIBLE);
					}
				});
			 log_expand.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(exp.isShown())
					{
					 rl.setVisibility(View.VISIBLE);  
					 TranslateAnimation slide = new TranslateAnimation(0, 0, -(rl.getHeight()+50),0 );   
					 slide.setDuration(1500);   
					 slide.setFillAfter(true);   
					 rl.startAnimation(slide); 
					 exp.setVisibility(View.INVISIBLE);
					 min.setVisibility(View.VISIBLE);
					}
				}
			});
			 /** Ends animating for the  Inspector information  **/	 
	}
	public void Clicker(View v)
	 {
		  switch(v.getId())
		  {
		  case R.id.img_startinspection:
			  startActivity(new Intent(HomeScreen.this,Dashboard.class));
			  break;
		  case R.id.img_deleteinspection:
			  cf.Create_Table(3);
			  System.out.println("Ins "+cf.Insp_id);
			  cf.sql = "select * from " + cf.policyholder
				+ " where  GCH_PH_InspectorId = '"
				+ cf.encode(cf.Insp_id) + "'";
			  Cursor cur = cf.gch_db.rawQuery(cf.sql, null);
			  cf.rws = cur.getCount();
			  if(cf.rws>0)
			  {
				  startActivity(new Intent(HomeScreen.this,DeleteInspection.class));
			  }
			  else
			  {
				  cf.ShowToast("Sorry, No Inspection found please import.", 1); 
			  }
			  break;
		  case R.id.img_import:
			  startActivity(new Intent(HomeScreen.this,Import.class));
			  break;
		  case R.id.img_export:
			  Intent inte = new Intent(getApplicationContext(),ExportInspection.class);
			  inte.putExtra("type", "export");
			  startActivity(inte);
              break;
		  case R.id.img_search:
			
			 
			 search();
			  
			  break;
		  case R.id.img_update:
			  Checkforupgrade();
			  break;
		
		/*  case R.id.HS_report_it:
			
				 Intent inte1 = new Intent(getApplicationContext(),ReportToIT.class);
				  startActivity(inte1);
							
			  break;*/
		  case R.id.expand:
			  
			  
				
		  break;
		  case R.id.minimize:
			  rl.setVisibility(View.VISIBLE);  
				 TranslateAnimation slide1 = new TranslateAnimation(0, 0, 0,-(rl.getHeight()+50));   
				 slide1.setDuration(1500);   
				 slide1.setFillAfter(true);   
				 rl.startAnimation(slide1); 
				 slide1.setAnimationListener(new AnimationListener() {
					
					@Override
					public void onAnimationStart(Animation animation) {
						// TODO Auto-generated method stub
						min.setVisibility(View.INVISIBLE);
					}
					
					@Override
					public void onAnimationRepeat(Animation animation) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void onAnimationEnd(Animation animation) {
						// TODO Auto-generated method stub
						exp.setVisibility(View.VISIBLE);
					}
				});
				 
				
				 
		  break;
		/*  case R.id.app_main_menu:
			 
		  if(cf.application_sta)
			 {
			  System.out.println("app ite stru");
			  
			  
			  
			  
					  Intent loginpage = new Intent(Intent.ACTION_MAIN);
					  loginpage.setComponent(new ComponentName("com.idinspection","com.idinspection.ApplicationMenu"));
					  startActivity(loginpage);
					  overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
			 }  
			 else 
			 {
				 System.out.println("app ite not install");
				 cf.ShowToast("Plese install the application then press for the main menu",1);
			 }
		  break;*/
		  case R.id.backtomainmenu:
				 
		  if(cf.application_sta)
			 {
			  System.out.println("app ite stru");
			  
			  
			  
			  
					  Intent loginpage = new Intent(Intent.ACTION_MAIN);
					  loginpage.setComponent(new ComponentName("com.idinspection","com.idinspection.ApplicationMenu"));
					  startActivity(loginpage);
					  overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
			 }  
			 else 
			 {
				 System.out.println("app ite not install");
				 cf.ShowToast("Plese install the application then press for the main menu",1);
			 }
		  break;
		  case R.id.check_new_insp:
			  getIDMA_application();
		  break;
		  }
		  
	 }
	 private void getIDMA_application() {
		// TODO Auto-generated method stub

			if (cf.isInternetOn()) {
				progressDialog = ProgressDialog.show(this, "",
						"Please wait...");
				new Thread() {
					private int usercheck = 0;

					public void run() {
						// Looper.prepare();
						try {
							if (getIDMAversioncodefromweb() == true) {
								if (k != 5 && k != 8) {
									usercheck = 1;
								}
								handler.sendEmptyMessage(0);
								progressDialog.dismiss();
							} else {
								
								progressDialog.dismiss();  
								
								k=20;
								handler.sendEmptyMessage(0);

							}

						} catch (SocketTimeoutException s) {
							k = 5;
							handler.sendEmptyMessage(0);
						} catch (NetworkErrorException n) {
							k = 5;
							handler.sendEmptyMessage(0);
						} catch (IOException io) {
							k = 5;
							handler.sendEmptyMessage(0);
						} catch (XmlPullParserException x) {
							k = 5;
							handler.sendEmptyMessage(0);
						} catch (Exception e) {
							progressDialog.dismiss();
							k = 8;
							handler.sendEmptyMessage(0);
						}
						
					};

					private boolean getIDMAversioncodefromweb() throws IOException,NetworkErrorException,SocketTimeoutException, XmlPullParserException, Exception {
						// TODO Auto-generated method stub
						 SoapObject request1 = new SoapObject(cf.NAMESPACE, "GetVersionInformation_IDMS");
							SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(
									SoapEnvelope.VER11);
							envelope1.dotNet = true;
							envelope1.setOutputSoapObject(request1);
							HttpTransportSE androidHttpTransport1 = new HttpTransportSE(cf.URL_IDMA);
					    	androidHttpTransport1.call(cf.NAMESPACE+"GetVersionInformation_IDMS", envelope1);
					    	
					    	SoapObject result1 = (SoapObject) envelope1.getResponse();
							
							SoapObject obj1 = (SoapObject) result1.getProperty(0);
							cf.newcode = String.valueOf(obj1.getProperty("VersionCode"));
							cf.newversion = String.valueOf(obj1.getProperty("VersionName"));
							System.out.println(" version name and version code  "+cf.newcode+"  "+cf.newversion);
							int newvcode=0;
							try
							{
							newvcode=Integer.parseInt(cf.newcode);
							}catch (Exception e) {
								// TODO: handle exception
							}	
							if (!"null".equals(cf.newcode) && null != cf.newcode && !cf.newcode.equals("")) {
								cf.Create_Table(20);
								Cursor c12 = cf.SelectTablefunction(cf.IDMAVersion," Where ID_VersionType='IDMA_Old' ");
								if(c12.getCount()>=1)
								{
									c12.moveToFirst();
									int V_code=c12.getInt(c12.getColumnIndex("ID_VersionCode"));
									System.out.println("issues not here in "+V_code+"<"+newvcode);
									if(V_code<newvcode)
									{
										System.out.println("issues not here in ");
										k=20;
										return true;
										
									}
									else
									{
										usercheck=1;
										k=0;
										return true;
									}
									
								}
								else
								{
									k=5;
									return false;
								}
								
							}else
							{
								k=5;
								return false;
								
							}
							
					}

					private Handler handler = new Handler() {
					public int m=0;
					private AlertDialog alertDialog;

						@Override
						public void handleMessage(Message msg) {
							System.out.println("comes in the handler k"+k);
							if (k == 5 || k == 8) {
								cf.ShowToast("There is a problem on your Network. Please try again later with better Network.. ",1);
								progressDialog.dismiss();
							}else if(k==20)
							{
							
								alertDialog = new AlertDialog.Builder(HomeScreen.this)
								.create();
						alertDialog
								.setMessage("Upgrade your Inspection Depot Mobile Application to the new Version "
										+ cf.newversion
										+ ".\n The previous data will not be affected.");
						alertDialog.setButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
							
								progressDialog1 = ProgressDialog
										.show(HomeScreen.this,
												"",
												"Please wait Upgrading Version  "
														+ cf.newversion
														+ " will take few minutes ...");
								new Thread() {
									public void run() {
										// Looper.prepare();
										
								try
								{
									
									upgrdingIDMS();
								} catch (SocketTimeoutException s) {
									m=1;
									//cf.ShowToast("You have problem in your server connection please conntact paperless inspection admin.",1);
								
								} catch (NetworkErrorException n) {
									m=2;
									//cf.ShowToast("You have internet problem please try again later.",1);
								
								} catch (IOException io) {
									m=1;
								//	cf.ShowToast("You have problem in your server connection please contact papperless inspection admin.",1);
								
								} catch (XmlPullParserException x) {
									m=1;
									//cf.ShowToast("You have problem in your server connection please conntact paperless inspection admin.",1);
								} catch (Exception e) {
									m=8;
									//cf.ShowToast("You have problem in your upgrading .",1);
								}
									};

									

									private Handler handler = new Handler() {
										public void handleMessage(
												Message msg) {
											progressDialog1.dismiss();

											if (m == 2) {
												cf.ShowToast("Internet connection is not available.",1);

											} else if (m == 8) {cf.ShowToast("Error in login please try again latter .",1);

											} else if (m == 5) {
												cf.ShowToast("There is a problem on your Network. Please try again later with better Network..",1);

											}
											else if(m==1)
											{
												cf.ShowToast("You have problem in your server connection please contact papperless inspection admin.",1);
											}

										}
									};
								}.start();
									}
						});
						alertDialog.show();
								
							}
							else if (usercheck == 1 && k!=20 ) {
							
							
								usercheck = 0;
								cf.ShowToast("Your system has been upgraded to the current version.",1);
							}
							}

						};
					
				}.start();
			} else {
				cf.ShowToast("Please enable the Internet connection.",1);
			}
			
	}
	 public void upgrdingIDMS() throws IOException,NetworkErrorException,SocketTimeoutException, XmlPullParserException, Exception
		{
			SoapObject webresult;
			SoapObject request = new SoapObject(cf.NAMESPACE, "GeAPKFile_IDMS");
			
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);

			HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL_IDMA);

			androidHttpTransport.call(cf.NAMESPACE+"GeAPKFile_IDMS", envelope);

			Object response = envelope.getResponse();

			byte[] b;
			if (cf.checkresponce(response)) // check the response is valid or
											// not
			{
				b = Base64.decode(response.toString());

				try {
					String PATH = Environment.getExternalStorageDirectory()
							+ "/Download/IDMS";
					File file = new File(PATH);
					file.mkdirs();

					File outputFile = new File(PATH + "/IDMS.apk");
					FileOutputStream fileOuputStream = new FileOutputStream(
							outputFile);
					fileOuputStream.write(b);
					fileOuputStream.close();

					//cf.fn_logout(cf.InspectorId); /* FOR LOGOUT*/
					cf.Create_Table(20);
					Cursor c12 = cf.SelectTablefunction(cf.IDMAVersion," ");

					if (c12.getCount() < 1) {
						try {
							cf.gch_db.execSQL("INSERT INTO "
									+ cf.IDMAVersion
									+ "(VId,ID_VersionCode,ID_VersionName,ID_VersionType)"
									+ " VALUES ('2','"+cf.newcode+"','" + cf.newversion
									+ "','IDMA_New')");

						} catch (Exception e) {
							
						}

					} else {
						try {
							cf.gch_db.execSQL("UPDATE " + cf.IDMAVersion
									+ " set ID_VersionCode='"+cf.newcode+"',ID_VersionName='"
									+ cf.newversion
									+ "' Where  ID_VersionType='IDMA_New'");
						} catch (Exception e) {
						
						}
					}
					progressDialog1.dismiss();
					Intent intent = new Intent(Intent.ACTION_VIEW);
					intent.setDataAndType(Uri.fromFile(new File(Environment
							.getExternalStorageDirectory()
							+ "/Download/IDMS/"
							+ "IDMS.apk")),
							"application/vnd.android.package-archive");
					startActivity(intent);
				} catch (IOException e) {
					cf.ShowToast("Update error!",1);
				}
			} else {
				progressDialog1.dismiss();
				cf.ShowToast("There is a problem on your Network.\n Please try again later with better Network.",1);
			}	
		}
		
	private void Checkforupgrade() {
		 
		// TODO Auto-generated method stub
			if (cf.isInternetOn()) {
				progressDialog = ProgressDialog.show(HomeScreen.this, "",
						"Please wait...");/**Testing Team Request us to change that form Checking for Update to Please wait **/
				new Thread() {
					private int usercheck = 0;

					public void run() {
						// Looper.prepare();
						try {
							if (getversioncodefromweb() == true) {
								if (k != 5 && k != 8) {
									usercheck = 1;
								}
								handler.sendEmptyMessage(0);
								progressDialog.dismiss();
							} else {
								progressDialog.dismiss();
								Intent imp = new Intent(HomeScreen.this,
										HomeScreen.class);
								imp.putExtra("newversion", cf.newversion);
								imp.putExtra("versioncode", cf.newcode);
								imp.putExtra("upgrade", "true");
								imp.putExtra("userpermitted", "false");
								startActivity(imp);

							}

						} catch (SocketTimeoutException s) {
							k = 5;
							handler.sendEmptyMessage(0);
						} catch (NetworkErrorException n) {
							k = 5;
							handler.sendEmptyMessage(0);
						} catch (IOException io) {
							k = 5;
							handler.sendEmptyMessage(0);
						} catch (XmlPullParserException x) {
							k = 5;
							handler.sendEmptyMessage(0);
						} catch (Exception e) {
							progressDialog.dismiss();
							k = 8;
							handler.sendEmptyMessage(0);
						}
					};

					private Handler handler = new Handler() {
						

						@Override
						public void handleMessage(Message msg) {
							if (k == 5 || k == 8) {
								cf.ShowToast("There is a problem on your Network. Please try again later with better Network.. ",1);
							} else if (usercheck == 1) {
								usercheck = 0;
								cf.ShowToast("Your system has been upgraded to the current version.",1);
							}

						}
					};
				}.start();
			} else {
				cf.ShowToast("Please enable the Internet connection.",1);
			}
		
	}
	private void search() {
		// TODO Auto-generated method stub
		    final EditText tvxtstname,tvlstname,tvploicy;
			final Dialog dialog = new Dialog(HomeScreen.this);
			dialog.setContentView(R.layout.search);
			dialog.setTitle("This is an online search. Internet connection is required.");

			dialog.setCancelable(true);
			tvxtstname = (EditText) dialog.findViewById(R.id.txtfirstname);
			tvlstname = (EditText) dialog.findViewById(R.id.txtlastname);
			tvploicy = (EditText) dialog.findViewById(R.id.txtpolicy);
			tvxtstname.requestFocusFromTouch();
			Button btnsrch = (Button) dialog
					.findViewById(R.id.search);
			btnsrch.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					if (tvxtstname.getText().toString().equals("")
							&& tvlstname.getText().toString().equals("")
							&& tvploicy.getText().toString().equals("")) {
						
						cf.ShowToast("Please enter any one of the fields to search.",1);
					} else {
						cf.hidekeyboard(tvxtstname);
						cf.hidekeyboard(tvlstname);
						cf.hidekeyboard(tvploicy);
						dialog.dismiss();
						show();
					}
				}

				private void show() {
					// TODO Auto-generated method stub
					AlertDialog.Builder builder = new AlertDialog.Builder(
							HomeScreen.this);
					builder.setTitle("Network Connection")
							.setMessage(
									" To search inspection, requires an internet connection. Do you have internet connection? ");
					builder.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
									NetworkInfo info = conMgr
											.getActiveNetworkInfo();
									if (info != null && info.isConnected()) {
										Intent srch = new Intent(
												HomeScreen.this,
												SearchInspection.class);
										srch.putExtra("first", tvxtstname
												.getText().toString());
										srch.putExtra("last", tvlstname
												.getText().toString());
										srch.putExtra("policy", tvploicy
												.getText().toString());
										startActivity(srch);
									} else {
									cf.ShowToast("Internet connection is not available.",1);cf.hidekeyboard();
									}
								}
							});
					builder.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									cf.hidekeyboard();
								}
							});
					builder.show();
				}

			});
			Button btncls = (Button) dialog.findViewById(R.id.cls);
			btncls.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					cf.hidekeyboard(tvxtstname);
					cf.hidekeyboard(tvlstname);
					cf.hidekeyboard(tvploicy);
					dialog.dismiss();
				}

			});
			dialog.show();
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
			// replaces the default 'Back' button action
		Intent loginpage;
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				if(cf.application_sta)
				{
					 loginpage = new Intent(Intent.ACTION_MAIN);
					 loginpage.setComponent(new ComponentName("com.idinspection","com.idinspection.ApplicationMenu"));
				}
				else
				{
			
				loginpage = new Intent(HomeScreen.this,GeneralConditionHazardActivity.class);
				loginpage.putExtra("back", "exit");
			
				
				}
				startActivity(loginpage);
				return true;
			}
	
			return super.onKeyDown(keyCode, event);
			
		}
	private void displayinspectorphoto() {
		// TODO Auto-generated method stub
		        
				try
				{
						Cursor c = cf.SelectTablefunction(cf.inspectorlogin,
								" where Fld_InspectorId='" + cf.Insp_id.toString() + "'");
						int rws1 = c.getCount();				
						c.moveToFirst();
						String inspext = cf.decode(c.getString(c.getColumnIndex("Fld_InspectorPhotoExtn")));
						File outputFile = new File(this.getFilesDir()+"/"+cf.Insp_id.toString()+inspext);
							
					BitmapFactory.Options o = new BitmapFactory.Options();
					o.inJustDecodeBounds = true;
					BitmapFactory.decodeStream(new FileInputStream(outputFile),
							null, o);
					final int REQUIRED_SIZE = 200;
					int width_tmp = o.outWidth, height_tmp = o.outHeight;
					int scale = 1;
					while (true) {
						if (width_tmp / 2 < REQUIRED_SIZE
								|| height_tmp / 2 < REQUIRED_SIZE)
							break;
						width_tmp /= 2;
						height_tmp /= 2;
						scale *= 2;
					}
					BitmapFactory.Options o2 = new BitmapFactory.Options();
					o2.inSampleSize = scale;
					Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(
							outputFile), null, o2);
					BitmapDrawable bmd = new BitmapDrawable(bitmap);
					cf.img_InspectorPhoto.setImageDrawable(bmd);
					inspector_photo.setImageDrawable(bmd);
				}
				catch (IOException e)
				{
					cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ HomeScreen.this+" "+" in the stage of retrieving inspector image at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
		}
	class CountDownRunner implements Runnable {
		// @Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				try {
					
					doWork();
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				} catch (Exception e) {
				}
			}
		}
	}
	
	
	public void doWork() {
		runOnUiThread(new Runnable() {
			public void run() {
				try {
					TextView txtCurrentTime = (TextView) findViewById(R.id.txtdate);
					Date dt = new Date();
					int hours = dt.getHours();
					int minutes = dt.getMinutes();
					int seconds = dt.getSeconds();
					String curTime = hours + ":" + minutes + ":" + seconds;

					txtCurrentTime.setText(formattedDate + " " + curTime);
				} catch (Exception e) {
				}
			}
		});
	}
	public boolean getversioncodefromweb() throws NetworkErrorException,
	SocketTimeoutException, IOException, XmlPullParserException,Exception
 {
	 SoapObject request = new SoapObject(cf.NAMESPACE, "GetVersionInformation_GenHazards");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
    	androidHttpTransport.call(cf.NAMESPACE+"GetVersionInformation_GenHazards", envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		SoapObject obj = (SoapObject) result.getProperty(0);
		cf.newcode = String.valueOf(obj.getProperty("VersionCode"));
		cf.newversion = String.valueOf(obj.getProperty("VersionName"));
		if (!"null".equals(cf.newcode) && null != cf.newcode && !cf.newcode.equals("")) {
			if (Integer.toString(vcode).equals(cf.newcode)) {
				return true;
			} else {
				try {     
					cf.Create_Table(2);
					Cursor c12 = cf.SelectTablefunction(cf.Version," ");

					if (c12.getCount() < 1) {
						try {
							cf.gch_db.execSQL("INSERT INTO "
									+ cf.Version
									+ "(VId,SH_VersionCode,SH_VersionName)"
									+ " VALUES ('1','"+cf.newcode+"','" + cf.newversion
									+ "')");

						} catch (Exception e) {
							
						}

					} else {
						try {
							cf.gch_db.execSQL("UPDATE " + cf.Version
									+ " set SH_VersionCode='"+cf.newcode+"',SH_VersionName='"
									+ cf.newversion
									+ "'");
						} catch (Exception e) {
						
						}

					}
					return false;
				} catch (Exception e) {
					return true;

				}

			}
		} else {
			return true;
		}
	 
 }
 private int getcurrentversioncode() {
		// TODO Auto-generated method stub

		try {
			vcode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;

		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return vcode;
	}
 private void Update()throws IOException,NetworkErrorException,SocketTimeoutException, XmlPullParserException, Exception {
		// TODO Auto-generated method stub
		
			SoapObject webresult;
			SoapObject request = new SoapObject(cf.NAMESPACE, "GetCurrentAPKFile_GenHazards");
			
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);
		
			HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);

			androidHttpTransport.call(cf.NAMESPACE+"GetCurrentAPKFile_GenHazards", envelope);

			Object response = envelope.getResponse();

			byte[] b;
			if (cf.checkresponce(response)) // check the response is valid or
											// not
			{
				b = Base64.decode(response.toString());

				try {
					String PATH = Environment.getExternalStorageDirectory()
							+ "/Download";
					File file = new File(PATH);
					file.mkdirs();

					File outputFile = new File(PATH + "/GCH.apk");
					FileOutputStream fileOuputStream = new FileOutputStream(
							outputFile);
					fileOuputStream.write(b);
					fileOuputStream.close();

					cf.fn_logout(cf.Insp_id); /* FOR LOGOUT*/
					Cursor c12 = cf.SelectTablefunction(cf.Version," ");

					if (c12.getCount() < 1) {
						try {
							cf.gch_db.execSQL("INSERT INTO "
									+ cf.Version
									+ "(VId,SH_VersionCode,SH_VersionName)"
									+ " VALUES ('1','"+cf.newcode+"','" + cf.newversion
									+ "')");

						} catch (Exception e) {
							
						}

					} else {
						try {
							cf.gch_db.execSQL("UPDATE " + cf.Version
									+ " set SH_VersionCode='"+cf.newcode+"',SH_VersionName='"
									+ cf.newversion
									+ "'");
						} catch (Exception e) {
						
						}
					}
					progressDialog1.dismiss();
					Intent intent = new Intent(Intent.ACTION_VIEW);
					intent.setDataAndType(Uri.fromFile(new File(Environment
							.getExternalStorageDirectory()
							+ "/Download/"
							+ "GCH.apk")),
							"application/vnd.android.package-archive");
					startActivity(intent);
				} catch (IOException e) {
					cf.ShowToast("Update error!",1);
				}
			} else {
				progressDialog1.dismiss();
				cf.ShowToast("There is a problem on your Network.\n Please try again later with better Network.",1);
			}

		
	}
 private void animatebutton() {
		// TODO Auto-generated method stub
		try {

			new Thread() {
				private boolean img1;

				public void run() {
					int count = 1;
					while (active) {
						try {
							this.sleep(500);
						} catch (InterruptedException e) {

							e.printStackTrace();

						}

						if ((count % 2) == 1) {

							img1 = false;

							handler.sendEmptyMessage(0);

						} else {
							img1 = true;
							handler.sendEmptyMessage(0);
						}

						count++;
					}
				};

				private Handler handler = new Handler() {

					public void handleMessage(Message msg) {

						if (img1) {
							img_update.setImageResource(R.drawable.checkforupdateversion1);
						} else {
							img_update.setImageResource(R.drawable.blink);
						}

					}
				};

			}.start();
		} catch (Exception e) {

		}
	}
 public void onStop() {
		active = false;
		super.onStop();
	}

	public void onResume() {
		active = true;
		super.onResume();

	}
	
}
