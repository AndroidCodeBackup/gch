package inspectiondepot.gch;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;

import android.R.drawable;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.view.inputmethod.InputMethodManager;
public class photos extends Activity  {

	private static final String TAG = null;
	CommonFunctions cf;
	public int elev = 0;
	public int t = 0;
	public Uri CapturedImageURI;
	public String name = "Other";int etchangeordermaxLength = 2;

	public int maxlevel;
	public TextView txthdr;
	Map<String, String[]> elevationcaption_map = new LinkedHashMap<String, String[]>();
	String[] arrpath, arrpathdesc, arrimgord;
	public LinearLayout lnrlayout;
	public String imagorder, updstr;;
	public int chk = 0;
	int globalvar=0;
	Spinner[] spnelevation;
	TextView[] tvstatus;
	/**Rotating the image variable declaration **/	 Bitmap rotated_b;
	 int  currnet_rotated=0;
	 /**Rotating the image variable declaration ends **/
	/** Variable declaration for the multi image selection **/
	String[] pieces1,pieces3;
	protected Button selectImgBtn;
	protected Button cancelBtn;
	protected Button backbtn;
	protected TextView chaild_titleheade;
	protected TextView titlehead;
	protected LinearLayout lnrlayout2_FI;
	protected LinearLayout lnrlayout1_FI;
	protected Dialog pd;
	public String finaltext="";
	public int selectedcount;
	public String[] selectedtestcaption;
	public int maximumindb;
	public int backclick_FI;
	public String path="";
	public int max_allowed=8;/**Allowed maxmum number of images **/
	Map<String, TextView> map1 = new LinkedHashMap<String, TextView>();
	
	/**Newly added for limiting option **/
	public int limit_start=0; 
	RelativeLayout Lin_Pre_nex=null;
	ImageView prev=null,next=null;
	File[] Currentfile_list=null;
	private File images=null;
	public  ScrollView scr2=null;
	protected RelativeLayout Rec_count_lin=null;
	protected TextView Total_Rec_cnt=null;
	protected TextView showing_rec_cnt;
	/** Variable declaration for the multi image selection ends **/

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cf = new CommonFunctions(this);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			try {
				cf.ph_type = extras.getInt("type");
				/** Find the elevation type from the type **/
				elev = cf.ph_type - 50;
				cf.selectedhomeid = extras.getString("homeid");
				cf.onlinspectionid = extras.getString("InspectionType");
				cf.onlstatus = extras.getString("status");
			} catch (Exception e) {
				// TODO: handle exception

			}
		}
		setContentView(R.layout.photos);
		cf.getDeviceDimensions();
		LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
		mainmenu_layout.setMinimumWidth(cf.wd);
		mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(),
				5, 0, cf));
		LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.generalsubmenu);
		submenu_layout.addView(new MyOnclickListener(getApplicationContext(),
				cf.ph_type, 1, cf));
		ScrollView scr = (ScrollView) findViewById(R.id.scr);
		scr.setMinimumHeight(cf.ht);
		cf.getInspectorId();
		cf.getinspectiondate();
		cf.Create_Table(16);
		cf.Create_Table(15);
		/** Declaring the name of elevation **/
		switch (elev) {
		case 1:
			name = "Front";
			break;
		case 2:
			name = "Right";
			break;
		case 3:
			name = "Back";
			break;
		case 4:
			name = "Left";
			break;
		case 5:
			name = "Additional Photographs";
			max_allowed=16;
			break;
		case 6:
			name = "Roof";
			max_allowed=8;
			break;

		}
		/** Declaring the name of elevation **/
		/** Declaration objects starts **/
		txthdr = (TextView) findViewById(R.id.titlehdr);
		lnrlayout = (LinearLayout) this.findViewById(R.id.linscrollview);

		/** Declaration objects ends **/

		/** Show the saved values **/
		show_savedvalue();
		/** Show the saved values ends **/
	}

	public void show_savedvalue() {
		// TODO Auto-generated method stub
		try {
			int rws = 0;
			Cursor c11 = cf.gch_db.rawQuery("SELECT * FROM " + cf.ImageTable
					+ " WHERE GCH_IM_SRID='" + cf.selectedhomeid
					+ "' and GCH_IM_Elevation='" + elev
					+ "' order by GCH_IM_ImageOrder", null);
			maxlevel = rws = c11.getCount();

			int rem = max_allowed - rws;
			arrpath = new String[rws];
			arrpathdesc = new String[rws];
			arrimgord = new String[rws];
			String source = "<b><font color=#000000> Number of uploaded images : "
					+ "</font><font color=#DAA520>"
					+ rws
					+ "</font><font color=#000000> Remaining : "
					+ "</font><font color=#DAA520>" + rem + "</font>";

			txthdr.setText(Html.fromHtml(source));
			if (rws == 0) {

				lnrlayout.removeAllViews();
				/*
				 * firsttxt.setVisibility(v1.GONE);
				 * fstimg.setVisibility(v1.GONE); updat.setVisibility(v1.GONE);
				 * del.setVisibility(v1.GONE);
				 */
			} else {
				int Column1 = c11.getColumnIndex("GCH_IM_ImageName");
				int Column2 = c11.getColumnIndex("GCH_IM_Description");
				int Column3 = c11.getColumnIndex("GCH_IM_ImageOrder");
				c11.moveToFirst();
				if (c11 != null) {
					int i = 0;
					do {
						commonelevation(cf.decode(c11.getString(Column3)), i); // set
																				// the
																				// caption
																				// form
																				// the
																				// database
						arrpath[i] = cf.decode(c11.getString(Column1));
						arrpathdesc[i] = cf.decode(c11.getString(Column2));
						arrimgord[i] = cf.decode(c11.getString(Column3));
						ColorDrawable sage = new ColorDrawable(this
								.getResources().getColor(R.color.sage));
						i++;
					} while (c11.moveToNext());
					
					dynamicview();
					//cf.hidekeyboard();
					
				}
			}
			showtick();
		} catch (Exception e) {
			// Log.i(TAG, "error correct= " + e.getMessage());
		}
	}

	public void showtick() {
		// TODO Auto-generated method stub
		System.out.println("cones in the correct part ");
		cf.Create_Table(15);
		Cursor c1=cf.gch_db.rawQuery("SELECT * FROM "+cf.ImageTable+" WHERE GCH_IM_SRID='"+cf.selectedhomeid+"' AND GCH_IM_ImageOrder='1' ORDER BY GCH_IM_Elevation",null);
		System.out.println("cones in the correct part  count "+c1.getCount());
		int fe1,fe2,fe3,fe4,fe5,fe6;
		fe1=fe2=fe3=fe4=fe5=fe6=0;
		if(c1.getCount()>=1)
		{	
		c1.moveToFirst();
		for(int i=0;i<c1.getCount();i++)
		{
			System.out.println("cones in the correct part  count no issues"+c1.getString(c1.getColumnIndex("GCH_IM_Elevation")));	
				if(c1.getString(c1.getColumnIndex("GCH_IM_Elevation")).equals("1"))
				{
					((ImageView) findViewById(R.id.ph_sub_1_tick)).setVisibility(View.VISIBLE);
					fe1=1;
				}
				else if(c1.getString(c1.getColumnIndex("GCH_IM_Elevation")).equals("2"))
				{
					((ImageView) findViewById(R.id.ph_sub_2_tick)).setVisibility(View.VISIBLE);
					fe2=1;
				}
				else if(c1.getString(c1.getColumnIndex("GCH_IM_Elevation")).equals("3"))
				{
					((ImageView) findViewById(R.id.ph_sub_3_tick)).setVisibility(View.VISIBLE);
					fe3=1;
				}
				else if(c1.getString(c1.getColumnIndex("GCH_IM_Elevation")).equals("4"))
				{
					((ImageView) findViewById(R.id.ph_sub_4_tick)).setVisibility(View.VISIBLE);
					fe4=1;
				}
				else if(c1.getString(c1.getColumnIndex("GCH_IM_Elevation")).equals("5"))
				{
					((ImageView) findViewById(R.id.ph_sub_5_tick)).setVisibility(View.VISIBLE);
					fe5=1;
				}
				else if(c1.getString(c1.getColumnIndex("GCH_IM_Elevation")).equals("6"))
				{
					((ImageView) findViewById(R.id.ph_sub_6_tick)).setVisibility(View.VISIBLE);
					fe6=1;
				}
				c1.moveToNext();
			}
			if(fe1==0)
			{
				((ImageView) findViewById(R.id.ph_sub_1_tick)).setVisibility(View.GONE);
			}
			if(fe2==0)
			{
				((ImageView) findViewById(R.id.ph_sub_2_tick)).setVisibility(View.GONE);
			}
			if(fe3==0)
			{
				((ImageView) findViewById(R.id.ph_sub_3_tick)).setVisibility(View.GONE);
			}
			if(fe4==0)
			{
				((ImageView) findViewById(R.id.ph_sub_4_tick)).setVisibility(View.GONE);
			}
			if(fe5==0)
			{
				((ImageView) findViewById(R.id.ph_sub_5_tick)).setVisibility(View.GONE);
			}
			if(fe6==0)
			{
				((ImageView) findViewById(R.id.ph_sub_6_tick)).setVisibility(View.GONE);
			}
			
		}
		else
		{
			((ImageView) findViewById(R.id.ph_sub_1_tick)).setVisibility(View.GONE);
			((ImageView) findViewById(R.id.ph_sub_2_tick)).setVisibility(View.GONE);
			((ImageView) findViewById(R.id.ph_sub_3_tick)).setVisibility(View.GONE);
			((ImageView) findViewById(R.id.ph_sub_4_tick)).setVisibility(View.GONE);
			((ImageView) findViewById(R.id.ph_sub_5_tick)).setVisibility(View.GONE);
			((ImageView) findViewById(R.id.ph_sub_6_tick)).setVisibility(View.GONE);
		}
		
		
	}

	private void commonelevation(String imgor, int spin) {
		// TODO Auto-generated method stub
		int imgorder = 0;
		try {
			imgorder = Integer.parseInt(imgor);
		} catch (Exception e) {

		}
		Cursor c1;

		c1 = cf.SelectTablefunction(cf.Photo_caption,
				" WHERE GCH_IMP_Elevation='" + elev
						+ "' and GCH_IMP_ImageOrder='" + imgor + "'");
		String[] temp;
		if (c1.getCount() > 0) {
			temp = new String[c1.getCount() + 2];
			temp[0] = "Select";
			temp[1] = "ADD PHOTO CAPTION";
			int i = 2;
			c1.moveToFirst();
			do {
				temp[i] = cf.decode(c1.getString(c1
						.getColumnIndex("GCH_IMP_Caption")));
				i++;
			} while (c1.moveToNext());
		} else {
			temp = new String[2];
			temp[0] = "Select";
			temp[1] = "ADD PHOTO CAPTION";
		}
		elevationcaption_map.put("spiner" + spin, temp);
	}

	private void dynamicview() throws FileNotFoundException {
		// TODO Auto-generated method stub

		
		ImageView[] elevationimage;

		lnrlayout.removeAllViews();
		ScrollView sv = new ScrollView(this);
		lnrlayout.addView(sv);

		LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(l1);
		// l1.setMinimumWidth(925);

		tvstatus = new TextView[arrpath.length];
		elevationimage = new ImageView[arrpath.length];
		spnelevation = new Spinner[arrpath.length];

		for (int i = 0; i < arrpath.length; i++) {

			LinearLayout l2 = new LinearLayout(this);
			l2.setOrientation(LinearLayout.HORIZONTAL);
			l1.addView(l2);

			LinearLayout lchkbox = new LinearLayout(this);
			LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
					175, ViewGroup.LayoutParams.WRAP_CONTENT);
			paramschk.topMargin = 20;
			paramschk.leftMargin = 10;
			l2.addView(lchkbox);

			tvstatus[i] = new TextView(this);
			tvstatus[i].setMinimumWidth(175);
			tvstatus[i].setMaxWidth(175);
			tvstatus[i].setText(arrpathdesc[i]);
			tvstatus[i].setTextColor(Color.BLACK);
			tvstatus[i].setTag("imagechange" + i);
			tvstatus[i].setTextSize(TypedValue.COMPLEX_UNIT_PX, 14);
			lchkbox.addView(tvstatus[i], paramschk);

			String j = "0";
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			try {
				BitmapFactory.decodeStream(new FileInputStream(arrpath[i]),
						null, o);
				j = "1";
			} catch (FileNotFoundException e) {
				
				j = "2";
				
			}
			if (j.equals("1")) {
				final int REQUIRED_SIZE = 100;
				int width_tmp = o.outWidth, height_tmp = o.outHeight;
				int scale = 1;
				while (true) {
					if (width_tmp / 2 < REQUIRED_SIZE
							|| height_tmp / 2 < REQUIRED_SIZE)
						break;
					width_tmp /= 2;
					height_tmp /= 2;
					scale *= 2;
				}
				
				
				// Decode with inSampleSize
				BitmapFactory.Options o2 = new BitmapFactory.Options();
				o2.inSampleSize = scale;
				Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(
						arrpath[i]), null, o2);
				BitmapDrawable bmd = new BitmapDrawable(bitmap);

				LinearLayout lelevimage = new LinearLayout(this);
				LinearLayout.LayoutParams paramselevimg = new LinearLayout.LayoutParams(
						100, 50);
				paramselevimg.topMargin = 10;
				paramselevimg.leftMargin = 20;
				l2.addView(lelevimage);

				elevationimage[i] = new ImageView(this);
				elevationimage[i].setMinimumWidth(75);
				elevationimage[i].setMaxWidth(75);
				elevationimage[i].setMinimumHeight(75);
				elevationimage[i].setMaxHeight(75);
				elevationimage[i].setImageDrawable(bmd);
				elevationimage[i].setTag("imagechange" + i);
				lelevimage.addView(elevationimage[i], paramselevimg);
			}
			else
			{
				Bitmap bmp= BitmapFactory.decodeResource(getResources(),R.drawable.photonotavail);
				LinearLayout lelevimage = new LinearLayout(this);
				LinearLayout.LayoutParams paramselevimg = new LinearLayout.LayoutParams(100, 50);
				paramselevimg.topMargin = 10;
				paramselevimg.leftMargin = 20;
				l2.addView(lelevimage);

				elevationimage[i] = new ImageView(this);
				elevationimage[i].setMinimumWidth(75);
				elevationimage[i].setMaxWidth(75);
				elevationimage[i].setMinimumHeight(75);
				elevationimage[i].setMaxHeight(75);
				elevationimage[i].setImageBitmap(bmp);
				elevationimage[i].setTag("imagechange" + i);
				lelevimage.addView(elevationimage[i], paramselevimg);
				
			}
			try
			{
			LinearLayout lspinner = new LinearLayout(this);
			LinearLayout.LayoutParams paramsspinn = new LinearLayout.LayoutParams(
					200, LayoutParams.WRAP_CONTENT);
			paramsspinn.topMargin = 5;
			paramsspinn.leftMargin = 20;
			l2.addView(lspinner);
			tvstatus[i].setOnClickListener(new View.OnClickListener() {

				public void onClick(final View v) {

					String getidofselbtn = v.getTag().toString();
					final String repidofselbtn = getidofselbtn.replace(
							"imagechange", "");
					final int cvrtstr = Integer.parseInt(repidofselbtn);

					String selpath = arrpath[cvrtstr];
					String seltxt = arrpathdesc[cvrtstr];

					Cursor c11 = cf.gch_db.rawQuery(
							"SELECT GCH_IM_ImageOrder FROM " + cf.ImageTable
									+ " WHERE GCH_IM_SRID='"
									+ cf.selectedhomeid
									+ "' and GCH_IM_Elevation='" + elev
									+ "' and GCH_IM_ImageName='"
									+ cf.encode(arrpath[cvrtstr]) + "'", null);
					c11.moveToFirst();
				   imagorder= c11.getString(c11.getColumnIndex("GCH_IM_ImageOrder"));
					String a;
					chk = 1;
					// dispfirstimg(cvrtstr);
					 dispfirstimg(cvrtstr,seltxt);

				}
			});
			elevationimage[i].setOnClickListener(new View.OnClickListener() {
				public void onClick(final View v) {

					String getidofselbtn = v.getTag().toString();
					final String repidofselbtn = getidofselbtn.replace(
							"imagechange", "");
					final int cvrtstr = Integer.parseInt(repidofselbtn);

					String selpath = arrpath[cvrtstr];
					String seltxt = arrpathdesc[cvrtstr];

					Cursor c11 = cf.gch_db.rawQuery(
							"SELECT GCH_IM_ImageOrder FROM " + cf.ImageTable
									+ " WHERE GCH_IM_SRID='"
									+ cf.selectedhomeid
									+ "' and GCH_IM_Elevation='" + elev
									+ "' and GCH_IM_ImageName='"
									+ cf.encode(arrpath[cvrtstr]) + "'", null);
					c11.moveToFirst();
					imagorder = c11.getString(c11
							.getColumnIndex("GCH_IM_ImageOrder"));
					String a;
					chk = 1;
					// dispfirstimg(cvrtstr);
					dispfirstimg(cvrtstr, seltxt);

				}
			});
			int n = i + 1;

			String[] SH_IM_Elevation = elevationcaption_map.get("spiner" + i);
			spnelevation[i] = new Spinner(this);
			spnelevation[i].setId(i);
			ArrayAdapter adapter2 = new ArrayAdapter(photos.this,android.R.layout.simple_spinner_item, SH_IM_Elevation);
			adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spnelevation[i].setAdapter(adapter2);
			lspinner.addView(spnelevation[i], paramsspinn);
			spnelevation[i].setOnItemSelectedListener(new MyOnItemSelectedListener1(i));
			
			}
			catch (Exception e) {
				// TODO: handle exception
				System.out.println("ececption"+e.getMessage());
			}
		}
		showtick();
	}
	
/** Showing the image for the Update starts **/
	public void dispfirstimg(final int selid, String photocaptions) {
		 System.gc();
		currnet_rotated=0;
		
		final int delimagepos = selid;
		String phtodesc = photocaptions;

		String k;
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;

		final Dialog dialog1 = new Dialog(photos.this,
				android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alert);

		/**
		 * get the help and update relative layou and set the visbilitys for the
		 * respective relative layout
		 */
		LinearLayout Re = (LinearLayout) dialog1.findViewById(R.id.maintable);
		Re.setVisibility(View.GONE);
		LinearLayout Reup = (LinearLayout) dialog1
				.findViewById(R.id.updateimage);
		Reup.setVisibility(View.VISIBLE);
		/**
		 * get the help and update relative layou and set the visbilitys for the
		 * respective relative layout
		 */

		final ImageView upd_img = (ImageView) dialog1.findViewById(R.id.firstimg);
		final EditText upd_Ed = (EditText) dialog1.findViewById(R.id.firsttxt);
		upd_Ed.setFocusable(false);
		//upd_Ed.setFocusableInTouchMode(true);
		Button btn_helpclose = (Button) dialog1
				.findViewById(R.id.imagehelpclose);
		Button btn_up = (Button) dialog1.findViewById(R.id.update);
		Button btn_del = (Button) dialog1.findViewById(R.id.delete);
		Button rotateleft = (Button) dialog1.findViewById(R.id.rotateleft);
		Button rotateright = (Button) dialog1.findViewById(R.id.rotateright);
		 upd_Ed.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				 
				cf.setFocus(upd_Ed);
				return false;
			}
		});
		// update and delete button function

	
		btn_up.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				updstr = upd_Ed.getText().toString();

				try {
					if (!updstr.trim().equals("")) {
						dialog1.setCancelable(true);
						dialog1.dismiss();

						cf.gch_db.execSQL("UPDATE " + cf.ImageTable
								+ " SET GCH_IM_Description='"
								+ cf.encode(updstr)
								+ "',GCH_IM_ModifiedOn='' WHERE GCH_IM_SRID ='"
								+ cf.selectedhomeid
								+ "' and GCH_IM_Elevation='" + elev
								+ "' and GCH_IM_ImageOrder='" + imagorder + "'");
						
						cf.ShowToast("Updated sucessfully.", 1);

					} else {
						cf.ShowToast("Please enter the caption.", 1);cf.hidekeyboard(upd_Ed);
					}
					/**Save the rotated value in to the external stroage place **/
					if(currnet_rotated>0)
					{ 

						try
						{
							/**Create the new image with the rotation **/
							String current=MediaStore.Images.Media.insertImage(getContentResolver(), rotated_b, "My bitmap", "My rotated bitmap");
							  ContentValues values = new ContentValues();
							  values.put(MediaStore.Images.Media.ORIENTATION, 0);
							  photos.this.getContentResolver().update(Uri.parse(current), values, MediaStore.Images.Media.DATA+ "=?", new String[] { current } );
							
							
							if(current!=null)
							{
							String path=getPath(Uri.parse(current));
							File fout = new File(arrpath[selid]);
							fout.delete();
							/** delete the selected image **/
							File fin = new File(path);
							/** move the newly created image in the slected image pathe ***/
							fin.renameTo(new File(arrpath[selid]));
							
							
						}
						} catch(Exception e)
						{
							System.out.println("Error occure while rotate the image "+e.getMessage());
						}
						
					}
				} catch (Exception e) {
					System.out.println("erre " + e.getMessage());
				}
				/**Save the rotated value in to the external stroage place ends **/
				show_savedvalue();
			}

		});
		
		btn_del.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog1.setCancelable(true);
				dialog1.dismiss();
				AlertDialog.Builder builder = new AlertDialog.Builder(
						photos.this);
				builder.setMessage(
						"Are you sure, Do you want to delete the image?")
						.setCancelable(false)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int id) {
										try {
											Cursor or = cf.gch_db
													.rawQuery(
															"select GCH_IM_ImageOrder from "
																	+ cf.ImageTable
																	+ " WHERE  GCH_IM_SRID ='"
																	+ cf.selectedhomeid
																	+ "' and GCH_IM_Elevation='"
																	+ elev
																	+ "' and GCH_IM_ImageName='"
																	+ cf.encode(arrpath[delimagepos])
																	+ "'", null);
											or.moveToFirst();
											int image_order = or.getInt(or
													.getColumnIndex("GCH_IM_ImageOrder"));

											cf.gch_db.execSQL("DELETE FROM "
													+ cf.ImageTable
													+ " WHERE GCH_IM_SRID ='"
													+ cf.selectedhomeid
													+ "' and GCH_IM_Elevation='"
													+ elev
													+ "' and GCH_IM_ImageName='"
													+ cf.encode(arrpath[delimagepos])
													+ "'");

											// Get the total count of images in
											// the table
											Cursor c3 = cf
													.SelectTablefunction(
															cf.ImageTable,
															" WHERE GCH_IM_SRID ='"
																	+ cf.selectedhomeid
																	+ "' and GCH_IM_Elevation='"
																	+ elev
																	+ "'");
											for (int m = image_order; m <= c3
													.getCount(); m++) {
												int k = m + 1;
												cf.gch_db.execSQL("UPDATE "
														+ cf.ImageTable
														+ " SET GCH_IM_ImageOrder='"
														+ m
														+ "' WHERE GCH_IM_SRID ='"
														+ cf.selectedhomeid
														+ "' and GCH_IM_Elevation='"
														+ elev
														+ "' and GCH_IM_ImageOrder='"
														+ k + "'");

											}

										} catch (Exception e) {
											System.out.println("exception e  "
													+ e);
										}
										cf.ShowToast(
												"Image has been deleted sucessfully.",
												1);

										chk = 0;
										try {
											Cursor c11 = cf.gch_db
													.rawQuery(
															"SELECT * FROM "
																	+ cf.ImageTable
																	+ " WHERE GCH_IM_SRID='"
																	+ cf.selectedhomeid
																	+ "' and GCH_IM_Elevation='"
																	+ elev
																	+ "'  ",
															null);
											int delchkrws = c11.getCount();

										} catch (Exception e) {

										}
										show_savedvalue();
										// showimages();
									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

										dialog.cancel();
									}
								});
				builder.show();
			}
		});
		btn_helpclose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog1.setCancelable(true);
				dialog1.dismiss();
			}
		});
		
		rotateright.setOnClickListener(new OnClickListener() {  
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.gc();
				currnet_rotated+=90;
				if(currnet_rotated>=360)
				{
					currnet_rotated=0;
				}
				
				Bitmap myImg;
				try {
					myImg = BitmapFactory.decodeStream(new FileInputStream(arrpath[selid]));
					Matrix matrix =new Matrix();
					matrix.reset();
					//matrix.setRotate(currnet_rotated);
					System.out.println("Ther is no more issues top ");
					matrix.postRotate(currnet_rotated);
					
					 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
					        matrix, true);
					 System.gc();
					 upd_img.setImageBitmap(rotated_b);

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (Exception e) {
					
				}
				catch (OutOfMemoryError e) {
					System.out.println("comes in to out ot mem exception");
					System.gc();
					try {
						myImg=null;
						System.gc();
						Matrix matrix =new Matrix();
						matrix.reset();
						//matrix.setRotate(currnet_rotated);
						matrix.postRotate(currnet_rotated);
						myImg= cf.ShrinkBitmap(arrpath[selid], 800, 800);
						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
						
						System.gc();
						upd_img.setImageBitmap(rotated_b); 

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 catch (OutOfMemoryError e1) {
							// TODO Auto-generated catch block
						 cf.ShowToast("You can not rotate this image. Image size is too large.",1);
					}
				}

			}
		});
		rotateleft.setOnClickListener(new OnClickListener() {  			
			public void onClick(View v) {

				// TODO Auto-generated method stub
			
				System.gc();
				currnet_rotated-=90;
				if(currnet_rotated<0)
				{
					currnet_rotated=270;
				}

				
				Bitmap myImg;
				try {
					myImg = BitmapFactory.decodeStream(new FileInputStream(arrpath[selid]));
					Matrix matrix =new Matrix();
					matrix.reset();
					//matrix.setRotate(currnet_rotated);
					System.out.println("Ther is no more issues top ");
					matrix.postRotate(currnet_rotated);
					
					 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
					        matrix, true);
					 
					 upd_img.setImageBitmap(rotated_b);

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (Exception e) {
					
				}
				catch (OutOfMemoryError e) {
					System.out.println("comes in to out ot mem exception");
					System.gc();
					try {
						myImg=null;
						System.gc();
						Matrix matrix =new Matrix();
						matrix.reset();
						//matrix.setRotate(currnet_rotated);
						matrix.postRotate(currnet_rotated);
						myImg= cf.ShrinkBitmap(arrpath[selid], 800, 800);
						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
						System.gc();
						upd_img.setImageBitmap(rotated_b); 

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 catch (OutOfMemoryError e1) {
							// TODO Auto-generated catch block
						 cf.ShowToast("You can not rotate this image. Image size is too large.",1);
					}
				}

			
			}
		});
		
		// update and delete button function ends
		try {
			if (chk == 1) {
				BitmapFactory.decodeStream(new FileInputStream(arrpath[selid]),
						null, o);
			} else {
				BitmapFactory.decodeStream(new FileInputStream(arrpath[0]),
						null, o);
			}
			k = "1";

		} catch (FileNotFoundException e) {
			k = "2";

		}
		if (k.equals("1")) {
			final int REQUIRED_SIZE = 400;
			int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;
			while (true) {
				if (width_tmp / 2 < REQUIRED_SIZE
						|| height_tmp / 2 < REQUIRED_SIZE)
					break;
				width_tmp /= 2;
				height_tmp /= 2;
				scale *= 2;
			}

			// Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			Bitmap bitmap = null;
			try {
				Cursor c11 = cf.gch_db.rawQuery("SELECT * FROM " + cf.ImageTable
						+ " WHERE GCH_IM_SRID='" + cf.selectedhomeid
						+ "' and GCH_IM_Elevation='" + elev
						+ "' and GCH_IM_ImageName='" + cf.encode(arrpath[selid])
						+ "' ", null);
				c11.moveToFirst();
				String SH_IM_Description = c11.getString(c11
						.getColumnIndex("GCH_IM_Description"));

				if (chk == 1) {
					bitmap = BitmapFactory.decodeStream(new FileInputStream(
							arrpath[selid]), null, o2);
					upd_Ed.setText(cf.decode(SH_IM_Description));

				} else {
					bitmap = BitmapFactory.decodeStream(new FileInputStream(
							arrpath[0]), null, o2);
					upd_Ed.setText(cf.decode(arrpathdesc[0]));
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				
			}
			rotated_b=bitmap;
			BitmapDrawable bmd = new BitmapDrawable(bitmap);
			
			upd_img.setImageDrawable(bmd);

			dialog1.setCancelable(false);
			dialog1.show();
		} else {
			upd_Ed.setText(photocaptions);
			rotateright.setVisibility(View.GONE);
			rotateleft.setVisibility(View.GONE);
			dialog1.setCancelable(false);
			dialog1.show();
		}
		upd_Ed.clearFocus();
      //cf.hidekeyboard();
	}
/** Showing the image for the Update starts **/
	private class MyOnItemSelectedListener1 implements OnItemSelectedListener {

		public final int spinnerid;

		MyOnItemSelectedListener1(int id) {
			this.spinnerid = id;
		}

		public void onItemSelected(AdapterView<?> parent, View view,
				final int pos,

				long id) {
			if (pos == 0) {
			} else {

				int maxLength = 99;
				try {

					final int j = spinnerid + 1;
					final String[] photocaption = elevationcaption_map
							.get("spiner" + spinnerid);

					String[] bits = arrpath[spinnerid].split("/");
					final String picname = bits[bits.length - 1];

					if (photocaption[pos].equals("ADD PHOTO CAPTION")) {
						final AlertDialog.Builder alert = new AlertDialog.Builder(
								photos.this);
						alert.setTitle("ADD PHOTO CAPTION");
						final EditText input = new EditText(photos.this);
						alert.setView(input);
						InputFilter[] FilterArray = new InputFilter[1];
						FilterArray[0] = new InputFilter.LengthFilter(maxLength);
						input.setFilters(FilterArray);
						input.setTextSize(14);
						input.setWidth(800);

						alert.setPositiveButton("Add",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int whichButton) {
										String commentsadd = input.getText()
												.toString();
										if (input.length() > 0
												&& !commentsadd.trim().equals(
														"")) {
											cf.gch_db.execSQL("INSERT INTO "
													+ cf.Photo_caption
													+ " (GCH_IMP_INSP_ID,GCH_IMP_Caption,GCH_IMP_Elevation,GCH_IMP_ImageOrder)"
													+ " VALUES ('" + cf.Insp_id
													+ "','"
													+ cf.encode(commentsadd)
													+ "','" + elev + "','" + j
													+ "')");
										
											cf.ShowToast(
													"Photo Caption added successfully.",
													1);
											
											
										cf.hidekeyboard(input);
											show_savedvalue();
											
											
										} else {
											cf.ShowToast(
													"Please enter the caption.",1);
											cf.hidekeyboard(input);
											spnelevation[spinnerid]
													.setSelection(0);  
										}
									}
								});

						alert.setNegativeButton("Cancel",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int whichButton) {
										spnelevation[spinnerid].setSelection(0);
										cf.hidekeyboard(input);
										dialog.cancel();
										
									}
								});
						alert.setCancelable(false);
						alert.show();

					} else {
						final Dialog dialog = new Dialog(photos.this);
						dialog.setContentView(R.layout.alertfront);

						dialog.setTitle("Choose Option");
						dialog.setCancelable(true);
						final EditText edit_desc = (EditText) dialog
								.findViewById(R.id.edittxtdesc);

						Button button_close = (Button) dialog
								.findViewById(R.id.Button01);
						final Button button_sel = (Button) dialog
								.findViewById(R.id.Button02);
						Button button_edit = (Button) dialog
								.findViewById(R.id.Button03);
						Button button_del = (Button) dialog
								.findViewById(R.id.Button04);
						final Button button_upd = (Button) dialog
								.findViewById(R.id.Button05);

						button_close.setOnClickListener(new OnClickListener() {
							public void onClick(View v) {
								spnelevation[spinnerid].setSelection(0);
								cf.hidekeyboard(edit_desc);
								dialog.setCancelable(true);
								dialog.cancel();
							}

							public void onNothingSelected(AdapterView<?> arg0) {
								// TODO Auto-generated method stub

							}
						});
						button_sel.setOnClickListener(new OnClickListener() {
							public void onClick(View v) {
								try {

									cf.gch_db.execSQL("UPDATE "
											+ cf.ImageTable
											+ " SET GCH_IM_Description='"
											+ cf.encode(photocaption[pos])
											+ "',GCH_IM_Nameext='"
											+ cf.encode(picname)
											+ "',GCH_IM_ModifiedOn='"
											+ cf.encode(cf.datewithtime
													.toString())
											+ "' WHERE GCH_IM_SRID ='"
											+ cf.selectedhomeid
											+ "' and GCH_IM_Elevation='" + elev
											+ "' and GCH_IM_ImageOrder='"
											+ arrimgord[spinnerid] + "'");
									spnelevation[spinnerid].setSelection(0);
								} catch (Exception e) {
									cf.Error_LogFile_Creation("Problem in select the caption in the photos at the elevation "
											+ elev
											+ " error  ="
											+ e.getMessage());
								}

								tvstatus[spinnerid].setText(photocaption[pos]);
								dialog.setCancelable(true);
								dialog.cancel();
							}
						});

						button_edit.setOnClickListener(new OnClickListener() {

							public void onClick(View v) {

								button_sel.setVisibility(v.GONE);

								edit_desc.setVisibility(v.VISIBLE);
								edit_desc.setText(photocaption[pos]);

								button_upd.setVisibility(v.VISIBLE);
								System.out.println("edit_desc = ");
								button_upd
										.setOnClickListener(new OnClickListener() {

											//
											public void onClick(View v) {
												if (!edit_desc.getText()
														.toString().trim()
														.equals("")) {
													cf.gch_db.execSQL("UPDATE "
															+ cf.Photo_caption
															+ " set GCH_IMP_Caption = '"
															+ cf.encode(edit_desc
																	.getText()
																	.toString())
															+ "' WHERE GCH_IMP_Elevation ='"
															+ elev
															+ "' and GCH_IMP_ImageOrder='"
															+ arrimgord[j - 1]
															+ "' and GCH_IMP_Caption='"
															+ cf.encode(photocaption[pos])
															+ "'");
													cf.hidekeyboard(edit_desc);
													show_savedvalue();
													cf.ShowToast("Photo Caption has been updated sucessfully.",1);
													dialog.setCancelable(true);
													dialog.cancel();
												} else {
													cf.ShowToast("Please enter the caption.",1);cf.hidekeyboard(edit_desc);
												}
											}
										});
							}
						});

						button_del.setOnClickListener(new OnClickListener() {
							public void onClick(View v) {

								AlertDialog.Builder builder = new AlertDialog.Builder(
										photos.this);
								builder.setMessage(
										"Are you sure, Do you want to delete?")
										.setCancelable(false)
										.setPositiveButton(
												"Yes",
												new DialogInterface.OnClickListener() {
													public void onClick(
															DialogInterface dialog,
															int id) {
														// SH_IMP_INSP_ID,SH_IMP_Caption,SH_IMP_Elevation,SH_IMP_ImageOrder
														cf.gch_db.execSQL("Delete From "
																+ cf.Photo_caption
																+ "  WHERE  GCH_IMP_Elevation ='"
																+ elev
																+ "' and GCH_IMP_ImageOrder='"
																+ arrimgord[j - 1]
																+ "' and GCH_IMP_Caption='"
																+ cf.encode(photocaption[pos])
																+ "'");
														show_savedvalue();
														cf.ShowToast(
																"Photo Caption has been deleted sucessfully.",
																1);

													}
												})
										.setNegativeButton(
												"No",
												new DialogInterface.OnClickListener() {
													public void onClick(
															DialogInterface dialog,
															int id) {
														spnelevation[spinnerid]
																.setSelection(0);
														dialog.cancel();
													}
												});
								dialog.setCancelable(true);
								builder.show();
								dialog.cancel();
							}
						});
						dialog.setCancelable(false);
						dialog.show();
					}

				} catch (Exception e) {
					System.out.println("e + " + e.getMessage());
				}

			}
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.

		}

	}
	private Cursor getVideoIdFromFilePath(String filePath,
	        ContentResolver contentResolver) {


	    long videoId;
	    Log.d(TAG,"Loading file " + filePath);

	            // This returns us content://media/external/videos/media (or something like that)
	            // I pass in "external" because that's the MediaStore's name for the external
	            // storage on my device (the other possibility is "internal")
	    Uri imageUri = MediaStore.Images.Media.getContentUri("external");

	    

	    String[] projection = {MediaStore.Images.ImageColumns._ID,MediaStore.Images.Media.ORIENTATION};

	    // TODO This will break if we have no matching item in the MediaStore.
	    Cursor cursor = contentResolver.query(imageUri, projection, MediaStore.Images.ImageColumns.DATA + " LIKE ?", new String[] { filePath }, null);
	    cursor.moveToFirst();

	   
	    return cursor;
	}

	public void clicker(View v) {
		switch (v.getId()) {
		

		case R.id.browsecamera:
			Cursor c12 = cf.gch_db.rawQuery("SELECT * FROM " + cf.ImageTable
					+ " WHERE GCH_IM_SRID='" + cf.selectedhomeid
					+ "' and GCH_IM_Elevation='" + elev
					+ "' order by GCH_IM_ImageOrder", null);
			int chkrws1 = c12.getCount();
			if (chkrws1 < max_allowed) {
				t = 1;
				startCameraActivity();
			} else {
				cf.ShowToast("You only upload "+max_allowed+" photos per Elevation", 1);
			}

			break;
		case R.id.browsetxt:
			Cursor c11 = cf.gch_db.rawQuery("SELECT * FROM " + cf.ImageTable
 					+ " WHERE GCH_IM_SRID='" + cf.selectedhomeid + "' and GCH_IM_Elevation='" + elev
 					+ "' order by GCH_IM_ImageOrder", null);
 			int chkrws = c11.getCount();
 			/***We call the centralized image selection part **/
 			maximumindb=chkrws;
 			if(maximumindb>=max_allowed)
 			{
 				cf.ShowToast("You can upload only "+max_allowed+" images", 1);
 			}else
 			{
 				c11.moveToFirst();
 				String[] selected_paht = new String[c11.getCount()];
 				
 				for(int i=0;i<c11.getCount();i++)
 				{
 					selected_paht[i]=cf.decode(c11.getString(c11.getColumnIndex("GCH_IM_ImageName"))); 
 					
 					c11.moveToNext();
 				}
 				t=0;
 				
 				Intent reptoit1 = new Intent();
 				Bundle b=new Bundle();
 				reptoit1.putExtra("Selectedvalue", selected_paht); /**Send the already selected image **/
 				reptoit1.putExtra("Maximumcount", maximumindb);/**Total count of image in the database **/
 				reptoit1.putExtra("Total_Maximumcount", max_allowed); /***Total count of image we need to accept**/
 				reptoit1.setClassName("com.idinspection","com.idinspection.Select_phots");
 				startActivityForResult(reptoit1,121); /** Call the Select image page in the idma application  image ***/
 				/***We call the centralized image selection part ends **/
 				/*
 				 String source = "<b><font color=#00FF33>Loading Images. Please wait..."+ "</font></b>";
 				 pd = ProgressDialog.show(photos.this, "",
 					Html.fromHtml(source), true);
 				 Thread thread = new Thread(photos.this);
 				 thread.start();*/
 				
 			}
 	   break;
		case R.id.home:
			cf.gohome();
			break;
		case R.id.save:
			cf.ShowToast("Saved succesfully", 1);
			nxtintent();
			break;
		case R.id.changeimageorder:
			change_image_order();
			break;
		
		}
	}

	protected void startCameraActivity() {
		String fileName = "temp.jpg";
		ContentValues values = new ContentValues();
		values.put(MediaStore.Images.Media.TITLE, fileName);
		CapturedImageURI = getContentResolver().insert(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, CapturedImageURI);
		startActivityForResult(intent, 0);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		String selectedImagePath = "", picname = "";
		try
		{
		if (t == 0) {

			if (resultCode == RESULT_OK) {
				if (requestCode == 0) {
					Uri selectedImageUri = data.getData();
					selectedImagePath = getPath(selectedImageUri);

					String[] bits = selectedImagePath.split("/");
					picname = bits[bits.length - 1];

				}
				else if(requestCode == 121)
				{
					/** we get the result from the Idma page for this elevation **/
				String[] value=	data.getExtras().getStringArray("Selected_array"); /**We pass the array of tje value from the IDAM Select page **/
					for(int i=0;i<value.length;i++ )
					{
						
	 	 					cf.gch_db.execSQL("INSERT INTO "
		 							+ cf.ImageTable
		 							+ " (GCH_IM_SRID,GCH_IM_Ques_Id,GCH_IM_Elevation,GCH_IM_ImageName,GCH_IM_Description,GCH_IM_Nameext,GCH_IM_CreatedOn,GCH_IM_ModifiedOn,GCH_IM_ImageOrder)"
		 							+ " VALUES ('" + cf.selectedhomeid + "','','"
		 							+ elev + "','"
		 							+ cf.encode(value[i]) + "','"
		 							+ cf.encode(name + (maximumindb+i+1))
		 							+ "','','"
		 							+ cf.datewithtime + "','" +cf.datewithtime + "','" + (maximumindb+1+i) + "')");
	 	 					
					}
					cf.ShowToast("Image saved successfully", 1);
					show_savedvalue();
					/** we get the result from the Idma page for this elevation Ends **/	
				}
			} else {
				selectedImagePath = "";
				// edbrowse.setText("");
			}
		} else if (t == 1) {
			switch (resultCode) {
			case 0:
				selectedImagePath = "";

				break;
			case -1:
				try {
					String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = managedQuery(CapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();
					String capturedImageFilePath = cursor
							.getString(column_index_data);
					selectedImagePath = capturedImageFilePath;
					display_taken_image(selectedImagePath);
										 show_savedvalue();
				} catch (Exception e) {
					System.out.println("my exception " + e);
				}

				break;

			}

		}
		}
		catch(Exception e)
		{
			System.out.println("no more issues ");
		}
		
	}

	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	public int getImageOrder(int Elevationtype, String GCH_IM_SRID) {
		int ImgOrder = 0;
		Cursor c11 = cf.gch_db.rawQuery("SELECT * FROM " + cf.ImageTable
				+ " WHERE GCH_IM_SRID='" + GCH_IM_SRID
				+ "' and GCH_IM_Elevation='" + Elevationtype
				+ "' order by GCH_IM_CreatedOn DESC", null);
		int imgrws = c11.getCount();
		if (imgrws == 0) {
			ImgOrder = imgrws + 1;
		} else {
			ImgOrder = imgrws + 1;

		}
		return ImgOrder;
	}
/** change the image order starts**/
	protected void change_image_order() {
 		// TODO Auto-generated method stub
		 final Dialog dialog;
		 Bitmap[] thumbnails1;
		 String[] arrPath1,str;
		 final int[] imageid;
		int[] SH_IM_ImageOrder;
		 ImageView[] chag_img;
		 final EditText[]  chan_ed;
 		Cursor d11 = cf.gch_db.rawQuery("SELECT * FROM " + cf.ImageTable
 				+ " WHERE GCH_IM_SRID='" + cf.selectedhomeid + "' and GCH_IM_Elevation='" + elev
 				+ "' order by GCH_IM_ImageOrder", null);
 		final int rws = d11.getCount();
 		if (rws == 0) {
 			cf.ShowToast("There is no Image, So please upload the Image.",1);
 		} else {
 			String Imagepath[] = new String[rws];

 		  EditText[] et = new EditText[rws];

 		    dialog = new Dialog(photos.this);
 			dialog.setContentView(R.layout.changeimageorder);
 			dialog.setTitle("Change Image Order");
 			dialog.setCancelable(false);
 			Button save=(Button) dialog.findViewById(R.id.Save);
 			Button close=(Button) dialog.findViewById(R.id.close);
 			
            TableLayout ln_main=(TableLayout) dialog.findViewById(R.id.changeorder);
 			try {

 				//this.count1 = d11.getCount();
 				thumbnails1 = new Bitmap[rws];
 				arrPath1 = new String[rws];
 				imageid = new int[rws];
 				SH_IM_ImageOrder = new int[rws];
 				str = new String[rws];
 				chag_img = new ImageView[rws];
 				chan_ed=new EditText[rws]; 
 				TableRow ln_sub=null;
 				d11.moveToFirst();

 				for (int i = 0; i < rws; i++) {
 					/**We create the new check box and edit box for the change image order **/
 					chag_img[i]=new ImageView(photos.this); 
 					TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
 					TableRow.LayoutParams lp2 = new TableRow.LayoutParams(100, 100);
 					lp.setMargins(5, 10, 5, 10);
 					chag_img[i].setLayoutParams(lp2);
 					chan_ed[i]=new EditText(photos.this);
 					//chan_ed[i].setMa))(2);
 					chan_ed[i].setInputType(InputType.TYPE_CLASS_NUMBER);
 					chan_ed[i].setLayoutParams(lp);
 					
 					InputFilter[] fArray = new InputFilter[1];
 					fArray[0] = new InputFilter.LengthFilter(etchangeordermaxLength);
 					chan_ed[i].setFilters(fArray);
 					LinearLayout li_tmp=new LinearLayout(photos.this);
 					li_tmp.setLayoutParams(lp);
 					
 					/**We create the new check box and edit box for the change image order ends  **/
 					imageid[i] = Integer.parseInt(d11.getString(0)
 							.toString());
 					SH_IM_ImageOrder[i] = d11.getInt(d11
 							.getColumnIndex("GCH_IM_ImageOrder"));
 					arrPath1[i] = cf.decode(d11.getString(d11
 							.getColumnIndex("GCH_IM_ImageName")));
 					Bitmap b = cf.ShrinkBitmap(arrPath1[i], 130, 130);
 					thumbnails1[i] = b;
 					if(i==0)
 					{
 						
 						LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
							ln_sub = new TableRow(photos.this);
							ln_sub.setLayoutParams(lp1);  
 					}
 					li_tmp.addView(chag_img[i]);
 					li_tmp.addView(chan_ed[i]);
 					ln_sub.addView(li_tmp);
 					chag_img[i].setImageBitmap(b);
 					chan_ed[i].setText(String.valueOf(SH_IM_ImageOrder[i]));
 					d11.moveToNext();
 					if((((i+1)%3)==0 || (i+1)==rws)&& ln_sub!=null )
						{
							ln_main.addView(ln_sub);
							LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
							ln_sub = new TableRow(photos.this);
							ln_sub.setLayoutParams(lp1);  
						}
 					
 				}
 				close.setOnClickListener(new OnClickListener() {

 					public void onClick(View v) {
 						// TODO Auto-generated method stub
 						dialog.setCancelable(true);
 						dialog.cancel();
 					}
 				});

 				save.setOnClickListener(new OnClickListener() {

 					public void onClick(View v) {
 						// TODO Auto-generated method stub
 						// dialog.cancel();
 						Boolean boo = true;
 						String temp[] = new String[rws];
 						try {
 							for (int i = 0; i < imageid.length; i++) {
 								temp[i] = chan_ed[i].getText().toString();
 								int myorder = 0;
 								if (temp[i] != null && !temp[i].equals("")) {
 									myorder = Integer.parseInt(temp[i]);
 									if (myorder <= imageid.length
 											&& myorder != 0) {

 									} else {
 										boo = false;
 									}

 								} else {
 									boo = false;
 								}

 							}
 							
 							int length = temp.length;
 							for (int i = 0; i < length; i++) {
 								for (int j = 0; j < length; j++) {
 									if (temp[i].equals(temp[j]) && i != j) {
 										boo = false;
 									}

 								}

 							}
 							if (!boo) {
 								cf.ShowToast("Please select different Image Order.",1);
 							} else {
 								for (int i = 0; i < imageid.length; i++) {
 									cf.gch_db.execSQL("UPDATE " + cf.ImageTable
 											+ " SET GCH_IM_ImageOrder='" + temp[i]
 											+ "' WHERE GCH_IM_ID='" + imageid[i]
 											+ "'");
 								}
 								dialog.setCancelable(true);
 								dialog.cancel();
 								cf.ShowToast("Image Order saved successfully.",1);
 								show_savedvalue();
 							}
 						}

 						catch (Exception e) {

 						}
 					}
 				});
                
 				dialog.show();

 				
 			} catch (Exception m) {
 				
 			}
 		}

 	}
/** change the image order Ends**/	
	public 	Bitmap ShrinkBitmap(String file, int width, int height) { try {
		BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
		bmpFactoryOptions.inJustDecodeBounds = true;
		Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
	
		int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
				/ (float) height);
		int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
				/ (float) width);
	
		if (heightRatio > 1 || widthRatio > 1) {
			if (heightRatio > widthRatio) {
				bmpFactoryOptions.inSampleSize = heightRatio;
			} else {
				bmpFactoryOptions.inSampleSize = widthRatio;
			}
		}
	
		bmpFactoryOptions.inJustDecodeBounds = false;
		bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
		return bitmap;
	} catch (Exception e) {
		return null;
	}
	
	}

	private void nxtintent() {
 		// TODO Auto-generated method stub
 	
 		if (elev == 1) {
 		  Intent myintent =new Intent(photos.this,photos.class);
 		  cf.putExtras(myintent);
 		  myintent.putExtra("type", 52);
 		 startActivity(myintent);
 		} else if (elev == 2) {
 			Intent myintent =new Intent(photos.this,photos.class);
	 		  cf.putExtras(myintent);
	 		 myintent.putExtra("type", 53);
	 		 startActivity(myintent);
 		} else if (elev == 3) {
 			Intent myintent =new Intent(photos.this,photos.class);
	 		  cf.putExtras(myintent);
	 		 myintent.putExtra("type", 54);
	 		startActivity(myintent);
 		} else if (elev == 4) {
 			Intent myintent =new Intent(photos.this,photos.class);
	 		  cf.putExtras(myintent);
	 		 myintent.putExtra("type", 56);
	 		startActivity(myintent);
 		} else if (elev == 5) {

 			Intent myintent =new Intent(photos.this,Feedback.class);
	 		  cf.putExtras(myintent);
	 		 startActivity(myintent);
	 		 
 		} else if (elev == 6) {
 			Intent myintent =new Intent(photos.this,photos.class);
	 		  cf.putExtras(myintent);
	 		 myintent.putExtra("type", 55);
	 		startActivity(myintent);
	 		 
		}
 	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
 		// replaces the default 'Back' button action
 		if (keyCode == KeyEvent.KEYCODE_BACK) {
 			if(cf.strschdate.equals("")){
 				cf.ShowToast("Layout navigation without scheduling not allowed. ", 1);
 			}else{
 			if (elev == 1) {
		 		  Intent myintent =new Intent(photos.this,RoofSection.class);
		 		  cf.putExtras(myintent);
		 		 
		 		startActivity(myintent);
		 		} else if (elev == 2) {
		 			Intent myintent =new Intent(photos.this,photos.class);
			 		  cf.putExtras(myintent);
			 		 myintent.putExtra("type", 51);
			 		startActivity(myintent);
		 		} else if (elev == 3) {
		 			Intent myintent =new Intent(photos.this,photos.class);
			 		  cf.putExtras(myintent);
			 		 myintent.putExtra("type", 52);
			 		startActivity(myintent);
		 		} else if (elev == 4) {
		 			Intent myintent =new Intent(photos.this,photos.class);
			 		  cf.putExtras(myintent);
			 		 myintent.putExtra("type", 53);
			 		startActivity(myintent);
		 		} else if (elev == 5) {
		 			Intent myintent =new Intent(photos.this,photos.class);
			 		  cf.putExtras(myintent);
			 		 myintent.putExtra("type", 56);
			 		 startActivity(myintent);
			 		 
		 		}
		 		 else if (elev == 6) {
			 			Intent myintent =new Intent(photos.this,photos.class);
				 		  cf.putExtras(myintent);
				 		 myintent.putExtra("type", 54);
				 		 startActivity(myintent);
				 		 
			 		}
 			}
 			return true;
 		}
 		return super.onKeyDown(keyCode, event);
 	}
	
	public void display_taken_image(final String slectimage)
	{
		 System.gc();
		 globalvar=0;
		
		BitmapFactory.Options o = new BitmapFactory.Options();
 		o.inJustDecodeBounds = true;
	
 		final Dialog dialog1 = new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alert);
		
		/** get the help and update relative layou and set the visbilitys for the respective relative layout */
		LinearLayout Re=(LinearLayout) dialog1.findViewById(R.id.maintable);
		Re.setVisibility(View.GONE);
		LinearLayout Reup=(LinearLayout) dialog1.findViewById(R.id.updateimage);
		Reup.setVisibility(View.GONE);
		LinearLayout camerapic=(LinearLayout) dialog1.findViewById(R.id.cameraimage);
		camerapic.setVisibility(View.VISIBLE);
		TextView tvcamhelp = (TextView)dialog1.findViewById(R.id.camtxthelp);
		tvcamhelp.setText("Save Picture");
		tvcamhelp.setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
		 ((RelativeLayout)dialog1.findViewById(R.id.cam_photo_update)).setVisibility(View.VISIBLE);
		((RelativeLayout)dialog1.findViewById(R.id.camaddcaption)).setVisibility(View.GONE);
		((TextView) dialog1.findViewById(R.id.elevtxt)).setVisibility(View.GONE);
		((Spinner) dialog1.findViewById(R.id.cameraelev)).setVisibility(View.GONE);
		/** get the help and update relative layou and set the visbilitys for the respective relative layout */
		
		final ImageView upd_img = (ImageView) dialog1.findViewById(R.id.cameraimg);
		Button btn_helpclose = (Button) dialog1.findViewById(R.id.camhelpclose);
		//((Button) dialog1.findViewById(R.id.camsave)).setVisibility(View.GONE);
		
		final Button btn_save = (Button) dialog1.findViewById(R.id.camsave);
		
		Button rotatecamleft = (Button) dialog1.findViewById(R.id.rotatecamimgleft);
		Button rotatecamright = (Button) dialog1.findViewById(R.id.rotatecamright);
		
		try {
				BitmapFactory.decodeStream(new FileInputStream(slectimage),
						null, o);
				final int REQUIRED_SIZE = 400;
 			int width_tmp = o.outWidth, height_tmp = o.outHeight;
 			int scale = 1;
 			while (true) {
 				if (width_tmp / 2 < REQUIRED_SIZE
 						|| height_tmp / 2 < REQUIRED_SIZE)
 					break;
 				width_tmp /= 2;
 				height_tmp /= 2;
 				scale *= 2;
 				BitmapFactory.Options o2 = new BitmapFactory.Options();
	 			o2.inSampleSize = scale;
	 			Bitmap bitmap = null;
	 	    	bitmap = BitmapFactory.decodeStream(new FileInputStream(
	 	    			slectimage), null, o2);
	 	       BitmapDrawable bmd = new BitmapDrawable(bitmap);
 			   upd_img.setImageDrawable(bmd);
 			}
		

	}
		catch(Exception e)
		{
			
		}
		btn_save.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	
            	if(globalvar==0)
        		{

            	globalvar=1;
            	btn_save.setVisibility(v.GONE);
            	String[] bits = slectimage.split("/");
        		String picname = bits[bits.length - 1];
        		Cursor c15 = cf.gch_db.rawQuery("SELECT * FROM "
        				+ cf.ImageTable + " WHERE GCH_IM_SRID='"
        				+ cf.selectedhomeid + "' and GCH_IM_Elevation='"
        				+ elev + "'", null);
        		int count_tot = c15.getCount();
        		int ImgOrder = getImageOrder(elev, cf.selectedhomeid);
        		try {
        			cf.gch_db.execSQL("INSERT INTO "
        					+ cf.ImageTable
        					+ " (GCH_IM_SRID,GCH_IM_Ques_Id,GCH_IM_Elevation,GCH_IM_ImageName,GCH_IM_Description,GCH_IM_Nameext,GCH_IM_CreatedOn,GCH_IM_ModifiedOn,GCH_IM_ImageOrder)"
        					+ " VALUES ('" + cf.selectedhomeid + "','','"
        					+ elev + "','" + cf.encode(slectimage)
        					+ "','" + cf.encode(name + (count_tot + 1))
        					+ "','" + cf.encode(picname) + "','"
        					+ cf.datewithtime + "','" + cf.datewithtime
        					+ "','" + ImgOrder + "')");
        			//dialog1.dismiss();
        			
        			/**Save the rotated value in to the external stroage place **/
					if(currnet_rotated>0)
					{ 

						try
						{
							/**Create the new image with the rotation **/
							String current=MediaStore.Images.Media.insertImage(getContentResolver(), rotated_b, "My bitmap", "My rotated bitmap");
							 ContentValues values = new ContentValues();
							  values.put(MediaStore.Images.Media.ORIENTATION, 0);
							  photos.this.getContentResolver().update(Uri.parse(current), values, MediaStore.Images.Media.DATA+ "=?", new String[] { current } );
							
							
							if(current!=null)
							{
							String path=getPath(Uri.parse(current));
							File fout = new File(slectimage);
							fout.delete();
							/** delete the selected image **/
							File fin = new File(path);
							/** move the newly created image in the slected image pathe ***/
							fin.renameTo(new File(slectimage));
							
							
						}
						} catch(Exception e)
						{
							System.out.println("Error occure while rotate the image "+e.getMessage());
						}
						
					}
				
				/**Save the rotated value in to the external stroage place ends **/
					show_savedvalue();
					cf.ShowToast("Selected image saved successfully", 1);
					dialog1.setCancelable(true);
	            	dialog1.dismiss();
        		} catch (Exception e) {
        			System.out.println("e= " + e.getMessage());
        		}
        		}
            }
        });
		btn_helpclose.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	dialog1.setCancelable(true);
            	dialog1.dismiss();
            }
		});
		rotatecamleft.setOnClickListener(new OnClickListener() {  
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 System.gc();
				 currnet_rotated-=90;
				 System.out.println("Rotating the image right  bfr"+currnet_rotated);
					if(currnet_rotated<0)
					{
						currnet_rotated=270;
					}	
					System.out.println("Rotating the image right  afr"+currnet_rotated);
				Bitmap myImg;
				try {
					myImg = BitmapFactory.decodeStream(new FileInputStream(slectimage));
					Matrix matrix =new Matrix();
					matrix.reset();
					matrix.setRotate(currnet_rotated);
					rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),matrix, true);
					System.gc();
					upd_img.setImageBitmap(rotated_b);

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch(Exception e)
				{
					System.out.println("rota eeee "+e.getMessage());
				}

				catch (OutOfMemoryError e) {
					System.out.println("comes in to out ot mem exception");
					System.gc();
					try {
						myImg=null;
						System.gc();
						Matrix matrix =new Matrix();
						matrix.reset();
						matrix.postRotate(currnet_rotated);
						myImg= cf.ShrinkBitmap(slectimage, 800, 800);
						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
						System.gc();
						upd_img.setImageBitmap(rotated_b); 

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 catch (OutOfMemoryError e1) {
							// TODO Auto-generated catch block
						 cf.ShowToast("You can not rotate this image. please Restart your application then try Rotate",1);
					}
				}
			}
		});
		rotatecamright.setOnClickListener(new OnClickListener() {  
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 System.gc();
				currnet_rotated+=90;
				System.out.println("Rotating the image right  bfr"+currnet_rotated);
				if(currnet_rotated>=360)
				{
					currnet_rotated=0;
				}
				System.out.println("Rotating the image right  afr"+currnet_rotated);
				Bitmap myImg;
				try {
					myImg = BitmapFactory.decodeStream(new FileInputStream(slectimage));
					Matrix matrix =new Matrix();
					matrix.reset();
					matrix.setRotate(currnet_rotated);
					rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),matrix, true);
					System.gc();
					upd_img.setImageBitmap(rotated_b);

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch(Exception e)
				{
					System.out.println("rota eeee "+e.getMessage());
				}

				catch (OutOfMemoryError e) {
					System.out.println("comes in to out ot mem exception");
					System.gc();
					try {
						myImg=null;
						System.gc();
						Matrix matrix =new Matrix();
						matrix.reset();
						matrix.postRotate(currnet_rotated);
						myImg= cf.ShrinkBitmap(slectimage, 800, 800);
						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
						System.gc();
						upd_img.setImageBitmap(rotated_b); 

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 catch (OutOfMemoryError e1) {
							// TODO Auto-generated catch block
						 cf.ShowToast("You can not rotate this image. Image size is too large.",1);
					}
				}
			}
		});
		dialog1.setCancelable(false);
		dialog1.show();
	}
	
}
