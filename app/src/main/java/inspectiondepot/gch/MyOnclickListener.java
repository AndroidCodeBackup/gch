package inspectiondepot.gch;




import java.util.Map;

import inspectiondepot.gch.R;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MyOnclickListener extends LinearLayout {
	private Context context = null;
	Context activityname;
	private int app;
	private int sub;
	public Intent myintent;
	private CommonFunctions cf;
	private TextView tvphoto;
	public static int count =0;
	/** button declaration for main menus starts **/
	
	
	
	
	Button btn_general,btn_buildinginfo,btn_photo,btn_feedback,btn_hazards,btn_roof,
           btn_map,btn_submit,gen_btncall,gen_btnagnt,gen_btncloud,gen_btnadditional;
	/** button declaration for main menu ends here **/
	
	/** Button declaration for general info sub menu starts here**/
	Button gen_btnschedule,gen_btnph,gen_btninspector;
	/** Button declaration for general info sub menu ends here**/
	
	/** Button declaration for photos sub menu starts here**/
	Button ph_sub1,ph_sub2,ph_sub3,ph_sub4,ph_sub5,ph_sub6;
	/** Button declaration for photos sub menu ends here**/
	
	ImageView cameraimg;
	
	public MyOnclickListener(Context context, int appname,
			int sub,CommonFunctions cf) {
		super(context);
		this.context = context; /** actiivty name **/
		this.app = appname;  /** class name **/
		this.sub = sub; /** in order to identify main/sub menu **/
		this.cf = cf;
		create();
	}

	public MyOnclickListener(Context context, AttributeSet attrs, int appname,
			 int sub,CommonFunctions cf) {
		super(context, attrs);
		this.context = context;
		this.app = appname;
		this.sub = sub;
		this.cf = cf;
	    create();
	}

	private void create() {  
		LayoutInflater layoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		/** menu **/
		if(this.sub==0){
			layoutInflater.inflate(R.layout.menuclick, this, true);
			tvphoto = (TextView)findViewById(R.id.txtphoto);
			cameraimg = (ImageView)findViewById(R.id.camera);
			btn_general = (Button) findViewById(R.id.generalinfo);
			btn_buildinginfo = (Button) findViewById(R.id.buildinginfo);
			btn_hazards = (Button)findViewById(R.id.hazards);
			btn_roof = (Button)findViewById(R.id.roof_section);
			btn_photo = (Button) findViewById(R.id.photo);
			btn_feedback = (Button) findViewById(R.id.feedback);
			btn_map = (Button) findViewById(R.id.map);
			btn_submit = (Button) findViewById(R.id.submit);
			
			TextView geninfotxt = (TextView) findViewById(R.id.topgentext);
			cf.Create_Table(3);cf.getInspectorId();
			try {
				Cursor c2 = cf.gch_db.rawQuery("SELECT * FROM "
						+ cf.policyholder + " WHERE GCH_PH_SRID='" + cf.encode(cf.selectedhomeid)
						+ "' and GCH_PH_InspectorId='" + cf.encode(cf.Insp_id) + "'", null);
				c2.moveToFirst();
				if (c2 != null) {
					String sch=(cf.decode(c2.getString(c2.getColumnIndex("GCH_Schedule_ScheduledDate"))).trim().equals(""))? "N/A":cf.decode(c2.getString(c2.getColumnIndex("GCH_Schedule_ScheduledDate")));
					String p_no=(cf.decode(c2.getString(c2.getColumnIndex("GCH_PH_Policyno"))).trim().equals(""))? "N/A":cf.decode(c2.getString(c2.getColumnIndex("GCH_PH_Policyno")));
						cf.toptextvalue="<font color=#bddb00> Policyholder Name : </font>" +
							"<b><font color=#ffffff> "+cf.decode(c2.getString(c2.getColumnIndex("GCH_PH_FirstName")))+" "+ cf.decode(c2.getString(c2.getColumnIndex("GCH_PH_LastName")))+" </font></b>"+"\t"+
							"<font color=#bddb00> Policy Number : </font>" +
							"<b><font color=#ffffff> "+p_no+" </font></b>"+"\t"+
							"<font color=#bddb00> Inspection Date : </font>" +
							"<b><font color=#ffffff> "+sch+"  </font></b>";
					cf.strschdate=cf.decode(c2.getString(c2.getColumnIndex("GCH_Schedule_ScheduledDate")));
					geninfotxt.setText(Html.fromHtml(cf.toptextvalue));
					
				}
			}
			catch(Exception e)
			{
			    cf.strerrorlog="Retrieving policy number ";
				cf.Error_LogFile_Creation(cf.strerrorlog+" "+" at "+ cf.con.getClass().getName().toString()+" "+" in the stage of Inspector Login Table Creation at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				
			}
			switch(this.app)
			{
			    case 56:
				  cameraimg.setVisibility(GONE);
				  tvphoto.setVisibility(GONE);
				 break;
			    case 55:
					  cameraimg.setVisibility(GONE);
					  tvphoto.setVisibility(GONE);
					 break;
			    case 54:
					  cameraimg.setVisibility(GONE);
					  tvphoto.setVisibility(GONE);
					 break;
			    case 53:
					  cameraimg.setVisibility(GONE);
					  tvphoto.setVisibility(GONE);
					 break;
			    case 52:
					  cameraimg.setVisibility(GONE);
					  tvphoto.setVisibility(GONE);
					 break;
			    case 51:
					  cameraimg.setVisibility(GONE);
					  tvphoto.setVisibility(GONE);
					 break;
			    case 8:
					  cameraimg.setVisibility(GONE);
					  tvphoto.setVisibility(GONE);
					 break;
			    case 7:
					  cameraimg.setVisibility(GONE);
					  tvphoto.setVisibility(GONE);
					 break;
			    case 6:
					  cameraimg.setVisibility(GONE);
					  tvphoto.setVisibility(GONE);
					 break;
			    case 5:
					  cameraimg.setVisibility(GONE);
					  tvphoto.setVisibility(GONE);
					 break;
			    case 9:
					  cameraimg.setVisibility(GONE);
					  tvphoto.setVisibility(GONE);
					 break;
			}
			/** click event for main menu buttons **/
			cameraimg.setOnClickListener(new clicker());
			btn_general.setOnClickListener(new clicker());
			btn_buildinginfo.setOnClickListener(new clicker());
			btn_hazards.setOnClickListener(new clicker());
			btn_roof.setOnClickListener(new clicker());
			btn_photo.setOnClickListener(new clicker());
			btn_feedback.setOnClickListener(new clicker());
			btn_map.setOnClickListener(new clicker());
			btn_submit.setOnClickListener(new clicker());
			
			/** click event for main menu ends  buttons **/
		}
		else if(this.sub==1)/** sub menu **/
		{
			if(this.app == 11 || this.app==12 || this.app == 13 || this.app==14 || this.app==15 || this.app==16 || this.app==17) /** General Information sub menu **/
			{
				layoutInflater.inflate(R.layout.generalsubmenu, this, true);
				RelativeLayout generalsub1 =(RelativeLayout) findViewById(R.id.generalInfo_submenu);
				RelativeLayout generalsub2 =(RelativeLayout) findViewById(R.id.photos_submenu);
				
				generalsub2.setVisibility(View.GONE);
				generalsub1.setVisibility(View.VISIBLE);

				gen_btnschedule = (Button) findViewById(R.id.scheduleinspection);
				gen_btnph = (Button) findViewById(R.id.phinformation);
				gen_btninspector = (Button) findViewById(R.id.inspectorinformation);
				gen_btncall = (Button) findViewById(R.id.callattempt);
				gen_btnagnt = (Button) findViewById(R.id.agent);
				gen_btncloud = (Button) findViewById(R.id.cloud);
				gen_btnadditional = (Button) findViewById(R.id.additional);
				
				try{Cursor cur = cf.SelectTablefunction(cf.Additional_table,
						" where GCH_AD_INSPID = '" + cf.Insp_id
								+ "' and GCH_AD_SRID='"+cf.selectedhomeid+"'");
				if(cur.getCount()>0)
				{
					cur.moveToFirst();
					if(cur.getString(cur.getColumnIndex("GCH_AD_IsRecord")).equals("true"))
					{
						gen_btnadditional.setVisibility(cf.show.VISIBLE);
					}
					else
					{
						gen_btnadditional.setVisibility(cf.show.GONE);
					}
				}
				else
				{
					gen_btnadditional.setVisibility(cf.show.GONE);
				}
				}
				catch(Exception e)
				{
					cf.strerrorlog="Error in retrieving value for additional table";
					cf.Error_LogFile_Creation(cf.strerrorlog+" "+" at "+ cf.con.getClass().getName().toString()+" "+" in the stage of Inspector Login Table Creation at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
					
				}
				gen_btnschedule.setOnClickListener(new clicker());
				gen_btnph.setOnClickListener(new clicker());
				gen_btninspector.setOnClickListener(new clicker());
				gen_btncall.setOnClickListener(new clicker());
				gen_btnagnt.setOnClickListener(new clicker());
				gen_btncloud.setOnClickListener(new clicker());
				gen_btnadditional.setOnClickListener(new clicker());
			}
			
			else if(this.app == 51 || this.app == 52 || this.app == 53 || this.app == 54 || this.app == 55 || this.app == 56 )/** photos  sub menu **/
			{
				layoutInflater.inflate(R.layout.generalsubmenu, this, true);
				RelativeLayout generalsub1 =(RelativeLayout) findViewById(R.id.generalInfo_submenu);
				RelativeLayout generalsub2 =(RelativeLayout) findViewById(R.id.photos_submenu);
				
				generalsub1.setVisibility(View.GONE);
				generalsub2.setVisibility(View.VISIBLE);
				ph_sub1=(Button) findViewById(R.id.ph_sub_1);
				ph_sub2=(Button) findViewById(R.id.ph_sub_2);
				ph_sub3=(Button) findViewById(R.id.ph_sub_3);
				ph_sub4=(Button) findViewById(R.id.ph_sub_4);
				ph_sub5=(Button) findViewById(R.id.ph_sub_5);
				ph_sub6=(Button) findViewById(R.id.ph_sub_6);
				
				// setting the listener for the submenus
				ph_sub1.setOnClickListener(new clicker());
				ph_sub2.setOnClickListener(new clicker());
				ph_sub3.setOnClickListener(new clicker());
				ph_sub4.setOnClickListener(new clicker());
				ph_sub5.setOnClickListener(new clicker());
				ph_sub6.setOnClickListener(new clicker());
			}

		}
		switch(this.app)
		{
		   case 1:/** Genearl Information button to be selected**/
			   btn_general.setBackgroundResource(R.drawable.general_information);
			   break;
		   case 11:/** Schedule information button to be selected**/
			   gen_btnschedule.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 12:/** Policyholder information button to be selected**/
			   gen_btnph.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		  case 13:/** Inspector information button to be selected**/
			   gen_btninspector.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;   
		  case 14:/** Callattempt button to be selected**/
			   gen_btncall.setBackgroundResource(R.drawable.subbackrepeatovr);
			  break;
		  case 15:/** Agent button to be selected**/
			   gen_btnagnt.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 16:/** cloud button to be selected**/
			   gen_btncloud.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 17:/** additional button to be selected**/
			   gen_btnadditional.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		  case 2:/** Building Information button to be selected**/
			   btn_buildinginfo.setBackgroundResource(R.drawable.building_information);
			  break;
		  case 3:
			  btn_hazards.setBackgroundResource(R.drawable.generalconditionovr);
			  break;
		  case 4:
			  btn_roof.setBackgroundResource(R.drawable.roofsection);
			  break;
		   case 5:/** photo button selected **/
			   btn_photo.setBackgroundResource(R.drawable.photographs);
			   break;
		   case 51:
			   if(this.sub==1)
			   {
				   ph_sub1.setBackgroundResource(R.drawable.subbackrepeatovr);
				   ph_sub1.setTextColor(Color.BLACK);
			   }
			   if(this.sub==0)
			   {
				   btn_photo.setBackgroundResource(R.drawable.backrepeatnor);
			   	   btn_photo.setTextColor(Color.BLACK);
			   }
			 
		   break;
		   case 52:
			   if(this.sub==1)
			   {
				   ph_sub2.setBackgroundResource(R.drawable.subbackrepeatovr);
				   ph_sub2.setTextColor(Color.BLACK);
			   }
			   if(this.sub==0)
			   {
				   btn_photo.setBackgroundResource(R.drawable.backrepeatnor);
				   btn_photo.setTextColor(Color.BLACK);
			   }
		   break;
		   case 53:
			   if(this.sub==1)
			   {
				   ph_sub3.setBackgroundResource(R.drawable.subbackrepeatovr);
				   ph_sub3.setTextColor(Color.BLACK);
			   }
			   if(this.sub==0)
			   {
				   btn_photo.setBackgroundResource(R.drawable.backrepeatnor);
				   btn_photo.setTextColor(Color.BLACK);
			   }
		   break;
		   case 54:
			   if(this.sub==1)
			   {
				   ph_sub4.setBackgroundResource(R.drawable.subbackrepeatovr);
				   ph_sub4.setTextColor(Color.BLACK);
			   }
			   if(this.sub==0)
			   {
				   btn_photo.setBackgroundResource(R.drawable.backrepeatnor);
				   btn_photo.setTextColor(Color.BLACK);
			   }
		   break;
		   case 55:
			   if(this.sub==1)
			   {
				   ph_sub5.setBackgroundResource(R.drawable.subbackrepeatovr);
				   ph_sub5.setTextColor(Color.BLACK);
			   }
			   if(this.sub==0)
			   {
				   btn_photo.setBackgroundResource(R.drawable.backrepeatnor);
				   btn_photo.setTextColor(Color.BLACK);
			   }
		   break;
		   case 56:
			   if(this.sub==1)
			   {
				   ph_sub6.setBackgroundResource(R.drawable.subbackrepeatovr);
				   ph_sub6.setTextColor(Color.BLACK);
			   }
			   if(this.sub==0)
			   {
				   btn_photo.setBackgroundResource(R.drawable.backrepeatnor);
				   btn_photo.setTextColor(Color.BLACK);
			   }
		   break;
		   case 6:/** Feedback button selected **/
			   btn_feedback.setBackgroundResource(R.drawable.feedback_section);
			   break;		  
		   case 7:/** map button selected **/
			   btn_map.setBackgroundResource(R.drawable.map);
			    break;
           case 9:/** submit button selected **/
			  btn_submit.setBackgroundResource(R.drawable.submit);
			  break;
			      
		}

	}  
     
	class clicker implements OnClickListener {

		public void onClick(View v) {
			// TODO Auto-generated method stub

			switch (v.getId()) {
			case R.id.generalinfo: /** calling General Inormation tab **/
				myintent = new Intent(context,PolicyHolder.class);
				cf.putExtras(myintent);
				if(count==0)
				{
					count++;
					call_dummy();
				
				}
				context.startActivity(myintent);
				break;
			case R.id.scheduleinspection:/** calling schedule tab **/
				myintent = new Intent(context,Schedule.class);
				cf.putExtras(myintent);
				if(count==0)
				{
					count++;
					call_dummy();
				
				}
				context.startActivity(myintent);
				break;
			case R.id.phinformation:/** calling ph tab **/
				System.out.println("oncreate policy click");
				myintent = new Intent(context,PolicyHolder.class);
				cf.putExtras(myintent);
				if(count==0)
				{
					count++;
					call_dummy();
				
				}
				context.startActivity(myintent);
				System.out.println("oncreate content");
				break;
			case R.id.inspectorinformation:/** calling inspector info tab **/
				myintent = new Intent(context,InspectorInfo.class);
				cf.putExtras(myintent);
				if(count==0)
				{
					count++;
					call_dummy();
				
				}
				context.startActivity(myintent);
				break; 
			case R.id.callattempt:/** calling callattempt  tab **/
				myintent = new Intent(context,Callattempt.class);
				cf.putExtras(myintent);
				if(count==0)
				{
					count++;
					call_dummy();
				
				}
				context.startActivity(myintent);
				break;
			case R.id.agent:/** calling agent  tab **/
				myintent = new Intent(context,AgentInfo.class);
				cf.putExtras(myintent);
				if(count==0)
				{
					count++;
					call_dummy();
				
				}
				context.startActivity(myintent);
				break;
			case R.id.cloud:/** calling cloud  tab **/
				myintent = new Intent(context,CloudInfo.class);
				cf.putExtras(myintent);
				if(count==0)
				{
					count++;
					call_dummy();
				
				}
				context.startActivity(myintent);
				break;
			case R.id.additional:/** calling additional  tab **/
				myintent = new Intent(context,AdditionalInfo.class);
				cf.putExtras(myintent);
				if(count==0)
				{
					count++;
					call_dummy();
				
				}
				context.startActivity(myintent);
				break;
			 case R.id.buildinginfo:// calling the building information
				 System.out.println("bulding "+cf.strschdate);
				   if(cf.strschdate.equals(""))
					{
						cf.ShowToast("Editing information without scheduling not allowed. ", 1);
					}else{myintent = new Intent(context,BuildingInformation.class);
					cf.putExtras(myintent);
					if(count==0)
					{
						count++;
						call_dummy();
					
					}
					context.startActivity(myintent);}
					break;
			 case R.id.hazards: // calling the hazard 
				 if(cf.strschdate.equals(""))
					{
						cf.ShowToast("Editing information without scheduling not allowed. ", 1);
					}else{myintent = new Intent(context,GeneralHazards.class);
					cf.putExtras(myintent);
					if(count==0)
					{
						count++;
						call_dummy();
					
					}
					context.startActivity(myintent);}
				 break;
			 case R.id.roof_section: // calling the hazard 
				 if(cf.strschdate.equals(""))
					{
						cf.ShowToast("Editing information without scheduling not allowed. ", 1);
					}else{myintent = new Intent(context,RoofSection.class);
					cf.putExtras(myintent);
					if(count==0)
					{
						count++;
						call_dummy();
					
					}
					context.startActivity(myintent);}
				 break;
			 case R.id.map:// calling the map
				 
						if(count==0)
						{
							count++;
							call_dummy();
						
						}
						myintent = new Intent(context,Maps.class); 
						cf.putExtras(myintent);
						context.startActivity(myintent);
							
				   // cf.netalert("MAPD");
				
				 	break;	
			 case R.id.submit:// calling the submit
				if(cf.strschdate.equals(""))
					{
						cf.ShowToast("Editing information without scheduling not allowed. ", 1);
					}else{
					myintent = new Intent(context,Submit.class); 
					cf.putExtras(myintent);
					if(count==0)
					{
						count++;
						call_dummy();
					
					}
					context.startActivity(myintent);}
					break;
			 case R.id.feedback:// calling the feddback
				   if(cf.strschdate.equals(""))
					{
						cf.ShowToast("Editing information without scheduling not allowed. ", 1);
					}else{
					myintent = new Intent(context,Feedback.class);
					cf.putExtras(myintent);
					if(count==0)
					{
						count++;
						call_dummy();
					
					}
					context.startActivity(myintent);
					}
					break;
			 case R.id.photo: // set the clicker function for the photos
				 if(cf.strschdate.equals(""))
					{
						cf.ShowToast("Editing information without scheduling not allowed. ", 1);
					}else{	myintent = new Intent(context,photos.class); 
				 	cf.putExtras(myintent);;
				 	myintent.putExtra("type",51);
				 	if(count==0)
					{
						count++;
						call_dummy();
					
					}
					context.startActivity(myintent);}
				 break;
			 case R.id.ph_sub_1:  // set the clicker function for the photos submenu 1
				 if(cf.strschdate.equals(""))
					{
						cf.ShowToast("Editing information without scheduling not allowed. ", 1);
					}else{myintent = new Intent(context,photos.class); 
				 	cf.putExtras(myintent);
				 	
				 	myintent.putExtra("type", 51);
				 	if(count==0)
					{
						count++;
						call_dummy();
					
					}
					context.startActivity(myintent);}
				 break;
			 case R.id.ph_sub_2: // set the clicker function for the photos submenu 2
				 if(cf.strschdate.equals(""))
					{
						cf.ShowToast("Editing information without scheduling not allowed. ", 1);
					}else{	myintent = new Intent(context,photos.class); 
				 	cf.putExtras(myintent);
					myintent.putExtra("type", 52);
					if(count==0)
					{
						count++;
						call_dummy();
					
					}
					context.startActivity(myintent);}
				 break;
			 case R.id.ph_sub_3: // set the clicker function for the photos submenu 3
				 if(cf.strschdate.equals(""))
					{
						cf.ShowToast("Editing information without scheduling not allowed. ", 1);
					}else{myintent = new Intent(context,photos.class); 
				 	cf.putExtras(myintent);
					myintent.putExtra("type", 53);
					if(count==0)
					{
						count++;
						call_dummy();
					
					}
					context.startActivity(myintent);}
				 break;
			 case R.id.ph_sub_4: // set the clicker function for the photos submenu 4
				 if(cf.strschdate.equals(""))
					{
						cf.ShowToast("Editing information without scheduling not allowed. ", 1);
					}else{myintent = new Intent(context,photos.class); 
				 	cf.putExtras(myintent);
					myintent.putExtra("type", 54);
					if(count==0)
					{
						count++;
						call_dummy();
					
					}
					context.startActivity(myintent);}
				 break;
		         		 
			 case R.id.ph_sub_5: // set the clicker function for the photos submenu 5
				 if(cf.strschdate.equals(""))
					{
						cf.ShowToast("Editing information without scheduling not allowed. ", 1);
					}else{	myintent = new Intent(context,photos.class); 
				 	cf.putExtras(myintent);
					myintent.putExtra("type", 55);
					if(count==0)
					{
						count++;
						call_dummy();
					
					}
					context.startActivity(myintent);}
				 break;
			 case R.id.ph_sub_6: // set the clicker function for the photos submenu 5
				 if(cf.strschdate.equals(""))
					{
						cf.ShowToast("Editing information without scheduling not allowed. ", 1);
					}else{	myintent = new Intent(context,photos.class); 
				 	cf.putExtras(myintent);
					myintent.putExtra("type", 56);
					if(count==0)
					{
						count++;
						call_dummy();
					
					}
					context.startActivity(myintent);}
				 break;
			case R.id.camera:
				 if(cf.strschdate.equals(""))
					{
						cf.ShowToast("Editing information without scheduling not allowed. ", 1);
					}else{
						if(count==0)
						{
							count++;
							call_dummy();
						
						}
						cf.startCameraActivity();}
				 break;
			
   
			
			}
		}
	}
	public void call_dummy() {
		// TODO Auto-generated method stub
		System.out.println("Called the dummy activity ="+count);
		
		Intent Dummy = new Intent(context,dummy.class);
		Dummy.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(Dummy);
	}
}
     