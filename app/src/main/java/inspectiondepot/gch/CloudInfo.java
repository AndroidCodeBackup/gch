package inspectiondepot.gch;


import inspectiondepot.gch.R;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;

public class CloudInfo extends Activity{
	CommonFunctions cf;
	TextView[] cloud = new TextView[69];
	TextView additionaltxt;
	TableLayout tbladdtinfo;

	 public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.selectedhomeid = extras.getString("homeid");
				cf.onlinspectionid = extras.getString("InspectionType");
			    cf.onlstatus = extras.getString("status");
    	     }
			setContentView(R.layout.cloud);
			cf.getDeviceDimensions();
       	    LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
       	    mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 1, 0,cf));
		    LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.generalsubmenu);
		    submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 16, 1,cf));
		    cf.getInspectorId();
		    
		    cloud[2] = (TextView) findViewById(R.id.assignmentId);
			cloud[3] = (TextView) findViewById(R.id.assignmentMasterId);
			cloud[4] = (TextView) findViewById(R.id.inspectionPolicy_Id);
			cloud[5] = (TextView) findViewById(R.id.priorAssignmentId);
			cloud[6] = (TextView) findViewById(R.id.inspectionType);
			cloud[7] = (TextView) findViewById(R.id.assignmentType);
			cloud[8] = (TextView) findViewById(R.id.policyId);
			cloud[9] = (TextView) findViewById(R.id.policyVersion);
			cloud[10] = (TextView) findViewById(R.id.policyEndorsement);
			cloud[11] = (TextView) findViewById(R.id.policyEffectiveDate);
			cloud[12] = (TextView) findViewById(R.id.policyForm);
			cloud[13] = (TextView) findViewById(R.id.policySystem);
			cloud[14] = (TextView) findViewById(R.id.lob);
			cloud[15] = (TextView) findViewById(R.id.structureCount);
			cloud[16] = (TextView) findViewById(R.id.structureNumber);
			cloud[17] = (TextView) findViewById(R.id.structureDescription);
			cloud[18] = (TextView) findViewById(R.id.propertyAddress);
			cloud[19] = (TextView) findViewById(R.id.propertyAddress2);
			cloud[20] = (TextView) findViewById(R.id.propertyCity);
			cloud[21] = (TextView) findViewById(R.id.propertyState);
			cloud[22] = (TextView) findViewById(R.id.propertyCounty);
			cloud[23] = (TextView) findViewById(R.id.propertyZip);
			cloud[24] = (TextView) findViewById(R.id.insuredFirstName);
			cloud[25] = (TextView) findViewById(R.id.insuredLastName);
			cloud[26] = (TextView) findViewById(R.id.insuredHomePhone);
			cloud[27] = (TextView) findViewById(R.id.insuredWorkPhone);
			cloud[28] = (TextView) findViewById(R.id.insuredAlternatePhone);
			cloud[29] = (TextView) findViewById(R.id.insuredEmail);
			cloud[30] = (TextView) findViewById(R.id.insuredMailingAddress);
			cloud[31] = (TextView) findViewById(R.id.insuredMailingAddress2);
			cloud[32] = (TextView) findViewById(R.id.insuredMailingCity);
			cloud[33] = (TextView) findViewById(R.id.insuredMailingState);
			cloud[34] = (TextView) findViewById(R.id.insuredMailingZip);
			cloud[35] = (TextView) findViewById(R.id.agencyID);
			cloud[36] = (TextView) findViewById(R.id.agencyName);
			cloud[37] = (TextView) findViewById(R.id.agencyPhone);
			cloud[38] = (TextView) findViewById(R.id.agencyFax);
			cloud[39] = (TextView) findViewById(R.id.agencyEmail);
			cloud[40] = (TextView) findViewById(R.id.agencyPrincipalFirstName);
			cloud[41] = (TextView) findViewById(R.id.agencyPrincipalLastName);
			cloud[42] = (TextView) findViewById(R.id.agencyPrincipalEmail);
			cloud[43] = (TextView) findViewById(R.id.agencyFEIN);
			cloud[44] = (TextView) findViewById(R.id.agencyMailingAddress);
			cloud[45] = (TextView) findViewById(R.id.agencyMailingAddress2);
			cloud[46] = (TextView) findViewById(R.id.agencyMailingCity);
			cloud[47] = (TextView) findViewById(R.id.agencyMailingState);
			cloud[48] = (TextView) findViewById(R.id.agencyMailingZip);
			cloud[49] = (TextView) findViewById(R.id.agentID);
			cloud[50] = (TextView) findViewById(R.id.agentFirstName);
			cloud[51] = (TextView) findViewById(R.id.agentLastName);
			cloud[52] = (TextView) findViewById(R.id.agentEmail);
			cloud[53] = (TextView) findViewById(R.id.previousInspectionDate);
			cloud[54] = (TextView) findViewById(R.id.previousInspectionCompany);
			cloud[55] = (TextView) findViewById(R.id.previousInspectorFirstName);
			cloud[56] = (TextView) findViewById(R.id.previousInspectorLastName);
			cloud[57] = (TextView) findViewById(R.id.previousInspectorLicense);
			cloud[58] = (TextView) findViewById(R.id.yearBuilt);
			cloud[59] = (TextView) findViewById(R.id.squareFootage);
			cloud[60] = (TextView) findViewById(R.id.numberOfStories);
			cloud[61] = (TextView) findViewById(R.id.construction);
			cloud[62] = (TextView) findViewById(R.id.roofCovering);
			cloud[63] = (TextView) findViewById(R.id.roofDeckAttachment);
			cloud[64] = (TextView) findViewById(R.id.roofWallAttachment);
			cloud[65] = (TextView) findViewById(R.id.roofShape);
			cloud[66] = (TextView) findViewById(R.id.secondaryWaterResistance);
			cloud[67] = (TextView) findViewById(R.id.openingCoverage);
			cloud[68] = (TextView) findViewById(R.id.buildingType);
			tbladdtinfo = (TableLayout) findViewById(R.id.tableLayoutAdditionalInfo);
			additionaltxt = (TextView) findViewById(R.id.additionaltext);
			
			ScrollView scr = (ScrollView)findViewById(R.id.scr);
		    scr.setMinimumHeight(cf.ht);
			try {
				Cursor cur = cf.SelectTablefunction(cf.Cloud_table, "where SRID='"
						+ cf.selectedhomeid + "'");
				int rws = cur.getCount();

				cur.moveToFirst();
				if (rws == 0) {
					additionaltxt.setVisibility(cf.show.VISIBLE);
					tbladdtinfo.setVisibility(cf.show.GONE);
				} else {
					additionaltxt.setVisibility(cf.show.GONE);
					tbladdtinfo.setVisibility(cf.show.VISIBLE);
					if (cur != null) {
						for (int i = 2; i <= 68; i++) {
							cloud[i].setText(cur.getString(i));

							if (cur.getString(i).equals("anyType{}")
									|| cur.getString(i).equals("")) {
								cloud[i].setText("Not Available");
							}
						}

					}
				}
			} catch (Exception e) {
				
			}

	 }
	 public void clicker(View v) {
			switch (v.getId()) {

			case R.id.home:
						cf.gohome();
			break;

			}
		}
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				Intent intimg = new Intent(getApplicationContext(), InspectorInfo.class);
				intimg.putExtra("homeid", cf.selectedhomeid);
				intimg.putExtra("InspectionType", cf.onlinspectionid);
				intimg.putExtra("status", cf.onlstatus);
				startActivity(intimg);
				return true;
			}
			return super.onKeyDown(keyCode, event);
		}

}
