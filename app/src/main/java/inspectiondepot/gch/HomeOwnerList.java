package inspectiondepot.gch;


import inspectiondepot.gch.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.os.Bundle;
import android.provider.SyncStateContract.Columns;
import android.text.Html;
import android.text.method.Touch;
import android.util.FloatMath;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.ZoomButtonsController;

public class HomeOwnerList extends Activity{
	CommonFunctions cf;
	String strtit;
	TextView title;
	 Matrix matrix = new Matrix();
	 Matrix savedMatrix = new Matrix();

	 // We can be in one of these 3 states
	 static final int NONE = 0;
	 static final int DRAG = 1;
	 static final int ZOOM = 2;
	 int mode = NONE;
	 String colomnname="";

	 // Remember some things for zooming
	 PointF start = new PointF();
	 PointF mid = new PointF();
	 float oldDist = 1f;

	 @Override
     public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        
	        cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.onlinspectionid = extras.getString("InspectionType");
				cf.onlstatus = extras.getString("status");
		 	}
			
			if ("Assign".equals(cf.onlstatus)) {
				colomnname="GCH_PH_Status";
				cf.statusofdata = "30";
			}
			if ("Schedule".equals(cf.onlstatus)) {
				colomnname="GCH_PH_Status";
				cf.statusofdata = "40";
		
			}
			if ("CIT".equals(cf.onlstatus)) {
				cf.onlstatus="Completed Inspection in Tablet";
				colomnname="GCH_PH_IsInspected";
				cf.statusofdata = "1";
		
			}
	        setContentView(R.layout.inspectionlist);
	        cf.setupUI((LinearLayout)findViewById(R.id.lin_h));
	        TextView tvheader = (TextView) findViewById(R.id.information);
			tvheader.setText(cf.onlstatus);
			cf.onlinspectionlist = (LinearLayout) findViewById(R.id.linlayoutdyn);
			cf.onlinspectionlist.setOnTouchListener(new Touch());
			
			
			TextView note  = (TextView) findViewById(R.id.note);
			
			if("Assign".equals(cf.onlstatus)) {
			String headernote="Note : Owner First Name Last Name | Policy Number | Address | City | State | County | Zipcode";
			note.setText(headernote);
			}
			else
			{
				String headernote="Note : Owner First Name Last Name | Policy Number | Address | City | State | County | Zipcode | Inspection date | Inspection Start time  - End time";
				note.setText(headernote);
			}
			
		
			cf.releasecode = (TextView)findViewById(R.id.releasecode);
	        cf.releasecode.setText(cf.apkrc);
	        cf.getInspectorId();
	        cf.welcome = (TextView) this.findViewById(R.id.welcomename);
	        cf.welcome.setText(cf.Insp_firstname.toUpperCase()+" "+cf.Insp_lastname.toUpperCase());
	        
	         title = (TextView) this.findViewById(R.id.deleteiinformation);
			if (cf.onlinspectionid.equals("11")) {
				strtit = cf.strret;
				//strtit = "General Conditions and Hazards";
			} else {
				strtit = cf.strcarr;
				//strtit = "General Conditions and Hazards - Carrier Ordered";
			}
			cf.search = (Button) this.findViewById(R.id.search);
			cf.search_text = (EditText)findViewById(R.id.search_text);
			cf.search_clear_txt = (Button)findViewById(R.id.search_clear_txt);
			cf.search_clear_txt.setOnClickListener(new OnClickListener() {
            	public void onClick(View arg0) {
					// TODO Auto-generated method stub
					cf.search_text.setText("");
					cf.res = "";
					dbquery();
            	}
          });
			cf.search.setOnClickListener(new OnClickListener() {
                   public void onClick(View v) {
					// TODO Auto-generated method stub

					String temp = cf.encode(cf.search_text.getText()
							.toString());
					if (temp.equals("")) {
						cf.ShowToast("Please enter the Name or Policy Number to search.", 1);
						cf.search_text.requestFocus();
					} else {
						cf.res = temp;
						dbquery();
					}

				}

			});
			
			dbquery();
			
			
	 }
	 public class Touch implements OnTouchListener {

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			  TextView view = (TextView) v;
			  // Dump touch event to log
			  dumpEvent(event);

			  // Handle touch events here...
			  switch (event.getAction() & MotionEvent.ACTION_MASK) {
			  case MotionEvent.ACTION_DOWN:
			   savedMatrix.set(matrix);
			   start.set(event.getX(), event.getY());
			   mode = DRAG;
			   break;
			  case MotionEvent.ACTION_POINTER_DOWN:
			   oldDist = spacing(event);
			   if (oldDist > 10f) {
			    savedMatrix.set(matrix);
			    midPoint(mid, event);
			    mode = ZOOM;
			   }
			   break;
			  case MotionEvent.ACTION_UP:
			  case MotionEvent.ACTION_POINTER_UP:
			   mode = NONE;
			   break;
			  case MotionEvent.ACTION_MOVE:
			   if (mode == DRAG) {
			    // ...    
			    matrix.set(savedMatrix);
			    matrix.postTranslate(event.getX() - start.x, event.getY() - start.y);    
			   } else if (mode == ZOOM) {
			    float newDist = spacing(event);
			    if (newDist > 10f) {
			     matrix.set(savedMatrix);
			     float scale = newDist / oldDist;
			     matrix.postScale(scale, scale, mid.x, mid.y);
			    }
			   }
			   break;
			  }

			  view.setText("vgf");
			  return true; // indicate event was handled
			 }
			 /** Show an event in the LogCat view, for debugging */
			 private void dumpEvent(MotionEvent event) {
			  String names[] = { "DOWN", "UP", "MOVE", "CANCEL", "OUTSIDE", 
			    "POINTER_DOWN", "POINTER_UP", "7?", "8?", "9?" };
			  StringBuilder sb = new StringBuilder();
			  int action = event.getAction();
			  int actionCode = action & MotionEvent.ACTION_MASK;
			  sb.append("event ACTION_").append(names[actionCode]);
			  if (actionCode == MotionEvent.ACTION_POINTER_DOWN
			    || actionCode == MotionEvent.ACTION_POINTER_UP) {
			   sb.append("(pid ").append(
			     action >> MotionEvent.ACTION_POINTER_ID_SHIFT);
			   sb.append(")");
			  }
			  sb.append("[");
			  for (int i = 0; i < event.getPointerCount(); i++) {
			   sb.append("#").append(i);
			   sb.append("(pid ").append(event.getPointerId(i));
			   sb.append(")=").append((int) event.getX(i));
			   sb.append(",").append((int) event.getY(i));
			   if (i + 1 < event.getPointerCount())
			    sb.append(";");
			  }
			  sb.append("]");
			 }

			 /** Determine the space between the first two fingers */
			 private float spacing(MotionEvent event) {
			  float x = event.getX(0) - event.getX(1);
			  float y = event.getY(0) - event.getY(1);
			  return FloatMath.sqrt(x * x + y * y);
			 }

			 /** Calculate the mid point of the first two fingers */
			 private void midPoint(PointF point, MotionEvent event) {
			  float x = event.getX(0) + event.getX(1);
			  float y = event.getY(0) + event.getY(1);
			  point.set(x / 2, y / 2);
			 }
	 
	 }
	 public void clicker(View v)
	 {
		  switch(v.getId())
		  {
		  case R.id.deletehome:
			  cf.gohome();
			  break;
		  case R.id.deleteall:
			  String temp="(";
			  Cursor c=cf.gch_db.rawQuery(" Select * from "+cf.policyholder+" where "+colomnname+" ="+cf.statusofdata+" and GCH_PH_InspectorId="+cf.Insp_id+" and GCH_PH_InspectionTypeId ="+cf.onlinspectionid+" and GCH_PH_SubStatus<>41",null);
			  System.out.println("Cpint "+c.getCount());
			 if( c.getCount()>=1)
			 {
				 c.moveToFirst();
				 for(int i=0;i<c.getCount();i++)
				 {
					 temp+="'"+c.getString(c.getColumnIndex("GCH_PH_SRID"))+"'";
					 if((i+1)==(c.getCount()))
					 {
						 temp+=")";
						// return;
					 }
					 else
					 {
						 temp+=",";
						 c.moveToNext();
					 }
				 }
				
				 cf.delete_all(temp);
			 }else
			 {
				 cf.ShowToast("You don't have any record to delete  ", 1);
			 }
			  
			  break;
		  }
	 }
	 private void dbquery() {
		
			cf.data = null;
			cf.inspdata = "";
			cf.countarr = null;
			cf.rws = 0;
			try		
			{
				cf.sql = "select * from " + cf.policyholder
						+ " where GCH_PH_InspectionTypeId='" + cf.encode(cf.onlinspectionid)
						+ "' and GCH_PH_Status='" + cf.encode(cf.statusofdata) + "' and GCH_PH_InspectorId = '"
						+ cf.encode(cf.Insp_id) + "' and GCH_PH_SubStatus!=41";
				/**Quesr y for excute the search text **/
				if (!cf.res.trim().equals("")) {
					cf.sql += " and (GCH_PH_FirstName like '%" + cf.encode(cf.res)
							+ "%' or GCH_PH_LastName like '%" + cf.encode(cf.res)
							+ "%' or GCH_PH_Policyno like '%" + cf.encode(cf.res) + "%' ) ";
					if (cf.onlstatus.equals("CIT")) 
					{
						cf.sql += " and GCH_PH_IsInspected=1 ";
					}
				} /**Quesr y for excute the search text ends **/
				else if (cf.onlstatus.equals("CIT")) 
				{
					cf.sql += " and  GCH_PH_IsInspected=1 ";
				}
				cf.sql += " order by GCH_Schedule_ScheduledDate";
				Cursor cur = cf.gch_db.rawQuery(cf.sql, null);
				cf.rws = cur.getCount();
				title.setText(strtit + "\n" +  " "+"Total Record : " + cf.rws);
				cf.data = new String[cf.rws];
				cf.countarr = new String[cf.rws];
				int j = 0;
				cur.moveToFirst();
				
				if (cur.getCount() >= 1) {
	
					do {
						String dbinspid = cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_InspectorId")));
						String dbinsptypeid = cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_InspectionTypeId")));
						String scheduleddate = cf.decode(cur.getString(cur.getColumnIndex("GCH_Schedule_ScheduledDate")));
					
						if (cf.Insp_id.equals(dbinspid)	&& cf.onlinspectionid.equals(dbinsptypeid)) 
						{
							if (cur.getString(cur.getColumnIndex(this.colomnname)).equals(cf.statusofdata))
							{
								String s =(cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_FirstName"))).trim().equals("")) ? "-":cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_FirstName")));
								cf.data[j] = " "+ s+ " ";
								s=(cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_LastName"))).trim().equals("")) ? "-":cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_LastName")));
								this.cf.data[j] += s + " | ";
								s=(cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_Policyno"))).trim().equals("")) ? "-":cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_Policyno")));
								this.cf.data[j] += s+ " \n ";
								s=(cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_Address1"))).trim().equals("")) ? "-":cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_Address1")));
								this.cf.data[j]+= s + " | ";
								s=(cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_City"))).trim().equals("")) ? "-":cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_City")));
								this.cf.data[j] += s + " | ";
								s=(cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_State"))).trim().equals("")) ? "-":cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_State")));
								this.cf.data[j] += s + " | ";
								s=(cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_County"))).trim().equals("")) ? "-":cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_County")));
								this.cf.data[j] += s + " | ";
								s=(cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_Zip"))).trim().equals("")) ? "-":cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_Zip")));
								this.cf.data[j] += s+ " \n ";
								if(!cf.onlstatus.equals("Assign")) {
								s=(cf.decode(cur.getString(cur.getColumnIndex("GCH_Schedule_ScheduledDate"))).trim().equals("")) ? "-":cf.decode(cur.getString(cur.getColumnIndex("GCH_Schedule_ScheduledDate")));
								this.cf.data[j] += s + " | ";
								s=(cf.decode(cur.getString(cur.getColumnIndex("GCH_Schedule_InspectionStartTime"))).trim().equals("")) ? "-":cf.decode(cur.getString(cur.getColumnIndex("GCH_Schedule_InspectionStartTime")));
								this.cf.data[j] += s + " - ";
								s=(cf.decode(cur.getString(cur.getColumnIndex("GCH_Schedule_InspectionEndTime"))).trim().equals("")) ? "-":cf.decode(cur.getString(cur.getColumnIndex("GCH_Schedule_InspectionEndTime")));
								this.cf.data[j] += s ;
								}
								cf.countarr[j] = cur.getString(cur.getColumnIndex("GCH_PH_SRID"));
								
	
								if (cf.data[j].contains("null")) {
									cf.data[j] = cf.data[j].replace("null", "");
								}
								if (cf.data[j].contains("N/A | ")) {
									cf.data[j] = cf.data[j].replace("N/A |", "");
								}
								if (cf.data[j].contains("N/A - N/A")) {
									cf.data[j] = cf.data[j].replace("N/A - N/A", "");
								}
								
								if(cf.data[j].contains("- -"))
								{
									cf.data[j] = cf.data[j].replace("- -", "");
								}
								if(cf.data[j].endsWith("-")) {
									cf.data[j] = cf.data[j].substring(0,cf.data[j].length()-2);
								}
								
								//cf.data[j]=cf.inspdata;
								j++;
							}
						}
					} while (cur.moveToNext());
					cf.search_text.setText("");
					display();
				} else {
					cf.onlinspectionlist.removeAllViews();
					if(cf.res.equals(""))
					{
						cf.gohome();
					}
					else
					{
						cf.ShowToast("Sorry, No results found.", 1);
						cf.hidekeyboard();
					}
					
				}
				
			}catch(Exception e)
			
			{
				System.out.println("EE "+e.getMessage());	
			}
			
		}
     private void display() {

			cf.onlinspectionlist.removeAllViews();
			cf.sv = new ScrollView(this);
			cf.onlinspectionlist.addView(cf.sv);

			final LinearLayout l1 = new LinearLayout(this);
			l1.setOrientation(LinearLayout.VERTICAL);
			cf.sv.addView(l1);
			if (cf.data.length>=1) {
				
				for (int i = 0; i < cf.data.length; i++) {
					cf.tvstatus = new TextView[cf.rws];
					cf.deletebtn = new Button[cf.rws];
					LinearLayout l2 = new LinearLayout(this);
					LinearLayout.LayoutParams mainparamschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
					l2.setLayoutParams(mainparamschk);
					l2.setOrientation(LinearLayout.HORIZONTAL);
					l1.addView(l2);
					LinearLayout lchkbox = new LinearLayout(this);
					LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
					paramschk.topMargin = 8;
					paramschk.leftMargin = 20;
					paramschk.bottomMargin = 10;
					l2.addView(lchkbox);
					cf.tvstatus[i] = new TextView(this);
					cf.tvstatus[i].setTag("textbtn" + i);
					cf.tvstatus[i].setText(cf.data[i]);
					cf.tvstatus[i].setTextColor(Color.WHITE);
					cf.tvstatus[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,14);

				    lchkbox.addView(cf.tvstatus[i], paramschk);
				    LinearLayout ldelbtn = new LinearLayout(this);
					LinearLayout.LayoutParams paramsdelbtn = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			        paramsdelbtn.setMargins(0, 10, 10, 0); //left, top, right, bottom
					ldelbtn.setLayoutParams(mainparamschk);
					ldelbtn.setGravity(Gravity.RIGHT);
				    l2.addView(ldelbtn);
				    cf.deletebtn[i] = new Button(this);
					cf.deletebtn[i].setBackgroundResource(R.drawable.deletebtn1);
					cf.deletebtn[i].setTag("deletebtn" + i);
					cf.deletebtn[i].setPadding(30, 0, 0, 0);
					ldelbtn.addView(cf.deletebtn[i], paramsdelbtn);
					cf.tvstatus[i].setOnClickListener(new View.OnClickListener() {

						public void onClick(final View v) {
							String getidofselbtn = v.getTag().toString();
							final String repidofselbtn = getidofselbtn.replace(
									"textbtn", "");
							final int s = Integer.parseInt(repidofselbtn);

							String select = cf.countarr[s];
							cf.selectedhomeid = select.toString();
							String ScheduleDate;
							if (cf.onlstatus.equals("Assign")) {
								try {
									Cursor cur = cf.gch_db.rawQuery("select * from "
											+ cf.policyholder
											+ " where GCH_PH_SRID='" + cf.encode(cf.selectedhomeid) + "'",
											null);
									cur.moveToFirst();
									if (cur != null) {
										do {
											ScheduleDate = cf.decode(cur.getString(cur.getColumnIndex("GCH_Schedule_ScheduledDate")));
										if (ScheduleDate.equals("Null")
													|| ScheduleDate
															.equals("Not Available")
													|| ScheduleDate.equals("")
													|| ScheduleDate.equals("N/A")) {
												Intent intimg = new Intent(HomeOwnerList.this,
														Callattempt.class);
												intimg.putExtra("homeid", cf.selectedhomeid);
												intimg.putExtra("InspectionType", cf.onlinspectionid);
												intimg.putExtra("status", cf.onlstatus);
												
												startActivity(intimg);
											} else {
												if(cf.onlstatus.equals("Completed Inspection in Tablet"))
												{
													cf.onlstatus="CIT";
												}
												Intent intimg = new Intent(HomeOwnerList.this,
														PolicyHolder.class);
												
												intimg.putExtra("homeid", cf.selectedhomeid);
												intimg.putExtra("InspectionType", cf.onlinspectionid);
												intimg.putExtra("status", cf.onlstatus);
												startActivity(intimg);
											}
										} while (cur.moveToNext());
									}
								} catch (Exception e) {
								}
							} else {
								if(cf.onlstatus.equals("Completed Inspection in Tablet"))
								{
									cf.onlstatus="CIT";
								}
								Intent intimg = new Intent(HomeOwnerList.this,
										PolicyHolder.class);
								intimg.putExtra("homeid", cf.selectedhomeid);
							    intimg.putExtra("InspectionType", cf.onlinspectionid);
								intimg.putExtra("status", cf.onlstatus);
								
								startActivity(intimg);
							}

						}

					});
					cf.deletebtn[i].setOnClickListener(new View.OnClickListener() {
						public void onClick(final View v) {
							String getidofselbtn = v.getTag().toString();
							final String repidofselbtn = getidofselbtn.replace(
									"deletebtn", "");
							final int cvrtstr = Integer.parseInt(repidofselbtn);
							final String dt = cf.countarr[cvrtstr];
							 cf.alerttitle="Delete";
							  cf.alertcontent="Are you sure want to delete?";
							    final Dialog dialog1 = new Dialog(HomeOwnerList.this,android.R.style.Theme_Translucent_NoTitleBar);
								dialog1.getWindow().setContentView(R.layout.alertsync);
								TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
								txttitle.setText( cf.alerttitle);
								TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
								txt.setText(Html.fromHtml( cf.alertcontent));
								Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
								Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
								btn_yes.setOnClickListener(new OnClickListener()
								{
				                	@Override
									public void onClick(View arg0) {
										// TODO Auto-generated method stub
										dialog1.dismiss();
										cf.fn_delete(dt);
										dbquery();
									}
									
								});
								btn_cancel.setOnClickListener(new OnClickListener()
								{

									@Override
									public void onClick(View arg0) {
										// TODO Auto-generated method stub
										dialog1.dismiss();
										
									}
									
								});
								dialog1.setCancelable(false);
								dialog1.show();
						

						}
					});
				}
			}

		}
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				startActivity(new Intent(HomeOwnerList.this,Dashboard.class));
				//intimg.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				return true;
			}
			return super.onKeyDown(keyCode, event);
		}
}
