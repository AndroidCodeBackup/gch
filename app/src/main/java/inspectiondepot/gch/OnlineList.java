package inspectiondepot.gch;

import inspectiondepot.gch.R;

import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeoutException;

import org.ksoap2.serialization.SoapObject;
import org.xmlpull.v1.XmlPullParserException;


import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

public class OnlineList extends Activity implements Runnable{
	CommonFunctions cf;
	String strtit;
	TextView title;
	int hand_msg=0;
	SoapObject chklogin;
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf = new CommonFunctions(this);
	        setContentView(R.layout.online);
	        cf.setupUI((ScrollView)findViewById(R.id.scr));
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.onlstatus = extras.getString("Status");
				cf.onlsubstatus = extras.getString("SubStatus");
				cf.onlinspectionid = extras.getString("InspectionType");
			}
			System.out.println("There is no isseus");
			TextView tvheader = (TextView) findViewById(R.id.information);
			
			TextView headernote = (TextView) findViewById(R.id.note);
			if (cf.onlstatus.equals("30")&&cf.onlsubstatus.equals("0")) {
				tvheader.setText("Assign");
				headernote.setText("Note : Owner First Name Last Name | Policy Number | Address | City | State | County | Zipcode");
			} else if (cf.onlstatus.equals("40")&&cf.onlsubstatus.equals("0")) {
			System.out.println("No moer issues in the if ");
				headernote.setText(Html.fromHtml("Note : Owner First Name Last Name | Policy Number | Address | City | State | County | Zipcode \n | Inspection date | Inspection Start time  - End time"));
				tvheader.setText("Schedule");
			} else if (cf.onlstatus.equals("110")&&cf.onlsubstatus.equals("111")) {
				headernote.setText("Note : Owner First Name Last Name | Policy Number | Address | City | State | County | Zipcode | Inspection date | Inspection Start time  - End time");
				tvheader.setText("Unable to Schedule Inspection");
			} else if (cf.onlstatus.equals("111")&&cf.onlsubstatus.equals("0")) {
				headernote.setText("Note : Owner First Name Last Name | Policy Number | Address | City | State | County | Zipcode | Inspection date | Inspection Start time  - End time");
				tvheader.setText("Cancelled Inspection");
			}
			else if (cf.onlstatus.equals("40")&&cf.onlsubstatus.equals("41")) {
				headernote.setText("Note : Owner First Name Last Name | Policy Number | Address | City | State | County | Zipcode | Inspection date | Inspection Start time  - End time");
				tvheader.setText("Completed in Online");
			}
			cf.onlinspectionlist = (LinearLayout) findViewById(R.id.linlayoutdyn);
			cf.releasecode = (TextView)findViewById(R.id.releasecode);
	        cf.releasecode.setText(cf.apkrc);
	        cf.getInspectorId();
	        cf.welcome = (TextView) this.findViewById(R.id.welcomename);
	        cf.welcome.setText(cf.Insp_firstname+" "+cf.Insp_lastname);
	         title = (TextView) this.findViewById(R.id.deleteiinformation);
			if (cf.onlinspectionid.equals("11")) {
				strtit = cf.strret;
			} else {
				strtit = cf.strcarr;
			}
			cf.search = (Button) this.findViewById(R.id.search);
			cf.search_text = (EditText)findViewById(R.id.search_text);
			cf.search_clear_txt = (Button)findViewById(R.id.search_clear_txt);
			cf.search_clear_txt.setOnClickListener(new OnClickListener() {
            	public void onClick(View arg0) {
					// TODO Auto-generated method stub
					cf.search_text.setText("");
					cf.res = "";
					dbquery();
            	}
          });
			cf.search.setOnClickListener(new OnClickListener() {
                   public void onClick(View v) {
					// TODO Auto-generated method stub

					String temp = cf.encode(cf.search_text.getText()
							.toString());
					if (temp.equals("")) {
						cf.ShowToast("Please enter the Name or Policy Number to search.", 1);
						cf.search_text.requestFocus();
					} else {
						cf.res = temp;
						dbquery();
					}

				}

			});
			if(cf.isInternetOn()==true)
			{
               String source = "<font color=#FFFFFF>Loading data. Please wait..."
						+ "</font>";
				cf.pd = ProgressDialog.show(OnlineList.this,"", Html.fromHtml(source), true);
				Thread thread = new Thread(OnlineList.this);
				thread.start();
			}
			else
			{
				cf.ShowToast("Internet connection is not available.",1);
			}
			
	 }
	 private void retrievedata(SoapObject chklogin) throws NetworkErrorException,
		 IOException, XmlPullParserException, SocketTimeoutException {
		 cf.dropTable(1);	
	     cf.Create_Table(6);
	 
	     int Cnt = chklogin.getPropertyCount();
	     System.out.println("cnt "+Cnt);
     	for (int i = 0; i < Cnt; i++) {
             SoapObject obj = (SoapObject) chklogin.getProperty(i);
		     try {
		    	 cf.HomeId = (String.valueOf(obj.getProperty("SRID")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("SRID")).equals("NA"))?"":(String.valueOf(obj.getProperty("SRID")).equals("N/A"))?"":String.valueOf(obj.getProperty("SRID"));
				 cf.InspectorId = (String.valueOf(obj.getProperty("InspectorId")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectorId")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectorId")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectorId"));
				 cf.ScheduledDate = (String.valueOf(obj.getProperty("ScheduledDate")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("ScheduledDate")).equals("NA"))?"":(String.valueOf(obj.getProperty("ScheduledDate")).equals("N/A"))?"":String.valueOf(obj.getProperty("ScheduledDate"));
				 cf.InspectionStartTime = (String.valueOf(obj.getProperty("InspectionStartTime")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionStartTime")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionStartTime")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionStartTime"));
				 cf.InspectionEndTime = (String.valueOf(obj.getProperty("InspectionEndTime")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionEndTime")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionEndTime")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionEndTime"));
				 cf.PH_Fname = (String.valueOf(obj.getProperty("FirstName")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("FirstName")).equals("NA"))?"":(String.valueOf(obj.getProperty("FirstName")).equals("N/A"))?"":String.valueOf(obj.getProperty("FirstName"));
				 cf.PH_Lname = (String.valueOf(obj.getProperty("LastName")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("LastName")).equals("NA"))?"":(String.valueOf(obj.getProperty("LastName")).equals("N/A"))?"":String.valueOf(obj.getProperty("LastName"));
				 cf.PH_Address1 = (String.valueOf(obj.getProperty("Address1")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Address1")).equals("NA"))?"":(String.valueOf(obj.getProperty("Address1")).equals("N/A"))?"":String.valueOf(obj.getProperty("Address1"));
				 cf.PH_City = (String.valueOf(obj.getProperty("City")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("City")).equals("NA"))?"":(String.valueOf(obj.getProperty("City")).equals("N/A"))?"":String.valueOf(obj.getProperty("City"));
				 cf.PH_State = (String.valueOf(obj.getProperty("State")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("State")).equals("NA"))?"":(String.valueOf(obj.getProperty("State")).equals("N/A"))?"":String.valueOf(obj.getProperty("State"));
				 cf.PH_Zip = (String.valueOf(obj.getProperty("Zip")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Zip")).equals("NA"))?"":(String.valueOf(obj.getProperty("Zip")).equals("N/A"))?"":String.valueOf(obj.getProperty("Zip"));
				 cf.PH_County = (String.valueOf(obj.getProperty("Country")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Country")).equals("NA"))?"":(String.valueOf(obj.getProperty("Country")).equals("N/A"))?"":String.valueOf(obj.getProperty("Country"));
				 cf.PH_Policyno = (String.valueOf(obj.getProperty("OwnerPolicyNo")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("OwnerPolicyNo")).equals("NA"))?"":(String.valueOf(obj.getProperty("OwnerPolicyNo")).equals("N/A"))?"":String.valueOf(obj.getProperty("OwnerPolicyNo"));
				 cf.PH_InspectionTypeId = (String.valueOf(obj.getProperty("InspectionTypeId")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionTypeId")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionTypeId")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionTypeId"));
				 cf.PH_Status = (String.valueOf(obj.getProperty("Status")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Status")).equals("NA"))?"":(String.valueOf(obj.getProperty("Status")).equals("N/A"))?"":String.valueOf(obj.getProperty("Status"));
				 cf.PH_SubStatus = (String.valueOf(obj.getProperty("SubStatusID")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("SubStatusID")).equals("NA"))?"":(String.valueOf(obj.getProperty("SubStatusID")).equals("N/A"))?"":String.valueOf(obj.getProperty("SubStatusID"));
				 
				 System.out.println("insert b4");
				 
			      cf.gch_db.execSQL("INSERT INTO "
					+ cf.Onlinepolicyholder
					+ " (GCH_OL_PH_SRID,GCH_OL_PH_InspectorId,GCH_OL_PH_Status,GCH_OL_PH_SubStatus,GCH_OL_PH_InspectionTypeId,GCH_OL_PH_FirstName,GCH_OL_PH_LastName,GCH_OL_PH_Address1,GCH_OL_PH_City,GCH_OL_PH_State,GCH_OL_PH_County,GCH_OL_PH_Zip,GCH_OL_PH_Policyno,GCH_OL_Schedule_ScheduledDate,GCH_OL_Schedule_InspectionStartTime,GCH_OL_Schedule_InspectionEndTime)"
					+ " VALUES ('" + cf.encode(cf.HomeId)  + "','"+ cf.encode(cf.Insp_id)+"','"
					+ cf.encode(cf.PH_Status)+"','"
					+ cf.encode(cf.PH_SubStatus)+"','"+cf.encode(cf.PH_InspectionTypeId)+"','"
					+ cf.encode(cf.PH_Fname) + "','"
					+ cf.encode(cf.PH_Lname) + "','"
					+ cf.encode(cf.PH_Address1) + "','"
					+ cf.encode(cf.PH_City) + "','"
					+ cf.encode(cf.PH_State) + "','"
					+ cf.encode(cf.PH_County) + "','" + cf.encode(cf.PH_Zip)
					+ "','" + cf.encode(cf.PH_Policyno) + "','"
					+ cf.encode(cf.ScheduledDate) + "','" + cf.encode(cf.InspectionStartTime) + "','"
					+ cf.encode(cf.InspectionEndTime)+"')");
			      System.out.println("insert afetr succ");
			
		} catch (Exception e) {
			System.out.println("ee "+e.getMessage());
			 cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ OnlineList.this +" "+" in the stage of(catch) retrieving policyholder details from webservice at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				
		}
		
	}

	}
	 private void dbquery() {

			int k = 1;
			cf.data = null;
			cf.inspdata = "";
			cf.sql = "select * from " + cf.Onlinepolicyholder;
			if (!cf.res.equals("")) {

				cf.sql += " where (GCH_OL_PH_FirstName like '%" + cf.encode(cf.res)
						+ "%' or GCH_OL_PH_LastName like '%" + cf.encode(cf.res)
						+ "%' or GCH_OL_PH_Policyno like '%" + cf.encode(cf.res)
						+ "%') and GCH_OL_PH_InspectionTypeId='" + cf.onlinspectionid
						+ "' and GCH_OL_PH_InspectorId='" + cf.Insp_id + "'";
				if (cf.onlstatus.equals("110")&& cf.onlsubstatus.equals("111")) {
					cf.sql += " and  GCH_OL_PH_Status=110 and GCH_OL_PH_SubStatus=111 and GCH_OL_PH_InspectionTypeId='" + cf.onlinspectionid + "'";
				} else if (cf.onlstatus.equals("90")&& cf.onlsubstatus.equals("0")) {
					cf.sql += " and GCH_OL_PH_Status=90 and GCH_OL_PH_SubStatus=0 and GCH_OL_PH_InspectionTypeId='" + cf.onlinspectionid + "'";
				} else if (cf.onlstatus.equals("40")&& cf.onlsubstatus.equals("41")) {
					cf.sql += " and GCH_OL_PH_Status=40 and GCH_OL_PH_SubStatus='41' and GCH_OL_PH_InspectionTypeId='" + cf.onlinspectionid
							+ "'";
				}
				 else if (cf.onlstatus.equals("40")&& cf.onlsubstatus.equals("0")) {
					 cf.sql += " and GCH_OL_PH_Status=40 and GCH_OL_PH_SubStatus='0' and GCH_OL_PH_InspectionTypeId='" + cf.onlinspectionid
								+ "'";
					}
				 else if (cf.onlstatus.equals("30")&& cf.onlsubstatus.equals("0")) {
					 cf.sql += " and GCH_OL_PH_Status=30 and GCH_OL_PH_SubStatus='0' and GCH_OL_PH_InspectionTypeId='" + cf.onlinspectionid
								+ "'";
					}

			} else {
				
			
				if (cf.onlstatus.equals("110")&& cf.onlsubstatus.equals("111")) {
					cf.sql += " where GCH_OL_PH_Status=110 and GCH_OL_PH_SubStatus=111 and GCH_OL_PH_InspectionTypeId='" + cf.onlinspectionid
							+ "' and GCH_OL_PH_InspectorId='" + cf.Insp_id + "'";
				} else if (cf.onlstatus.equals("90")&& cf.onlsubstatus.equals("0")) {
					cf.sql += " where GCH_OL_PH_Status=90  and GCH_OL_PH_SubStatus=0 and GCH_OL_PH_InspectionTypeId='" + cf.onlinspectionid
							+ "' and GCH_OL_PH_InspectorId='" + cf.Insp_id + "'";
				}  else if (cf.onlstatus.equals("40")&& cf.onlsubstatus.equals("41")) {
					cf.sql += " where GCH_OL_PH_Status=40 and GCH_OL_PH_SubStatus='41' and GCH_OL_PH_InspectionTypeId='" + cf.onlinspectionid
							+ "' and GCH_OL_PH_InspectorId='" + cf.Insp_id + "'";
				}
				 else if (cf.onlstatus.equals("40")&& cf.onlsubstatus.equals("0")) {
					 cf.sql += " where GCH_OL_PH_Status=40 and GCH_OL_PH_SubStatus='0' and GCH_OL_PH_InspectionTypeId='" + cf.onlinspectionid
								+ "' and GCH_OL_PH_InspectorId='" + cf.Insp_id + "'";
					}
				 else if (cf.onlstatus.equals("30")&& cf.onlsubstatus.equals("0")) {
					 cf.sql += " where GCH_OL_PH_Status=30 and GCH_OL_PH_SubStatus='0' and GCH_OL_PH_InspectionTypeId='" + cf.onlinspectionid
								+ "' and GCH_OL_PH_InspectorId='" + cf.Insp_id + "'";
					}

			}
			Cursor cur = cf.gch_db.rawQuery(cf.sql, null);
			cf.rws = cur.getCount();
			title.setText(strtit + "\n" + "Total Record : " + cf.rws);
			cf.data = new String[cf.rws];
			int j = 0;
			cur.moveToFirst();
			if (cur.getCount() >0) {
				int i=0;
				do {
					cf.data[j] = " "+ cf.decode(cur.getString(cur.getColumnIndex("GCH_OL_PH_FirstName"))) + " ";
					cf.data[j] += cf.decode(cur.getString(cur.getColumnIndex("GCH_OL_PH_LastName"))) + " | ";
					cf.data[j] += cf.decode(cur.getString(cur.getColumnIndex("GCH_OL_PH_Policyno"))) + " \n ";
					cf.data[j] += cf.decode(cur.getString(cur.getColumnIndex("GCH_OL_PH_Address1"))) + " | ";
					cf.data[j] += cf.decode(cur.getString(cur.getColumnIndex("GCH_OL_PH_City"))) + " | ";
					cf.data[j] += cf.decode(cur.getString(cur.getColumnIndex("GCH_OL_PH_State"))) + " | ";
					cf.data[j] += cf.decode(cur.getString(cur.getColumnIndex("GCH_OL_PH_County"))) + " | ";
					cf.data[j] += cf.decode(cur.getString(cur.getColumnIndex("GCH_OL_PH_Zip")))+ " \n ";
					if (! cf.onlstatus.equals("30")) {
					cf.data[j] += cf.decode(cur.getString(cur.getColumnIndex("GCH_OL_Schedule_ScheduledDate")))+ " | ";
					cf.data[j] += cf.decode(cur.getString(cur.getColumnIndex("GCH_OL_Schedule_InspectionStartTime"))) + " - ";
					cf.data[j] += cf.decode(cur.getString(cur.getColumnIndex("GCH_OL_Schedule_InspectionEndTime")));
					}

					if (cf.data[j].contains("null")) {
						cf.data[j] = cf.data[j].replace("null", "");
					}
					if (cf.data[j].contains(" | - ")) {
						cf.data[j] = cf.data[j].replace(" | -", "");
					}
					if (cf.data[j].contains("N/A |")) {
						cf.data[j] = cf.data[j].replace("N/A |", "");
					}
					if (cf.data[j].contains("N/A - N/A")) {
						cf.data[j] = cf.data[j].replace("N/A - N/A", "");
					}
					if (cf.data[j].contains("N/A")) {
						cf.data[j] = cf.data[j].replace("N/A", "-");
					}
					if(cf.data[j].endsWith("- ")) {
						cf.data[j] = cf.data[j].substring(0,cf.data[j].length()-3);
					}
					
					j++;
				} while (cur.moveToNext());
				cf.search_text.setText("");
				display();
			} else {
				cf.ShowToast("Sorry, No results found.", 1);cf.hidekeyboard();
				cf.onlinspectionlist.removeAllViews();
			}

		}
	 private void display() {
		 cf.onlinspectionlist.removeAllViews();
			cf.sv = new ScrollView(this);
			cf.onlinspectionlist.addView(cf.sv);

			final LinearLayout l1 = new LinearLayout(this);
			l1.setOrientation(LinearLayout.VERTICAL);
			cf.sv.addView(l1);

			if (cf.data!=null && cf.data.length>0) {
				
				for (int i = 0; i < cf.data.length; i++) {
					cf.tvstatus = new TextView[cf.rws];
					LinearLayout l2 = new LinearLayout(this);
					LinearLayout.LayoutParams mainparamschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
					l2.setLayoutParams(mainparamschk);
					l2.setOrientation(LinearLayout.HORIZONTAL);
					l1.addView(l2);
					LinearLayout lchkbox = new LinearLayout(this);
					LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
					paramschk.topMargin = 8;
					paramschk.leftMargin = 20;
					paramschk.bottomMargin = 10;
					l2.addView(lchkbox);

					cf.tvstatus[i] = new TextView(this);
					cf.tvstatus[i].setMinimumWidth(580);
					cf.tvstatus[i].setMaxWidth(580);
					cf.tvstatus[i].setTag("textbtn" + i);
					cf.tvstatus[i].setText(cf.data[i]);
					cf.tvstatus[i].setTextColor(Color.WHITE);
					cf.tvstatus[i].setTextSize(14);
					lchkbox.addView(cf.tvstatus[i], paramschk);

					LinearLayout ldelbtn = new LinearLayout(this);
					LinearLayout.LayoutParams paramsdelbtn = new LinearLayout.LayoutParams(
							93, 37);
					paramsdelbtn.rightMargin = 10;
					paramsdelbtn.bottomMargin = 10;
					l2.addView(ldelbtn);
			
				}
			}

		}
	 public void clicker(View v)
	 {
		  switch(v.getId())
		  {
		  case R.id.deletehome:
			  cf.gohome();
			  break;
		  }
	 }
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				startActivity(new Intent(OnlineList.this,Dashboard.class));
				return true;
			}
			return super.onKeyDown(keyCode, event);
		}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		try{
			 chklogin = cf.Calling_WS2(cf.onlstatus,cf.onlsubstatus,cf.onlinspectionid,cf.Insp_id,"OnlineSyncPolicyInfo");
			 
			 System.out.println("chklogin "+chklogin);
			if (chklogin.equals("anyType{}")) {
				hand_msg=2;
				handler.sendEmptyMessage(0);
				
				//cf.ShowToast("Server is busy. Please try again later.", 2);
				
			} else {
				hand_msg=1;
				handler.sendEmptyMessage(0);
				
				
					
					
			}
			
		}
		catch (Exception e) {
			System.out.println("catch "+e.getMessage());
			// TODO Auto-generated catch block
			hand_msg=3;
			//cf.ShowToast("Problem in Connecting server . Please try again later.", 2);
			handler.sendEmptyMessage(0);
			e.printStackTrace();
		} 
	}
	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			cf.pd.dismiss();
			if(hand_msg==1)
			{
				try {
					retrievedata(chklogin);
				} catch (SocketTimeoutException e) {
					// TODO Auto-generated catch block
					cf.ShowToast("No data found.", 2);
					e.printStackTrace();
				} catch (NetworkErrorException e) {
					// TODO Auto-generated catch block
					cf.ShowToast("No data found.", 2);
					e.printStackTrace();
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					cf.ShowToast("No data found.", 2);
					e.printStackTrace();
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					cf.ShowToast("No data found.", 2);
					e.printStackTrace();
				}
				dbquery();
			}
			else if(hand_msg==2)
			{
				cf.ShowToast("Server is busy. Please try again later.", 2);
				startActivity(new Intent(OnlineList.this,HomeScreen.class));
			}
			else if(hand_msg==3)
			{
				cf.ShowToast("Problem in Connecting server . Please try again later.", 2);
				startActivity(new Intent(OnlineList.this,HomeScreen.class));
			}
				
		}
	};
}
