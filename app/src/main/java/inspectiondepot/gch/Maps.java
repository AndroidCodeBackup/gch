package inspectiondepot.gch;

import inspectiondepot.gch.R;

import java.io.IOException;
import java.io.ObjectOutputStream.PutField;

import java.io.InputStream;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Maps extends MapActivity implements Runnable {

	String homeid;
	Geocoder myGeocoder;
	static GeoPoint point;
	final static int MAX_RESULT = 10;
	private static final String TAG = null;
	static List<Overlay> mapOverlays;
	Drawable drawable;
	static String addr, addr1;
	static String straddr;
	static String strcty;
	int ichk, k;
	String strstatnam, strcntry;
	static String newstraddr;
	ProgressDialog pd;
	static String newstrcty;
	int value, Count;
	CommonFunctions cf;
	String newstrstatnam, newstrcntry, InspectionType, status;
	TextView policyholderinfo;
	double lat, lon;
	static MapView mapView;
	static SimpleItemizedOverlay itemizedOverlay;

	public void onCreate(Bundle SavedInstanceState) {
		super.onCreate(SavedInstanceState);
		cf = new CommonFunctions(this);
		
		 Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.selectedhomeid =  extras.getString("homeid");
				cf.onlinspectionid = extras.getString("InspectionType");
			    cf.onlstatus = extras.getString("status");
		 	}
			AlertDialog.Builder builder = new AlertDialog.Builder(Maps.this);
				builder.setTitle("Network Connection")
				.setMessage(" Mapping feature requires an Internet Connection.\n Do you have internet Connection? .   ");
				builder.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								
								set_map();
							}
						});
				builder.setNegativeButton("No",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
	                        // ShowToast("Internet connection is not available.",0);
								Intent	myintent = new Intent(Maps.this,Submit.class); 
									cf.putExtras(myintent);
									startActivity(myintent);
								
							}
						});
				builder.show();
				builder.setCancelable(false);
		
	}

	public void set_map() {
		// TODO Auto-generated method stub
		setContentView(R.layout.map);
		cf.getDeviceDimensions();
		LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
		mainmenu_layout.setMinimumWidth(cf.wd);
	    mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 7, 0,cf));
		
		myGeocoder = new Geocoder(this);
		 
		cf.getInspectorId();
		cf.getinspectiondate();
		/** menu **/
		try {
			Cursor c2 = cf.gch_db.rawQuery("SELECT * FROM  "
					+ cf.policyholder + " WHERE GCH_PH_SRID='" + cf.selectedhomeid
					+ "'", null);
			int rws = c2.getCount();
			int Column5 = c2.getColumnIndex("GCH_PH_Address1");
			int Column6 = c2.getColumnIndex("GCH_PH_City");
			int Column8 = c2.getColumnIndex("GCH_PH_State");
			int Column9 = c2.getColumnIndex("GCH_PH_County");
			c2.moveToFirst();
			if (c2 != null) {
				do {
					//straddr = "2401+NW+49TH+LN,BOCA+RATON,Florida,PALM+BEACH";//cf.getsinglequotes(c2.getString(Column5));
					straddr =cf.decode(c2.getString(Column5));
					strcty = cf.decode(c2.getString(Column6));
					strstatnam =cf.decode(c2.getString(Column8));
					strcntry =cf.decode(c2.getString(Column9));
					
				} while (c2.moveToNext());
			}
			c2.close();

		} catch (Exception e) {
			
		}

		try {
			mapView = (MapView) findViewById(R.id.mapview);
			mapView.setBuiltInZoomControls(true);
			mapOverlays = mapView.getOverlays();
			if (straddr.contains("")) {
				newstraddr = straddr.replace(" ", "+");
			}
			if (strcty.contains("")) {
				newstrcty = strcty.replace(" ", "+");
			}
			if (strstatnam.contains("")) {
				newstrstatnam = strstatnam.replace(" ", "+");
			}
			if (strcntry.contains("-")) {
				newstrcntry = strcntry.replace("-", "+");
				if (strcntry.contains("")) {
					newstrcntry = newstrcntry.replace(" ", "+");
				}

			} else {
				if (strcntry.contains("")) {
					newstrcntry = strcntry.replace(" ", "+");
				}
			}
		} catch (Exception e) {
			
		}
		// addr="2401+NW+49TH+LN,BOCA+RATON,Florida,PALM+BEACH";
		// addr="1420+NW+73RD+STREET,MIAMI,FLORIDA,MIAMI+DADE";
		// addr= 1420+NW+73RD+STREET,Florida,Florida,MIAMI-DADE

		addr = newstraddr + "," + newstrcty + "," + newstrstatnam + ","
				+ newstrcntry;
		System.out.println("adre"+addr);
		mapOverlays = mapView.getOverlays();
		drawable = this.getResources().getDrawable(R.drawable.iconmarker);
		itemizedOverlay = new SimpleItemizedOverlay(drawable, mapView);
	//	 itemizedOverlay1 = new SimpleItemizedOverlay(drawable,mapView1);
		String source = "<b><font color=#00FF33>Loading map. Please wait..."
				+ "</font></b>";
		pd = ProgressDialog.show(Maps.this, "", Html.fromHtml(source), true);
		ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = conMgr.getActiveNetworkInfo();
		if (info != null && info.isConnected()) {
			ichk = 0;
		} else {
			ichk = 1;
		}
		Thread thread = new Thread(Maps.this);
		thread.start();
 
	}

	public static JSONObject getLocationInfo(String address) {
		HttpGet httpGet = new HttpGet(
				"http://maps.google.com/maps/api/geocode/json?address="
						+ address + "ka&sensor=false");
		HttpClient client = new DefaultHttpClient();
		HttpResponse response;
		StringBuilder stringBuilder = new StringBuilder();

		try {
			response = client.execute(httpGet);
			HttpEntity entity = response.getEntity();
			InputStream stream = entity.getContent();
			int b;
			while ((b = stream.read()) != -1) {
				stringBuilder.append((char) b);
			}
		} catch (ClientProtocolException e) {
			Log.i(TAG, "ClientProtocolException" + e.getMessage());
		} catch (IOException e) {
			Log.i(TAG, "IOException" + e.getMessage());
		}

		JSONObject jsonObject = new JSONObject();
		try {
			Log.i(TAG, "fdfgd");
			jsonObject = new JSONObject(stringBuilder.toString());
			Log.i(TAG, "fds" + jsonObject);
			getGeoPoint(jsonObject);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonObject;
	}

	public static GeoPoint getGeoPoint(JSONObject jsonObject) {

		Double lon = new Double(0);
		Double lat = new Double(0);
		Log.i(TAG, "latsdgfsdf");
		try {

			lon = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
					.getJSONObject("geometry").getJSONObject("location")
					.getDouble("lng");

			lat = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
					.getJSONObject("geometry").getJSONObject("location")
					.getDouble("lat");
			Log.i(TAG, "lat=" + lat + lon);

			showmap(lat, lon);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new GeoPoint((int) (lat * 1E6), (int) (lon * 1E6));

	}

	public static void showmap(Double lati, Double longi) {
		point = new GeoPoint((int) (lati * 1E6), (int) (longi * 1E6));
		Log.i(TAG, "chk");
		mapView.getController().animateTo(point);
		Log.i(TAG, "g");

		OverlayItem overlayitem = new OverlayItem(point, "Home Owner", straddr
				+ "," + strcty);
		Log.i(TAG, "chkkkk");

		itemizedOverlay.addOverlay(overlayitem);
		Log.i(TAG, "chkkkasdfk");

		mapOverlays.add(itemizedOverlay);
		Log.i(TAG, "asfd");

		MapController mapController = mapView.getController();
		Log.i(TAG, "re");
		
		mapController.setZoom(28);
		Log.i(TAG, "sdf");
		mapView.setSatellite(true);
		Log.i(TAG, "gas");

	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			 if(cf.strschdate.equals(""))
				{
					cf.ShowToast("Editing information without scheduling not allowed. ", 1);
				}
			 else
			 {
				Intent intimg = new Intent(Maps.this, Feedback.class);
				cf.putExtras(intimg);
				startActivity(intimg);
			 }
			return true;
		}
		//return super.onKeyDown(keyCode, event);
		return true;
	}

	public void run() {
		// TODO Auto-generated method stub
		if (ichk == 0) {
			getLocationInfo(addr);
			k = 1;
		} else {
			k = 2;
		}
		handler.sendEmptyMessage(0);
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			pd.dismiss();
			if (k == 1) {

			} else if (k == 2) {
	
			}

		}
	};
	public void clicker(View v) {
		switch (v.getId()) {

		case R.id.home:
					  cf.gohome();
					  break;
			
		}
	}
	
}
