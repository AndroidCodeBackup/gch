package inspectiondepot.gch;


import inspectiondepot.gch.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class CompletedInspectionList extends Activity{
	CommonFunctions cf;
	TextView title;
	String strtit;
	private String columnname="";
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.onlinspectionid = extras.getString("InspectionType");
				System.out.println("cf.online "+cf.onlinspectionid);
				cf.onlstatus = extras.getString("status");
			} 
			setContentView(R.layout.completedinspectionlist);
			cf.setupUI((LinearLayout)findViewById(R.id.lin_h));
			TextView tvheader = (TextView) findViewById(R.id.information);
			if (cf.onlstatus.equals("CIO")) {
				cf.statusofdata = "41";
				columnname="GCH_PH_SubStatus";
				tvheader.setText("Completed Inspection in online");
			} else if (cf.onlstatus.equals("UTS")) {
				cf.statusofdata = "110";
				columnname="GCH_PH_Status";
				tvheader.setText("Unable to Schedule Inspection");
			} else if (cf.onlstatus.equals("CAN")) {
				cf.statusofdata = "110";
				columnname="GCH_PH_Status";
				tvheader.setText("Cancelled Inspection");
			}
			TextView note  = (TextView) findViewById(R.id.note);
			if(cf.onlstatus.equals("UTS")) {
				String headernote="Note : Owner First Name Last Name | Policy Number | Address | City | State | County | Zipcode";
				note.setText(headernote);
				}
				else
				{
					String headernote="Note : Owner First Name Last Name | Policy Number | Address | City | State | County | Zipcode | Inspection date | Inspection Start time  - End time";
					note.setText(headernote);
				}
			
			cf.onlinspectionlist = (LinearLayout) findViewById(R.id.linlayoutdyn);
			cf.releasecode = (TextView)findViewById(R.id.releasecode);
	        cf.releasecode.setText(cf.apkrc);
	        cf.getInspectorId();
	        cf.welcome = (TextView) this.findViewById(R.id.welcomename);
	        cf.welcome.setText(cf.Insp_firstname.toUpperCase()+" "+cf.Insp_lastname.toUpperCase());
	        title = (TextView) this.findViewById(R.id.deleteiinformation);
			if (cf.onlinspectionid.equals("11")) {
				strtit = cf.strret;
			} else {
				strtit = cf.strcarr;
			}
			cf.search = (Button) this.findViewById(R.id.search);
			cf.search_text = (EditText)findViewById(R.id.search_text);
			cf.search_clear_txt = (Button)findViewById(R.id.search_clear_txt);
			cf.search_clear_txt.setOnClickListener(new OnClickListener() {
            	public void onClick(View arg0) {
					// TODO Auto-generated method stub
					cf.search_text.setText("");
					cf.res = "";
					dbquery();
            	}
          });
			cf.search.setOnClickListener(new OnClickListener() {
                   public void onClick(View v) {
					// TODO Auto-generated method stub

					String temp = cf.encode(cf.search_text.getText()
							.toString());
					if (temp.equals("")) {
						cf.ShowToast("Please enter the Name or Policy Number to search.", 1);
						cf.search_text.requestFocus();
					} else {
						cf.res = temp;
						dbquery();
					}

				}

			});
			dbquery();
	 }
	 private void dbquery() {
		    int k = 1;
			cf.data = null;
			cf.inspdata = "";
			cf.sql = "select * from " + cf.policyholder;
			if (!cf.res.equals("")) {

				cf.sql += " where (GCH_PH_FirstName like '%" + cf.encode(cf.res)
						+ "%' or GCH_PH_LastName like '%" + cf.encode(cf.res)
						+ "%' or GCH_PH_Policyno like '%" + cf.encode(cf.res)
						+ "%') and GCH_PH_InspectionTypeId='" + cf.encode(cf.onlinspectionid)
						+ "' and GCH_PH_InspectorId='" + cf.encode(cf.Insp_id) + "'";
				if (cf.onlstatus.equals("UTS")) {
					cf.sql += " and  GCH_PH_Status=110 and GCH_PH_InspectionTypeId='" + cf.encode(cf.onlinspectionid) + "'";
				} else if (cf.onlstatus.equals("CAN")) {
					cf.sql += " and GCH_PH_Status=111 and GCH_PH_InspectionTypeId='" + cf.encode(cf.onlinspectionid) + "'";
				} else if (cf.onlstatus.equals("CIO")) {
					cf.sql += " and GCH_PH_SubStatus='41' and GCH_PH_InspectionTypeId='" + cf.encode(cf.onlinspectionid)
							+ "'";
				}

			} else {
				if (cf.onlstatus.equals("UTS")) {
					cf.sql += " where GCH_PH_Status=110 and GCH_PH_InspectionTypeId='" + cf.encode(cf.onlinspectionid)
							+ "' and GCH_PH_InspectorId='" + cf.encode(cf.Insp_id) + "'";
				} else if (cf.onlstatus.equals("CAN")) {
					cf.sql += " where GCH_PH_Status=111 and GCH_PH_InspectionTypeId='" + cf.encode(cf.onlinspectionid)
							+ "' and GCH_PH_InspectorId='" + cf.encode(cf.Insp_id) + "'";
				} else if (cf.onlstatus.equals("CIO")) {
					cf.sql += " where GCH_PH_SubStatus='41' and GCH_PH_InspectionTypeId='" + cf.encode(cf.onlinspectionid)
							+ "' and GCH_PH_InspectorId='" + cf.encode(cf.Insp_id) + "'";
				}

			}
			Cursor cur = cf.gch_db.rawQuery(cf.sql, null);
			cf.rws = cur.getCount();
			
			title.setText(strtit + "\n" + "Total Record : " + cf.rws);
			cf.data = new String[cf.rws];
			cf.countarr = new String[cf.rws];
			int j = 0;
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				do {
					cf.inspdata += " "
							+ cf.decode(cur.getString(cur
									.getColumnIndex("GCH_PH_FirstName"))) + " ";
					cf.inspdata += cf.decode(cur.getString(cur
							.getColumnIndex("GCH_PH_LastName"))) + " | ";
					cf.inspdata += cf.decode(cur.getString(cur
							.getColumnIndex("GCH_PH_Policyno")))
							+ " \n ";
					cf.inspdata += cf.decode(cur.getString(cur
							.getColumnIndex("GCH_PH_Address1"))) + " | ";
					cf.inspdata += cf.decode(cur.getString(cur
							.getColumnIndex("GCH_PH_City"))) + " | ";
					cf.inspdata += cf.decode(cur.getString(cur
							.getColumnIndex("GCH_PH_State"))) + " | ";
					cf.inspdata += cf.decode(cur.getString(cur
							.getColumnIndex("GCH_PH_County"))) + " | ";
					cf.inspdata += cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_Zip")))
							+ " \n ";
					cf.inspdata += cf.decode(cur.getString(cur
							.getColumnIndex("GCH_Schedule_ScheduledDate"))) + " | ";
					cf.inspdata += cf.decode(cur.getString(cur
							.getColumnIndex("GCH_Schedule_InspectionStartTime"))) + " - ";
					cf.inspdata += cf.decode(cur.getString(cur
							.getColumnIndex("GCH_Schedule_InspectionEndTime"))) + "~";


					cf.countarr[j] = cf.decode(cur.getString(cur.getColumnIndex("GCH_PH_SRID")));
					j++;

					if (cf.inspdata.contains("null")) {
						cf.inspdata = cf.inspdata.replace("null", "");
					}
					if (cf.inspdata.contains("N/A |")) {
						cf.inspdata = cf.inspdata.replace("N/A |", "");
					}
					if (cf.inspdata.contains("N/A - N/A")) {
						cf.inspdata = cf.inspdata.replace("N/A - N/A", "");
					}
				} while (cur.moveToNext());
				cf.search_text.setText("");
				display();
			} else {
				cf.onlinspectionlist.removeAllViews();
				if(cf.res.equals(""))
				{
					cf.gohome();
				}
				else
				{
					cf.ShowToast("Sorry, No results found.", 1);cf.hidekeyboard();
				}
			}

		}
	 private void display() {
		    cf.onlinspectionlist.removeAllViews();
			cf.sv = new ScrollView(this);
			cf.onlinspectionlist.addView(cf.sv);

			final LinearLayout l1 = new LinearLayout(this);
			l1.setOrientation(LinearLayout.VERTICAL);
			cf.sv.addView(l1);

			if (!cf.inspdata.equals(null) && !cf.inspdata.equals("null")
					&& !cf.inspdata.equals("")) {
				this.cf.data = cf.inspdata.split("~");
				for (int i = 0; i < cf.data.length; i++) {
					cf.tvstatus = new TextView[cf.rws];
					cf.deletebtn = new Button[cf.rws];
					LinearLayout l2 = new LinearLayout(this);
					LinearLayout.LayoutParams mainparamschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
					l2.setLayoutParams(mainparamschk);
					l2.setOrientation(LinearLayout.HORIZONTAL);
					l1.addView(l2);

					LinearLayout lchkbox = new LinearLayout(this);
					LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
					paramschk.topMargin = 8;
					paramschk.leftMargin = 20;
					paramschk.bottomMargin = 10;
					l2.addView(lchkbox);
					cf.tvstatus[i] = new TextView(this);
					cf.tvstatus[i].setTag("textbtn" + i);
					cf.tvstatus[i].setText(cf.data[i]);
					cf.tvstatus[i].setTextColor(Color.WHITE);
					cf.tvstatus[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,14);

				    lchkbox.addView(cf.tvstatus[i], paramschk);

					LinearLayout ldelbtn = new LinearLayout(this);
					LinearLayout.LayoutParams paramsdelbtn = new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			        paramsdelbtn.setMargins(0, 10, 10, 0); //left, top, right, bottom
					ldelbtn.setLayoutParams(mainparamschk);
					ldelbtn.setGravity(Gravity.RIGHT);
				    l2.addView(ldelbtn);
					cf.deletebtn[i] = new Button(this);
					cf.deletebtn[i].setBackgroundResource(R.drawable.deletebtn1);
					cf.deletebtn[i].setTag("deletebtn" + i);
					cf.deletebtn[i].setPadding(30, 0, 0, 0);
					ldelbtn.addView(cf.deletebtn[i], paramsdelbtn);

					cf.deletebtn[i].setOnClickListener(new View.OnClickListener() {
						public void onClick(final View v) {
							String getidofselbtn = v.getTag().toString();
							final String repidofselbtn = getidofselbtn.replace(
									"deletebtn", "");
							final int cvrtstr = Integer.parseInt(repidofselbtn);
							final String dt = cf.countarr[cvrtstr];
							 cf.alerttitle="Delete";
							  cf.alertcontent="Are you sure want to delete?";
							    final Dialog dialog1 = new Dialog(CompletedInspectionList.this,android.R.style.Theme_Translucent_NoTitleBar);
								dialog1.getWindow().setContentView(R.layout.alertsync);
								TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
								txttitle.setText( cf.alerttitle);
								TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
								txt.setText(Html.fromHtml( cf.alertcontent));
								Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
								Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
								btn_yes.setOnClickListener(new OnClickListener()
								{
				                	@Override
									public void onClick(View arg0) {
										// TODO Auto-generated method stub
										dialog1.dismiss();
										cf.fn_delete(dt);
										dbquery();
									}
									
								});
								btn_cancel.setOnClickListener(new OnClickListener()
								{

									@Override
									public void onClick(View arg0) {
										// TODO Auto-generated method stub
										dialog1.dismiss();
										
									}
									
								});
								dialog1.setCancelable(false);
								dialog1.show();
							
						}
					});
				}
			}

		}
	 public void clicker(View v)
	 {
		  switch(v.getId())
		  {
		  case R.id.deletehome:
			  cf.gohome();
			  break;
		  
	 case R.id.deleteall:
		  String temp="(";
		  Cursor c=cf.gch_db.rawQuery(" Select * from "+cf.policyholder+" where "+columnname+" ="+cf.statusofdata+" and GCH_PH_InspectorId="+cf.Insp_id+" and GCH_PH_InspectionTypeId ="+cf.onlinspectionid+"",null);
		  System.out.println("Cpint "+c.getCount());
		 if( c.getCount()>=1)
		 {
			 c.moveToFirst();
			 for(int i=0;i<c.getCount();i++)
			 {
				 temp+="'"+c.getString(c.getColumnIndex("GCH_PH_SRID"))+"'";
				 if((i+1)==(c.getCount()))
				 {
					 temp+=")";
					// return;
				 }
				 else
				 {
					 temp+=",";
					 c.moveToNext();
				 }
			 }
			// System.out.println("the where "+temp);
			 cf.delete_all(temp);
		 }else
		 {
			 cf.ShowToast("You don't have any record to delete  ", 1);
		 }
		  break;
		  }
	 }
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				startActivity(new Intent(CompletedInspectionList.this,Dashboard.class));
				return true;
			}
			return super.onKeyDown(keyCode, event);
		}
}
