package inspectiondepot.gch;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;


import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class CommonFunctions {
	    public String NAMESPACE = "http://tempuri.org/";
	   public String URL = "http://72.15.221.151:89/AndroidWebService.asmx"; // UAT
	 // public String URL="http://72.15.221.153:93/AndroidWebService.asmx";//Live
	    
	   public String URL_IDMA="http://72.15.221.151:89/AndroidWebService.asmx";//Uat IDMA
	//   public String URL_IDMA="http://72.15.221.153:91/AndroidWebService.asmx";//Live IDMA

	    public String MY_DATABASE_NAME = "GCHDatabases";
	    
	    public String apkrc="RC10-04-2012";
	    
		/*TABLE DECLARATION*/ 
	    public static final String inspectorlogin = "tbl_inspectorlogin";
	    public static final String policyholder = "GCH_Policyholder";
	    public static final String Onlinepolicyholder = "GCH_Online_Policyholder";
	    public static final String BIQ_table = "GCH_BuildingInformation_LBC"; 
	    public static final String Version = "GCH_Version";
	    public static final String ImageTable="GCH_Photos";
	    public static final String FeedBackInfoTable="GCH_FeedBackInfoTable";
	    public static final String FeedBackDocumentTable="GCH_FeedBackDocumentTable";  
	    public static final String Photo_caption="GCH_PhotoCaption";
	    public static final String Agent_tabble="GCH_Agent_information";
	    public static final String Cloud_table="GCH_Cloud_information";
	    public static final String Additional_table="GCH_Additional_information";
	    public static final String GHC_table= "GCH_HazardsTable";
	    public static final String IDMAVersion="ID_Version";
	    public static final String Roof_cover_type="Roof_cover_type";
	    public static final String General_Roof_information="General_Roof_Information";
	    public int sp=0,elev;
	    public EditText etcaption;
	    protected Bitmap rotated_b;
	    private static final int MODE_WORLD_READABLE = 0;
		SQLiteDatabase gch_db;
	    public String strerrorlog,Inspectiontypeid="",picpath="",name="";
        public Uri mCapturedImageURI;
        public static final int CAPTURE_PICTURE_INTENT = 0;
		private static final int SELECT_PICTURE = 0;
	    public android.text.format.DateFormat df;
		public CharSequence datewithtime;  
		public Button btn_helpclose,btn_camcaptclos;
	    public String Chk_Inspector="false",newspin,alerttitle,alertcontent;
	    public String colorname,deviceId,model,manuf,devversion,apiLevel,strschdate="",txt="";
	    public int ipAddress,wd,quesid=0;
		String exprtcnt;
		public int currnet_rotated=0;
	    public Dialog dialog1;
	    public ArrayAdapter adapter2;
	    public Intent myintent;
	    public SoapSerializationEnvelope envelope;
	    public SoapObject onlresult;
	    public RelativeLayout spinvw,capvw;
	    public String[] elevnames={"Select Elevation","Front Elevation","Right Elevation","Back Elevation","Left Elevation","Roof Images","Additional Photographs"};
	    public Spinner elevspin,captionspin;
	    public TextView cameratxt,tvcamhelp;
		Context con;
		public Toast toast;
		public String strcarr= " General Conditions and Hazards - Carrier Ordered",inspectiondate;
		public String strret= " General Conditions and Hazards ";
		public Class tempclass;
		public boolean chkbool;
		public String selectedhomeid="";
		public LinearLayout SQ_ED_type1_parrant,SQ_ED_type1,SQ_ED_type2_parrant,SQ_ED_type2;
		public Class classnames[]={GeneralConditionHazardActivity.class,HomeScreen.class,Import.class,Dashboard.class,HomeOwnerList.class,CompletedInspectionList.class};
		/*DECLARATION OF VARIABLES FOR LOGIN PAGE*/ 
		public EditText et_password;
		public AutoCompleteTextView et_username;
		public ProgressDialog pd;    
		public String Insp_id="", Insp_firstname, Insp_middlename, Insp_lastname, Insp_address, Insp_companyname, Insp_companyId, Insp_username, Insp_password, Insp_flag1, status,
		              Insp_Photo,Insp_PhotoExtn,Insp_Status,Insp_email;
		public boolean application_sta=false;
		/*DECLARATION OF VARIABLES FOR LOGIN ENDS HERE*/
		public TextView SQ_TV_type1,SQ_TV_type2;
		/*DECLARATION OF VARIABLES FOR DASHBOARD STARTS HERE*/
		 public TextView releasecode,welcome,txtversion;
		 public ImageView img_InspectorPhoto;
		 public String versionname,newcode,newversion;
		/*DECLARATION OF VARIABLES FOR DASHBOARD ENDS HERE*/
		
		 /*DECLARATION OF VARAIABLES IN IMPORT STARTS HERE*/
		 public int typeBar=1,mState,usercheck;
		 public static int RUNNING = 1;
		 public int total; // Determines type progress bar: 0 = spinner, 1 = horizontal
		 public int delay = 40; // Milliseconds of delay in the update loop
		 public int maxBarValue = 0; // Maximum value of horizontal progress bar
		 public String HomeId,InspectorId,ScheduledDate,ScheduledCreatedDate,AssignedDate,Schedulecomments,InspectionStartTime,InspectionEndTime,
	                   PH_Fname,PH_Lname,PH_Address1,PH_Address2,PH_City,PH_State,PH_Zip,PH_County,PH_Inspectionfees,
	                   PH_InsuranceCompany,PH_HPhone,PH_WPhone,PH_CPhone,PH_Email,PH_Policyno,PH_Status,PH_SubStatus,PH_Noofstories,
	                   PH_InspectionTypeId,PH_IsInspected="0",PH_IsUploaded="0",PH_EmailChk="0";
		 /*DECLARATION OF VARAIABLES IN IMPORT ENDS HERE*/
		 /*DECLARATION OF VARIABLES FOR DASHBOARD STARTS HERE*/
		 public int carrschedule,carrassign,carrcio,carruts,carrcan,carrcit,retschedule,retassign,retcio,retuts,retcan,retcit,carrtotal,rettotal=0;
		 public Button btn_carrassign,btn_carrschedule,btn_carrcit,btn_carrcio,btn_carruts,btn_carrcan,btn_carrtotal,
		               btn_retassign,btn_retschedule,btn_retcit,btn_retcio,btn_retuts,btn_retcan,btn_rettotal,
		               btn_onlcarrassign,btn_onlcarrschedule,btn_onlcarrcio,btn_onlcarruts,btn_onlcarrcan,btn_onlcarrtotal,
		               btn_onlretassign,btn_onlretschedule,btn_onlretcio,btn_onlretuts,btn_onlretcan,btn_onlrettotal;
		 public int onlcarrassign,onlcarrschedule,onlcarrcio,onlcarruts,onlcarrcan,onlcarrtotal,
		               onlretassign,onlretschedule,onlretcio,onlretuts,onlretcan,onlrettotal;
		 /*DECLARATION OF VARIABLES FOR DASHBOARD ENDS HERE*/
		 /*DECLARATION OF VARIABLES FOR ONLINE LIST STARTS HERE*/
		 public String onlstatus,onlsubstatus,onlinspectionid,res="",sql,inspdata;
		 public LinearLayout onlinspectionlist;
		 public Button search,search_clear_txt;
		 public EditText search_text;
		 public int rws,count,ht;
		 public TextView tvstatus[];
		 public ScrollView sv;
		 public Button deletebtn[];
		 public String[] data,countarr;
		 /*DECLARATION OF VARIABLES FOR ONLINE LIST ENDS HERE*/
		 /*DECLARATION OF VARIABLES FOR HOMEOWNER LIST STARTS HERE*/
		 public int columnvalue;
		 public String statusofdata;
	/** Declarations for Policyholder starts **/
		public EditText et_firstname,et_lastname,et_address1,et_zip,et_address2,et_hmephn,et_website,
		       et_city,et_wrkphn,et_state,et_cellphn,et_county,et_email,et_policyno,
		       et_inspdate,et_commenttxt;
		public Button get_inspectiondate;
		public Spinner Sch_Spinnerstart,Sch_Spinnerend;
		public TextView txt_fees,et_companyname;
		public int mYear,mMonth,mDay;public String newphone,Sh_assignedDate;
   /** Declarations for Policyholder ends **/
		/* DECLARATION OF VARIABLES FOR SCHEDULE STARTS HERE **/
		public CheckBox schchk;
		/* DECLARATION OF VARIABLES FOR SCHEDULE ENDS HERE */
		
		/* DECLARATION OF VARIABLES FOR BUILDING INFORAMTION STARTS HERE **/
		public String[] BI_GBI_year={"Select","1992","1993","1994","1995","1996","1997","1998","1999","2000","2001","2002","2003","2004","2005","2006","2007","2008","2009","2010","2011","2012","Other"},
				  BI_WSC_storey ={"Select","1 Storey","2 Storey","3 Storey","4 Storey","5 Storey"},BI_WSC_bedroom={"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","Guest bedroom","Master bedroom","Jack and Jill bedroom","Media Room","Pantry"};
		  

		/* Photos variable declaration */
		public int ph_type=51;  
		/* Photos variable declaration ends */
		public View show;
		public DisplayMetrics dm;
		public int Density=160;
	    public float dnt=(float)1.0;
	public String toptextvalue="";
	public String redcolor ="<font color=red> * </font>";
	public String redcolortext ="<font color=red></font>";
	public String yearbuilt;
	//public boolean application_sta=false;
	String year[] = { "--Select--","Other","2012","2011","2010","2009","2008","2007","2006","2005","2004","2003","2002","2001","2000","1999","1998","1997","1996",
			"1995","1994","1993","1992","1991","1990","1989","1988","1987","1986","1985","1984","1983","1982","1981","1980","1979","1978","1977","1976",
			"1975","1974","1973","1972","1971","1970","1969","1968","1967","1966","1965","1964","1963","1962","1961","1960","1959","1958","1957","1956",
			"1955","1954","1953","1952","1951","1950","1949","1948","1947","1946","1945","1944","1943","1942","1941","1940","1939","1938","1937","1936",
			"1935","1934","1933","1932","1931","1930","1929","1928","1927","1926","1925","1924","1923","1922","1921","1920","1919","1918","1917","1916",
			"1915","1914","1913","1912","1911","1910","1909","1908","1907","1906","1905","1904","1903","1902","1901","1900"};
	int slenght=0;
	String wfi_str;
	CommonFunctions(Context con) {
		
		gch_db = con.openOrCreateDatabase(MY_DATABASE_NAME, 1, null);
		this.con = con;
		df = new android.text.format.DateFormat();
		datewithtime = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		/***checking for the All risk report application **/
		try
		{
		final PackageManager pm = con.getPackageManager();
        //get a list of installed apps.
		        List<ApplicationInfo> packages = pm
		                .getInstalledApplications(PackageManager.GET_META_DATA);
		
        for (ApplicationInfo packageInfo : packages) {

	          
			  if(packageInfo.packageName.toString().trim().equals("com.idinspection")) // for checking the main app has installed  or not 
	            {
				  application_sta=true; // check if the main appliccation IDinspection has installed  or not 
	            }
        }
		}catch(Exception e)
		{
			System.out.println("Problem in the application sta"+e.getMessage());
		}
        /***checking for the All risk report application Ends here  **/
		get_density_frm_db(); 
	}
	public void get_density_frm_db()
	{
		
		try
		{
			
			  /***checking for the All risk report application Ends here  **/
	        Activity activity = (Activity) this.con;
	        /** Set the medium density for the Screen Starts**/
			dm = new DisplayMetrics(); // Creat new Display metrics class 
			activity.getWindowManager().getDefaultDisplay().getMetrics(dm); // set the Current Display metrics value to the new   
			/**Seting the customized value to the Display metrics**/
			
			
			int density=((dm.widthPixels*160)/1024); // Calculate the densitydpi for the currnet width 
			
			           
			if(density>=120)
			{
				    
			float dnt= (float) ((float)density/160.00); // calcul ate the  density for the Width 
			 
			dm.density=dnt;
			dm.densityDpi=density;
			dm.scaledDensity=dnt;    
			dm.xdpi=density;
			dm.ydpi=density;
			Density=density;
			this.dnt=dnt;
			
			//dm.widthPixels=1024;
			/**Seting the customized value to the Display metrics Ends**/
			activity.getApplicationContext().getResources().getDisplayMetrics().setTo(dm); // Set the new class to the display metrics
					
			}
			else
			{
				dm.density=dnt;
				dm.densityDpi=Density;
				dm.scaledDensity=dnt;    
				dm.xdpi=Density;
				dm.ydpi=Density;
				Density=Density;
				this.dnt=dnt;
				
				//dm.widthPixels=1024;
				/**Seting the customized value to the Display metrics Ends**/
				activity.getApplicationContext().getResources().getDisplayMetrics().setTo(dm); // Set the new class to the display metrics
			}
			//db.execSQL("Insert into "+Settings_table+" (SId,S_name,S_value) values (1,'Density','"+density+":"+dnt+"') ");
			//}
		}
		catch (Exception e) {
			// TODO: handle exception
			System.out.println("The error Was "+e.getMessage());
		}
	}
	public void showhelp(String alerttitle,String alertcontent) {
		// TODO Auto-generated method stub
		
		final Dialog dialog = new Dialog(this.con,android.R.style.Theme_Translucent_NoTitleBar);
		dialog.getWindow().setContentView(R.layout.alert);
		TextView txt = (TextView) dialog.findViewById(R.id.txtid);
		txt.setText(Html.fromHtml(alertcontent));
		Button btn_helpclose = (Button) dialog.findViewById(R.id.helpclose);
		btn_helpclose.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
			
		});
		dialog.setCancelable(true);
		dialog.show();
	}

	public void ShowToast(String s, int i) {
		
		switch(i)
		{
		case 0:
			colorname="#000000";
			break;
		case 1:
			colorname="#890200";
			break;
		case 2:
			colorname="#F3C3C3";
			break;
		}
		 toast = new Toast(this.con);
		 LayoutInflater inflater = (LayoutInflater)this.con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				 View layout = inflater.inflate(R.layout.toast, null);
		
		TextView tv = (TextView) layout.findViewById(
				R.id.text);
		tv.setTextColor(Color.parseColor(colorname));
		toast.setGravity(Gravity.CENTER, 0, 0);
		tv.setText(s);
		//tv.setTextSize(Typeface.BOLD);
		
		//toast.setGravity(Gravity.BOTTOM, 10, 80);
		toast.setView(layout);
		fireLongToast();
		

	}
	
	private void fireLongToast() {

	        Thread t = new Thread() {
	            public void run() {
	                int count = 0;
	                try {
	                    while (true && count < 10) {
	                        toast.show();
	                        sleep(3);
	                        count++;

	                        // do some logic that breaks out of the while loop
	                    }
	                } catch (Exception e) {
	                   
	                }
	            }
	        };
	        t.start();
	    }

	public void Device_Information()
	{
		deviceId = Settings.System.getString(this.con.getContentResolver(), Settings.System.ANDROID_ID);
		model = android.os.Build.MODEL;
		manuf = android.os.Build.MANUFACTURER;
		devversion = android.os.Build.VERSION.RELEASE;
		apiLevel = android.os.Build.VERSION.SDK;
		WifiManager wifiManager = (WifiManager)(this.con.getSystemService(this.con.WIFI_SERVICE));
		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
		ipAddress = wifiInfo.getIpAddress();
	}
	public void Create_Table(int select) {
		switch (select) {
		case 1:
			/*INSPECTOR LOGIN*/
			try {
				gch_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ inspectorlogin
						+ " (Id INTEGER PRIMARY KEY AUTOINCREMENT,Fld_InspectorId varchar(50),Fld_InspectorFirstName varchar(50),Fld_InspectorMiddleName varchar(50),Fld_InspectorLastName varchar(50),Fld_InspectorAddress varchar(150),Fld_InspectorCompanyName varchar(150),Fld_InspectorCompanyId varchar(100),Fld_InspectorUserName varchar(50),Fld_InspectorPassword varchar(50),Fld_InspectorPhotoExtn Varchar(10),Android_status bit,Fld_InspectorFlag bit,Fld_Remember_pass  bit DEFAULT 0,Fld_InspectorEmail varchar(100),Insp_sign_img varchar(50),Insp_sign_img_ext varchar(50) DEFAULT('.jpg'),Insp_PrimaryLicenseType varchar(50),Insp_PrimaryLicenseNumber varchar(50));");
			} catch (Exception e) {
				strerrorlog="Inspector Login table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Inspector Login Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
				
			}
			break;
		case 2:
			/*VERSION TABLE*/ 
		     try {
				// gch_db("DROP TABLE IF  EXISTS "
				// + version + " ");
		    	 gch_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Version
						+ "(VId INTEGER PRIMARY KEY Not null,GCH_VersionCode varchar(50),GCH_VersionName varchar(50));");

			} catch (Exception e) {
				strerrorlog="Version Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Inspector Login Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
				
			}
			break;
		case 3:
			/* POLICYHOLDER */
			try {
				gch_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ policyholder
						+ " (Ph_Id INTEGER PRIMARY KEY AUTOINCREMENT,GCH_PH_InspectorId varchar(50) NOT NULL,GCH_PH_SRID varchar(50) NOT NULL,GCH_PH_FirstName varchar(100) NOT NULL,GCH_PH_LastName varchar(100) NOT NULL,GCH_PH_Address1 varchar(100),GCH_PH_Address2 varchar(100),GCH_PH_City varchar(100),GCH_PH_Zip varchar(100),GCH_PH_State varchar(100),GCH_PH_County varchar(100),GCH_PH_Policyno varchar(100),GCH_PH_Inspectionfees varchar(100),GCH_PH_InsuranceCompany varchar(100),GCH_PH_HomePhone varchar(100),GCH_PH_WorkPhone varchar(100),GCH_PH_CellPhone varchar(100),GCH_PH_NOOFSTORIES varchar(5),GCH_PH_WEBSITE varchar(100),GCH_PH_Email varchar(100),GCH_PH_EmailChkbx Integer,GCH_PH_InspectionTypeId Integer,GCH_PH_IsInspected Integer,GCH_PH_IsUploaded Integer,GCH_PH_Status Integer,GCH_PH_SubStatus Integer,GCH_Schedule_ScheduledDate varchar(100),GCH_Schedule_InspectionStartTime varchar(100),GCH_Schedule_InspectionEndTime varchar(100),GCH_Schedule_Comments varchar(500),GCH_Schedule_ScheduleCreatedDate varchar(100),GCH_Schedule_AssignedDate varchar(100),GCH_ScheduleFlag Integer,GCH_YearBuilt varchar(100),GCH_ContactPerson Varchar(50),BuidingSize Varchar(20));");
			} catch (Exception e) {
				strerrorlog="PolicyHolder table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of PolicyHolder Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
				
			}
			break;
		case 5:
			/*Building information  */
			try{
				
				gch_db.execSQL("CREATE TABLE IF NOT EXISTS "+BIQ_table+" (GCH_BI_Id INTEGER PRIMARY KEY AUTOINCREMENT,GCH_BI_Inspectorid varchar(50),GCH_BI_Homeid varchar(50),GCH_BI_INSPECTED_DATE varchar(50),GCH_BI_START_TIME varchar(50),GCH_BI_END_TIME varchar(50),GCH_FINIGCH_TIME varchar(50),GCH_BI_NO_STORIES varchar(50),GCH_BI_YOC varchar(50),GCH_BI_INS_CARRIER varchar(50),GCH_BI_INSURED_IS varchar(250),GCH_BI_INS_OTHER varchar(150)," +
						"GCH_BI_OCCUPIED varchar,GCH_BI_OCCUPIEDPER INTEGER,GCH_BI_VACANT varchar,GCH_BI_VACANTPER INTEGER,GCH_BI_NOT_DETERMINED varchar,GCH_BI_OCC_TYPE varchar(50),GCH_BI_OCC_OTHER varchar(50),GCH_BI_OBSERV_TYPE varchar(50),GCH_BI_OBSERV_OTHER varchar(50),GCH_BI_NEIGH_IS varchar(50),GCH_BI_NEIGH_OTHER varchar(50),GCH_BI_BUIL_SIZE INTEGER,GCH_BI_NSTORIES varchar(50),GCH_BI_BUILT_YR varchar(50),GCH_BI_YR_BUILT_OPTION varchar(50)," +
						"GCH_BI_YR_BUILT_OTHER varchar(50),GCH_BI_PERM_CONFIRMED varchar(20),GCH_BI_PERM_CONF_DESC varchar(150),GCH_BI_ADDI_STRU varchar(20),GCH_BI_ADDI_STRU_DESC varchar(150),GCH_BI_BALCONY_PRES varchar(20),GCH_BI_NBALCONY_PRES INTEGER,GCH_BI_BALCONY_RAIL_PRES varchar(20),GCH_BI_LOC1 varchar(20),GCH_BI_LOC1_DESC varchar(200),GCH_BI_LOC2 varchar(20),GCH_BI_LOC2_DESC varchar(200),GCH_BI_LOC3 varchar(20),GCH_BI_LOC3_DESC varchar(200),GCH_BI_LOC4 varchar(20),GCH_BI_LOC4_DESC varchar(200),GCH_BI_OTHER_LOC varchar(750),GCH_BI_OBSERV1 varchar(20)," +
						"GCH_BI_OBSERV2 varchar(20),GCH_BI_OBSERV2_DESC varchar(750),GCH_BI_OBSERV3 varchar(20),GCH_BI_OBSERV3_DESC varchar(750),GCH_BI_OBSERV4 varchar(20),GCH_BI_OBSERV4_DESC varchar(750),GCH_BI_OBSERV5 varchar(20),GCH_BI_OBSERV5_DESC varchar(750),GCH_BI_WSC_PRESENT varchar,GCH_BI_CONC_UNREIN varchar(50),GCH_BI_CONC_UNREINPER varchar(10),GCH_BI_CONC_REIN varchar(50),GCH_BI_CONC_REINPER varchar(10),GCH_BI_SOLID_CONC varchar(50),GCH_BI_SOILD_CONCPER varchar(10),GCH_BI_WFRAME varchar(50)," +
						"GCH_BI_WFRAMERPER varchar(10),GCH_BI_REIN_MASONRY varchar(50),GCH_BI_REIN_MASONRYPER varchar(10),GCH_BI_UNREIN_MASONRY varchar(50),GCH_BI_UNREIN_MASONRYPER varchar(10),GCH_BI_OTHER_WSTITLE varchar(50),GCH_BI_OTHER_WS varchar(50),GCH_BI_OTHER_WALLPER varchar(10),GCH_BI_WALLCLADD_PRES varchar,GCH_BI_ALUM_SLIDING varchar(50),GCH_BI_VINYL_SLIDING varchar(50),GCH_BI_WOOD_SLIDING varchar(50),GCH_BI_CEMENT_FIBER varchar(50)" +
						",GCH_BI_STUCCO varchar(50),GCH_BI_BRICKVENEER varchar(50),GCH_BI_PAINTEDBLOCK varchar(50),GCH_BI_OTHER_WCLADTITLE1 varchar(50),GCH_BI_OTHER_WCLAD1 varchar(50),GCH_BI_OTHER_WCLADTITLE2 varchar(50),GCH_BI_OTHER_WCLAD2 varchar(50),GCH_BI_OTHER_WCLADTITLE3 varchar(50),GCH_BI_OTHER_WCLAD3 varchar(50),GCH_BI_ISO_CLASSPRES varchar,GCH_BI_FRAME varchar(10),GCH_BI_JOISTED varchar(10),GCH_BI_NONCOMBUST varchar(10)," +
						"GCH_BI_MASON_NONCOMBUST varchar(10),GCH_BI_MODIFY_FIRERESIST varchar(10),GCH_BI_FIRERESIST varchar(10),GCH_BI_BUILD_COMMENTS varchar(800),GCH_BI_CREATEDON Datetime);");
			}
			catch (Exception e) {
				strerrorlog="Building question table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
				
			}
		break;

		case 6:
			/* ONLINE POLICYHOLDER */
			try {
				gch_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Onlinepolicyholder
						+ " (On_Ph_Id INTEGER PRIMARY KEY AUTOINCREMENT,GCH_OL_PH_InspectorId varchar(50) NOT NULL,GCH_OL_PH_SRID varchar(50) NOT NULL,GCH_OL_PH_FirstName varchar(100) NOT NULL,GCH_OL_PH_LastName varchar(100) NOT NULL,GCH_OL_PH_Address1 varchar(100),GCH_OL_PH_City varchar(100),GCH_OL_PH_Zip varchar(100),GCH_OL_PH_State varchar(100),GCH_OL_PH_County varchar(100),GCH_OL_PH_Policyno varchar(100),GCH_OL_PH_InspectionTypeId Integer,GCH_OL_PH_Status Integer,GCH_OL_PH_SubStatus Integer,GCH_OL_Schedule_ScheduledDate varchar(100),GCH_OL_Schedule_InspectionStartTime varchar(100),GCH_OL_Schedule_InspectionEndTime varchar(100));");
			} catch (Exception e) {
				strerrorlog="Online PolicyHolder table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of PolicyHolder Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
				
			}
			break;
		case 7:
			/* General hazards conditions  */
			try{
				
				gch_db.execSQL("CREATE TABLE IF NOT EXISTS "+GHC_table+" (GCH_BI_Id INTEGER PRIMARY KEY AUTOINCREMENT,GCH_HZ_Inspectorid varchar(50),GCH_HZ_Homeid varchar(50),GCH_HZ_ISPOOL_PRES varchar DEFAULT ( '1' ),GCH_HZ_POOLPRES varchar(50) DEFAULT ( '' ),GCH_HZ_HOTTUB varchar(10) DEFAULT ( '' ),GCH_HZ_PPS varchar(10) DEFAULT ( '' ),GCH_HZ_HOTTUBCOVER varchar(10) DEFAULT ( '' ),GCH_HZ_POOLSLIDE varchar(10) DEFAULT ( '' ),GCH_HZ_DIVINGBOARD varchar(10) DEFAULT ( '' ),GCH_HZ_PERIPOOL varchar(10) DEFAULT ( '' ),GCH_HZ_POOLENCLOS varchar(10) DEFAULT ( '' ),GCH_HZ_EMPTYPOOLPRESENT varchar(10) DEFAULT ( '' ), GCH_HZ_ISPERI_PF varchar DEFAULT ( '1' )," +
						"GCH_HZ_PERI_PF varchar(50) DEFAULT ( '' ),GCH_HZ_OTHERPERI_PF varchar(50) DEFAULT ( '' ),GCH_HZ_SELFLATCH varchar(20) DEFAULT ( '' ),GCH_HZ_PROF_INST varchar(20) DEFAULT ( '' ),GCH_HZ_PF_DISREPAIR varchar(20) DEFAULT ( '' ),GCH_HZ_VICIOUS varchar(10) DEFAULT ( '' ),GCH_HZ_VICIOUSDESC varchar(150) DEFAULT ( '' )," +
						"GCH_HZ_LIVESTOCK varchar(10) DEFAULT ( '' ),GCH_HZ_LIVESTOCKDESC varchar(150) DEFAULT ( '' ),GCH_HZ_OVERHANGING varchar(10) DEFAULT ( '' ),GCH_HZ_TRAMPOLINE varchar(10) DEFAULT ( '' ),GCH_HZ_SKATEBOARD varchar(10) DEFAULT ( '' ),GCH_HZ_BICYCLE varchar(50) DEFAULT ( '' ),GCH_HZ_GROUNDPOOL varchar(10) DEFAULT ( '' ),GCH_HZ_TRIPHAZARD varchar(50) DEFAULT ( '' ),GCH_HZ_TRIPHAZARDNOTES varchar(50) DEFAULT ( '' ),GCH_HZ_UNSAFESTAIRWAY varchar(10) DEFAULT ( '' ),GCH_HZ_PORCHANDDESK varchar(10) DEFAULT ( '' ),GCH_HZ_NONSTDCONST varchar(10) DEFAULT ( '' ),GCH_HZ_OUTDOORAPPL varchar(10) DEFAULT ( '' ),GCH_HZ_OPENFOUND varchar(20) DEFAULT ( '' ),GCH_HZ_WOODSINGHLED varchar(10) DEFAULT ( '' ),GCH_HZ_EXCESSDEBRIS varchar(10) DEFAULT ( '' ),GCH_HZ_BUISPREMISES varchar(10) DEFAULT ( '' ),GCH_HZ_BUISPREMDESC varchar(150) DEFAULT ( '' ),GCH_HZ_GENDISREPAIR varchar(10) DEFAULT ( '' ),GCH_HZ_PROP_DAMAGE varchar(10) DEFAULT ( '' ),GCH_HZ_STRUPARTIAL varchar(10) DEFAULT ( '' ),GCH_HZ_INOPERATIVE varchar(10) DEFAULT ( '' ),GCH_HZ_RECDRYWALL varchar(10) DEFAULT ( '' ),GCH_HZ_CHINESEDRYWALL varchar(10) DEFAULT ( '' ),GCH_HZ_CONFDRYWALL varchar(10) DEFAULT ( '' ),GCH_HZ_NONSECURITY varchar(10) DEFAULT ( '' ),GCH_HZ_NONSMOKE varchar(10) DEFAULT ( '' ),GCH_HZ_RECDRYWALL_BSI varchar(200) DEFAULT ( '' ),GCH_HZ_CHINESEDRYWALL_BSI varchar(200) DEFAULT ( '' ),GCH_HZ_CONFDRYWALL_BSI varchar(200) DEFAULT ( '' ),GCH_HZ_NONSECURITY_BSI varchar(200) DEFAULT ( '' ),GCH_HZ_NONSMOKE_BSI varchar(200) DEFAULT ( '' ),GCH_HZ_COMMENTS varchar(1000) DEFAULT ( '' )) ;");
			}
			catch (Exception e) {
				System.out.println("error in cretin"+e.getMessage());
				strerrorlog="Building question table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
				
			}
		break;
		case 10:
			/** Feedback info table **/
			try {
				// db("DROP TABLE IF  EXISTS "
				// + FeedBackInfoTable + " ");
				gch_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ FeedBackInfoTable
						+ " (GCH_FI_Wsid INTEGER PRIMARY KEY Not null,GCH_FI_Srid Varchar(50) ,GCH_FI_IsCusServiceCompltd Bit Not null Default(0),GCH_FI_PresentatInspection Varchar(50) ,GCH_FI_IsInspectionPaperAvbl Bit Not null Default(0),GCH_FI_IsManufacturerInfo Bit Not null Default(0),GCH_FI_FeedbackComments Varchar(2000),GCH_FI_CreatedOn Datetime Null,GCH_FI_othertxt Varchar(50));");

			} catch (Exception e) {
				
			}

			break;
		case 11:
			/** Creating feedbackdocuments table **/
			try {
				// db("DROP TABLE IF  EXISTS "
				// + FeedBackDocumentTable + " ");
				gch_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ FeedBackDocumentTable
						+ " (GCH_FI_DocumentId INTEGER PRIMARY KEY Not null,GCH_FI_D_SRID Varchar(50) ,GCH_FI_D_DocumentTitle Varchar(50),GCH_FI_D_FileName Varchar(50) ,GCH_FI_D_Nameext Varchar(150),GCH_FI_D_ImageOrder INTEGER Not null default(0),GCH_FI_D_IsOfficeUse Bit Not null Default(0),GCH_FI_D_CreatedOn Datetime Null,GCH_FI_D_ModifiedDate Datetime Null);");

			} catch (Exception e) {
				
			}
			break;
			 case 15:
			 /** creating image table **/
			
			  try { gch_db.execSQL("CREATE TABLE IF NOT EXISTS " + ImageTable +
			 " (GCH_IM_ID INTEGER PRIMARY KEY Not null,GCH_IM_SRID Varchar(50) ,GCH_IM_Ques_Id INTEGER Not null Default(0),GCH_IM_Elevation INTEGER Not null Default(0),GCH_IM_ImageName Varchar(150) ,GCH_IM_Nameext Varchar(150),GCH_IM_Description Varchar(150),GCH_IM_CreatedOn Datetime Null,GCH_IM_ModifiedOn Datetime Null,GCH_IM_ImageOrder INTEGER Not null default(0));"
			  );
			  
			  } catch (Exception e) {
			  strerrorlog="images table creation table not created";
			  Error_LogFile_Creation(strerrorlog+" "+" at "+
			  this.con.getClass().getName
			  ().toString()+" "+" in the stage of images Table Creation at "
			  +" "+datewithtime+" "+"in apk"+" "+apkrc); } break; 
			  
			 
			 case 16:
			 /** creating Photocaption table **/
			
			  try { gch_db.execSQL("CREATE TABLE IF NOT EXISTS " + Photo_caption +
			  " (GCH_IMP_ID INTEGER PRIMARY KEY Not null,GCH_IMP_INSP_ID Varchar(50) ,GCH_IMP_Caption Varchar(150) ,GCH_IMP_Elevation INTEGER Not null Default(0),GCH_IMP_ImageOrder Varchar(150));"
			  ); } catch (Exception e) {
			  strerrorlog="images table creation table not created";
			  Error_LogFile_Creation(strerrorlog+" "+" at "+
			  this.con.getClass().getName
			  ().toString()+" "+" in the stage of images Table Creation at "
			  +" "+datewithtime+" "+"in apk"+" "+apkrc); } 
			  break;
			 
	
	
		case 17: 
			/** creating Agent information table **/
			try
			{
				gch_db.execSQL("CREATE TABLE IF NOT EXISTS "
			    + Agent_tabble + " (GCH_AI_id INTEGER PRIMARY KEY Not null,GCH_AI_SRID  Varchar(50) ,GCH_AI_AgencyName  Varchar(50) ,GCH_AI_AgentName  Varchar(50) ,GCH_AI_AgentAddress  Varchar(50) ,GCH_AI_AgentAddress2  Varchar(50) ,GCH_AI_AgentCity  Varchar(50) ,GCH_AI_AgentCounty  Varchar(50) ,GCH_AI_AgentRole  Varchar(50) ,GCH_AI_AgentState  Varchar(50) ,GCH_AI_AgentZip  Varchar(50) ,GCH_AI_AgentOffPhone  Varchar(50) ,GCH_AI_AgentContactPhone  Varchar(50) ,GCH_AI_AgentFax  Varchar(50) ,GCH_AI_AgentEmail  Varchar(50) ,GCH_AI_AgentWebSite Varchar(50));");

			} catch (Exception e) {
				strerrorlog="Agent info  table creation table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Agent info Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			
	break;
		case 18: 
			/** creating Agent information table **/
			try
			{
				gch_db.execSQL("CREATE TABLE IF NOT EXISTS "
			    + Cloud_table + " (GCH_CI_id INTEGER PRIMARY KEY Not null, SRID Varchar(100) ,  assignmentId  Varchar(100) , assignmentMasterId  Varchar(100) , inspectionPolicy_Id  Varchar(100) , " +
			    "priorAssignmentId  Varchar(100) , inspectionType  Varchar(100) , assignmentType  Varchar(100) , policyId  Varchar(100) , policyVersion  Varchar(100) , " +
			    "policyEndorsement  Varchar(100) , policyEffectiveDate  Varchar(100) , policyForm  Varchar(100) , policySystem  Varchar(100) , lob  Varchar(100) , " +
			    "structureCount  Varchar(100) , structureNumber  Varchar(100) , structureDescription  Varchar(100) , propertyAddress  Varchar(100) , propertyAddress2  " +
			    "Varchar(100) , propertyCity  Varchar(100) , propertyState  Varchar(100) , propertyCounty  Varchar(100) , propertyZip  Varchar(100) , insuredFirstName  " +
			    "Varchar(100) , insuredLastName  Varchar(100) , insuredHomePhone  Varchar(100) , insuredWorkPhone  Varchar(100) , insuredAlternatePhone  Varchar(100) , " +
			    "insuredEmail  Varchar(100) , insuredMailingAddress  Varchar(100) , insuredMailingAddress2  Varchar(100) , insuredMailingCity  Varchar(100) , " +
			    "insuredMailingState  Varchar(100) , insuredMailingZip  Varchar(100) , agencyID  Varchar(100) , agencyName  Varchar(100) , agencyPhone  Varchar(100) ," +
			    " agencyFax  Varchar(100) , agencyEmail  Varchar(100) , agencyPrincipalFirstName  Varchar(100) , agencyPrincipalLastName  Varchar(100) ," +
			    " agencyPrincipalEmail  Varchar(100) , agencyFEIN  Varchar(100) , agencyMailingAddress  Varchar(100) , agencyMailingAddress2  Varchar(100) ," +
			    " agencyMailingCity  Varchar(100) , agencyMailingState  Varchar(100) , agencyMailingZip  Varchar(100) , agentID  Varchar(100) , agentFirstName " +
			    " Varchar(100) , agentLastName  Varchar(100) , agentEmail  Varchar(100) , previousInspectionDate  Varchar(100) , previousInspectionCompany  " +
			    "Varchar(100) , previousInspectorFirstName  Varchar(100) , previousInspectorLastName  Varchar(100) , previousInspectorLicense  Varchar(100) ," +
			    " yearBuilt  Varchar(100) , squareFootage  Varchar(100) , numberOfStories  Varchar(100) , construction  Varchar(100) , roofCovering  Varchar(100)" +
			    " , roofDeckAttachment  Varchar(100) , roofWallAttachment  Varchar(100) , roofShape  Varchar(100) , secondaryWaterResistance  Varchar(100) , " +
			    "openingCoverage  Varchar(100) , buildingType Varchar(100));");

			} catch (Exception e) {
				strerrorlog="Cloud info  table creation not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of cloud info Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			
	break;
	
		case 19: 
			/** creating Addiitonal information table **/
			try
			{
				gch_db.execSQL("CREATE TABLE IF NOT EXISTS "
			    + Additional_table + " (GCH_AD_id INTEGER PRIMARY KEY Not null,GCH_AD_SRID  Varchar(50) ,GCH_AD_INSPID Varchar(50),GCH_AD_IsRecord boolean,GCH_AD_UserTypeName Varchar(50),GCH_AD_ContactEmail varchar(50),GCH_AD_PhoneNumber varchar(15),GCH_AD_MobileNumber varchar(15),GCH_AD_BestTimetoCallYou varchar(50),GCH_AD_FirstChoice varchar(100),GCH_AD_SecondChoice varchar(50),GCH_AD_ThirdChoice varchar(50),SF_AD_FeedbackComments varchar(500));");

			} catch (Exception e) {
				strerrorlog="Additional info  table creation not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Agent info Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			
	break;
		case 20:/** CREATING IDMA VERSION TABLE **/
			try {
				
				gch_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ IDMAVersion
						+ "(VId INTEGER PRIMARY KEY Not null,ID_VersionCode varchar(50),ID_VersionName varchar(50),ID_VersionType varchar(50));");

			} catch (Exception e) {
				
				
			}
			break;
		case 21:/** CREATING Roof Cover type TABLE **/
			try {
				
				gch_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Roof_cover_type
						+ "(RCT_Id INTEGER PRIMARY KEY Not null,RC_RCT_SRID Varchar(100), RC_RCT_RCT Varchar(100) Default(''), RC_RCT_RCT_Other Varchar(100) Default('') , RC_RCT_PAD Varchar(100) Default('')," +
						"RC_RCT_YIR Varchar(100) Default(''), RC_RCT_RS Varchar(100) Default(''), RC_RCT_RSh Varchar(100) Default(''), RC_RCT_RSh_Other Varchar(100) Default(''), RC_RCT_RSt Varchar(100) Default(''), " +
						"RC_RCT_RSt_Other Varchar(100) Default(''), RC_RCT_PD Varchar(100) Default(''),RC_RCT_RSh_per Varchar(100) Default(''),RC_RCT_NIP Varchar(2) Default(0),RC_RCT_Domi Varchar(2) Default(0));");

			} catch (Exception e) {
				System.out.println("issues occure "+e.getMessage());
				
			}
			break;
		case 22:/** CREATING Roof section type TABLE **/
			try {
				
				gch_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ General_Roof_information
						+ " (RC_Id INTEGER PRIMARY KEY Not null,RC_SRID Varchar(100), RC_Condition_type Varchar(100) Default(''), RC_Condition_type_DESC Varchar(1000) Default('') ,RC_Condition_type_val Varchar(50) Default('No'),Created_date Varchar(50) Default(''),RC_Condition_type_order Varchar(5) Default(''));");

			} catch (Exception e) {
				System.out.println("issues occure "+e.getMessage());
				
			}
			break;
			

		}

	}
	public SoapObject export_header(String string)
	{
		envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		SoapObject add_property = new SoapObject(NAMESPACE,string);
		return add_property;
	}
    public void Error_LogFile_Creation(String strerrorlog2)
    {
        try {
 			
 			 DataOutputStream out = new DataOutputStream(this.con.openFileOutput("errorlogfile.txt", this.con.MODE_APPEND));
 			 out.writeUTF(strerrorlog2);
 			 
    	 }
    	 catch(Exception e)
    	 {
    		 
    	 }
    }
    public String encode(String oldstring) {
		if(oldstring==null)
		{
			oldstring="";
		}
		try {
			oldstring = URLEncoder.encode(oldstring, "UTF-8");
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		return oldstring;

	}
    public String decode(String newstring) {
    	
		try {
			newstring = URLDecoder.decode(newstring, "UTF-8");
			
			if(newstring==null)
			{
				System.out.println("The new string "+newstring);
				newstring="";
			}
			return newstring;
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
		
    	
	}
    public Cursor SelectTablefunction(String tablename, String wherec) {
		Cursor cur = gch_db.rawQuery("select * from " + tablename + " " + wherec, null);
		return cur;

	}
    public SoapObject Calling_WS(String string, String string2, String string3) throws SocketException,IOException,NetworkErrorException,TimeoutException, XmlPullParserException,Exception
    {
    	SoapObject request = new SoapObject(NAMESPACE,string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("UserName",string);
		request.addProperty("Password",string2);
		envelope.setOutputSoapObject(request);;
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE+string3,envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		return result;
		
    }

	public void show_ProgressDialog(String string) {
		// TODO Auto-generated method stub
		String source = "<b><font color=#00FF33>"+string+" . Please wait...</font></b>";
		pd = ProgressDialog.show(this.con, "", Html.fromHtml(source), true);
	}

	public SoapObject Calling_WS1(String id, String string) throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException,SocketTimeoutException, XmlPullParserException {
		// TODO Auto-generated method stub
		
		SoapObject request = new SoapObject(NAMESPACE,string);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("inspectorid",id);
		envelope.setOutputSoapObject(request);
		System.out.println("the property ="+request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE+string,envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		System.out.println("Result "+envelope.getResponse());
		return result;
	}

	public String export_footer(SoapSerializationEnvelope envelope22,
			SoapObject add_property, String string) {
		// TODO Auto-generated method stub
		envelope22.setOutputSoapObject(add_property);
		HttpTransportSE androidHttpTransport11 = new HttpTransportSE(URL);
		try {
			androidHttpTransport11.call(NAMESPACE+string, envelope22);
			String result = String.valueOf(envelope22.getResponse());
			return result;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "false";
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "false";
		}
		
		
	}

	public void getInspectorId() {

		// TODO Auto-generated method stub
		try {
			Cursor cur = this.SelectTablefunction(this.inspectorlogin,
					" where Fld_InspectorFlag=1");
			cur.moveToFirst();
			if (cur != null) {
				do {
					Insp_id = decode(cur.getString(cur.getColumnIndex("Fld_InspectorId")));
					//Insp_id="1655";
					Insp_firstname = decode(cur.getString(cur.getColumnIndex("Fld_InspectorFirstName")));
					Insp_lastname = decode(cur.getString(cur.getColumnIndex("Fld_InspectorLastName")));
					Insp_address = decode(cur.getString(cur.getColumnIndex("Fld_InspectorAddress")));
					Insp_companyname = decode(cur.getString(cur.getColumnIndex("Fld_InspectorCompanyName")));
					Insp_email = decode(cur.getString(cur.getColumnIndex("Fld_InspectorEmail")));
					
					//Insp_firstname="ANDREW";Insp_lastname="DULCIE";
				} while (cur.moveToNext());
			}
			cur.close();
		} catch (Exception e) {
			Error_LogFile_Creation(e.getMessage()+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of getting inspector details from Inspector Login table at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
		}
	}

	public String getdeviceversionname() {
		// TODO Auto-generated method stub
		try {
			versionname = this.con.getPackageManager().getPackageInfo(this.con.getPackageName(), 0).versionName;
			
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			Error_LogFile_Creation(e.getMessage()+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of getting device version name at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
		}
		return versionname;
	}

	public void fn_logout(final String insp_id2) {
		// TODO Auto-generated method stub
		final Dialog dialog1 = new Dialog(this.con,android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alert);
		LinearLayout lin = (LinearLayout)dialog1.findViewById(R.id.remember_password);
		lin.setVisibility(show.VISIBLE);
		TextView txttitle = (TextView) dialog1.findViewById(R.id.RP_txthelp);
		txttitle.setText("Logout");//txttitle.setTextAppearance(con,Typeface.BOLD);
		Button btn_helpclose = (Button) dialog1.findViewById(R.id.RP_close);
		TextView txt = (TextView) dialog1.findViewById(R.id.RP_txtid);
		txt.setText("Are you sure,Do you want to Exit the application?");
		Button btn_yes= (Button) dialog1.findViewById(R.id.RP_yes);
		Button btn_no= (Button) dialog1.findViewById(R.id.RP_no);
		btn_no.setText("   No   ");
		
		btn_helpclose.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog1.setCancelable(true);
				dialog1.dismiss();
			}
			
		});
		btn_no.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog1.setCancelable(true);
				dialog1.dismiss();
			}
			
		});
		btn_yes.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog1.setCancelable(true);
				try{gch_db.execSQL("UPDATE " + inspectorlogin
						+ " SET Fld_InspectorFlag=0" + " WHERE Fld_InspectorId ='"+ insp_id2 + "'");
				Intent loginpage;
				
				if(application_sta)
				{
					loginpage = new Intent(Intent.ACTION_MAIN);
					loginpage.setComponent(new ComponentName("com.idinspection","com.idinspection.LogOut"));
				}
				else
				{
				loginpage = new Intent(con,GeneralConditionHazardActivity.class);
				
				}
				con.startActivity(loginpage);
				
				}catch(Exception e)
				{
					Error_LogFile_Creation(e.getMessage()+" "+" at "+ con.getClass().getName().toString()+" "+" in the stage of logout(catch) at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
					
				}
				dialog1.dismiss();
				ShowToast("You are Log out sucessfully.", 0);
			}
			
		});
		dialog1.show();
	
	}
	public final boolean isInternetOn() {
			boolean chk = false;
			ConnectivityManager conMgr = (ConnectivityManager) this.con.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo info = conMgr.getActiveNetworkInfo();
			if (info != null && info.isConnected()) {
				chk = true;
			} else {
				chk = false;
			}

			return chk;
		}

	public void showalert(String alerttitle2, String alertcontent2) {
		// TODO Auto-generated method stub
		final Dialog dialog1 = new Dialog(this.con,android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alert);
		TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
		txttitle.setText(alerttitle2);
		TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
		txt.setText(Html.fromHtml(alertcontent2));
		Button btn_helpclose = (Button) dialog1.findViewById(R.id.helpclose);
		btn_helpclose.setVisibility(show.GONE);
		Button btn_ok = (Button) dialog1.findViewById(R.id.ok);
		btn_ok.setVisibility(show.VISIBLE);
		btn_ok.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog1.dismiss();
				con.startActivity(new Intent(con,Import.class));
			}
			
		});
		Button btn_dash = (Button) dialog1.findViewById(R.id.dash);
		btn_dash.setVisibility(show.VISIBLE);
		btn_dash.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog1.dismiss();
				con.startActivity(new Intent(con,Dashboard.class));
			}
			
		});
		dialog1.setCancelable(false);
		dialog1.show();
	}
	

	public void gohome() {
		// TODO Auto-generated method stub
		this.con.startActivity(new Intent(this.con,HomeScreen.class));
	}

	public void MoveTo_HomeOwner(String string, int carrcit2, String string2, int i) {
		// TODO Auto-generated method stub
		
		 String name,classname ;
		if(string.equals("12"))
		{
			name=strcarr;
		}
		else
		{
			name=strret;
		}
		switch(i)
		{
		case 1:
			tempclass=classnames[4];
			
			break;
		case 2:
			tempclass=classnames[5];
			break;
		}
		if(carrcit2==0)
		{
			ShowToast("Sorry, No records found for "+string2+" status in "+name+".",0);
		}
		else
		{
			Intent intent = new Intent(this.con,tempclass);
			intent.putExtra("InspectionType", string);
			intent.putExtra("status", string2);
			this.con.startActivity(intent);
		}
		
	}

	public void MoveTo_OnlineList(String string, int onlcarrassign2,
			String string2, String string3, String string4) {
		// TODO Auto-generated method stub
		   String name;
			if(string.equals("12"))
			{
				name=strcarr;
			}
			else
			{
				name=strret;
			}
			if(onlcarrassign2==0)
			{
				ShowToast("Sorry, No records found for "+string4+" status in "+name+".",0);
			}
			else
			{
				Intent intent = new Intent(this.con,OnlineList.class);
				intent.putExtra("InspectionType", string);
				intent.putExtra("Status", string2);
				intent.putExtra("SubStatus", string3);
				this.con.startActivity(intent);
			}
	}

	public SoapObject Calling_WS2(String onlstatus2, String onlsubstatus2,
			String onlinspectionid2, String insp_id2, String string) {
		// TODO Auto-generated method stub
		SoapObject request = new SoapObject(NAMESPACE,string);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("InspectionStatus",onlstatus2);
		request.addProperty("SubStatus",onlsubstatus2);
		request.addProperty("InspectiontypeID",onlinspectionid2);
		request.addProperty("inspectorid",insp_id2);
		System.out.println("there is no issues "+request+" URL =" +URL);
		envelope.setOutputSoapObject(request);
		
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		try {
			androidHttpTransport.call(NAMESPACE+string,envelope);
			SoapObject result = (SoapObject) envelope.getResponse();System.out.println("result "+result);
			return result;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
		
	}
	public void dropTable(int select) {
		switch (select) {
		case 1:
			/** drop OnlineTable **/
			try {
				gch_db.execSQL("DROP TABLE IF EXISTS "+ Onlinepolicyholder );
				
			} catch (Exception e) {
				strerrorlog="Online Policy holder table not droped";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Inspector Login Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
				
			}
			break;
		}
    }

	public boolean Checkbox(CheckBox cb_mailing) {
		// TODO Auto-generated method stub
		if(cb_mailing.isChecked())
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}

	public String PhoneNo_validation(String decode) {
		// TODO Auto-generated method stub
		if (decode.length() == 10) {
			StringBuilder sVowelBuilder = new StringBuilder(decode);
			sVowelBuilder.insert(0, "(");
			sVowelBuilder.insert(4, ")");
			sVowelBuilder.insert(8, "-");
			newphone = sVowelBuilder.toString();
			return "Yes";
            
		} else {
			return "No";
		}


}

	public String Email_Validation(String string) {
		// TODO Auto-generated method stub
		
			//Pattern emailPattern = Pattern.compile(".+[@].+\\.[a-z]+");
        Pattern emailPattern = Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
  		Matcher emailMatcher = emailPattern.matcher(string);
  		if (emailMatcher.matches()) {
              return "Yes";
    		} else {
    			return "No";
    		}
	}

	public CharSequence Set_phoneno(String strphn) {
		// TODO Auto-generated method stub
		StringBuilder sVowelBuilder1 = new StringBuilder(strphn);
		sVowelBuilder1.deleteCharAt(0);
		sVowelBuilder1.deleteCharAt(3);
		sVowelBuilder1.deleteCharAt(6);
		strphn = sVowelBuilder1.toString();
	return strphn;
	}
	public void showing_limit(String s,LinearLayout lp,LinearLayout l,TextView t, String i) // Need to send the text in edit text and the parrent li then li then the text view whic show the percnetag   
	{
		int length=s.toString().length();
		double par_width= lp.getWidth();
		double par_unit;
		if(par_width==0)
		{
			par_width=Double.parseDouble(i); // SET THE DEFAULT VALUE FOR THE FIRST TIME LOADING 
		}
		
		par_unit=par_width/Double.parseDouble(i);
		
		
		
		/*LinearLayout.LayoutParams mParam = new LinearLayout.LayoutParams((int)(length*par_unit),(int)(8));
		l.setLayoutParams(mParam);*/
		double totalle=(length*par_unit);
		
	    int percent=(int) ((length)*(100.00/Double.parseDouble(i)));
	   
	    if(length==Integer.parseInt(i))
	    {
	    	//LinearLayout.LayoutParams mParam1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,(int)(8));
			//l.setLayoutParams(mParam1);
	    	
	    	//t.setText("   "+percent+" % Exceed limit");
	    	//t.setText(Html.fromHtml(redcolortext+"   "+100+" % Exceed limit"));
	    	t.setText("You can Enter "+i+" Letters  You have Entered  "+percent+" % of Letters  .");
	    	
	    }
	    else
	    {
	    	t.setText("You can Enter "+i+" Letters You have Entered  "+percent+" % of Letters only.");
	    }
		
	}
	public void showing_limit(String s,LinearLayout lp,LinearLayout l,TextView t) // Need to send the text in edit text and the parrent li then li then the text view whic show the percnetag   
	{
		int length=s.toString().length();
		double par_width= lp.getWidth();
		double par_unit;
		if(par_width==0)
		{
			par_width=940; // SET THE DEFAULT VALUE FOR THE FIRST TIME LOADING 
		}
		
		par_unit=par_width/999.00;
		LinearLayout.LayoutParams mParam = new LinearLayout.LayoutParams((int)(length*par_unit),(int)(8));
		l.setLayoutParams(mParam);
		double totalle=(length*par_unit);
	    int percent=(int) ((length)*(100.00/999.00));
	    if(length==999)
	    {
	    	t.setText("   "+percent+" % Exceed limit");
	    }
	    else
	    {
	    	t.setText("   "+percent+" %");
	    }
		
	}

	// get the selected radio button form the group of radiuo buttons 
	public String getslected_radio(RadioButton[] R) {
		// TODO Auto-generated method stub
		for(int i=0;i<R.length;i++)
		{
			//if(R[i].isChecked() && !R[i].getText().toString().equals("Other"))
				if(R[i].isChecked())
			{
				return R[i].getText().toString();
			}
		}
		return "";
	}

	public String getselected_chk(CheckBox[] CB) {
		// TODO Auto-generated method stub
		
		String s = "";
		for(int i=0;i<CB.length;i++)
		{
			//if(CB[i].isChecked() && !CB[i].getText().toString().equals("Other"))
				if(CB[i].isChecked())
				{
				 s+=CB[i].getText().toString()+"^";
			}
		}
		if(s.length()>=2)
		{
			s=s.substring(0, s.length()-1);
		}
		return s;
	}

	public void setvalueradio(String s,RadioButton[] R) {
		// TODO Auto-generated method stub
		if(!s.equals(""))
		{
			for(int i=0;i<R.length;i++)
			{
				if(s.equals(R[i].getText().toString()))
				{
					R[i].setChecked(true);
					return;
				}
			}
			R[R.length-1].setChecked(true); // no matches found so we set the other text to true and pass the value to that edit text view 
			
			return;
		}
	}

	public void setvaluechk(String s,CheckBox[] CB) {
		// TODO Auto-generated method stub
		
		if(!s.equals(""))
		{
			String k=s.replace("^", "~");
			String temp[]=k.split("~");
			
			int j=0;
			do{
				for(int i=0;i<CB.length;i++)
				{
					
					if(temp[j].equals(CB[i].getText().toString()))
					{
						
						CB[i].setChecked(true);
						j++;
						if(j==temp.length)
						{
							return;
						}
					}
					
				}
				if(j!=temp.length)
				{
					
					CB[CB.length-1].setChecked(true); // no matches found for the last value in the split text so we set the other to tru  
					
					j++;	
				}
			}while(j<temp.length);
		}
	}

	public void putExtras(Intent myintent) {
		// TODO Auto-generated method stub
		myintent.putExtra("homeid", selectedhomeid );
		myintent.putExtra("InspectionType", onlinspectionid );
		myintent.putExtra("status", onlstatus );
		myintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
	}

	public void getCalender() {
		// TODO Auto-generated method stub
		final Calendar c = Calendar.getInstance();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);
	}

	public void Set_UncheckBox(CheckBox[] cb_obs142, EditText etother_obs142) {
		// TODO Auto-generated method stub
		for(int i=0;i<cb_obs142.length;i++)
		{
			 cb_obs142[i].setChecked(false);
			 if(cb_obs142[i].getText().toString().equals("Other"))
			 {
				 etother_obs142.setText("");
				 etother_obs142.setVisibility(show.GONE);
			 }
		}
	}
public void getinspectiondate() {
		
		try {
			Cursor cur = this.SelectTablefunction(this.policyholder,
					" where GCH_PH_SRID='" + this.selectedhomeid + "' and GCH_PH_InspectorId='"
							+ this.Insp_id + "'");
			cur.moveToFirst();
			this.inspectiondate = cur.getString(cur
					.getColumnIndex("GCH_Schedule_ScheduledDate"));
			
			
		} catch (Exception e) {
		
		}
	}
	public Bitmap ShrinkBitmap(String file, int width, int height) {
		 Bitmap bitmap =null;
			try {
				BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
				bmpFactoryOptions.inJustDecodeBounds = true;
			    bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

				int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
						/ (float) height);
				int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
						/ (float) width);
               
				if (heightRatio > 1 || widthRatio > 1) {
					if (heightRatio > widthRatio) {
						bmpFactoryOptions.inSampleSize = heightRatio;
					} else {
						bmpFactoryOptions.inSampleSize = widthRatio;
					}
				}

				bmpFactoryOptions.inJustDecodeBounds = false;
				bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
				return bitmap;
			} catch (Exception e) {
			
				return bitmap;
			}

		}
	
	public Dialog showalert() {
		// TODO Auto-generated method stub
		final Dialog dialog1 = new Dialog(this.con,android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alert);
		TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
		TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
		Button btn_helpclose = (Button) dialog1.findViewById(R.id.helpclose_ex);
		Button btn_helpclose_t = (Button) dialog1.findViewById(R.id.helpclose_ex_t);
		
		btn_helpclose.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog1.setCancelable(true);
				dialog1.dismiss();
			}
			
		});
		btn_helpclose_t.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog1.setCancelable(true);
				dialog1.dismiss();
			}
			
		});
		return dialog1;
		
	}
	public void showing_limitwithdefaultwidth(String s,LinearLayout lp,LinearLayout l,TextView t,int defaultwidth) // Need to send the text in edit text and the parrent li then li then the text view whic show the percnetag   
	{
		int length=s.toString().length();
		double par_width= lp.getWidth();
		double par_unit;
		if(par_width==0)
		{
			par_width=defaultwidth; // SET THE DEFAULT VALUE FOR THE FIRST TIME LOADING 
		}
		
		par_unit=par_width/999.00;
		LinearLayout.LayoutParams mParam = new LinearLayout.LayoutParams((int)(length*par_unit),(int)(8));
		l.setLayoutParams(mParam);
		double totalle=(length*par_unit);
	    int percent=(int) ((length)*(100.00/999.00));
	    if(length==999)
	    {
	    	t.setText("   "+percent+" % Exceed limit");
	    }
	    else
	    {
	    	t.setText("   "+percent+" %");
	    }
		
	}
	public boolean getinspectionstatus(String id, String string) throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException {
		// TODO Auto-generated method stub
		SoapObject request = new SoapObject(NAMESPACE,string);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("InspectorID",id);
		request.addProperty("SRID",this.selectedhomeid);
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE+"GetInspectionStatus",envelope);
		String result =  envelope.getResponse().toString();
		
		if(result.trim().equals("40")||result.trim().equals("41"))
		{
			return true;
		}
		else
		{		
			return false;
		}
		
	}

	public String[] setvalueToArray(Cursor tbl) {
		// TODO Auto-generated method stub
		String[] arr=null;
		if(tbl!=null && tbl.getCount()==1)
		{
			tbl.moveToFirst();
			arr=new String[tbl.getColumnCount()];
			for(int i=0;i<tbl.getColumnCount();i++)
			{
				
				if(tbl.getString(i)!=null)
				{
				
				if(decode(tbl.getString(i))==null)
				{
				
					arr[i]="";	
				}
				else
				{
					
					
					arr[i]=decode(tbl.getString(i));
				
				}
				
				}
				else
				{
					arr[i]="";	
				}
				
			}
		}
			return arr;
		
	}

	public boolean check_Status(String result) {
		// TODO Auto-generated method stub
		try
		{
			if(result!=null)
			{
			if(result.trim().equals("true"))
			{
				return true;
			}
			else
			{
				return false;
			}
			}
			else
			{
				return false;
			}
		}
		catch (Exception e)
		{
			return false;
		}
	}

	public CharSequence phototextdisplay() {
		// TODO Auto-generated method stub
		  SpannableString content = new SpannableString("Take Photo");
		  content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
		 
		return content;
	}

	public void getInspecionTypeid(String srid)
	{
		try {
			Cursor c2 = gch_db.rawQuery("SELECT * FROM "
					+ policyholder + " WHERE GCH_PH_SRID='" + encode(srid)
					+ "' and GCH_PH_InspectorId='" + encode(Insp_id) + "'", null);
			
		
			c2.moveToFirst();
			if(c2.getCount()==1)
			{
				Inspectiontypeid=decode(c2.getString(c2.getColumnIndex("GCH_PH_InspectionTypeId")));
			}
		}
		catch(Exception e)
		{
		    strerrorlog="Retrieving policy number ";
			Error_LogFile_Creation(strerrorlog+" "+" at "+ con.getClass().getName().toString()+" "+" in the stage of Inspector Login Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			
		}
		
	}

	public void setFocus(EditText etcomments_obs1_opt12) {
		// TODO Auto-generated method stub
		etcomments_obs1_opt12.setFocusableInTouchMode(true);
		etcomments_obs1_opt12.requestFocus();
		etcomments_obs1_opt12.setSelection(etcomments_obs1_opt12.getText().length()); //For set the cursor at the end of the selection
	}

	public void fn_delete(String dt) {
		// TODO Auto-generated method stub
		Create_Table(1);Create_Table(2);Create_Table(3);Create_Table(4);
		Create_Table(5);Create_Table(6);Create_Table(7);Create_Table(8);
		Create_Table(9);Create_Table(10);Create_Table(11);Create_Table(12);
		Create_Table(13);Create_Table(14);Create_Table(15);Create_Table(16);
		try {
			gch_db.execSQL("DELETE FROM " + policyholder
					+ " WHERE GCH_PH_SRID ='" + encode(dt) + "'");
		} catch (Exception e) {

		}
		
		try {
			gch_db.execSQL("DELETE FROM " + BIQ_table
					+ " WHERE GCH_SQ_Homeid ='" + encode(dt) + "'");
		} catch (Exception e) {
		}
		
		try {
			gch_db.execSQL("DELETE FROM " + ImageTable
					+ " WHERE GCH_IM_SRID ='" + encode(dt) + "'");
		} catch (Exception e) {

		}
		try {
			gch_db.execSQL("DELETE FROM " + FeedBackInfoTable
					+ " WHERE GCH_FI_Srid ='" + encode(dt) + "'");
		} catch (Exception e) {

		}
		try {
			gch_db.execSQL("DELETE FROM " + FeedBackDocumentTable
					+ " WHERE GCH_FI_SRID ='" + encode(dt) + "'");
		} catch (Exception e) {

		}
		ShowToast("Deleted sucessfully.",1);
	}

	public void getDeviceDimensions() {
		// TODO Auto-generated method stub
		    DisplayMetrics metrics = new DisplayMetrics();
			((Activity) this.con).getWindowManager().getDefaultDisplay().getMetrics(metrics);

			wd = metrics.widthPixels;
			ht = metrics.heightPixels;
	}

	public void startCameraActivity() {
		// TODO Auto-generated method stub
		String fileName = "temp.jpg";
 		ContentValues values = new ContentValues();
 		values.put(MediaStore.Images.Media.TITLE, fileName);
 		mCapturedImageURI =this.con.getContentResolver().insert(
 				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
 		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
 		intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
 		((Activity) this.con).startActivityForResult(intent, CAPTURE_PICTURE_INTENT);
		/*Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		((Activity) this.con).startActivityForResult(Intent.createChooser(intent, "Select Picture"),SELECT_PICTURE);*/
	}
	public void showselectedimage(final String capturedImageFilePath) {
		currnet_rotated=0;

		// TODO Auto-generated method stub
		picpath=capturedImageFilePath;
		BitmapFactory.Options o = new BitmapFactory.Options();
	 		o.inJustDecodeBounds = true;
		
	 		dialog1 = new Dialog(this.con,android.R.style.Theme_Translucent_NoTitleBar);
			dialog1.getWindow().setContentView(R.layout.alert);
			
			/** get the help and update relative layou and set the visbilitys for the respective relative layout */
			LinearLayout Re=(LinearLayout) dialog1.findViewById(R.id.maintable);
			Re.setVisibility(View.GONE);
			LinearLayout Reup=(LinearLayout) dialog1.findViewById(R.id.updateimage);
			Reup.setVisibility(View.GONE);
			LinearLayout camerapic=(LinearLayout) dialog1.findViewById(R.id.cameraimage);
			camerapic.setVisibility(View.VISIBLE);
			tvcamhelp = (TextView)dialog1.findViewById(R.id.camtxthelp);
			tvcamhelp.setText("Save Picture");
			tvcamhelp.setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
			spinvw = (RelativeLayout)dialog1.findViewById(R.id.cam_photo_update);
			capvw = (RelativeLayout)dialog1.findViewById(R.id.camaddcaption);
			
			/** get the help and update relative layou and set the visbilitys for the respective relative layout */
			
			final ImageView upd_img = (ImageView) dialog1.findViewById(R.id.cameraimg);
			btn_helpclose = (Button) dialog1.findViewById(R.id.camhelpclose);
			btn_camcaptclos  = (Button) dialog1.findViewById(R.id.camcapthelpclose);
			Button btn_save = (Button) dialog1.findViewById(R.id.camsave);
			//Button btn_cancl= (Button) dialog1.findViewById(R.id.camdelete);
			Button rotatecamleft = (Button) dialog1.findViewById(R.id.rotatecamimgleft);
			Button rotatecamright = (Button) dialog1.findViewById(R.id.rotatecamright);
			elevspin = (Spinner) dialog1.findViewById(R.id.cameraelev);
			cameratxt = (TextView)dialog1.findViewById(R.id.captxt);
			captionspin = (Spinner) dialog1.findViewById(R.id.cameracaption);
			 adapter2 = new ArrayAdapter(this.con,android.R.layout.simple_spinner_item, elevnames);
			 
 			adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
 			elevspin.setAdapter(adapter2);
 			elevspin.setOnItemSelectedListener(new MyOnItemSelectedListener());
			try {
	 				BitmapFactory.decodeStream(new FileInputStream(capturedImageFilePath),
	 						null, o);
	 				final int REQUIRED_SIZE = 400;
		 			int width_tmp = o.outWidth, height_tmp = o.outHeight;
		 			int scale = 1;
		 			while (true) {
		 				if (width_tmp / 2 < REQUIRED_SIZE
		 						|| height_tmp / 2 < REQUIRED_SIZE)
		 					break;
		 				width_tmp /= 2;
		 				height_tmp /= 2;
		 				scale *= 2;
		 				BitmapFactory.Options o2 = new BitmapFactory.Options();
			 			o2.inSampleSize = scale;
			 			Bitmap bitmap = null;
			 	    	bitmap = BitmapFactory.decodeStream(new FileInputStream(
			 	    		capturedImageFilePath), null, o2);
			 	       BitmapDrawable bmd = new BitmapDrawable(bitmap);
		 			   upd_img.setImageDrawable(bmd);
		 			}
	 		
	 		} catch (FileNotFoundException e) {
	 			System.out.println("Issues "+e.getMessage());
	 		}
			
			btn_save.setOnClickListener(new OnClickListener() {


	            public void onClick(View v) {


	            	if(!elevspin.getSelectedItem().toString().equals("Select Elevation"))
	            	{
	            		
	            		if(captionspin.getSelectedItem().toString().equals("Select")||captionspin.getSelectedItem().toString().equals("ADD PHOTO CAPTION"))
	            		{
	            			ShowToast("Please add Photo Caption.", 0);
	            		}
	            		else
	            		{ 
                            int j;
		            		
		            		Cursor c11 = gch_db.rawQuery("SELECT * FROM " + ImageTable
		        	 				+ " WHERE GCH_IM_SRID='" + selectedhomeid + "' and GCH_IM_Elevation='" + elev
		        	 				+ "'  order by GCH_IM_CreatedOn DESC", null);
		        	 		int imgrws = c11.getCount();
		        	 		if (imgrws == 0) {
		        	 			j = imgrws + 1;
		        	 		} else {
		        	 			j = imgrws + 1;

		        	 		}
		        	 		
		        	 				int elevation_max=8;
				        	 		String full_name="";
				        	 		if(elev==1)
				        			{
				        				name="Front";
				        				full_name="Front Elevation";
				        				
				        			}
				        			else if(elev==2)
				        			{
				        				name="Right";
				        				full_name="Right Elevation";
				        			}
				        			else if(elev==3)
				        			{
				        				name="Back";
				        				full_name="Back Elevation";
				        			}
				        			else if(elev==4)
				        			{
				        				name="Left";
				        				full_name="Left Elevation";
				        			}
				        			else if(elev==5)
				        			{
				        				name="Additional";
				        				full_name="Additional Photographs";
				        				elevation_max=16;
				        			}
				        			else if(elev==6)
				        			{
				        				name="Roof";
				        				full_name="Roof images";
				        			}
				        	 		if (imgrws >= elevation_max ) {		
				        	 			ShowToast("You are allowed to select only "+elevation_max+" Images for the elevation "+full_name, 0);
					            	}
				        	 		else
				        	 		{
				        	 		String[] bits = picpath.split("/");
									String	picname = bits[bits.length - 1];
								gch_db.execSQL("INSERT INTO "
												+ ImageTable
												+ " (GCH_IM_SRID,GCH_IM_Ques_Id,GCH_IM_Elevation,GCH_IM_ImageName,GCH_IM_Description,GCH_IM_Nameext,GCH_IM_CreatedOn,GCH_IM_ModifiedOn,GCH_IM_ImageOrder)"
												+ " VALUES ('"
												+ selectedhomeid
												+ "','"
												+ quesid
												+ "','"
												+ elev
												+ "','"
												+ encode(picpath)
												+ "','"
												+ encode(newspin) + "','"
												+ encode(picname)
												+ "','" + datewithtime + "','" + datewithtime + "','"
												+ j + "')");
								/**Save the rotated value in to the external stroage place **/
								if(currnet_rotated>0)
								{ 

									try
									{
										/**Create the new image with the rotation **/
										String current=MediaStore.Images.Media.insertImage(con.getContentResolver(), rotated_b, "My bitmap", "My rotated bitmap");
										ContentValues values = new ContentValues();
										  values.put(MediaStore.Images.Media.ORIENTATION, 0);
										  con.getContentResolver().update(Uri.parse(current), values, MediaStore.Images.Media.DATA+ "=?", new String[] { current } );
										
										
										if(current!=null)
										{
										String path=getPath(Uri.parse(current));
										File fout = new File(capturedImageFilePath);
										fout.delete();
										/** delete the selected image **/
										File fin = new File(path);
										/** move the newly created image in the slected image pathe ***/
										fin.renameTo(new File(capturedImageFilePath));
										
										
									}
									} catch(Exception e)
									{
										System.out.println("Error occure while rotate the image "+e.getMessage());
									}
									
								}
							
							/**Save the rotated value in to the external stroage place ends **/	
								
								
								
								ShowToast("\"Taken photo successfully saved into your "+full_name+" . \"",1);
				            		
				            		
				            		
				            		dialog1.dismiss();
		        	 		}
	            		}
	            	}
	            	else
	            	{
	            		ShowToast("Please select Elevation type.", 0);
	            	}
	            }
			});
			/*btn_cancl.setOnClickListener(new OnClickListener() {
	            public void onClick(View v) {
	            	dialog1.dismiss();
	            }
			});*/
			btn_helpclose.setOnClickListener(new OnClickListener() {
	            public void onClick(View v) {
	            	dialog1.dismiss();
	            }
			});
			rotatecamright.setOnClickListener(new OnClickListener() {  
				
				public void onClick(View v) {
					// TODO Auto-generated method stub
					System.gc();
					currnet_rotated+=90;
					if(currnet_rotated>=360)
					{
						currnet_rotated=0;
					}
					
					Bitmap myImg;
					try {
						myImg = BitmapFactory.decodeStream(new FileInputStream(capturedImageFilePath));
						Matrix matrix =new Matrix();
						matrix.reset();
						//matrix.setRotate(currnet_rotated);
					
						matrix.postRotate(currnet_rotated);
						
						 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
						        matrix, true);
						 System.gc();
						 upd_img.setImageBitmap(rotated_b);

					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					catch (Exception e) {
						
					}
					catch (OutOfMemoryError e) {
						System.gc();
						try {
							myImg=null;
							System.gc();
							Matrix matrix =new Matrix();
							matrix.reset();
							//matrix.setRotate(currnet_rotated);
							matrix.postRotate(currnet_rotated);
							myImg= ShrinkBitmap(capturedImageFilePath, 800, 800);
							rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
							System.gc();
							upd_img.setImageBitmap(rotated_b); 

						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						 catch (OutOfMemoryError e1) {
								// TODO Auto-generated catch block
							ShowToast("You can not rotate this image. Image size is too large.",1);
						}
					}

				}
			});
			rotatecamleft.setOnClickListener(new OnClickListener() {  
				
				public void onClick(View v) {
					// TODO Auto-generated method stub
					System.gc();
					 currnet_rotated-=90;
						if(currnet_rotated<0)
						{
							currnet_rotated=270;
						}	
					Bitmap myImg;
					try {
						myImg = BitmapFactory.decodeStream(new FileInputStream(capturedImageFilePath));
						Matrix matrix =new Matrix();
						matrix.reset();
						//matrix.setRotate(currnet_rotated);
					
						matrix.postRotate(currnet_rotated);
						
						 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
						        matrix, true);
						 System.gc();
						 upd_img.setImageBitmap(rotated_b);

					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					catch (Exception e) {
						
					}
					catch (OutOfMemoryError e) {
						System.gc();
						try {
							myImg=null;
							System.gc();
							Matrix matrix =new Matrix();
							matrix.reset();
							//matrix.setRotate(currnet_rotated);
							matrix.postRotate(currnet_rotated);
							myImg= ShrinkBitmap(capturedImageFilePath, 800, 800);
							rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
							System.gc();
							upd_img.setImageBitmap(rotated_b); 

						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						 catch (OutOfMemoryError e1) {
								// TODO Auto-generated catch block
							ShowToast("You can not rotate this image. Image size is too large.",1);
						}
					}

				}
			});
			dialog1.setCancelable(false);
			dialog1.show();
				
			
	}

	private class MyOnItemSelectedListener implements OnItemSelectedListener {

		
		public void onItemSelected(AdapterView<?> parent, View view,final int pos,



				long id) {
			String selected_elev = elevspin.getSelectedItem().toString();
		    if(selected_elev.equals("Front Elevation"))
			{
		    	cameratxt.setVisibility(view.VISIBLE);
		    	captionspin.setVisibility(view.VISIBLE);
		    	sp=1;
		    	
			}
			else if(selected_elev.equals("Right Elevation"))
			{
				cameratxt.setVisibility(view.VISIBLE);
		    	captionspin.setVisibility(view.VISIBLE);
		    	sp=2;
			}
			else if(selected_elev.equals("Back Elevation"))
			{
				cameratxt.setVisibility(view.VISIBLE);
		    	captionspin.setVisibility(view.VISIBLE);
		    	sp=3;
			}
			else if(selected_elev.equals("Left Elevation"))
			{
				cameratxt.setVisibility(view.VISIBLE);
		    	captionspin.setVisibility(view.VISIBLE);
		    	sp=4;
			}
			else if(selected_elev.equals("Additional Photographs"))
			{
				cameratxt.setVisibility(view.VISIBLE);
		    	captionspin.setVisibility(view.VISIBLE);
		    	sp=5;
			}
			else if(selected_elev.equals("Roof Images"))
			{
				cameratxt.setVisibility(view.VISIBLE);
		    	captionspin.setVisibility(view.VISIBLE);
		    	sp=6;
			}
			else
			{
				cameratxt.setVisibility(view.GONE);
		    	captionspin.setVisibility(view.GONE);
		    	sp=0;
			}
		    
		    call_eleavtion(selected_elev,sp);
			}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			cameratxt.setVisibility(arg0.GONE);
	    	captionspin.setVisibility(arg0.GONE);
		}
		}
	public void call_eleavtion(String selected_elev, int sp2) {

		// TODO Auto-generated method stub
		String elev_name;
		//spinelevval = adapter2.getPosition(selected_elev);
		elev=sp2;
		
		if(selected_elev.equals("Front Elevation")) {elev_name="Front"; }
		else if(selected_elev.equals("Right Elevation")) {elev_name="Right";}
		else if(selected_elev.equals("Back Elevation")){elev_name="Back";}
		else if(selected_elev.equals("Left Elevation"))	{elev_name="Left";}
		else if(selected_elev.equals("Additional Photographs")) {elev_name="Additional";}
		else if(selected_elev.equals("Roof Images")) {elev_name="Roof";}
		
		Create_Table(15);Create_Table(16);
		
		Cursor c11 = gch_db.rawQuery("SELECT * FROM " + ImageTable
				+ " WHERE GCH_IM_SRID='" + this.selectedhomeid + "' and GCH_IM_Elevation='" + elev
				+ "' order by GCH_IM_CreatedOn DESC", null);
	
	    int k;
		int imgrws = c11.getCount();
		if (imgrws == 0) {
			k = imgrws + 1;
		} else {
			k = imgrws + 1;

		}
        
		
		Cursor c1=SelectTablefunction(Photo_caption, " WHERE GCH_IMP_Elevation='"+sp2+"' and GCH_IMP_ImageOrder='"+k+"'");
		String[] temp;
		if(c1.getCount()>0)
		{
			temp=new String[c1.getCount()+2];
			temp[0]="Select";
			temp[1]="ADD PHOTO CAPTION";
			int i=2;
			c1.moveToFirst();
			do
			{
				temp[i]=decode(c1.getString(c1.getColumnIndex("GCH_IMP_Caption")));	
				i++;
			}while(c1.moveToNext());
		}
		else
		{
			temp=new String[2];
			temp[0]="Select";
			temp[1]="ADD PHOTO CAPTION";
		}
		    ArrayAdapter adapter = new ArrayAdapter(this.con,android.R.layout.simple_spinner_item, temp);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			captionspin.setAdapter(adapter);
			captionspin.setOnItemSelectedListener(new MyOnItemSelectedListener2());
	}
	private class MyOnItemSelectedListener2 implements OnItemSelectedListener {
		public void onItemSelected(AdapterView<?> parent, View view,final int pos,long id) {
			newspin = captionspin.getSelectedItem().toString();
			if(newspin.equals("ADD PHOTO CAPTION"))
			{
				capvw.setVisibility(view.VISIBLE);
				btn_camcaptclos.setVisibility(view.VISIBLE);
				btn_helpclose.setVisibility(view.GONE);
				spinvw.setVisibility(view.GONE);
			
				tvcamhelp.setText("Add Caption");
				Button edt_save = (Button) dialog1.findViewById(R.id.edtsave);
				Button edt_cancl= (Button) dialog1.findViewById(R.id.edtdelete);
				etcaption = (EditText) dialog1.findViewById(R.id.addcaption);
				btn_camcaptclos.setOnClickListener(new OnClickListener() {


		            public void onClick(View v) {
		            	capvw.setVisibility(v.GONE);
						spinvw.setVisibility(v.VISIBLE);
						btn_camcaptclos.setVisibility(v.GONE);
						btn_helpclose.setVisibility(v.VISIBLE);
						captionspin.setSelection(0);
		            }
				});
				edt_cancl.setOnClickListener(new OnClickListener() {


		            public void onClick(View v) {

		            	etcaption.setText("");
		            	capvw.setVisibility(v.GONE);
						spinvw.setVisibility(v.VISIBLE);
						btn_camcaptclos.setVisibility(v.GONE);
						btn_helpclose.setVisibility(v.VISIBLE);
						captionspin.setSelection(0);
						//etcaption.setFocusable(false);
						tvcamhelp.setText("Save Picture");
				
		            }
				});
				
				edt_save.setOnClickListener(new OnClickListener() {



		            public void onClick(View v) {



		            	if(etcaption.getText().toString().trim().equals(""))
		            	{
		            		ShowToast("Please enter caption.",0);
		            	}
		            	else
		            	{
		            		int j;
		            		
		            		Cursor c11 = gch_db.rawQuery("SELECT * FROM " + ImageTable
		        	 				+ " WHERE GCH_IM_SRID='" + selectedhomeid + "' and GCH_IM_Elevation='" + elev
		        	 				+ "' order by GCH_IM_CreatedOn DESC", null);
		        	 		int imgrws = c11.getCount();
		        	 		if (imgrws == 0) {
		        	 			j = imgrws + 1;
		        	 		} else {
		        	 			j = imgrws + 1;

		        	 		}
		        	 		if(elev==1)
		        			{
		        				name="Front";
		        			}
		        			else if(elev==2)
		        			{
		        				name="Right";
		        			}
		        			else if(elev==3)
		        			{
		        				name="Back";
		        			}
		        			else if(elev==4)
		        			{
		        				name="left";
		        			}
		        			else if(elev==5)
		        			{
		        				name="Additional";
		        			}
		        			else if(elev==6)
		        			{
		        				name="Roof";
		        			}
		        	 		String[] bits = picpath.split("/");
							String	picname = bits[bits.length - 1];
							
		            		gch_db.execSQL("INSERT INTO "
									+ Photo_caption
									+ " (GCH_IMP_INSP_ID,GCH_IMP_Caption,GCH_IMP_Elevation,GCH_IMP_ImageOrder)"
									+ " VALUES ('"+Insp_id+"','" + encode(etcaption.getText().toString()) + "','" + elev + "','" + j + "')");
		            
		            		ShowToast("\"Caption has been added sucessfully.\"",1);
		            		hidekeyboard(etcaption);
		            		etcaption.setText("");
			            	capvw.setVisibility(v.GONE);
							spinvw.setVisibility(v.VISIBLE);
							btn_camcaptclos.setVisibility(v.GONE);
							btn_helpclose.setVisibility(v.VISIBLE);
							Create_Table(15);Create_Table(16);
							
							Cursor c12 = gch_db.rawQuery("SELECT * FROM " + ImageTable
									+ " WHERE GCH_IM_SRID='" + selectedhomeid + "' and GCH_IM_Elevation='" + elev
									+ "'  order by GCH_IM_CreatedOn DESC", null);
						
						    int k;
							int imgrws1 = c12.getCount();
							if (imgrws1 == 0) {
								k = imgrws1 + 1;
							} else {
								k = imgrws1 + 1;

							}
					        
							
							Cursor c1=SelectTablefunction(Photo_caption, " WHERE GCH_IMP_Elevation='"+elev+"' and GCH_IMP_ImageOrder='"+k+"'");
							String[] temp;
							if(c1.getCount()>0)
							{
								temp=new String[c1.getCount()+2];
								temp[0]="Select";
								temp[1]="ADD PHOTO CAPTION";
								int i=2;
								c1.moveToFirst();
								do
								{
									temp[i]=decode(c1.getString(c1.getColumnIndex("GCH_IMP_Caption")));	
									i++;
								}while(c1.moveToNext());
							}
							else
							{
								temp=new String[2];
								temp[0]="Select";
								temp[1]="ADD PHOTO CAPTION";
							}
							    ArrayAdapter adapter = new ArrayAdapter(con,android.R.layout.simple_spinner_item, temp);
								adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
								captionspin.setAdapter(adapter);
								captionspin.setOnItemSelectedListener(new MyOnItemSelectedListener2());
						//	captionspin.setSelection(0);
		            		//dialog1.dismiss();
		            	}
		            }
				});
		
		}
		else
		{
			tvcamhelp.setText("Save Picture");
		}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {

			// TODO Auto-generated method stub
			
		}
	}
	public void hidekeyboard()	
	{
		try
		{
			InputMethodManager imm = (InputMethodManager)con.getSystemService(Activity.INPUT_METHOD_SERVICE);
	    //imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
		imm.hideSoftInputFromWindow(((Activity) con).getCurrentFocus().getWindowToken(), 0);
		}
		catch (Exception e) {
			// TODO: handle exception
			System.out.println("issues clear"+e.getMessage());
		}

	    //imm.toggleSoftInput(InputMethodManager.RESULT_UNCHANGED_HIDDEN, 0);
	}
	public void hidekeyboard(EditText ed)	
	{
		try
		{
			
			((InputMethodManager) con.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(ed.getWindowToken(), 0);	
		//imm.hideSoftInputFromWindow(((Activity) con).getCurrentFocus().getWindowToken(), 0);
		}
		catch (Exception e) {
			// TODO: handle exception
			//System.out.println("issues clear"+e.getMessage());
		}

	    //imm.toggleSoftInput(InputMethodManager.RESULT_UNCHANGED_HIDDEN, 0);
	}
	
	public boolean checkresponce(Object responce) {
		if (responce != null) {
			if (!"null".equals(responce.toString())
					&& !responce.toString().equals("")) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public void netalert(final String string) {
		// TODO Auto-generated method stub
		String newstr = "";
		if(string.equals("FGS")||string.equals("MAPD"))
		{
			if(string.equals("MAPD")|| string.equals("MAPC"))
			{
				newstr=" Mapping feature";
			}else{
			newstr=" Florida Geological Survey(FGS) Map";}
		}
		else if(string.equals("MAP")){
			newstr="Feedback section has been saved sucessfully."+" Mapping feature";
		}
		AlertDialog.Builder builder = new AlertDialog.Builder(
				 this.con);
			builder.setTitle("Network Connection")
			.setMessage(
					newstr+" requires an Internet Connection.\n Do you have internet Connection? ");
			builder.setPositiveButton("Yes",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int which) {
							
							if(string.equals("MAPD")||string.equals("MAP"))
							{
								myintent = new Intent(con,Maps.class); 
								putExtras(myintent);
								con.startActivity(myintent);
								
							}
						}
					});
			builder.setNegativeButton("No",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int which) {
                        // ShowToast("Internet connection is not available.",0);
							if(string.equals("MAPC"))
							{
								myintent = new Intent(con,Submit.class); 
								putExtras(myintent);
								con.startActivity(myintent);
							}
						}
					});
			builder.show();
			

	}

	public boolean common(String path) {
		File f = new File(path);
		if (f.length() < 2097152) {
			return true;
		} else {
			return false;
		}

	}

	public String fn_CurrentInspector(String insp_id2, String selectedhomeid2) throws SocketException,IOException,NetworkErrorException,TimeoutException, XmlPullParserException,Exception {
		// TODO Auto-generated method stub
		SoapObject request = new SoapObject(NAMESPACE,"IsCurrentInspector");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("InspectorID",insp_id2);
		request.addProperty("Srid",selectedhomeid2);
		envelope.setOutputSoapObject(request);;	
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE+"IsCurrentInspector",envelope);
		String result =  envelope.getResponse().toString();
		return result;
		
	}

	public String fn_CheckExportCount(String insp_id2, String selectedhomeid2) throws SocketException,IOException,NetworkErrorException,TimeoutException, XmlPullParserException,Exception {
		// TODO Auto-generated method stub
		SoapObject request = new SoapObject(NAMESPACE,"GetExportCount");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("InspectorID",insp_id2);
		request.addProperty("SRID",selectedhomeid2);
		envelope.setOutputSoapObject(request);;
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE+"GetExportCount",envelope);
		String resultcnt =  envelope.getResponse().toString();
		return resultcnt;
	}
	public String Website_validation(String string) {
		// TODO Auto-generated method stub
		if(URLUtil.isValidUrl(string))
		{
			return "Valid";
		}
		else{ return "Invalid";}
		
	}

	public void delete_all(final String where) {
		// TODO Auto-generated method stub
		
		
		 alerttitle="Delete";
		  alertcontent="Are you sure want to delete All Inspection ?";
		    final Dialog dialog1 = new Dialog(con,android.R.style.Theme_Translucent_NoTitleBar);
			dialog1.getWindow().setContentView(R.layout.alertsync);
			TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
			txttitle.setText(alerttitle);
			TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
			txt.setText(Html.fromHtml( alertcontent));
			Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
			Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
			btn_yes.setOnClickListener(new OnClickListener()
			{
	       	@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog1.dismiss();
					Create_Table(1);Create_Table(2);Create_Table(3);Create_Table(4);
					Create_Table(5);Create_Table(6);Create_Table(7);Create_Table(8);
					Create_Table(9);Create_Table(10);Create_Table(11);Create_Table(12);
					Create_Table(13);Create_Table(14);Create_Table(15);Create_Table(16);
					try {
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where GCH_PH_SRID in "+where;
											
						gch_db.execSQL("DELETE FROM " + policyholder
								+ " " + temp + " ");
					} catch (Exception e) {

					}
					
					try {
						
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where GCH_SQ_Homeid in "+where;
											
						gch_db.execSQL("DELETE FROM " + BIQ_table
								+ " " + temp + "");
					} catch (Exception e) {
					}
					
					try {
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where GCH_IM_SRID in "+where;
						
						gch_db.execSQL("DELETE FROM " + ImageTable
								+ " " + temp + "");
					} catch (Exception e) {

					}
					try {
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where GCH_FI_Srid in "+where;
						
						gch_db.execSQL("DELETE FROM " + FeedBackInfoTable
								+ " " + temp + "");
					} catch (Exception e) {

					}
					try {
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where GCH_FI_SRID in "+where;
						
						gch_db.execSQL("DELETE FROM " + FeedBackDocumentTable
								+ "  " + temp + "");
					} catch (Exception e) {

					}
					
					ShowToast("Deleted sucessfully.",1);
					con.startActivity(new Intent(con,HomeScreen.class));
					
				}
				
			});
			btn_cancel.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog1.setCancelable(true);
					dialog1.dismiss();
					
				}
				
			});
			dialog1.setCancelable(false);
			dialog1.show();
	 
		
	}
	public String getPath(Uri uri) {

		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = ((Activity) con).managedQuery(uri, projection, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	public void setupUI(View view) {

	    //Set up touch listener for non-text box views to hide keyboard.
	    if(!(view instanceof EditText)) {

	        view.setOnTouchListener(new OnTouchListener() {

	            public boolean onTouch(View v, MotionEvent event) {
	               hidekeyboard();
	                return false;
	            }

	        });
	    }
	    else
	    {
	    	
	    	((EditText) ((Activity) con).findViewById(view.getId())).setOnTouchListener(new Touch_Listener(0));
	    	/*((EditText) ((Activity) con).findViewById(view.getId())).requestFocus();
	    	((EditText) ((Activity) con).findViewById(view.getId())).setSelection(((EditText) ((Activity) con).findViewById(view.getId())).length());*/  
	    }

	    //If a layout container, iterate over children and seed recursion.
	    if (view instanceof ViewGroup) {

	        for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

	            View innerView = ((ViewGroup) view).getChildAt(i);
	            setupUI(innerView);
	        }
	    }
	}
	class Touch_Listener implements OnTouchListener
	{
		   public int type;
		   Touch_Listener(int type)
			{
				this.type=type;
				
			}
		    @Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
		    	((EditText) ((Activity) con).findViewById(v.getId())).setFocusableInTouchMode(true);
		    	((EditText) ((Activity) con).findViewById(v.getId())).requestFocus();
		    	((EditText) ((Activity) con).findViewById(v.getId())).setSelection(((EditText) ((Activity) con).findViewById(v.getId())).length());   
		    	System.out.println("success");
				return false;
		    }
	}
	public String[] gettheWifidata() {
		String s[] = new String[4];
		ConnectivityManager c = (ConnectivityManager) con
				.getSystemService(con.CONNECTIVITY_SERVICE);
		
		try {
			
			ConnectivityManager cm = (ConnectivityManager) con
					.getSystemService(con.CONNECTIVITY_SERVICE);
			
			NetworkInfo info = cm.getActiveNetworkInfo();
			
			int type = 0, subType = 0;
			
			if (info != null) {
				type = info.getType();
				subType = info.getSubtype();
				if (type == ConnectivityManager.TYPE_WIFI) {
				
					WifiManager wifiManager = (WifiManager) con
							.getSystemService(con.WIFI_SERVICE);
					
					s[0] = "WIFI";
					s[1] = String.valueOf(wifiManager.getConnectionInfo()
							.getLinkSpeed());
					s[2] = wifiManager.getConnectionInfo().LINK_SPEED_UNITS
							.toString();
					s[3] = "good";
					 slenght = wifiManager.getConnectionInfo().getRssi();
					
					int tmp=wifiManager.calculateSignalLevel(slenght, 5);
					if(tmp==1)
					{
						s[3]="Very Weak";
					}
					else if(tmp==2)
					{
						s[3]="Weak";
					}
					else if(tmp==3)
					{
						s[3]="Good";
					}
					else if(tmp==4 || tmp==5)
					{
						s[3]="Excellent";
					}
					
					//ShowToast(" wifi data value "+slenght+" link speed= "+s[1]+" link speed units"+s[2]+"signal strength"+, 0);
					//System.out.println(" wifi data value "+slenght+s[1]+s[2]+s[3]);
					return s;
				} else if (type == ConnectivityManager.TYPE_MOBILE) {
					switch (subType) {
					case TelephonyManager.NETWORK_TYPE_1xRTT:
						s[0] = "1xRTT";
						s[1] = "50-100";
						s[2] = "kbps";
						s[3] = "Poor";
						return s; // ~ 50-100 kbps
					case TelephonyManager.NETWORK_TYPE_CDMA:

						s[0] = "CDMA";
						s[1] = "14-64";
						s[2] = "kbps";
						s[3] = "Poor";
						return s; // ~ 14-64 kbps
					case TelephonyManager.NETWORK_TYPE_EDGE:

						s[0] = "EDGE";
						s[1] = "50-100";
						s[2] = "kbps";
						s[3] = "Poor";
						return s; // ~ 50-100 kbps
					case TelephonyManager.NETWORK_TYPE_EVDO_0:

						s[0] = "3 GI Connection";
						s[1] = "400-1000";
						s[2] = "kbps";
						s[3] = "Not bad";
						return s; // ~ 400-1000 kbps
					case TelephonyManager.NETWORK_TYPE_EVDO_A:

						s[0] = "3 GI Connection";
						s[1] = "600-1400";
						s[2] = "kbps";
						s[3] = "Good";
						return s; // ~ 600-1400 kbps
					case TelephonyManager.NETWORK_TYPE_GPRS:

						s[0] = "GPRS";
						s[1] = "100";
						s[2] = "kbps";
						s[3] = "Poor";
						return s; // ~ 100 kbps
					case TelephonyManager.NETWORK_TYPE_HSDPA:

						s[0] = "3 GI Connection";
						s[1] = "2-14";
						s[2] = "Mbps";
						s[3] = "Strong";

						return s;// ~ 2-14 Mbps
					case TelephonyManager.NETWORK_TYPE_HSPA:

						s[0] = "3 GI Connection";
						s[1] = "700-1700";
						s[2] = "kbps";
						s[3] = "Good";

						return s; // ~ 700-1700 kbps
					case TelephonyManager.NETWORK_TYPE_HSUPA:

						s[0] = "3 GI Connection";
						s[1] = "1-23";
						s[2] = "Mbps";
						s[3] = "Strong";
						return s; // ~ 1-23 Mbps
					case TelephonyManager.NETWORK_TYPE_UMTS:

						s[0] = "3 GI Connection";
						s[1] = "400-700";
						s[2] = "kbps";
						s[3] = "Good";
						return s; // ~ 400-7000 kbps
						// NOT AVAILABLE YET IN API LEVEL 7
					/*case TelephonyManager.NETWORK_TYPE_EHRPD:

						s[0] = "EHRPD";
						s[1] = "1-2";
						s[2] = "Mbps";
						s[3] = "Good";
						return s; // ~ 1-2 Mbps
					case TelephonyManager.NETWORK_TYPE_EVDO_B:

						s[0] = "EVDO";
						s[1] = "5";
						s[2] = "Mbps";
						s[3] = "Strong";

						return s; // ~ 5 Mbps
					case TelephonyManager.NETWORK_TYPE_HSPAP:

						s[0] = "HSPAP";
						s[1] = "10-20";
						s[2] = "Mbps";
						s[3] = "Strong";

						return s;// ~ 10-20 Mbps
					case TelephonyManager.NETWORK_TYPE_IDEN:

						s[0] = "IDEN";
						s[1] = "25";
						s[2] = "kbps";
						s[3] = "Poor";
						return s;// ~25 kbps
					case TelephonyManager.NETWORK_TYPE_LTE:

						s[0] = "LTE";
						s[1] = "10+";
						s[2] = "Mbps";
						s[3] = "Strong";

						return s;// ~ 10+ Mbps
						// Unknown
*/					case TelephonyManager.NETWORK_TYPE_UNKNOWN:

						s[0] = "UNKNOWN";
						s[1] = "N/D";
						s[2] = "N/D";
						s[3] = "N/D";

						return s;
					default:

						s[0] = "UNKNOWN";
						s[1] = "N/D";
						s[2] = "N/D";
						s[3] = "N/D";
						return s;
					}
				} else {

					s[0] = "UNKNOWN";
					s[1] = "N/D";
					s[2] = "N/D";
					s[3] = "N/D";
					return s;
				}
			} else {

				s[0] = "UNKNOWN";
				s[1] = "N/D";
				s[2] = "N/D";
				s[3] = "N/D";
				return s;
			}

		} catch (Exception e) {
			System.out.println("Problem" + e);
			s[0] = "UNKNOWN";
			s[1] = "N/D";
			s[2] = "N/D";
			s[3] = "N/D";
			return s;
		}
	}
}
