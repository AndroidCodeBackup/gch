package inspectiondepot.gch;
/***   	Page Information
Version :1.0
VCode:1
Created Date:19/10/2012
Purpose:For Policy holder information  
Created By:Rakki S
Last Updated:19/10/2012   
Last Updatedby:Rakki s
***/
import inspectiondepot.gch.R;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class PolicyHolder extends Activity{
	CommonFunctions cf;
	public String chkstatus[]={"true","true","true","true"};
	public String strwrkphn,strcellphn,strhmephn;
	public int mc=0,ml=0;
	//String etnoofstoriestxt="";
	public ArrayAdapter adapter1;
	public EditText cntactperson;
	
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf = new CommonFunctions(this);
	      
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.selectedhomeid = extras.getString("homeid");
				cf.onlinspectionid = extras.getString("InspectionType");
			    cf.onlstatus = extras.getString("status");
         	}
         	setContentView(R.layout.policyholder);
         	/**Used for hide he key board when it clik out side**/
         	System.out.println("no moer isseus ");
        	cf.setupUI((ScrollView) findViewById(R.id.scr));

        /**Used for hide he key board when it clik out side**/
         	cf.getDeviceDimensions();
 	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
 	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 1, 0,cf));
			LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.generalsubmenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 12, 1,cf));
			System.out.println("no moer isseus 0.0");
			TextView firstnametxt = (TextView) findViewById(R.id.tvfirstname);
			firstnametxt.setText(Html.fromHtml(cf.redcolor+" "+"First Name "));
			cf.et_firstname  = (EditText) findViewById(R.id.etfirstname);
			
			TextView lastnametxt = (TextView) findViewById(R.id.tvlastname);
			lastnametxt.setText(Html.fromHtml(cf.redcolor+" "+"Last Name "));
			cf.et_lastname  = (EditText) findViewById(R.id.etlastname);
			
			TextView addresstxt = (TextView) findViewById(R.id.tvaddress1);
			addresstxt.setText(Html.fromHtml(cf.redcolor+" "+"Address "));
			cf.et_address1  = (EditText) findViewById(R.id.etaddress1);
			
			cntactperson  = (EditText) findViewById(R.id.etcontactperson);
			System.out.println("no moer isseus 0.1");
			TextView ziptxt = (TextView) findViewById(R.id.tvzip);
			ziptxt.setText(Html.fromHtml(cf.redcolor+" "+"ZIP "));
			cf.et_zip  = (EditText) findViewById(R.id.etzip);
			
			/*TextView noofstoriestxt = (TextView) findViewById(R.id.tvnoofstories);
			noofstoriestxt.setText(Html.fromHtml(cf.redcolor+" "+"# of Stories "));*/
			
			TextView hmephntxt = (TextView) findViewById(R.id.tvhmephn);
			hmephntxt.setText(Html.fromHtml(cf.redcolor+" "+"Home Phone "));
			cf.et_hmephn  = (EditText) findViewById(R.id.ethmephn1);
			
			TextView citytxt = (TextView) findViewById(R.id.tvcity);
			citytxt.setText(Html.fromHtml(cf.redcolor+"City "));
			cf.et_city  = (EditText) findViewById(R.id.etcity);
			
			TextView wrkphntxt = (TextView) findViewById(R.id.tvwrkphn);
			wrkphntxt.setText(Html.fromHtml(" "+"Work Phone "));
			cf.et_wrkphn  = (EditText) findViewById(R.id.etwrkphn);
			
			TextView statetxt = (TextView) findViewById(R.id.tvstate);
			statetxt.setText(Html.fromHtml(cf.redcolor+"State "));
			cf.et_state  = (EditText) findViewById(R.id.etstate);
			
			TextView cellphntxt = (TextView) findViewById(R.id.tvcellphn);
			cellphntxt.setText(Html.fromHtml(" "+"Cell Phone "));
			cf.et_cellphn  = (EditText) findViewById(R.id.etcellphn);
			
			TextView countytxt = (TextView) findViewById(R.id.tvcounty);
			countytxt.setText(Html.fromHtml(cf.redcolor+"County "));
			cf.et_county  = (EditText) findViewById(R.id.etcounty);
			
			TextView emailtxt = (TextView) findViewById(R.id.tvemail);
			emailtxt.setText(Html.fromHtml(cf.redcolor+"Email "));
			cf.et_email  = (EditText) findViewById(R.id.etemail);
			
			TextView policynotxt = (TextView) findViewById(R.id.tvpolicyno);
			policynotxt.setText(Html.fromHtml(cf.redcolor+"Policy # "));
			cf.et_policyno  = (EditText) findViewById(R.id.etpolicyno);
			
			/*adapter1 = new ArrayAdapter(PolicyHolder.this,android.R.layout.simple_spinner_item, cf.BI_GBI_year);
			adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);*/
		    /*Y_O_C_spn = (Spinner) findViewById(R.id.PH_Y_O_C_spn);
		    Y_O_C_spn.setAdapter(adapter1);
		    Y_O_C_spn_other = (EditText) findViewById(R.id.PH_Y_O_C_spn_other);
		    Y_O_C_spn.setOnItemSelectedListener(new spinerlisner());*/
		   // TextView Y_O_C_txt = (TextView) findViewById(R.id.PH_Y_O_C_txt);
		  //  Y_O_C_txt.setText(Html.fromHtml(cf.redcolor+"Year of Home "));
				
			TextView companynametxt = (TextView) findViewById(R.id.tvinsurancecompany);
			companynametxt.setText(Html.fromHtml(" "+"Insurance Company "));
			cf.et_companyname  = (TextView) findViewById(R.id.etinsurancecompany);
			 final CheckBox cb_email = (CheckBox)findViewById(R.id.chkemail);
				System.out.println("no moer isseus 1 ");
			    cb_email.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						cf.chkbool=cf.Checkbox(cb_email);
						if(cf.chkbool==true)
						{
							 mc = 1;
							 cf.et_email.setEnabled(false);
						}
						else
						{
							 mc = 0;
							 cf.et_email.setEnabled(true);
						}
					
					}
				});
			    //cf.et_noofstories = (EditText)findViewById(R.id.etnoofstories);
				
			    cf.et_website = (EditText)findViewById(R.id.etwebsite);
		        cf.getInspectorId();
				if (cf.onlinspectionid.equals("11")) {
					cf.et_policyno.setEnabled(true);
					
				} else {
					cf.et_policyno.setEnabled(false);
				
				}
				
			try {
				Cursor c2 = cf.gch_db.rawQuery("SELECT * FROM "
						+ cf.policyholder + " WHERE GCH_PH_SRID='" + cf.encode(cf.selectedhomeid)
						+ "' and GCH_PH_InspectorId='" + cf.encode(cf.Insp_id) + "'", null);
				int rws = c2.getCount();
				System.out.println("RW" +rws);
				c2.moveToFirst();
				if (c2 != null) {
					do {
						
						cf.et_firstname.setText(cf.decode(c2.getString(c2.getColumnIndex("GCH_PH_FirstName"))));
						cf.et_lastname.setText(cf.decode(c2.getString(c2.getColumnIndex("GCH_PH_LastName"))));
						cf.et_address1.setText(cf.decode(c2.getString(c2.getColumnIndex("GCH_PH_Address1"))));
						cf.et_city.setText(cf.decode(c2.getString(c2.getColumnIndex("GCH_PH_City"))));
						cf.et_state.setText(cf.decode(c2.getString(c2.getColumnIndex("GCH_PH_State"))));
						cf.et_county.setText(cf.decode(c2.getString(c2.getColumnIndex("GCH_PH_County"))));
						cf.et_policyno.setText(cf.decode(c2.getString(c2.getColumnIndex("GCH_PH_Policyno"))));
						System.out.println("no moer isseus 4");
						if(cf.decode(c2.getString(c2.getColumnIndex("GCH_PH_InsuranceCompany"))).equals("")|| cf.decode(c2.getString(c2.getColumnIndex("GCH_PH_InsuranceCompany"))).equals("0"))
						{
							cf.et_companyname.setText("Not Available");
						}
						else
						{
							cf.et_companyname.setText(cf.decode(c2.getString(c2.getColumnIndex("GCH_PH_InsuranceCompany"))));	
						}
						
						cf.et_policyno.setText(cf.decode(c2.getString(c2.getColumnIndex("GCH_PH_Policyno"))));
						
						
						cf.et_zip.setText(cf.decode(c2.getString(c2.getColumnIndex("GCH_PH_Zip"))));
						
						//String Y_O_C=cf.decode(c2.getString(c2.getColumnIndex("GCH_YearBuilt")));
						cntactperson.setText(cf.decode(c2.getString(c2.getColumnIndex("GCH_ContactPerson"))));
						
						
						if (cf.decode(c2.getString(c2.getColumnIndex("GCH_PH_HomePhone"))).equals("")) {
							cf.et_hmephn.setText("");
						}else{
						cf.et_hmephn.setText(cf.Set_phoneno(cf.decode(c2.getString(c2.getColumnIndex("GCH_PH_HomePhone")))));
						}
						if (cf.decode(c2.getString(c2.getColumnIndex("GCH_PH_WorkPhone"))).equals("")) {
							cf.et_wrkphn.setText("");
						}else{
						cf.et_wrkphn.setText(cf.Set_phoneno(cf.decode(c2.getString(c2.getColumnIndex("GCH_PH_WorkPhone")))));
						}
						if (cf.decode(c2.getString(c2.getColumnIndex("GCH_PH_CellPhone"))).equals("")) {
							cf.et_cellphn.setText("");
						}else{
						cf.et_cellphn.setText(cf.Set_phoneno(cf.decode(c2.getString(c2.getColumnIndex("GCH_PH_CellPhone")))));
						}
						
						//cf.et_noofstories.setText(cf.decode(c2.getString(c2.getColumnIndex("GCH_PH_NOOFSTORIES"))));
						cf.et_website.setText(cf.decode(c2.getString(c2.getColumnIndex("GCH_PH_WEBSITE"))));
						cf.et_email.setText(cf.decode(c2.getString(c2.getColumnIndex("GCH_PH_Email"))));
						if (cf.decode(c2.getString(c2.getColumnIndex("GCH_PH_EmailChkbx"))).equals("1")) {
							mc=1;
							cb_email.setChecked(true);
							cf.et_email.setEnabled(false);
						} else {
						   mc=0;
							cb_email.setChecked(false);
							cf.et_email.setEnabled(true);
						}
						cf.ScheduledDate=cf.decode(c2.getString(c2.getColumnIndex("GCH_Schedule_ScheduledDate")));	
						
						/*if(!Y_O_C.trim().equals(""))
						{
							if(getFromArray(Y_O_C,cf.BI_GBI_year))
							 {
								 Y_O_C_spn.setSelection(adapter1.getPosition("Other"));
								 Y_O_C_spn_other.setText(Y_O_C); 
							 }
							 else
							 {
								Y_O_C_spn.setSelection(adapter1.getPosition(Y_O_C));
							 }
						}	*/
						
					} while (c2.moveToNext());
				}
				
		  } catch (Exception e) {
			 	cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ PolicyHolder.this +" "+" in the processing stage of retrieving data from PH table  at "+" "+ cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		  }
			
	 }
	public void clicker(View v) {
			switch (v.getId()) {

			case R.id.save:
			Save_PHData();
				break;
				  case R.id.home:
						  cf.gohome();
						  break;
				
			}
		}
	private void Save_PHData() {
		// TODO Auto-generated method stub
		/*CHECKING WORKPHONE VALIDATION*/
		if (!cf.et_wrkphn.getText().toString().trim().equals("")) {
			if (cf.PhoneNo_validation(cf.et_wrkphn.getText().toString()) != "Yes") {
				cf.ShowToast("Please enter the Work Phone in 10 digit Number.", 1);
				cf.et_wrkphn.setText("");
				cf.et_wrkphn.requestFocus();
				chkstatus[0] = "false";
			} else {
				chkstatus[0] = "true";
				strwrkphn=cf.newphone;
			}
		} else {
			chkstatus[0] = "true";
		}
		/*CHECKING CELLPHONE VALIDATION*/
		if (!cf.et_cellphn.getText().toString().trim().equals("")) {
			if (cf.PhoneNo_validation(cf.et_cellphn.getText().toString())!= "Yes") {
				cf.ShowToast("Please enter the Cell Phone in 10 digit Number.", 1);
				cf.et_cellphn.setText("");
				cf.et_cellphn.requestFocus();
				chkstatus[1] = "false";
			} else {
				chkstatus[1] = "true";
				strcellphn=cf.newphone;
			}
		} else {
			chkstatus[1] = "true";
		}
		
		/*CHECKING WEBSITE VALIDATION*/
		if (!cf.et_website.getText().toString().trim().equals("")) {
			if (!cf.Website_validation(cf.et_website.getText().toString()).equals("Valid")) {
				cf.ShowToast("Please enter the website in valid format.", 1);
				cf.et_website.setText("");
				cf.et_website.requestFocus();
				chkstatus[3] = "false";
			} else {
				chkstatus[3] = "true";
			
			}
		} else {
			chkstatus[3] = "true";
		}
      boolean b[] = new boolean[1]; 
     // etnoofstoriestxt = cf.et_noofstories.getText().toString();
      try
      {
      b[0]=true;
      }
      catch(Exception e)
      {
    	  System.out.println("EE  "+e.getMessage());
      }
      
		/*VALIDATIONS FOR SAVE BUTTON*/
	    //if (!"".equals(cf.ScheduledDate)) { /*CHECKING SCHEDULE DATE IA AVAILABLE OR NOT*/
	    	if (!"".equals(cf.et_firstname.getText().toString().trim())) {  /*CHECKING FIRSTNAME IS AVAILABLE OR NOT*/
	    		if (!"".equals(cf.et_lastname.getText().toString().trim())) {/*CHECKING LASTNAME IS AVAILABLE OR NOT*/
	    			 if (!"".equals(cf.et_address1.getText().toString().trim())) {/*CHECKING ADDRESS1 IS AVAILABLE OR NOT*/
					        if (!"".equals(cf.et_city.getText().toString().trim())) {/*CHECKING CITY IS AVAILABLE OR NOT*/
					        	 if (!"".equals(cf.et_state.getText().toString().trim())) {/*CHECKING STATE IS AVAILABLE OR NOT*/
					        		 if (!"".equals(cf.et_county.getText().toString().trim())) {/*CHECKING COUNTY IS AVAILABLE OR NOT*/
					        	        if (!"".equals(cf.et_zip.getText().toString().trim())) {/*CHECKING ZIP IS AVAILABLE OR NOT*/
						                  if (cf.et_zip.getText().length() == 5) { /*CHECKING LENGTH OF ZIPCODE*/
						                	 if (!"".equals(cf.et_hmephn.getText().toString().trim())) { /*CHECKING HOMEPHONE IS AVAILABLE OR NOT*/
								                    if (cf.PhoneNo_validation(cf.et_hmephn.getText().toString().trim()).equals("Yes")) { /*VALIDATION FOR HOME PHONE*/
								                    	strhmephn=cf.newphone;
								                    	/*if(!"".equals(cf.et_noofstories.getText().toString().trim())){
								                    		if(b[0]==true){*/
								                    	if (cf.et_email.getText().toString().trim().equals("")&& (mc == 0)) { /*VALIDATION FOR EMAIL*/
															cf.ShowToast("Please enter the Email.", 1);
										                	cf.et_email.requestFocus();
															chkstatus[2] = "false";
														 } else {
															if (mc == 1) {
																db_update();
																chkstatus[2] = "true";
															
															} else if (mc == 0) {
																if (cf.Email_Validation(cf.et_email.getText().toString().trim()) != "Yes") {
																	cf.ShowToast("Please enter the valid Email.", 1);
																	cf.et_email.setText("");
																	cf.et_email.requestFocus();
																	chkstatus[2] = "false";
																} else {
																	db_update();
																	chkstatus[2] = "true";
																	
																}
															}

														}
								                    	/*}
								                    		else
								                    		{
								                    			cf.ShowToast("Please enter the no of stories less than 35 .", 1);
															    cf.et_noofstories.requestFocus();
								                    		}
								                    }
								                    else
								                    {
								                    
									                    	cf.ShowToast("Please enter the no of stories.", 1);
														    cf.et_noofstories.requestFocus();
								                    	
								                    }*/
												    } else {
														cf.ShowToast("Please enter the Home Phone in 10 digit Number.", 1);
													    cf.et_hmephn.setText("");
													    cf.et_hmephn.requestFocus();
													}
											} else {
												cf.ShowToast("Please enter the Home Phone Number.", 1);
												cf.et_hmephn.requestFocus();
											}
                                      } else {
											cf.ShowToast("Please enter the valid ZipCode.",1);
											cf.et_zip.setText("");
											cf.et_zip.requestFocus();
										}
				        	 	} else {
									cf.ShowToast("Please enter the ZipCode.", 1);
									cf.et_zip.requestFocus();
								}
			        		  } else {
									cf.ShowToast("Please enter the County.", 1);
									cf.et_zip.requestFocus();
								}
			        	        } else {
									cf.ShowToast("Please enter the State.", 1);
									cf.et_zip.requestFocus();
							}
                             } else {
								 cf.ShowToast("Please enter the City.",1);
								 cf.et_city.requestFocus();
						}
					} else {
						cf.ShowToast("Please enter the Address.", 1);
						cf.et_address1.requestFocus();
					}
             	} else {
					cf.ShowToast("Please enter the Last Name.", 1);
				    cf.et_lastname.requestFocus();
				}
         	}
            else {
				cf.ShowToast("Please enter the First Name.", 1);
				cf.et_firstname.requestFocus();
			}

	   /* } else {
			cf.ShowToast("You cannot submit Policy Holder Information without Scheduling.", 1);
			
		}*/
	}
	
	private void db_update() {
		// TODO Auto-generated method stub
		if (chkstatus[0] == "true" && chkstatus[1] == "true"
				&& chkstatus[2] == "true" && chkstatus[3] == "true" ) {
			
			/*if(!Y_O_C_spn.getSelectedItem().toString().equals("Select") && (!Y_O_C_spn.getSelectedItem().toString().equals("Other") || !Y_O_C_spn_other.getText().toString().trim().equals("")))
				{
			     String Y_O_C=(Y_O_C_spn.getSelectedItem().toString().equals("Other"))? Y_O_C_spn_other.getText().toString():Y_O_C_spn.getSelectedItem().toString();*/
				try{
					
					cf.gch_db.execSQL("UPDATE " + cf.policyholder
							+ " SET GCH_PH_FirstName='" + cf.encode(cf.et_firstname.getText().toString())
							+ "',GCH_PH_LastName='"
							+ cf.encode(cf.et_lastname.getText().toString())
							+ "',GCH_PH_Address1='"
							+ cf.encode(cf.et_address1.getText().toString())+ "',GCH_PH_City='"
							+ cf.encode(cf.et_city.getText().toString()) + "',GCH_PH_State='"
							+ cf.encode(cf.et_state.getText().toString()) + "',GCH_PH_County='"
							+ cf.encode(cf.et_county.getText().toString()) + "',GCH_PH_Policyno='"
							+ cf.encode(cf.et_policyno.getText().toString())
							+ "',GCH_PH_InsuranceCompany='" + cf.encode((cf.et_companyname.getText().toString().trim().equals("Not Available"))? "0":cf.et_companyname.getText().toString().trim())
							+ "',GCH_PH_Zip='"
							+ cf.encode(cf.et_zip.getText().toString())
							+ "',GCH_PH_HomePhone='" + cf.encode(strhmephn)
							+ "',GCH_PH_WorkPhone='" + cf.encode(strwrkphn)
							+ "',GCH_PH_CellPhone='" + cf.encode(strcellphn)
							//+ "',GCH_PH_NOOFSTORIES='" + cf.encode(cf.et_noofstories.getText().toString())
							+ "',GCH_PH_WEBSITE='" + cf.encode(cf.et_website.getText().toString())
							+ "',GCH_PH_Email='"
							+ cf.encode(cf.et_email.getText().toString()) + "',GCH_PH_EmailChkbx='"
							+ cf.encode(Integer.toString(mc)) + "'," +
							//"GCH_YearBuilt='"+cf.encode(Y_O_C)+"'," +
							"GCH_ContactPerson='"+cf.encode(cntactperson.getText().toString())+"' WHERE GCH_PH_SRID ='" + cf.encode(cf.selectedhomeid)
							+ "'");
					/*cf.Create_Table(5);
					Cursor s=cf.SelectTablefunction(cf.BIQ_table, " where GCH_BI_Homeid='"+cf.encode(cf.selectedhomeid)+"'");
					if(s.getCount()>0)
					{
						cf.gch_db.execSQL("UPDATE " + cf.BIQ_table + " SET GCH_BI_NSTORIES='"+ cf.encode(cf.et_noofstories.getText().toString())+"', GCH_BI_BUILT_YR='"+cf.encode(Y_O_C_spn.getSelectedItem().toString())+"',GCH_BI_YR_BUILT_OTHER ='"+cf.encode(Y_O_C_spn_other.getText().toString().trim())+"' where GCH_BI_Homeid='"+cf.encode(cf.selectedhomeid)+"'" );
					}
					*/		
					cf.ShowToast("Policyholder Information has been updated", 1);
				}
				
				catch(Exception e)
				{
					cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ PolicyHolder.this +" "+" in the processing stage of updating data into PH table  at "+" "+ cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}		
			/*}else
			{
				if(Y_O_C_spn.getSelectedItem().toString().equals("Other") && Y_O_C_spn_other.getText().toString().trim().equals(""))
				{
					cf.ShowToast("Please enter  other text for year of home ", 1);
				}
				else
				{
					cf.ShowToast("Please select year of home ", 1);
				}
			}*/
		}
	}
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
			// replaces the default 'Back' button action
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				Intent  myintent = new Intent(PolicyHolder.this,HomeOwnerList.class);
			 	myintent.putExtra("InspectionType", cf.onlinspectionid);
				myintent.putExtra("status", cf.onlstatus);
				myintent.putExtra("homeid", cf.selectedhomeid);
				startActivity(myintent);
				return true;
			}
			return super.onKeyDown(keyCode, event);
		}
	 protected void onActivityResult(int requestCode, int resultCode, Intent data) {
				switch (resultCode) {
	 			case 0:
	 				break;
	 			case -1:
	 				try {

	 					String[] projection = { MediaStore.Images.Media.DATA };
	 					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
	 							null, null, null);
	 					int column_index_data = cursor
	 							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
	 					cursor.moveToFirst();
	 					String capturedImageFilePath = cursor.getString(column_index_data);
	 					cf.showselectedimage(capturedImageFilePath);
	 				} catch (Exception e) {
	 					
	 				}
	 				
	 				break;

	 		}

	 	}
	/* class spinerlisner implements OnItemSelectedListener
	 {

	 	@Override
	 	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
	 			long arg3) {
	 		// TODO Auto-generated method stub
	 		
	 			 String s=Y_O_C_spn.getSelectedItem().toString();
	 			 if(s.equals("Other"))
	 			 {
	 				Y_O_C_spn_other.setVisibility(arg1.VISIBLE);
	 			 }
	 			 else
	 			 {
	 				Y_O_C_spn_other.setVisibility(arg1.GONE);
	 			 }
	 				 
	 			 		
	 		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			Y_O_C_spn_other.setVisibility(arg0.GONE);
		}
	 		 
	 	}*/
	 public boolean getFromArray(String GCH_BI_GBI_YOC2, String[] bI_GBI_year)
	 {
	 	for(int i=0;i < bI_GBI_year.length;i++)
	 	 {
	 		 if(GCH_BI_GBI_YOC2.equals(bI_GBI_year[i]))
	 		 {
	 			 return false;
	 		 }
	 	 }
	 	return true;
	 }
}
